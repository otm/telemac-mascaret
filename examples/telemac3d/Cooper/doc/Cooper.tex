\chapter{Buoyant tracer (Cooper)}

\section{Purpose}

This test demonstrates the ability of \telemac{3D} to model the buoyancy
of an active tracer.

\section{Description}

We consider a square channel of 4,000~m side with a flat bottom at
$z = -10$~m with a bump at $z = -6$~m in the middle
($x = 2,000$~m; $y = 2,000$~m) (see Figure \ref{fig:Cooper:bottom}).
The source of tracer is located above the bump at $z = -5$~m.\\
We observe the buoyancy of the active tracer.

\begin{figure}[H]
 \centering
\includegraphicsmaybe{[width=0.49\textwidth]}{../img/mesh.png}
\includegraphicsmaybe{[width=0.49\textwidth]}{../img/bathy.png}
 \caption{Horizontal mesh and bathymetry.}\label{fig:Cooper:bottom}
\end{figure}

\subsection{Initial and Boundary Conditions}

The computation is initialised with a constant elevation equal to 0~m
and no velocity.
The initial value of tracer is 0 everywhere.
See Figure \ref{fig:Cooper:initial}.

\begin{figure}[H]
 \centering
\includegraphicsmaybe{[width=0.95\textwidth]}{../img/tracer_i.png}
\includegraphicsmaybe{[width=0.95\textwidth]}{../img/velocity_i.png}
 \caption{Initial state Tracer (up) and velocity (bottom).}\label{fig:Cooper:initial}
\end{figure}

The boundary conditions (closed boundaries) are:
\begin{itemize}
\item For the solid walls, a slip condition on channel banks is used for the
velocities,
\item On the bottom, a Nikuradse’s formula with asperities of 0.01~m
is prescribed.
\end{itemize}

Tracer discharge at source is equal to 20.0~m$^3$/s whereas tracer value at
source is 333.33~g/L or kg/m$^3$.

\subsection{Mesh and numerical parameters}

The 2D mesh (Figure \ref{fig:Cooper:bottom})
is made of 3,204 triangular elements (1,683 nodes).

11 fixed planes are regularly spaced in the vertical direction
with the 7th plane fixed at -4~m
(see Figure \ref{fig:Cooper:meshV}).

\begin{figure}[H]
 \centering
\includegraphicsmaybe{[width=0.95\textwidth]}{../img/vert_mesh.png}
 \caption{Vertical mesh.}\label{fig:Cooper:meshV}
\end{figure}

The non-hydrostatic version of \telemac{3D} is used.

To solve the advection, the N-type MURD scheme (scheme \#4) is used for both
velocities and tracer.

The time step is 5~s for a simulated period of 30~min (= 1,800~s).

To accelerate the solving of diffusion of velocities, tracers and Poisson
Pressure Equations (as using conjugate gradient), preconditing 34
(= 2 $\times$ 17) is chosen for these 3 operations, which enables to only
need 1 or very few iterations to converge to the chosen accuracy
(mainly default value 10$^{-8}$ or 10$^{-15}$ for tracers as easily reached
for this example).
\\

Mass conservation is improved (up to machine precision) by using default value
for the keyword \telkey{TREATMENT OF NEGATIVE DEPTHS} (= 2), instead of the old
default value = 1 (smoothings) for this example (until release 9.0).

\subsection{Physical parameters}

Constant diffusion of velocity:
\begin{itemize}
\item Horizontal: $10^{-4}$~m$^2$/s,
\item Vertical: no.
\end{itemize}
Constant diffusion of tracer: 
\begin{itemize}
\item Horizontal: no,
\item Vertical: 0.1~m$^2$/s.
\end{itemize}
Tracer density law specifying a $\beta$ spatial expansion coefficient of
0.0003~K$^{-1}$ and a standard value of the tracer of 0.0.\\

\section{Results}

Figure \ref{fig:Cooper:finalal} highlights that the buoyancy of the active
tracer generates vertical velocities,
thus establishing a large recirculation around the bump.
This is not due to the injected flow rate.

\begin{figure}[H]
 \centering
\includegraphicsmaybe{[width=0.95\textwidth]}{../img/tracer_f.png}
\includegraphicsmaybe{[width=0.95\textwidth]}{../img/velocity_f.png}
 \caption{Final state Tracer (up) and velocity (bottom).}\label{fig:Cooper:final}
\end{figure}

\begin{figure}[H]
 \centering
\includegraphicsmaybe{[width=0.95\textwidth]}{../img/vectorial.png}
 \caption{Vectorial view of celerity with a zoom.}\label{fig:Cooper:finalal}
\end{figure}

Mass balance of the log file after 1,800~s:
\begin{lstlisting}[language=TelFortran]
                FINAL MASS BALANCE
T =        1800.0000

--- WATER ---
INITIAL VOLUME                      :    0.1598743E+09
FINAL VOLUME                        :    0.1599103E+09
VOLUME EXITING (BOUNDARY OR SOURCE) :    -36000.00
TOTAL VOLUME LOST                   :    0.2086163E-06

--- TRACER 1: TRACER 1        , UNIT : ??              * M3)
INITIAL QUANTITY OF TRACER          :     0.000000
FINAL QUANTITY OF TRACER            :    0.1199988E+08
QUANTITY EXITING (BOUNDARY/SOURCE)  :   -0.1199988E+08
TOTAL QUANTITY OF TRACER LOST       :    0.1434237E-06
\end{lstlisting}

The amount of water injected by the source is correct:
20~m$^3$/s $\times$ 1,800~s~=~36,000~m$^3$.
The amount of tracer injected is correct:
333.33~kg/m$^3 \times $20~m$^3$/s $\times$ 1,800~s~=~1.19999 10$^7$~kg.
Moreover, one can see that mass is well conserved (water and tracer).

\section{Comments}

The tested steering file is “t3d\_cooper.cas”.
The cases “t3d\_cooper-hyd.cas” \& “t3d\_cooper-supg.cas” have been deleted
just after the V8P0 release.

\section{Conclusion}

\telemac{3D} simulates correctly the buoyancy of an active tracer.
