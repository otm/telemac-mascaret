\chapter{Automatic Mesh Displacement (amr)}

\section{Purpose}
The AMD (Adaptive Mesh Displacement) method places nodes along the vertical
depending on the vertical gradient of a referenced quantity (such as tracer)
independently of its horizontal neighbours. In order to reduce the local
steepness of the horizontal variations of intermediate surfaces between two
neighbouring verticals, a simple low pass filter is used in two dimensions where
the average value of one node is combined with half the value of its neighbouring
nodes and solved through a Laplace equation.
This test case demonstrates the effectiveness of the simple low pass filter.
A demonstration of the AMD method itself is presented through the test case “lock-exchange”.

\section{Description}
For illustrative purposes we consider a point source of tracer located at the
bottom of a straight channel with rectangular cross section with a constant flow
in the absence of friction (whereas it is not recommended to put a punctual
source on the bottom for imperviousness reason).
A depth averaged velocity of 1~m/s is applied at the entrance of the channel.
At the source, the tracer concentration is arbitrarily set to 215 units,
discharged at a rate of 0.5~m$^3$/s.

\subsection{Geometry and Mesh}
The channel is 1 km~long, 100~m wide and 10~m deep.
The edge length of the mesh is uniform, set at about 10~m.
\begin{figure}
 \centering
\includegraphicsmaybe{[width=0.49\textwidth]}{../img/meshsigma.png}
\includegraphicsmaybe{[width=0.49\textwidth]}{../img/vert_meshsigma.png}
\caption{Horizontal mesh and initial vertical mesh for the $\sigma$ computation.}
\label{fig:amrsigma:mesh}
\end{figure}

\begin{figure}
 \centering
\includegraphicsmaybe{[width=0.49\textwidth]}{../img/mesh.png}
\includegraphicsmaybe{[width=0.49\textwidth]}{../img/vert_mesh.png}
\caption{Horizontal mesh and initial vertical mesh for the AMD computation.}
\label{fig:amr:mesh}
\end{figure}

\subsection{Numerical parameters}
The AMD method is activated with the keyword \telkey{MESH TRANSFORMATION} = 5.

\section{Reference}
This test case is based on the work presented within the PhD Thesis of S.E.
Bourban [2013, Stratified Shallow Flow Modelling].

\section{Results}
The steady state results of the following two test cases are considered:

\begin{itemize}
\item A standard vertical discretisation based on 21 horizontal surfaces, which
in this case of a flat bottom and free surface can be associated either to a
$\sigma$-stretched transformation or a fixed horizontal layering
(Figure \ref{fig:amrsigma:mesh}).

The top left inset of Figure \ref{fig:amr:result1} shows the
concentration contours of the tracer in a vertical cross section along the
$xz$--plane, through the middle of the channel.
The 21 horizontal lines, from the bottom of the channel at $z$ = -10~m to the top
of the channel at $z$ = 0~m highlight, the locations of the 21 surfaces in the
3D mesh.
The top right inset also shows a vertical cross section but along the $yz$--plane,
at $x$ = 250~m.
The bottom inset of Figure \ref{fig:amr:result1} shows a horizontal cross
section along the $xy$--plane, through the bottom plane of the channel.

\begin{figure}
  \includegraphicsmaybe{[width=0.85\textwidth]}{../img/coupeysigma.png}
  \includegraphicsmaybe{[width=0.14\textwidth]}{../img/coupexsigma.png}
  \includegraphicsmaybe{[width=0.85\textwidth]}{../img/fondsigma.png}
 \caption{$\sigma$ results.}\label{fig:amr:result1}
\end{figure}

\begin{figure}
  \includegraphicsmaybe{[width=0.85\textwidth]}{../img/coupey.png}
  \includegraphicsmaybe{[width=0.14\textwidth]}{../img/coupex.png}
  \includegraphicsmaybe{[width=0.85\textwidth]}{../img/fond.png}  
 \caption{AMD results.}\label{fig:amr:result2}
\end{figure}

\item Use of the AMD method based on only 11 surfaces, or half the number of layers.

Again, the top left and right insets of Figure \ref{fig:amr:result2} show the
concentration contours in vertical cross sections along the $xz$--plane and
$yz$--plane respectively.
The 11 curvatures of the mesh in the vicinity of the highest gradient of the
concentration highlight the successful adaptation of the 3D mesh.
The bottom inset of Figure \ref{fig:amr:result2} shows the concentration contours
in a horizontal cross section along the same $xy$--plane as in Figure
\ref{fig:amr:result1}.
\end{itemize}

%\begin{figure}
% \centering
%\includegraphicsmaybe{[width=0.49\textwidth]}{../img/mesh.png}
% \caption{Horizontal mesh and bathymetry.}\label{fig:amr:meshamr}
%\end{figure}

A comparison of the figures shows that using the AMD method allows a better
characterisation of the plume horizontally and vertically yet with half the
number of intermediate surfaces.

\section{Conclusion}
This test case demonstrates the effectiveness of the simple low pass filter
combined with the AMD method.
