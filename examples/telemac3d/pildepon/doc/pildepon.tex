\chapter{Flow in a channel with 2 bridge piers (pildepon)}

\section{Purpose}

This example demonstrates the ability of \telemac{3D} to represent
the impact of an obstacle on a channel flow.
It also demonstrates the capability to represent unsteady eddies in a
model with steady state boundary.
Finally, it compares the advection of tracers with several methods.

\section{Description}

The configuration is a 28.5~m long and 20~m wide prismatic channel with
trapezoidal cross-section contains bridge-like obstacles in one cross-section
made of two abutments and two circular 4~m diameter piles
(See Figure \ref{t3d:pildepon:fig:Bottom}).
The flow resulting from steady state boundary conditions is studied.
The deepest water depth is 4~m.

\begin{figure}[!htbp]
 \centering
 \includegraphicsmaybe{[width=0.9\textwidth]}{../img/Bottom.png}
 \caption{Bottom elevation.}
 \label{t3d:pildepon:fig:Bottom}
\end{figure}

\subsection{Initial and Boundary Conditions}

The computation is initialised with a constant elevation equal to 0~m
and no velocity.

The boundary conditions are:
\begin{itemize}
\item For the solid walls, a slip condition on channel banks is used for the
velocities,
\item On the bottom, a Strickler law with friction coefficient equal to
40~m$^{1/3}$/s is prescribed,
\item Upstream a flowrate equal to 62~m$^3$/s is prescribed,
linearly increasing from 0 to 62~m$^3$/s during the first 20~s,
\item Downstream the water level is equal to 0.~m (= initial elevation).
\end{itemize}

\subsection{Mesh and numerical parameters}

The 2D mesh (Figure \ref{t3d:pildepon:fig:meshH})
is made of 4,304 triangular elements (2,280 nodes).
6 planes are regularly spaced on the vertical (see Figure \ref{t3d:pildepon:fig:meshV}).\\

\begin{figure}[!htbp]
 \centering
 \includegraphicsmaybe{[width=0.9\textwidth]}{../img/MeshH.png}
 \caption{Horizontal mesh.}
 \label{t3d:pildepon:fig:meshH}
\end{figure}

\begin{figure}[!htbp]
 \centering
 \includegraphicsmaybe{[width=0.9\textwidth]}{../img/MeshV.png}
 \caption{Initial vertical mesh.}
 \label{t3d:pildepon:fig:meshV}
\end{figure}

2 computations are run: one with the hydrostatic hypothesis and one with
non-hydrostatic version.

To solve the advection, the LIPS scheme is used for velocities (default choice).
It used to be the method of characteristics but there were differences between
sequential and parallel runs with this method for this example.

GMRES is used for solving the propagation (option 7), conjugate gradient for the
diffusion of velocities (option 1).
Conjugate gradient is used to solve the Poisson Pressure Equation because
a little bit more efficient for this example.

The implicitation coefficients for depth and velocities are respectively equal
to 0.6 and 1.

The time step is 0.4~s for both hydrostatic and non-hydrostatic versions.
The simulated period is 80~s for both cases.
\\

Default value since release 9.0 for keyword \telkey{TREATMENT OF NEGATIVE DEPTHS}
(= 2, i.e. flux control) is chosen to improve the mass-conservation of water
(up to machine precision).
\\

In the non-hydrostatic example, 10 tracers are released from the inlet
(initial value = 0 for every tracer).
Horizontal coefficient for diffusion of tracers is equal to 0.01~m$^2$/s
whereas vertical coefficient for diffusion of tracers is equal to 0.1~m$^2$/s.
Every tracer is advected with a different advection scheme:
\begin{itemize}
\item Method of characteristics (= 1),
\item SUPG,
\item Leo Postma,
\item MURD N-type,
\item MURD PSI-Type,
\item NERD \#13,
\item NERD \#14,
\item Predictor-Correct order 1,
\item Predictor-Correct order 2,
\item LIPS.
\end{itemize}

\subsection{Physical parameters}

A mixing length model is used as vertical turbulence model combined with
constant horizontal viscosity for velocity equal to 0.005~m$^2$/s.

\section{Results}

The obstacles create a contraction of the streamlines, and Karman
vortices are observed behind the piers.
The Karman vortices produce an asymmetry of the velocity field.
This velocity field is unsteady behind the piers in the Karman vortices
(see top of Figures \ref{t3d:pildepon:VeloHydro} and \ref{t3d:pildepon:VeloNH},
where surface velocities are
shown for the hydrostatic and non-hydrostatic simulations respectively).
%At the bottom of the same figure a time profile of the depth-averaged
%vertical velocity is given.
%After a transition of about 150~s a periodic regime takes place.
%Streamlines for positions where $x$ > -0.5~m (behind the piles) are
%shown of figure 3.5.4 for the hydrostatic (a) and non-hydrostatic (b)
%simulations.
%The figures show that the Karman vortices are better represented by the
%non-hydrostatic simulation, indicating the necessity to solve such
%turbulence problems using the non-hydrostatic version of \telemac{3D}.

\begin{figure}[H]
  \centering
  \includegraphicsmaybe{[width=0.9\textwidth]}{../img/VelocityHHydro.png}
  \caption{Velocity magnitude at the surface at final time step for the hydrostatic case.}
  \label{t3d:pildepon:VeloHydro}
\end{figure}

\begin{figure}[H]
  \centering
  \includegraphicsmaybe{[width=0.9\textwidth]}{../img/VelocityHNH.png}
  \caption{Velocity magnitude at the surface at final time step for the non-hydrostatic case.}
  \label{t3d:pildepon:VeloNH}
\end{figure}

Figures \ref{t3d:pildepon:FreeSurfHydro} and \ref{t3d:pildepon:FreeSurfNH} show
the free surface at final time step (=~80~s) for the hydrostatic and non-hydrostatic
computations.

\begin{figure}[H]
  \centering
  \includegraphicsmaybe{[width=0.9\textwidth]}{../img/FreeSurfaceHydro.png}
  \caption{Free surface at final time step for the hydrostatic case.}
  \label{t3d:pildepon:FreeSurfHydro}
\end{figure}

\begin{figure}[H]
  \centering
  \includegraphicsmaybe{[width=0.9\textwidth]}{../img/FreeSurfaceNH.png}
  \caption{Free surface at final time step for the non-hydrostatic case.}
  \label{t3d:pildepon:FreeSurfNH}
\end{figure}

Mass-conservation of tracer is not ensured with the method of characteristics.
With SUPG, it is not as good as other distributive schemes.
In the distributive schemes family, mass conservation of tracer with PSI and
NERD \#14 is less well preserved compared to the other advection schemes
(Leo Postma, N, NERD \#13, NERD Leo Postma for Tidal Flats, Predictor-Corrector
order 1 and 2 + LIPS).

\begin{lstlisting}[language=TelFortran]
                FINAL MASS BALANCE
T =          80.0000

--- WATER ---
INITIAL VOLUME                      :     1637.919
FINAL VOLUME                        :     1649.809
VOLUME EXITING (BOUNDARY OR SOURCE) :    -11.88995
TOTAL VOLUME LOST                   :   -0.5378809E-11

--- TRACER 1: TRACEUR 1       , UNIT : CARACTERISTIQUES* M3)
INITIAL QUANTITY OF TRACER          :     0.000000
FINAL QUANTITY OF TRACER            :     122.0862
QUANTITY EXITING (BOUNDARY/SOURCE)  :    -161.2828
TOTAL QUANTITY OF TRACER LOST       :     39.19659

--- TRACER 2: TRACEUR 2       , UNIT : SUPG            * M3)
INITIAL QUANTITY OF TRACER          :     0.000000
FINAL QUANTITY OF TRACER            :     112.9911
QUANTITY EXITING (BOUNDARY/SOURCE)  :    -112.9831
TOTAL QUANTITY OF TRACER LOST       :   -0.7956231E-02

--- TRACER 3: TRACEUR 3       , UNIT : LPO             * M3)
INITIAL QUANTITY OF TRACER          :     0.000000
FINAL QUANTITY OF TRACER            :     131.6895
QUANTITY EXITING (BOUNDARY/SOURCE)  :    -131.6895
TOTAL QUANTITY OF TRACER LOST       :    0.3009006E-09

--- TRACER 4: TRACEUR 4       , UNIT : NSC             * M3)
INITIAL QUANTITY OF TRACER          :     0.000000
FINAL QUANTITY OF TRACER            :     131.6393
QUANTITY EXITING (BOUNDARY/SOURCE)  :    -131.6393
TOTAL QUANTITY OF TRACER LOST       :    0.3065850E-09

--- TRACER 5: TRACEUR 5       , UNIT : PSI             * M3)
INITIAL QUANTITY OF TRACER          :     0.000000
FINAL QUANTITY OF TRACER            :     124.5492
QUANTITY EXITING (BOUNDARY/SOURCE)  :    -124.5492
TOTAL QUANTITY OF TRACER LOST       :   -0.1365517E-05

--- TRACER 6: TRACEUR 6       , UNIT : LPO_TF          * M3)
INITIAL QUANTITY OF TRACER          :     0.000000
FINAL QUANTITY OF TRACER            :     131.7259
QUANTITY EXITING (BOUNDARY/SOURCE)  :    -131.7259
TOTAL QUANTITY OF TRACER LOST       :   -0.7301537E-09

--- TRACER 7: TRACEUR 7       , UNIT : NSC_TF          * M3)
INITIAL QUANTITY OF TRACER          :     0.000000
FINAL QUANTITY OF TRACER            :     131.6728
QUANTITY EXITING (BOUNDARY/SOURCE)  :    -131.6728
TOTAL QUANTITY OF TRACER LOST       :    0.8511336E-07

--- TRACER 8: TRACEUR 8       , UNIT : PC1             * M3)
INITIAL QUANTITY OF TRACER          :     0.000000
FINAL QUANTITY OF TRACER            :     123.0018
QUANTITY EXITING (BOUNDARY/SOURCE)  :    -123.0018
TOTAL QUANTITY OF TRACER LOST       :    0.7421335E-09

--- TRACER 9: TRACEUR 9       , UNIT : PC2             * M3)
INITIAL QUANTITY OF TRACER          :     0.000000
FINAL QUANTITY OF TRACER            :     122.9811
QUANTITY EXITING (BOUNDARY/SOURCE)  :    -122.9811
TOTAL QUANTITY OF TRACER LOST       :    0.5133813E-09

--- TRACER10: TRACEUR 10      , UNIT : LIPS            * M3)
INITIAL QUANTITY OF TRACER          :     0.000000
FINAL QUANTITY OF TRACER            :     128.0557
QUANTITY EXITING (BOUNDARY/SOURCE)  :    -128.0557
TOTAL QUANTITY OF TRACER LOST       :    0.4271499E-09
\end{lstlisting}

SUPG and LIPS for the advection of tracers are not the best parallelised
advection schemes for this example, to be investigated.

\section{Conclusion}

\telemac{3D} can be used to study the hydrodynamic impact of engineering
works (like bridge piers), and to analyse unsteady flow, such as the
Karman vortices.
