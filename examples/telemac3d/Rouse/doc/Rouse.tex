\chapter{Non cohesive suspended sediment transport (Rouse)}

\section{Purpose}

This test validates the modelling of the hydrodynamics and non-cohesive
suspended sediment transport, in a permanent and uniform flow.
We compare the mean flow velocities to the logarithmic profile and the
sediment concentration to an analytical solution derived from the Rouse
profile \cite{HervouetVillaret_2004}.

\section{Description}

It consists of a steady and uniform flow in a rectangular channel
(500~m $\times$ 100~m) with constant slope, without friction on the
lateral boundaries, and with friction on the bottom.
The turbulence model is chosen to be consistent with the logarithmic
velocity profile on the vertical.
At the entrance of the channel, sediment is introduced with a constant
concentration along the vertical, and an equilibrium profile gradually
appears downstream.
The constant slope is of 1.01 10$^{-3}$ (at $x$ = 0~m, $z$ = 0~m and at
$x$ = 500~m, $z$ = -0.505~m),
designed so as to get a uniform flow with a Strickler coefficient equal
to 50~m$^{1/3}$/s when the depth is 0.5~m.

%\section{Reference}
%[1] HERVOUET J.-M., VILLARET C. Profil de Rouse modifié, une solution
%analytique pour valider TELEMAC-3D en sédimentologie.
%EDF-LNHE Report HP-75/04/013/A.

\subsection{Initial and Boundary Conditions}

The computation is initialised with a constant water depth equal to 0.5~m,
constant sediment concentration equal to 0.02~g/L
velocity and total viscosity field initialised with a logarithmic
profile along the vertical (see Figures \ref{t3d:Rouse:Velot0} and
\ref{t3d:Rouse:Visct0}).

\begin{figure}[H]
  \centering
  \includegraphicsmaybe{[width=\textwidth]}{../img/Velocityt0.png}
  \caption{Vertical distribution of velocity at initial time step.}
  \label{t3d:Rouse:Velot0}
\end{figure}

\begin{figure}[H]
  \centering
  \includegraphicsmaybe{[width=\textwidth]}{../img/ViscVt0.png}
  \caption{Vertical distribution of viscosity at initial time step.}
  \label{t3d:Rouse:Visct0}
\end{figure}

The boundary conditions are:
\begin{itemize}
\item For the solid walls, a slip condition on channel banks is used for the
velocities,
\item On the bottom, a Nikuradse law with bed roughness of $k_s$ = 0.0162~m
(equivalent to a Strickler coefficient of 50~m$^{1/3}$/s at the depth of
0.5~m) is prescribed,
\item Upstream a flowrate of 50~m$^3$/s and constant sediment concentration of
0.02~g/L are prescribed and logarithmic velocity profile,
\item Downstream the water level is equal to -0.005~m and logarithmic velocity
profile.
\end{itemize}

\subsection{Mesh and numerical parameters}

The 2D mesh (Figure \ref{t3d:Rouse:fig:meshH})
is made of 2,204 triangular elements (1,188 nodes).

16 planes are irregularly spaced (zstar) in the vertical direction
(see Figure \ref{t3d:Rouse:fig:meshV}).

\begin{figure}[!htbp]
 \centering
 \includegraphicsmaybe{[width=\textwidth]}{../img/MeshH.png}
 \caption{Horizontal mesh.}
 \label{t3d:Rouse:fig:meshH}
\end{figure}

\begin{figure}[!htbp]
 \centering
 \includegraphicsmaybe{[width=\textwidth]}{../img/MeshV.png}
 \caption{Initial vertical mesh along $y$ = 1~m.}
 \label{t3d:Rouse:fig:meshV}
\end{figure}

The hydrostatic version of \telemac{3D} is used.

To solve the advection, the method of characteristics is used for velocities
and sediment.

The time step is 2~s for a simulated period of 2,000~s.

\subsection{Physical parameters}

Nezu and Nakagawa mixing length model is used as vertical turbulence model
combined with constant horizontal viscosity for velocity equal to 0.1~m$^2$/s.

Sediment of mean diameter of 6~mm with a settling velocity of -0.01~m/s
is modelled with no influence of turbulence on sediment settling velocity.
Laminar diffusivity of sediment is equal to 10$^{-4}$~m$^2$/s for the vertical
direction (10$^{-6}$~m$^2$/s for the horizontal directions).

\section{Results}

Figure 8 (to be drawn!!!) compares the theoretical \cite{HervouetVillaret_2004}
(i.e. logarithmic) velocity
profile with the computed result and shows an excellent agreement.
One can see that the point at the first plane above the bottom coincides
with the theoretical value, which guarantees that the friction velocity
is correct.\\
Figure 9 (to be drawn!!!) compares the theoretical \cite{HervouetVillaret_2004}
and the computed turbulent
viscosity profiles.
The maximum error happens at the first two planes below the surface
(perhaps because of the size of the mesh at this level).\\
Figure 10 (to be drawn!!!) compares the classic theoretical Rouse profile, the
modified Rouse profile \cite{HervouetVillaret_2004},
and the numerical solution obtained with a
laminar viscosity of 10$^{-4}$~m$^2$/s.
The numerical solution is close to the modified profile.
In theory, the Rouse profile is only valid beyond the viscous layer.
The modified profile brings a notable modification only in this viscous
layer and is presented here for its interest in software validation.

Figures \ref{t3d:Rouse:Velotf}, \ref{t3d:Rouse:Visctf}, \ref{t3d:Rouse:Seditf}
show the vertical distribution of velocity, viscosity and sediment concentration
at final time step.

\begin{figure}[H]
  \centering
  \includegraphicsmaybe{[width=\textwidth]}{../img/Velocitytf.png}
  \caption{Vertical distribution of velocity at final time step.}
  \label{t3d:Rouse:Velotf}
\end{figure}

\begin{figure}[H]
  \centering
  \includegraphicsmaybe{[width=\textwidth]}{../img/ViscVtf.png}
  \caption{Vertical distribution of viscosity at final time step.}
  \label{t3d:Rouse:Visctf}
\end{figure}

\begin{figure}[H]
  \centering
  \includegraphicsmaybe{[width=\textwidth]}{../img/Seditf.png}
  \caption{Vertical distribution of sediment concentration at final time step.}
  \label{t3d:Rouse:Seditf}
\end{figure}

\section{Conclusion}

These comparisons with analytical solutions, in hydrodynamics or
suspended sediment transport, thoroughly valid the treatment of
diffusion on the vertical in \telemac{3D}, including a settling
 velocity.
The Nezu and Nakagawa mixing length turbulence model, and the
computation of velocity gradients, is also validated.
