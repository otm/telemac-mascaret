\chapter{Wesel}

\section{Purpose}
This test case shows a real case application
with tidal flats and very large time steps.
It was made for comparison with the program UnTRIM
from Prof. Casulli (University of Trento).
The mesh is compatible with UnTRIM meshes which have restrictions according the
orthogonality.

\section{Description}

\subsection{Geometry and mesh}
The model area is a 9 km stretch of Lower Rhine River near the towns of Wesel
and Xanten (Rh-km 812.5 - 821.5).
The resolution is very coarse with mean node distances of about 6 m in the main
channel and about 30 m at the floodplains.
The vertical is discretised by only 3 sigma layers
(see Figure \ref{t3d:Wesel:vertical_grid}).
The horizontal mesh contains 9,064 nodes and 17,340 elements and can be seen in
Figure \ref{t3d:Wesel:mesh}.
The mesh is compatible to UnTRIM meshes which means that the center of each
triangle is inside the triangle.
The bathymetry is shown in Figure \ref{t3d:Wesel:Bathy}.

\begin{figure}[h!]
\centering
\includegraphicsmaybe{[width=\textwidth]}{../img/wesel_mesh0.png}
\caption{Horizontal mesh.}
\label{t3d:Wesel:mesh}
\end{figure}

\begin{figure}[h!]
\centering
\includegraphicsmaybe{[width=\textwidth]}{../img/vertical_grid.png}
\caption{Vertical discretisation with 3 planes.}
\label{t3d:Wesel:vertical_grid}
\end{figure}

\begin{figure} [h!]
\centering
\includegraphicsmaybe{[width=\textwidth]}{../img/Bathy.png}
\caption{Bathymetry.}
\label{t3d:Wesel:Bathy}
\end{figure}


\subsection{Initial condition}

The initialising of the water level is done with the subroutine
\telfile{surfini.f} called by \telfile{user\_condi3d\_h.f}.
From water level measurements in m+NN at low water conditions in 1997 in the
file fo1\_wesel.txt (\telkey{FORMATTED DATA FILE 1}) the initial water level is
interpolated (see Figure \ref{t3d:Wesel:free_surface0}).
The coordinates are given as the left and right hectometer points.
The initial velocities are set to zero.

\begin{figure} [h!]
\centering
\includegraphicsmaybe{[width=\textwidth]}{../img/FreeSurface0.png}
\caption{Initial water levels.}
\label{t3d:Wesel:free_surface0}
\end{figure}

\subsection{Boundary conditions}

At the inlet, the discharge is imposed across the full cross section.
In order to avoid instabilities in the beginning the discharge is increased
from 0 to 1,061 m$^3$/s in 30 min and remains constant afterwards.
At the outlet the water depth is fixed to 11.82 m+NN.


\subsection{Physical parameters}
For the time optimisation constant values for the friction and the turbulence
are chosen:
The Nikuradse friction law is used and an uniform equivalent sand roughness of
3.5 cm is applied.
The constant turbulent viscosities are set to 2 m$^2$/s for the horizontal
dimension and 0.1 m$^2$/s for the vertical dimension.
This very high horizontal value stabilises the simulation.
Typically values of one to two orders of magnitudes lower are applied.

\subsection{Numerical parameters}
12 h are simulated with a time step of 50 s to ensure steady state conditions.
Figure \ref{t3d:Wesel:fluxes} shows that the flux at the outlet equals the flux
at the inlet at the end of the simulation time.
The non-hydrostatic version is applied and the NERD scheme (\# 13) is used for
the advection type.
Relatively low accuracies of 10$^{-4}$ for propagation and pressure
and $10^{-5}$ for diffusion of velocities are chosen.
Full implicit conditions are set for depth and velocities.

\section{Results}

The final water levels are shown in Figure \ref{t3d:Wesel:free_surface}. 
The difference between the initial and the final water levels are quite small
as the initial water levels are calculated from the measurements. 
The velocity distribution of a typical cross section in the straight part of the
river is presented in Figure \ref{t3d:Wesel:velo_cross}.
Due to the small number of equally spaced planes, a typical logarithmic profile
was not produced.
The velocity is nearly constant over the vertical.
A comparison along the main channel between the measured water levels and the
simulated ones is presented in Figure \ref{t3d:Wesel:diff_waterlevels}.
The simulated water levels are a little higher at the inlet, it can be assumed
that the friction should be increased for a perfect agreement.

%\begin{figure} [h!]
%\centering
%\includegraphicsmaybe{[width=\textwidth]}{../img/Velocity_tf_pos.png}
%\caption{Final velocities.}
%\label{t3d:Wesel:velocity}
%\end{figure}

\begin{figure} [h!]
\centering
\includegraphicsmaybe{[width=\textwidth]}{../img/FreeSurface.png}
\caption{Final water levels.}
\label{t3d:Wesel:free_surface}
\end{figure}

\begin{figure} [h!]
\centering
\includegraphicsmaybe{[width=\textwidth]}{../img/veloV.png}
\caption{Velocities in a cross section at the straight part of the river.}
\label{t3d:Wesel:velo_cross}
\end{figure}

\begin{figure} [h!]
\centering
\includegraphicsmaybe{[width=\textwidth]}{../img/diff_free_surface.png}
\caption{Comparison of measured and simulated final water levels along river axis.}
\label{t3d:Wesel:diff_waterlevels}
\end{figure}

\begin{figure} [h!]
\centering
\includegraphicsmaybe{[width=\textwidth]}{../img/fluxes.png}
\caption{Evolution of inlet and outlet fluxes.}
\label{t3d:Wesel:fluxes}
\end{figure}

\section{Conclusion}
The example shows a successful simulation of low water steady state conditions
with tidal flats.
The configuration is time optimised by using a coarse mesh, a big time step,
a low solver accuracy, a full implicit scheme and a high turbulent viscosity.


%\section{References}

%Karim, F., Kennedy, J. F.: (1981) \textit {Computer – based predictors for sediment discharge and friction factor of alluvial streams}, Report No. 242, Iowa Institute of Hydraulic Research, University of Iowa, Iowa City, Iowa.\newline

%Rátky, Éva: (2006) \textit{Modellierung von Geschiebezugaben am Niederrhein bei Wesel
%mit einem 2D-tiefengemittelten morphologischen Modell}, Thesis, Universität Karlsruhe (TH), Karlsruhe.\newline

%Savova, S.: (2004) \textit {Datengrundlage für den Testfall Wesel-Xanten}, Report, BAW, Karlsruhe.\newline
