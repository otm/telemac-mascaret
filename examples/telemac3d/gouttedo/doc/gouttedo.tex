\chapter{Gaussian water surface with reflecting boundary conditions (gouttedo)}
\label{gouttedo}

\section{Description}
\bigskip
This test case presents the evolution of a Gaussian water surface centred in 
a square domain with solid boundaries. 
It demonstrates that the \telemac{3D} solution is not polarised
because it can simulate the circular spreading of a wave in a square
domain.
It also shows that the no-flow condition is satisfied on solid
boundaries and that the solution remains symmetric after reflection
of the circular wave on the boundaries.

\bigskip
The considered domain is a square of 20.1~m length with a flat bottom. 
The fluid is initially at rest with a Gaussian free surface in the 
centre of a square domain (see Figure \ref{t3d:gouttedo:fig:initial}).
The evolution of the surface and the reflection of the wave by the 
solid boundaries are then calculated during 4 s.
Note that the turbulent viscosity is constant in both directions 
and equal to the water molecular viscosity.

\subsection{Initial and boundary conditions}
\bigskip
The initial water level is a Gaussian-shape, where the water 
depth is 4.8~m at the centre and 2.4~m at boundary 
(Figure \ref{t3d:gouttedo:fig:initial}). In addition, the velocity is null.
Note that the initial free surface elevation is prescribed in 
the \telfile{USER\_CONDI3D\_H} subroutine.

\begin{figure}[!htbp]
\centering
 \includegraphicsmaybe{[width=0.7\textwidth]}{../img/water_depth_0.png}
\caption{Initial free surface.}
 \label{t3d:gouttedo:fig:initial}
\end{figure}

\bigskip
The boundary conditions are:
\begin{itemize}
\item For the solid boundaries (or lateral walls), a slip condition is
imposed for the velocity,
\item On the bottom a Chézy law with a friction coefficient equal to 
60~$\text{m}^{1/2}.\text{s}^{-1}$ is imposed (default value
for the friction coefficient until version 8.0).
\end{itemize}

\subsection{Mesh and numerical parameters}
\bigskip
The mesh (Figures \ref{t3d:gouttedo:fig:meshH} and \ref{t3d:gouttedo:fig:meshV})
is composed of 8,978 triangular elements (4,624 nodes) with 3 planes  
regularly spaced on the vertical, to form prism elements.

\begin{figure}[!htbp]
 \centering
 \includegraphicsmaybe{[width=0.68\textwidth]}{../img/Mesh.png}
 \caption{Horizontal mesh.}
 \label{t3d:gouttedo:fig:meshH}
\end{figure}
\begin{figure}[!htbp]
 \centering
 \includegraphicsmaybe{[width=\textwidth]}{../img/MeshV.png}
 \caption{Vertical mesh at initial time step.}
 \label{t3d:gouttedo:fig:meshV}
\end{figure}
\bigskip
The time step is 0.04~s for a simulated period of 4~s. 

\bigskip
This case is computed with the non-hydrostatic version. The LIPS scheme 
is used for the velocities (scheme 5) to solve the advection and 
the GMRES (Generalized Minimal Residual Method, scheme 7) 
is used for propagation. The implicitation coefficients for depth 
and velocities are equal to 0.6.
\\

Mass conservation is greatly improved (up to machine precision) by using default
values for keywords \telkey{TIDAL FLATS} (= YES) and
\telkey{TREATMENT OF NEGATIVE DEPTHS} (= 2), contrary to choices for this
example until 9.0 (NO and 1).

\section{Results}
\bigskip
The wave spreads circularly around the initial water surface peak
elevation (Figure \ref{t3d:gouttedo:fig:depth_evol3D}).
When it reaches the boundaries, reflection well occurs as expected.
The reflected wave is also axi-symmetric.
In addition, the final volume in the domain is equal to the initial volume.

\begin{figure}[!htbp]
\begin{minipage}[t]{0.50\textwidth}
 \centering
 \includegraphicsmaybe{[width=\textwidth]}{../img/water_depth_0.png}
\end{minipage}
\begin{minipage}[t]{0.50\textwidth}
 \centering
 \includegraphicsmaybe{[width=\textwidth]}{../img/water_depth_08.png}
\end{minipage}\\
\begin{minipage}[t]{0.50\textwidth}
 \centering
 \includegraphicsmaybe{[width=\textwidth]}{../img/water_depth_16.png}
\end{minipage}
\begin{minipage}[t]{0.50\textwidth}
 \centering
 \includegraphicsmaybe{[width=\textwidth]}{../img/water_depth_24.png}
\end{minipage}
\begin{minipage}[t]{0.50\textwidth}
 \centering
 \includegraphicsmaybe{[width=\textwidth]}{../img/water_depth_32.png}
\end{minipage}
\begin{minipage}[t]{0.50\textwidth}
 \centering
 \includegraphicsmaybe{[width=\textwidth]}{../img/water_depth_40.png}
\end{minipage}
 \caption{Water depth evolution in three dimensions.}
 \label{t3d:gouttedo:fig:depth_evol3D}
\end{figure}

%\section{Conclusion}

\bigskip
Even though the mesh is polarised (along the $x$ and $y$ directions and
the main diagonal), the solution is not.
Solid boundaries are treated properly: no bias occurs in the reflected
wave. Water mass is conserved.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\chapter{Gaussian water surface with open boundary conditions (thompson)}

\section{Description}
\bigskip
This test presents the evolution of a Gaussian water surface centred
in a square domain.
It is identical to validation case \ref{gouttedo} but with open boundaries.
It shows that the circular wave propagates out of the computational
domain freely without any reflection.
It demonstrates that the \telemac{3D} solution is not polarised because
it can simulate the circular spreading of a wave in square computation
domain.
Moreover, it also demonstrates that \telemac{3D} is capable to deal
with open boundaries without prescribing water depth or velocity by
using the Thompson method based on characteristic.

\bigskip
The considered domain is a square of 20.1~m length with a flat bottom. 
The fluid is initially at rest with a Gaussian free surface in the 
centre of a square domain (see Figure \ref{t3d:thomson:fig:initial}).
The evolution of the surface is then calculated during 4 s.\\
Note that the turbulent viscosity is constant in both directions 
and equal to the water molecular viscosity.

\subsection{Initial and boundary conditions}
\bigskip
The initial water level is fixed as a Gaussian-shape, where the water depth 
is 4.8~m at the centre and 2.4~m at boundary 
(Figure \ref{t3d:thomson:fig:initial}). The velocity is null everywhere.
Note that the initial free surface elevation is prescribed in 
the \telfile{USER\_CONDI3D\_H} subroutine.

\begin{figure}[!htbp]
 \centering
 \includegraphicsmaybe{[width=0.7\textwidth]}{../img/water_depth_th_0.png}
\caption{Initial free surface.}
 \label{t3d:thomson:fig:initial}
\end{figure}

\bigskip
The boundary conditions are:
\begin{itemize}
\item On lateral sides, the open boundary with the Thompson method 
based on characteristic is used,
\item On the bottom, Chézy law with friction coefficient equal to 
60~$\text{m}^{1/2}.\text{s}^{-1}$ is imposed (default value
for the friction coefficient until version 8.0).
\end{itemize}

\subsection{Mesh and numerical parameters}

\bigskip
The mesh (Figures \ref{t3d:thomson:fig:meshH} and \ref{t3d:thomson:fig:meshV})  
is composed of 8,978 triangular elements (4,624 nodes) with 3 planes  
regularly spaced on the vertical, to form prism elements.

\begin{figure}[!htbp]
 \centering
 \includegraphicsmaybe{[width=0.68\textwidth]}{../img/Mesh_th.png}
 \caption{Horizontal mesh.}
 \label{t3d:thomson:fig:meshH}
\end{figure}
\begin{figure}[!htbp]
 \centering
 \includegraphicsmaybe{[width=\textwidth]}{../img/MeshV_th.png}
 \caption{Vertical mesh (at initial time step).}
 \label{t3d:thomson:fig:meshV}
\end{figure}

\bigskip
The time step is 0.04~s for a simulated period of 4~s. 

\bigskip
This case is computed with the non-hydrostatic version. The LIPS scheme 
is used for the velocities (scheme 5) to solve the advection and the GMRES 
(Generalized Minimal Residual Method, scheme 7) is used for propagation. 
The implicitation coefficients for depth and velocities are equal to 0.6.
Moreover, the free surface gradient compatibility coefficient is equal to 0.8
to decrease spurious oscillations (0.9 is not enough, but 0.8 is OK).
\\

Mass conservation is greatly improved (up to machine precision) by using default
values for keywords \telkey{TIDAL FLATS} (= YES) and
\telkey{TREATMENT OF NEGATIVE DEPTHS} (= 2), contrary to choices for this
example until 9.0 (NO and 1).

\section{Results}

\bigskip
Figure \ref{t3d:thomson:fig:depth_evol3D}
shows that the wave spreads circularly around the initial water 
surface peak elevation when it reaches the boundaries,
the wave goes out of the domain freely, no reflection occurs as expected.

%\begin{figure}[!htbp]
%\begin{minipage}[t]{0.50\textwidth}
% \centering
% \includegraphicsmaybe{[width=\textwidth]}{../img/2D_time0.png}
%\end{minipage}
%\begin{minipage}[t]{0.50\textwidth}
% \centering
% \includegraphicsmaybe{[width=\textwidth]}{../img/2D_time08.png}
%\end{minipage}
%\begin{minipage}[t]{0.50\textwidth}
% \centering
% \includegraphicsmaybe{[width=\textwidth]}{../img/2D_time16.png}
%\end{minipage}
%\begin{minipage}[t]{0.50\textwidth}
% \centering
% \includegraphicsmaybe{[width=\textwidth]}{../img/2D_time24.png}
%\end{minipage}
%\begin{minipage}[t]{0.50\textwidth}
% \centering
% \includegraphicsmaybe{[width=\textwidth]}{../img/2D_time32.png}
%\end{minipage}
%\begin{minipage}[t]{0.50\textwidth}
% \centering
% \includegraphicsmaybe{[width=\textwidth]}{../img/2D_time04.png}
%\end{minipage}
% \caption{Water depth evolution in two dimensions.}
% \label{t3d:thomson:fig:depth_evol}
%\end{figure}

\begin{figure}[!htbp]
\begin{minipage}[t]{0.50\textwidth}
 \centering
 \includegraphicsmaybe{[width=\textwidth]}{../img/water_depth_th_0.png}
\end{minipage}
\begin{minipage}[t]{0.50\textwidth}
 \centering
 \includegraphicsmaybe{[width=\textwidth]}{../img/water_depth_th_08.png}
\end{minipage}\\
\begin{minipage}[t]{0.50\textwidth}
 \centering
 \includegraphicsmaybe{[width=\textwidth]}{../img/water_depth_th_16.png}
\end{minipage}
\begin{minipage}[t]{0.50\textwidth}
 \centering
 \includegraphicsmaybe{[width=\textwidth]}{../img/water_depth_th_24.png}
\end{minipage}\\
\begin{minipage}[t]{0.50\textwidth}
 \centering
 \includegraphicsmaybe{[width=\textwidth]}{../img/water_depth_th_32.png}
\end{minipage}
\begin{minipage}[t]{0.50\textwidth}
 \centering
 \includegraphicsmaybe{[width=\textwidth]}{../img/water_depth_th_40.png}
\end{minipage}
 \caption{Water depth evolution in three dimensions.}
 \label{t3d:thomson:fig:depth_evol3D}
\end{figure}

%\section{Conclusion}

\bigskip
Even though the mesh is polarised (along the $x$ and $y$ directions and
the main diagonal), the solution is not.
Open boundaries are treated properly: no bias occurs.
The Thompson method, based on characteristic,
enables to use open boundaries (444) without the need to specify water
depth or velocity at the boundary.
