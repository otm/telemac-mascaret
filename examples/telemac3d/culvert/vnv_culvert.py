
"""
Validation script for culvert
"""
from vvytel.vnv_study import AbstractVnvStudy
from execution.telemac_cas import TelemacCas, get_dico
from data_manip.extraction.telemac_file import TelemacFile

class VnvStudy(AbstractVnvStudy):
    """
    Class for validation
    """

    def _init(self):
        """
        Defines the general parameter
        """
        self.rank = 2
        self.tags = ['telemac3d']

    def _pre(self):
        """
        Defining the studies
        """

        # culvert scalar mode
        self.add_study('vnv_1',
                       'telemac3d',
                       't3d_culvert.cas')


        # culvert parallel mode
        cas = TelemacCas('t3d_culvert.cas', get_dico('telemac3d'))
        cas.set('PARALLEL PROCESSORS', 4)

        self.add_study('vnv_2',
                       'telemac3d',
                       't3d_culvert_par.cas',
                       cas=cas)

        del cas



    def _check_results(self):
        """
        Post-treatment processes
        """

        # Comparison with the last time frame of the reference file.
        self.check_epsilons('vnv_1:T3DRES',
                            'f3d_culvert.slf',
                            eps=[1e-7, 1e-6, 1e-6, 1e-6, 0.45])

        # Comparison with the last time frame of the reference file.
        self.check_epsilons('vnv_2:T3DRES',
                            'f3d_culvert.slf',
                            eps=[1e-6, 1e-6, 1e-6, 1e-6, 0.43])

        # Comparison between sequential and parallel run.
        self.check_epsilons('vnv_1:T3DRES',
                            'vnv_2:T3DRES',
                            eps=[1e-6, 1e-6, 1e-6, 1e-6, 0.40])


    def _post(self):
        """
        Post-treatment processes
        """
        from postel.plot_vnv import vnv_plot2d, vnv_plot1d_history
                # Getting files
        vnv_1_t3dgeo = self.get_study_file('vnv_1:T3DGEO')
        res_vnv_1_t3dgeo = TelemacFile(vnv_1_t3dgeo)
        vnv_1_t3dhyd = self.get_study_file('vnv_1:T3DHYD')
        res_vnv_1_t3dhyd = TelemacFile(vnv_1_t3dhyd)

        #Plotting mesh
        vnv_plot2d('BOTTOM',
                   res_vnv_1_t3dgeo,
                   plot_mesh=True,
                   fig_size=(12, 7),
                   fig_name='img/mesh')

        #Plotting FREE SURFACE
        vnv_plot1d_history('FREE SURFACE',
                           res_vnv_1_t3dhyd,
                           legend_labels='FREE SURFACE',
                           points=[[5., 10.], [35., 10.]],
                           x_label='$t$ (h)', y_label='$z$ (m)',
                           x_factor=1./3600.,
                           fig_size=(12, 7),
                           fig_name='img/FreeSurfaceTimeSeries')

        res_vnv_1_t3dgeo.close()
        res_vnv_1_t3dhyd.close()
