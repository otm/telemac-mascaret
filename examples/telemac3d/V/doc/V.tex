\chapter{Stability of vertical density in a V-shaped channel (V)}

\section{Description}
\bigskip
This test case presents the stability of the variable vertical 
density in a V-shaped channel. The purpose of this test is to verify the 
validity of the diffusion step and the proper treatment of the buoyancy terms. 
Moreover, this test demonstrates the ability of \telemac{3D} 
to model a vertical stratification induced by an active tracer 
distribution on a non-horizontal topography. A closed rectangular channel 
is initialised with such vertical tracer distribution without motion. 
This stratification is stable and the distribution of the tracer should 
not evolve in time neither generate any flow. The test case is treated
 with both prism and tetrahedron elements.

\bigskip
The considered domain is a horizontal V-shaped channel. 
The geometry dimensions of the channel are 100~m wide and 500~m long. 
The depth is -13~m in the center of the channel, and 0~m on both sides of the channel. 
The bottom is interpolated linearly between the centre and the banks 
(observed in Figure \ref{t3d:V:fig:meshV}).  The horizontal mesh is composed of triangular 
cells nearly homogenous in size. The test case is first solved using 
a vertical mesh of prismatic cells fitting the topography, and in 
a second time using tetrahedron elements. The horizontal and vertical meshes 
are presented in Figures \ref{t3d:V:fig:meshV} and \ref{t3d:V:fig:meshH}.\\
The active tracer is the temperature with a corresponding value of the thermal 
expansion coefficient $\beta = 2.10^{-4}\text{K}^{-1}$ (see the User's Manual). 
Note that only the horizontal diffusivity is taken into account. It is constant 
with velocity diffusivity and tracer diffusion equal to $1~\text{m}^2\cdot\text{s}^{-1}$.

\subsection{Initial and boundary conditions}
\bigskip
The initial water level is $z = 0.1~\text{m}$ with a null velocity.
The temperature is initialised as follows: 
\begin{equation*}
T (^\circ\text{C}) = 10 + \displaystyle\frac{z}{1.3}.
\end{equation*}

\bigskip
The boundary conditions are:
\begin{itemize}
\item All lateral boundaries are solid, i.e., no inlet and no outlet in the domain,
\item No flux through the bottom and the free surface for the temperature (or tracer),
\item No friction on the bottom and on the channel banks.
\end{itemize}

\subsection{Mesh and numerical parameters}
\bigskip
The mesh (see Figures \ref{t3d:V:fig:meshV} and \ref{t3d:V:fig:meshH}) 
is composed of 648 triangular elements (373 nodes) with 11 planes regularly 
spaced in the vertical direction, to form prism or tetrahedron elements.

\bigskip
The time step is 0.1~s for a simulated period of 1~s.

\bigskip
This case is computed with the non-hydrostatic version.
%To solve the advection, 
%the explicit Leo Postma scheme (scheme 3) is used for the velocities and also the tracer.

\begin{figure}[!htbp]
 \centering
 \includegraphicsmaybe{[width=0.5\textwidth]}{../img/MeshV.png}
 \caption{Topography and vertical mesh.}
 \label{t3d:V:fig:meshV}
\end{figure}
\begin{figure}[!htbp]
 \centering
 \includegraphicsmaybe{[width=\textwidth]}{../img/Mesh.png}
 \caption{Horizontal mesh.}
 \label{t3d:V:fig:meshH}
\end{figure}

\section{Results}
\bigskip
During the simulation, the vertical profile of tracer concentration is stable 
for both computations. The first computed with prism elements (Figure \ref{t3d:V:fig:prismTr})
 and the second computed with tetrahedron elements (Figure \ref{t3d:V:fig:tetraTr}).\\
However, at the end of the computation with prism elements, some negligible differences 
on the temperature field (maximum value for example, up to 0.06 \%) are observed. %0.06224953 \%
Figure \ref{t3d:V:fig:prismVelo} shows disturbance of velocity field negligible 
(of the order of 10$^{-8}$) for the prism elements computation. This velocity field is connected 
to the difficulty to build the diffusion matrix using prismatic elements. % 1.083e-8 -1.521e-8
The computation with tetrahedron elements (see Figure \ref{t3d:V:fig:tetraVelo}) shows 
some very negligible (of the order of 10$^{-16}$) disturbance of the velocity field. %5.526e-17 -4.474e-17
In any case, the situation can be considered as globally stable. The test is done essentially 
to verify the horizontal diffusion terms, which are estimated in the transformed $\sigma$-mesh 
(buoyancy terms). In this case, the tracer does not induce any flow. 
The final mass balance of computation using prism elements exposes a very good mass conservation 
(the mass loss is less than 10$^{-9}$ for the water and 10$^{-3}$ for the temperature). %water 0.1164153E-09 T 0.8483063E-03
For the computation using tetrahedron elements, the conservation of mass and temperature is perfect.

For prismatic elements, mass conservation is improved (up to machine precision
for water depth) by using default value for keyword
\telkey{TREATMENT OF NEGATIVE DEPTHS} (= 2), contrary to choice 1 for this
example until 9.0.

\begin{figure}[!htbp]
\begin{minipage}[t]{0.50\textwidth}
 \centering
\includegraphicsmaybe{[width=\textwidth]}{../img/init_T_prism.png}
\end{minipage}
\begin{minipage}[t]{0.50\textwidth}
 \centering
\includegraphicsmaybe{[width=\textwidth]}{../img/end_T_prism.png}
\end{minipage}
 \caption{Vertical distribution of tracer with prism elements (initial state and end of computation).}
 \label{t3d:V:fig:prismTr}
\end{figure}

\begin{figure}[!htbp]
\begin{minipage}[t]{0.50\textwidth}
 \centering
\includegraphicsmaybe{[width=\textwidth]}{../img/init_T_tetra.png}
\end{minipage}
\begin{minipage}[t]{0.50\textwidth}
 \centering
\includegraphicsmaybe{[width=\textwidth]}{../img/end_T_tetra.png}
\end{minipage}
 \caption{Vertical distribution of tracer with tetrahedron elements (initial state and end of computation).}
 \label{t3d:V:fig:tetraTr}
\end{figure}


\begin{figure}[!htbp]
\begin{minipage}[t]{0.50\textwidth}
 \centering
\includegraphicsmaybe{[width=\textwidth]}{../img/init_V_prism.png}
\end{minipage}
\begin{minipage}[t]{0.50\textwidth}
 \centering
\includegraphicsmaybe{[width=\textwidth]}{../img/end_V_prism.png}
\end{minipage}
 \caption{Vertical velocities with prism elements (initial state and end of computation).}
 \label{t3d:V:fig:prismVelo}
\end{figure}

\begin{figure}[!htbp]
\begin{minipage}[t]{0.50\textwidth}
 \centering
\includegraphicsmaybe{[width=\textwidth]}{../img/init_V_tetra.png}
\end{minipage}
\begin{minipage}[t]{0.50\textwidth}
 \centering
\includegraphicsmaybe{[width=\textwidth]}{../img/end_V_tetra.png}
\end{minipage}
 \caption{Vertical velocities with tetrahedron elements (initial state and end of computation).}
 \label{t3d:V:fig:tetraVelo}
\end{figure}

%\section{Conclusion}

\bigskip
To conclude, the diffusion equation of tracers is properly solved by \telemac{3D}.
The buoyancy terms are properly taken into account for a linear vertical tracer distribution.
Unlike tetrahedron elements computation, using prism elements generates a small disturbance
of the velocity field and some negligible differences on the temperature field. Nevertheless, 
the generated disturbance is small with respect to the vertical gradient of the tracer and is 
principally due to machine precision and diffusion matrix treatment. The tracer is still 
stable at the end of the simulation.

