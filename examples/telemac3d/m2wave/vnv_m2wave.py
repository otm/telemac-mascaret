
"""
Validation script for m2wave
"""
from vvytel.vnv_study import AbstractVnvStudy
from execution.telemac_cas import TelemacCas, get_dico
from data_manip.extraction.telemac_file import TelemacFile
from utils.exceptions import TelemacException

class VnvStudy(AbstractVnvStudy):
    """
    Class for validation of m2wave
    """

    def _init(self):
        """
        Defines the general parameter
        """
        self.rank = 2
        self.tags = ['telemac3d']

    def _pre(self):
        """
        Defining the studies
        """
        # scalar mode
        self.add_study('vnv_1',
                       'telemac3d',
                       't3d_m2wave.cas')
        # parallel mode
        cas = TelemacCas('t3d_m2wave.cas', get_dico('telemac3d'))
        cas.set('PARALLEL PROCESSORS', 4)

        self.add_study('vnv_2',
                       'telemac3d',
                       't3d_m2wave_par.cas',
                       cas=cas)
        del cas
        # scalar mode
        self.add_study('vnv_3',
                       'telemac3d',
                       't3d_m2waveCas.cas')
        # parallel mode
        cas = TelemacCas('t3d_m2waveCas.cas', get_dico('telemac3d'))
        cas.set('PARALLEL PROCESSORS', 4)

        self.add_study('vnv_4',
                       'telemac3d',
                       't3d_m2waveCas_par.cas',
                       cas=cas)
        del cas


    def _check_results(self):
        """
        Check epsilons
        """
        import numpy as np
        from os import path
        # Comparison with the last time frame of the reference file.
        self.check_epsilons('vnv_1:T3DHYD',
                            'f2d_m2wave.slf',
                            eps=[1.e-8])

        # Comparison with the last time frame of the reference file.
        self.check_epsilons('vnv_2:T3DHYD',
                            'f2d_m2wave.slf',
                            eps=[1.e-8])

        # Comparison between sequential and parallel run.
        self.check_epsilons('vnv_1:T3DHYD',
                            'vnv_2:T3DHYD',
                            eps=[1.e-8])

        # Comparison with the last time frame of the reference file.
        self.check_epsilons('vnv_1:T3DRES',
                            'f3d_m2wave.slf',
                            eps=[1.e-8])

        # Comparison with the last time frame of the reference file.
        self.check_epsilons('vnv_2:T3DRES',
                            'f3d_m2wave.slf',
                            eps=[1.e-8])

        # Comparison between sequential and parallel run.
        self.check_epsilons('vnv_1:T3DRES',
                            'vnv_2:T3DRES',
                            eps=[1.e-8])
                            
        # Comparison with the last time frame of the reference file.
        self.check_epsilons('vnv_3:T3DHYD',
                            'f2d_m2wave.slf',
                            eps=[1.e-8])

        # Comparison with the last time frame of the reference file.
        self.check_epsilons('vnv_4:T3DHYD',
                            'f2d_m2wave.slf',
                            eps=[1.e-8])

        # Comparison between sequential and parallel run.
        self.check_epsilons('vnv_3:T3DHYD',
                            'vnv_4:T3DHYD',
                            eps=[1.e-8])

        # Comparison with the last time frame of the reference file.
        self.check_epsilons('vnv_3:T3DRES',
                            'f3d_m2wave.slf',
                            eps=[1.e-8])

        # Comparison with the last time frame of the reference file.
        self.check_epsilons('vnv_4:T3DRES',
                            'f3d_m2wave.slf',
                            eps=[1.e-8])

        # Comparison between sequential and parallel run.
        self.check_epsilons('vnv_3:T3DRES',
                            'vnv_4:T3DRES',
                            eps=[1.e-8])                            

        # Comparison between Fortran and steering file defined layering
        self.check_epsilons('vnv_1:T3DHYD',
                            'vnv_3:T3DHYD',
                            eps=[1.e-15])

        # Comparison between Fortran and steering file defined layering
        self.check_epsilons('vnv_2:T3DHYD',
                            'vnv_4:T3DHYD',
                            eps=[1.e-15])
 
        # Comparison between Fortran and steering file defined layering
        self.check_epsilons('vnv_1:T3DRES',
                            'vnv_3:T3DRES',
                            eps=[1.e-15])

        # Comparison between Fortran and steering file defined layering
        self.check_epsilons('vnv_2:T3DRES',
                            'vnv_4:T3DRES',
                            eps=[1.e-15])


    def _post(self):
        """
        Post-treatment processes
        """
        from postel.plot_vnv import vnv_plot2d
        # Getting files
        res_file = self.get_study_file('vnv_1:T3DHYD')
        res = TelemacFile(res_file)

        # Plot mesh:
        vnv_plot2d(\
            '', res,
            fig_size=(6, 5),
            record=-1,
            x_factor=1/10000.,
            y_factor=1/10000.,
            x_label='$x/10^4$',
            y_label='$y/10^4$',
            fig_name='img/Mesh',
            plot_mesh=True)

        # Plot bottom:
        vnv_plot2d(\
            'BOTTOM', res,
            fig_size=(8, 6),
            record=-1,
            cbar_label='Bottom elevation (m)',
            x_factor=1/10000.,
            y_factor=1/10000.,
            x_label='$x/10^4$',
            y_label='$y/10^4$',
            fig_name='img/Bathy',
            filled_contours=True)

        # Closing files
        res.close()

