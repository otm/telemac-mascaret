!                   **********************
                    SUBROUTINE USER_BORD3D
!                   **********************
!
!
!***********************************************************************
! TELEMAC3D   V7P3
!***********************************************************************
!
!brief    SPECIFIC BOUNDARY CONDITIONS NOT IMPLEMENTED IN USUAL BORD3D.
!
!warning  MAY BE MODIFIED BY THE USER
!
!history  C.-T. PHAM (LNHE)
!+        04/04/2017
!+        V7P3
!+   Creation from BORD3D and 4 examples of TELEMAC-3D:
!+   stratification, tetra, NonLinearWave and Viollet
!+   Prescribed stratification over the vertical along a liquid boundary
!+   for stratification and tetra cases
!+   Specific boundary conditions for H, U, V and P for lateral
!+   boundaries for NonLinearWave case
!+   Specific boundary conditions for U, V, temp, epsilon for Viollet
!+
!~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
!~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
!
      USE BIEF
      USE DECLARATIONS_TELEMAC
      USE DECLARATIONS_TELEMAC3D, ONLY: HBOR, NPTFR2, T3D_FILES, T3DFO1,
     & BOUNDARY_COLOUR,ZF, AT, LT,MESH2D
!      USE INTERFACE_TELEMAC3D
!
      USE DECLARATIONS_SPECIAL
      IMPLICIT NONE
!
!
!+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
!
!
!  TABLEAUX ISSUS DU FICHIER 26 , DONNANT LES CONDITIONS AUX LIMITES
!
      DOUBLE PRECISION HB(77),PHASEB(77),HC(77),PHASEC(77)
!
      DOUBLE PRECISION PI,AN,ARG,T,W
!
      INTEGER NBORL(77),NPTFRL,I,KK,ID,K
      INTEGER, POINTER :: NBOR(:)
!
!  TABLEAUX DE DONNEES TEMPORELLES
!
      SAVE HB,PHASEB,PHASEC,NBORL,HC
!
!-----------------------------------------------------------------------
!
!  LECTURE SUR LE FICHIER 26 DE HB ET PHASEB, CALCULES
!  PAR INTERPOLATION SUR CHAQUE POINT FRONTIERE
!
      NBOR=>MESH2D%NBOR%I
      PI = 4.D0*ATAN(1.D0)
      NPTFRL = 77
      IF(LT.EQ.0) THEN
        DO K = 1, NPTFR2
          HBOR%R(K) = 1.D0
        ENDDO
      ENDIF
      IF(ABS(AT-150.D0).LT.1.D-5) THEN
        ID = T3D_FILES(T3DFO1)%LU
        REWIND ID
        DO K= 1, NPTFRL
          READ(ID,*) I,HB(K),PHASEB(K),HC(K),PHASEC(K)
          PHASEB(K) = PI/180.D0*PHASEB(K)
          PHASEC(K) = PI/180.D0*PHASEC(K)
          NBORL(K) = I
        ENDDO
      ENDIF
!
      T = 44714.D0
      W = 2.D0*PI/T
!
      DO K= 1 , NPTFRL
        ARG = MOD (W*AT - PHASEB(K),2.D0*PI)
        AN  = HB(K) * COS(ARG)
        ARG = MOD (2.D0*W*AT - PHASEC(K),2.D0*PI)
        AN  = AN + HC(K) * COS(ARG)
        IF (AT.LT.2500.D0) AN = AN*0.0004D0*AT
        IF(NCSIZE.GT.0) THEN
          DO KK=1,NPTFR2
            IF(BOUNDARY_COLOUR%I(KK).EQ.NBORL(K)) THEN
              HBOR%R(KK) = AN-ZF%R(NBOR(KK))
            ENDIF
          ENDDO
        ELSE
          HBOR%R(NBORL(K)) = AN-ZF%R(NBOR(NBORL(K)))
        ENDIF
      ENDDO
!
!-----------------------------------------------------------------------
!
      RETURN
      END
