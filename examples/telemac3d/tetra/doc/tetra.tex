\chapter{Discretisation with tetrahedra elements (tetra)}

\section{Purpose}

This test demonstrates the ability of \telemac{3D} to be discretised
with prisms split into tetrahedra.

\section{Description}

The configuration is a frictionless channel presenting an idealised bump on the
bottom.
The channel is horizontal with a 4~m long bump in its middle.
The maximum elevation of the bump is 20~cm.
The flow regime is sub-critical.
It is the same geometry as the \telemac{2D} test case ``bumflu''.

The tracer used is salinity (considered as active with the 2nd density law).

The bottom elevation is defined by (see Figure \ref{t3d:tetra:fig:Bottom}):
\begin{equation*}
\left\{
\begin{array}{rl}
  \textrm{if } x \ge 2.5, & z = \max \left(-0.0246875(x-10)^2, -0.3 \right)\\
  \textrm{if } x < 2.5, & z = \max \left(-0.2-0.3(\frac{x}{2.5})^2, -0.3 \right)\\
\end{array}
\right.
\end{equation*}

%If 6.51~m < $x$ < 13.49~m, $z_{\rm{f}} = -0.0246875(x-10)^2$\\
%If $x$ < 1.44~m, $z_{\rm{f}} = -0.2-0.3(\frac{x}{2.5})^2$\\
%$z_{\rm{f}}$ = -0.3~m elsewhere

%The solution produced by \telemac{3D} is compared to the analytical solution to
%this problem.

\begin{figure}[!htbp]
 \centering
 \includegraphicsmaybe{[width=\textwidth]}{../img/Bottom.png}
 \caption{Bottom elevation.}
 \label{t3d:tetra:fig:Bottom}
\end{figure}

\begin{figure}[!htbp]
 \centering
 \includegraphicsmaybe{[width=\textwidth]}{../img/Profil.png}
 \caption{Bump profile.}
 \label{t3d:tetra:fig:Profil}
\end{figure}

\subsection{Initial and Boundary Conditions}

The computation is initialised with a constant elevation equal to 1.8~m
and no velocity.
The initial value of salinity is 30~g/L everywhere.

The boundary conditions are:
\begin{itemize}
\item For the solid walls, a slip condition on channel banks is used for the
velocities,
\item On the bottom, a Strickler law with friction coefficient equal to
30~m$^{1/3}$/s is prescribed,
\item Upstream a flowrate and salinity are prescribed.
Flowrate is equal to 4~m$^3$/s, linearly increasing from 0 to 4~m$^3$/s during
the first 10~s.
Imposed salinity is equal to 40~g/L if $z \leq$ -0.2~m or 30~g/L if $z$ > -0.2~m,
it is directly prescribed through a user-defined tracer vertical profile
(\telkey{TRACERS VERTICAL PROFILES} = 0) by implementing
\telfile{USER\_TRA\_PROF\_Z} subroutine,
\item Downstream the water level is equal to 1.8~m (= initial elevation),
so that the water depth is 2~m.
\end{itemize}

\subsection{Mesh and numerical parameters}

The 2D mesh (Figure \ref{t3d:tetra:fig:meshH})
is made of 2,620 triangular elements (1,452 nodes).

10 planes are regularly spaced in the vertical direction except one fixed
plane with the elevation -0.2~m (plane number 4),
see Figure \ref{t3d:tetra:fig:meshV}.

\begin{figure}[!htbp]
 \centering
 \includegraphicsmaybe{[width=\textwidth]}{../img/MeshH.png}
 \caption{Horizontal mesh.}
 \label{t3d:tetra:fig:meshH}
\end{figure}

\begin{figure}[!htbp]
 \centering
 \includegraphicsmaybe{[width=\textwidth]}{../img/MeshV.png}
 \caption{Initial vertical mesh along $y$ = 1~m.}
 \label{t3d:tetra:fig:meshV}
\end{figure}

Tetrahedra elements (split prisms) are chosen for discretisation.

The non-hydrostatic version of \telemac{3D} is used.

To solve the advection, the edge by edge explicit finite volume Leo Postma
(scheme \#13) is used for every variable (velocities, salinity, $k$ and
$\epsilon$).

The time step is 0.04~s for a simulated period of 40~s.

\subsection{Physical parameters}

Turbulence is modelled with the $k-\epsilon$ model of \telemac{3D}.

\section{Results}

\begin{figure}[H]
  \centering
  \includegraphicsmaybe{[width=\textwidth]}{../img/FreeSurface.png}
  \caption{Free surface at final time step.}
  \label{t3d:tetra:FreeSurf}
\end{figure}

\begin{figure}[H]
  \centering
  \includegraphicsmaybe{[width=\textwidth]}{../img/Velocity.png}
  \caption{Velocity magnitude at the surface at final time step.}
  \label{t3d:tetra:Velo}
\end{figure}

\begin{figure}[H]
  \centering
  \includegraphicsmaybe{[width=\textwidth]}{../img/VelocityV.png}
  \caption{Vertical distribution of velocity magnitude at the surface at final time step.}
  \label{t3d:tetra:VeloV}
\end{figure}

Until release 9.0, for this example, the following keywords were set to:
\begin{itemize}
\item \telkey{TIDAL FLATS} = NO,
\item \telkey{TREATMENT OF NEGATIVE DEPTHS} = 1 (old default value until
release 8.5).
\end{itemize}

Setting them to their default values improves the mass conservation of water
for this example.
\\

Moreover, without increasing CPU times, it is possible to decrease the
\telkey{ACCURACY FOR PPE} from 10$^{-5}$ to 10$^{-6}$ by changing
\telkey{SOLVER FOR PPE} from default one GMRES (choice = 7) to conjugate
gradient (choice = 1) AND using also \telkey{PRECONDITIONING FOR PPE}
= 34 (2 times 17).
With that combo, CPU time is not so different from using
\telkey{ACCURACY FOR PPE} = 1.E-5 and default \telkey{SOLVER FOR PPE} = 7
(aka GMRES).

\section{Conclusion}

\telemac{3D} is able to be discretised with prisms split into tetrahedra.
