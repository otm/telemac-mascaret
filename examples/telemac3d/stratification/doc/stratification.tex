\chapter{Stability of a stratified flow (stratification)}

\section{Description}
\bigskip
This test is a flat channel with saline stratification.
It demonstrates the ability of \telemac{3D} to model stratified
flow with a special focus on the stability of the stratification.
This case also demonstrates the capacity of the $k$-$\epsilon$ model
to reproduce turbulent phenomena.

\bigskip
The domain is a rectangular channel with 2,000~m long and 100~m wide 
(see Figure \ref{t3d:stratification:fig:bathy}). The bottom of 
this channel has a mild slope (0~m at the entrance, -0.019~m at the output).
The general water depth is 10~m and the constant velocity 
along the channel is imposed. Salinity (or tracer) is prescribed 
as shown in Figure \ref{t3d:stratification:fig:salInit}.
Density law depends then on salinity ($S_{al}$):
\begin{equation*}
\rho = \rho_{ref} (1 + 750.10^{-6} . S_{al} ),
\rm{with }~\rho_{ref} = 999.972~\rm{kg.m}^{-3}
\end{equation*}
\\
Note that the turbulent viscosity is constant in horizontal direction
and equal to $0.1~\text{m}^2.\text{s}^{-1}$. The $k$-$\epsilon$~model 
is only used in the vertical direction. 

\begin{figure}[!htbp]
 \centering
 \includegraphicsmaybe{[width=\textwidth]}{../img/bathy.png}
 \caption{Bottom topography.}
 \label{t3d:stratification:fig:bathy}
\end{figure}
\begin{figure}[!htbp]
 \centering
 \includegraphicsmaybe{[width=\textwidth]}{../img/Sal_ini.png}
 \caption{Initial salinity along the channel.}
 \label{t3d:stratification:fig:salInit}
\end{figure}

\subsection{Initial and Boundary Conditions}
\bigskip
The initial water depth is 10~m with a constant longitudinal 
velocity equal to 1~m.s$^{-1}$.\\
The tracer (or salinity) is equal to 38~kg.m$^{-3}$ (g.L$^{-1}$)
at the bottom below the plane number 18 
(i.e. at 2.5~m of the water surface) and 28~kg.m$^{-3}$ 
at the top above the plane number 18 
(Figure \ref{t3d:stratification:fig:salInit}).
Note that the tracer and velocity field are directly initialised in the
\telfile{USER\_CONDI3D\_UVW} and \telfile{USER\_CONDI3D\_TRAC} subroutines.

\bigskip
The boundary conditions are:
\begin{itemize}
\item For the solid lateral walls, a slip condition 
on the channel banks is used for the velocity,
\item On the bottom, Strickler law with friction coefficient equal to 
70~$\text{m}^{1/3}.\text{s}^{-1}$ is imposed,
\item Upstream, a flow rate equal to 1,000~$\text{m}^{3}.\text{s}^{-1}$ is imposed
and a tracer equal to the initial repartition,
\item Downstream, the water depth is imposed at 9.981054~m.
\end{itemize}
%Note that the tracer and velocity field are directly prescribed on the liquid
%boundaries in the new (since V7P3) \telfile{USER\_BORD3D} subroutine.
Note that the tracer field is directly prescribed on the liquid boundaries
through a user-defined tracer vertical profile
(\telkey{TRACERS VERTICAL PROFILES} = 0) by implementing
\telfile{USER\_TRA\_PROF\_Z} subroutine.

\subsection{Mesh and numerical parameters}
\bigskip
The mesh (Figure \ref{t3d:stratification:fig:meshH} and \ref{t3d:stratification:fig:meshV})  
is composed of 2,204 triangular elements (1,188 nodes) with 21 planes  
regularly spaced on the vertical, to form prism elements.\\

\begin{figure}[!htbp]
 \centering
 \includegraphicsmaybe{[width=\textwidth]}{../img/Mesh.png}
 \caption{Horizontal mesh.}
 \label{t3d:stratification:fig:meshH}
\end{figure}
\begin{figure}[!htbp]
 \centering
 \includegraphicsmaybe{[width=\textwidth]}{../img/meshV.png}
 \caption{Vertical mesh.}
 \label{t3d:stratification:fig:meshV}
\end{figure}

\bigskip
The time step is 2~s for a simulated period of 2,000~s. 

\bigskip
This case is computed with the non-hydrostatic version.
To solve advection, the N-type MURD scheme (scheme 4)
is used for the velocities, $k$-$\epsilon$ model and the tracer (or salinity). 

\section{Results}

\bigskip
As expected, the vertical gradient of salinity remains well stable, see Figures
\ref{t3d:stratification:fig:salInit} and \ref{t3d:stratification:fig:sal2000}.

\bigskip
In Figure \ref{t3d:stratification:fig:evolTurb}, we can observe 
the turbulence phenomenon modelled by the $k$-$\epsilon$ model.
This turbulence is created at the bottom and is developing on the
vertical column of water.
\begin{figure}[!htbp]
 \centering
 \includegraphicsmaybe{[width=1\textwidth]}{../img/Sal_f.png}
 \caption{Final salinity along the channel at 2,000~s.}
 \label{t3d:stratification:fig:sal2000}
\end{figure}

However, the turbulence is clearly blocked by the saline stratification 
(see Figure \ref{t3d:stratification:fig:visco}).
\\

\begin{figure}[!htbp]
 \centering
 \includegraphicsmaybe{[width=1\textwidth]}{../img/turb.png}
 \caption{Evolution of turbulence on the vertical along the channel.}
 \label{t3d:stratification:fig:evolTurb}
\end{figure}

Until release 9.0, \telkey{TREATMENT OF NEGATIVE DEPTHS} was set to 1 for this
example (old default value until release 8.5).
Changing to 2 (new default value since release 9.0, i.e. flux control) improves
the mass conservation of water for this example (up to machine precision)
and also improves the mass conservation of tracer (2 orders of magnitude).
\\

There are no significant differences in the results if using the non-hydrostatic
version or not.

\section{Conclusion}

%\bigskip
\telemac{3D} is able to represent correctly stratified flows.
In addition, the $k$-$\epsilon$ model is able to simulate turbulence
generated by bottom friction.

\begin{figure}[!htbp]
 \centering
 \includegraphicsmaybe{[width=0.85\textwidth]}{./img/visco_profile.png}
 \caption{Viscosity profile along the vertical at 1,250~m.}
 \label{t3d:stratification:fig:visco}
\end{figure}
