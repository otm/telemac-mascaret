!                   **********************
                    SUBROUTINE USER_BORD3D
!                   **********************
!
!
!***********************************************************************
! TELEMAC3D
!***********************************************************************
!
!brief    SPECIFIC BOUNDARY CONDITIONS NOT IMPLEMENTED IN USUAL BORD3D.
!
!warning  MAY BE MODIFIED BY THE USER
!
!history  C.-T. PHAM (LNHE)
!+        04/04/2017
!+        V7P3
!+   Creation from BORD3D and 4 examples of TELEMAC-3D:
!+   stratification, tetra, NonLinearWave and Viollet
!+   Prescribed stratification over the vertical along a liquid boundary
!+   for stratification and tetra cases
!+   Specific boundary conditions for H, U, V and P for lateral
!+   boundaries for NonLinearWave case
!+   Specific boundary conditions for U, V, temp, epsilon for Viollet
!+
!~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
!~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
!
      USE BIEF
      USE DECLARATIONS_TELEMAC
      USE DECLARATIONS_TELEMAC3D
!
      USE DECLARATIONS_SPECIAL
      IMPLICIT NONE
!
!-----------------------------------------------------------------------
!
      INTEGER IBORD,IPLAN,I
      DOUBLE PRECISION PI,PER,L,HEIGHT,DEPTH,KK,OMEGA,ZZ,ZSURF,TPS
!
!-----------------------------------------------------------------------
!
      PI=4.D0*ATAN(1.D0)
      PER=1.01D0
      HEIGHT=0.041D0
      DEPTH=0.43D0
      OMEGA=2.D0*PI/PER
      L=1.D0
      DO I=1,100
        L=GRAV*PER**2/2.D0/PI*TANH(2.D0*PI*DEPTH/L)
      ENDDO
      KK=2.D0*PI/L
!     TPS=AT+PER/2.D0
      TPS=AT
!
      DO I=1,NPTFR2
!
        IF(LIHBOR%I(I).EQ.KENT.OR.LIUBOL%I(I).EQ.KENTU) THEN
!
          HBOR%R(I)=-ZF%R(NBOR2%I(I))
     &             +(HEIGHT*COS(OMEGA*TPS)/2.D0
     &    +(KK*HEIGHT**2/16.D0)*(COSH(KK*DEPTH)/SINH(KK*DEPTH)**3)
     &    *(2.D0+COSH(2.D0*KK*DEPTH))*COS(2.D0*OMEGA*TPS))*
!         RAMPE DE 1 S SUR LE TEMPS
     &    MIN(1.D0,AT)
!
          DO  IPLAN=1, NPLAN
            IBORD = (IPLAN-1)*NPTFR2 + I
            ZSURF=Z(NBOR3%I((NPLAN-1)*NPTFR2 + I))
            ZZ=Z(NBOR3%I(IBORD))-ZSURF
            UBORL%R(IBORD)=OMEGA*COS(OMEGA*TPS)*HEIGHT/
     &             2.D0*COSH(KK*(ZZ+DEPTH))/SINH(KK*DEPTH)
     & +3.D0/16.D0*OMEGA*KK*HEIGHT**2*COSH(2.D0*KK*(ZZ+DEPTH))/
     &      SINH(KK*DEPTH)**4*COS(2.D0*OMEGA*TPS)
            VBORL%R(IBORD)=0.D0
            PBORL%R(IBORD)=DT*GRAV*HEIGHT*COS(OMEGA*TPS)/2.D0*
     &                 (COSH(KK*(ZZ+DEPTH))/COSH(KK*DEPTH)-1.D0)
!
            UBORL%R(IBORD)=UBORL%R(IBORD)*MIN(1.D0,AT)
            VBORL%R(IBORD)=VBORL%R(IBORD)*MIN(1.D0,AT)
            PBORL%R(IBORD)=PBORL%R(IBORD)*MIN(1.D0,AT)
          ENDDO
        ENDIF
      ENDDO
!
!-----------------------------------------------------------------------
!
      RETURN
      END
