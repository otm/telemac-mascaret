\chapter{Uncovering of a beach (vasque)}

\section{Purpose}

This test demonstrates the ability of \telemac{3D} to model water
retention in a bowled beach during ebbing tide.

\section{Description}

A beach profile presenting a bowl is considered.
Its extent is a channel of size 46~m $\times$ 9~m.
The water level is initially at high tide level,
covering the entire domain (see Figure \ref{fig:vasque:FSinit}).

\begin{figure}[H]
 \centering
 \includegraphicsmaybe{[width=.7\textwidth]}{../img/FreeSurfaceInit.png}
  \caption{Initial free surface elevation and bottom elevation.}\label{fig:vasque:FSinit}
\end{figure}

The ebbing tide is simulated with the seaward final water level below
the beach bowl.

\subsection{Geometry and mesh}

The bathymetry is a beach profile starting at $z$ = -0.14~m, presenting
a bowl, and ending at $z$ = -0.6~m.
It is defined by $z = -0.6+0.01x+0.1e^{\frac{-(x-19)^2}{20}}$,
see Figure \ref{fig:vasque:Bottom}.

\begin{figure}[H]
 \centering
 \includegraphicsmaybe{[width=.7\textwidth]}{../img/Bottom.png}
  \caption{Bathymetry.}\label{fig:vasque:Bottom}
\end{figure}

The bowled beach profile is specified in the \telkey{USER\_T3D\_CORFON}
subroutine.\\

The mesh is regular. It is made of squares split into two triangles
(see Figure \ref{fig:vasque:MeshH}) with the following characteristics:
\begin{itemize}
\item 828 triangular elements,
\item 470 nodes,
\item Maximum size range: $\sqrt{2}$~m.
\end{itemize}

\begin{figure}[H]
 \centering
 \includegraphicsmaybe{[width=.7\textwidth]}{../img/MeshH.png}
  \caption{Horizontal mesh.}\label{fig:vasque:MeshH}
\end{figure}

10 planes are regularly spaced on the vertical (see Figure \ref{fig:vasque:MeshV}
for the initial distribution).

\begin{figure}[H]
 \centering
 \includegraphicsmaybe{[width=.7\textwidth]}{../img/MeshV.png}
  \caption{Vertical mesh at initial time step.}\label{fig:vasque:MeshV}
\end{figure}

\subsection{Physical parameters}

A constant viscosity of 0.1~m$^2$/s is used in both directions.
A Strickler law with coefficient equal to 40~m$^{1/3}$/s is used to model bottom
friction.

\subsection{Initial and boundary conditions}

Constant free surface level at $z$ = 0~m corresponding to high tide
and no velocity are used as initial conditions.

The boundary conditions are:
\begin{itemize}
\item Shoreline: solid wall with slip condition (closed boundaries on sides and landward),
%\item Offshore boundary: $Q = -\frac{5 H}{0.6}$ imposed,
\item Seaward boundary controlling decreasing water depth (ebbing tide) with
\telkey{USER\_SL3} function:
$z = 0.275\left( \cos(\frac{2\pi}{600}t) -1 \right)$ is imposed,
\item Lateral boundaries: solid walls with slip condition in the channel.
\end{itemize}

\subsection{Numerical parameters}

The non-hydrostatic version of \telemac{3D} is used.
To solve the advection, the method of characteristics (scheme \#1) is used for
velocities.

The conjugate gradient (option 1) combined with preconditioning = 34
= 2 $\times$ 17 is used for solving PPE as more efficient than GMRES (option 7)
or the former choice CGSTAB (option 6) for this example.

The time step is 0.5~s for a simulated period of 300~s.

\section{Results}

Figures \ref{fig:vasque:FreeSurfacet0} and \ref{fig:vasque:FreeSurfacetf}
present longitudinal cross profiles of water level at
initial and final times of the simulation.
The water level decreases regularly over the beach.
At final time, the bowl is filled of water and the water level is completely
horizontal, while the seaward water level is below the bowl position
(see Figures \ref{fig:vasque:FreeSurfacetf}
and \ref{fig:vasque:FreeSurface}).

\begin{figure}[H]
 \centering
% \includegraphicsmaybe{[width=.7\textwidth]}{../img/FreeSurface_tf.png}
 \includegraphicsmaybe{[width=.7\textwidth]}{../img/FreeSurfaceInit.png}
  \caption{Free surface elevation at initial time.}\label{fig:vasque:FreeSurfacet0}
\end{figure}

\begin{figure}[H]
 \centering
 \includegraphicsmaybe{[width=.7\textwidth]}{../img/FreeSurface.png}
  \caption{Free surface elevation at final time (= 300~s).}\label{fig:vasque:FreeSurfacetf}
\end{figure}

\begin{figure}[H]
 \centering
 \includegraphicsmaybe{[width=.7\textwidth]}{../img/FreeSurface_Y5.png}
  \caption{Evolution of the free surface elevation in time.}\label{fig:vasque:FreeSurface}
\end{figure}

If using \telkey{TREATMENT OF NEGATIVE DEPTHS} = 2 (default choice since release
9.0), there is no negative water depths during the computation contrary to the
previous old default choice (= 1) for which there were a few nodes with negative
water depths.

\section{Conclusion}

\telemac{3D} is capable to model water retention in bathymetry bowls.
