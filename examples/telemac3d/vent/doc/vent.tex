\chapter{Flow in a channel submitted to wind (vent)}

\section{Description}

\bigskip
This test, a rectangular channel submitted to a wind,
demonstrates the availability of \telemac{3D} to represent the currents
induced by wind blowing at the surface of a closed channel.
More precisely, this case allows verifying that a linear decrease of the mixing
length model at the bottom and at the surface is able to reproduce such circulation.
Moreover, this test allows verifying the proper implementation of the sources terms
and external forces as the wind.

\bigskip
The geometry dimensions of rectangular channel is 500~m long and 100~m wide,
 with horizontal bed at depth -10~m. At the initial state, the channel is submitted
 to a constant 10~$\text{m}\cdot\text{s}^{-1}$ wind. \\
The wind generates a slope of the free surface and a vertical two-dimensional circulation.
Tsanis \cite{Tsanis1989} has made an inventory of existing laboratory or in-situ
measurements and has plotted these values on a non-dimensional graph.
He deduced a characteristic vertical velocity profile
(Figure \ref{t3d:vent:fig:mixinglength}), which will be compared to
the numerical results of this test case.
Tsanis mixing length turbulence model is so used for the vertical turbulence model.

The turbulent viscosity has the following expression:
\begin{equation*}
\nu_{\rm{t}} = l^2 \sqrt{\frac{1}{2}\left[ \frac{\partial u_i}{\partial x_j}
 + \frac{\partial u_j}{\partial x_i} \right]^2},
\end{equation*}
where $l$ is mixing length and $h$ the water depth.\\
On the other hand, the horizontal viscosity for velocity is constant and equal
to 0.1~$\text{m}^2\cdot\text{s}^{-1}$.

\begin{figure}[!htbp]
 \centering
 \includegraphicsmaybe{[width=0.5\textwidth]}{./img/mixinglength.png}
 \caption{Characteristic vertical velocity profile,
 where $K$ is Karman constant equal to 0.40.}
 \label{t3d:vent:fig:mixinglength}
\end{figure}

\bigskip
Wind induces a wind stress ($\vec{\tau}$) at the free surface of channel.
The wind stress is written as follows:
\begin{equation*}
\vec{\tau}=\rho_{air}K_{\rm{F}} \vec w |\vec w|,
\end{equation*}
With:
\begin{equation*}
\vec w = 10~\text{m}\cdot\text{s}^{-1}\rm{~Velocity~of~wind},
\end{equation*}
\begin{equation*}
K_{\rm{F}} = \frac{(-0.12 + 0.137 \| \vec w \| )}{1000}.
\end{equation*}

\subsection{Initial and Boundary Conditions}

\bigskip
The initial water level is 0~m and the velocity is null.

\bigskip
The boundary conditions are:
\begin{itemize}
\item For the solid walls, a slip condition on the channel banks is used
for the velocity,
\item On the bottom, a friction stress due to mixing length is taken into account.
\end{itemize}
For the wind, the conditions are :
\begin{itemize}
\item A wind velocity equal to 10 $\text{m}\cdot\text{s}^{-1}$,
\item The coefficient of wind influence is $a_{wind} \frac{\rho_{air}}
{\rho_{water}} = 1.625 \cdot 10^{-6}$ with $\rho_{air}$, $\rho_{water}$
which are respectively the air density and the water density and
with $a_{wind}$ an addimentional coefficient,
\item Shear stress wind is taken account on the surface water.
\end{itemize}

\subsection{Mesh and numerical parameters}
\bigskip
The mesh (Figures \ref{t3d:vent:fig:meshH}, \ref{t3d:vent:fig:meshV} and
\ref{t3d:vent:fig:meshV_Zoom})  is composed of 543 triangular elements
(315 nodes) with 15 planes  irregularly spaced on the vertical, to form prism elements.

Note that the quality of this mesh can be improved by changing overconstrained
triangles at each corner.

\begin{figure}[!htbp]
 \centering
 \includegraphicsmaybe{[width=\textwidth]}{../img/Mesh.png}
 \caption{Horizontal mesh.}
 \label{t3d:vent:fig:meshH}
\end{figure}

\begin{figure}[!htbp]
 \centering
 \includegraphicsmaybe{[width=\textwidth]}{../img/MeshV.png}
 \caption{Full vertical mesh.}
 \label{t3d:vent:fig:meshV}
 \end{figure}

 \begin{figure}[!htbp]
 \centering
 \includegraphicsmaybe{[width=\textwidth]}{../img/MeshV_zoom.png}
 \caption{Zoom on vertical mesh close to the surface.}
  \label{t3d:vent:fig:meshV_Zoom}
  \end{figure}

\bigskip
The time step is 10~s for a simulated period of 20,000~s (5~h 33~min 20~s).

\bigskip
This case is computed with the non-hydrostatic pressure hypothesis.
To solve advection, the LIPS scheme is used for the velocities
and Diagonal preconditioning is used for propagation (default option).

The resolution accuracy is $10^{-8}$ for the diffusivity velocity (default
value), so is for PPE whereas it is set to $10^{-15}$ for the propagation
to have less differences between sequential and parallel runs.
To solve the Poisson Pressure equation, conjugate gradient combined with
a preconditioning equal to 34 are used as more efficient than GMRES.

The implicitation coefficients for depth and velocities are equal to 0.6.
\\

Mass conservation is improved (up to machine precision) by using default value
for the keyword \telkey{TREATMENT OF NEGATIVE DEPTHS} (= 2), instead of the old
default value = 1 (smoothings) for this example (until release 9.0).

\section{Results}
\bigskip
The mass balance is the following:
\begin{lstlisting}[language=TelFortran]
   INITIAL VOLUME                      :     500000.0
   FINAL VOLUME                        :     500000.0
   VOLUME EXITING (BOUNDARY OR SOURCE) :     0.000000
   TOTAL VOLUME LOST                   :   -0.2910383E-09
\end{lstlisting}

\bigskip
Thus, the mass balance is perfect (improved from release 9.0 by using
default choice for \telkey{TREATMENT OF NEGATIVE DEPTHS} = 2).

Figure \ref{t3d:vent:fig:vecVelo} shows the vertical circulation induced by the wind.
\begin{figure}[!htbp]
 \centering
 \includegraphicsmaybe{[width=\textwidth]}{../img/vecVelo.png}
 \caption{The vertical circulation induced by the wind.}
 \label{t3d:vent:fig:vecVelo}
 \end{figure}
 \begin{figure}[!htbp]
 \centering
 \includegraphicsmaybe{[width=\textwidth]}{../img/fieldVelo.png}
 \caption{Velocity field on the vertical.}
 \label{t3d:vent:fig:fieldvelo}
\end{figure}

\bigskip
The velocity at the surface is 0.18~$\text{m}\cdot \text{s}^{-1}$ and the return
%Read USURF or MSURF in the 2D RESULT FILE: 0.177586
current reaches a maximum value of 0.058~$\text{m}\cdot \text{s}^{-1}$ (Figure
\ref{t3d:vent:fig:fieldvelo}). %0.05798
However, it must be pointed out that the velocity at the surface depends
on the refinement near the surface because the velocity gradient is
very high in this area.
A distance between the two first vertical points of 0.50~m instead of
0.10~m induced a velocity at the surface of 0.12~$\text{m}\cdot \text{s}^{-1}$, but the velocity
field below 1~m under the surface was only slightly modified.
J. Wu \cite{Wu1973} proposed for the velocity $\vec{u_s}$ us at the surface the
expression:
\begin{equation*}
\vec{u_s} = 0.55 \left(\frac{\vec{\tau}}{\rho_{air}}\right)^{1/2}.
\end{equation*}
This gives $\vec{u_s}$ = 0.19~$\text{m}\cdot \text{s}^{-1}$.
Then, the velocity computed with a mesh of 0.10~m is very close to
this theoretical value.
Figure \ref{t3d:vent:fig:profileV} shows the non-dimensional plot of the vertical velocity
profile.
\begin{figure}[!htbp]
 \centering
 \includegraphicsmaybe{[width=\textwidth]}{./img/profileV.png}
 \caption{Vertical profile of velocity.}
 \label{t3d:vent:fig:profileV}
\end{figure}
The numerical results fit the measurements reasonably well.
The upper part of the profile, where the velocities have the same
orientation as the wind, is close to measured profiles, but the lower
part is smoothed.
This could mean that the turbulence intensity is stronger in nature.
The slope of the free surface presented in figure \ref{t3d:vent:fig:FreeSurface}
is equal to $1.58\cdot 10^{-6}$ (between $x=100$~m and $x=400$~m). %1.5757212
% = (0.00022398578+0.00024873059)/(400-100)
The computation of the slope, assuming that the flow is homogeneous on the vertical,
gives a slope equal to $1.66\cdot 10^{-6}$.
This value is in agreement with the value given by \telemac{3D}.

\begin{figure}[!htbp]
 \centering
 \includegraphicsmaybe{[width=\textwidth]}{../img/freeSurface.png}
 \caption{Free surface.}
 \label{t3d:vent:fig:FreeSurface}
\end{figure}

\section{Conclusion}

%\bigskip
The velocity field produced by \telemac{3D} using the standard mixing length is correct.
Near the surface, the quality of the results depends on the vertical resolution near the surface.
The second level of the vertical mesh should be fixed 0.10~m below the surface for a good result.
In the deeper part, the profile is a little bit more smoothed.
Taking into account the effect of the wind directly into the turbulence model
may improve this result.
