!                   **********************
                    SUBROUTINE USER_LIMWAC
!                   **********************
     &(F     , FBOR  , NPTFR , NDIRE , NF    , NPOIN2,
     & KENT  , PRIVE , NPRIV , IMP_FILE)
!
!***********************************************************************
! TOMAWAC
!***********************************************************************
!
!     brief USER LIMIT SPECTRUM.
!           THIS IS AN EXAMPLE OF MAKING A SPECTRUM DEPENDING ON TIME
!           THE CHARACTERISTIC OF SEA STATE (HM0 TP and DIRECTION)
!           ARE READ IN A TEXT FILE LUFO1 = FORMATTED FILE 1
!           THERE IS NO INTERPOLATION BETWEEN TWO TIMES READ IN THE FILE
!
!~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
!| APHILL         |-->| BOUNDARY PHILLIPS CONSTANT
!| AT             |-->| COMPUTATION TIME
!| F              |-->| VARIANCE DENSITY DIRECTIONAL SPECTRUM
!| FBOR           |<->| SPECTRAL VARIANCE DENSITY AT THE BOUNDARIES
!| FETCHL         |-->| BOUNDARY MEAN FETCH VALUE
!| FPICL          |-->| BOUNDARY PEAK FREQUENCY
!| FPMAXL         |-->| BOUNDARY MAXIMUM PEAK FREQUENCY
!| FRA            |<--| DIRECTIONAL SPREADING FUNCTION VALUES
!| FRABL          |-->| BOUNDARY ANGULAR DISTRIBUTION FUNCTION
!| GAMMAL         |-->| BOUNDARY PEAK FACTOR
!| GRAVIT         |-->| GRAVITY ACCELERATION
!| HM0L           |-->| BOUNDARY SIGNIFICANT WAVE HEIGHT
!| IMP_FILE       |-->| MESH FILE WITH THE IMPOSED SPECTRA
!| KENT           |-->| B.C.: A SPECTRUM IS PRESCRIBED AT THE BOUNDARY
!| LIFBOR         |-->| TYPE OF BOUNDARY CONDITION ON F
!| LIMSPE         |-->| TYPE OF BOUNDARY DIRECTIONAL SPECTRUM
!| LT             |-->| NUMBER OF THE TIME STEP CURRENTLY SOLVED
!| NBOR           |-->| GLOBAL NUMBER OF BOUNDARY POINTS
!| NF             |-->| NUMBER OF FREQUENCIES
!| LUFO1          |-->| LOGICAL UNIT NUMBER OF THE USER FORMATTED FILE
!| NDIRE          |-->| NUMBER OF DIRECTIONS
!| NPOIN2         |-->| NUMBER OF POINTS IN 2D MESH
!| NPRIV          |-->| NUMBER OF PRIVATE ARRAYS
!| NPTFR          |-->| NUMBER OF BOUNDARY POINTS
!| PRIVE          |-->| USER WORK TABLE
!| SIGMAL         |-->| BOUNDARY SPECTRUM VALUE OF SIGMA-A
!| SIGMBL         |-->| BOUNDARY SPECTRUM VALUE OF SIGMA-B
!| SPEC           |<--| VARIANCE DENSITY FREQUENCY SPECTRUM
!| SPRE1L         |-->| BOUNDARY DIRECTIONAL SPREAD 1
!| SPRE2L         |-->| BOUNDARY DIRECTIONAL SPREAD 2
!| TETA1L         |-->| BOUNDARY MAIN DIRECTION 1
!| TETA2L         |-->| BOUNDARY MAIN DIRECTION 2
!| UV2D, VV2D     |-->| WIND VELOCITIES AT THE MESH POINTS
!| XLAMDL         |-->| BOUNDARY WEIGHTING FACTOR FOR ANGULAR
!|                |   | DISTRIBUTION FUNCTION
!~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
!
      USE INTERFACE_TOMAWAC, EX_USER_LIMWAC => USER_LIMWAC

      USE DECLARATIONS_TOMAWAC, ONLY : UV2D,VV2D,PROF, NPB,
     &     LIMSPE, FPMAXL, FETCHL, SIGMAL, SIGMBL, GAMMAL, FPICL ,
     &     HM0L  , APHILL, TETA1L, SPRE1L, TETA2L, SPRE2L, XLAMDL,
     &     SPEC  , FRA   , FRABL , AT    , LT    
     &     ,LIFBOR, NBOR, LUFO1, DEGRAD

      USE DECLARATIONS_SPECIAL
      USE BND_SPECTRA
      USE BIEF_DEF, ONLY : BIEF_FILE
      IMPLICIT NONE
!
!+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-
!
      INTEGER, INTENT(IN)            :: NPTFR,NDIRE,NF,NPOIN2,NPRIV
      INTEGER, INTENT(IN)            :: KENT
      DOUBLE PRECISION, INTENT(IN)   :: PRIVE(NPOIN2,NPRIV)
      TYPE(BIEF_FILE), INTENT(IN)    :: IMP_FILE
      DOUBLE PRECISION, INTENT(INOUT):: F(NPOIN2,NDIRE,NF)
      DOUBLE PRECISION, INTENT(INOUT):: FBOR(NPTFR,NDIRE,NF)
!
!+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-
!
      INTEGER NB_TIMES
      DOUBLE PRECISION,DIMENSION(:), ALLOCATABLE :: HM0L_FILE,FPICL_FILE
      DOUBLE PRECISION,DIMENSION(:), ALLOCATABLE :: TETA1L_FILE, AT_FILE
      INTEGER IPTFR, IDIR, IFF, TT, K
      SAVE TT, AT_FILE, HM0L_FILE, FPICL_FILE, TETA1L_FILE
!
!-----------------------------------------------------------------------
!     
      IF(LT.LT.1) THEN
        
        READ(LUFO1,*)
        READ(LUFO1,*) NB_TIMES
        READ(LUFO1,*)
        READ(LUFO1,*)
        ALLOCATE(HM0L_FILE(NB_TIMES),FPICL_FILE(NB_TIMES),
     &       TETA1L_FILE(NB_TIMES), AT_FILE(NB_TIMES))
          
        DO K = 1, NB_TIMES
          READ(LUFO1,*) AT_FILE(K), HM0L_FILE(K), FPICL_FILE(K)
     &         ,TETA1L_FILE(K)
        ENDDO
          
        HM0L = HM0L_FILE(1)
        FPICL = FPICL_FILE(1)
!     BE CAREFUL ANGLE ARE GIVEN IN DEGREE BUT TOMAWAC WORKS WITH RADIAN
        TETA1L = TETA1L_FILE(1) * DEGRAD
        TT = 2
      ENDIF
      IF(AT .GE. AT_FILE(TT)) THEN
        HM0L = HM0L_FILE(TT)
        FPICL = FPICL_FILE(TT)
        TETA1L = TETA1L_FILE(TT) * DEGRAD
        TT=TT+1
      ENDIF

      CALL SPEINI
     &     (FBOR  ,SPEC  ,FRA   ,UV2D  ,VV2D  , FPMAXL, FETCHL,
     &     SIGMAL, SIGMBL, GAMMAL, FPICL, HM0L  ,APHILL, TETA1L, SPRE1L,
     &     TETA2L, SPRE2L, XLAMDL, NPB  , NDIRE ,NF    ,
     &     LIMSPE, PROF  ,FRABL)
      DO IPTFR=1,NPTFR
        IF(LIFBOR(IPTFR).EQ.KENT) THEN
          DO IFF=1,NF
            DO IDIR=1,NDIRE
              F(NBOR(IPTFR),IDIR,IFF)=FBOR(IPTFR,IDIR,IFF)
            ENDDO
          ENDDO
        ENDIF
      ENDDO
      RETURN
      END
