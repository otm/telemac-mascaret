1 8
0 1000 50
500    25       1 tom_01
500    50       2 tom_02
500    75       3 tom_03
500   100       4 tom_04
500   125       5 tom_05
500   150       6 tom_06
500   175       7 tom_07
500   200       8 tom_08

Description of the file
1st line: number of periods and number of points
2nd line: period 1: start time, end time and interval (in seconds)
3rd-10th line: output points; x coordinate, y coordinate, station number, and station name

The x and y coordinate are used for linear interpolation.
The station number is an index, which is written to IKLE. (So you can easily know which station is in the output file)
The station name is not used by TELEMAC, but is to increase the readibility of this file.
The rest of the file are comments

This file is specially made to test the use of two different output periods
