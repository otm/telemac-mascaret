
"""
Class for validation of opposing
"""
from vvytel.vnv_study import AbstractVnvStudy
from execution.telemac_cas import TelemacCas, get_dico
from data_manip.extraction.telemac_file import TelemacFile
from postel.plot1d import plot1d
import matplotlib.pyplot as plt

class VnvStudy(AbstractVnvStudy):
    """
    Class for validation
    """

    def _init(self):
        """
        Defines the general parameter
        """
        self.rank = 2
        self.tags = ['tomawac']

    def _pre(self):
        """
        Defining the studies
        """
        # triads case scalar mode
        self.add_study('vnv_1',
                       'tomawac',
                       'tom_bejiBattjes.cas')

        #  triads case parallel mode
        cas = TelemacCas('tom_bejiBattjes.cas', get_dico('tomawac'))
        cas.set('PARALLEL PROCESSORS', 4)

        self.add_study('vnv_2',
                       'tomawac',
                       'tom_bejiBattjes_par.cas',
                       cas=cas)
        del cas



    def _check_results(self):
        """
        Post-treatment processes
        """
        # Comparison with the last time frame of the reference file.
        self.check_epsilons('vnv_1:WACRES',
                            'fom_bejiBattjes.slf',
                            eps=[1e-5])

        # Comparison with the last time frame of the reference file.
        self.check_epsilons('vnv_2:WACRES',
                            'fom_bejiBattjes.slf',
                            eps=[1e-5])

        # Comparison between sequential and parallel run.
        self.check_epsilons('vnv_1:WACRES',
                            'vnv_2:WACRES',
                            eps=[1e-5])


    def _post(self):
        """
        Post-treatment processes
        """
        # Getting files
        vnv_1_wacleo = self.get_study_file('vnv_1:WACLEO')
        spe = TelemacFile(vnv_1_wacleo)
        points = spe.get_list_spectrum_points()

        record = -1
        point = 7
        # Getting list of frequencies and spectrum value
        freq, spectrum = spe.get_spectrum(points[point], record)
        # Plotting it
        axe = plt.gca()
        plot1d(axe, freq, spectrum,
               plot_label='W',
               x_label='Frequency [Hz]',
               y_label='Spectrum')

        axe.legend()

        fig_name = "img/bejiSpec"
        print(" "*8+"~> Plotting "+fig_name)
        plt.savefig(fig_name)
        plt.close('all')

        spe.close()
