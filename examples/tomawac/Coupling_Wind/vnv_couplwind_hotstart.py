
"""
Validation script for Next_Comput
"""
from vvytel.vnv_study import AbstractVnvStudy
from execution.telemac_cas import TelemacCas, get_dico
from data_manip.extraction.telemac_file import TelemacFile
import matplotlib.pyplot as plt

class VnvStudy(AbstractVnvStudy):
    """
    Class for validation
    """

    def _init(self):
        """
        Defines the general parameter
        """
        self.rank = 2
        self.tags = ['tomawac']

    def _pre(self):
        """
        Defining the studies
        """
        self.add_study('vnv_1',
                       'telemac2d',
                       't2d_makeref.cas')
        self.add_study('vnv_2',
                       'telemac2d',
                       't2d_makeini.cas')
        self.add_study('vnv_3',
                       'telemac2d',
                       't2d_next.cas')

        cas =  TelemacCas('t2d_next.cas', get_dico('telemac2d'))
        cas.set('PARALLEL PROCESSORS', 4)
        self.add_study('vnv_3p',
                       'telemac2d',
                       'tom_next_par.cas',
                       cas=cas)
        del cas

    def _check_results(self):
        """
        Post-treatment processes
        """

        # Comparison with reference file and calculated directly with the 100 time step.
        self.check_epsilons('vnv_3:T2DRES',
                            'vnv_1:T2DRES',
                            eps=[3e-7, 8e-8, 4e-8, 4e-8, 1e-10, 1e-10, 1e-10])
        # Comparison between sequential and parallel run.
        self.check_epsilons('vnv_3:T2DRES',
                            'vnv_3p:T2DRES',
                            eps=[1e-9])

        # Comparison with reference file and calculated directly with the 100 time step.
        self.check_epsilons('vnv_3:WACRES',
                            'vnv_1:WACRES',
                            eps=[3e-8, 5e-6, 1e-10, 4e-8, 3e-7, 5e-8, 1e-10,\
                                 1e-10, 5e-9, 2e-8, 2e-5])
        # Comparison between sequential and parallel run.
        self.check_epsilons('vnv_3:WACRES',
                            'vnv_3p:WACRES',
                            eps=[1e-9])

    def _post(self):
        """
        Post-treatment processes
        """
        _, axe = plt.subplots(figsize=(10, 8))

        # Wind REF
        resfile = self.get_study_file('vnv_1:T2DRES')
        reswind = TelemacFile(resfile)
        windx = reswind.get_timeseries_on_nodes("WIND ALONG X", [0])
        windy = reswind.get_timeseries_on_nodes("WIND ALONG Y", [0])
        normwind = (windx**2+windy**2)**0.5
        timewind = reswind.times.transpose()
        axe.plot(timewind, normwind[0,:], 'x', label='Wind ref T2D')

        # wind before hotstart
        resfile = self.get_study_file('vnv_2:WACRES')
        reswind = TelemacFile(resfile)
        timewind = reswind.times.transpose()
        windx = reswind.get_timeseries_on_nodes("WIND ALONG X",[0])
        windy = reswind.get_timeseries_on_nodes("WIND ALONG Y",[0])
        normwind = (windx**2 + windy**2)**0.5
        axe.plot(timewind, normwind[0,:],'.' , label='Wind wac before hot start')

        # Hot start
        resfile = self.get_study_file('vnv_2:WACRBI')
        reswind = TelemacFile(resfile)
        timewind = reswind.times
        windx = reswind.get_data_on_horizontal_plane("CURRENT-WIND", -1, 2)
        windy = reswind.get_data_on_horizontal_plane("CURRENT-WIND", -1, 3)
        normwind = (windx**2 + windy**2)**0.5
        axe.plot(timewind[-1], normwind[0], 'go', markersize=8, label='Hot start')

        # after Hotstart
        resfile = self.get_study_file('vnv_3:WACRES')
        reswind = TelemacFile(resfile)
        windx = reswind.get_timeseries_on_nodes("WIND ALONG X",[0])
        windy = reswind.get_timeseries_on_nodes("WIND ALONG Y",[0])
        timewind = reswind.times
        normwind = (windx**2 + windy**2)**0.5
        axe.plot(timewind, normwind[0,:],'.' , label='Wind wac after hot start')

        plt.title('Wind (m/s) before and after a hot start')
        axe.legend()
        print('        ~> Plotting img/wind.png')
        plt.savefig('img/wind.png')

        waveheight = reswind.get_timeseries_on_nodes("WAVE HEIGHT HM0",[1053])
        ref = self.get_study_file('vnv_1:WACRES')
        resref = TelemacFile(ref)
        timeref = resref.times.transpose()
        whref=resref.get_timeseries_on_nodes("WAVE HEIGHT HM0",[1053])
        _, axe = plt.subplots(figsize=(10, 8))
        axe.plot(timeref, whref[0,:],'.' , label='reference of wave height')
        axe.plot(timewind, waveheight[0,:],'.' , label='after a hot start')
        plt.title('Wave height (m) at point 1053')
        axe.legend()
        print('        ~> Plotting img/waveheight.png')
        plt.savefig('img/waveheight.png')
