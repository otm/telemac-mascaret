
"""
Validation script for HERMES Python API
"""
import importlib.util
from sys import platform

from vvytel.vnv_study import AbstractVnvStudy


class VnvStudy(AbstractVnvStudy):
    """
    Class for validation
    """

    def _init(self):
        """
        Defines the general parameter
        """
        self.rank = 0
        self.tags = ['api', 'python3', 'med']

    def _pre(self):
        """
        Defining the studies
        """
        python = 'python' if platform == 'win32' else 'python3'

        # Skip read-write tests if the HERMES Python API is not available
        if importlib.util.find_spec("_hermes") is not None:
            # Read-write using HERMES
            self.add_command('vnv_read_write',
                            f'{python} read_write.py')

            # Read-write using HERMES with different file formats
            self.add_command('vnv_read_write_format',
                            f'{python} read_write_format.py')
        else:
            print(
                "  ~> HERMES Python API is not available, skipping read-write "
                "tests"
            )

        # TelemacFile test
        self.add_command('vnv_telemac_file',
                         f'{python} test_telemac_file.py')

    def _check_results(self):
        """
        Post-treatment processes
        """

    def _post(self):
        """
        Post-treatment processes
        """
