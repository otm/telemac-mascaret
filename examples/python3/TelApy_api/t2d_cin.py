#!/usr/bin/env python3
"""
Example of a TelApy TELEMAC-2D run (test case gouttedo_cin)
"""
from os import chdir, environ, getcwd, makedirs, path

import numpy as np
from mpi4py import MPI

from execution.telemac_cas import TelemacCas
from telapy.api.t2d import Telemac2d


def vnv_copy_files(module, cas_file, par):
    """
    Copying input files into validation folder if HOMETEL and USETEL other wise
    running in cas folder

    @param module (str) Name of the module
    @param cas_file (str) steering file
    @param dist (str) Folder in which to copy (creates it if it does not exist)
    """

    if 'USETELCFG' in environ and 'HOMETEL' in environ:
        vnv_working_dir = path.join(getcwd(),
                                    'vnv_api',
                                    path.basename(__file__[:-3])+par,
                                    environ['USETELCFG'])

        # Creating folders if they do not exists
        if MPI.COMM_WORLD.Get_rank() == 0:
            if not path.exists(vnv_working_dir):
                print("  ~> Creating: ", vnv_working_dir)
                makedirs(vnv_working_dir)
            chdir(path.dirname(cas_file))
            dico_file = path.join(environ['HOMETEL'], 'sources', module,
                                  module+'.dico')
            cas = TelemacCas(cas_file, dico_file)

            cas.copy_cas_files(vnv_working_dir, copy_cas_file=True)

            del cas
        MPI.COMM_WORLD.barrier()
    else:
        vnv_working_dir = path.dirname(cas_file)

    return vnv_working_dir


def main(recompile=True):
    """
    Main function of script

    @param recompile (Boolean) If True recompiling user fortran

    @retuns Value of ... at the end of the simulation
    """
    comm = MPI.COMM_WORLD

    root = environ.get('HOMETEL', path.join('..', '..', '..'))

    pwd = getcwd()

    cas_file = path.join(root, 'examples', 'telemac2d', 'gouttedo',
                         't2d_gouttedo_cin.cas')

    ncsize = comm.Get_size()

    par = '-par' if ncsize > 1 else "-seq"

    vnv_working_dir = vnv_copy_files('telemac2d', cas_file, par)

    chdir(vnv_working_dir)

    # Creation of the instance Telemac2d
    study = Telemac2d(path.basename(cas_file), user_fortran='user_fortran',
                      comm=comm, stdout=0, recompile=recompile)
    # Testing construction of variable list
    _ = study.variables

    study.set_case()
    # Initalization
    study.init_state_default()
    # Run all time steps
    study.run_all_time_steps()

    tmp = study.get_array("MODEL.IKLE")
    study.set_array("MODEL.IKLE", tmp)
    tmp2 = study.get_array("MODEL.IKLE")
    diff = abs(tmp2 - tmp)
    assert np.amax(diff) == 0

    tmp = study.mpi_get_array("MODEL.WATERDEPTH")
    study.mpi_set_array("MODEL.WATERDEPTH", tmp)
    tmp2 = study.mpi_get_array("MODEL.WATERDEPTH")
    diff = abs(tmp2 - tmp)
    assert np.amax(diff) < 1e-8
    # Ending the run
    study.finalize()
    # Instance delete
    del study
    chdir(pwd)
    return tmp2

if __name__ == "__main__":
    VAL1 = main()
    print("First run passed")
    VAL2 = main(recompile=False)
    print("Second run passed")
    assert np.array_equal(VAL1, VAL2)
    print("My work is done")
