#!/usr/bin/env python3
"""
Example of a TelApy MASCARET run (test case steady_kernel)
"""

from os import chdir, environ, getcwd, path

from telapy.api.masc import Mascaret


def main():
    "Main function"
    root = environ.get('HOMETEL', path.join('..', '..', '..'))

    pwd = getcwd()

    chdir(path.join(root, 'examples', 'mascaret', '01_Steady_Kernel'))
    # Model create
    masc = Mascaret()
    masc.create_mascaret(iprint=1)

    #  Mascaret files & import
    files_name = ['sarap.xcas', 'geometrie', 'hydrogramme.loi',
                  'limnigramme.loi', 'mascaret0.lis', 'mascaret0.opt']
    files_type = ['xcas', 'geo', 'loi', 'loi', 'listing', 'res']
    study_files = [files_name, files_type]
    masc.import_model(study_files[0], study_files[1])

    # Initialization
    npoin = masc.get_var_size('Model.X')[0]
    masc.init_hydro([0.]*npoin, [0.]*npoin)

    # Steady state computation with one step
    masc.compute(0., 1., 1.)

    # Get some results
    z, q = masc.get_hydro()
    print(z)
    print(q)
    vol = masc.get_volume()
    print(vol)

    # Delete Mascaret
    masc.delete_mascaret()

    chdir(pwd)


if __name__ == "__main__":
    main()
    main()
