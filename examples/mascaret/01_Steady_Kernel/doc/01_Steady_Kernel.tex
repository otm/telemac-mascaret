\chapter{Steady State}

%--------------------------------------------------------------------------------------------------
\section{Purpose}
This test shows the capability of \mascaret kernels to well compute a steady state in the very simple case of a rectangular channel.

A comparison of the result of the steady kernel and the converged results of the transcritical kernel, both implicit and explicit form is also done with the analytical solution.

%--------------------------------------------------------------------------------------------------
\section{Description}
\subsection*{Geometry}

The domain is a rectangular channel of $10000~\mathrm{m}$ long and $100~\mathrm{m}$ width with a slope of $0.05\%$.\\
The geometry is described by 2 cross section located at $X = 0~\mathrm{m}$ and $X = 10000~\mathrm{m}$ .

\subsection*{Initial condition}

No initial condition for the steady kernel (not requested).

An initial constant water level $Z = 8~\mathrm{m}$ with a constant discharge of $Q = 1000~\mathrm{m^{3}.s^{-1}}$ for the other simulation.

\subsection*{Boundary condition}

A constant discharge upstream: $Q = 1000~\mathrm{m^{3}.s^{-1}}$.\\
A constant level downstream: $H = 3~\mathrm{m}$.

\subsection*{Physical parameters}

The roughness coefficient is chosen so that the normal height is $h_n = 5~\mathrm{m}$ hence a value $K = 30.6~\mathrm{m^{1/3}.s^{-1}}$ (application of the Strickler formula).

\subsection*{Numerical parameters}

The mesh is define with a space step of $100~\mathrm{m}$.

The vertical discretization of the Cross Sections is homogeneous and equal to $1~\mathrm{m}$.

No time step parameters for the steady kernel.

For the transcritical kernel

The simulation duration is set to $2000$ time step.

The time step is variable with a wishes Courant number of $0.8$ for the explicit form and a wishes Courant number of $2$ for the implicit form.


%--------------------------------------------------------------------------------------------------
\section{Results}

\subsection*{Analytic solution}

In steady state, the Saint-Venant equation system is reduced to the dynamic equation, which is itself simplified. In addition, in a uniform channel, of rectangular geometry, the expression of the friction term is further simplified when this friction is assumed to be zero on the banks: the hydraulic radius is then equal to the water height $ h $, that is ie the variable to be determined. The solution is then written:
\begin{equation}
	\frac{\partial h}{\partial x}=\frac{I-\displaystyle \frac{q^2}{K^2h^{10/3}}}{1-\displaystyle \frac{q^2}{gh^3}}
\end{equation}
with
\begin{description}
	\item $I$: Bottom slope
	\item $K$: Strickler friction coefficient
	\item $q$: Discharge per width unit
\end{description}

This differential equation on $h$ is solved using a Runge-Kutta method ($10~\mathrm{m}$ step, $4^{th}$ order)

\begin{table}[H]
	\centering
	\begin{tabular}{c|c|c|c|c}
		\multicolumn{1}{|p{2.5cm}|}{\centering Abscissae}
		& \multicolumn{1}{|p{2.5cm}|}{\centering Analytic \\ Solution}
		& \multicolumn{1}{|p{2.5cm}|}{\centering Steady \\ Kernel}
		& \multicolumn{1}{|p{2.5cm}|}{\centering Transcritical \\ Kernel \\ Explicit}
		& \multicolumn{1}{|p{2.5cm}|}{\centering Transcritical \\ Kernel \\ Implicit} \\
		\hline
		\InputIfFileExists{../img/table.txt}{}{}{}{}{}\\
	\end{tabular}
	\label{mascaret:steady_kernel:tab1}
	\caption{Numerical comparison of analytic and computed water height}
\end{table}

Figures \ref{mascaret:steady_kernel:long} and \ref{mascaret:steady_kernel:zoom} show the comparison of water height with the different kernel.
We could also see in figure \ref{mascaret:steady_kernel:long} that the water height upstream tends to approach asymptotically the normal height $ h_n $ which is theoretically exact

\begin{figure}[H]
	\centering
	\includegraphicsmaybe{[width=0.75\textwidth]}{../img/long.png}
	\caption{Comparison of analytic and computed water height}
	\label{mascaret:steady_kernel:long}
\end{figure}

\begin{figure}[H]
	\centering
	\includegraphicsmaybe{[width=0.75\textwidth]}{../img/zoom.png}
	\caption{Zoom of figure \ref{mascaret:steady_kernel:long} between $9000~\mathrm{m}$ and $10000~\mathrm{m}$}
	\label{mascaret:steady_kernel:zoom}
\end{figure}

%--------------------------------------------------------------------------------------------------
\section{Conclusion}
The treatment of gravity-friction terms in the dynamic equation is good.

The comparison of all solutions to the analytical one show the good agreement of all kernels.

