
"""
Validation script for test21
"""
from os import path
import numpy as np
import matplotlib.pyplot as plt
from vvytel.vnv_study import AbstractVnvStudy
from postel.plot1d import plot1d


class VnvStudy(AbstractVnvStudy):
    """
    Class for validation
    """

    def _init(self):
        """
        Defines the general parameter
        """
        self.rank = 0
        self.tags = ['mascaret']

    def _pre(self):
        """
        Defining the studies
        """

        # Test21 transcritical kernel
        self.add_study('vnv_1',
                       'mascaret',
                       'mascaret.xcas')

    def _check_results(self):
        """
        Comparison with reference file
        """
        # self.check_epsilons('vnv_1:res',
        #                     'ref/mascaret.rub',
        #                     eps=[1.E-1],
        #                     var1='Cote de l eau',
        #                     var2='Debit mineur',
        #                     masc=True)

    def _post(self):
        """
        Post-treatment processes
        """
        masc_res, _ = self.get_study_res(
            'vnv_1:mascaret.rub', 'mascaret')

        t_masc = masc_res.times

        x_masc = [masc_res.reaches[1].get_section_pk_list(),
                  masc_res.reaches[2].get_section_pk_list(),
                  masc_res.reaches[3].get_section_pk_list(),
                  ]
        x_masc = np.concatenate(x_masc)
        x3_11 = np.argmax(x_masc >= 11000)
        x2_8 = np.argmax(x_masc >= 8300)

        var_pos_z = masc_res.get_position_var("Cote de l eau")
        var_pos_q = masc_res.get_position_var("Debit total")
        z3_masc = masc_res.get_series(3, x3_11, [var_pos_z])
        q3_masc = masc_res.get_series(3, x3_11, [var_pos_q])
        z2_masc = masc_res.get_series(2, x2_8, [var_pos_z])
        q2_masc = masc_res.get_series(2, x2_8, [var_pos_q])

        # =====================================================================
        #         TELEMAC
        # =====================================================================
        t_telemac = [0, 1900, 2800, 3700, 4600, 5500, 6400, 7300, 8200, 9100, 10000]
        q_amont = [0, 0, 540, 500, 0, -90, -170, -150, -160, -155, -200]
        q_aval = [0, 0, 2304, 3370, 4262, 4375, 4201, 3906, 3620, 3310, 3079]

        # =====================================================================
        # DISPLAY
        # =====================================================================
        fig, axe = plt.subplots(figsize=(6, 5))
        plot1d(axe, t_masc, q3_masc,
               plot_label='Mascaret',
               x_label='TIME [s]',
               y_label='Q [m3/s]',
               linestyle='-', color='b',)
        plot1d(axe, t_telemac, q_amont,
               plot_label='Telemac-2D',
               x_label='TIME [s]',
               y_label='Q [m3/s]',
               linestyle=':', color='r', marker='+')
        axe.legend()
        plt.tight_layout()
        plt.savefig(path.join('.', 'img', 'Q3.png'))
        plt.close(fig)

        fig, axe = plt.subplots(figsize=(6, 5))
        plot1d(axe, t_masc, q2_masc,
               plot_label='Mascaret',
               x_label='TIME [s]',
               y_label='Q [m3/s]',
               linestyle='-', color='b',)
        plot1d(axe, t_telemac, q_aval,
               plot_label='Telemac-2D',
               x_label='TIME [s]',
               y_label='Q [m3/s]',
               linestyle=':', color='r', marker='+')
        axe.legend()
        plt.tight_layout()
        plt.savefig(path.join('.', 'img', 'Q2.png'))
        plt.close(fig)

        fig, axe = plt.subplots(figsize=(6, 5))
        plot1d(axe, t_masc, z3_masc,
               plot_label='Mascaret',
               x_label='TIME [s]',
               y_label='Z [m]',
               linestyle='-', color='b',)
        axe.legend()
        plt.tight_layout()
        plt.savefig(path.join('.', 'img', 'z3.png'))
        plt.close(fig)

        fig, axe = plt.subplots(figsize=(6, 5))
        plot1d(axe, t_masc, z2_masc,
               plot_label='Mascaret',
               x_label='TIME [s]',
               y_label='Z [m]',
               linestyle='-', color='b',)
        axe.legend()
        plt.tight_layout()
        plt.savefig(path.join('.', 'img', 'z2.png'))
        plt.close(fig)
