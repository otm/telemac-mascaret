
"""
Validation script for test18
"""
from os import path
import numpy as np
import matplotlib.pyplot as plt
from vvytel.vnv_study import AbstractVnvStudy
from postel.plot1d import plot1d


class VnvStudy(AbstractVnvStudy):
    """
    Class for validation
    """

    def _init(self):
        """
        Defines the general parameter
        """
        self.rank = 0
        self.tags = ['mascaret']

    def _pre(self):
        """
        Defining the studies
        """

        # Test18 permanent kernel
        self.add_study('vnv_1',
                       'mascaret',
                       'sarap.xcas')

        # Test18 transcritical kernel
        self.add_study('vnv_2',
                       'mascaret',
                       'mascaret.xcas')

    def _check_results(self):
        """
        Comparison with reference file
        """
        self.check_epsilons('vnv_1:res',
                            'ref/sarap_ref.opt',
                            eps=[1.E-3],
                            masc=True)

        self.check_epsilons('vnv_2:res',
                            'ref/mascaret_ref.opt',
                            eps=[1.E-3],
                            masc=True)

    def _post(self):
        """
        Post-treatment processes
        """

        # Getting results from mascaret file SARAP
        sarap_res, _ = self.get_study_res('vnv_1:sarap_ecr.opt', 'mascaret')
        x_sarap = sarap_res.reaches[1].get_section_pk_list()
        var_pos_sarap = sarap_res.get_position_var('Cote du fond')
        var_pos_sarap = sarap_res.get_position_var('Cote de l eau')
        cote_sarap = sarap_res.get_values_at_reach(-1, 1, [var_pos_sarap])
        var_pos_sarap = sarap_res.get_position_var('Nombre de Froude')
        fr_sarap = sarap_res.get_values_at_reach(-1, 1, [var_pos_sarap])
        ressaut_sarap = x_sarap[np.argmin(np.diff(fr_sarap, axis=0))]
        amont_sarap = cote_sarap[0]
        var_pos_sarap = sarap_res.get_position_var('Debit mineur')
        q_sarap = sarap_res.get_values_at_reach(-1, 1, [var_pos_sarap])

        # Getting results from mascaret file transcritical kernel
        masc_res, _ = self.get_study_res('vnv_2:mascaret_ecr.opt', 'mascaret')
        x_masc = masc_res.reaches[1].get_section_pk_list()
        var_pos_masc = masc_res.get_position_var('Cote du fond')
        fond_masc = masc_res.get_values_at_reach(-1, 1, [var_pos_masc])
        var_pos_masc = masc_res.get_position_var('Cote de l eau')
        cote_masc = masc_res.get_values_at_reach(-1, 1, [var_pos_masc])
        var_pos_masc = masc_res.get_position_var('Nombre de Froude')
        fr_masc = masc_res.get_values_at_reach(-1, 1, [var_pos_masc])
        ressaut_masc = x_masc[np.argmin(np.diff(fr_masc, axis=0))]
        amont_masc = cote_masc[0]
        var_pos_masc = masc_res.get_position_var('Debit mineur')
        q_masc = masc_res.get_values_at_reach(-1, 1, [var_pos_masc])

        # =====================================================================
        # DISPLAY
        # =====================================================================

        fig, axe = plt.subplots(figsize=(6, 5))
        plot1d(axe, x_sarap, fond_masc,
               plot_label='Bottom level',
               x_label='Abscissae [m]',
               y_label='Water level [m]',
               linestyle='-', color='k',)
        plot1d(axe, x_sarap, cote_sarap,
               plot_label='Steady State',
               x_label='Abscissae [m]',
               y_label='Water level [m]',
               linestyle=':', color='r',)
        plot1d(axe, x_masc, cote_masc,
               plot_label='Transcritical Explicit (Nc=0.8)',
               x_label='Abscissae [m]',
               y_label='Water level [m]',
               linestyle='--', color='b',
               )
        axe.legend()
        plt.tight_layout()
        plt.savefig(path.join('.', 'img', 'cote.png'))
        plt.close(fig)

        fig, axe = plt.subplots(figsize=(6, 5))
        plot1d(axe, x_sarap, q_sarap,
               plot_label='Steady State',
               x_label='Abscissae [m]',
               y_label='Q [m3/s]',
               linestyle='-', color='r',)
        plot1d(axe, x_masc, q_masc,
               plot_label='Transcritical Explicit (Nc=0.8)',
               x_label='Abscissae [m]',
               y_label='Q [m3/s]',
               linestyle='--', color='b',)
        axe.legend()
        plt.tight_layout()
        plt.savefig(path.join('.', 'img', 'q.png'))
        plt.close(fig)

        fig, axe = plt.subplots(figsize=(6, 5))
        plot1d(axe, x_sarap, fr_sarap,
               plot_label='Steady State',
               x_label='Abscissae [m]',
               y_label='Froude',
               linestyle='-', color='r',)
        plot1d(axe, x_masc, fr_masc,
               plot_label='Transcritical Explicit (Nc=0.8)',
               x_label='Abscissae [m]',
               y_label='Froude',
               linestyle='--', color='b',)
        axe.legend()
        plt.tight_layout()
        plt.savefig(path.join('.', 'img', 'Fr.png'))
        plt.close(fig)

        # =====================================================================
        # TABLE
        # =====================================================================

        with open(path.join('.', 'img', 'comparaison.txt'), 'w') as file:
            # Write header
            file.write(
                " & SARAP & MASCARET (EXP., Nc=0.8) & Difference \\\\\n")
            # Write Data
            file.write(f"""Upstream water level &
                       {amont_sarap[0]:.3f} m & {amont_masc[0]:.3f} m &
                       {amont_sarap[0] - amont_masc[0]:.3f} m \\\\\n""")
            file.write(f"""Position of hydraulic jump &
                       {ressaut_sarap:.1f} m & {ressaut_masc:.1f} m &
                       {ressaut_masc-ressaut_sarap:.1f} m""")
