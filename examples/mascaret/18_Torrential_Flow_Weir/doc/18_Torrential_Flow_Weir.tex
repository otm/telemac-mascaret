\chapter{Torrential Flow in a Weir}

%--------------------------------------------------------------------------------------------------
\section{Purpose}
The aim of this test case is to validate the transcritical kernel and the steady-state kernel SARAP, in the context of strong torrential flow, with a downstream rise influenced by the downstream boundary condition. The flow is introduced by 18 lateral inflows at the beginning of the channel, which is located in the section with the lowest gradient. Validation is carried out by comparing the results obtained using the steady-state kernel SARAP with those from the transcritical kernel. The steady-state is achieved by convergence for the transcritical kernel.

%--------------------------------------------------------------------------------------------------
\section{Description}

\subsection*{Geometry}
The calculation is performed in a steeply sloping prismatic channel $240~\mathrm{m}$ long. The channel is divided into three sections. The first section, $60~\mathrm{m}$ long, has a slope of $8\%$, and the second section, $1300~\mathrm{m}$ long, has a slope of $25\%$. The downstream section is almost horizontal and features a widening of the channel.

The channel geometry is described by 10 cross-sections, as illustrated in Figure \ref{mascaret:test18:geometry}.

\begin{figure}[H]
	\centering
	\includegraphicsmaybe{[width=\textwidth]}{../doc/channel.png}
	\caption{Top view of channel geometry.}
	\label{mascaret:test18:geometry}
\end{figure}

\subsection*{Initial condition}
For the steady kernel SARAP, no initial condition is necessary.\\
For the transient kernel, an initial water line is imposed.

\subsection*{Boundary condition}
Upstream: Constant discharge, $Q = 6.138~\mathrm{m^{3}.s^{-1}}$.\\
Downstream: Constant water level,$\mathrm{Water~level} = 744.874~\mathrm{m}$.

\subsection*{Physical parameters}
Strickler coefficients are defined as follows:
\begin{itemize} 
	\item $K_m = 75~\mathrm{m^{1/3}.s^{-1}}$ in the first part of the channel ($X\leq 191~\mathrm{m}$).
	\item $K_m = 5~\mathrm{m^{1/3}.s^{-1}}$ in the downstream part ($X>191~\mathrm{m}$).
\end{itemize}
 
\subsection*{Lateral inflows}
Eighteen linear inflows are distributed between $0$ and $60~\mathrm{m}$, with each inflow equal to $4.07~\mathrm{m^{3}.s^{-1}}$. 

\subsection*{Numerical parameters}
The domain has been divided into several mesh zones:
\begin{itemize} 
	\item For $0~\mathrm{m} <x< 66~\mathrm{m}$, $\Delta x = 1~\mathrm{m}$.
	\item For $66~\mathrm{m} <x< 190~\mathrm{m}$, $\Delta x = 2~\mathrm{m}$.
	\item For $190~\mathrm{m} <x< 240~\mathrm{m}$, $\Delta x = 4~\mathrm{m}$.
\end{itemize}
The vertical discretization of the cross-section is uniform in the domain and equal to $0.1~\mathrm{cm}$.

No time step parameters for the steady kernel SARAP.\\
For the transcritical kernel : the simulation duration is set to $5000$ time steps. The time step is variable with a wishes Courant number of $0.8$.

Two kernels are used : the steady-state kernel SARAP and transcritical kernel in its explicit form.
 
%--------------------------------------------------------------------------------------------------
\section{Results}

The water line calculated with the steady-state kernel SARAP is compared with the converged water line of the transient regime. The comparison of water levels, flow rates, and Froude numbers are shown in Figures \ref{mascaret:test18:cote}, \ref{mascaret:test18:q} and \ref{mascaret:test18:fr}. The two schemes yield nearly identical results upstream of the hydraulic jump (Table \ref{mascaret:test18:comparaison}). 
However, the hydraulic jump occurs further upstream with the transcritical kernel compared to the steady-state kernel.

\begin{figure}[H]
	\centering
	\includegraphicsmaybe{[width=0.75\textwidth]}{../img/cote.png}
	\caption{Comparison of water levels.}
	\label{mascaret:test18:cote}
\end{figure}

\begin{figure}[H]
	\centering
	\includegraphicsmaybe{[width=0.75\textwidth]}{../img/q.png}
	\caption{Comparison of flow discharge.}
	\label{mascaret:test18:q}
\end{figure}

\begin{figure}[H]
	\centering
	\includegraphicsmaybe{[width=0.75\textwidth]}{../img/Fr.png}
	\caption{Comparison of Froude numbers.}
	\label{mascaret:test18:fr}
\end{figure}

\begin{table}[H]
	\begin{center}
		\caption{Comparison of results computed with the steady-state kernel SARAP and the transcritical kernel.}
		\label{mascaret:test18:comparaison}
		\begin{tabular}{l|c|c|c}
			\toprule
			\InputIfFileExists{../img/comparaison.txt}{}{}{}{}{}\\
			\bottomrule
		\end{tabular}
	\end{center}
\end{table}

The transcritical kernel reveals a flow peak near the hydraulic jump, a behavior observed in all test cases using the transcritical kernel.

%--------------------------------------------------------------------------------------------------
\section*{Conclusion}
This test case successfully validates the steady-state kernel SARAP for modeling very torrential flows, characterized here by a Froude number greater than 5