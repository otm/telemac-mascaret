"""
Validation script for test13
"""
from os import path
import numpy as np
import matplotlib.pyplot as plt
from vvytel.vnv_study import AbstractVnvStudy
from postel.plot1d import plot1d


class VnvStudy(AbstractVnvStudy):
    """
    Class for validation
    """

    def _init(self):
        """
        Defines the general parameter
        """
        self.rank = 0
        self.tags = ['mascaret']

    def _pre(self):
        """
        Defining the studies
        """

        # Test13 unsteady subcritical kernel
        self.add_study('vnv_1',
                       'mascaret',
                       'rezo.xcas')

        # Test13 transcritical kernel
        self.add_study('vnv_2',
                       'mascaret',
                       'mascaret.xcas')

    def _check_results(self):
        """
        Comparison with reference file
        """
        self.check_epsilons('vnv_1:res',
                            'ref/rezo_ref.opt',
                            eps=[1.E-3],
                            masc=True)

        # Comparison with reference file
        self.check_epsilons('vnv_2:res',
                            'ref/mascaret_imp_ref.opt',
                            eps=[1.E-3],
                            masc=True)

    def _post(self):
        """
        Post-treatment processes
        """
        # Getting results from mascaret file rezo
        rezo_res, _ = self.get_study_res('vnv_1:rezo_ecr.opt', 'mascaret')
        x_rezo = rezo_res.reaches[1].get_section_pk_list()
        indice_x_list = [1, np.argmax(x_rezo >= 2000), len(x_rezo)]
        t_rezo = rezo_res.times
        var_pos = rezo_res.get_position_var("Debit mineur")
        q_rezo = []
        for indice_x in indice_x_list:
            q_rezo.append(rezo_res.get_series(1, indice_x, [var_pos]))

        peak_discharge_rezo = np.argmax(np.array(q_rezo)[2, :])
        time_peak_rezo = t_rezo[peak_discharge_rezo]
        q_peak_rezo = np.max(np.array(q_rezo)[2, :])

        # Getting results from mascaret file mascaret
        masc_imp_res, _ = self.get_study_res('vnv_2:mascaret_ecr.opt', 'mascaret')
        x_masc = masc_imp_res.reaches[1].get_section_pk_list()
        indice_x_list = [1, np.argmax(x_masc >= 2000), len(x_masc)]
        t_masc = masc_imp_res.times
        var_pos = masc_imp_res.get_position_var("Debit mineur")
        q_masc = []
        for indice_x in indice_x_list:
            q_masc.append(masc_imp_res.get_series(1, indice_x, [var_pos]))

        peak_discharge_masc = np.argmax(np.array(q_masc)[2, :])
        time_peak_masc = t_masc[peak_discharge_masc]
        q_peak_masc = np.max(np.array(q_masc)[2, :])

        # =====================================================================
        # TABLE
        # =====================================================================
        time_peak_analytical = 43200
        q_peak_analytical = 5800
        with open(path.join('.', 'img', 'peak_discharge.txt'), 'w') as file:
            # Write header
            file.write(
                " & Analytical  & Rezo  & Mascaret (Implicit) \\\\\n")

            file.write(f""" Time to peak & {time_peak_analytical:.0f} s &
                       {time_peak_rezo:.0f} s &
                       {time_peak_masc:.0f} s \\\\\n""")

            file.write(f"""Maximal discharge & {q_peak_analytical:.0f} $m^3/s$ &
                       {q_peak_rezo:.0f} $m^3/s$ &
                       {q_peak_masc:.0f} $m^3/s$""")

        # =====================================================================
        # DISPLAY
        # =====================================================================
        fig, axe = plt.subplots(figsize=(6, 5))
        plot1d(axe, t_rezo, np.array(q_rezo)[0, :],
               plot_label='X = 0 m',
               x_label='TIME [s]',
               y_label='Q [m3/s]',
               linestyle='-', color='b',)
        plot1d(axe, t_rezo, np.array(q_rezo)[1, :],
               plot_label='X = 2000 m',
               x_label='TIME [s]',
               y_label='Q [m3/s]',
               linestyle='--', color='r',)
        plot1d(axe, t_rezo, np.array(q_rezo)[2, :],
               plot_label='X = 4000 m',
               x_label='TIME [s]',
               y_label='Q [m3/s]',
               linestyle='-.', color='g',)
        axe.legend()
        plt.tight_layout()
        plt.savefig(path.join('.', 'img', 'q_rezo.png'))
        plt.close(fig)

        fig, axe = plt.subplots(figsize=(6, 5))
        plot1d(axe, t_masc, np.array(q_masc)[0, :],
               plot_label='X = 0 m',
               x_label='TIME [s]',
               y_label='Q [m3/s]',
               linestyle='-', color='b',)
        plot1d(axe, t_masc, np.array(q_masc)[1, :],
               plot_label='X = 2000 m',
               x_label='TIME [s]',
               y_label='Q [m3/s]',
               linestyle='--', color='r',)
        plot1d(axe, t_masc, np.array(q_masc)[2, :],
               plot_label='X = 4000 m',
               x_label='TIME [s]',
               y_label='Q [m3/s]',
               linestyle='-.', color='g',)
        axe.legend()
        plt.tight_layout()
        plt.savefig(path.join('.', 'img', 'q_masc.png'))
        plt.close(fig)

        # =====================================================================
        # DIFFERENCES KERNEL
        # =====================================================================
        q_difference = 100 * \
            (np.array(q_masc)[2, :] - np.array(q_rezo)[2, :])/np.array(q_rezo)[2, :]

        fig, axe = plt.subplots(figsize=(6, 5))
        plot1d(axe, t_masc, q_difference,
               plot_label='REZO/MASCARET',
               x_label='Abscissae [m]',
               y_label='$\\epsilon$ [%]')
        plt.tight_layout()
        plt.savefig(path.join('.', 'img', 'difference_q.png'))
        plt.close(fig)
