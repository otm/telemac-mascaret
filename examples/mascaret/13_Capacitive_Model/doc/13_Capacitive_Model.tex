\chapter{Capacitive Model}

%--------------------------------------------------------------------------------------------------
\section{Purpose}
This test case aims to validate the transient calculation kernels in the case of a reservoir / spillway capacitive model. The input wave is a very slow flood wave (24 hours), the reach is short, and its depth is high, so acceleration terms are negligible and the propagation speed of disturbances is very fast. Additionally, the bottom is assumed to be horizontal, and there is no friction. The represented reach will therefore behave like a reservoir (with characteristic variables being the inflow rate, the outflow rate, and the surface elevation), storing water during the first part of the flood (as long as the elevation rises), then releasing it afterward (from the moment the elevation falls), until returning to the initial state. The simplest flood propagation models are based on this schematization (whose essential result is the capacity, hence the term "capacitive model").

%--------------------------------------------------------------------------------------------------
\section{Description}
\subsection*{Geometry}
The calculation is conducted in a rectangular channel with zero slope, extending over a length of $4~\mathrm{km}$, a width of $1000~\mathrm{m}$ and $20~\mathrm{m}$ deep. The channel geometry is described by 2 cross-sections located at $X = 0~\mathrm{m}$ and $X = 4~\mathrm{km}$.

\subsection*{Initial condition}
The initial condition is a steady, uniform flow with a constant discharge of $1118~\mathrm{m^{3}.s^{-1}}$ and a horizontal free surface with an elevation of $Z = 30~\mathrm{m}$.

\subsection*{Boundary condition}
Downstream, a rating curve is imposed as $Q = aH^{3/2}$ to simulate the operation of a spillway.\\
Upstream, the discharge is imposed according to a specified hydrograph.

\subsection*{Physical parameters}
Friction is not considered in this test case.

\subsection*{Numerical parameters}
The domain has been divided into $39$ cells of constant length $100~\mathrm{m}$.\\
The vertical discretization of the cross-section is uniform across the domain and set at $2~\mathrm{m}$ ($40$ horizontal steps).

The time step is $30~\mathrm{s}$ for both the unsteady fluvial kernel REZO and the transcritical kernel simulations, with each calculation running for $24~\mathrm{h}$, totaling $2880$ time steps

Two simulations are conducted: one with the unsteady fluvial kernel REZO, and another with the transcritical kernel in its implicit forms.
 
%--------------------------------------------------------------------------------------------------
\section{Results}
\subsection*{Analytical solution}
The model functions as a storage system. A quasi-analytical solution can be computed to determine the time to reach the maximum reservoir level, which corresponds to the peak downstream discharge and the moment when upstream discharge becomes less than downstream discharge. The theoretical time to reach the maximum downstream flow of $5800~\mathrm{m^3.s^{-1}}$ is $43200~\mathrm{s}$, or approximately $71~\mathrm{h}$, with an analytical water elevation of $40~\mathrm{m}$.
% TODO : Set the complete method

\subsection*{Validation}
Figures~\ref{mascaret:test13:q_rezo} and \ref{mascaret:test13:q_masc} show the hydrographs at three points along the reach ($X = 0~\mathrm{m}$, $X = 2000~\mathrm{m}$, $X = 4000~\mathrm{m}$) for both the unsteady fluvial kernel REZO and the transcritical implicit kernel. The hydrograph peaks are very close, indicating that disturbances propagate quickly due to the significant water depth in the reservoir.

\begin{figure}[H]
	\centering
	\includegraphicsmaybe{[width=0.75\textwidth]}{../img/q_rezo.png}
	\caption{Temporal evolution of discharge for the unsteady fluvial kernel REZO.}
	\label{mascaret:test13:q_masc}
\end{figure}

\begin{figure}[H]
	\centering
	\includegraphicsmaybe{[width=0.75\textwidth]}{../img/q_masc.png}
	\caption{Temporal evolution of discharge for the transcritical kernel with its implicit forms.}
	\label{mascaret:test13:q_rezo}
\end{figure}

The time to reach the maximum downstream flow and the peak discharge calculated by the two kernels are compared with the analytical solution in Table~\ref{mascaret:test13:peak_downstream}.

\begin{table}[H]
	\begin{center}
		\caption{Numerical comparison of analytic and computed of the peak downstream discharge.}
		\label{mascaret:test13:peak_downstream}
		\begin{tabular}{l|c|c|c}
			\toprule
			\InputIfFileExists{../img/peak_discharge.txt}{}{}{}{}{}\\
			\bottomrule
		\end{tabular}
	\end{center}
\end{table}

Calculations are close to analytical values.

The calculated time to reach the maximum downstream flow is $43200~\mathrm{s}$ ($12~\mathrm{h}$) for a discharge of $5800~\mathrm{m^{3}.s^{-1}}$ and a water elevation of $39.99~\mathrm{m}$ with both kernels.

Figure~\ref{mascaret:test13:difference} compares the results of the unsteady fluvial kernel REZO with the transcritical kernel. The transcritical kernel remains stable with time steps of $30~\mathrm{s}$, corresponding to a Courant number of 5, and the comparison between the two schemes is entirely satisfactory.

\begin{figure}[H]
	\centering
	\includegraphicsmaybe{[width=0.75\textwidth]}{../img/q_masc.png}
	\caption{Difference in downstream discharge between unsteady fluvial kernel REZO and transcritical kernel.}
	\label{mascaret:test13:difference}
\end{figure}

%--------------------------------------------------------------------------------------------------
\section*{Conclusion}
The results from the two simulation schemes are consistent and closely match the analytical solution, demonstrating the accuracy of both the unsteady fluvial kernel REZO and the transcritical kernel in modeling a capacitive reservoir/spillway system.