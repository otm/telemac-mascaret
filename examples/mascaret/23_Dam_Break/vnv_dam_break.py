
"""
Validation script for test23
"""
from os import path
import numpy as np
import matplotlib.pyplot as plt
from vvytel.vnv_study import AbstractVnvStudy
from postel.plot1d import plot1d


class VnvStudy(AbstractVnvStudy):
    """
    Class for validation
    """

    def _init(self):
        """
        Defines the general parameter
        """
        self.rank = 0
        self.tags = ['mascaret']

    def _pre(self):
        """
        Defining the studies
        """

        # Test23 transcritical kernel
        self.add_study('vnv_1',
                       'mascaret',
                       'mascaret.xcas')

    def _check_results(self):
        """
        Comparison with reference file
        """
        self.check_epsilons('vnv_1:res',
                            'ref/mascaret.rub',
                            eps=[1.E-1],
                            var1='Cote de l eau',
                            var2='Debit total',
                            masc=True)

    def _post(self):
        """
        Post-treatment processes
        """

        masc_res, _ = self.get_study_res(
            'vnv_1:mascaret.rub', 'mascaret')
        t_masc = np.array(masc_res.times)
        t1 = 0
        t3000 = np.argmax(t_masc >= 3000)
        t6000 = np.argmax(t_masc >= 6000)
        t9000 = np.argmax(t_masc >= 9000)

        x_masc = [masc_res.reaches[1].get_section_pk_list(),
                  masc_res.reaches[2].get_section_pk_list(),
                  masc_res.reaches[3].get_section_pk_list(),
                  masc_res.reaches[4].get_section_pk_list(), ]

        x_masc = np.concatenate(x_masc)
        x_dam2 = np.argmax(x_masc >= 56318)
        x_trib1 = np.argmax(x_masc >= 9911)
        x_trib_p = len(x_masc)

        var_pos_zmax = masc_res.get_position_var("Cote maximale atteinte")
        var_pos_z = masc_res.get_position_var("Cote de l eau")
        var_pos_q = masc_res.get_position_var("Debit total")
        var_pos_b = masc_res.get_position_var("Cote du fond")

        zb_masc = [masc_res.get_values_at_reach(-1, 1, var_pos_b),
                   masc_res.get_values_at_reach(-1, 2, var_pos_b),
                   masc_res.get_values_at_reach(-1, 3, var_pos_b),
                   masc_res.get_values_at_reach(-1, 4, var_pos_b)]
        zb_masc = np.concatenate(zb_masc)

        zmax_masc = [masc_res.get_values_at_reach(-1, 1, var_pos_zmax),
                     masc_res.get_values_at_reach(-1, 2, var_pos_zmax),
                     masc_res.get_values_at_reach(-1, 3, var_pos_zmax),
                     masc_res.get_values_at_reach(-1, 4, var_pos_zmax)]
        zmax_masc = np.concatenate(zmax_masc)

        z1_masc = [masc_res.get_values_at_reach(t1, 1, var_pos_z),
                   masc_res.get_values_at_reach(t1, 2, var_pos_z),
                   masc_res.get_values_at_reach(t1, 3, var_pos_z),
                   masc_res.get_values_at_reach(t1, 4, var_pos_z)]
        z1_masc = np.concatenate(z1_masc)

        z3_masc = [masc_res.get_values_at_reach(t3000, 1, var_pos_z),
                   masc_res.get_values_at_reach(t3000, 2, var_pos_z),
                   masc_res.get_values_at_reach(t3000, 3, var_pos_z),
                   masc_res.get_values_at_reach(t3000, 4, var_pos_z)]
        z3_masc = np.concatenate(z3_masc)

        z6_masc = [masc_res.get_values_at_reach(t6000, 1, var_pos_z),
                   masc_res.get_values_at_reach(t6000, 2, var_pos_z),
                   masc_res.get_values_at_reach(t6000, 3, var_pos_z),
                   masc_res.get_values_at_reach(t6000, 4, var_pos_z)]
        z6_masc = np.concatenate(z6_masc)

        z9_masc = [masc_res.get_values_at_reach(t9000, 1, var_pos_z),
                   masc_res.get_values_at_reach(t9000, 2, var_pos_z),
                   masc_res.get_values_at_reach(t9000, 3, var_pos_z),
                   masc_res.get_values_at_reach(t9000, 4, var_pos_z)]
        z9_masc = np.concatenate(z9_masc)

        q_dam2_up = masc_res.get_series(3, x_dam2, [var_pos_q])
        q_dam2_down = masc_res.get_series(3, x_dam2+1, [var_pos_q])
        q_trib1_up = masc_res.get_series(1, x_trib1, [var_pos_q])
        q_trib1_down = masc_res.get_series(2, x_trib1+1, [var_pos_q])
        q_trib1_trib = masc_res.get_series(5, x_trib_p, [var_pos_q])

        # =====================================================================
        # DISPLAY
        # =====================================================================
        fig, axe = plt.subplots(figsize=(6, 5))
        plot1d(axe, x_masc, zb_masc,
               plot_label='$Z_{bottom}$',
               x_label='$X$ [m]',
               y_label='$Z_{maxe}$ [m]',
               linestyle='-', color='k',)
        plot1d(axe, x_masc, zmax_masc,
               plot_label='$Z_{maxe}$',
               x_label='$X$ [m]',
               y_label='$Z_{maxe}$ [m]',
               linestyle='--', color='b',)
        axe.legend()
        axe.set_xlim(min(x_masc), 100000)
        plt.tight_layout()
        plt.savefig(path.join('.', 'img', 'zmax.png'))
        plt.close(fig)

        fig, axe = plt.subplots(figsize=(6, 5))
        plot1d(axe, x_masc, zb_masc,
               plot_label='$Z_{bottom}$',
               x_label='$X$ [m]',
               y_label='$Z_{maxe}$ [m]',
               linestyle='-', color='k',)
        plot1d(axe, x_masc, z1_masc,
               plot_label='$Z$ (t = 1 s)',
               x_label='$X$ [m]',
               y_label='$Z$ [m]',
               linestyle='--', color='b',)
        plot1d(axe, x_masc, z3_masc,
               plot_label='$Z$ (t = 3025 s)',
               x_label='$X$ [m]',
               y_label='$Z$ [m]',
               linestyle='-.', color='r',)
        plot1d(axe, x_masc, z6_masc,
               plot_label='$Z$ (t = 6131 s)',
               x_label='$X$ [m]',
               y_label='$Z$ [m]',
               linestyle=':', color='g',)
        plot1d(axe, x_masc, z9_masc,
               plot_label='$Z$ (t = 9245 s)',
               x_label='$X$ [m]',
               y_label='$Z$ [m]',
               linestyle='--', color='m',)
        axe.set_xlim(min(x_masc), 100000)
        axe.legend()
        plt.tight_layout()
        plt.savefig(path.join('.', 'img', 'z.png'))
        plt.close(fig)

        fig, axe = plt.subplots(figsize=(6, 5))
        plot1d(axe, t_masc, q_dam2_up,
               plot_label='Upstream dam 2',
               x_label='$T$ [s]',
               y_label='$Q$ [m3/s]',
               linestyle='-', color='b', marker='+')
        plot1d(axe, t_masc, q_dam2_down,
               plot_label='Downstream dam 2',
               x_label='$T$ [s]',
               y_label='$Q$ [m3/s]',
               linestyle='--', color='r', marker='o')
        axe.legend()
        plt.tight_layout()
        plt.savefig(path.join('.', 'img', 'q_dam2.png'))
        plt.close(fig)

        fig, axe = plt.subplots(figsize=(6, 5))
        plot1d(axe, t_masc, q_trib1_up,
               plot_label='Upstream',
               x_label='$T$ [s]',
               y_label='$Q$ [m3/s]',
               linestyle='-', color='b', marker='+')
        plot1d(axe, t_masc, q_trib1_down,
               plot_label='Downstream',
               x_label='$T$ [s]',
               y_label='$Q$ [m3/s]',
               linestyle='--', color='r', marker='o')
        plot1d(axe, t_masc, q_trib1_trib,
               plot_label='Tributary 1',
               x_label='$T$ [s]',
               y_label='$Q$ [m3/s]',
               linestyle=':', color='g', marker='s')
        plot1d(axe, t_masc, q_trib1_up-q_trib1_trib,
               plot_label='Upstream - Tributary',
               x_label='$T$ [s]',
               y_label='$Q$ [m3/s]',
               linestyle='-.', color='m', marker='s')
        axe.legend()
        plt.tight_layout()
        plt.savefig(path.join('.', 'img', 'q_trib.png'))
        plt.close(fig)
