\chapter{Dam Break}

%--------------------------------------------------------------------------------------------------
\section{Purpose}
This test case simulates the propagation of a flood wave triggered by the instantaneous failure of a dam in a real valley. The model includes a complex hydraulic network with three tributaries and three downstream dams, allowing for a comprehensive analysis of flood wave dynamics and dam break scenarios in a real-world context.

%--------------------------------------------------------------------------------------------------
\section{Description}
\subsection*{Geometry}
The modeled valley is $100~\mathrm{km}$ in length, and three tributaries, with respective lengths of $6.5~\mathrm{km}$, $4.5~\mathrm{km}$, and $3~\mathrm{km}$, are integrated into the system. Cross-sections for both the main valley and its tributaries are derived from IGN maps at a scale of $1/25000$, supplemented by data from the French Nivellement Général service. The average density of cross-sectional profiles is approximately two per kilometer, ensuring high geometric fidelity.

The model represents the hydraulic network as shown in Figure~\ref{mascaret:test23:geometry}, which includes the three tributaries and four dams (three modeled, and one that breaks instantaneously at the simulation start). The PK0 is located at the main dam.

\begin{figure}[H]
	\centering
	\includegraphicsmaybe{[width=\textwidth]}{../doc/cond_ini.eps}
	\caption{Diagram of the hydraulic network: main dam, three downstream dams, and three tributaries.}
	\label{mascaret:test23:geometry}
\end{figure}

 
Table~\ref{mascaret:test23:dams} provides details on the dams. The first dam, located at the upstream of the system, fails instantaneously at $t=0$. Dams 2 and 3 are modeled as breakage-resistant weirs.

\begin{table}[H]
	\begin{center}
		\caption{Characteristics of the dams}
		\label{mascaret:test23:dams}
		\begin{tabular}{|c|c|c|c|c|}
			\hline
			Dam & Position ($\mathrm{m}$) & Crest Elevation ($\mathrm{m NGF}$) & Discharge coefficient & Behaviour \\
			\hline
			Dam 1 & 26011 & 344 & 0.38 & Break instantly \\
			Dam 2 & 56318 & 264 & 0.38 & Breakage-resistant \\
			Dam 3 & 64143 & 195.04 & 0.38 & Breakage-resistant \\
			\hline
		\end{tabular}
	\end{center}
\end{table}

Dams 2 and 3 are represented using a free flow weir singularity:
\begin{align}
	Q=CLH\sqrt{2gH}
\end{align}
where $Q$ is the flow through the structure, $H$ is the upstream water head ($H=Z_\mathrm{upstream}-Z_\mathrm{weir}$), $g$ is gravitational acceleration, $L$ is the weir width, and $C$ is the discharge coefficient, set at $0.38$ for both dams. 

The tributaries join the main channel at $X=9.9~\mathrm{km}$, $X=36.2~\mathrm{km}$, and $X=63.06~\mathrm{km}$, respectively. The geometries of the confluences are detailed in Figure~\ref{mascaret:test23:confluence}. The transcritical kernel uses a 12-cell 2D representation.

\begin{figure}[H]
	\centering
	\includegraphicsmaybe{[width=\textwidth]}{../doc/geom_conflu.eps}
	\caption{Confluence geometries of the main channel and tributaries.}
	\label{mascaret:test23:confluence}
\end{figure}

\subsection*{Initial condition}
All reaches are initially dry, except for the reservoirs. No initial flow is present in the system.

\subsection*{Boundary condition}
Upstream : A time-varying hydrograph is imposed at each tributary inlet, $Q_{1,2,3} =f_{1,2,3}(t)~\mathrm{m^{3}.s^{-1}}$.\\
Free-flow boundary condition is applied at the downstream end of main channel (Reach~4).

\subsection*{Physical parameters}
Strickler coefficient: $K_m = 30~\mathrm{m^{1/3}.s^{-1}}$ for all reaches.\\
There is no friction on the vertical walls.


\subsection*{Numerical parameters}
The mesh is define with a uniform spatial step of $100~\mathrm{m}$.\\
The vertical discretization of the cross-section is uniform across the domain and set at $1.5~\mathrm{m}$.

For the transcritical kernel, the simulation is conducted with a variable time step governed by a target Courant number of $0.8$. The simulated duration is $9800~\mathrm{s}$.

The transcritical kernel is used in its explicit form.

%--------------------------------------------------------------------------------------------------
\section{Results}
The results of the simulation, presented in Figures~\ref{mascaret:test23:zmax} and \ref{mascaret:test23:z} showcase the maximum water surface elevation observed during the simulation, as well as snapshots of the water surface at different times. Notably, the first dam breaks instantly upon the initiation of the flood wave, while the downstream dams remain intact.

Figure~\ref{mascaret:test23:qdam2} highlights the evolution of flow rates at Dam 2. By comparing upstream and downstream flow rates, there is no significant reservoir effect, as expected due to the fully filled reservoir.

In Figure~\ref{mascaret:test23:q_trib}, the flow rates at the main channel upstream, downstream of the first tributary, and within the tributary are plotted. The confluence effectively maintains mass conservation throughout the simulation, demonstrating the robustness of the flow exchange between the main channel and the tributary. The initial flood wave generates a rise in the tributary flow, followed by a second peak as the tributary reservoir empties into the main channel.

\begin{figure}[H]
	\centering
	\includegraphicsmaybe{[width=0.75\textwidth]}{../img/zmax.png}
	\caption{Maximum water level reached along the main canal.}
	\label{mascaret:test23:zmax}
\end{figure}

\begin{figure}[H]
	\centering
	\includegraphicsmaybe{[width=0.75\textwidth]}{../img/z.png}
	\caption{Evolution of water levels along the main canal over time.}
	\label{mascaret:test23:z}
\end{figure}

\begin{figure}[H]
	\centering
	\includegraphicsmaybe{[width=0.75\textwidth]}{../img/q_dam2.png}
	\caption{Temporal evolution of the flow at the 2nd dam.}
	\label{mascaret:test23:qdam2}
\end{figure}

\begin{figure}[H]
	\centering
	\includegraphicsmaybe{[width=0.75\textwidth]}{../img/q_trib.png}
	\caption{Temporal evolution of the flow at the first tributary.}
	\label{mascaret:test23:q_trib}
\end{figure}

%--------------------------------------------------------------------------------------------------
\section*{Conclusion}
The dam break scenario modeled in this test case effectively simulates the propagation of a flood wave in a complex hydraulic network with multiple dams and tributaries. The results demonstrate the ability of the transcritical kernel to handle flow interactions, including dam breaches and confluence dynamics, with a high degree of accuracy in both water level predictions and mass conservation.
