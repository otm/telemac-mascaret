
"""
Validation script for test11
"""
from os import path
import matplotlib.pyplot as plt
from vvytel.vnv_study import AbstractVnvStudy
from postel.plot1d import plot1d


class VnvStudy(AbstractVnvStudy):
    """
    Class for validation
    """

    def _init(self):
        """
        Defines the general parameter
        """
        self.rank = 0
        self.tags = ['mascaret']

    def _pre(self):
        """
        Defining the studies
        """

        # Test11 permanent kernel
        self.add_study('vnv_1',
                       'mascaret',
                       'sarap.xcas')

        # Test11 unsteady subcritical kernel
        self.add_study('vnv_2',
                       'mascaret',
                       'rezo.xcas')

    def _check_results(self):
        """
        Comparison with reference file
        """
        self.check_epsilons('vnv_1:res',
                            'ref/sarap_ref.opt',
                            eps=[1.E-3],
                            masc=True)

        # Comparison with reference file
        self.check_epsilons('vnv_2:res',
                            'ref/rezo_ref.opt',
                            eps=[1.E-3],
                            masc=True)

    def _post(self):
        """
        Post-treatment processes
        """
        sarap_res, _ = self.get_study_res(
            'vnv_1:sarap_ecr.opt', 'mascaret')
        x_sarap = sarap_res.reaches[1].get_section_pk_list()
        var_pos = sarap_res.get_position_var("Cote de l eau")
        z_sarap = sarap_res.get_values_at_reach(-1, 1, [var_pos])
        var_pos = sarap_res.get_position_var('Debit mineur')
        q_minor_sarap = sarap_res.get_values_at_reach(-1, 1, [var_pos])
        var_pos = sarap_res.get_position_var('Debit majeur')
        q_major_sarap = sarap_res.get_values_at_reach(-1, 1, [var_pos])

        rezo_res, _ = self.get_study_res(
            'vnv_2:rezo_ecr.opt', 'mascaret')
        x_rezo = rezo_res.reaches[1].get_section_pk_list()
        var_pos = rezo_res.get_position_var("Cote de l eau")
        z_rezo = rezo_res.get_values_at_reach(-1, 1, [var_pos])
        var_pos = rezo_res.get_position_var('Debit mineur')
        q_minor_rezo = rezo_res.get_values_at_reach(-1, 1, [var_pos])
        var_pos = rezo_res.get_position_var('Debit majeur')
        q_major_rezo = rezo_res.get_values_at_reach(-1, 1, [var_pos])

        # =====================================================================
        # CALCUL DIF
        # =====================================================================
        diff_z = 100*(z_sarap - z_rezo)/z_sarap
        diff_q_minor = 100*(q_minor_sarap-q_minor_rezo)/q_minor_sarap
        diff_q_major = 100*(q_major_sarap-q_major_rezo)/q_major_sarap

        # =====================================================================
        # DISPLAY
        # =====================================================================
        fig, axe = plt.subplots(figsize=(6, 5))
        plot1d(axe, x_sarap, z_sarap,
               plot_label='SARAP',
               x_label='$X$ [m]',
               y_label='$Z$ [m]',
               linestyle='-', color='b',)
        plot1d(axe, x_rezo, z_rezo,
               plot_label='REZO',
               x_label='$X$ [m]',
               y_label='$Z$ [m]',
               linestyle='--', color='r',)
        axe.legend()
        plt.savefig(path.join('.', 'img', 'cote.png'))
        plt.close(fig)

        fig, axe = plt.subplots(figsize=(6, 5))
        plot1d(axe, x_sarap, q_minor_sarap+q_major_sarap,
               plot_label='SARAP',
               x_label='$X$ [m]',
               y_label='$Q_t$ [m³/s]',
               linestyle='-', color='b',)
        plot1d(axe, x_rezo, q_minor_rezo+q_major_rezo,
               plot_label='REZO',
               x_label='$X$ [m]',
               y_label='$Q_t$ [m³/s]',
               linestyle='--', color='r',)
        axe.legend()
        plt.tight_layout()
        plt.savefig(path.join('.', 'img', 'qt.png'))
        plt.close(fig)

        fig, axe = plt.subplots(figsize=(6, 5))
        plot1d(axe, x_sarap, q_minor_sarap,
               plot_label='SARAP',
               x_label='$X$ [m]',
               y_label='$Q_m$ [m³/s]',
               linestyle='-', color='b',)
        plot1d(axe, x_rezo, q_minor_rezo,
               plot_label='REZO',
               x_label='$X$ [m]',
               y_label='$Q_m$ [m³/s]',
               linestyle='--', color='r',)
        axe.legend()
        plt.tight_layout()
        plt.savefig(path.join('.', 'img', 'qm.png'))
        plt.close(fig)

        fig, axe = plt.subplots(figsize=(6, 5))
        plot1d(axe, x_sarap, q_major_sarap,
               plot_label='SARAP',
               x_label='$X$ [m]',
               y_label='$Q_M$ [m³/s]',
               linestyle='-', color='b',)
        plot1d(axe, x_rezo, q_major_rezo,
               plot_label='REZO',
               x_label='$X$ [m]',
               y_label='$Q_M$ [m³/s]',
               linestyle='--', color='r',)
        axe.legend()
        plt.tight_layout()
        plt.savefig(path.join('.', 'img', 'qM.png'))
        plt.close(fig)

        #  DIFF
        fig, axe = plt.subplots(figsize=(6, 5))
        plot1d(axe, x_sarap, diff_z,
               plot_label="Difference of water level",
               x_label='$X$ [m]',
               y_label='$\\epsilon$ [%]',
               linestyle='-', color='b',)
        plot1d(axe, x_sarap, diff_q_minor,
               plot_label="Difference of low flow discharge",
               x_label='$X$ [m]',
               y_label='$\\epsilon$ [%]',
               linestyle='--', color='r',)
        plot1d(axe, x_sarap, diff_q_major,
               plot_label="Difference of hight flow discharge",
               x_label='$X$ [m]',
               y_label='$\\epsilon$ [%]',
               linestyle=':', color='g',)
        axe.legend()
        plt.tight_layout()
        plt.savefig(path.join('.', 'img', 'difference.png'))
        plt.close(fig)
