\chapter{Propagation over a Dry Riverbed}

%--------------------------------------------------------------------------------------------------
\section{Purpose}
This test case examines the flow dynamics resulting from the sudden removal of a dam that separates a body of water at rest from a dry riverbed. At time $t = 0~\mathrm{s}$, the dam is removed, and the goal is to determine the resulting flow dynamics.

In a rectangular channel with a flat bottom and no friction, it is possible to determine the analytical solution to the problem, as presented by A. Ritter in his 1892 work, Die Fortpflanzung der Wasserwellen \footnote{A. Ritter, \emph{Die Fortplanzung der Wasserwellen}, Z Verdeut. Ing 36, 1892}. 
This case is especially valuable for validating the numerical scheme for wave propagation over a dry riverbed.

%--------------------------------------------------------------------------------------------------
\section{Description}

\subsection*{Geometry}

The simulation is performed in a flat, rectangular channel that is $5000~\mathrm{m}$ long, with a constant width of $1~\mathrm{m}$. 

\subsection*{Initial condition}
\begin{itemize}
	\item Water level is set to $Z = 6~\mathrm{m}$ for $x < 2000~\mathrm{m}$;
	\item Water level is set to $Z = 10^{-6}~\mathrm{m}$ for $x \geq 2000~\mathrm{m}$.
\end{itemize}

The downstream water level is assigned a small non-zero value instead of zero to prevent numerical issues.

\subsection*{Boundary condition}
Upstream: Zero discharge, $Q = 0~\mathrm{m^{3}.s^{-1}}$.\\
Downstream: No water, $\mathrm{Water~level} = 0~\mathrm{m}$.

\subsection*{Physical parameters}
No friction is considered in the model.

\subsection*{Numerical parameters}
The mesh is define with a uniform spatial step of $ 5~\mathrm{m}$.\\
The vertical discretization of the cross-section is uniform across the domain and set at $50~\mathrm{cm}$.

The time step is set to $0.08~\mathrm{s}$ for a simulation duration of $200~\mathrm{s}$, resulting in $2500$ steps.

The transcritical kernel is used in its explicit form.
%--------------------------------------------------------------------------------------------------
\section{Results}

Figures~\ref{mascaret:test20:cote} and \ref{mascaret:test20:q} present a comparison of the calculated water levels and discharge against the analytical solutions. The results show a good agreement, with the front region accurately represented. A minor shock is observed instead of a perfectly tangent water level to the bed elevation. Additionally, some diffusion is noted where the rarefaction wave transitions to the upstream state (elevation of $6~\mathrm{m}$).

\begin{figure}[H]
	\centering
	\includegraphicsmaybe{[width=0.75\textwidth]}{../img/cote.png}
	\caption{Comparison of water levels.}
	\label{mascaret:test20:cote}
\end{figure}

\begin{figure}[H]
	\centering
	\includegraphicsmaybe{[width=0.75\textwidth]}{../img/q.png}
	\caption{Comparison of flow discharges.}
	\label{mascaret:test20:q}
\end{figure}

%--------------------------------------------------------------------------------------------------
\section*{Conclusion}
The simulation of wave propagation over a dry riverbed using the transcritical kernel has been successfully validated against the analytical solution. These results confirm the model's capability to accurately simulate wave behavior in scenarios involving dry beds.
