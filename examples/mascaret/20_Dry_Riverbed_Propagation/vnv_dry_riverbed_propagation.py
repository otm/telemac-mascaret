
"""
Validation script for test20
"""
from os import path

import matplotlib.pyplot as plt
import numpy as np

from postel.plot1d import plot1d
from vvytel.vnv_study import AbstractVnvStudy


class VnvStudy(AbstractVnvStudy):
    """
    Class for validation
    """

    def _init(self):
        """
        Defines the general parameter
        """
        self.rank = 0
        self.tags = ['mascaret']

    def _pre(self):
        """
        Defining the studies
        """

        # Test20 transcritical kernel
        self.add_study('vnv_1',
                       'mascaret',
                       'mascaret.xcas')

    def _check_results(self):
        """
        Comparison with reference file
        """
        self.check_epsilons('vnv_1:res',
                            'ref/mascaret.rub',
                            eps=[1.E-3],
                            masc=True)

    def _post(self):
        """
        Post-treatment processes
        """

        masc_res, _ = self.get_study_res(
            'vnv_1:mascaret.rub', 'mascaret')

        x_masc = masc_res.reaches[1].get_section_pk_list()
        var_pos = masc_res.get_position_var("Cote de l eau")
        cote_masc = masc_res.get_values_at_reach(-1, 1, [var_pos])
        var_pos = masc_res.get_position_var("Debit total")
        q_masc = masc_res.get_values_at_reach(-1, 1, [var_pos])

        # =====================================================================
        # Load analytical data
        # =====================================================================
        data = np.loadtxt('ref/res.txt')
        x_analytical = data[:, 0]
        cote_analytical = data[:, 1]
        q_analytical = data[:, 2]

        # =====================================================================
        # DISPLAY
        # =====================================================================
        fig, axe = plt.subplots(figsize=(6, 5))
        plot1d(axe, x_analytical, cote_analytical,
               plot_label='Analytical result',
               x_label='Abscissae [m]',
               y_label='Water level [m]',
               linestyle='-', linewidth=3,
               marker='None',
               color='k',
               )
        plot1d(axe, x_masc, cote_masc,
               plot_label='Transcritical Explicit (Nc=0.8)',
               x_label='Abscissae [m]',
               y_label='Water level [m]',
               linestyle='--', linewidth=3,
               marker='None',
               color='r',
               )
        axe.legend()
        plt.tight_layout()
        plt.savefig(path.join('.', 'img', 'cote.png'))
        plt.close(fig)

        fig, axe = plt.subplots(figsize=(6, 5))
        plot1d(axe, x_analytical, q_analytical,
               plot_label='Analytical result',
               x_label='Abscissae [m]',
               y_label='Q [m3/s]',
               linestyle='-', linewidth=3,
               marker='None',
               color='k',
               )
        plot1d(axe, x_masc, q_masc,
               plot_label='Transcritical Explicit (Nc=0.8)',
               x_label='Abscissae [m]',
               y_label='Q [m3/s]',
               linestyle='--', linewidth=3,
               marker='None',
               color='r',
               )
        axe.legend()
        plt.tight_layout()
        plt.savefig(path.join('.', 'img', 'q.png'))
        plt.close(fig)
