
"""
Validation script for test10
"""
from os import path

import matplotlib.pyplot as plt
import numpy as np

from postel.plot1d import plot1d
from vvytel.vnv_study import AbstractVnvStudy


class VnvStudy(AbstractVnvStudy):
    """
    Class for validation
    """

    def _init(self):
        """
        Defines the general parameter
        """
        self.rank = 0
        self.tags = ['mascaret']

    def _pre(self):
        """
        Defining the studies
        """

        # Test10 permanent kernel
        self.add_study('vnv_1',
                       'mascaret',
                       'sarap.xcas')

        # Test10 transcritical kernel
        self.add_study('vnv_2',
                       'mascaret',
                       'mascaret.xcas')

    def _check_results(self):
        """
        Comparison with reference file
        """
        self.check_epsilons('vnv_1:res',
                            'ref/sarap_ref.opt',
                            eps=[1.E-3],
                            masc=True)

        self.check_epsilons('vnv_2:res',
                            'ref/mascaret_ref.opt',
                            eps=[1.E-3],
                            masc=True)

    def _post(self):
        """
        Post-treatment processes
        """

        sarap_res, _ = self.get_study_res(
            'vnv_1:sarap_ecr.opt', 'mascaret')
        x_sarap = sarap_res.reaches[1].get_section_pk_list()
        var_pos = sarap_res.get_position_var("Cote de l eau")
        cote_sarap = sarap_res.get_values_at_reach(-1, 1, [var_pos])
        index_weir = np.argmax(x_sarap >= 4000)
        zup_sarap = sarap_res.get_values_at_reach(-1, 1, [var_pos])[index_weir]
        var_pos = sarap_res.get_position_var("Debit total")
        q_sarap = sarap_res.get_values_at_reach(-1, 1, [var_pos])[index_weir]

        mascaret_res, _ = self.get_study_res(
            'vnv_2:mascaret_ecr.opt', 'mascaret')
        x_mascaret = mascaret_res.reaches[1].get_section_pk_list()
        var_pos = mascaret_res.get_position_var("Cote de l eau")
        cote_mascaret = mascaret_res.get_values_at_reach(-1, 1, [var_pos])
        index_weir = np.argmax(x_mascaret >= 4000)
        zup_mascaret = mascaret_res.get_values_at_reach(-1, 1, [var_pos])[
            index_weir]
        var_pos = mascaret_res.get_position_var("Debit total")
        q_mascaret = mascaret_res.get_values_at_reach(-1, 1, [var_pos])[index_weir]
        # =====================================================================
        # Compute analytical data
        # =====================================================================
        x_uniform = [0, 5000]
        z_uniform = [15, 12.5]
        q_up = 1000
        z_up = 13.5

        # =====================================================================
        # TABLE
        # =====================================================================

        with open(path.join('.', 'img', 'comparaison.txt'), 'w') as file:
            # Write header
            file.write(
                " & Analytic solution & SARAP & Transcritical \\\\\n")
            file.write("\\hline\n")
            file.write(
                f"$Q$ Upstream weir [m3/s]  & {q_up:.3f} & {q_sarap[0]:.3f} & {q_mascaret[0]:.3f} \\\\\n")
            file.write(
                f"$Z$ Upstream weir [m] & {z_up:.3f} & {zup_sarap[0]:.3f} & {zup_mascaret[0]:.3f}")

        # =====================================================================
        # DISPLAY
        # =====================================================================
        fig, axe = plt.subplots(figsize=(6, 5))
        plot1d(axe, x_uniform, z_uniform,
               plot_label='Uniform flow',
               x_label='Abscissae [m]',
               y_label='Water level [m]',
               linestyle='-.', linewidth=2,
               marker='None',
               color='g',
               )
        plot1d(axe, x_sarap, cote_sarap,
               plot_label='SARAP',
               x_label='Abscissae [m]',
               y_label='Water level [m]',
               linestyle='--', linewidth=3,
               marker='None',
               color='r',
               )
        plot1d(axe, x_mascaret, cote_mascaret,
               plot_label='Transcritical',
               x_label='Abscissae [m]',
               y_label='Water level [m]',
               linestyle=':', linewidth=3,
               marker='None',
               color='b',
               )
        axe.legend()
        plt.tight_layout()
        plt.savefig(path.join('.', 'img', 'cote.png'))
        plt.close(fig)
