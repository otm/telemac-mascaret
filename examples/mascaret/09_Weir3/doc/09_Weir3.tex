\chapter{Weir with a crest profile}
%--------------------------------------------------------------------------------------------------
\section{Purpose}
This test case aims to validate both the steady-state SARAP kernel and the unsteady REZO kernel for a rectangular channel incorporating a weir defined by a crest profile. For the unsteady kernel REZO, the goal is to confirm that with a constant inflow discharge, the computed hydraulic variables (velocity and water depths) converge to the values predicted by the steady-state kernel SARAP. 

%--------------------------------------------------------------------------------------------------
\section{Description}

\subsection*{Geometry}
The simulation is conducted in a rectangular channel with a length of $5000~\mathrm{m}$, a constant width of $100~\mathrm{m}$ and a constant slope of $0.0005$.\\
The geometry is defined by 2 cross section located at $X = 0~\mathrm{m}$ and $X = 5000~\mathrm{m}$.\\
 The weir is positioned immediately downstream of the point with an abscissa of $X = 4000~\mathrm{m}$.

\subsection*{Initial condition}
No initial conditions are required for the steady-state kernel SARAP.\\
For the unsteady REZO kernel, the initial water surface profile is derived from a steady-state simulation that does not include the weir.

\subsection*{Boundary condition}
Upstream: Constant discharge, $Q = 1000~\mathrm{m^{3}.s^{-1}}$.\\
Downstream: Constant water level, $\mathrm{Water~level} = 12.5~\mathrm{m}$.

\subsection*{Physical parameters}
he friction coefficient is set to achieve a uniform flow depth of $5~\mathrm{m}$.\\
Strickler coefficient: $K_m = 30.59~\mathrm{m^{1/3}.s^{-1}}$.\\
There is no friction on the vertical walls.

\subsection*{Weir}
The cross-sectional profile of the weir crest is defined by points $(Y,Z)$. The discharge law for the segment $[Y, Y+\Delta Y]$ is given by:

\begin{align}
	\Delta Q = C \mu H(Y) \Delta Y \sqrt{2gH(Y)}
\end{align}

where $H(Y)$ is the specific head at the lateral coordinate $Y$, $\Delta Y$ is a width element, $\mu$ is the discharge coefficient, and $C$ is a coefficient defined by

\begin{subequations}
	\begin{align}
		C=1 \quad \mathrm{if} \quad RH<0.8\\
		C=-25RH^2+40RH-15 \quad \mathrm{if} \quad 0.8<RH<1
	\end{align}
\end{subequations}

With $RH=H_\mathrm{downstream}/H_\mathrm{upstream}$, $H_\mathrm{upstream}$ et $H_\mathrm{downstream}$ are the specific heads (relative to the weir crest).\\
The total discharge flowing over the weir is obtained by integrating this equation across the entire crest of the weir.


The crest elevation of the weir is : $10.5~\mathrm{m}$.\\
Weir discharge coefficient $\mu: 0.435$.

The weir equation is provided in Table\ref{mascaret:test9:weir}.

\begin{table}[H]
	\begin{center}
		\caption{Weir law.}
		\label{mascaret:test9:weir}
		\begin{tabular}{|c|c|}
			\hline Width $Y~\mathrm{(m)}$) & Upstream water level ($\mathrm{m}$)\\ 
			\hline $0$ & $10.2$\\ 
			\hline $100$ & $10.8$\\ 
			\hline 
		\end{tabular} 
	\end{center}
\end{table}

\subsection*{Numerical parameters}
The mesh is define with a uniform spatial step of $100~\mathrm{m}$.\\
The vertical discretization of the cross-section is uniform across the domain and set at $0.25~\mathrm{m}$.

No time step parameters for the steady kernel (not requested).\\
The unsteady REZO kernel uses a simulation duration of $1000$ time steps with a time step of $10~\mathrm{s}$.

%--------------------------------------------------------------------------------------------------
\section{Results}

\subsection*{Analytic Solution}
By enforcing the downstream water depth in a uniformly shaped channel, the flow remains uniform across the channel up to the downstream section of the weir. In the upstream section, the flow discharge and the corresponding upstream water level should match the values dictated by the weir flow equation. Further upstream, the flow should asymptotically approach uniform flow conditions.

In this test case, the upstream water level should be $Z_\mathrm{upstream} = 13.509~\mathrm{m}$ according to the applied weir equation, since the water level immediately downstream of the weir is $Z = 12.95~\mathrm{m}$.

\subsection*{Validation}
Figure~\ref{mascaret:test9:cote} shows the comparison between the water surface profiles obtained from the steady-state SARAP and unsteady REZO kernels. The differences between the two profiles are negligible. The water levels calculated upstream of the weir are consistent with theoretical predictions (see Table~\ref{mascaret:test9:comparaison}). 

\begin{table}[H]
	\begin{center}
		\caption{Comparison of computed and theoretical water level near the weir.}
		\label{mascaret:test9:comparaison}
		\begin{tabular}{c|c|c|c}
			\toprule
			\InputIfFileExists{../img/comparaison.txt}{}{}{}{}{}\\
			\bottomrule
		\end{tabular}
	\end{center}
\end{table}

\begin{figure}[H]
	\centering
	\includegraphicsmaybe{[width=0.75\textwidth]}{../img/cote.png}
	\caption{Comparison of computed water level for the two kernel.}
	\label{mascaret:test9:cote}
\end{figure}

%--------------------------------------------------------------------------------------------------
\section*{Conclusion}
The application of a weir defined by a crest profile proves to be effective. Additionally, there is no discrepancy between different kernel versions and theoretical results, validating the steady-state SARAP kernel for this scenario. Furthermore, the unsteady REZO kernel provides identical results, further confirming its accuracy.