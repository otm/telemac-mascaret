
"""
Validation script for test24
"""
from os import path
import numpy as np
import matplotlib.pyplot as plt
from vvytel.vnv_study import AbstractVnvStudy
from postel.plot1d import plot1d


class VnvStudy(AbstractVnvStudy):
    """
    Class for validation
    """

    def _init(self):
        """
        Defines the general parameter
        """
        self.rank = 1
        self.tags = ['mascaret']

    def _pre(self):
        """
        Defining the studies
        """

        # Test24 transcritical implicit kernel
        self.add_study('vnv_1',
                       'mascaret',
                       'mascaret_imp.xcas')

        # Test24 transcritical explicit kernel
        self.add_study('vnv_2',
                       'mascaret',
                       'mascaret_exp.xcas')

    def _check_results(self):
        """
        Comparison with reference file
        """
        self.check_epsilons('vnv_1:res',
                            'ref/mascaret_imp_ref.opt',
                            eps=[1.E-1],
                            var1='Cote de l eau',
                            var2='Debit total',
                            masc=True)

        self.check_epsilons('vnv_2:res',
                            'ref/mascaret_exp_ref.opt',
                            eps=[1.E-1],
                            var1='Cote de l eau',
                            var2='Debit total',
                            masc=True)

    def _post(self):
        """
        Post-treatment processes
        """
        seuil = 0.5

        masc_imp_res, _ = self.get_study_res(
            'vnv_1:mascaret_imp_ecr.opt', 'mascaret')
        x_imp = masc_imp_res.reaches[1].get_section_pk_list()
        t_imp = np.array(masc_imp_res.times)
        var_posc = masc_imp_res.get_position_var("Cote de l eau")
        var_posh = masc_imp_res.get_position_var("Hauteur d'eau")
        cote_imp = []
        h_imp = []
        for i in range(masc_imp_res.ntimestep):
            cote_imp.append(masc_imp_res.get_values_at_reach(i, 1, [var_posc]))
            h_imp.append(masc_imp_res.get_values_at_reach(i, 1, [var_posh]))
        cote_imp = np.array(cote_imp).squeeze()
        h_imp = np.array(h_imp).squeeze()

        masc_exp_res, _ = self.get_study_res(
            'vnv_2:mascaret_exp_ecr.opt', 'mascaret')
        x_exp = masc_exp_res.reaches[1].get_section_pk_list()
        t_exp = np.array(masc_exp_res.times)
        var_posc = masc_exp_res.get_position_var("Cote de l eau")
        var_posh = masc_exp_res.get_position_var("Hauteur d'eau")
        cote_exp = []
        h_exp = []
        for i in range(masc_exp_res.ntimestep):
            cote_exp.append(masc_exp_res.get_values_at_reach(i, 1, [var_posc]))
            h_exp.append(masc_exp_res.get_values_at_reach(i, 1, [var_posh]))
        cote_exp = np.array(cote_exp).squeeze()
        h_exp = np.array(h_exp).squeeze()

        # =====================================================================
        #  CALCUL
        # =====================================================================
        cotemax_imp = np.max(cote_imp, axis=0)
        t_max_imp = t_imp[np.argmax(cote_imp, axis=0)]
        t_propagation_imp = t_imp[np.argmax(h_imp >= seuil, axis=0)]

        cotemax_exp = np.max(cote_exp, axis=0)
        t_max_exp = t_exp[np.argmax(cote_exp, axis=0)]
        t_propagation_exp = t_exp[np.argmax(h_exp >= seuil, axis=0)]

        # =====================================================================
        # TABLE FIELD
        # =====================================================================
        transformateur_label = ['A (1400 m)', 'B (9200 m)', 'C (10450 m)']
        transformateur_time = [100, 1240, 1420]

        with open(path.join('.', 'img', 'comparaison_field_exp.txt'), 'w') as file:
            # Write header
            file.write("""Transformer &
                       Failure Time &
                       Simùulation Time  &
                       Time to max elevation """)

            # Write Data
            for i, label in enumerate(transformateur_label):
                file.write(f"""\\\\\n \\hline {label} &
                           {transformateur_time[i]:.1f} s &
                           {t_propagation_exp[i]:.1f} s &
                           {t_max_exp[i]:.1f} s""")

        with open(path.join('.', 'img', 'comparaison_field_imp.txt'), 'w') as file:
            # Write header
            file.write("""Transformer &
                       Failure Time &
                       Simulation Time &
                       Time to max elevation""")
            # Write Data
            for i, label in enumerate(transformateur_label):
                file.write(f"""\\\\\n \\hline {label} &
                           {transformateur_time[i]:.1f} s &
                           {t_propagation_imp[i]:.1f} s &
                           {t_max_imp[i]:.1f} s""")

        # =====================================================================
        # TABLE MODEL
        # =====================================================================
        sonde_label = ['S6 (290 m)', 'S7 (1600 m)', 'S8 (2930 m)',
                       'S9 (3830 m)', 'S10 (5410 m)', 'S11 (6720 m)',
                       'S12 (8210 m)', 'S13 (9005 m)', 'S14 (10100 m)']
        sonde_x = [290, 1600, 2930, 3830, 5410, 6720, 8210, 9005, 10100]
        sonde_time = [10.2, 102, 182, 263, 404, 600, 845, 972, 1139, ]
        sonde_cote = [84.2, 56.84, 54, 40.2, 34.9, 27.4, 21.5, 16.1, 12.9]

        with open(path.join('.', 'img', 'comparaison_model_exp.txt'), 'w') as file:
            # Write header
            file.write("""Probes &
                       Propagation &
                       Propagation &
                       Maximum water &
                       Maximum water \\\\\n""")
            file.write(""" & Time Model &
                       Time Mascaret &
                       level Model &
                       level Mascaret""")

            # Write Data
            for i, label in enumerate(sonde_label):
                file.write(f"""\\\\\n \\hline {label} &
                           {sonde_time[i]:.1f} s &
                           {t_propagation_exp[i+3]:.1f} s &
                           {sonde_cote[i]:.2f} m &
                           {cotemax_exp[i+3]:.2f} m""")

        with open(path.join('.', 'img', 'comparaison_model_imp.txt'), 'w') as file:
            # Write header
            file.write("""Probes & Propagation &
                       Propagation &
                       Maximum water &
                       Maximum water \\\\\n""")
            file.write(""" (localisation) &
                       Time Model &
                       Time Mascaret &
                       level Model & level Mascaret""")

            # Write Data
            for i, label in enumerate(sonde_label):
                file.write(f"""\\\\\n \\hline {label} &
                           {sonde_time[i]:.1f} s &
                           {t_propagation_imp[i+3]:.1f} s &
                           {sonde_cote[i]:.2f} m &
                           {cotemax_imp[i+3]:.2f} m""")

        # =====================================================================
        #         DISPLAY
        # =====================================================================

        fig, axe = plt.subplots(figsize=(6, 5))
        plot1d(axe, sonde_x, sonde_cote,
               plot_label='Physical Model',
               x_label='Abscissae [m]',
               y_label='Water level [m]',
               linestyle='-', color='k',
               marker='o')
        plot1d(axe, x_exp[3:], cotemax_exp[3:],
               plot_label='Transcritical Explicit (Nc=0.8)',
               x_label='Abscissae [m]',
               y_label='Water level [m]',
               linestyle='-.', color='r',
               marker='s')
        plot1d(axe, x_imp[3:], cotemax_imp[3:],
               plot_label='Transcritical Implicit (Nc=2)',
               x_label='Abscissae [m]',
               y_label='Water level [m]',
               linestyle=':', color='b',
               marker='+')

        axe.legend()
        plt.tight_layout()
        plt.savefig(path.join('.', 'img', 'cote.png'))
        plt.close(fig)
