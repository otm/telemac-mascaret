
"""
Validation script for test6
"""
from os import path
import matplotlib.pyplot as plt
from vvytel.vnv_study import AbstractVnvStudy
from postel.plot1d import plot1d


class VnvStudy(AbstractVnvStudy):
    """
    Class for validation
    """

    def _init(self):
        """
        Defines the general parameter
        """
        self.rank = 0
        self.tags = ['mascaret']

    def _pre(self):
        """
        Defining the studies
        """

        # Test6 transcritical kernel, large mesh
        self.add_study('vnv_1',
                       'mascaret',
                       'mascaret_large.xcas')

        # Test6 transcritical kernel, fine mesh
        self.add_study('vnv_2',
                       'mascaret',
                       'mascaret_fin.xcas')

    def _check_results(self):
        """
        Comparison with reference file
        """
        self.check_epsilons('vnv_1:res',
                            'ref/mascaret_large_ref.opt',
                            eps=[1.E-3],
                            masc=True)

        self.check_epsilons('vnv_2:res',
                            'ref/mascaret_fin_ref.opt',
                            eps=[1.E-3],
                            masc=True)

    def _post(self):
        """
        Post-treatment processes
        """

        # Getting results from mascaret large files
        large_res, _ = self.get_study_res(
            'vnv_1:mascaret_large_ecr.opt', 'mascaret')
        x_large = large_res.reaches[1].get_section_pk_list()
        var_pos = large_res.get_position_var("Cote de l eau")
        z_large = large_res.get_values_at_reach(-1, 1, [var_pos])
        var_pos = large_res.get_position_var('Debit mineur')
        q_large = large_res.get_values_at_reach(-1, 1, [var_pos])

        # Getting results from mascaret large files
        fin_res, _ = self.get_study_res(
            'vnv_1:mascaret_large_ecr.opt', 'mascaret')
        x_fin = fin_res.reaches[1].get_section_pk_list()
        var_pos = fin_res.get_position_var("Cote de l eau")
        z_fin = fin_res.get_values_at_reach(-1, 1, [var_pos])
        var_pos = fin_res.get_position_var('Debit mineur')
        q_fin = fin_res.get_values_at_reach(-1, 1, [var_pos])

        # =====================================================================
        # DISPLAY
        # =====================================================================
        fig, axe = plt.subplots(figsize=(6, 5))
        plot1d(axe, x_large, z_large,
               plot_label='Coarse mesh',
               x_label='$X$ [m]',
               y_label='$Z$ [m]',
               linestyle='-', linewidth=2, color='b',
               )
        plot1d(axe, x_fin, z_fin,
               plot_label='Fine mesh',
               x_label='$X$ [m]',
               y_label='$Z$ [m]',
               linestyle='--', linewidth=2, color='r',
               )
        axe.legend()
        plt.tight_layout()
        plt.savefig(path.join('.', 'img', 'cote.png'))
        plt.close(fig)

        fig, axe = plt.subplots(figsize=(6, 5))
        plot1d(axe, x_large, q_large,
               plot_label='Coarse mesh',
               x_label='$X$ [m]',
               y_label='$Q$ [m³/s]',
               linestyle='-', linewidth=2, color='b',
               )
        plot1d(axe, x_fin, q_fin,
               plot_label='Fine mesh',
               x_label='$X$ [m]',
               y_label='$Q$ [m³/s]',
               linestyle='--', linewidth=2, color='r',
               )
        axe.set_ylim(-1E-11, 1E-11)
        axe.legend()
        plt.tight_layout()
        plt.savefig(path.join('.', 'img', 'qm.png'))
        plt.close(fig)
