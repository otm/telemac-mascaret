\chapter{Inflow discharge}

%--------------------------------------------------------------------------------------------------
\section{Purpose}
This test shows the capability of \mascaret kernels to well compute a steady state in the case of a simple rectangular channel with inflow discharge.
A comparison of the result of the steady kernel and the converged results of the transcritical kernel with explicit form is also done with the analytical solution.

%--------------------------------------------------------------------------------------------------
\section{Description}
\subsection*{Geometry}
The domain is a rectangular channel of $4990~\mathrm{m}$ long and $1~\mathrm{m}$ width without slope. The geometry is described by 2 cross section located at $X = 0~\mathrm{m}$ and $X = 4990~\mathrm{m}$.\\
The inflow discharge is located at $X = 2500~\mathrm{m}$.

\subsection*{Initial condition}
No initial condition for the steady kernel (not requested).\\
Constant discharge equal to $1~\mathrm{m^{3}.s^{-1}}$ and water level linearly varying from $1.5~\mathrm{m}$ to $1~\mathrm{m}$ for the transient kernels.

\subsection*{Boundary condition}
Upstream: Constant discharge, $Q = 1~\mathrm{m^{3}.s^{-1}}$. \\
Downstream: Constant water level, $H = 1~\mathrm{m}$.

\subsection*{Physical parameters}
Strickler coefficient: $K_m = 90~\mathrm{m^{1/3}.s^{-1}}$. There is no friction on the vertical walls. \\
Inflow discharge : $q_l = 1~\mathrm{m^{3}.s^{-1}}$.

\subsection*{Numerical parameters}
The mesh is define with a uniform spatial step of $10~\mathrm{m}$.\\
The vertical discretization of the cross-section is uniform across the domain and set at $10~\mathrm{m}$.

No time step parameters for the steady kernel (not requested).\\
For the transcritical kernel : the simulation duration is set to $5000$ time steps. The time step is variable with a wishes Courant number of $0.8$. 

The transcritical kernel is used in its explicit form.

%--------------------------------------------------------------------------------------------------
\section{Results}

\subsection*{Analytic solution}
The conservation of momentum equation for this scenario is given by:

\begin{align}
	\frac{\partial}{\partial x}\left( \frac{Q^2}{Lh}+\frac{1}{2}gLh^2\right)= \gamma_\mathrm{inflow}
\end{align}

where $Q$ is the total discharge, $L$ the channel's width and $h$ the water depth.

Given that \mascaret does not account for the inflow term in the momentum equation, the equation simplifies by neglecting the right-hand side term (meaning momentum is not conserved). Thus, at the inflow position:

\begin{align}
	\left[ \frac{2}{L \left(h_1+h_2 \right)} \times \left({Q_1}^2-{Q_2}^2\right) \right] +\left[ \frac{\left(Q_1+Q_2\right)^2}{L\left(h_1+h_2\right)^2}+ gL \times \frac{h_1+h_2}{2}\right] \times \left(h_2-h_1\right)=0
\end{align}
Where index $1$ corresponds to the upstream section of the inflow andindex $2$ corresponds to the downstream section of the inflow.

The water depth $h_2$ is calculated using the Runge-Kutta (RK4) method.

Numerical application with:
\begin{subequations}
	\begin{align}
		Q_{1} = 1~\mathrm{m^{3}.s^{-1}} \\
		Q_{2} = 2~\mathrm{m^{3}.s^{-1}} \\
		h_{2} = 1.580~\mathrm{m}
	\end{align}
\end{subequations}
The calculated head loss at the inflow is $\Delta h_\mathrm{analytical} = 0.118~\mathrm{m}$ with $h_{1} = 1.709~\mathrm{m}$.
This head loss can be recalculated using the downstream water level obtained by \mascaret.

\subsection*{Validation}
Figure \ref{mascaret:test5:depth} compares the water level profiles obtained with the steady-state kernel SARAP and the transient supercritical kernel. Both kernels produce nearly identical results, confirming the accuracy of the simulation.

\begin{figure}[H]
	\centering
	\includegraphicsmaybe{[width=0.75\textwidth]}{../img/depth.png}
	\caption{Comparison of analytic and computed water depth.}
	\label{mascaret:test5:depth}
\end{figure}

The head loss at the inflow calculated by \mascaret is slightly higher than the analytical result, as shown in Table \ref{mascaret:test5:head_loss}.

\begin{table}[H]
\centering
	\caption{Numerical comparison of analytic and computed head loss at the inflow. The analytical solution corresponds to a solution obtained with $h_2$ approximated by the RK4 method downstream. The corrected analytical solution corresponds to a head loss obtained with imposed $h_2$.}
	\label{mascaret:test5:head_loss}
	\begin{tabular}{l|c|c|c}
		\toprule
		\InputIfFileExists{../img/table.txt}{}{}\\
		\bottomrule
	\end{tabular}
\end{table}

%--------------------------------------------------------------------------------------------------
\section*{Conclusion}
The results indicate that the treatment of inflow discharge by \mascaret is correct, as evidenced by the close agreement between the computed and analytical solutions. The kernels effectively simulate the flow dynamics and accurately capture the head loss at the inflow location.
