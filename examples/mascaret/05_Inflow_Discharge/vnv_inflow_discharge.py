"""
Validation script for test5
"""
from os import path
import matplotlib.pyplot as plt
import numpy as np
from scipy.optimize import fsolve
from postel.plot1d import plot1d
from vvytel.vnv_study import AbstractVnvStudy


def apport(h2):

    # DATA
    gravity = 9.81  # m /s^2
    width = 1.  # m
    flow_1 = 1.  # m^3/s
    q_inflow = 1.  # m^3/s
    flow_2 = flow_1+q_inflow

    def equation(h1):
        flow = (flow_1+flow_2)/2
        depth = (h1+h2)/2
        dh = (h2-h1)
        dq = (flow_2-flow_1)
        term1 = 2*flow*depth*dq
        term2 = -flow**2*dh
        term3 = gravity*width**2*depth**3*dh

        return term1 + term2 + term3

    h1 = fsolve(equation, h2)[0]
    return h1


def analytic_solution():

    def f(y0, q, strickler, gravity, width):
        fy = gravity*q**2/strickler**2 * 1 / \
            (q**2*y0**(1/3)-gravity*width**2*y0**(10/3))
        return fy

    # DATA
    gravity = 9.81  # m /s^2
    strickler = 90.  # m^1/3/s
    width = 1.  # m
    length = 4990.  # m
    x_inflow = 2500.  # m
    h_downstream = 1.  # m
    flow_1 = 1.  # m^3/s
    q_inflow = 1.  # m^3/s

    flow_2 = flow_1+q_inflow  # m^3/s
    dx = 0.1  # m

    # =============================================================================
    # ANALYTIC RK4
    # =============================================================================
    # DOWNSTREAM
    y0 = h_downstream

    x_analytic_downstream = np.arange(length, x_inflow-dx, -dx)
    y_analytic_downstream = np.zeros_like(x_analytic_downstream)
    y_analytic_downstream[0] = y0

    for i in range(len(x_analytic_downstream) - 1):
        k1 = f(y0, flow_2, strickler, gravity, width)
        k2 = f(y0 - dx * 0.5 * k1,  flow_2, strickler, gravity, width)
        k3 = f(y0 - dx * 0.5 * k2,  flow_2, strickler, gravity, width)
        k4 = f(y0 - dx * k3,  flow_2, strickler, gravity, width)

        y1 = y0 - (dx / 6.0) * (k1 + 2. * k2 + 2. * k3 + k4)
        y_analytic_downstream[i + 1] = y1
        y0 = y1

    # INFLOW
    h2 = y_analytic_downstream[-1]  # m
    h1 = apport(h2)
    # UPSTREAM
    y0 = h1

    x_analytic_upstream = np.arange(x_inflow-dx, -dx, -dx)
    y_analytic_upstream = np.zeros_like(x_analytic_upstream)
    y_analytic_upstream[0] = y0

    for i in range(len(y_analytic_upstream) - 1):
        k1 = f(y0, flow_1, strickler, gravity, width)
        k2 = f(y0 - dx * 0.5 * k1,  flow_1, strickler, gravity, width)
        k3 = f(y0 - dx * 0.5 * k2,  flow_1, strickler, gravity, width)
        k4 = f(y0 - dx * k3,  flow_1, strickler, gravity, width)

        y1 = y0 - (dx / 6.0) * (k1 + 2. * k2 + 2. * k3 + k4)
        y_analytic_upstream[i + 1] = y1
        y0 = y1

    x_analytic = np.flip(np.concatenate(
        (x_analytic_downstream, x_analytic_upstream)))
    y_analytic = np.flip(np.concatenate(
        (y_analytic_downstream, y_analytic_upstream)))

    return x_analytic, y_analytic, h1, h2


class VnvStudy(AbstractVnvStudy):
    """
    Class for validation
    """

    def _init(self):
        """
        Defines the general parameter
        """
        self.rank = 0
        self.tags = ['mascaret']

    def _pre(self):
        """
        Defining the studies
        """

        # Test5 permanent kernel
        self.add_study('vnv_1',
                       'mascaret',
                       'sarap.xcas')

        # Test5 transcritical kernel
        self.add_study('vnv_2',
                       'mascaret',
                       'mascaret.xcas')

    def _check_results(self):
        """
        Comparison with reference file
        """
        # Comparison with reference file
        self.check_epsilons('vnv_1:res',
                            'ref/sarap_ref.opt',
                            eps=[1.E-3],
                            masc=True)

        # Comparison with reference file
        self.check_epsilons('vnv_2:res',
                            'ref/mascaret_exp_ref.opt',
                            eps=[1.E-3],
                            masc=True)

    def _post(self):
        """
        Post-treatment processes
        """
# =============================================================================
#         GET VALUES
# =============================================================================
        # CALCUL ANALYTIC
        x_analytic, y_analytic, h1_analytic, h2_analytic = analytic_solution()

        # Getting results from mascaret files
        sarap_res, _ = self.get_study_res('vnv_1:sarap_ecr.opt', 'mascaret')
        x_sarap = sarap_res.reaches[1].get_section_pk_list()
        var_pos = sarap_res.get_position_var_abbr('Y')
        y_sarap = sarap_res.get_values_at_reach(-1, 1, [var_pos])

        trans_exp_res, _ = self.get_study_res(
            'vnv_2:mascaret_ecr.opt', 'mascaret')
        x_trans = trans_exp_res.reaches[1].get_section_pk_list()
        var_pos = trans_exp_res.get_position_var_abbr('Y')
        y_trans = trans_exp_res.get_values_at_reach(-1, 1, [var_pos])

        #  CALCUL hEAD LOSS
        index_inflow_sarap = np.argmin(x_sarap <= 2500)
        h2_sarap = y_sarap[index_inflow_sarap-1]
        h1_sarap = y_sarap[index_inflow_sarap-2]
        deltah_sarap = h1_sarap-h2_sarap

        index_inflow_trans = np.argmin(x_trans <= 2500)
        h2_trans = y_trans[index_inflow_trans-1]
        h1_trans = y_trans[index_inflow_trans-2]
        deltah_trans = h1_trans-h2_trans

        #  CALCUL ANALYTIC CORRECTED
        h1_analytic_corrected = apport(h2_sarap)
        delta_analytic_corrected = h1_analytic_corrected-h2_sarap

# =============================================================================
# CALCUL ERROR
# =============================================================================
        # EPSILON COTE
        y_analytic_compare = np.interp(
            x_sarap, x_analytic, y_analytic)
        y_analytic_compare = y_analytic_compare[:, np.newaxis]


# =============================================================================
# SAVE FIGURES
# =============================================================================
        # PRINT TABLE
        h2_values = [h2_analytic, h2_sarap, h2_trans, h2_sarap]
        h1_values = [h1_analytic, h1_sarap, h1_trans, h1_analytic_corrected]
        deltah_values = [h1_analytic-h2_analytic,
                         deltah_sarap, deltah_trans, delta_analytic_corrected]

        lign = ['Analytic solution', 'SARAP',
                'MASCARET EXP.', 'Corrected analytic solution']
        header = '& $h_2~\\mathrm{[m]}$ & $h_1~\\mathrm{[m]}$ & $\\Delta h~\\mathrm{[m]}$'
        file_path = path.join('.', 'img', 'table.txt')
        with open(file_path, 'w') as outfile:
            outfile.write(header)
            for i, _ in enumerate(h2_values):
                outfile.write(' \\\\ \n %s ' % lign[i])
                outfile.write(' & %6.3f' % h2_values[i])
                outfile.write(' & %6.3f' % h1_values[i])
                outfile.write(' & %6.3f' % deltah_values[i])

        # DISPLAY WATER DEPTH
        fig, axe = plt.subplots(figsize=(6, 5))
        plot1d(axe, x_analytic, y_analytic,
               plot_label='Analytic',
               x_label='$X$ [m]',
               y_label='Water height $Y$ [m]',
               linestyle='-', color='k',)
        plot1d(axe, x_sarap, y_sarap,
               plot_label='SARAP',
               x_label='$X$ [m]',
               y_label='Water height $Y$ [m]',
               linestyle='--', color='b',)
        plot1d(axe, x_trans, y_trans,
               plot_label='MASCARET EXP. (Nc=0.8)',
               x_label='$X$ [m]',
               y_label='Water height $Y$ [m]',
               linestyle=':', color='r',)
        axe.legend()
        plt.tight_layout()
        plt.savefig(path.join('.', 'img', 'depth.png'))
        plt.close(fig)

        # DISPLAY ERROR
        fig, axe = plt.subplots(figsize=(6, 5))
        plot1d(axe, x_sarap, y_analytic_compare-y_sarap,
               plot_label='SARAP',
               x_label='$X$ [m]',
               y_label='$\\epsilon$ [m]',
               linestyle='--', color='b',)
        plot1d(axe, x_trans, y_analytic_compare-y_trans,
               plot_label='MASCARET EXP. (Nc=0.8)',
               x_label='$X$ [m]',
               y_label='$\\epsilon$ [m]',
               linestyle=':', color='r',)
        axe.legend()
        plt.tight_layout()
        plt.savefig(path.join('.', 'img', 'epsilon_cote.png'))
        plt.close(fig)
