
"""
Validation script for test16_choc
"""
from vvytel.vnv_study import AbstractVnvStudy
from execution.telemac_cas import TelemacCas, get_dico


class VnvStudy(AbstractVnvStudy):
    """
    Class for validation
    """

    def _init(self):
        """
        Defines the general parameter
        """
        self.rank = 0
        self.tags = ['mascaret']

    def _pre(self):
        """
        Defining the studies
        """

        # Test16_choc steady kernel
        self.add_study('vnv_1',
                       'mascaret',
                       'sarap.xcas')

        # Test16_choc unsteady subcritical kernel
        self.add_study('vnv_2',
                       'mascaret',
                       'rezo.xcas')

        # Test16_choc transcritical kernel
        self.add_study('vnv_3',
                       'mascaret',
                       'mascaret.xcas')

    def _check_results(self):
        """
        Comparison with reference file
        """
        self.check_epsilons('vnv_1:res',
                            'ref/sarap_ref.opt',
                            eps=[1.E-3],
                            masc=True)

        self.check_epsilons('vnv_2:res',
                            'ref/rezo_ref.opt',
                            eps=[1.E-3],
                            masc=True)
        self.check_epsilons('vnv_3:res',
                            'ref/mascaret_ref.opt',
                            eps=[1.E-3],
                            masc=True)

    def _post(self):
        """
        Post-treatment processes
        """
