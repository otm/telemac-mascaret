
"""
Validation script for test22
"""
from os import path

import matplotlib.pyplot as plt

from postel.plot1d import plot1d
from vvytel.vnv_study import AbstractVnvStudy


class VnvStudy(AbstractVnvStudy):
    """
    Class for validation
    """

    def _init(self):
        """
        Defines the general parameter
        """
        self.rank = 0
        self.tags = ['mascaret']

    def _pre(self):
        """
        Defining the studies
        """

        # Test22 permanent kernel
        self.add_study('vnv_1',
                       'mascaret',
                       'sarap.xcas')

        # Test22 transcritical kernel
        self.add_study('vnv_2',
                       'mascaret',
                       'mascaret.xcas')

    def _check_results(self):
        """
        Comparison with reference file
        """
        self.check_epsilons('vnv_1:res',
                            'ref/sarap.opt',
                            eps=[1.E-3],
                            masc=True)
        self.check_epsilons('vnv_2:res',
                            'ref/mascaret.rub',
                            eps=[1.E-3],
                            masc=True)

    def _post(self):
        """
        Post-treatment processes
        """
        # Getting results from mascaret file SARAP
        sarap_res, _ = self.get_study_res('vnv_1:sarap.opt', 'mascaret')
        x_sarap = sarap_res.reaches[1].get_section_pk_list()
        var_pos_sarap = sarap_res.get_position_var('Cote du fond')
        var_pos_sarap = sarap_res.get_position_var('Cote de l eau')
        cote_sarap = sarap_res.get_values_at_reach(-1, 1, [var_pos_sarap])
        var_pos_sarap = sarap_res.get_position_var('Debit mineur')
        qm_sarap = sarap_res.get_values_at_reach(-1, 1, [var_pos_sarap])
        var_pos_sarap = sarap_res.get_position_var('Debit total')
        qt_sarap = sarap_res.get_values_at_reach(-1, 1, [var_pos_sarap])

        # Getting results from mascaret file transcritical kernel
        masc_res, _ = self.get_study_res('vnv_2:mascaret.rub', 'mascaret')
        x_masc = masc_res.reaches[1].get_section_pk_list()
        var_pos_masc = masc_res.get_position_var('Cote du fond')
        fond_masc = masc_res.get_values_at_reach(-1, 1, [var_pos_masc])
        var_pos_masc = masc_res.get_position_var('Cote de l eau')
        cote_masc = masc_res.get_values_at_reach(-1, 1, [var_pos_masc])
        var_pos_masc = masc_res.get_position_var('Debit mineur')
        qm_masc = masc_res.get_values_at_reach(-1, 1, [var_pos_masc])
        var_pos_masc = masc_res.get_position_var('Debit total')
        qt_masc = masc_res.get_values_at_reach(-1, 1, [var_pos_masc])

        # =====================================================================
        # EPSILON
        # =====================================================================
        qt_epsilon = qt_masc-qt_sarap
        h_epsilon = cote_masc-cote_sarap

        # =====================================================================
        # DISPLAY
        # =====================================================================

        fig, axe = plt.subplots(figsize=(6, 5))
        plot1d(axe, x_sarap, fond_masc,
               plot_label='Bottom level',
               x_label='Abscissae [m]',
               y_label='Water level [m]',
               linestyle='-', color='k',)
        plot1d(axe, x_sarap, cote_sarap,
               plot_label='Steady State',
               x_label='Abscissae [m]',
               y_label='Water level [m]',
               linestyle='-', color='r',)
        plot1d(axe, x_masc, cote_masc,
               plot_label='Transcritical Explicit (Nc=0.8)',
               x_label='Abscissae [m]',
               y_label='Water level [m]',
               linestyle='None', marker='+',
               color='b',
               )
        axe.legend()
        plt.tight_layout()
        plt.savefig(path.join('.', 'img', 'cote.png'))
        plt.close(fig)

        fig, axe = plt.subplots(figsize=(6, 5))
        plot1d(axe, x_sarap, qt_sarap,
               plot_label='Steady State $Q_{total}$',
               x_label='Abscissae [m]',
               y_label='Q [m3/s]',
               linestyle='-', color='r',)
        plot1d(axe, x_sarap, qm_sarap,
               plot_label='Steady State $Q_{m}$',
               x_label='Abscissae [m]',
               y_label='Q [m3/s]',
               linestyle='--', marker='+', color='r',)
        plot1d(axe, x_masc, qt_masc,
               plot_label='Transcritical $Q_{total}$',
               x_label='Abscissae [m]',
               y_label='Q [m3/s]',
               linestyle='-.', marker='None',
               color='b',
               )
        plot1d(axe, x_masc, qm_masc,
               plot_label='Transcritical $Q_{m}$',
               x_label='Abscissae [m]',
               y_label='Q [m3/s]',
               linestyle=':', marker='+',
               color='b',
               )
        axe.legend()
        plt.tight_layout()
        plt.savefig(path.join('.', 'img', 'q.png'))
        plt.close(fig)

        fig, axe = plt.subplots(figsize=(6, 5))
        plot1d(axe, x_masc, qt_epsilon,
               x_label='Abscissae [m]',
               y_label='$\\epsilon_{Qt}$ [m3/s]',
               linestyle='-', marker='+',
               color='b',
               )
        plt.tight_layout()
        plt.savefig(path.join('.', 'img', 'epsilon_q.png'))
        plt.close(fig)

        fig, axe = plt.subplots(figsize=(6, 5))
        plot1d(axe, x_masc, h_epsilon,
               x_label='Abscissae [m]',
               y_label='$\\epsilon_{h}$ [m]',
               linestyle='-', marker='+',
               color='b',
               )
        plt.tight_layout()
        plt.savefig(path.join('.', 'img', 'epsilon_h.png'))
        plt.close(fig)
