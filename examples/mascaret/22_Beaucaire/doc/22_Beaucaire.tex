\chapter{Beaucaire and Compound Channel}

%--------------------------------------------------------------------------------------------------
\section{Purpose}
The objective of this test is to evaluate the transcritical kernel's ability to model flows in a compound channel (combining low and high flow sections). The \emph{Debord} model was chosen to maintain consistency with existing fluvial kernels, as SARAP and REZO. The results obtained with the transcritical kernel are compared with those from the steady-state SARAP kernel, to validate the transcritical kernel's performance in this context. The modeled reach corresponds to a section of the Beaucaire reach on the Rhône River.

%--------------------------------------------------------------------------------------------------
\section{Description}

\subsection*{Geometry}
The model represents a single reach approximately $12.7~\mathrm{km}$ long, modeled by $29$ cross-sections. These include a low flow channel and a high flow channels on the left and right banks.

\subsection*{Initial condition}
For the steady-state SARAP kernel, no initial condition is required. For the transcritical kernel, an initial water level is imposed, which is obtained from a prior calculation using the SARAP kernel.

\subsection*{Boundary condition}
Upstream: Constant discharge, $Q = 9100~\mathrm{m^{3}.s^{-1}}$.\\
Downstream: Constant water level, $\mathrm{Water~level} = 15.56~\mathrm{m}$.

\subsection*{Physical parameters}
Strickler coefficients: 
\begin{itemize} 
	\item Low flow channel: $K_m = 44.0~\mathrm{m^{1/3}.s^{-1}}$.
	\item Hight flow channel: $K_M = 22.0~\mathrm{m^{1/3}.s^{-1}}$.
\end{itemize}
The Debord model is employed for these simulations..

\subsection*{Numerical parameters}
The mesh is define with a uniform spatial step of $ 200~\mathrm{m}$.\\
The vertical discretization of the cross-section is uniform across the domain and set at $40~\mathrm{cm}$.

No time step parameters are required for the steady-state SARAP kernel. For the transcritical kernel, the simulation is conducted over $2000$ time steps, with a variable time step governed by a target Courant number of $0.8$.

Two kernels are employed: the transcritical kernel in its explicit form and the steady-state SARAP kernel, the latter serving as the reference.

%--------------------------------------------------------------------------------------------------
\section{Results}
The results are presented in Figures \ref{mascaret:test22:cote} and \ref{mascaret:test22:q}. Convergence towards a steady-state solution is not guaranteed when using the transcritical kernel in a compound channel. Minor fluctuations persist, on the order of $1\%$ of the total flow (Figure \ref{mascaret:test22:epsilon_q}).

Although this result is not entirely satisfactory from a theoretical perspective, it is adequate for practical studies in a compound channel.

\begin{figure}[H]
	\centering
	\includegraphicsmaybe{[width=0.75\textwidth]}{../img/cote.png}
	\caption{Comparison of water levels.}
	\label{mascaret:test22:cote}
\end{figure}

\begin{figure}[H]
	\centering
	\includegraphicsmaybe{[width=0.75\textwidth]}{../img/q.png}
	\caption{Comparison of flow discharges.}
	\label{mascaret:test22:q}
\end{figure}

Comparison of the respective low flow discharges and free surface levels confirms the good quality of the results obtained (Figure \ref{mascaret:test22:epsilon_h}). The difference in water level between the two kernels is consistently less than $2~\mathrm{cm}$, with an average difference of $0.5~\mathrm{cm}$.


\begin{figure}[H]
	\centering
	\includegraphicsmaybe{[width=0.75\textwidth]}{../img/epsilon_q.png}
	\caption{Difference between total flow discharges calculated with the transcritical kernel and steady-state kernel.}
	\label{mascaret:test22:epsilon_q}
\end{figure}

\begin{figure}[H]
	\centering
	\includegraphicsmaybe{[width=0.75\textwidth]}{../img/epsilon_h.png}
	\caption{Difference between water levels calculated with the transcritical kernel and steady-state kernel.}
	\label{mascaret:test22:epsilon_h}
\end{figure}

%--------------------------------------------------------------------------------------------------
\section*{Conclusion}
The transcritical kernel can handle compound channel flows for real-life studies. Comparisons with the steady-state kernel SARAP are satisfactory. However, the presence of a compound channel no longer ensures convergence to a steady state at constant flow. Slight fluctuations persist, of the order of $1\%$. This is undoubtedly due to the way in which the additional terms that have appeared in the Saint-Venant equations have been discretized (explicit discretization as a forcing term of the $\beta$ coefficient that appears in the propagation terms).