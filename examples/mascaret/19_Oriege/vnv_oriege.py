
"""
Validation script for test19
"""
from os import path

import matplotlib.pyplot as plt
import numpy as np

from postel.plot1d import plot1d
from vvytel.vnv_study import AbstractVnvStudy


class VnvStudy(AbstractVnvStudy):
    """
    Class for validation
    """

    def _init(self):
        """
        Defines the general parameter
        """
        self.rank = 0
        self.tags = ['mascaret']

    def _pre(self):
        """
        Defining the studies
        """

        # Test19 permanent kernel
        self.add_study('vnv_1',
                       'mascaret',
                       'sarap.xcas')

        # Test19 transcritical kernel
        self.add_study('vnv_2',
                       'mascaret',
                       'mascaret.xcas')

    def _check_results(self):
        """
        Comparison with reference file
        """
        self.check_epsilons('vnv_1:res',
                            'ref/sarap.opt',
                            eps=[1.E-1],
                            var1='Cote de l eau',
                            var2='Debit mineur',
                            masc=True)

        self.check_epsilons('vnv_2:res',
                            'ref/mascaret.rub',
                            eps=[1.E-1],
                            var1='Cote de l eau',
                            var2='Debit mineur',
                            masc=True)

    def _post(self):
        """
        Post-treatment processes
        """
        # Getting results from mascaret file SARAP
        sarap_res, _ = self.get_study_res('vnv_1:sarap.opt', 'mascaret')
        x_sarap = sarap_res.reaches[1].get_section_pk_list()
        var_pos_sarap = sarap_res.get_position_var('Cote du fond')
        var_pos_sarap = sarap_res.get_position_var('Cote de l eau')
        cote_sarap = sarap_res.get_values_at_reach(-1, 1, [var_pos_sarap])
        var_pos_sarap = sarap_res.get_position_var("Hauteur d'eau")
        h_sarap = sarap_res.get_values_at_reach(-1, 1, [var_pos_sarap])
        var_pos_sarap = sarap_res.get_position_var('Debit mineur')
        q_sarap = sarap_res.get_values_at_reach(-1, 1, [var_pos_sarap])
        var_pos_sarap = sarap_res.get_position_var('Nombre de Froude')
        fr_sarap = sarap_res.get_values_at_reach(-1, 1, [var_pos_sarap])

        # Getting results from mascaret file transcritical kernel
        masc_res, _ = self.get_study_res('vnv_2:mascaret.rub', 'mascaret')
        x_masc = masc_res.reaches[1].get_section_pk_list()
        var_pos_masc = masc_res.get_position_var('Cote du fond')
        fond_masc = masc_res.get_values_at_reach(-1, 1, [var_pos_masc])
        var_pos_masc = masc_res.get_position_var('Cote de l eau')
        cote_masc = masc_res.get_values_at_reach(-1, 1, [var_pos_masc])
        var_pos_masc = masc_res.get_position_var("Hauteur d'eau")
        h_masc = masc_res.get_values_at_reach(-1, 1, [var_pos_masc])
        var_pos_masc = masc_res.get_position_var('Debit mineur')
        q_masc = masc_res.get_values_at_reach(-1, 1, [var_pos_masc])
        var_pos_masc = masc_res.get_position_var('Nombre de Froude')
        fr_masc = masc_res.get_values_at_reach(-1, 1, [var_pos_masc])

        # =====================================================================
        # Load measurement data
        # =====================================================================
        x_measure = [0, 340, 564.5, 1594, 2119, 2132.5,
                     2402.5, 2716.5, 3216.5, 3829.5, 4354, 4442, 5007]
        z_measure = [899.89, 893.62, 883.76, 864.91, 846.99, 845.69,
                     838.6, 835.33, 830.05, 825.59, 821.94, 821.15, 817.4]
        index_list = []
        for x in x_measure:
            index = np.argmin(np.abs(x_masc - x))
            index_list.append(index)

        # =====================================================================
        # DISPLAY
        # =====================================================================

        fig, axe = plt.subplots(figsize=(6, 5))
        plot1d(axe, x_sarap, fond_masc,
               plot_label='Bottom level',
               x_label='Abscissae [m]',
               y_label='Water level [m]',
               linestyle='-', color='k',)
        plot1d(axe, x_measure, z_measure,
               plot_label='Measure',
               x_label='Abscissae [m]',
               y_label='Water level [m]',
               linestyle='None', color='g',
               marker='o')
        plot1d(axe, x_sarap, cote_sarap,
               plot_label='Steady State',
               x_label='Abscissae [m]',
               y_label='Water level [m]',
               linestyle=':', color='r',)
        plot1d(axe, x_masc, cote_masc,
               plot_label='Transcritical Explicit (Nc=0.8)',
               x_label='Abscissae [m]',
               y_label='Water level [m]',
               linestyle='--', color='b',
               )
        axe.legend()
        plt.tight_layout()
        plt.savefig(path.join('.', 'img', 'cote.png'))
        plt.close(fig)

        fig, axe = plt.subplots(figsize=(6, 5))
        plot1d(axe, x_sarap, q_sarap,
               plot_label='Steady State',
               x_label='Abscissae [m]',
               y_label='Q [m3/s]',
               linestyle=':', color='r',)
        plot1d(axe, x_masc, q_masc,
               plot_label='Transcritical Explicit (Nc=0.8)',
               x_label='Abscissae [m]',
               y_label='Q [m3/s]',
               linestyle='--', color='b',)
        axe.legend()
        plt.tight_layout()
        plt.savefig(path.join('.', 'img', 'q.png'))
        plt.close(fig)

        fig, axe = plt.subplots(figsize=(6, 5))
        plot1d(axe, x_sarap, h_sarap,
               plot_label='Steady State',
               x_label='Abscissae [m]',
               y_label='Depth [m]',
               linestyle=':', color='r',)
        plot1d(axe, x_masc, h_masc,
               plot_label='Transcritical Explicit (Nc=0.8)',
               x_label='Abscissae [m]',
               y_label='Depth [m]',
               linestyle='--', color='b',)
        axe.legend()
        plt.tight_layout()
        plt.savefig(path.join('.', 'img', 'h.png'))
        plt.close(fig)

        fig, axe = plt.subplots(figsize=(6, 5))
        plot1d(axe, x_sarap, fr_sarap,
               plot_label='Steady State',
               x_label='Abscissae [m]',
               y_label='Froude',
               linestyle=':', color='r',)
        plot1d(axe, x_masc, fr_masc,
               plot_label='Transcritical Explicit (Nc=0.8)',
               x_label='Abscissae [m]',
               y_label='Froude',
               linestyle='--', color='b',)
        axe.legend()
        plt.tight_layout()
        plt.savefig(path.join('.', 'img', 'Fr.png'))
        plt.close(fig)

        # =====================================================================
        # TABLE
        # =====================================================================

        with open(path.join('.', 'img', 'comparaison.txt'), 'w') as file:
            # Write header
            file.write("""Abscissa &
                       Measured &
                       SARAP &
                       Difference &
                       Trans EXP. &
                       Difference \\\\\n""")
            file.write("""$X$ [m] &
                       $Z$ [m] &
                       $Z$ [m] &
                       $\\Delta$ [m] &
                       $Z$ [m] &
                       $\\Delta$ [m]""")
            # Write data
            for i, x in enumerate(x_measure):
                index = np.argmin(np.abs(x_masc - x))
                diff_sarap = cote_sarap[index] - z_measure[i]
                diff_masc = cote_masc[index] - z_measure[i]
                file.write(f"""\\\\\n \\hline {x_measure[i]:.2f} &
                           {z_measure[i]:.2f} &
                           {cote_sarap[index][0]:.2f} &
                           {diff_sarap[0]:.2f} &
                           {cote_masc[index][0]:.2f} & {diff_masc[0]:.2f}""")
