\chapter{Permanent Flow in the Oriege Valley}

%--------------------------------------------------------------------------------------------------
\section{Purpose}
The test case involves modeling the Oriege Valley over a distance of $5~\mathrm{km}$ downstream of the Orlu plant. The aim is to create a hydraulic model of the Oriege valley for storage water propagation. Steady-state measurements were taken to calibrate the model.

This study is particularly valuable as it validates the model in a real-world scenario involving permanent flow in a mountain torrent, which presents several challenges, including torrential flow with very low discharges, a steep gradient of $5\%$, and the presence of basins.

The model geometry is based on bathymetric measurements. Additionally, $13$ water gauges were installed to establish permanent flow lines. These measurements are used to validate the model.

%--------------------------------------------------------------------------------------------------
\section{Description}

\subsection*{Geometry}
The Oriege valley ($5~\mathrm{km}$ long) is modeled using $75$ cross-sections. These profiles, surveyed by the EDF-LNHE, are positioned to accurately represent the average geometry of the river. However, certain areas were not surveyed bathymetrically due to accessibility issues.


\subsection*{Initial condition}
For the steady-state kernel SARAP, no initial condition is necessary.\\
For the transient kernel, an initial water surface elevation is imposed.

\subsection*{Boundary condition}
Upstream: Constant discharge, $Q = 12.36~\mathrm{m^{3}.s^{-1}}$.\\
Downstream: Constant water level, $\mathrm{Water~level} = 817.45~\mathrm{m}$.

\subsection*{Physical parameters}
Strickler coefficient : $K_m = 20~\mathrm{m^{1/3}.s^{-1}}$ .

\subsection*{Lateral inflows}
A inflow of $0.85~\mathrm{m^{3}.s^{-1}}$ is located at abscissa $2938~\mathrm{m}$. 

\subsection*{Numerical parameters}
The domain is divided into several mesh zones:
\begin{itemize} 
	\item The section between $0~\mathrm{m} \leq x \leq 200~\mathrm{m}$, is divided into 29 meshes.
	\item The section between $200~\mathrm{m} < x \leq 1500~\mathrm{m}$, is divided into 350 meshes.
	\item The section between $1500~\mathrm{m} < x \leq 5043~\mathrm{m}$, is divided into 550 meshes.
\end{itemize}
The vertical discretization of the cross-section is uniform across the domain and set at $0.3~\mathrm{cm}$.

No time step parameters for the steady kernel (not requested).\\
For the transcritical kernel, the simulation duration is set to $8000$ time steps. The time step is variable, with a target Courant number of $0.8$.

Two kernels are used: the steady-state kernel SARAP and the transcritical kernel in its explicit form.
 
%--------------------------------------------------------------------------------------------------
\section{Results}

Figures \ref{mascaret:test19:cote} and \ref{mascaret:test19:q} show the water level and the flow discharge. The flow calculated with the transcritical kernel exhibits some oscillations, with amplitudes of up to $10\%$. These oscillations correspond to areas with significant variations in the Froude number (Figure \ref{mascaret:test19:fr}). In contrast, the steady-state kernel SARAP shows no flow oscillations.

\begin{figure}[H]
	\centering
	\includegraphicsmaybe{[width=0.75\textwidth]}{../img/cote.png}
	\caption{Comparaison of water levels.}
	\label{mascaret:test19:cote}
\end{figure}

\begin{figure}[H]
	\centering
	\includegraphicsmaybe{[width=0.75\textwidth]}{../img/q.png}
	\caption{Comparison of flow discharge.}
	\label{mascaret:test19:q}
\end{figure}

\begin{figure}[H]
	\centering
	\includegraphicsmaybe{[width=0.75\textwidth]}{../img/Fr.png}
	\caption{Comparison of Froude numbers.}
	\label{mascaret:test19:fr}
\end{figure}

The water depths obtained with the two calculation kernels are nearly identical (Figure \ref{mascaret:test19:h}).
\begin{figure}[H]
	\centering
	\includegraphicsmaybe{[width=0.75\textwidth]}{../img/h.png}
	\caption{Comparison of depths.}
	\label{mascaret:test19:h}
\end{figure}

Table \ref{mascaret:test19:comparaison} compares the calculated water levels with the measured data, showing good agreement between the two.

\begin{table}[H]
	\begin{center}
		\caption{Comparison of computed and measured water levels.}
		\label{mascaret:test19:comparaison}
		\begin{tabular}{|c|c|c|c|c|c|}
			\toprule
			\InputIfFileExists{../img/comparaison.txt}{}{}{}{}{}\\
			\bottomrule
		\end{tabular}
	\end{center}
\end{table}

%--------------------------------------------------------------------------------------------------
\section*{Conclusion}
This test case successfully validated the steady-state kernel SARAP and the transcritical kernel for modeling torrential flows in a realistic river geometry.