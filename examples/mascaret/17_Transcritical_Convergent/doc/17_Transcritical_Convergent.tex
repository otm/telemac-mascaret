\chapter{Transcritical Flow in a Convergent Channel}

%--------------------------------------------------------------------------------------------------
\section{Purpose}
This test case aims to validate the steady-state kernel SARAP in a rectangular channel with a converging width. Validation is performed by comparing the results with those obtained from the transcritical kernel, where the steady-state is achieved through convergence.

%--------------------------------------------------------------------------------------------------
\section{Description}

\subsection*{Geometry}
The calculation is performed in a rectangular channel $500~\mathrm{m}$ long with no slope.\\
The channel geometry is described by 6 cross-sections located at $X = 0~\mathrm{m}$, $X = 100~\mathrm{m}$, $X = 110~\mathrm{m}$, $X = 130~\mathrm{m}$, $X = 140~\mathrm{m}$ and $X = 500 ~\mathrm{m}$. The channel width is decreases from $10~\mathrm{m}$ to $2~\mathrm{m}$ (a reduction by a factor of $5$).

%\begin{figure}[H]
%	\centering
%	\begin{tikzpicture}
%		
%		% Canal (vue de dessus)
%		\draw[thick] (0,0) -- (14,0); % Ligne supérieure du canal
%		\draw[thick] (0,-2) -- (14,-2); % Ligne inférieure du canal
%		
%		% Marqueurs de largeur
%		\draw[thick] (3,-0.2) -- (3,-1.8); % Marqueur gauche
%		\draw[thick] (6,-0.2) -- (6,-1.8); % Marqueur central
%		\draw[thick] (10,-0.2) -- (10,-1.8); % Marqueur droite
%		
%		% Partie centrale du canal
%		\draw[thick] (3,-0.2) -- (6,-0.2) -- (6,-1.8) -- (10,-1.8) -- (10,-0.2) -- (13,-0.2);
%		\draw[thick] (3,-1.8) -- (6,-1.8) -- (6,-0.2) -- (10,-0.2) -- (10,-1.8) -- (13,-1.8);
%		
%		% Texte (dimensions)
%		\node at (6.5,-2.5) {100 m};
%		\node at (9,-2.5) {110 m};
%		\node at (11.5,-2.5) {130 m};
%		
%		\node at (14.5,-1) {10 m};
%		
%		% Indicateur de direction
%		\draw[thick,->] (0, -2.5) -- (14, -2.5); % Flèche horizontale
%		
%	\end{tikzpicture}
%	\caption{Top view of channel geometry.}
%	\label{mascaret:test17:geometry}
%\end{figure}

\subsection*{Initial condition}
For the steady-state kernel SARAP, no initial condition is necessary.\\
For the transcritical kernel, the initial condition is still water at an elevation of $2.5~\mathrm{m}$, with a constant discharge of $50~\mathrm{m^{3}.s^{-1}}$.

\subsection*{Boundary condition}
Upstream: Constant discharge, $Q = 20~\mathrm{m^{3}.s^{-1}}$.\\
Downstream: Constant water level, $H = 2.5~\mathrm{m}$.

\subsection*{Physical parameters}
Friction is not considered in this test case.
 
\subsection*{Numerical parameters}
The domain is divided into cells with a constant length of $0.5~\mathrm{m}$.\\
The vertical discretization of the cross-section is uniform across the domain and set at $0.5~\mathrm{cm}$ 

No time step parameters are required for the steady-state kernel SARAP.\\
For the transcritical kernel, the simulation is set to run for $100,000$ time steps. The time step is variable, with a target Courant number of $0.8$. 

Two kernels are used: the steady-state kernel SARAP and the transcritical kernel in explicit form.
 
%--------------------------------------------------------------------------------------------------
\section{Results}
The water level calculated with the steady-state kernel SARAP is compared with the converged water level from the transient regime. The results from both kernels are similar (see Table \ref{mascaret:test17:comparaison} and Figure \ref{mascaret:test17:cote}). The position of the hydraulic jump is determined from the gradient of the Froude number. The jump is located slightly upstream when using the transcritical kernel compared to the steady-state kernel.

\begin{table}[H]
	\begin{center}
		\caption{Comparison of results computed with the steady-state kernel SARAP and the converged transcritical kernel.}
		\label{mascaret:test17:comparaison}
		\begin{tabular}{l|c|c|c}
			\InputIfFileExists{../img/comparaison.txt}{}{}{}{}{}\\
		\end{tabular}
	\end{center}
\end{table}

\begin{figure}[H]
	\centering
	\includegraphicsmaybe{[width=0.75\textwidth]}{../img/cote.png}
	\caption{Comparison of water levels.}
	\label{mascaret:test17:cote}
\end{figure}

There is a slight difference in the upstream water level calculated by the SARAP kernel compared to the transcritical kernel. If the mesh size is coarse (e.g., $2~\mathrm{m}$), this difference can be about $20~\mathrm{cm}$. This discrepancy is due to differences in the accuracy of the two numerical schemes: the finite difference scheme used in the steady-state kernel is of second order, while the transcritical kernel uses a first-order scheme.

Figure~\ref{mascaret:test17:q} compares the flow discharge calculated by the two kernels. A peak in discharge is observed at the hydraulic jump with the transcritical kernel, a common issue in transcritical flow simulations and present in all test cases with the transcritical kernel.

\begin{figure}[H]
	\centering
	\includegraphicsmaybe{[width=0.75\textwidth]}{../img/q.png}
	\caption{Comparison of flow discharge.}
	\label{mascaret:test17:q}
\end{figure}

%--------------------------------------------------------------------------------------------------
\section*{Conclusion}
The steady-state kernel SARAP correctly identifies the position of the hydraulic jump, with results closely matching those obtained from the converged transcritical kernel. The physical behavior of the flow is consistent and well-represented by both methods.
