
"""
Validation script for test17
"""
from os import path
import numpy as np
import matplotlib.pyplot as plt
from vvytel.vnv_study import AbstractVnvStudy
from postel.plot1d import plot1d


class VnvStudy(AbstractVnvStudy):
    """
    Class for validation
    """

    def _init(self):
        """
        Defines the general parameter
        """
        self.rank = 2
        self.tags = ['mascaret']

    def _pre(self):
        """
        Defining the studies
        """

        # Test17 permanent kernel
        self.add_study('vnv_1',
                       'mascaret',
                       'sarap.xcas')

        # Test17 transcritical kernel
        self.add_study('vnv_2',
                       'mascaret',
                       'mascaret.xcas')

    def _check_results(self):
        """
        Comparison with reference file
        """
        self.check_epsilons('vnv_1:res',
                            'ref/sarap.opt',
                            eps=[1.E-3],
                            masc=True)

        self.check_epsilons('vnv_2:res',
                            'ref/mascaret.rub',
                            eps=[1.E-3],
                            masc=True)

    def _post(self):
        """
        Post-treatment processes
        """

        # Getting results from mascaret file SARAP
        sarap_res, _ = self.get_study_res('vnv_1:sarap.opt', 'mascaret')
        x_sarap = sarap_res.reaches[1].get_section_pk_list()
        var_pos_sarap = sarap_res.get_position_var_abbr('Y')
        cote_sarap = sarap_res.get_values_at_reach(-1, 1, [var_pos_sarap])
        var_pos_sarap = sarap_res.get_position_var('Nombre de Froude')
        fr_sarap = sarap_res.get_values_at_reach(-1, 1, [var_pos_sarap])
        ressaut_sarap = x_sarap[np.argmin(np.diff(fr_sarap, axis=0))]
        amont_sarap = cote_sarap[0]
        var_pos_sarap = sarap_res.get_position_var('Debit mineur')
        q_sarap = sarap_res.get_values_at_reach(-1, 1, [var_pos_sarap])

        # Getting results from mascaret file transcritical kernel
        masc_res, _ = self.get_study_res('vnv_2:mascaret.rub', 'mascaret')
        x_masc = masc_res.reaches[1].get_section_pk_list()
        var_pos_masc = masc_res.get_position_var_abbr('Y')
        cote_masc = masc_res.get_values_at_reach(-1, 1, [var_pos_masc])
        var_pos_masc = masc_res.get_position_var('Nombre de Froude')
        fr_masc = masc_res.get_values_at_reach(-1, 1, [var_pos_masc])
        ressaut_masc = x_masc[np.argmin(np.diff(fr_masc, axis=0))]
        amont_masc = cote_masc[0]
        var_pos_masc = masc_res.get_position_var('Debit mineur')
        q_masc = masc_res.get_values_at_reach(-1, 1, [var_pos_masc])

        # =====================================================================
        # DISPLAY
        # =====================================================================

        fig, axe = plt.subplots(figsize=(6, 5))
        plot1d(axe, x_sarap, cote_sarap,
               plot_label='Steady State',
               x_label='Abscissae [m]',
               y_label='Water depth [m]',
               linestyle='-', color='r',)
        plot1d(axe, x_masc, cote_masc,
               plot_label='Transcritical Explicit (Nc=0.8)',
               x_label='Abscissae [m]',
               y_label='Water height [m]',
               linestyle='--', color='b',
               )
        axe.legend()
        plt.tight_layout()
        plt.savefig(path.join('.', 'img', 'cote.png'))
        plt.close(fig)

        fig, axe = plt.subplots(figsize=(6, 5))
        plot1d(axe, x_sarap, q_sarap,
               plot_label='Steady State',
               x_label='Abscissae [m]',
               y_label='Q [m3/s]',
               linestyle='-', color='r',)
        plot1d(axe, x_masc, q_masc,
               plot_label='Transcritical Explicit (Nc=0.8)',
               x_label='Abscissae [m]',
               y_label='Q [m3/s]',
               linestyle='--', color='b',)
        axe.legend()
        plt.tight_layout()
        plt.savefig(path.join('.', 'img', 'q.png'))
        plt.close(fig)

        fig, axe = plt.subplots(figsize=(6, 5))
        plot1d(axe, x_sarap, fr_sarap,
               plot_label='Steady State',
               x_label='Abscissae [m]',
               y_label='Froude',
               linestyle='-', color='r',)
        plot1d(axe, x_masc, fr_masc,
               plot_label='Transcritical Explicit (Nc=0.8)',
               x_label='Abscissae [m]',
               y_label='Froude',
               linestyle='--', color='b',)
        axe.legend()
        plt.tight_layout()
        plt.savefig(path.join('.', 'img', 'Fr.png'))
        plt.close(fig)

        # =====================================================================
        # TABLE
        # =====================================================================

        with open(path.join('.', 'img', 'comparaison.txt'), 'w') as file:
            # Write header
            file.write(
                " & Analytical & MASCARET (EXP., Nc=0.8 )  & Difference \\\\\n")
            file.write(f"""\\hline Upstream water level [m] &
                       {amont_sarap[0]:.3f} &
                       {amont_masc[0]:.3f} &
                       {amont_sarap[0] - amont_masc[0]:.3f} \\\\\n""")
            file.write(f"""Position of hydraulic jump [m] &
                       {ressaut_sarap:.1f} & {ressaut_masc:.1f} &
                       {ressaut_masc-ressaut_sarap:.1f}""")
