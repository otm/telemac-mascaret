"""
Validation script for Compound Channel
"""
from os import path
import numpy as np
import matplotlib.pyplot as plt
from postel.plot1d import plot1d
from vvytel.vnv_study import AbstractVnvStudy


def compute_analytic_solution():
    # Constants
    gravity = 9.81  # Gravitational acceleration [m/s^2]
    strickler_minor = 90  # Roughness coefficient of the low flow channel
    strickler_major = 20  # Roughness coefficient of the high flow channel
    channel_width = 1  # Channel width [m]
    downstream_depth = 1  # Downstream water height [m]
    upstream_flow = 1  # Upstream discharge [m^3/s]
    reach_length = 4990  # Reach length [m]

    # Analytical solution using the RK4 method
    def differential_eq(y0, q_minor, q_major, strickler_minor, strickler_major, gravity, width):
        dy_dx = 2 * gravity / y0**(1/3) * (q_minor**2 / strickler_minor**2 + q_major**2 / strickler_major**2) / \
            (2 * (q_minor**2 + q_major**2) - gravity * width**2 * y0**3)
        return dy_dx

    x_current = reach_length
    y_current = downstream_depth
    step_size = 0.01
    x_analytic = np.arange(x_current, -step_size, -step_size)
    y_analytic = np.zeros_like(x_analytic)
    y_analytic[0] = y_current

    ratio = 0.9 * (strickler_major /
                   strickler_minor) ** (1./6.)
    low_flow_discharge = 0.5 * strickler_minor * \
        ratio * channel_width * downstream_depth ** (5./3.)
    high_flow_discharge = 0.5 * strickler_major * \
        channel_width * (2 - ratio ** 2) ** 0.5 * downstream_depth ** (5./3.)
    eta = low_flow_discharge / high_flow_discharge
    q_major_analytic = upstream_flow / (1 + eta)
    q_minor_analytic = upstream_flow - q_major_analytic

    for i in range(len(x_analytic) - 1):
        k1 = differential_eq(y_current, q_minor_analytic, q_major_analytic,
                             strickler_minor, strickler_major, gravity, channel_width)
        k2 = differential_eq(y_current - step_size * 0.5 * k1, q_minor_analytic, q_major_analytic,
                             strickler_minor, strickler_major, gravity, channel_width)
        k3 = differential_eq(y_current - step_size * 0.5 * k2, q_minor_analytic, q_major_analytic,
                             strickler_minor, strickler_major, gravity, channel_width)
        k4 = differential_eq(y_current - step_size * k3, q_minor_analytic, q_major_analytic,
                             strickler_minor, strickler_major, gravity, channel_width)

        y_next = y_current - (step_size / 6.0) * (k1 + 2 * k2 + 2 * k3 + k4)
        y_analytic[i + 1] = y_next
        y_current = y_next

    x_analytic = np.flip(x_analytic)
    y_analytic = np.flip(y_analytic)

    return round(q_major_analytic, 5), round(q_minor_analytic, 5), x_analytic, y_analytic


class VnvStudy(AbstractVnvStudy):
    """
    Class for validation
    """

    def _init(self):
        """
        Defines the general parameter
        """
        self.rank = 0
        self.tags = ['mascaret']

    def _pre(self):
        """
        Defining the studies
        """

        # Test3 permanent kernel
        self.add_study('vnv_1',
                       'mascaret',
                       'sarap.xcas')

        # Test3 transcritical implicit kernel
        self.add_study('vnv_2',
                       'mascaret',
                       'mascaret_imp.xcas')

        # Test3 transcritical explicit kernel
        self.add_study('vnv_3',
                       'mascaret',
                       'mascaret_exp.xcas')

    def _check_results(self):
        """
        Comparison with reference file
        """
        self.check_epsilons('vnv_1:res',
                            'ref/sarap_ref.opt',
                            eps=[1.E-3],
                            masc=True)

        self.check_epsilons('vnv_2:res',
                            'ref/mascaret_imp_ref.opt',
                            eps=[1.E-3],
                            masc=True)

        self.check_epsilons('vnv_3:res',
                            'ref/mascaret_exp_ref.opt',
                            eps=[1.E-3],
                            masc=True)

    def _post(self):
        """
        Post-treatment processes
        """
        # Getting results from mascaret files
        steady_res, _ = self.get_study_res('vnv_1:sarap_ecr.opt', 'mascaret')
        masc_exp_res,  _ = self.get_study_res(
            'vnv_2:mascaret_imp.opt', 'mascaret')
        masc_imp_res,  _ = self.get_study_res(
            'vnv_3:mascaret_exp.opt', 'mascaret')

        x_sarap = steady_res.reaches[1].get_section_pk_list()
        x_exp = masc_exp_res.reaches[1].get_section_pk_list()
        x_imp = masc_imp_res.reaches[1].get_section_pk_list()

        var_pos = steady_res.get_position_var("Hauteur d'eau")
        y_sarap = steady_res.get_values_at_reach(-1, 1, [var_pos])
        var_pos = masc_exp_res.get_position_var("Hauteur d'eau")
        y_exp = masc_exp_res.get_values_at_reach(-1, 1, [var_pos])
        var_pos = masc_imp_res.get_position_var("Hauteur d'eau")
        y_imp = masc_imp_res.get_values_at_reach(-1, 1, [var_pos])

        var_pos = steady_res.get_position_var('Debit majeur')
        q_major_sarap = steady_res.get_values_at_reach(-1, 1, [var_pos])
        var_pos = masc_exp_res.get_position_var('Debit majeur')
        q_major_exp = masc_exp_res.get_values_at_reach(-1, 1, [var_pos])
        var_pos = masc_imp_res.get_position_var('Debit majeur')
        q_major_imp = masc_imp_res.get_values_at_reach(-1, 1, [var_pos])

        var_pos = steady_res.get_position_var('Debit mineur')
        q_minor_sarap = steady_res.get_values_at_reach(-1, 1, [var_pos])
        var_pos = masc_exp_res.get_position_var('Debit mineur')
        q_minor_exp = masc_exp_res.get_values_at_reach(-1, 1, [var_pos])
        var_pos = masc_imp_res.get_position_var('Debit mineur')
        q_minor_imp = masc_imp_res.get_values_at_reach(-1, 1, [var_pos])

        # =====================================================================
        # ANALyTICAL
        # =====================================================================
        # Compute analytical solution using RK4
        q_major_analytic, q_minor_analytic, x_analytic, y_analytic = compute_analytic_solution()

        # =====================================================================
        # EPSILON
        # =====================================================================

        y_analytic_compare = np.interp(
            steady_res.reaches[1].get_section_pk_list(), x_analytic, y_analytic)
        y_analytic_compare = y_analytic_compare[:, np.newaxis]
        epsilon_sarap = 100*np.abs(y_analytic_compare -
                                   y_sarap) / y_analytic_compare
        epsilon_exp = 100*np.abs(y_analytic_compare -
                                 y_exp) / y_analytic_compare
        epsilon_imp = 100*np.abs(y_analytic_compare -
                                 y_imp) / y_analytic_compare

        # =====================================================================
        # DISPLAY
        # =====================================================================
        # Plotting water height
        fig, axes = plt.subplots(figsize=(6, 5))
        plot1d(axes, x_analytic, y_analytic,
               plot_label='Analytic',
               x_label='Abscissae [m]',
               y_label='Water height [m]',
               linestyle='-', color='k',)
        plot1d(axes, x_sarap, y_sarap,
               plot_label='SARAP',
               x_label='Abscissae [m]',
               y_label='Water height [m]',
               linestyle='--', color='b',)
        plot1d(axes, x_exp, y_exp,
               plot_label='Transcritical Explicit (Nc=0.8)',
               x_label='Abscissae [m]',
               y_label='Water height [m]',
               linestyle='-.', color='r',)
        plot1d(axes, x_imp, y_imp,
               plot_label='Transcritical Implicit (Nc=2)',
               x_label='Abscissae [m]',
               y_label='Water height [m]',
               linestyle=':', color='g',)
        axes.legend()
        plt.tight_layout()
        plt.savefig(path.join('.', 'img', 'depth.png'))
        plt.close(fig)

        # Plotting flow qm (minor discharge)
        fig, axes = plt.subplots(figsize=(6, 5))
        plot1d(axes, x_analytic, np.ones_like(x_analytic)*q_minor_analytic,
               plot_label='Analytic',
               x_label='Abscissae [m]',
               y_label='Low Flow Discharge [m3/s]',
               linestyle='-', color='k',)
        plot1d(axes, x_sarap, q_minor_sarap,
               plot_label='SARAP',
               x_label='Abscissae [m]',
               y_label='Low Flow Discharge [m3/s]',
               linestyle='--', color='b',)
        plot1d(axes, x_exp, q_minor_exp,
               plot_label='Transcritical Explicit (Nc=0.8)',
               x_label='Abscissae [m]',
               y_label='Low Flow Discharge [m3/s]',
               linestyle='-.', color='r',)
        plot1d(axes, x_imp, q_minor_imp,
               plot_label='Transcritical Implicit (Nc=2)',
               x_label='Abscissae [m]',
               y_label='Low Flow Discharge [m3/s]',
               linestyle=':', color='g',)
        axes.legend()
        plt.tight_layout()
        plt.savefig(path.join('.', 'img', 'qm.png'))
        plt.close(fig)

        # Plotting flow qM (major discharge)
        fig, axes = plt.subplots(figsize=(6, 5))
        plot1d(axes, x_analytic, np.ones_like(x_analytic)*q_major_analytic,
               plot_label='Analytic',
               x_label='Abscissae [m]',
               y_label='Debit majeur [m3/s]')
        plot1d(axes, x_sarap, q_major_sarap,
               plot_label='SARAP',
               x_label='Abscissae [m]',
               y_label='Hight flow discharge [m3/s]',
               linestyle='--', color='b',)
        plot1d(axes, x_exp, q_major_exp,
               plot_label='Transcritical kernel Expl. (Nc=0.8)',
               x_label='Abscissae [m]',
               y_label='Hight flow discharge [m3/s]',
               linestyle='-.', color='r',)
        plot1d(axes, x_imp, q_major_imp,
               plot_label='Transcritical kernel Impl. (Nc=2)',
               x_label='Abscissae [m]',
               y_label='Hight flow discharge [m3/s]',
               linestyle=':', color='g',)
        axes.legend()
        plt.tight_layout()
        plt.savefig(path.join('.', 'img', 'qM.png'))
        plt.close(fig)

        # EPSILON
        fig, axes = plt.subplots(figsize=(6, 5))
        plot1d(axes, x_sarap, epsilon_sarap,
               plot_label='SARAP',
               x_label='Abscissae [m]',
               y_label='$\\epsilon$ [%]',
               linestyle='--', color='b',)
        plot1d(axes, x_exp, epsilon_exp,
               plot_label='Transcritical Explicit (Nc=0.8)',
               x_label='Abscissae [m]',
               y_label='$\\epsilon$ [%]',
               linestyle='-.', color='r',)
        plot1d(axes, x_imp, epsilon_imp,
               plot_label='Transcritical Implicit (Nc=2)',
               x_label='Abscissae [m]',
               y_label='$\\epsilon$ [%]',
               linestyle=':', color='g',)
        axes.legend()
        plt.tight_layout()
        plt.savefig(path.join('.', 'img', 'epsilon_cote.png'))
        plt.close(fig)
