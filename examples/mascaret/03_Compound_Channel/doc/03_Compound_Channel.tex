\chapter{Compound Channel}

%--------------------------------------------------------------------------------------------------
\section{Purpose}
This test aims to validate the capability of \mascaret to accurately simulate steady-state flow in a compound rectangular channel, which consists of both a low flow and a high flow channel. The validation focuses on implementing and assessing the Debord model within \mascaret, which determines the distribution of discharge between these channels based on hydraulic roughness characteristics. The Debord model was established through experimental studies of uniform flow regimes.\\
The study compares results from the steady kernel and the transcritical kernel (both implicit and explicit forms) against an analytical solution to ensure accuracy. 

%--------------------------------------------------------------------------------------------------
\section{Description}

\subsection*{Geometry}

The domain is a rectangular channel of $4990~\mathrm{m}$ long and $1~\mathrm{m}$ width without slope.\\
The geometry is described by 2 cross section located at $X = 0~\mathrm{m}$ and $X = 4990~\mathrm{m}$. The distribution between the low flow channel and the high flow channel is as follows (Figure~\ref{mascaret:test3:geometry}):

\begin{figure}[htbp]
	\centering
	\begin{tikzpicture}
		% Axes
		\draw[thick] (0,0) -- (10,0); % Allonge l'axe horizontal
		\draw[thick] (0,0) -- (0,2);
		\draw[thick] (10,0) -- (10,2); % Ajuste la position de l'axe vertical droit
		
		% Labels
		\node at (0, -0.2) {0};
		\node at (5, -0.2) {0,5}; % Ajuste la position de l'étiquette 0,5
		\node at (10, -0.2) {1}; % Ajuste la position de l'étiquette 1
		
		% Lit mineur
		\draw[thick, ->] (0.2,1.2) -- (5,1.2); % Ajuste la position de la flèche lit mineur
		\draw[thick, <-] (0,1.2) -- (4.8,1.2); % Ajuste la position de la flèche lit mineur
		\node at (2.5, 1.4) {low flow channel}; % Ajuste la position du texte lit mineur
		
		% Lit majeur
		\draw[thick, ->] (5,0.8) -- (9.8,0.8); % Ajuste la position de la flèche lit majeur
		\draw[thick, <-] (5.2,0.8) -- (10,0.8); % Ajuste la position de la flèche lit majeur
		\node at (7.5, 1) {high flow channel}; % Ajuste la position du texte lit majeur
		
		% Ligne pointillée au centre
		\draw[dashed] (5, 0) -- (5, 2); % Ajuste la position de la ligne pointillée
	\end{tikzpicture}
	\caption{Diagram of channel's geometry with low flow channel and the high flow channel.}
	\label{mascaret:test3:geometry}
\end{figure}

\subsection*{Initial condition}
No initial condition for the steady-state kernel (not requested).

Constant discharge equal to $1~\mathrm{m^{3}.s^{-1}}$ and water level linearly varying from $2~\mathrm{m}$ to $1~\mathrm{m}$ for the transient kernels.

\subsection*{Boundary condition}
Upstream: Constant discharge, $Q = 1~\mathrm{m^{3}.s^{-1}}$.\\
Downstream: Constant water level, $H = 1~\mathrm{m}$.

\subsection*{Physical parameters}
Strickler coefficients:
\begin{itemize} 
	\item Low flow channel: $K_m = 90~\mathrm{m^{1/3}.s^{-1}}$
	\item High flow channel: $K_M = 20~\mathrm{m^{1/3}.s^{-1}}$
\end{itemize}

The Debord model is used for flow distribution. There is no friction on the vertical walls.

\subsection*{Numerical parameters}
The mesh is define with a uniform spatial step of $10~\mathrm{m}$.\\
The vertical discretization of the cross-section is uniform across the domain and set at $10~\mathrm{cm}$.

No time step parameters for the steady kernel (not requested).\\
For the transcritical kernel : the simulation duration is set to $2000$ time steps. The time step is variable with a wishes Courant number of $0.8$ for the explicit form and a wishes Courant number of $2$ for the implicit form.

%--------------------------------------------------------------------------------------------------
\section{Results}
\subsection*{Analytic solution with Debord Model}
The Debord model calculates the flow distribution between the low and high flow channels using the following equations:
\begin{subequations}
	\begin{align}
		Q_{m}=\frac{Q}{1+\eta}\\
		Q_{M}=Q-Q_{m}
	\end{align}
\end{subequations}

Where:

\begin{align}
	\eta =\frac{DEB_m}{DEB_M}
\end{align}

The generalized conveyances are defined as:
\begin{align}
	D_{mg}=\frac{1}{2}K_{m}ALh^{5/3}
\end{align}

\begin{align}
	D_{Mg}={1}{2}K_{M}L\sqrt{2-A^{2}}h^{5/3}
\end{align}

Where:

\begin{align}
	A=0.9\left(\frac {K_{M}}{K_{m}}\right)^{1/6}
\end{align}

Numerical application provides the flow distribution as follows:
\begin{subequations}
	\begin{align}
		Q_{M} = 0.2805~\mathrm{m^3.s^{-1}}\\
		Q_{m} = 0.7195~\mathrm{m^3.s^{-1}}.
	\end{align}
\end{subequations}

In steady state, the Saint-Venant equation system simplifies to the dynamic equation. Utilizing the distribution between the low flow channel and the high flow channel, the water height $y$ is computed using the Saint-Venant equations and an approximation via the classical fourth-order Runge-Kutta method (RK4):

\begin{align}
	f(y)= \frac{2g}{y^{1/3}}\left(\frac{Q_m^2}{K_m^2}+\frac{Q_M^2}{K_M^2}\right) / \left(2\left(Q_m^2+Q_M^2\right)-gL^2y0^3\right)
\end{align}

\begin{align}
	y^{n-1} = y^{n} - \frac{\Delta x}{6}\left(k_1+2k_2+2k_3+k_4 \right)
\end{align}

where

\begin{subequations}
	\begin{align}
		k_1 = f\left(y^{n}\right) \\
		k_2 = f\left(y^{n}-\frac{\Delta x}{2}k_1\right) \\
		k_3 = f\left(y^{n}-\frac{\Delta x}{2}k_2\right) \\
		k_4 = f\left(y^{n}-\Delta x k_3\right)
	\end{align}
\end{subequations}

\subsection*{Validation}
Figure~\ref{mascaret:test3:depth} show the comparison of water height with the different kernel. The results are consistent.
The comparison of all solutions to the analytical one show the good agreement of all kernels (Figure~\ref{mascaret:test3:epsilon}) with an error of less than $0.1\%$.
Similarly, the computed discharges with different kernels match well with the analytical values, as illustrated in Figures~\ref{mascaret:test3:Qm} and \ref{mascaret:test3:QM}.

\begin{figure}[H]
	\centering
	\includegraphicsmaybe{[width=0.75\textwidth]}{../img/depth.png}
	\caption{Comparison of analytic and computed water height for the three kernels.}
	\label{mascaret:test3:depth}
\end{figure}

\begin{figure}[H]
	\centering
	\includegraphicsmaybe{[width=0.75\textwidth]}{../img/epsilon_cote.png}
	\caption{Percentage error between Runge-Kutta solution and mascaret solution according to the kernels.}
	\label{mascaret:test3:epsilon}
\end{figure}


\begin{figure}[H]
	\centering
	\includegraphicsmaybe{[width=0.75\textwidth]}{../img/Qm.png}
	\caption{Comparison of analytic and computed discharge in the low flow channel for the three kernels.}
	\label{mascaret:test3:Qm}
\end{figure}

\begin{figure}[H]
	\centering
	\includegraphicsmaybe{[width=0.75\textwidth]}{../img/QM.png}
	\caption{Comparison of analytic and computed discharge in the high flow channel for the three kernels.}
	\label{mascaret:test3:QM}
\end{figure}

%--------------------------------------------------------------------------------------------------
\section*{Conclusion}
The distribution between the low flow channel and the high flow channel, modeled using the Debord model, is accurately represented across all three kernels. The results demonstrate the effectiveness of \mascaret in simulating flow dynamics in composite channels.