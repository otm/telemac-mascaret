
"""
Validation script for test15
"""
from os import path
import numpy as np
import matplotlib.pyplot as plt
from vvytel.vnv_study import AbstractVnvStudy
from postel.plot1d import plot1d


class VnvStudy(AbstractVnvStudy):
    """
    Class for validation
    """

    def _init(self):
        """
        Defines the general parameter
        """
        self.rank = 2
        self.tags = ['mascaret']

    def _pre(self):
        """
        Defining the studies
        """

        # Test15 transcritical kernel
        self.add_study('vnv_1',
                       'mascaret',
                       'mascaret.xcas')

        # Test15 rezo kernel
        self.add_study('vnv_2',
                       'mascaret',
                       'rezo.xcas')

        # Test15 rezo kernel without confluence
        self.add_study('vnv_3',
                       'mascaret',
                       'rezo_sous_tirage.xcas')

    def _check_results(self):
        """
        Comparison with reference file
        """
        self.check_epsilons('vnv_1:res',
                            'ref/mascaret0.rub',
                            eps=[1.E-3],
                            masc=True)

        self.check_epsilons('vnv_2:res',
                            'ref/rezo.rub',
                            eps=[1.E-3],
                            masc=True)

        self.check_epsilons('vnv_3:res',
                            'ref/rezo_sous_tirage.rub',
                            eps=[1.E-3],
                            masc=True)

    def _post(self):
        """
        Post-treatment processes
        """

        # Getting results from transcritical kernel
        trans_res, _ = self.get_study_res('vnv_1:mascaret0.rub', 'mascaret')
        var_pos = trans_res.get_position_var('Cote de l eau')

        x_trans1 = trans_res.reaches[1].get_section_pk_list()
        x_trans2 = trans_res.reaches[2].get_section_pk_list()
        cote_trans1 = trans_res.get_values_at_reach(-1, 1, [var_pos])
        cote_trans2 = trans_res.get_values_at_reach(-1, 2, [var_pos])
        x_trans = np.append(x_trans1, x_trans2)
        cote_trans = np.append(cote_trans1, cote_trans2)

        # Getting results from rezo kernel
        rezo_res, _ = self.get_study_res('vnv_2:mascaret0.rub', 'mascaret')
        var_pos = rezo_res.get_position_var('Cote de l eau')

        x_rezo1 = rezo_res.reaches[1].get_section_pk_list()
        x_rezo2 = rezo_res.reaches[2].get_section_pk_list()
        cote_rezo1 = rezo_res.get_values_at_reach(-1, 1, [var_pos])
        cote_rezo2 = rezo_res.get_values_at_reach(-1, 2, [var_pos])
        x_rezo = np.append(x_rezo1, x_rezo2)
        cote_rezo = np.append(cote_rezo1, cote_rezo2)

        # Getting results from rezo kernel without diffluence
        subtitling_flow_res, _ = self.get_study_res(
            'vnv_3:mascaret0.rub', 'mascaret')
        x_subtitling_flow = subtitling_flow_res.reaches[1].get_section_pk_list()
        var_pos = subtitling_flow_res.get_position_var('Cote de l eau')
        cote_subtitling_flow = subtitling_flow_res.get_values_at_reach(-1, 1, [
                                                                       var_pos])

        # =====================================================================
        # DISPLAY
        # =====================================================================
        fig, axe = plt.subplots(figsize=(6, 5))
        plot1d(axe, x_trans, cote_trans,
               plot_label='Transcrtical',
               x_label='$X$ [m]',
               y_label='Water level [m]',
               linestyle='-', color='k',
               )
        plot1d(axe, x_rezo, cote_rezo,
               plot_label='REZO',
               x_label='$X$ [m]',
               y_label='Water level [m]',
               linestyle='--', color='r',
               )
        plot1d(axe, x_subtitling_flow, cote_subtitling_flow,
               plot_label='REZO without diffluence',
               x_label='$X$ [m]',
               y_label='Water level [m]',
               linestyle='-.', color='b',
               )
        axe.legend()
        plt.tight_layout()
        plt.savefig(path.join('.', 'img', 'cote.png'))
        plt.close(fig)
