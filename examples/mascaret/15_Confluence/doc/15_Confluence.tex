\chapter{Distributary with Unsteady Flow}

%--------------------------------------------------------------------------------------------------
\section{Purpose}
This test case is designed to validate the transcritical kernel's ability to accurately model a distributary zone under unsteady diffluent flow conditions. The primary objective is to ensure the kernel's effectiveness in handling flow splits and merges during transient states.
Additionally, the test aims to validate the unsteady REZO kernel within a subcritical flow regime by comparing the resulting water surface profile against that obtained from the transcritical kernel. This comparison is crucial for ensuring consistency and reliability across different flow regimes.
Lastly, a simulation is conducted without the confluence, incorporating a flow subtraction to represent the distributary's effect on the main channel's flow.

%--------------------------------------------------------------------------------------------------
\section{Description}

\subsection*{Geometry}

The model consists of three reaches (see Figure~\ref{mascaret:test15:geometry}). Reaches 1 and 2 correspond to the main channel, with reach 1 upstream of the diffluence and reach 2 downstream. The third reach is a distributary.\\
The main channel is rectangular with a uniform slope of $0.001$, a length of $10.8~\mathrm{m}$, and a width of $0.8~\mathrm{m}$. The cross sections are defined by four profiles: one at the upstream A-B, one at the downstream E-G, and two profiles, C-D and E-F, that define the diffluence zone (as seen in Figure~\ref{mascaret:test15:geometry}).\\
The distributary reach is also a rectangular channel with a uniform slope of $0.001$, a length of $3~\mathrm{m}$, and a width that gradually increases to a constant width of $0.3~\mathrm{m}$. The cross sections are defined by two profiles: one at the upstream F-K and one at the downstream I-J.\\
The diffluence is positioned at the abscissa $X=7.2~\mathrm{m}$.

\begin{figure}[H] 
	\begin{center}
		\includegraphicsmaybe{[width=10cm]}{Figure1.eps}
		\caption{Model diagram with a distributary.} 
		\label{mascaret:test15:geometry}
	\end{center}
\end{figure}

The main variables are, as see in Figure~\ref{mascaret:test15:geometry}
\begin{itemize}
	\item $Q_{p}$: Inflow discharge $0.03~\mathrm{m^{3}.s^{-1}}$.
	\item $L_{p}$: Width of the downstream channel.
	\item $Q_{l}$: Discharge in the distributary $0.01~\mathrm{m^{3}.s^{-1}}$.
	\item $L_{l}$: Width of the distributary.
	\item $\alpha = 120^\circ$.
	\item $Q_{t}=Q_{p}-Q_{l}$: Outflow discharge.
\end{itemize}

The model uses a single riverbed configuration, without hight flow channel.

\subsection*{Initial condition}
An initial water level is imposed, with $H=0.205~\mathrm{m}$, $Q_{p}=0.03~\mathrm{m^3.s^{-1}}$, $Q_{l}=0.01~\mathrm{m^3.s^{-1}}$ and $Q_{t}=0.02~\mathrm{m^3.s^{-1}}$.

\subsection*{Boundary condition}
Upstream of the main channel (reach 1): Constant discharge, $ Q_{p} = 0.03~\mathrm{m^{3}.s^{-1}}$.\\
Downstream of the main channel (reach 2): Constant water level, $\mathrm{Water~level}_\mathrm{t} = 0.91~\mathrm{m}$.\\
Downstream of the distributary (reach 3): Constant discharge, $ Q_{l} = 0.01~\mathrm{m^{3}.s^{-1}}$.

\subsection*{Physical parameters}
Strickler coefficient: $K_m = 62~\mathrm{m^{1/3}.s^{-1}}$ in all reachs.\\
There is no friction on the vertical walls.

\subsection*{Diffluence modeling}

\subsubsection*{Transcritical kernel Confluence Modeling}
The 2D Saint-Venant equations replace the 1D equations at and around the junction, approximating the confluence's effect using a 2D mesh of 12 cells (see Figure~\ref{mascaret:test15:confluence} and Table~\ref{mascaret:test15:confluence_geometrie}). The 2D Saint-Venant equations are solved using a finite volume module equivalent to that used in 1D (Roe scheme).

\begin{figure}[H]
	\begin{center}
		\includegraphicsmaybe{[width=0.75\textwidth]}{Figure2.eps}
		\caption{Confluence zone modeling for the transcritical kernel.} 
		\label{mascaret:test15:confluence}
	\end{center}
\end{figure}

\begin{table}[H] 
	\begin{center} 
		\caption{Geometry of the confluence for transcritical kernel} 
		\label{mascaret:test15:confluence_geometrie}
		\begin{tabular}{|c|c|c|} 
			\hline & Coordinates $X;Y$ ($\mathrm{m}$) & Angle\\
			\hline Main reach upstream (Reach~1) & $-0.3608~;~0$ & $180^\circ$\\ 
			\hline Main reach downstream (Reach~2) & $0.2165~;~0$ & $0^\circ$\\ 
			\hline Affluent (Reach~3) & $0.05~;~059$ & $60^\circ$\\ 
			\hline
		\end{tabular} 
	\end{center} 
\end{table}

This method introduces a potential non-conservation of momentum.

\subsubsection*{REZO kernel Confluence Modeling}
The confluence zone assumes equality of water elevations in each branch and continuity of discharges. The confluence is reduced to a single point by placing cross-sections as close as possible to the junction and introducing singular head losses to account for energy dissipation.
Two additional cross-sections are added to the main channel: one on the reach 1 (upstream reach of the main channel) at $X=7.16~\mathrm{m}$; a second, on the reach 2 (downstream reach of the main channel), at $X=7.2~\mathrm{m}$.
To summarise, for the unsteady kernel REZO, the main channel is defined by 6 cross-sections:
\begin{itemize}
	\item Reach 1: $X=0~\mathrm{m}$, $X=6.8~\mathrm{m}$, and $X=7.16~\mathrm{m}$
	\item Reach 2: $X=7.2~\mathrm{m}$, $X=7.3773~\mathrm{m}$, and $X=10.8~\mathrm{m}$
\end{itemize}


\subsubsection*{Flow Subtraction for REZO kernel without Confluence}
To model the effect of a distributary without adding a diffluence, a singularity representing a local inflow is imposed at $X=7.2~\mathrm{m}$. The flow is negative ($Q_{l}=-0.01~\mathrm{m^{3}.s^{-1}}$) to simulate the effect of a distributary.
In this configuration, the distributary is represented only by its lateral contribution of flow, without contribution of momentum. The momentum is therefore not conserved

\subsection*{Numerical parameters}
The mesh is define with a uniform spatial step of $0.05~\mathrm{m}$.\\
The vertical discretization of the cross-section is uniform across the domain and set at $0.01~\mathrm{m}$.

The unsteady REZO kernel uses a simulation duration of $1000$ time steps with a time step of $10~\mathrm{s}$.\\
For the transcritical kernel, the simulation is conducted over $10000$ time steps, with a variable time step governed by a target Courant number of $0.8$.

The transcritical kernel is used in its explicit form.

%--------------------------------------------------------------------------------------------------
\section{Results}
Figure~\ref{mascaret:test15:cote} compares the converged water elevations. The transcritical kernel shows a lowering of the water level at the diffluence due to the head difference. In contrast, the unsteady REZO kernel imposes equal elevations at the junction. In the simulation where the distributary is modeled with flow subtraction, a significant drop in the water surface is observed, indicating a lack of momentum conservation.

\begin{figure}[H]
	\centering
	\includegraphicsmaybe{[width=0.75\textwidth]}{../img/cote.png}
	\caption{Comparison of water levels.}
	\label{mascaret:test15:cote}
\end{figure}

%--------------------------------------------------------------------------------------------------
\section*{Conclusion}
The transcritical kernel effectively models the diffluence zone, while the unsteady REZO kernel provides an accurate downstream representation. However, the REZO kernel's handling of upstream water levels does not align with the transcritical model's predictions.
