
"""
Validation script for test8
"""
from os import path

import matplotlib.pyplot as plt
import numpy as np

from postel.plot1d import plot1d
from vvytel.vnv_study import AbstractVnvStudy


class VnvStudy(AbstractVnvStudy):
    """
    Class for validation
    """

    def _init(self):
        """
        Defines the general parameter
        """
        self.rank = 0
        self.tags = ['mascaret']

    def _pre(self):
        """
        Defining the studies
        """

        # Test8 permanent kernel
        self.add_study('vnv_1',
                       'mascaret',
                       'sarap.xcas')

        # Test8 unsteady subcritical kernel
        self.add_study('vnv_2',
                       'mascaret',
                       'rezo.xcas')

    def _check_results(self):
        """
        Comparison with reference file
        """
        self.check_epsilons('vnv_1:res',
                            'ref/sarap_ref.opt',
                            eps=[1.E-3],
                            masc=True)

        self.check_epsilons('vnv_2:res',
                            'ref/rezo_ref.opt',
                            eps=[1.E-3],
                            masc=True)

    def _post(self):
        """
        Post-treatment processes
        """

        sarap_res, _ = self.get_study_res(
            'vnv_1:sarap_ecr.opt', 'mascaret')
        x_sarap = sarap_res.reaches[1].get_section_pk_list()
        var_pos = sarap_res.get_position_var("Cote de l eau")
        cote_sarap = sarap_res.get_values_at_reach(-1, 1, [var_pos])
        index_weir = np.argmax(x_sarap >= 4000)
        zup_sarap = cote_sarap[index_weir]
        zdown_sarap = cote_sarap[index_weir+1]

        rezo_res, _ = self.get_study_res(
            'vnv_2:rezo_ecr.opt', 'mascaret')
        x_rezo = rezo_res.reaches[1].get_section_pk_list()
        var_pos = rezo_res.get_position_var("Cote de l eau")
        cote_rezo = rezo_res.get_values_at_reach(-1, 1, [var_pos])
        index_weir = np.argmax(x_rezo >= 4000)
        zup_rezo = cote_rezo[index_weir]
        zdown_rezo = cote_rezo[index_weir+1]

        # =====================================================================
        # Compute analytical data
        # =====================================================================
        x_uniform = [0, 5000]
        z_uniform = [15, 12.5]
        z_up = 13.5
        z_down = 12.95

        # =====================================================================
        # TABLE
        # =====================================================================

        with open(path.join('.', 'img', 'comparaison.txt'), 'w') as file:
            # Write header
            file.write(
                " & Analytic solution & SARAP & REZO \\\\\n")
            file.write("\\hline\n")
            file.write(
                f"$Z$ Upstream weir [m]& {z_up:.3f} & {zup_sarap[0]:.3f} & {zup_rezo[0]:.3f} \\\\\n")
            file.write(
                f"$Z$ Downstream weir [m] & {z_down:.3f} & {zdown_sarap[0]:.3f} & {zdown_rezo[0]:.3f}")

        # =====================================================================
        # DISPLAY
        # =====================================================================
        fig, axe = plt.subplots(figsize=(6, 5))
        plot1d(axe, x_uniform, z_uniform,
               plot_label='Uniform flow',
               x_label='Abscissae [m]',
               y_label='Water level [m]',
               linestyle='-.', linewidth=2,
               marker='None',
               color='g',
               )
        plot1d(axe, x_sarap, cote_sarap,
               plot_label='SARAP',
               x_label='Abscissae [m]',
               y_label='Water level [m]',
               linestyle='--', linewidth=3,
               marker='None',
               color='r',
               )
        plot1d(axe, x_rezo, cote_rezo,
               plot_label='REZO',
               x_label='Abscissae [m]',
               y_label='Water level [m]',
               linestyle=':', linewidth=3,
               marker='None',
               color='b',
               )
        axe.legend()
        plt.tight_layout()
        plt.savefig(path.join('.', 'img', 'cote.png'))
        plt.close(fig)
