
"""
Validation script for test4
"""
from os import path

import matplotlib.pyplot as plt
import numpy as np

from postel.plot1d import plot1d
from vvytel.vnv_study import AbstractVnvStudy


def compute_analytic_solution():

    # Constants
    strickler_minor = 90.  # Roughness coefficient of the low flow channel
    strickler_major = 20.  # Roughness coefficient of the high flow channel
    l_minor = 0.5
    l_major = 0.3
    downstream_height = 1  # Downstream water height [m]
    upstream_flow = 1  # Upstream discharge [m^3/s]
    reach_length = 4990  # Reach length [m]

    x_current = reach_length
    y_current = downstream_height
    step_size = 0.01
    x_analytic = np.arange(x_current, -step_size, -step_size)
    y_analytic = np.zeros_like(x_analytic)
    y_analytic[0] = y_current

    ratio = 0.9 * (strickler_major /
                   strickler_minor) ** (1./6.)
    d_minor_by_d_major = (strickler_minor/strickler_major)*(l_minor/l_major)

    eta = ratio/(np.sqrt(1+l_minor/l_major*(1-ratio**2)))*d_minor_by_d_major
    q_major_analytic = upstream_flow / (1 + eta)
    q_minor_analytic = upstream_flow - q_major_analytic

    return round(q_major_analytic, 5), round(q_minor_analytic, 5)


class VnvStudy(AbstractVnvStudy):
    """
    Class for validation
    """

    def _init(self):
        """
        Defines the general parameter
        """
        self.rank = 0
        self.tags = ['mascaret']

    def _pre(self):
        """
        Defining the studies
        """

        # Test4 transcritical kernel
        self.add_study('vnv_1',
                       'mascaret',
                       'mascaret.xcas')

    def _check_results(self):
        """
        Comparison with reference file
        """
        self.check_epsilons('vnv_1:res',
                            'ref/mascaret_ref.opt',
                            eps=[1.E-3],
                            masc=True)

    def _post(self):
        """
        Post-treatment processes
        """
        masc_res, _ = self.get_study_res(
            'vnv_1:mascaret_ecr.opt', 'mascaret')
        x_masc = masc_res.reaches[1].get_section_pk_list()
        var_pos = masc_res.get_position_var("Cote de l eau")
        cote_masc = masc_res.get_values_at_reach(-1, 1, [var_pos])
        var_pos = masc_res.get_position_var("Debit mineur")
        q_minor_masc = np.mean(masc_res.get_values_at_reach(-1, 1, [var_pos]))
        var_pos = masc_res.get_position_var("Debit majeur")
        q_major_masc = np.mean(masc_res.get_values_at_reach(-1, 1, [var_pos]))

        # =====================================================================
        # Compute analytical data
        # =====================================================================
        q_major_analytic, q_minor_analytic = compute_analytic_solution()
        epsilon_minor = 100*(q_minor_masc-q_minor_analytic)/q_minor_analytic
        epsilon_major = 100*(q_major_masc-q_major_analytic)/q_major_analytic

        # =====================================================================
        # TABLE
        # =====================================================================

        with open(path.join('.', 'img', 'comparaison.txt'), 'w') as file:
            # Write header
            file.write(""" & Analytic solution [m3/s] &
                       Steady-state SARAP [m3/s] &
                       Difference [$\\%$] \\\\\n""")
            file.write("\\hline\n")
            file.write(f"""$q_m$ & {q_minor_analytic:.4f} &
                       {q_minor_masc:.4f} &
                       {epsilon_minor:.2f} \\\\\n""")
            file.write(f"""$q_M$ & {q_major_analytic:.4f} &
                       {q_major_masc:.4f} &
                       {epsilon_major:.2f}""")

        # =====================================================================
        # DISPLAY
        # =====================================================================
        fig, axe = plt.subplots(figsize=(6, 5))
        plot1d(axe, x_masc, cote_masc,
               plot_label='Steady-state kernel',
               x_label='Abscissae [m]',
               y_label='Water level [m]',
               linestyle='--', linewidth=3,
               marker='None',
               color='r',
               )
        axe.legend()
        plt.tight_layout()
        plt.savefig(path.join('.', 'img', 'cote.png'))
        plt.close(fig)
