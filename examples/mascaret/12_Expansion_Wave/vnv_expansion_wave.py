"""
Validation script for test12
"""
from os import path
import numpy as np
import matplotlib.pyplot as plt
from vvytel.vnv_study import AbstractVnvStudy
from postel.plot1d import plot1d


class VnvStudy(AbstractVnvStudy):
    """
    Class for validation
    """

    def _init(self):
        """
        Defines the general parameter
        """
        self.rank = 0
        self.tags = ['mascaret']

    def _pre(self):
        """
        Defining the studies
        """

        # Test12 unsteady subcritical kernel
        self.add_study('vnv_1',
                       'mascaret',
                       'rezo.xcas')

        # Test12 transcritical implicit kernel
        self.add_study('vnv_2',
                       'mascaret',
                       'mascaret_imp.xcas')

        # Test12 transcritical explicit kernel
        self.add_study('vnv_3',
                       'mascaret',
                       'mascaret_exp.xcas')

    def _check_results(self):
        """
        Comparison with reference files
        """
        self.check_epsilons('vnv_1:res',
                            'ref/rezo_ref.opt',
                            eps=[1.E-3],
                            masc=True)

        self.check_epsilons('vnv_2:res',
                            'ref/mascaret_imp_ref.opt',
                            eps=[1.E-3],
                            masc=True)

        self.check_epsilons('vnv_3:res',
                            'ref/mascaret_exp_ref.opt',
                            eps=[1.E-3],
                            masc=True)

    def _post(self):
        """
        Post-treatment processes
        """

        rezo_res, _ = self.get_study_res('vnv_1:rezo_ecr.opt', 'mascaret')
        x_rezo = rezo_res.reaches[1].get_section_pk_list()
        t_rezo = rezo_res.times
        var_pos = rezo_res.get_position_var("Cote de l eau")
        z_rezo_end = rezo_res.get_values_at_reach(-1, 1, [var_pos])
        z_rezo_amont = rezo_res.get_series(1, 1, [var_pos])
        var_pos = rezo_res.get_position_var('Debit total')
        q_rezo_end = rezo_res.get_values_at_reach(-1, 1, [var_pos])

        masc_imp_res, _ = self.get_study_res(
            'vnv_2:mascaret_imp_ecr.opt', 'mascaret')
        x_masc_imp = masc_imp_res.reaches[1].get_section_pk_list()
        t_masc_imp = masc_imp_res.times
        var_pos = masc_imp_res.get_position_var("Cote de l eau")
        z_masc_imp_end = masc_imp_res.get_values_at_reach(-1, 1, [var_pos])
        z_masc_imp_amont = masc_imp_res.get_series(1, 1, [var_pos])
        var_pos = masc_imp_res.get_position_var('Debit total')
        q_masc_imp_end = masc_imp_res.get_values_at_reach(-1, 1, [var_pos])

        masc_exp_res, _ = self.get_study_res(
            'vnv_3:mascaret_exp_ecr.opt', 'mascaret')
        x_masc_exp = masc_exp_res.reaches[1].get_section_pk_list()
        t_masc_exp = masc_exp_res.times
        var_pos = masc_exp_res.get_position_var("Cote de l eau")
        z_masc_exp_end = masc_exp_res.get_values_at_reach(-1, 1, [var_pos])
        z_masc_exp_amont = masc_exp_res.get_series(1, 1, [var_pos])
        var_pos = masc_exp_res.get_position_var('Debit total')
        q_masc_exp_end = masc_exp_res.get_values_at_reach(-1, 1, [var_pos])

        # =====================================================================
        # UPSTREAM TABLE
        # =====================================================================
        time = [200, 400, 600, 800, 1000]
        h_analytic = [4.718, 4.445, 4.180, 3.923, 3.674]

        h_rezo = np.interp(time, t_rezo, np.array(z_rezo_amont).ravel()-10)
        h_masc_imp = np.interp(
            time, t_masc_imp, np.array(z_masc_imp_amont).ravel()-10)
        h_masc_ext = np.interp(
            time, t_masc_exp, np.array(z_masc_exp_amont).ravel()-10)

        with open(path.join('.', 'img', 'amont.txt'), 'w') as file:
            # Write header
            file.write(
                "Time [s] & Analytical & REZO  & MASCARET (IMP.) & MASCARET (EXP.)")

            # Write data
            for t, ha, hr, hmi, hme in zip(time, h_analytic, h_rezo, h_masc_imp, h_masc_ext):
                file.write(
                    f"\\\\\n {t} & {ha:.3f} m & {hr:.3f} m & {hmi:.3f} m & {hme:.3f} m")
        # =====================================================================
        # WAVE FRONT TABLE
        # =====================================================================
        q_pos = 1974
        x_analytic = 9000
        x_front_rezo = x_rezo[np.argmax(np.array(q_rezo_end) >= q_pos)]
        x_front_masc_imp = x_masc_imp[np.argmax(
            np.array(q_masc_imp_end) >= q_pos)]
        x_front_masc_exp = x_masc_exp[np.argmax(
            np.array(q_masc_exp_end) >= q_pos)]

        with open(path.join('.', 'img', 'front.txt'), 'w') as file:
            # Write header
            file.write(
                "Analytical & REZO  & MASCARET (IMP.) & MASCARET (EXP.) \\\\\n")
            # Write data
            file.write(
                f"{x_analytic:.0f} m & {x_front_rezo:.0f} m & {x_front_masc_imp:.0f} m & {x_front_masc_exp:.0f} m")

        # =====================================================================
        # DISPLAY
        # =====================================================================
        fig, axe = plt.subplots(figsize=(6, 5))
        plot1d(axe, x_rezo, z_rezo_end,
               plot_label='REZO',
               x_label='$X$ [m]',
               y_label='$Z$ [m]',
               linestyle='-', color='b',)
        plot1d(axe, x_masc_imp, z_masc_imp_end,
               plot_label='MASCARET IMP.',
               x_label='$X$ [m]',
               y_label='$Z$ [m]',
               linestyle='--', color='r',)
        plot1d(axe, x_masc_exp, z_masc_exp_end,
               plot_label='MASCARET EXP.',
               x_label='$X$ [m]',
               y_label='$Z$ [m]',
               linestyle='-.', color='g',)
        axe.legend()
        plt.tight_layout()
        plt.savefig(path.join('.', 'img', 'cote.png'))
        plt.close(fig)

        fig, axe = plt.subplots(figsize=(6, 5))
        plot1d(axe, x_rezo, q_rezo_end,
               plot_label='REZO',
               x_label='$X$ [m]',
               y_label='$Q$ [m³/s]',
               linestyle='-', color='b',)
        plot1d(axe, x_masc_imp, q_masc_imp_end,
               plot_label='MASCARET IMP.',
               x_label='$X$ [m]',
               y_label='$Q$ [m³/s]',
               linestyle='--', color='r',)
        plot1d(axe, x_masc_exp, q_masc_exp_end,
               plot_label='MASCARET EXP.',
               x_label='$X$ [m]',
               y_label='$Q$ [m³/s]',
               linestyle='-.', color='g',)
        axe.legend()
        plt.tight_layout()
        plt.savefig(path.join('.', 'img', 'Q.png'))
        plt.close(fig)

        fig, axe = plt.subplots(figsize=(6, 5))
        plot1d(axe, t_rezo, z_rezo_amont,
               plot_label='REZO',
               x_label='TIME [s]',
               y_label='$Z$ [m]',
               linestyle='-', color='b',)
        plot1d(axe, t_masc_imp, z_masc_imp_amont,
               plot_label='MASCARET IMP.',
               x_label='TIME [s]',
               y_label='$Z$ [m]',
               linestyle='--', color='r',)
        plot1d(axe, t_masc_exp, z_masc_exp_amont,
               plot_label='MASCARET EXP.',
               x_label='TIME [s]',
               y_label='$Z$ [m]',
               linestyle='-.', color='g',)
        axe.legend()
        plt.title('$X = 0$ m')
        plt.tight_layout()
        plt.savefig(path.join('.', 'img', 'amont.png'))
        plt.close(fig)
