\chapter{Expansion Wave in a Rectangular Channel}

%--------------------------------------------------------------------------------------------------
\section{Purpose}
This test case aims to validate the unsteady kernel REZO and the transcritical kernel (both implicit and explicit forms) in the case of an expansion wave in a rectangular channel. The calculation domain $(x,t)$ is such that the downstream boundary condition has no influence (infinite channel).

%--------------------------------------------------------------------------------------------------
\section{Description}

\subsection*{Geometry}
The calculation is conducted in a rectangular channel with zero slope, extending over a length of $18~\mathrm{km}$ and a width of $200~\mathrm{m}$. The channel geometry is described by 2 cross-sections located at $X = 0~\mathrm{m}$ and $X = 18000~\mathrm{m}$.

\subsection*{Initial condition}
The initial condition is a steady, uniform flow with a constant discharge of $2000~\mathrm{m^{3}.s^{-1}}$ and a horizontal free surface with an elevation of $Z = 15~\mathrm{m}$.

\subsection*{Boundary condition}
Downstream: Constant water level, $H = 15~\mathrm{m}$.\\
Upstream discharge is imposed such that the velocity decreases linearly, as detailed in Table \ref{mascaret:test12:hydrograph}:

\begin{table}[H]
	\begin{center}
		\caption{Upstream imposed hydrograph.}
		\label{mascaret:test12:hydrograph}
		\begin{tabular}{|c|c|}
			\hline Time ($\mathrm{s}$) & Discharge ($\mathrm{m^{3}.s^{-1}}$)\\ 
			\hline $ 0 $ & $ 2000 $\\ 
			\hline $ 25 $ & $ 1936.1036 $\\ 
			\hline $ 50 $ & $ 1872.9678 $\\ 
			\hline $ 75 $ & $ 1810.5895 $\\ 
			\hline $ 100 $ & $ 1748.9646 $\\ 
			\hline $ 200 $ & $ 1509.9228 $\\ 
			\hline $ 300 $ & $ 1282.6300 $\\ 
			\hline $ 400 $ & $ 1066.8414 $\\ 
			\hline $ 500 $ & $ 862.3125 $\\ 
			\hline $ 600 $ & $ 668.7986 $\\ 
			\hline $ 700 $ & $ 496.0551 $\\ 
			\hline $ 800 $ & $ 313.8372 $\\ 
			\hline $ 900 $ & $ 151.9004 $\\ 
			\hline $ 1000 $ & $ 0. $\\ 
			\hline 
		\end{tabular}
	\end{center}
\end{table}

\subsection*{Physical parameters}
Friction is not considered in this test case.

\subsection*{Numerical parameters}
The mesh is define with a uniform spatial step of $90~\mathrm{m}$.\\
The vertical discretization of the cross-section is uniform across the domain and set at $0.25~\mathrm{m}$ ($40$ horizontal steps).

The time step is constant and equal to $5~\mathrm{s}$ and the calculation is carried out for $1000~\mathrm{s}$, i.e. 200 time steps.\\
Three simulations are performed: one with the REZO kernel and two with the transcritical kernel (one in its implicit form and the other in its explicit form).
 
%--------------------------------------------------------------------------------------------------
\section{Results}

\subsection*{Analytical solution}
The analytical solution can be calculated using the method of characteristics. This allows for a comparison between the simulated and expected wave behavior.
% TODO : Set the complete method
 
\subsection*{Validation}
Table \ref{mascaret:test12:upstream_water_level} presents a comparison of the time evolution of the water level at the upstream boundary with the analytical solution.

\begin{table}[H]
	\begin{center}
		\caption{Numerical comparison of analytic and computed water level at the upstream boundary.}
		\label{mascaret:test12:upstream_water_level}
		\begin{tabular}{l|c|c|c|c}
			\toprule
			\InputIfFileExists{../img/amont.txt}{}{}{}{}{}\\
			\bottomrule
		\end{tabular}
	\end{center}
\end{table}

Both the implicit and explicit schemes of the transcritical kernel show good agreement with the analytical solution.
Figures \ref{mascaret:test12:q} and \ref{mascaret:test12:cote} show the spatial evolution of the discharge and water level at time $T=1000~\mathrm{s}$.
\begin{figure}[H]
	\centering
	\includegraphicsmaybe{[width=0.75\textwidth]}{../img/Q.png}
	\caption{Longitudinal profiles of the discharge at $T=1000~\mathrm{s}$.}
	\label{mascaret:test12:q}
\end{figure}

\begin{figure}[H]
	\centering
	\includegraphicsmaybe{[width=0.75\textwidth]}{../img/cote.png}
	\caption{Longitudinal profiles of the water level at $T=1000~\mathrm{s}$.}
	\label{mascaret:test12:cote}
\end{figure}

At $T=1000~\mathrm{s}$, the analytical position of the wave front is $x_{\mathrm{analytic}} = 9000~\mathrm{m}$, defined by a discharge greater than $1974~\mathrm{m^{3}.s^{-1}}$. Table \ref{mascaret:test12:wave_front}, compares the wave front positions for the different kernels.

\begin{table}[H]
	\begin{center}
		\caption{Wavefront position at $T=1000~\mathrm{s}$ for different simulations.}
		\label{mascaret:test12:wave_front}
		\begin{tabular}{c|c|c|c}
			\toprule
			\InputIfFileExists{../img/front.txt}{}{}{}{}{}\\
			\bottomrule
		\end{tabular}
	\end{center}
\end{table}

The finite differences method of the REZO kernel is second order in space, resulting in less diffusion than the other two first-order schemes. This leads to a more accurate representation of the wave front.

%--------------------------------------------------------------------------------------------------
\section*{Conclusion}
The comparison between the different kernels and the analytical solution demonstrates the accuracy of both the REZO and transcritical kernels in simulating an expansion wave in a rectangular channel. Both kernels perform well, with the REZO kernel showing slightly better accuracy in wave front position due to its higher-order spatial discretization.
