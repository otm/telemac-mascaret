\chapter{Flood Propagation on a Compound Channel}

%--------------------------------------------------------------------------------------------------
\section{Purpose}
The purpose of this test case is to validate the unsteady fluvial kernel REZO in the context of non-permanent flow (flood propagation) in a compound channel. The flow is modeled over a length greater than the flood propagation distance to eliminate the influence of downstream boundary conditions.

%--------------------------------------------------------------------------------------------------
\section{Description}

\subsection*{Geometry}
The calculation is conducted in a prismatic channel with a uniform slope of $0.0001$, and a total length of $400~\mathrm{km}$. The channel geometry is defined by four cross-sections located at $X = 0~\mathrm{km}$, $X = 10~\mathrm{km}$, $X = 20~\mathrm{km}$ and $X = 400 ~\mathrm{km}$.\\
The channel consists of a low flow channel and a high flow channel, with the latter gradually deepening. A storage area is present on the right bank. Figure~\ref{mascaret:test14:geometry}) illustrates the channel's geometry. 

\begin{figure}[H]
	\centering
	\includegraphicsmaybe{[width=0.7\textwidth]}{../doc/channel.png}
	\caption{Diagram of channel's geometry with low flow channel and the high flow channel.}
	\label{mascaret:test14:geometry}
\end{figure}

Because of the storage zone, the results cannot be compared with the transcritical kernel.

\subsection*{Initial condition}
The initial condition is a steady, uniform flow with a constant discharge of $300~\mathrm{m^{3}.s^{-1}}$.

\subsection*{Boundary condition}
Upstream, the discharge is imposed according to the hydrograph in Table~\ref{mascaret:test14:hydrograph} (representing a flood wave): 

\begin{table}[H]
	\begin{center}
		\caption{Upstream imposed hydrograph.}
		\label{mascaret:test14:hydrograph}
		\begin{tabular}{|c|c|}
			\hline Time (s) & Discharge($~\mathrm{m^{3}.s^{-1}}$)\\ 
			\hline $ 0 $ & $ 300 $\\ 
			\hline $ 14400 $ & $ 1000 $\\ 
			\hline $ 28800 $ & $ 300 $\\ 
			\hline $ 43200 $ & $ 300 $\\ 
			\hline 
		\end{tabular} 
	\end{center}
\end{table}

Downstream, a water level/discharge law is imposed according based on the rating curve shown in Table~\ref{mascaret:test14:rating_curve}, derived from the uniform flow regime:

\begin{table}[H]
	\begin{center}
		\caption{Downstream imposed rating curve.}
		\label{mascaret:test14:rating_curve}
		\begin{tabular}{|c|c|}
			\hline Cote(m) & Discharge ($\mathrm{m^{3}.s^{-1}}$)\\ 
			\hline $ 18.5 $ & $ 100 $\\ 
			\hline $ 20.617 $ & $ 300 $\\ 
			\hline $ 22.814 $ & $ 1000 $\\ 
			\hline 
		\end{tabular} 
	\end{center} 
\end{table}

\subsection*{Physical parameters}
Strickler coefficients:
\begin{itemize} 
	\item Low flow channel: $K_m = 35~\mathrm{m^{1/3}.s^{-1}}$
	\item Hight flow channel: $K_M = 15~\mathrm{m^{1/3}.s^{-1}}$
\end{itemize}
The Debord model is used for flow distribution. There is no friction on the vertical walls.

\subsection*{Numerical parameters}
The mesh is define with a uniform spatial step of $500~\mathrm{m}$.\\
The vertical discretization of the cross-section is uniform in the domain equal to $25~\mathrm{cm}$ (40 horizontal steps).\\
The time step is $300~\mathrm{s}$, and the simulation runs for $12$~hours, resulting in $144$ time steps.

The simulation is conducted using the unsteady fluvial kernel REZO.
 
%--------------------------------------------------------------------------------------------------
\section{Results}
Figure~\ref{mascaret:test14:q_rezo}, shows the flow propagation at several cross-sections: the origin cross-section (where the flow boundary condition is imposed), the cross-section at $X = 5~\mathrm{km}$, the cross-section at $X = 10~\mathrm{km}$, and the cross-section at $X = 15~\mathrm{km}$.

\begin{figure}[H]
	\centering
	\includegraphicsmaybe{[width=0.75\textwidth]}{../img/qm_rezo.png}
	\caption{Flow propagation in the low flow channel.}
	\label{mascaret:test14:q_rezo}
\end{figure}

%--------------------------------------------------------------------------------------------------
\section*{Conclusion}
The results exhibit consistent physical behavior, validating the application of the transient fluvial kernel REZO for modeling flood propagation in a compound channel.