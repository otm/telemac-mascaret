
"""
Validation script for test14
"""
from os import path
import numpy as np
import matplotlib.pyplot as plt
from vvytel.vnv_study import AbstractVnvStudy
from postel.plot1d import plot1d


class VnvStudy(AbstractVnvStudy):
    """
    Class for validation
    """

    def _init(self):
        """
        Defines the general parameter
        """
        self.rank = 0
        self.tags = ['mascaret']

    def _pre(self):
        """
        Defining the studies
        """

        # Test14 unsteady subcritical kernel
        self.add_study('vnv_1',
                       'mascaret',
                       'rezo.xcas')

    def _check_results(self):
        """
        Comparison with reference file
        """
        self.check_epsilons('vnv_1:res',
                            'ref/rezo.rub',
                            eps=[1.E-3],
                            masc=True)

    def _post(self):
        """
        Post-treatment processes
        """
        # Getting results from mascaret file rezo
        rezo_res, _ = self.get_study_res('vnv_1:rezo.rub', 'mascaret')
        x_rezo = rezo_res.reaches[1].get_section_pk_list()
        indice_x_list = [0, np.argmax(x_rezo >= 5000), np.argmax(
            x_rezo >= 10000), np.argmax(x_rezo >= 15000)]
        t_rezo = rezo_res.times
        var_pos = rezo_res.get_position_var("Debit mineur")
        q_rezo = []
        for indice_x in indice_x_list:
            q_rezo.append(rezo_res.get_series(1, indice_x, [var_pos]))

        # =====================================================================
        # DISPLAY
        # =====================================================================
        fig, axe = plt.subplots(figsize=(6, 5))
        plot1d(axe, t_rezo, np.array(q_rezo)[0, :],
               plot_label='$X = 0$ m',
               x_label='TIME [s]',
               y_label='Q [m3/s]',
               linestyle='-', color='r',)
        plot1d(axe, t_rezo, np.array(q_rezo)[1, :],
               plot_label='$X = 5$ km',
               x_label='TIME [s]',
               y_label='Q [m3/s]',
               linestyle='--', color='b',)
        plot1d(axe, t_rezo, np.array(q_rezo)[2, :],
               plot_label='$X = 10$ km',
               x_label='TIME [s]',
               y_label='Q [m3/s]',
               linestyle=':', color='g',)
        plot1d(axe, t_rezo, np.array(q_rezo)[3, :],
               plot_label='$X = 15$ km',
               x_label='TIME [s]',
               y_label='Q [m3/s]',
               linestyle='-.', color='m',)
        axe.legend()
        plt.tight_layout()
        plt.savefig(path.join('.', 'img', 'qm_rezo.png'))
        plt.close(fig)
