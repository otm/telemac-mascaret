
"""
Validation script for btwi
"""
from vvytel.vnv_study import AbstractVnvStudy
from execution.telemac_cas import TelemacCas, get_dico
from data_manip.extraction.telemac_file import TelemacFile

class VnvStudy(AbstractVnvStudy):
    """
    Class for validation
    """

    def _init(self):
        """
        Defines the general parameter
        """
        self.rank = 1
        self.tags = ['artemis']

    def _pre(self):
        """
        Defining the studies
        """

        # bwti scalar mode
        self.add_study('vnv_1',
                       'artemis',
                       'art_btwi.cas')


        # bwti parallel mode
        cas = TelemacCas('art_btwi.cas', get_dico('artemis'))
        cas.set('PARALLEL PROCESSORS', 4)
        cas.set('SOLVER', 9)


        self.add_study('vnv_2',
                       'artemis',
                       'art_btwi_par.cas',
                       cas=cas)

        del cas

    def _check_results(self):
        """
        Post-treatment processes
        """

        # Comparison with the last time frame of the reference file.
        self.check_epsilons('vnv_1:ARTRES',
                            'f2d_btwi.slf',
                            eps=[1.e-12])

        # Comparison with the last time frame of the reference file.
        self.check_epsilons('vnv_2:ARTRES',
                            'f2d_btwi.slf',
                            eps=[1.e-12])

        # Comparison between sequential and parallel run.
        self.check_epsilons('vnv_1:ARTRES',
                            'vnv_2:ARTRES',
                            eps=[1.e-12])


    def _post(self):
        """
        Post-treatment processes
        """
        import matplotlib.pyplot as plt
        from postel.plot_vnv import vnv_plot2d
        from postel.plot2d import plot2d_scalar_map
        # Getting files
        vnv_1_artres = self.get_study_file('vnv_1:ARTRES')
        res_vnv_1_artres = TelemacFile(vnv_1_artres)
        vnv_1_artgeo = self.get_study_file('vnv_1:ARTGEO')
        res_vnv_1_artgeo = TelemacFile(vnv_1_artgeo)

        polys = [[[217.413, 84.862], [363.825, 154.561]],
                 [[264.673, 206.254], [318.419, 173.821]],
                 [[352.705, 203.474], [418.498, 242.394]],
                 [[377.725, 175.674], [436.104, 203.474]]]
        xmes = [[235,345],[275,300],[365,405],[385,420]]
        mes = [[1.8,0.6],[0.3,0.3],[0.55,0.55],[0.4,0.35]]
        for i, poly in enumerate(polys):
            alpha =  (poly[1][1]-poly[0][1])/(poly[1][0]-poly[0][0])
            # Plotting WAVE HEIGHT over polyline over records 0
            _, abs_curv, wav_polylines = res_vnv_1_artres.get_data_on_polyline(
                 'WAVE HEIGHT', -1, poly)
            xreel = poly[0][0] + abs_curv/(1+alpha**2)**0.5
            fig, axes = plt.subplots(1, 1, figsize=(8, 6))
            plt.plot(xreel, wav_polylines)
            plt.plot(xmes[i][0], mes[i][0], 'ro')
            plt.plot(xmes[i][1], mes[i][1], 'ro')
            titre = 'Cas 1 profile {}'.format(i+1)
            plt.title(titre)
            figname= 'img/cas1_mesure{}'.format(i+1)
            plt.savefig(figname)
            plt.close()

        #Plotting mesh
        #TODO: Plot polyline on top of mesh ?
        vnv_plot2d('',
                   res_vnv_1_artgeo,
                   plot_mesh=True,
                   fig_size=(12, 8),
                   fig_name='img/Mesh')

        # Plotting BOTTOM at 0
        bottom = res_vnv_1_artres.get_data_value("BOTTOM",-1)
        fig, axes = plt.subplots(1, 1, figsize=(16, 12))
        plot2d_scalar_map(fig, axes, res_vnv_1_artres.tri, bottom, data_name='Bottom')
        for i, poly in enumerate(polys):
            plt.plot([poly[0][0],poly[1][0]], [poly[0][1],poly[1][1]],'k-')
            plt.text((poly[0][0]+poly[1][0])/2,(poly[0][1]+poly[1][1])/2,str(i+1),fontsize=24)
        plt.savefig('img/bottom.png')
        plt.close()

        # Plotting WAVE HEIGHT at 0
        vnv_plot2d('WAVE HEIGHT',
                   res_vnv_1_artres,
                   record=0,
                   filled_contours=True,
                   fig_size=(12, 8),
                   label='Wave height hm0',
                   fig_name='img/WaveHeight_1')

        # Plotting QB at 0
        vnv_plot2d('QB',
                   res_vnv_1_artres,
                   record=0,
                   filled_contours=True,
                   fig_size=(12, 8),
                   label='Breaking QB',
                   fig_name='img/Breaking_1')

       # Closing files
        res_vnv_1_artres.close()
        res_vnv_1_artgeo.close()
