\chapter{Swell agitation in a real harbor: btwi}

\section{Summary}
The aim of this test case is to analyze the behavior of \artemis on an example
of harbor agitation. In particular, this case offers the possibility of using
\artemis in a real configuration.

The results are compared with experimental results obtained on a physical model
by Hydraulic Research Wallingford. They enable us to check that reflection,
diffraction and refraction are taken into account by the \artemis code.

\section{Description of the test case}
The modelled case represents a real harbour with the exact configuration and
bathymetry shown in Figure \ref{fig:btwi_Bathy}.
Monodirectional random swell is used.
In release 6.2 of the documentation, there used to be 2 different cases but
since the result of the second case cannot be recovered with the steering file
present here, it is not presented here even if it is run in the validation process. 

The dimension of the domain is 515~m $\times$ 260~m, with a water depth of 2.1~m.
The mesh is made of 40,506 triangles and 20,775 nodes with a size of mesh of
3.6~m offshore and 1.3~m near the coast. 

\begin{figure}[h]
\begin{center}
  \includegraphicsmaybe{[width=0.99\textwidth]}{btwi_cl.png}
\end{center}
\caption{Boundary conditions and mesh}
\label{fig:btwi_Mesh}
\end{figure}

%\subsection{Cas 1}

The incident swell is on the boundaries 3 and 4 with a period of 10.4~s a
significative height of 2.8~m and a direction of propagation of $114^\circ$.  

For this boundary, the phasis is calculated incrementally on the boundary from a
reference point which is noted $A$:
$$
\displaystyle
\Phi_I = \sum_{P=A}^{I-1} k(P) cos \Theta (x_{p+1}-x_p)
$$
Where $k(P)$ is the wave number at the frontier point $P$. The phase is converted
in degree and transmitted to the code through the array ALFAP.

The boundary 1 is a free output with a reflexion coefficient of 0, a phase shift
of 0 and an attack angle of $24^\circ$.

The boundary 2 is a free output with a reflexion coefficient of 0, a phase shift
of 0 and an attack angle of $66^\circ$.

The solid boundary has a reflexion coefficient of 1, a phase shift of 0 and an
attack angle of $0^\circ$.

%\subsection{Cas 2}

%The incident swell is on the boundaries 2 and 3 with a period of 5.3s a significative height
%of 1.7 m and a direction of propagation of $67^\circ$.  

%For this boundary, the phasis is calculated incrementally on the boundary from a reference
%point which we will note B :
%$$
%\displaystyle
%\Phi_I = \sum_{P=B}^{I-1} k(P) cos \Theta (x_{p+1}-x_p)
%$$
%Where k(P) is the wave number at the frontier point P. The phase is converted in degree and
%transmitted to the code through the array ALFAP.

%The boundary 1 is a free output with a reflexion coefficient of 0 a phase shift of 0 and an
%attack angle of $23^\circ$

%The boundary 4 is a free output with a reflexion coefficient of 0 a phase shift of 0 and an
%attack angle of $67^\circ$

%The solid boundary has a reflexion coefficient of 1 a phase shift of 0 and an attack angle of
%$0^\circ$

\begin{figure}[h]
\begin{center}
  \includegraphicsmaybe{[width=0.99\textwidth]}{../img/bottom.png}
\end{center}
\caption{Bathymetry and profiles}
\label{fig:btwi_Bathy}
\end{figure}

\begin{table} [htbp]
  \begin{tabular}{|c|c|c|c|c|}
    \hline
      & Profile 1         & Profile 2         & Profile 3       & Profile 4       \\
    \hline
    X &[217.413;363.825] & [264.673;318.419] &[352.705;418.498]&[377.725;436.104]\\
    \hline
    Y &[84.862;154.561]  & [206.254;173.821] &[203.474;242.394]&[175.674;203.474]\\
    \hline
    measurement 1 & X = 235 & X = 275 & X = 365 & X = 385 \\
    \hline
    measurement 2 & X = 345 & X = 300 & X = 405 & X = 420 \\
    \hline
  \end{tabular}
\caption{Definition of profiles and point of measurements.}
\end{table}

\section{Results}
Comparisons between the results obtained by \artemis and the experimental
results show good overall agreement (Figure \ref{fig:btwi_resprofile}), except
for profile 1 where the significant wave height is overestimated upstream of the
harbour.
They also demonstrate that \artemis takes good account of breaking, reflection,
diffraction and refraction.
Moreover, Figure \ref{fig:btwi_Breaking} shows where the breaker is the most
noticeable.
The results for the breaking rate are consistent with bathymetry, water level at
rest, wave direction and wave height
(Figure \ref{fig:btwi_Breaking}).
\begin{figure}[h]
\begin{tabular}{cc}
  \includegraphicsmaybe{[width=0.49\textwidth]}{../img/cas1_mesure1.png}&
  \includegraphicsmaybe{[width=0.49\textwidth]}{../img/cas1_mesure2.png}\\
  \includegraphicsmaybe{[width=0.49\textwidth]}{../img/cas1_mesure3.png}&
  \includegraphicsmaybe{[width=0.49\textwidth]}{../img/cas1_mesure4.png}\\
\end{tabular}
\caption{Result on profiles}
\label{fig:btwi_resprofile}
\end{figure}

\begin{figure}[h]
\begin{center}
  \includegraphicsmaybe{[width=0.99\textwidth]}{../img/WaveHeight_1.png}
\end{center}
\caption{Wave height in the harbour}
\label{fig:btwi_Waveheight}
\end{figure}

\begin{figure}[h]
\begin{center}
  \includegraphicsmaybe{[width=0.99\textwidth]}{../img/Breaking_1.png}
\end{center}
\caption{Breaking in the harbour}
\label{fig:btwi_Breaking}
\end{figure}

\section{Conclusion}
This test case compares the results produced by \artemis with experimental
results.
It validates that \artemis takes into account the dissipation of swell energy by
breaking waves, as well as reflection, diffraction and refraction, which occur
in a real case with complex bathymetry and geometry on the coast.
