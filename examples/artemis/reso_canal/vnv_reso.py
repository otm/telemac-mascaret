
"""
Validation script for reso
"""
from vvytel.vnv_study import AbstractVnvStudy
from execution.telemac_cas import TelemacCas, get_dico
from data_manip.extraction.telemac_file import TelemacFile

class VnvStudy(AbstractVnvStudy):
    """
    Class for validation
    """

    def _init(self):
        """
        Defines the general parameter
        """
        self.rank = 1
        self.tags = ['artemis']

    def _pre(self):
        """
        Defining the studies
        """

        # Resonance in a flume
        self.add_study('vnv_1',
                       'artemis',
                       'art_reso.cas')

        # Resonance in a flume, parallel mode
        cas = TelemacCas('art_reso.cas', get_dico('artemis'))
        cas.set('PARALLEL PROCESSORS', 4)
        cas.set('SOLVER', 9)

        self.add_study('vnv_2',
                       'artemis',
                       'art_reso_par.cas',
                       cas=cas)

        del cas

    def _check_results(self):
        """
        Post-treatment processes
        """

        # Comparison with the last time frame of the reference file.
        self.check_epsilons('vnv_1:ARTRES',
                            'f2d_reso.slf',
                            eps=[1.e-12])

        # Comparison with the last time frame of the reference file.
        self.check_epsilons('vnv_2:ARTRES',
                            'f2d_reso.slf',
                            eps=[1.e-12])

        # Comparison between sequential and parallel run.
        self.check_epsilons('vnv_1:ARTRES',
                            'vnv_2:ARTRES',
                            eps=[1.e-12])

    def _post(self):
        """
        Post-treatment processes
        """
        import matplotlib.pyplot as plt
        import numpy as np
        from postel.plot_vnv import vnv_plot2d
        from postel.plot1d import plot1d

        # Getting files
        vnv_1_artres = self.get_study_file('vnv_1:ARTRES')
        res_vnv_1_artres = TelemacFile(vnv_1_artres)
        vnv_1_artgeo = self.get_study_file('vnv_1:ARTGEO')
        res_vnv_1_artgeo = TelemacFile(vnv_1_artgeo)

        #Plotting mesh
        vnv_plot2d(\
                'MAILLAGE',
                res_vnv_1_artgeo,
                plot_mesh=True,
                fig_size=(7, 10),
                fig_name='img/Mesh')

        #Plotting mesh zoom
        vnv_plot2d(\
                'MAILLAGE',
                res_vnv_1_artgeo,
                plot_mesh=True,
                fig_size=(7, 10),
                xlim=[1900, 3000], ylim=[1500, 2700],
                fig_name='img/MeshZoom')

        # Plotting WAVE HEIGHT at 50
        vnv_plot2d(\
                 'WAVE HEIGHT',
                 res_vnv_1_artres,
                 record=0,
                 filled_contours=True,
                 fig_size=(7, 10),
                 fig_name='img/Wave_height50')

        # Plotting WAVE HEIGHT at 50
        vnv_plot2d(\
                 'WAVE HEIGHT',
                 res_vnv_1_artres,
                 record=0,
                 filled_contours=True,
                 fig_size=(7, 10),
                 xlim=[1900, 3000], ylim=[1500, 2700],
                 fig_name='img/Wave_height50Zoom')

        # Plotting WAVE PHASE at 50
        vnv_plot2d(\
                 'WAVE PHASE',
                 res_vnv_1_artres,
                 record=0,
                 filled_contours=True,
                 fig_size=(7, 10),
                 fig_name='img/Wave_phaset50')

        points = [[2975,2025]]
        times = res_vnv_1_artres.times
        wavephase = res_vnv_1_artres.get_timeseries_on_points('WAVE PHASE',points)
        _, axe = plt.subplots(figsize=(10,5))
        plot1d(axe, times, wavephase[0,:],marker='o',
               x_label='$t$ (s)',
               y_label='Wave phase')
        fig_name = 'img/wavephase1d'
        plt.savefig(fig_name)
        plt.close('all')

        waveheight = res_vnv_1_artres.get_timeseries_on_points('WAVE HEIGHT',points)
        _, axe = plt.subplots(figsize=(10,5))
        plot1d(axe, times, waveheight[0,:],
               x_label='$t$ (s)',
               y_label='Wave height (m)')
        maxtime = [ 500, 150, 80]
        for i in range(3):
            ind = np.where(times<maxtime[i])[0]
            maxhs = max(waveheight[0,ind])
            timemax = times[np.where(waveheight[0,ind]==maxhs)]
            strmax = str(int(timemax)) + 's'
            plt.text(timemax, maxhs, strmax)
        axe.set(ylim=[0, 1.05*max(waveheight[0,:])])
        fig_name = 'img/waveheight1d'
        plt.savefig(fig_name)
        plt.close('all')

        res_vnv_1_artgeo.close()
        res_vnv_1_artres.close()
