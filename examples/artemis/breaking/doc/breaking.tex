\chapter{Monochromatic swell breaking on beach: breaking}

\section{Summary}

The aim of this test case is to analyze the behavior of \artemis during wave
breaking on a beach with a constant gentle slope. It also enables us to test
the two breaking models introduced in \artemis.

The results are compared with experimental results obtained on a physical model
by Delft Hydraulics Laboratory \cite{Stive1984}. They enable us to verify that
the \artemis code takes well into account wave breaking.

\section{Description of the test case}
The modeled case is the one described in \cite{Stive1984}, i.e. a beach on the
opposite side to the swell generation. Two breaking formulas are compared. In
the first case, the formula of Battjes \& Janssen \cite{Battjes1978} is used and
in the second case Dally et al \cite{Dally1984} formula is used. For the latter
case, $K = 0.25$ and $\Gamma = 0.4$. These choices are based on the values
proposed by Dally et al \cite{Dally1984}, and after tests carried out on the
\artemis code for several values of $K$ and $\Gamma$.

\subsection{Geometry}

Domain: 42.5~m $\times$ 1~m

Maximum depth: 0.7~m

Beach slope: 1/40

\begin{figure}[h]
\begin{center}
  \includegraphicsmaybe{[width=0.7\textwidth]}{../img/Bathy.png}
\end{center}
\caption{Bathymetry of the case}
\label{fig:breaking_Bathy}
\end{figure}

\subsection{Mesh}
Number of triangular elements: 1,421

Size of mesh: 40~cm (inlet), 10~cm (outlet)

Nodes: 863

Number of elements per wavelength: 11.7 (for $h$ = 0.7~m)

\begin{figure}[h]
\begin{center}
  \includegraphicsmaybe{[width=0.7\textwidth]}{../img/Mesh.png}
\end{center}
\caption{Mesh of the case}
\label{fig:breaking_Mesh}
\end{figure}

\subsection{Boundary conditions}
\begin{figure}[h]
\begin{center}
  \includegraphicsmaybe{[width=\textwidth]}{boundary.png}
\end{center}
\caption{Boundary Conditions}
\label{fig:breaking_bc}
\end{figure}

Incident swell:
\begin{itemize}
\item Period: 1.79~s
\item Swell height: 0.145~m
\item Phase (ALFAP): 0 on all the boundary
\item Propagation direction: 0 $^\circ$
\end{itemize}

Solid wall:
\begin{itemize}
\item Reflection Coefficient: 1
\item Dephasing: 0
\item Attack angle: 0 $^\circ$
\end{itemize}
Liquid boundary (beach): 
\begin{itemize}
\item Reflection Coefficient: 0
 \item Dephasing: 0
\item Attack angle: 0 $^\circ$
\end{itemize}

\subsection{Solver}
For both cases, the direct solver (\telkey{SOLVER} = 8) is used.
%with accuracy of $10^{-4}$.
Calculation time is less than 1 second.
For the dissipation coefficient calculation, the relaxation coefficient between two sub
iterations is 0.25.


\section{Results}
The experimental results obtained in \cite{Stive1984} are the reference values.
The points are on a section for $x$ from 34 to 42~m and $y$ = 0.5~m.
We compare wave height experimentally and numerically.

The wave height of swell obtained for the two models are compared in Figures
\ref{fig:breaking_WH_Dally} and \ref{fig:breaking_WH_Janssen}, the breaking
rate in Figures \ref{fig:breaking_QB_Dally} and \ref{fig:breaking_QB_Janssen}.
Figure \ref{fig:breaking_WH_Profile} compares the result obtained with \artemis
with the 2 models and the experimental results. The measurements used to make
the Figures have been approximated with the reading of an old Figure.
A comparison (Figure \ref{fig:breaking_WH_Profile}) between the results
obtained by \artemis and those obtained in \cite{Stive1984} shows good
agreement.
It shows that \artemis has adequately taken into account breaking phenomena.
Nevertheless, the results produced by
\artemis are overestimated compared to the measurements. However, Dally's
formula seems to slightly less overestimate.

There is a simple explanation for the discrepancy between numerical and
experimental results. The shallow slope leads to a spilling breaking which
means that the dissipation occurs on a long distance and not in a restricted
area. In the case of a monochromatic swell the breaking rate is 0 or 1 which
gives breaking or not at all. This binary description is not suitable to that
type of breaking, which explains the overestimation in the surfing area. The
differences are less important for a random swell.  

\begin{figure}[h]
\begin{center}
  \includegraphicsmaybe{[width=0.9\textwidth]}{../img/WaveHeightDally.png}
\end{center}
\caption{Wave height with Dally model}
\label{fig:breaking_WH_Dally}
\end{figure}

\begin{figure}[h]
\begin{center}
  \includegraphicsmaybe{[width=0.9\textwidth]}{../img/WaveHeightJanssen.png}
\end{center}
\caption{Wave height with Janssen model}
\label{fig:breaking_WH_Janssen}
\end{figure}

\begin{figure}[h]
\begin{center}
  \includegraphicsmaybe{[width=0.9\textwidth]}{../img/BreakingDally.png}
\end{center}
\caption{Breaking rate with Dally model}
\label{fig:breaking_QB_Dally}
\end{figure}

\begin{figure}[h]
\begin{center}
  \includegraphicsmaybe{[width=0.9\textwidth]}{../img/BreakingJanssen.png}
\end{center}
\caption{breaking rate with Janssen model}
\label{fig:breaking_QB_Janssen}
\end{figure}

\begin{figure}[h]
  \begin{center}
  \includegraphicsmaybe{[width=0.9\textwidth]}{../img/profile.png}
\end{center}
\caption{Comparison of wave height with measurements on a profile}
\label{fig:breaking_WH_Profile}
\end{figure}

\section{Conclusion}
This test case compares the results produced by \artemis with experimental
results on a physical model. It validates \artemis ability to take into account
wave breaking for a monochromatic swell. However, this type of swell is not
suitable for modeling of all breaking modes, especially for sliding waves. It
is therefore recommended to use \artemis with a random swell, especially when
breaking plays an important role in wave propagation.
