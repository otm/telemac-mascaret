\chapter{Elliptical bump on beach: bosse\_elliptique}

\section{Summary}
The objective of this test case is to analyze the behavior of \artemis during
the transformation of the swell over a bump on a beach. When the beach is
exclusively considered, the transformation of the swell is only due to
refraction. In presence of the bump, the coupling between refraction and
diffraction appears. The refraction methods, applied in this case, lead to a
convergence of the swell behind the hump, associated with a concentration of
energy. In reality, the energy of the swell is dispersed, which is mainly
due to diffraction in the lateral direction.

The results are compared with experimental results presented by Berkhoff
\cite{berkhoff1982}. They verify that the coupling between refraction and
diffraction is well taken into account by the \artemis code.


\section{Description of the test case}

The modeled case represents the one realized in \cite{berkhoff1982}, i.e. a
bump, on a beach on the side opposite to the generation of the swell. The
angle between the beach and the swell direction is 20$^\circ$.
The side edges are solid walls. The slope and curvature terms are taken into
account  in the Berkhoff equation (extended diffraction-refraction equation,
option \telkey{RAPIDLY VARYING TOPOGRAPHY}).

\subsection{Geometry}
Domain
\begin{itemize}
 \item  Size domain: 30~m $\times$ 35~m
\item Max depth: 0.45~m
\item Beach slope: 1/50
\end{itemize}
  Elliptical Bump
\begin{itemize}
\item   Large axis:  8~m
\item Small axis: 6~m
\item Max height: 0.2~m
\end{itemize}

\subsection{Mesh}
\begin{itemize}
\item Triangular elements: 49,083
\item Mesh size: 20~cm
\item Nodes: 24,842
\item Elements per wavelength: 7.5 (for $h$ = 0.45~m)
\end{itemize}

\subsection{Boundary conditions}

\begin{figure}[h]
\begin{center}
  \includegraphicsmaybe{[width=0.9\textwidth]}{bc_bosse_elipt.png}
\end{center}
\caption{Dimensions and Boundary conditions}
\label{fig:bc_bosse_elipt}
\end{figure}

Incident swell:
\begin{itemize}
\item  Period: 1~s
\item  Wave height: 0.0464~m
\item  Zero phase on the whole boundary: ALFAP = 0
\item  Direction of propagation: 90$^\circ$
\end{itemize}
Solid walls:
\begin{itemize}
  \item Reflection coefficient: 0
\item  Phase shift: 0$^\circ$
\item  Angle of attack: 90$^\circ$
\end{itemize}

Liquid boundary:
\begin{itemize}
\item  Reflection coefficient: 0
\item  Phase shift: 0$^\circ$
\item  Angle of attack: 20$^\circ$
\end{itemize}

\section{Reference solution}

The experimental results obtained in \cite{berkhoff1982} constitute the
reference values, divided into 8 sections.
The wave heights obtained numerically and experimentally are compared by
presenting the amplification coefficient, defined by the following formula:
$$
\frac{H(x,y)}{H_{incident}}
$$

\section{Results}

\begin{figure}[h]
\begin{center}
  \includegraphicsmaybe{[width=0.9\textwidth]}{section_bosse_elipt.png}
\end{center}
\caption{definition of sections}
\label{fig:defsection_bosse_elipt}
\end{figure}

\begin{figure}[h]
\begin{center}
  \includegraphicsmaybe{[width=0.45\textwidth]}{resu_berkhoff.png}
  \includegraphicsmaybe{[width=0.45\textwidth]}{resu_artemis.png}
\end{center}
\caption{amplification coefficient of wave height with Berkhoff (on the left) and \artemis (on the right)}
\label{fig:bosse_elipt_resu_comp}
\end{figure}

The wave amplitudes obtained with the solver 3
(Figure \ref{fig:bosse_elipt_resu_comp}) are compared to those presented by
Berkhoff \cite{berkhoff1982}.
The measurements were taken along different sections parallel to the $x$ and $y$
axes, represented on the diagram below (Figure \ref{fig:defsection_bosse_elipt}).


\begin{figure}[h]
\begin{center}
  \includegraphicsmaybe{[width=0.9\textwidth]}{sections_bosse.png}
\end{center}
\caption{amplification coefficients of wave height for the 8 sections.}
\label{fig:ressections_bosse}
\end{figure}

The comparison between the results obtained by \artemis and those obtained in
\cite{berkhoff1982} shows good correspondences (Figure \ref{fig:ressections_bosse}).
We can clearly see the effect of the bump: a maximum amplification of 227\%.
Experimentally, this maximum is 221\%. This highlights that \artemis takes into
account the refraction-diffraction interactions.


\section{Conclusions}
This test case compares the results produced by \artemis with experimental
results on physical model.
It allows to validate the modeling of the combined effects of refraction and
diffraction.
