\chapter{Island on a parabolic bottom: ile\_para}

\section{Summary}
The aim of this test case is to analyze the behavior of \artemis on
a theoretical example involving the coupled effects of
refraction-diffraction and reflection on an obstacle. It also illustrates
the theory of long waves (tsunamis, dry waves), i.e. when the wavelength is
very long compared to the depth of the ocean. An analytical solution on
the perimeter of the obstacle is known and presented by Vastano and Reid
\cite{Vastano1967}.

In the original version of the documentation (release 6.2 of \artemis), a
convergence of the solution was studied through the simulations of 3 meshes
finer and finer towards the analytical solution.
Only the simulation run on the second mesh (19,488 triangles) are presented here.
The interested user can read the old version of the documentation.

\section{Description of the test case}
\subsection{Geometry}
The modeled case represents an island on a parabolic bottom.
The surrounding background is flat as shown in Figure \ref{fig:island_geom}.

The domain is a square of 480~km $\times$ 480~km, the maximum depth $h_1$ is
4,000~m. The island is a 40~km circle and has a depth of 444~m.
The parabolic bottm has a ray of 120~km.

\begin{figure}[h]
\begin{center}
  \includegraphicsmaybe{[width=0.7\textwidth]}{geom_island.png}
\end{center}
\caption{Geometry of the island}
\label{fig:island_geom}
\end{figure}

\begin{figure}[h]
\begin{center}
  \includegraphicsmaybe{[width=0.7\textwidth]}{../img/Mesh.png}
\end{center}
\caption{Mesh around the island}
\label{fig:island_mesh}
\end{figure}

\subsection{Boundary conditions}

The incident swell has a period of 600~s a wave height of 1~m and a direction of
$90^\circ$. The phase (ALPHAP) is calculated as follow: As the bathymetry is
constant, we choose a point where the phase is null. Let us call A this
reference point. We can then impose in every point of the boundary.
$$
\Phi_I= k. \cos\theta.(x_I-x_A)+ k \sin\theta(y_I-y_A)
$$

This result is converted in degree and transmitted through the array ALFAP.

On the solid boundary, the reflexion coefficient is 1, the phase shift $t0$ and
the attack angle $0^\circ$.

\begin{figure}[h]
\begin{center}
  \includegraphicsmaybe{[width=0.3\textwidth]}{cl_island.png}
\end{center}
\caption{Boundary conditions of the island}
\label{fig:island_cl}
\end{figure}

\section{Reference solution}
The analytical solution on the island's perimeter is the reference solution.
The wave heights obtained numerically and analytically on the circumference
are compared as a function of the angle with respect to the axis (Oy)
(trigonometric direction from the point with coordinates $x$ = 0 and $y$ =
40~km).

The analytic solution can be written as

\includegraphicsmaybe{[width=0.5\textwidth]}{formule.png}

  with
  
\includegraphicsmaybe{[width=0.7\textwidth]}{with.png}

where
\begin{itemize}
\item $H_n$ Hankel function of first type 
\item $\epsilon_n$, 1 if $n$=0, 2 if $n>0$
\item $g$ gravity
\item $\omega$: wave pulsation 
\end{itemize}

\section{Results}
The wave height obtained by \artemis appears to be correct (Figure
\ref{fig:island_height}).
Indeed, diffraction can be observed on both sides of the island.
In particular, there is perfect symmetry.

In Figure \ref{fig:island_oldresult}, we remind the results obtained with
version 6.2. One can see that refining mesh 1 to mesh 2 leads to results
closer to the analytical solution. 

\begin{figure}[h]
\begin{center}
  \includegraphicsmaybe{[width=0.9\textwidth]}{../img/Waveheight.png}
\end{center}
\caption{Wave height around the island}
\label{fig:island_height}
\end{figure}


\begin{figure}[h]
\begin{center}
  \includegraphicsmaybe{[width=0.9\textwidth]}{oldresults.png}
\end{center}
\caption{Wave heights compared to analytical solution obtained with version 6.2}
\label{fig:island_oldresult}
\end{figure}

\section{Conclusion}
This test case compares the results produced by \artemis with an analytical
solution. It validates the modeling of the combined effects of refraction,
diffraction and reflection.
