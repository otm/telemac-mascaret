\chapter{Flow over a bump (bump)}
\label{chapter:bump}

\section{Purpose}

This test case presents a flow over a bump on the bed with subcritical condition.
It allows to show that \telemac{2D} is able to correctly reproduce
the hydrodynamic impact of a changing bed slopes, vertical flow
contractions and expansions. Furthermore, it allows to have a good
representation of flows computed in steady and transient flow regimes. \\

The solution produced by \telemac{2D} in a frictionless channel
presenting an idealised bump on the bottom  is compared with
the analytical solution to this problem. In this test case,
all flow regimes i.e. subcritical, critical and transcritical are studied.

\section{Description}

\subsection{Analytical solution}

From the strict hyperbolicity of the shallow water system, the flow over the bump can be caracterized by a
criticality condition. Depending on the value of the Froude number,
the flow can either be subcritical (fluvial, $Fr<1$)
or supercritical (torrential, $Fr>1$).
Let us recall the definition of the Froude number:
\begin{equation}
Fr = \dfrac{|{\bf u}|}{\sqrt{gh}}
\end{equation}
where $|{\bf u}|$ is the norm of the depth averaged velocity and $g$ is the gravitational acceleration. Another useful quantity in the case of a constant discharge $q$ is the critical height defined by:
\begin{equation}
h_c = \left( \dfrac{q}{\sqrt{g}}\right)^{2/3}
\end{equation}
The flow can either be subcritical if $h>h_c$ or supercritical if $h<h_c$.
In the case of a frictionless channel with a constant disharge $q_0$, the Bernoulli relation allows to express the water height in the channel at abscissa $x$ by the relation:
\begin{equation}
\dfrac{q_0^2}{2 g h(x)^2} + h(x) + z_b(x) = cst
\end{equation}
with $z_b$ the bottom elevation. Let us consider a channel of length $L$.
From the Bernoulli relation, one can express the water depth as a solution of third degree equation, which depends on the
the flow criticality:

\begin{itemize}
\item {\bf Subcritical flow:}
\begin{equation}
 h(x)^3 + \left( z_b(x) - \dfrac{q_0^2}{2g h(L)^2} - h(L) \right) h(x)^2 + \dfrac{q_0^2}{2g} = 0 \quad \forall x \in [0,L]
\end{equation}
\

\item {\bf Critical flow:}
\begin{equation}
h(x)^3 + \left( z_b(x) - \dfrac{q_0^2}{2g h_c^2} - h_c - z_M \right) h(x)^2 + \dfrac{q_0^2}{2g} = 0 \quad \forall x \in [0,L]
\end{equation}
with $ z_M = \max_{x \in [0,L]}z_b$.
\

\item {\bf Transcritical flow:}
\begin{equation}
\begin{cases}
h(x)^3 + \left( z_b(x) - \dfrac{q_0^2}{2g h_c^2} - h_c - z_M \right) h(x)^2 + \dfrac{q_0^2}{2g} = 0 \quad & \text{for  } x < x_{shock}  \\
h(x)^3 + \left( z_b(x) - \dfrac{q_0^2}{2g h(L)^2} - h(L) \right) h(x)^2 + \dfrac{q_0^2}{2g} = 0 \quad &\text{for  } x > x_{shock} \\
q_0^2 \left( \dfrac{1}{h(x_{shock}^-)} - \dfrac{1}{h(x_{shock}^+)} \right) + \dfrac{g}{2} \left( h(x_{shock}^-)^2 -h(x_{shock}^+)^2 \right) = 0
\end{cases}
\end{equation}
\end{itemize}

\subsection{Geometry and mesh}

The geometry dimensions of the channel are 2~m wide and 20~m long.
The mesh is regular along the channel. It is made up with
quadrangles split into two triangles.
It is composed of 1,280 triangular elements (729 nodes)
and the size of triangles is about 0.25~m
(see Figure \ref{t2d:bumpsub:fig:mesh}).

\begin{figure}[!htbp]
 \centering
 \includegraphicsmaybe{[width=\textwidth]}{../img/bumpsub_mesh0.png}
 \caption{Mesh of the channel.}
 \label{t2d:bumpsub:fig:mesh}
\end{figure}

\subsection{Bathymetry}
The maximum elevation of
the bump is 0.25~m (see Figure \ref{t2d:bumpsub:fig:baty}) with
the bottom $z_b$ described by the following equation:
\begin{equation*}
z_b = 0.25 \exp \left(\dfrac{-(x-10)^2}{2} \right)
\end{equation*}

\begin{figure}[H]
 \centering
 \includegraphicsmaybe{[width=0.95\textwidth]}{../img/bumpsub_bathy.png}
 \caption{Bathymetry in the channel.}
 \label{t2d:bumpsub:fig:baty}
\end{figure}

\subsection{Initial and boundary conditions}
The initial conditions are defined with analytical solution for both velocity and water depth to quickly achieve the steady state.
The boundary conditions are defined as follows:
\begin{itemize}
\item {\bf Subcritical flow:}
At the channel entrance, the flow rate is $Q =  1.5~\text{m}^3\text{s}^{-1}$
(note that the discharge per unit length $q_0=Q/B$, $B$ being the channel width).
The flow rate $Q$ is taken so that the $q_0$ value is equal to the value
$\sqrt{gh}$ at the entrance.
At the channel outlet, the free surface elevation is $z_s$ = 0.8~m.

\begin{figure}[!htbp]
 \centering
 \includegraphicsmaybe{[width=0.9\textwidth]}{../img/bumpsub_mesh.png}
 \caption{Boundary condition type for the subcritical case.}
 \label{t2d:bumpsub:fig:bc}
\end{figure}

\item {\bf Critical flow:}
At the channel entrance, the flow rate is $Q = 0.3~\text{m}^3\text{s}^{-1}$.
At the channel outlet, free velocity and free water level are imposed on the liquid boundary.

\begin{figure}[!htbp]
 \centering
 \includegraphicsmaybe{[width=0.9\textwidth]}{../img/bumpcri_mesh.png}
 \caption{Boundary condition type for the critical case.}
 \label{t2d:bumpcri:fig:bc}
\end{figure}

\item {\bf Transcritical flow:}
At the channel entrance, the flow rate is $Q = 0.45~\text{m}^3\text{s}^{-1}$.
At the channel outlet, the free surface elevation is $z_{s} = 0.35~\text{m}$.

\begin{figure}[!htbp]
 \centering
 \includegraphicsmaybe{[width=0.9\textwidth]}{../img/bumptrans_mesh.png}
 \caption{Boundary condition type for the transcritical case.}
 \label{t2d:bumptrans:fig:bc}
\end{figure}

\end{itemize}

\subsection{Physical parameters}

No friction is taken into account on the bottom and on the lateral walls.
%For the transcritical case, either without or with friction is tested with a Strickler formula and a friction coefficient equal to 40~$\text{m}^{1/3}.\text{s}^{-1}$.
Note that the horizontal viscosity turbulent is constant and equal to zero for all cases
(\telkey{VELOCITY DIFFUSIVITY} = 0.).
%% The 2 following lines are not what is done in the steering file,
%% they have been commented
%However instead of prescribing a zero viscosity, no diffusion step could
%have been used (keyword \telkey{DIFFUSION OF VELOCITY} = NO).

\subsection{Numerical parameters}

\telemac{2D} is run forward in time until a steady state flow is obtained.
Several numerical schemes are tested.
The parameters specific to each case are summed up in the Tables
\ref{tab:bump:subcases}, \ref{tab:bump:cricases}, \ref{tab:bump:transcases}.

\subsubsection{Subcritical flow}
\begin{table}[H]
  \resizebox{\textwidth}{!}{%
    \begin{tabular}{|c|c|c|c|c|}
      \hline Case & Name & Equations & Advection scheme for velocities & Time step / Desired Courant number \\
      \hline 1 & CHAR & Wave Eq. FE & Characteristics & 0.02~s / - \\
      \hline 2 & N    & Wave Eq. FE & N-scheme & 0.02~s / - \\
      \hline 3 & PSI  & Wave Eq. FE & PSI scheme & 0.02~s / - \\
      \hline 4 & LIPS & Wave Eq. FE & PSI LIPS scheme & 0.02~s / - \\
      \hline 5 & PC1 & Wave Eq. FE & PSI Predictor-Corrector order 1 scheme & 0.02~s / - \\
      \hline 6 & PC2 & Wave Eq. FE & PSI Predictor-Corrector order 2 scheme & 0.02~s / - \\
      \hline 7 & NERD & Wave Eq. FE & Edge based N-scheme & 0.02~s / - \\
      \hline 8 & ERIA & Wave Eq. FE & ERIA scheme & 0.02~s / - \\
      \hline 9 & ROE  & Saint-Venant FV & ROE order 1 & - /0.9 \\
      \hline 10 & KIN1 & Saint-Venant FV & Kinetic order 1 & - /0.9 \\
      \hline 11 & KIN2 & Saint-Venant FV & Kinetic order 2 & - /0.9 \\
      \hline 12 & HLLC & Saint-Venant FV & HLLC order 1 & - /0.9 \\
      \hline
    \end{tabular}
  }
  \caption{List of the simulation parameters used for the cases tested in the subcritical bump example.}
  \label{tab:bump:subcases}
\end{table}
\subsubsection{Critical flow}
\begin{table}[H]
  \resizebox{\textwidth}{!}{%
    \begin{tabular}{|c|c|c|c|c|}
      \hline Case & Name & Equations & Advection scheme for velocities & Time step / Desired Courant number \\
      \hline 1 & CHAR & Wave Eq. FE & Characteristics & 0.02~s / - \\
      \hline 2 & N    & Wave Eq. FE & N-scheme & 0.02~s / - \\
      \hline 3 & PSI  & Wave Eq. FE & PSI scheme & 0.02~s / - \\
      \hline 4 & LIPS & Wave Eq. FE & PSI LIPS scheme & 0.02~s / - \\
      \hline 5 & PC1  & Wave Eq. FE & PSI Predictor-Corrector order 1 scheme & 0.02~s / - \\
      \hline 6 & PC2  & Wave Eq. FE & PSI Predictor-Corrector order 2 scheme & 0.02~s / - \\
      \hline 7 & NERD & Wave Eq. FE & Edge based N-scheme & 0.005~s / - \\
      \hline 8 & ERIA & Wave Eq. FE & ERIA scheme & 0.005~s / - \\
      \hline 9 & ROE  & Saint-Venant FV & ROE order 1 & - /0.9 \\
      \hline 10 & KIN1 & Saint-Venant FV & Kinetic order 1 & - /0.9 \\
      \hline 11 & KIN2 & Saint-Venant FV & Kinetic order 2 & - /0.9 \\
      \hline 12 & HLLC & Saint-Venant FV & HLLC order 1 & - /0.9 \\
      \hline
    \end{tabular}
  }
  \caption{List of the simulation parameters used for the cases tested in the critical bump example.}
  \label{tab:bump:cricases}
\end{table}
\subsubsection{Transcritical flow}
\begin{table}[H]
  \resizebox{\textwidth}{!}{%
    \begin{tabular}{|c|c|c|c|c|}
      \hline Case & Name & Equations & Advection scheme for velocities & Time step / Desired Courant number \\
      \hline 1 & CHAR & Wave Eq. FE & Characteristics & 0.8~s / - \\
      \hline 2 & N    & Wave Eq. FE & N-scheme & 0.7~s / - \\
      \hline 3 & PSI  & Wave Eq. FE & PSI scheme & 0.7~s / - \\
      \hline 4 & LIPS & Wave Eq. FE & PSI LIPS scheme & 0.7~s / - \\
      \hline 5 & PC1 & Wave Eq. FE & PSI Predictor-Corrector order 1 scheme & 0.7~s / - \\
      \hline 6 & PC2 & Wave Eq. FE & PSI Predictor-Corrector order 2 scheme & 0.7~s / - \\
      \hline 7 & NERD & Wave Eq. FE & Edge based N-scheme & 0.7~s / - \\
      \hline 8 & ERIA & Wave Eq. FE & ERIA scheme & 0.7~s / - \\
      \hline 9 & ROE  & Saint-Venant FV & ROE order 1 & - /0.9 \\
      \hline 10 & KIN1 & Saint-Venant FV & Kinetic order 1 & - /0.9 \\
      \hline 11 & KIN2 & Saint-Venant FV & Kinetic order 2 & - /0.9 \\
      \hline 12 & HLLC & Saint-Venant FV & HLLC order 1 & - /0.9 \\
      \hline
    \end{tabular}
  }
  \caption{List of the simulation parameters used for the cases tested in the transcritical bump example.}
  \label{tab:bump:transcases}
\end{table}

\section{Results}

\subsection{Computation time}

Simulation times for each of these cases with sequential and parallel runs (using 4 processors) are shown in Figure~\ref{fig:bump:cputime}.

\begin{figure}[H]
  \centering
  \includegraphicsmaybe{[width=0.75\textwidth]}{../img/bumpsub_cpu_times.png}
  \includegraphicsmaybe{[width=0.75\textwidth]}{../img/bumpcri_cpu_times.png}
  \includegraphicsmaybe{[width=0.75\textwidth]}{../img/bumptrans_cpu_times.png}
  \caption{CPU times for subcritical, critical and transcritical flow with shock (from top to bottom).}\label{fig:bump:cputime}
\end{figure}


\subsection{First observation}

In this test case, the numerical results are compared with the
analytical solution, when the state flow is steady. Furthermore, the computed
Froude number ($Fr$) is also
compared with the analytical solution.
The solution produced by \telemac{2D} is in close agreement with
the analytical solution as shown in
Figure \ref{fig:bumpsub:fig:hfr}.


\begin{figure}[H]
\begin{minipage}[t]{0.5\textwidth}
 \centering
 \includegraphicsmaybe{[width=\textwidth]}{../img/bumpsub_free_surface.png}
\end{minipage}%
\begin{minipage}[t]{0.5\textwidth}
 \centering
 \includegraphicsmaybe{[width=\textwidth]}{../img/bumpsub_froude_number.png}
\end{minipage}
\begin{minipage}[t]{0.5\textwidth}
 \centering
 \includegraphicsmaybe{[width=\textwidth]}{../img/bumpcri_free_surface.png}
\end{minipage}%
\begin{minipage}[t]{0.5\textwidth}
 \centering
 \includegraphicsmaybe{[width=\textwidth]}{../img/bumpcri_froude_number.png}
\end{minipage}
\begin{minipage}[t]{0.5\textwidth}
 \centering
 \includegraphicsmaybe{[width=\textwidth]}{../img/bumptrans_free_surface.png}
\end{minipage}%
\begin{minipage}[t]{0.5\textwidth}
 \centering
 \includegraphicsmaybe{[width=\textwidth]}{../img/bumptrans_froude_number.png}
\end{minipage}
 \caption{Comparison between analytical solution and \telemac{2D} HLLC
 solution for subcritical, critical and transcritical flow with shock (from top to bottom).}\label{fig:bumpsub:fig:hfr}
\end{figure}


\subsection{Comparison of schemes}
In Figure \ref{fig:bumpsub:fig:comp}, results obtained with different numerical schemes are compared to the analytical solution at final time. For both critical and transcritical cases, numerical oscillations are visible for some schemes.

\begin{figure}[H]
\begin{minipage}[t]{0.5\textwidth}
 \centering
 \includegraphicsmaybe{[width=\textwidth]}{../img/bumpsub_elevation_1dslice_comparison_tf.png}
\end{minipage}%
\begin{minipage}[t]{0.5\textwidth}
 \centering
 \includegraphicsmaybe{[width=\textwidth]}{../img/bumpsub_froud_1dslice_comparison_tf.png}
\end{minipage}
\begin{minipage}[t]{0.5\textwidth}
 \centering
 \includegraphicsmaybe{[width=\textwidth]}{../img/bumpcri_elevation_1dslice_comparison_tf.png}
\end{minipage}%
\begin{minipage}[t]{0.5\textwidth}
 \centering
 \includegraphicsmaybe{[width=\textwidth]}{../img/bumpcri_froud_1dslice_comparison_tf.png}
\end{minipage}
\begin{minipage}[t]{0.5\textwidth}
 \centering
 \includegraphicsmaybe{[width=\textwidth]}{../img/bumptrans_elevation_1dslice_comparison_tf.png}
\end{minipage}%
\begin{minipage}[t]{0.5\textwidth}
 \centering
 \includegraphicsmaybe{[width=\textwidth]}{../img/bumptrans_froud_1dslice_comparison_tf.png}
\end{minipage}
 \caption{Comparison between analytical solution and various \telemac{2D} numerical schemes
 for subcritical, critical and transcritical flow with shock (from top to bottom).}\label{fig:bumpsub:fig:comp}
\end{figure}

\subsection{Accuracy}

For a more quantitative comparison of schemes, the $L^1$, $L^2$ and $L^\infty$ error norms
of the water depth and velocity are calculated at final time.
Results are presented in Figure \ref{fig:bump:errors}.

\begin{figure}[H]
  \centering
  \includegraphicsmaybe{[width=\textwidth]}{../img/bumpsub_errors_H_tf.png}
  \includegraphicsmaybe{[width=\textwidth]}{../img/bumpcri_errors_H_tf.png}
  \includegraphicsmaybe{[width=\textwidth]}{../img/bumptrans_errors_H_tf.png}
%  \caption{Errors at final time for subcritical, critical, transcritical without friction and transcritical with friction (from top to bottom).}\label{fig:bump:errors}
  \caption{Errors at final time for subcritical, critical, transcritical (from top to bottom).}\label{fig:bump:errors}
\end{figure}

\subsection{Convergence in the subcritical case}

In this section, a mesh convergence is carried out for each numerical scheme on the subcritical test case.
From a starting mesh with 160 elements, we divide by 4 each triangle recursively to generate new meshes.
Meshes used in the convergence study are presented in Figure \ref{t2d:bump:meshes}.
Final time is set to $t_f$ = 2~min.
With decreasing space step, we adjust time step to ensure a constant CFL for each mesh increment.

\begin{figure}[H]
\begin{minipage}[t]{\textwidth}
 \centering
 \includegraphicsmaybe{[width=0.8\textwidth]}{../img/mesh_0.png}
\end{minipage}
\begin{minipage}[t]{\textwidth}
 \centering
 \includegraphicsmaybe{[width=0.8\textwidth]}{../img/mesh_1.png}
\end{minipage}
\begin{minipage}[t]{\textwidth}
 \centering
 \includegraphicsmaybe{[width=0.8\textwidth]}{../img/mesh_2.png}
\end{minipage}
\begin{minipage}[t]{\textwidth}
 \centering
 \includegraphicsmaybe{[width=0.8\textwidth]}{../img/mesh_3.png}
\end{minipage}
 \caption{Meshes used in bump mesh convergence.}
 \label{t2d:bump:meshes}
\end{figure}

Errors in $L^2$ norm for water depth
are presented as functions of mesh discretization in Figure
\ref{fig:bump:ErrNumH_convergence}. In Figure \ref{fig:bump:ErrNumH_convergence_mesh3}, errors on H
at final time and on the finest mesh are compared.
On the next pages, convergence results are presented in $L^1$, $L^2$ and $L^{\infty}$ norms for both water depth and velocity.

\begin{figure}[H]
\centering
  \includegraphicsmaybe{[width=0.9\textwidth]}{../img/t2d_bumpsub_errors_tf_finemesh_H_L2_allsc.png}
  \caption{H convergence in $L^2$ norm.}
\label{fig:bump:ErrNumH_convergence}
\end{figure}

\begin{figure}[H]
\begin{minipage}[t]{0.50\textwidth}
 \centering
 \includegraphicsmaybe{[width=\textwidth]}{../img/t2d_bumpsub_CHARAC_errors_tf_finemesh_H.png}
\end{minipage}%
\begin{minipage}[t]{0.50\textwidth}
 \centering
 \includegraphicsmaybe{[width=\textwidth]}{../img/t2d_bumpsub_N_errors_tf_finemesh_H.png}
\end{minipage}
\begin{minipage}[t]{0.50\textwidth}
 \centering
 \includegraphicsmaybe{[width=\textwidth]}{../img/t2d_bumpsub_PSI_errors_tf_finemesh_H.png}
\end{minipage}%
\begin{minipage}[t]{0.50\textwidth}
 \centering
 \includegraphicsmaybe{[width=\textwidth]}{../img/t2d_bumpsub_LIPS_errors_tf_finemesh_H.png}
\end{minipage}
\begin{minipage}[t]{0.50\textwidth}
 \centering
 \includegraphicsmaybe{[width=\textwidth]}{../img/t2d_bumpsub_NERD_errors_tf_finemesh_H.png}
\end{minipage}%
\begin{minipage}[t]{0.50\textwidth}
 \centering
 \includegraphicsmaybe{[width=\textwidth]}{../img/t2d_bumpsub_PC1_errors_tf_finemesh_H.png}
\end{minipage}
\begin{minipage}[t]{0.50\textwidth}
 \centering
 \includegraphicsmaybe{[width=\textwidth]}{../img/t2d_bumpsub_PC2_errors_tf_finemesh_H.png}
\end{minipage}%
\begin{minipage}[t]{0.50\textwidth}
 \centering
 \includegraphicsmaybe{[width=\textwidth]}{../img/t2d_bumpsub_ERIA_errors_tf_finemesh_H.png}
\end{minipage}
\begin{minipage}[t]{0.50\textwidth}
 \centering
 \includegraphicsmaybe{[width=\textwidth]}{../img/t2d_bumpsub_KIN1_errors_tf_finemesh_H.png}
\end{minipage}%
\begin{minipage}[t]{0.50\textwidth}
 \centering
 \includegraphicsmaybe{[width=\textwidth]}{../img/t2d_bumpsub_KIN2_errors_tf_finemesh_H.png}
\end{minipage}
\begin{minipage}[t]{0.50\textwidth}
 \centering
 \includegraphicsmaybe{[width=\textwidth]}{../img/t2d_bumpsub_HLLC1_errors_tf_finemesh_H.png}
\end{minipage}%
\begin{minipage}[t]{0.50\textwidth}
 \centering
 \includegraphicsmaybe{[width=\textwidth]}{../img/t2d_bumpsub_HLLC2_errors_tf_finemesh_H.png}
\end{minipage}
  \caption{H convergence with different numerical schemes.}
 \label{t2d:bumpsub:mesh_convergence_H}
\end{figure}


\begin{figure}[H]
\begin{minipage}[t]{0.50\textwidth}
 \centering
 \includegraphicsmaybe{[width=\textwidth]}{../img/t2d_bumpsub_CHARAC_errors_tf_finemesh_U.png}
\end{minipage}%
\begin{minipage}[t]{0.50\textwidth}
 \centering
 \includegraphicsmaybe{[width=\textwidth]}{../img/t2d_bumpsub_N_errors_tf_finemesh_U.png}
\end{minipage}
\begin{minipage}[t]{0.50\textwidth}
 \centering
 \includegraphicsmaybe{[width=\textwidth]}{../img/t2d_bumpsub_PSI_errors_tf_finemesh_U.png}
\end{minipage}%
\begin{minipage}[t]{0.50\textwidth}
 \centering
 \includegraphicsmaybe{[width=\textwidth]}{../img/t2d_bumpsub_LIPS_errors_tf_finemesh_U.png}
\end{minipage}
\begin{minipage}[t]{0.50\textwidth}
 \centering
 \includegraphicsmaybe{[width=\textwidth]}{../img/t2d_bumpsub_PC1_errors_tf_finemesh_U.png}
\end{minipage}%
\begin{minipage}[t]{0.50\textwidth}
 \centering
 \includegraphicsmaybe{[width=\textwidth]}{../img/t2d_bumpsub_PC2_errors_tf_finemesh_U.png}
\end{minipage}
\begin{minipage}[t]{0.50\textwidth}
 \centering
 \includegraphicsmaybe{[width=\textwidth]}{../img/t2d_bumpsub_NERD_errors_tf_finemesh_U.png}
\end{minipage}%
\begin{minipage}[t]{0.50\textwidth}
 \centering
 \includegraphicsmaybe{[width=\textwidth]}{../img/t2d_bumpsub_ERIA_errors_tf_finemesh_U.png}
\end{minipage}
\begin{minipage}[t]{0.50\textwidth}
 \centering
 \includegraphicsmaybe{[width=\textwidth]}{../img/t2d_bumpsub_KIN1_errors_tf_finemesh_U.png}
\end{minipage}%
\begin{minipage}[t]{0.50\textwidth}
 \centering
 \includegraphicsmaybe{[width=\textwidth]}{../img/t2d_bumpsub_KIN2_errors_tf_finemesh_U.png}
\end{minipage}
\begin{minipage}[t]{0.50\textwidth}
 \centering
 \includegraphicsmaybe{[width=\textwidth]}{../img/t2d_bumpsub_HLLC1_errors_tf_finemesh_U.png}
\end{minipage}%
\begin{minipage}[t]{0.50\textwidth}
 \centering
 \includegraphicsmaybe{[width=\textwidth]}{../img/t2d_bumpsub_HLLC2_errors_tf_finemesh_U.png}
\end{minipage}
  \caption{U convergence with different numerical schemes.}
 \label{t2d:bumpsub:mesh_convergence_U}
\end{figure}

\begin{figure}[H]
\centering
  \includegraphicsmaybe{[width=0.9\textwidth]}{../img/t2d_bumpsub_errors_tf_finemesh_mesh3.png}
  \caption{H errors on the finest mesh.}
\label{fig:bump:ErrNumH_convergence_mesh3}
\end{figure}


\subsection{Convergence in the critical case}

The same convergence study is carried out for the critical test case.
Convergence of water depth in $L^2$ norm for different numerical schemes are presented in Figure \ref{fig:bumpcri:ErrNumH_convergence}. On the next pages, convergence results for both velocity and water depth
are presented in $L^1$, $L^2$ and $L^{\infty}$ norms. Finally, errors at final time on the finest mesh are presented in Figure \ref{fig:bumpcri:ErrNumH_convergence_mesh3}.

\begin{figure}[H]
\centering
  \includegraphicsmaybe{[width=0.9\textwidth]}{../img/t2d_bumpcri_errors_tf_finemesh_H_L2_allsc.png}
  \caption{H convergence in $L^2$ norm.}
\label{fig:bumpcri:ErrNumH_convergence}
\end{figure}

\begin{figure}[H]
\begin{minipage}[t]{0.50\textwidth}
 \centering
 \includegraphicsmaybe{[width=\textwidth]}{../img/t2d_bumpcri_CHARAC_errors_tf_finemesh_H.png}
\end{minipage}%
\begin{minipage}[t]{0.50\textwidth}
 \centering
 \includegraphicsmaybe{[width=\textwidth]}{../img/t2d_bumpcri_N_errors_tf_finemesh_H.png}
\end{minipage}
\begin{minipage}[t]{0.50\textwidth}
 \centering
 \includegraphicsmaybe{[width=\textwidth]}{../img/t2d_bumpcri_PSI_errors_tf_finemesh_H.png}
\end{minipage}%
\begin{minipage}[t]{0.50\textwidth}
 \centering
 \includegraphicsmaybe{[width=\textwidth]}{../img/t2d_bumpcri_LIPS_errors_tf_finemesh_H.png}
\end{minipage}
\begin{minipage}[t]{0.50\textwidth}
 \centering
 \includegraphicsmaybe{[width=\textwidth]}{../img/t2d_bumpcri_PC1_errors_tf_finemesh_H.png}
\end{minipage}%
\begin{minipage}[t]{0.50\textwidth}
 \centering
 \includegraphicsmaybe{[width=\textwidth]}{../img/t2d_bumpcri_PC2_errors_tf_finemesh_H.png}
\end{minipage}
\begin{minipage}[t]{0.50\textwidth}
 \centering
 \includegraphicsmaybe{[width=\textwidth]}{../img/t2d_bumpcri_NERD_errors_tf_finemesh_H.png}
\end{minipage}%
\begin{minipage}[t]{0.50\textwidth}
 \centering
 \includegraphicsmaybe{[width=\textwidth]}{../img/t2d_bumpcri_ERIA_errors_tf_finemesh_H.png}
\end{minipage}
\begin{minipage}[t]{0.50\textwidth}
 \centering
 \includegraphicsmaybe{[width=\textwidth]}{../img/t2d_bumpcri_KIN1_errors_tf_finemesh_H.png}
\end{minipage}%
\begin{minipage}[t]{0.50\textwidth}
 \centering
 \includegraphicsmaybe{[width=\textwidth]}{../img/t2d_bumpcri_KIN2_errors_tf_finemesh_H.png}
\end{minipage}
\begin{minipage}[t]{0.50\textwidth}
 \centering
 \includegraphicsmaybe{[width=\textwidth]}{../img/t2d_bumpcri_HLLC1_errors_tf_finemesh_H.png}
\end{minipage}%
\begin{minipage}[t]{0.50\textwidth}
 \centering
 \includegraphicsmaybe{[width=\textwidth]}{../img/t2d_bumpcri_HLLC2_errors_tf_finemesh_H.png}
\end{minipage}
  \caption{H convergence with different numerical schemes.}
 \label{t2d:bumpcri:mesh_convergence_H}
\end{figure}


\begin{figure}[H]
\begin{minipage}[t]{0.50\textwidth}
 \centering
 \includegraphicsmaybe{[width=\textwidth]}{../img/t2d_bumpcri_CHARAC_errors_tf_finemesh_U.png}
\end{minipage}%
\begin{minipage}[t]{0.50\textwidth}
 \centering
 \includegraphicsmaybe{[width=\textwidth]}{../img/t2d_bumpcri_N_errors_tf_finemesh_U.png}
\end{minipage}
\begin{minipage}[t]{0.50\textwidth}
 \centering
 \includegraphicsmaybe{[width=\textwidth]}{../img/t2d_bumpcri_PSI_errors_tf_finemesh_U.png}
\end{minipage}%
\begin{minipage}[t]{0.50\textwidth}
 \centering
 \includegraphicsmaybe{[width=\textwidth]}{../img/t2d_bumpcri_LIPS_errors_tf_finemesh_U.png}
\end{minipage}
\begin{minipage}[t]{0.50\textwidth}
 \centering
 \includegraphicsmaybe{[width=\textwidth]}{../img/t2d_bumpcri_PC1_errors_tf_finemesh_U.png}
\end{minipage}%
\begin{minipage}[t]{0.50\textwidth}
 \centering
 \includegraphicsmaybe{[width=\textwidth]}{../img/t2d_bumpcri_PC2_errors_tf_finemesh_U.png}
\end{minipage}
\begin{minipage}[t]{0.50\textwidth}
 \centering
 \includegraphicsmaybe{[width=\textwidth]}{../img/t2d_bumpcri_NERD_errors_tf_finemesh_U.png}
\end{minipage}%
\begin{minipage}[t]{0.50\textwidth}
 \centering
 \includegraphicsmaybe{[width=\textwidth]}{../img/t2d_bumpcri_ERIA_errors_tf_finemesh_U.png}
\end{minipage}
\begin{minipage}[t]{0.50\textwidth}
 \centering
 \includegraphicsmaybe{[width=\textwidth]}{../img/t2d_bumpcri_KIN1_errors_tf_finemesh_U.png}
\end{minipage}%
\begin{minipage}[t]{0.50\textwidth}
 \centering
 \includegraphicsmaybe{[width=\textwidth]}{../img/t2d_bumpcri_KIN2_errors_tf_finemesh_U.png}
\end{minipage}
\begin{minipage}[t]{0.50\textwidth}
 \centering
 \includegraphicsmaybe{[width=\textwidth]}{../img/t2d_bumpcri_HLLC1_errors_tf_finemesh_U.png}
\end{minipage}%
\begin{minipage}[t]{0.50\textwidth}
 \centering
 \includegraphicsmaybe{[width=\textwidth]}{../img/t2d_bumpcri_HLLC2_errors_tf_finemesh_U.png}
\end{minipage}
  \caption{U convergence with different numerical schemes.}
 \label{t2d:bumpcri:mesh_convergence_U}
\end{figure}

\begin{figure}[H]
\centering
  \includegraphicsmaybe{[width=0.9\textwidth]}{../img/t2d_bumpcri_errors_tf_finemesh_mesh3.png}
  \caption{H errors on the finest mesh.}
\label{fig:bumpcri:ErrNumH_convergence_mesh3}
\end{figure}


\section{Conclusion}

To conclude, this type of channel flow is adequately reproduced by
\telemac{2D} in either subcritical, transcritical or critical flow regimes.



