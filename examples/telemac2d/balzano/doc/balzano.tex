\chapter{Balzano}

\section{Purpose}

This test illustrates that \telemac{2D} is able to simulate water retention in a bowled beach.
It is also useful to check that the schemes are well-balanced and
mass conservative in severe drying conditions.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
\section{Description}

\subsection{Geometry and mesh}
\label{subsection:balzano:bathy}

The computational domain is a 13,800~m $\times$ 1,200~m rectangular channel.
A triangular non-structured mesh is constructed on this domain and contains
8,000 triangular elements and 4,161 nodes (cf. Figure \ref{fig:balzano:mesh}).

\begin{figure}[H]
  \centering
  \includegraphicsmaybe{[width=\textwidth]}{../img/t2d_balzano_mesh.png}
  \caption{Geometry and mesh of the Balzano test case.}\label{fig:balzano:mesh}
\end{figure}

\subsection{Bathymetry}

The channel is characterized by a uniform slope,
going from maximum $z$ = 0~m at $x$ = 0~m to $z$ = -5~m at $x$ = 13,800~m, with a
reservoir located at $x$ = 4,800~m.
The bathymetry is defined in the \telfile{USER\_CORFON} subroutine as follows:

\begin{equation}
   z(x) =
    \begin{cases}
      -\frac{x}{2760} ,& x < 3600\\
       \frac{x}{2760} - \frac{60}{23} ,&  3600 \leq x < 4800\\
       -\frac{x}{920} + \frac{100}{23} ,& 4800 \leq x \leq 6000 \\
       -\frac{x}{2760} ,& x \ge 6000
    \end{cases}
    \label{eq:balzano:analytical}
\end{equation}

A 3D plot of the bathymetry can be found in Figure~\ref{fig:balzano:bathy},
as well as the profile of bathymetry along the $x$ axis.

\begin{figure}[h!]
\begin{minipage}[t]{0.45\textwidth}
 \centering
 \includegraphicsmaybe{[width=\textwidth]}{../img/bathymetry1D.png}
\end{minipage}%
\begin{minipage}[t]{0.55\textwidth}
 \centering
 \includegraphicsmaybe{[width=\textwidth]}{../img/t2d_balzano_bathy.png}
\end{minipage}
\caption{Bathymetry of the Balzano test case.}\label{fig:balzano:bathy}
\end{figure}

%\begin{figure}[H]
%  \centering
%  \includegraphicsmaybe{[width=0.6\textwidth]}{../img/t2d_balzano_bathy.png}
%  \caption{Bathymetry of the Balzano test case}\label{fig:balzano:bathy}
%\end{figure}
%
%\begin{figure}[H]
%  \centering
%  \includegraphicsmaybe{[width=0.6\textwidth]}{../img/bathymetry1D.png}
%  \caption{Longitudinal bathymetry profile of the Balzano case.}\label{fig:balzano:bathy1D}
%\end{figure}

\subsection{Initial conditions}

At the beginning of the simulation the water is at rest (no velocities),
with a constant free surface elevation equal to 2~m.

\subsection{Boundary conditions}

The boundaries are solid walls everywhere, except on the right-hand side
boundary, which is a liquid boundary.
There is no friction on the solid walls and a water level is prescribed at the
liquid boundary, with a value of -2~m.

\subsection{Analytical solution at equilibrium}

Given the initial and boundary conditions, during the simulation
the water level gradually decreases until it reaches the prescribed value of -2~m
in the right-hand
side part of the domain ($x$ > 4,800~m). In the left-hand side
part of the domain ($x$ < 4,800~m), the water level gradually reaches the
elevation $z$ = -0.87~m, which
is the elevation of the bathymetry peak at $x$ = 4,800~m, and stabilizes.
This is a severe drying scenario,
and the stabilization of the free surface in both parts of the domain is a
worthy result in itself.
The analytical solution for the free surface at equilibrium is expressed as
follows:
\begin{itemize}
\item -2~m in the right-hand side of the domain ($x$ > 4,800~m),
\item -0.87~m in the left-hand side part of the domain ($x$ < 4,800~m).
\end{itemize}

\subsection{Physical parameters}

The physical characteristics of the case are the following:
\begin{itemize}
  \itemsep0em
\item Seaside water depth $H_0$ = 3~m,
\item Characteristic length $L$ = 13,800~m,
\item Characteristic slope defined by $\tan(\alpha) = ~3.6 \times 10^{-4}$ m,
\item Galilei Number \textbf{$G_a = \dfrac{g \times L^3}{\nu} =  2.58 \times 10^{19}$}
  where $\nu$ is the kinematic viscosity of water and $g$ is the gravity acceleration.
\end{itemize}

The viscosity is set as constant and equal to 0~m$^2$/s
(\telkey{VELOCITY DIFFUSIVITY} = 0.).

\subsection{Numerical parameters}

For this test case, duration is set to 70,000~s and several numerical schemes
are confronted.
The solver used is the conjugate gradient with an accuracy of $10^{-4}$.
For finite element schemes the treatment of the linear system is set to wave
equation (\telkey{TREATMENT OF THE LINEAR SYSTEM} = 2).
The simulation parameters specific to each case are summed up in
Table~\ref{tab:balzano:cases}.
\begin{table}[H]
  \resizebox{\textwidth}{!}{%
    \begin{tabular}{|c|c|c|c|c|c|}
      \hline Case & Name & Equations & Elements for U,V \& H  & Advection scheme for velocities & Time-step / Desired Courant number \\
      \hline 1 & CHAR & Wave Eq. FE & P1-P1 & characteristics &  5~s / - \\
      \hline 2 & NERD & Wave Eq. FE & P1-P1 & edge-based N-scheme & 5~s / - \\
      \hline 3 & ERIA & Wave Eq. FE & P1-P1 & ERIA scheme & 5~s / - \\
      \hline 4 & KIN1 & Saint-Venant FV & - & kinetic order 1 & - / 0.9 \\
      \hline 5 & HLLC & Saint-Venant FV & - & HLLC order 1 & - / 0.9 \\
      \hline
    \end{tabular}
  }
  \caption{List of the simulation parameters used for the five cases.}
  \label{tab:balzano:cases}
\end{table}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
\section{Results}

\subsection{First observation}
\label{subsection:balzano:observations}

The water level in the reservoir gradually reaches the elevation -0.87~m.
On the right side of the bump,
an hydraulic jump forms as well as near the exit boundary until water level on
the right-hand side of the domain stabilize around -2~m.
The evolution of the free surface during the simulation is presented in Figure
\ref{fig:balzano:firstobs} with the kinetic scheme.

\begin{figure}[H]
\begin{minipage}[t]{0.5\textwidth}
 \centering
 \includegraphicsmaybe{[width=0.9\textwidth]}{../img/SL_firstobs.png}
\end{minipage}%
\begin{minipage}[t]{0.5\textwidth}
 \centering
 \includegraphicsmaybe{[width=0.9\textwidth]}{../img/U_firstobs.png}
\end{minipage}
\caption{Free surface (left) and velocity (right) at different times with the first order kinetic scheme.}
  \label{fig:balzano:firstobs}
\end{figure}

\subsection{Computation time}

\begin{figure}[H]
  \centering
  \includegraphicsmaybe{[width=0.65\textwidth]}{../img/t2d_balzano_cpu_times.png}
  \caption{CPU times.}\label{fig:balzano:cputime}
\end{figure}

Simulation times for each of these cases with sequential and parallel runs
(using 4 processors) are shown in Figure~\ref{fig:balzano:cputime}
\footnote{Keep in mind that these times
are specific to the validation run and the type of processors that were used for this purpose.}.

\subsection{Comparison of schemes}

\begin{figure}[H]
\begin{minipage}[t]{0.5\textwidth}
 \centering
 \includegraphicsmaybe{[width=0.9\textwidth]}{../img/SL_0.png}
\end{minipage}%
\begin{minipage}[t]{0.5\textwidth}
 \centering
 \includegraphicsmaybe{[width=0.9\textwidth]}{../img/SL_2500.png}
\end{minipage}
\begin{minipage}[t]{0.5\textwidth}
 \centering
 \includegraphicsmaybe{[width=0.9\textwidth]}{../img/SL_5000.png}
\end{minipage}%
\begin{minipage}[t]{0.5\textwidth}
 \centering
 \includegraphicsmaybe{[width=0.9\textwidth]}{../img/SL_10000.png}
\end{minipage}
\begin{minipage}[t]{0.5\textwidth}
 \centering
 \includegraphicsmaybe{[width=0.9\textwidth]}{../img/SL_20000.png}
\end{minipage}%
\begin{minipage}[t]{0.5\textwidth}
 \centering
 \includegraphicsmaybe{[width=0.9\textwidth]}{../img/SL_70000.png}
\end{minipage}
\caption{Free surface shape at different times in the Balzano case.}
\label{fig:balzano:SL}
\end{figure}

In this section, we compare the solution obtained with all the numerical schemes.
In Figure \ref{fig:balzano:SL} the evolution of the free surface is presented.
Kinetic and HLLC schemes handle very well discontinuities and the jumps are well
captured.
CHAR, NERD and ERIA schemes are not able to capture the jumps with large time
steps and numerical oscillations are visible when the drying occurs.
Once the hydraulic jump disappears the solutions are smoother and slowly
converge towards equilibrium due to numerical diffusion.

\begin{figure}[H]
\begin{minipage}[t]{0.5\textwidth}
 \centering
 \includegraphicsmaybe{[width=\textwidth]}{../img/SL_reservoir.png}
\end{minipage}%
\begin{minipage}[t]{0.5\textwidth}
 \centering
 \includegraphicsmaybe{[width=\textwidth]}{../img/SL_downstream.png}
\end{minipage}
\caption{Evolution of the free surface elevation at points P1 and P2 for the five considered setups on the Balzano example.}
\label{fig:balzano:temporalSL}
\end{figure}

We should observe a sinusoidal depletion of the right-hand side boundary level.
The evolution of the water level at two different points, P1 ($x$ = 3,600~m, $y$ = 600~m)
and P2 ($x$ = 8,000~m, $y$ = 600~m), located in the reservoir and at the
right-hand side of the reservoir is plotted in Figure \ref{fig:balzano:temporalSL}.
For all the tested schemes, the asymptotic evolution in the reservoir is
realized and is fast.
On the right-hand side part of the domain, the water level abruptly falls at the
beginning of the simulation, and later oscillates around the value -2~m,
with a decreasing amplitude. In absence of friction and viscosity, the damping
is slow and only comes from numerical diffusion.

\begin{figure}[H]
\begin{minipage}[t]{0.5\textwidth}
 \centering
 \includegraphicsmaybe{[width=\textwidth]}{../img/U_reservoir.png}
\end{minipage}%
\begin{minipage}[t]{0.5\textwidth}
 \centering
 \includegraphicsmaybe{[width=\textwidth]}{../img/U_downstream.png}
\end{minipage}
\caption{Evolution of the streamwise velocity using different schemes on the Balzano case.}
\label{fig:balzano:temporalU}
\end{figure}

Velocity values can also be compared in the reservoir (P1) and at the right-hand
side of the channel (P2) in Figure~\ref{fig:balzano:temporalU}.
In the reservoir, there is an abrupt acceleration at the beginning of the
simulation, followed by an
abrupt deceleration, towards a permanent regime establishment (zero velocity).
At the right-hand side of the channel, the same acceleration and deceleration
phases are followed by an oscillation around a zero velocity for all schemes.

%TODO

%In the case of simulations with friction (Strickler coefficient $k_s = 50 m^{1/3}s^{-1}$), the stabilization happens for all schemes, because the friction source term balances the bathymetry source term (slope). Stabilization is quite fast, as shown in Figure~\ref{fig:balzano:temporalSL_withFriction}.

%\begin{figure}[H]
%\centering
%%ligne1
%\mbox{
%\subfloat[][P1]{
%\includegraphics*[width=0.48\textwidth]{img/SL_reservoir_withFriction.png}
%}
%\subfloat[][P2]{
%\includegraphics*[width=0.48\textwidth]{img/SL_downstream_withFriction.png}
%}}
%\caption{Evolution of the free surface elevation at points P1 and P2 for the five considered setups on the Balzano example, with addition of friction in the model.
%\textcolor{red}{Warning: these figures were generated with the version V7P3 and are not automatically re-built during the validation process.}}
%\label{fig:balzano:temporalSL_withFriction}
%\end{figure}

\subsection{Positivity of the water depth}

The minimum values of the water depth are checked during all simulation.
Results are shown in Figure \ref{t2d:balzano:minmax}.
In the case of finite volume schemes, positivity is ensured without additionnal
treatment.
With finite element schemes the positivity is ensured with a treatment of
negative depths during simulation (\telkey{TREATMENT OF NEGATIVE DEPTHS} = 2
for characteristics and NERD and 3 for ERIA scheme).

\begin{figure}[H]
\centering
\includegraphicsmaybe{[width=0.9\textwidth]}{../img/t2d_balzano_minT.png}
\caption{Minimum values of water depths.}
\label{t2d:balzano:minmax}
\end{figure}

\subsection{Mass balance}

Mass conservation can be checked by calculating the mass in the domain during
time. The lost mass is calculated as
$M_{initial} - M_{final}$ and presented in Figure \ref{fig:balzano:massbalance}.

\begin{figure}[h!]
  \centering
  \includegraphicsmaybe{[width=0.9\textwidth]}{../img/Mass_balance.png}
  \caption{Balzano mass balance.}\label{fig:balzano:massbalance}
\end{figure}

\subsection{Energy balance}

In this section, the conservation of the mean energy is checked.
In fact, for the Saint-Venant equations,
the quantity $h \frac{||U||^2}{2} + g \frac{h^2}{2}$, called mean or integrated
energy, is conserved,
where $||U||$ is the Saint-Venant depth averaged velocity magnitude.
%\begin{equation}
%  ||U||^2 = \left( \left(\frac{1}{h} \int_0^h u~dz\right) \overrightarrow{e_x} \right)^2 + \left( \left(\frac{1}{h} \int_0^h v~dz\right)  \overrightarrow{e_y} \right)^2
%\end{equation}
%Where $u$ and $v$ are the horizontal components of the 3D velocity. \\
The following quantities are studied:
\begin{itemize}
\item Integrated potential energy \textbf{$E_p =\int\int_{\Omega_{xy}}\rho_{water} g \frac{h^2}{2} dxdy$} where $\Omega_{xy}$ is the $2D$ domain of simulation: Figure~\ref{fig:balzano:Ep},
\item Integrated kinetic energy \textbf{$E_c =\int\int_{\Omega_{xy}} \rho_{water} h \frac{||U||^2}{2} dxdy$}: Figure~\ref{fig:balzano:Ec},
\item Integrated mechanical energy \textbf{$E_m = E_p + E_c$}: Figure~\ref{fig:balzano:Em}.
\end{itemize}

\begin{figure}[H]
  \centering
  \includegraphicsmaybe{*[width=0.8\textwidth, keepaspectratio=true]}{../img/PotentialETime.png}
  \caption{Potential energy evolution for all tested schemes on the Balzano case.}
\label{fig:balzano:Ep}
\end{figure}

\begin{figure}[H]
  \centering
  \includegraphicsmaybe{*[width=0.8\textwidth, keepaspectratio=true]}{../img/KineticETime.png}
  \caption{Kinetic energy evolution for all tested schemes on the Balzano case.}
\label{fig:balzano:Ec}
\end{figure}

\begin{figure}[H]
  \centering
  \includegraphicsmaybe{*[width=0.8\textwidth, keepaspectratio=true]}{../img/MechanicalETime.png}
  \caption{Mechanical energy evolution for all tested schemes on the Balzano case.}
\label{fig:balzano:Em}
\end{figure}

The potential energy evolution is directly linked to the evolution of the free
surface as seen in Figure~\ref{fig:balzano:temporalSL}.
In fact, there is a major loss of potential energy due to the abrupt decrease of
water depth, and later oscillations around a constant value.
The kinetic energy evolution visible in Figure~\ref{fig:balzano:temporalU} is,
on the other hand, linked to the velocity variations.

%\subsection{Accuracy}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
\section{Conclusion}

This test case shows that \telemac{2D} schemes are capable of simulating severe drying problems
with some precautions regarding the choice of numerical parameters.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%\subsection*{Accuracy}
%In order to assess the precision of the tested advection schemes, the results are compared to the analytical solution.
%For this purpose, relative $L^2$ error norms are calculated between the numerical results and the analytical solution,
%for example at $y=600m$ (half-way of the domain along the y-axis - the case does not present any variation along $y$ anyway).
%The values can be found in the Table~\ref{tab:balzano:errorsSL}.
%
%\begin{table}[H]
%  \resizebox{\textwidth}{!}{%
%    \centering
%    \begin{tabular}{|l}
%      \hline Scheme \\
%      $t= t_0 = 65000~s$ \\
%      $t= t_0 + T/3 = 67500~s$ \\
%      $t= t_0 + 2T/3 =70000~s$ \\
%      \hline
%    \end{tabular}
%    \begin{tabular}{|c|c|c|c|c|}
%      \hline   CHAR & NERD & ERIA & KIN1 & HLLC\\
%      \hline
%      \InputIfFileExists{../errorsSL.dat}{}{}\\
%      \hline
%  \end{tabular}}
%  \caption{$L^2$ relative errors of free surface on the channel's center streamline for the Balzano case.}
%  \label{tab:balzano:errorsSL}
%\end{table}
%
%From Table~\ref{tab:balzano:errorsSL}, we can deduce the following:
%\begin{itemize}
%\itemsep0em
%\item ERIA scheme error is the lowest and is stable in time. This is related to the early stabilization of the scheme around the analytical value;
%\item CHAR and NERD give error at its lowest at T/3, because the numerical value meets the analytical one;
%\item HLLC and KIN1 give error at its lowest at 2T/3. This also confirms the previous analysis.
%\end{itemize}
%Relative gap percentage between the analytical and numerical results is also calculated at some points of the
%center streamline of the channel.This error percentage is calculated as a $L^{1}$-type relative error given by $100 * \dfrac{|S_{telemac2d} - S_{analytical}|}{S_{analytical}}$.
%
%\begin{table}[H]
%  \resizebox{\textwidth}{!}{%
%    \centering
%    \begin{tabular}{|c||c|c|c|c|c|c|}
%      \hline   Location & Distance & CHAR & NERD & ERIA & KIN1 & HLLC\\
%      \hline
%      \InputIfFileExists{../errorsPSL.dat}{}{}\\
%      \hline
%  \end{tabular}}
%  \caption{Balzano example: $L^1$ relative gap percentage of free surface on the channel's center streamline at $t=70000~s$.}
%  \label{tab:balzano:errorsPSL}
%\end{table}
%
%For the first and second slope, all schemes have low gap percentages at last time step $t=70000~s$. \\
%In the reservoir, as seen earlier, all the schemes reach the analytical value asymptotically.
%This can also be noticed in the Table~\ref{tab:balzano:errorsPSL}, as the error percentages are very low.
%It is interesting to note that the calculated free surface is quite below the reservoir fill level for all schemes, no exception. \\
%
%For the third slope, where the water level should be equal to $-2~m$, ERIA scheme has the lowest gap percentage values,
%and its result is quite beyond the analytical solution. The CHAR and NERD schemes have
%lower errors than the KIN1 and HLLC schemes at this time of simulation (2T/3), which is coherent with previous analysis. \\
%
%Further more, CHAR errors are lower than NERD errors because CHAR sinusoidal oscillation amplitudes
%are lower than those of NERD, because CHAR is more diffusive. Whereas KIN1 and HLLC have the same
%error amplitudes because there estimations of free surface are very comparable.
