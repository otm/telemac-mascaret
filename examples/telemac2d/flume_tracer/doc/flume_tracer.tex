\chapter{Flume with tracers (flume\_tracer)}

\section{Purpose}
This test shows the performance of the finite volume advection schemes of
\telemac{2D} for passive scalar transport in a time dependent case.
It shows the advection of a tracer (or any other passive scalar) in a flume with
flat frictionless bottom.

%--------------------------------------------------------------------------------------------------
\section{Description}

\subsection{Geometry and mesh}

The dimensions of the domain are [2 $\times$ 20]~m$^2$.
The mesh is made from a regular grid from which all squares are cut in half.
The number of elements and points in the mesh are 729 and 1,280 respectively.

\begin{figure}[h!]
\centering
\includegraphicsmaybe{[width=0.85\textwidth]}{../img/flumetracer_mesh0.png}
\caption{2D domain and mesh of the flume\_tracer test case.}
\label{t2d:flumetracer:mesh}
\end{figure}

\subsection{Initial condition}

The water depth is constant in time and in space, equal to 1~m.
The streamwise velocity $u$ is constant and equal to 1~m/s.
Two tracers are defined in the domain.
Their initial values are taken from analytical solution at time $t$ = 0.

\subsection{Analytical solution}

The first tracer is described by a Gaussian function and the second is defined
by a crenel.
The analytical solution for both tracers $T_1$ and $T_2$ are given by:

\begin{equation*}
T_1(x,y,t)=e^{-\frac{1}{2}{(x-x_0(t))^2}}
\end{equation*}

and 

\begin{equation*}
T_2(x,y,t)=
\left\{
    \begin{array}{ll}
        1 \quad \text{if} \quad x_1(t) \leq x \leq x_2(t) \\
        0 \quad \text{else}
    \end{array}
\right.
\end{equation*}

with 

\begin{equation*}
\left\{
    \begin{array}{ll}
        x_0(t) = 5 + u*t \\
        x_1(t) = 2.5 + u*t \\
        x_2(t) = 7.5 + u*t
    \end{array}
\right.
\end{equation*}


\subsection{Physical parameters}

No bottom friction is imposed and diffusivity of tracer is set to zero.

\subsection{Numerical parameters}

The simulation time is set to 7.5~s.
For tracers advection, most numerical schemes available in \telemac{2D} finite
volume solver are tested.
First order schemes results are very similar for passive scalar transport so
only Roe, Kinetic and HLLC are tested.
Second order reconstructions are tested for Kinetic and HLLC schemes.
We also test different flux limitors on tracer i.e. Minmod (MM), Van Albada (VA)
and monotonized central (MC) limitors.
All limitors are tested with Kinetic scheme.
Since HLLC and Kinetic give very similar results only MC limitor is tested with HLLC.

%--------------------------------------------------------------------------------------------------
\section{Results}

\subsection{Computation time}

Simulation times for each of these cases with sequential and parallel runs
(using 4 processors) are shown in Figure ~\ref{fig:flumetracer:cputime}

\begin{figure}[H]
  \centering
  \includegraphicsmaybe{[width=0.8\textwidth]}{../img/flumetracer_cpu_times.png}
  \caption{CPU times.}\label{fig:flumetracer:cputime}
\end{figure}


\subsection{Comparison of schemes}

One dimensional profiles are extracted from slice plane $(x,y=1,z)$ at final
time in Figure \ref{t2d:flumetracer:1dslice}.

\begin{figure}[H]
\centering
\includegraphicsmaybe{[width=0.95\textwidth]}{../img/t2d_flumetracer_vf_T1_-1.png}
\centering
\includegraphicsmaybe{[width=0.95\textwidth]}{../img/t2d_flumetracer_vf_T2_-1.png}
\caption{1D solution along slice plane $(x,z)$, $y=1$ at $t$ = 7.5~s for $T_1$ (up) and $T_2$ (down).}
\label{t2d:flumetracer:1dslice}
\end{figure}


\newpage
\subsection{Convergence}
To assess the accuracy of the schemes, computation of error on one mesh is not
sufficient.
In this section a mesh convergence is carried out for each numerical scheme.
From a starting mesh, we divide by 4 each triangle recursively to generate new
meshes.
The first 4 meshes used in the convergence study are presented in Figure
\ref{t2d:flumetracer:meshes}.

\begin{figure}[h!]
\begin{minipage}[t]{0.9\textwidth}
 \centering
 \includegraphicsmaybe{[width=\textwidth]}{../img/mesh_0.png}
\end{minipage}
\begin{minipage}[t]{0.9\textwidth}
 \centering
 \includegraphicsmaybe{[width=\textwidth]}{../img/mesh_1.png}
\end{minipage}
\begin{minipage}[t]{0.9\textwidth}
 \centering
 \includegraphicsmaybe{[width=\textwidth]}{../img/mesh_2.png}
\end{minipage}
\begin{minipage}[t]{0.9\textwidth}
 \centering
 \includegraphicsmaybe{[width=\textwidth]}{../img/mesh_3.png}
\end{minipage}
 \caption{2D Mesh used in flume\_tracer mesh convergence.}
 \label{t2d:flumetracer:meshes}
\end{figure}

Convergence slopes of error in $L^1$, $L^2$ and $L^\infty$ norm at final time
are plotted for each numerical scheme in Figure
\ref{t2d:flumetracer:mesh_convergence}.

\begin{figure}[h!]
\begin{minipage}[t]{0.50\textwidth}
 \centering
 \includegraphicsmaybe{[width=\textwidth]}{../img/t2d_flumetracer_KIN1_errors_tf_finemesh_T1.png}
\end{minipage}
\begin{minipage}[t]{0.50\textwidth}
 \centering
 \includegraphicsmaybe{[width=\textwidth]}{../img/t2d_flumetracer_HLLC1_errors_tf_finemesh_T1.png}
\end{minipage}
\begin{minipage}[t]{0.50\textwidth}
 \centering
 \includegraphicsmaybe{[width=\textwidth]}{../img/t2d_flumetracer_KIN2-MM_errors_tf_finemesh_T1.png}
\end{minipage}
\begin{minipage}[t]{0.50\textwidth}
 \centering
 \includegraphicsmaybe{[width=\textwidth]}{../img/t2d_flumetracer_KIN2-VA_errors_tf_finemesh_T1.png}
\end{minipage}
\begin{minipage}[t]{0.50\textwidth}
 \centering
 \includegraphicsmaybe{[width=\textwidth]}{../img/t2d_flumetracer_KIN2-MC_errors_tf_finemesh_T1.png}
\end{minipage}
\begin{minipage}[t]{0.50\textwidth}
 \centering
 \includegraphicsmaybe{[width=\textwidth]}{../img/t2d_flumetracer_HLLC2-MC_errors_tf_finemesh_T1.png}
\end{minipage}
 \caption{Mesh convergence of numerical schemes.}
 \label{t2d:flumetracer:mesh_convergence}
\end{figure}

Convergence slopes in $L^2$ norm are compared in Figure
\ref{t2d:flumetracer:error_slopes_allvars}.
Errors on the finest mesh are presented in Figure
\ref{t2d:flumetracer:error_finemesh}.

\begin{figure}[h!]
\centering
\includegraphicsmaybe{[width=0.9\textwidth]}{../img/t2d_flumetracer_errors_tf_T1_L2_allsc.png}
\caption{Mesh convergence in $L^2$ norm.}
\label{t2d:flumetracer:error_slopes_allvars}
\end{figure}

\begin{figure}[h!]
\centering
\includegraphicsmaybe{[width=0.9\textwidth]}{../img/t2d_flumetracer_T1_errors_tf_finemesh_mesh3.png}
\caption{Mesh convergence in $L^2$ norm.}
\label{t2d:flumetracer:error_finemesh}
\end{figure}

%--------------------------------------------------------------------------------------------------

\section{Conclusion}
\telemac{2D} is able to model passive scalar transport problems in shallow water flows.
