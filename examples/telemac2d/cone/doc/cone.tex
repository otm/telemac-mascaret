\chapter{Advection of tracers with a rotating cone (cone)}

\section{Purpose}
This test shows the performance of the finite element advection schemes of
\telemac{2D} for passive scalar transport in a time dependent case.
It shows the advection of a tracer (or any other passive scalar) in a square
basin with flat frictionless bottom and with closed boundaries.

\section{Description}

\subsection{Geometry and mesh}

The dimensions of the domain are $[d \times d]=[20 \times 20]~\rm{m}^2$.
The mesh is made from a regular grid from which all square are cut in half.
The number of elements and nodes in the mesh are 20,000 and 10,201 respectively.

\begin{figure}[h!]
\centering
\includegraphicsmaybe{[width=0.6\textwidth]}{../img/cone_mesh_bnd.png}
\caption{2D domain and mesh of the cone test case.}
\label{t2d:cone:mesh}
\end{figure}

\subsection{Initial condition}

The water depth is constant in time and in space, equal to 2~m.
The velocity field is constant in time as well and is divergence free:
\begin{equation*}
  \vec{u}=\left\{
         \begin{array}{l}
          u(x,y)=-(y-y_0) \\
          v(x,y)=(x-x_0)
         \end{array}\right.
\end{equation*}
With $x_0$ = 10~m and $y_0$ = 10~m. The initial value for the tracer is given by
the Gaussian function off-centered 5~m to the right of $(x_0, y_0)$:
\begin{equation*}
c^0(x,y)=e^{-\frac{1}{5}[(x-x_0-5)^2+(y-y_0)^2]}.
\end{equation*}

\subsection{Analytical solution}

The tracer is described by a Gaussian function and is submitted to a rotating
velocity field.
After one period we expect that the tracer function has the same position and
the same values as the initial condition (i.e. maximum value equal to 1 at the
center).
The analytical solution for the tracer $c$ is given by:
\begin{equation*}
c(x,y,t)=e^{-\frac{1}{5}[X^2+Y^2]},
\end{equation*}
with:
\begin{equation*}
\left\{
    \begin{array}{ll}
        X = x - x_0 - R \cos(\omega t) \\
        Y = y - y_0 - R \sin(\omega t)
    \end{array}
\right.
\end{equation*}
where $R$ = 5~m.

\subsection{Physical parameters}

In this case the tracer advection equation is solved using fixed hydrodynamic
conditions.
No bottom friction is imposed and diffusivity of tracer is set to zero.
Angular velocity of the rotating cone is equal to 1 rad.s$^{-1}$ which gives a
rotation period equal to $T=2\pi$ (6.28~s).

\subsection{Numerical parameters}

The simulation time is set to one period of rotation.
The time step is chosen in order to do the whole period in 64 steps, so it is
equal to 0.098174771~s.

For tracers advection, all the numerical schemes available in \telemac{2D} are
tested.
For weak characteristics, the number of Gauss points is set to 12.
For distributive schemes, like predictor-corrector (PC) schemes (scheme 4 and 5
with options 2, 3) and locally implicit schemes (LIPS: scheme 4 and 5 with
options 4), the number of corrections is set to 5, which is usually sufficient
to converge to accurate results. For the locally implicit schemes (scheme 4 and
5 with option 4), the number of sub-steps is equal to 20.

\section{Results}

\subsection{Comparison of schemes}

The final contour maps after one rotation of the cone are plotted for each
scheme in Figures \ref{t2d:cone:profiles1} and \ref{t2d:cone:profiles2}.
One dimensional profiles are also extracted from slice plane $(x,y=10,z)$
at $t = T/2$ and $t = T$ in Figure \ref{t2d:cone:1dslice}.

\newpage

\begin{figure}[H]
\begin{minipage}[t]{0.50\textwidth}
 \centering
 \includegraphicsmaybe{[width=0.95\textwidth]}{../img/figure_EX.png}
\end{minipage}%
\begin{minipage}[t]{0.50\textwidth}
 \centering
 \includegraphicsmaybe{[width=0.95\textwidth]}{../img/figure_WCHAR.png}
\end{minipage}
\begin{minipage}[t]{0.50\textwidth}
 \centering
 \includegraphicsmaybe{[width=0.95\textwidth]}{../img/figure_NERD.png}
\end{minipage}
\begin{minipage}[t]{0.50\textwidth}
 \centering
 \includegraphicsmaybe{[width=0.95\textwidth]}{../img/figure_SCHAR.png}
\end{minipage}
\begin{minipage}[t]{0.50\textwidth}
 \centering
 \includegraphicsmaybe{[width=0.95\textwidth]}{../img/figure_NLIPS.png}
\end{minipage}%
\begin{minipage}[t]{0.50\textwidth}
 \centering
 \includegraphicsmaybe{[width=0.95\textwidth]}{../img/figure_PSILIPS.png}
\end{minipage}
\begin{minipage}[t]{0.50\textwidth}
 \centering
 \includegraphicsmaybe{[width=0.95\textwidth]}{../img/figure_PSIPC1.png}
\end{minipage}%
\begin{minipage}[t]{0.50\textwidth}
 \centering
 \includegraphicsmaybe{[width=0.95\textwidth]}{../img/figure_NPC1.png}
\end{minipage}
  \caption{Cone test: contour maps of tracer after one period of rotation, for the advection schemes of \telemac{2D}.}
 \label{t2d:cone:profiles1}
\end{figure}

\begin{figure}[H]
\begin{minipage}[t]{0.50\textwidth}
 \centering
 \includegraphicsmaybe{[width=0.95\textwidth]}{../img/figure_PSIPC2.png}
\end{minipage}%
\begin{minipage}[t]{0.50\textwidth}
 \centering
 \includegraphicsmaybe{[width=0.95\textwidth]}{../img/figure_NPC2.png}
\end{minipage}
\begin{minipage}[t]{0.50\textwidth}
 \centering
 \includegraphicsmaybe{[width=0.95\textwidth]}{../img/figure_PSI.png}
\end{minipage}%
\begin{minipage}[t]{0.50\textwidth}
 \centering
 \includegraphicsmaybe{[width=0.95\textwidth]}{../img/figure_N.png}
\end{minipage}
 \caption{Cone test: contour maps of tracer after one period of rotation, for the advection schemes of \telemac{2D}.}
 \label{t2d:cone:profiles2}
\end{figure}

\begin{figure}[H]
\begin{minipage}[t]{0.50\textwidth}
 \centering
 \includegraphicsmaybe{[width=\textwidth]}{../img/figure_1d_0,5T.png}
\end{minipage}%
\begin{minipage}[t]{0.50\textwidth}
 \centering
 \includegraphicsmaybe{[width=\textwidth]}{../img/figure_1d_T.png}
\end{minipage}%
\caption{1D solution along slice plane $(x,z)$, $y$ = 10~m at $t = T/2$ (left) and $t = T$ (right).}
\label{t2d:cone:1dslice}
\end{figure}

\newpage

\subsection{Maximum principle}

The minimum value of the Gaussian function is measured after one rotation.
The maximum value is computed as well, in order to check the respect of the
maximum principle (or monotonicity).
Results are shown in Figures \ref{t2d:cone:minmax}.

\begin{figure}[H]
\centering
\includegraphicsmaybe{[width=0.9\textwidth]}{../img/t2d_cone_maxT.png}
\includegraphicsmaybe{[width=0.9\textwidth]}{../img/t2d_cone_minT.png}
\caption{Maximum and minimum values of tracer after one rotation of the cone.}
\label{t2d:cone:minmax}
\end{figure}

\subsection{Accuracy}

In order to evaluate the behaviour of the scheme, the error norms
$L^1, L^2, L^{\infty}$ are computed.
Error norms are integrated over time to take into account the unsteady nature of
the problem.
The error norms integrated over time for one rotation of the cone are reported
in Figure \ref{t2d:cone:error_timeintegrals}.

\begin{figure}[H]
\centering
\includegraphicsmaybe{[width=0.9\textwidth]}{../img/t2d_cone_errors_timeintegrals.png}
\caption{Error norms integrated over time for one rotation of the cone.}
\label{t2d:cone:error_timeintegrals}
\end{figure}

\newpage
\subsection{Convergence}
To assess the accuracy of the schemes, computation of error on one mesh is not
sufficient.
In this section a mesh convergence is carried out for each numerical scheme.
From a starting mesh with 441 points and 800 elements we divide by 4 each
triangles recursively to generate new meshes.
The first 4 meshes used in the convergence study are presented in Figure
\ref{t2d:cone:meshes}.

\begin{figure}[h!]
\begin{minipage}[t]{0.5\textwidth}
 \centering
 \includegraphicsmaybe{[width=\textwidth]}{../img/mesh_0.png}
\end{minipage}%
\begin{minipage}[t]{0.5\textwidth}
 \centering
 \includegraphicsmaybe{[width=\textwidth]}{../img/mesh_1.png}
\end{minipage}
\begin{minipage}[t]{0.5\textwidth}
 \centering
 \includegraphicsmaybe{[width=\textwidth]}{../img/mesh_2.png}
\end{minipage}%
\begin{minipage}[t]{0.5\textwidth}
 \centering
 \includegraphicsmaybe{[width=\textwidth]}{../img/mesh_3.png}
\end{minipage}
 \caption{2D Mesh used in cone mesh convergence.}
 \label{t2d:cone:meshes}
\end{figure}

Final time is set to $t_f = T/4$.
Time step is set to ensure a constant CFL for each mesh increment.
Strong characteristics, N and PSI schemes converge at first order with a slope
slightly inferior to one.
LIPS and predictor-corrector (PC) schemes as well as NERD have steeper slope of
convergence comprised between one and two.
Convergence slopes of error in $L^1$, $L^2$ and $L^\infty$ norm at final time
are plotted for each numerical scheme in Figure \ref{t2d:cone:mesh_convergence}.

\begin{figure}[h!]
\begin{minipage}[t]{0.50\textwidth}
 \centering
 \includegraphicsmaybe{[width=\textwidth]}{../img/t2d_cone_errors_tf_finemesh_WCHAR.png}
\end{minipage}%
\begin{minipage}[t]{0.50\textwidth}
 \centering
 \includegraphicsmaybe{[width=\textwidth]}{../img/t2d_cone_errors_tf_finemesh_SCHAR.png}
\end{minipage}
\begin{minipage}[t]{0.50\textwidth}
 \centering
 \includegraphicsmaybe{[width=\textwidth]}{../img/t2d_cone_errors_tf_finemesh_PSI.png}
\end{minipage}%
\begin{minipage}[t]{0.50\textwidth}
 \centering
 \includegraphicsmaybe{[width=\textwidth]}{../img/t2d_cone_errors_tf_finemesh_N.png}
\end{minipage}
\begin{minipage}[t]{0.50\textwidth}
 \centering
 \includegraphicsmaybe{[width=\textwidth]}{../img/t2d_cone_errors_tf_finemesh_PSILIPS.png}
\end{minipage}%
\begin{minipage}[t]{0.50\textwidth}
 \centering
 \includegraphicsmaybe{[width=\textwidth]}{../img/t2d_cone_errors_tf_finemesh_NLIPS.png}
\end{minipage}
\begin{minipage}[t]{0.50\textwidth}
 \centering
 \includegraphicsmaybe{[width=\textwidth]}{../img/t2d_cone_errors_tf_finemesh_PSIPC1.png}
\end{minipage}%
\begin{minipage}[t]{0.50\textwidth}
 \centering
 \includegraphicsmaybe{[width=\textwidth]}{../img/t2d_cone_errors_tf_finemesh_NPC1.png}
\end{minipage}
\begin{minipage}[t]{0.50\textwidth}
 \centering
 \includegraphicsmaybe{[width=\textwidth]}{../img/t2d_cone_errors_tf_finemesh_PSIPC2.png}
\end{minipage}%
\begin{minipage}[t]{0.50\textwidth}
 \centering
 \includegraphicsmaybe{[width=\textwidth]}{../img/t2d_cone_errors_tf_finemesh_NPC2.png}
\end{minipage}
\begin{minipage}[t]{\textwidth}
 \centering
 \includegraphicsmaybe{[width=0.5\textwidth]}{../img/t2d_cone_errors_tf_finemesh_NERD.png}
\end{minipage}
  \caption{Mesh convergence of numerical schemes.}
 \label{t2d:cone:mesh_convergence}
\end{figure}

Convergence slopes in $L^2$ norm are compared in Figure
\ref{t2d:cone:error_timeintegrals}.
Error with strong characteristics, N and PSI schemes are of the same magnitude
as well as LIPS, PC, NERD.
Errors on the finest mesh are presented in Figure
\ref{t2d:cone:error_timeintegrals_mesh3}.

\begin{figure}[h!]
\centering
\includegraphicsmaybe{[width=0.9\textwidth]}{../img/t2d_cone_errors_tf_finemesh_L2_allvars.png}
\caption{Mesh convergence in $L^2$ norm.}
\label{t2d:cone:error_timeintegrals}
\end{figure}

\begin{figure}[H]
\centering
\includegraphicsmaybe{[width=0.9\textwidth]}{../img/t2d_cone_errors_timeintegrals_mesh3.png}
\caption{Error norms integrated over time for the finest mesh.}
\label{t2d:cone:error_timeintegrals_mesh3}
\end{figure}

\section{Conclusion}
\telemac{2D} is able to model passive scalar transport problems in shallow water flows.
