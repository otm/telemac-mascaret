\chapter{Flow in a channel with a cavity (cavity)}

\section{Purpose}
This test case is inspired from the paper from Faure (1995)
\cite{Faure1995} where the constant viscosity and $k-\epsilon$ turbulence models
are compared to tests performed in a physical model. The Smagorinski model has
been added to \telemac{2D} after this comparison because it proved to be more
efficient on this class of applications.

\section{Approach}
A 23~m long and 3~m wide channel is fed by a constant flow. The fluid
is well-mixed over the 0.43~m depth: no stratification is present. On the
left bank of the channel is located a harbour cavity. The cavity has a
cross-section (perpendicular to the channel axis) terminating with a shallow
beach having a mild slope (see Figure \ref{fig:cavity:mesh}).

This three-dimensional configuration induces the formation of large size
eddies. Bijvelds et al. \cite{Bijvelds1997} have shown that vortices induced in
a similar harbour with constant depth are better reproduced with 3D
simulation. In our case, a 2D simulation is possible since the beach where
large size vortices are present is very shallow. The flow is typically unsteady
although boundary conditions are steady. The Smagorinski turbulence model is
tested on this configuration.

\subsection{Geometry and mesh}

\begin{itemize}
\item Size of the model: channel = 23~m $\times$ 3~m, cavity = 6~m $\times$ 3~m,
\item Water depth at rest = 0.43~m.
\end{itemize}

The mesh is dense, particularly in the cavity where the vortices take place and
on the steep junction slope between the cavity and the beach. It is made up
with quadrangles split into two triangles (see Figure \ref{fig:cavity:mesh}).

\begin{itemize}
\item 8,160 triangular elements,
\item 4,233 nodes,
\item Size range: from 0.033 to 0.34 m.
\end{itemize}

\begin{figure}
 \centering
 \includegraphicsmaybe{[width=0.85\textwidth]}{../img/mesh.png}
 \includegraphicsmaybe{[width=0.85\textwidth]}{../img/bottom.png}
 \includegraphicsmaybe{[width=0.85\textwidth]}{../img/FondProfile.png}
 \caption{Mesh and topography.}\label{fig:cavity:mesh}
\end{figure}

\subsection{Boundaries}

\begin{itemize}
\item Channel entrance : $Q$ = 0.155~m$^{3}$/s imposed,
\item Channel outlet: $h$ = 0.445~m imposed,
\item Lateral boundaries: solid walls with slip condition in the channel and no-slip
condition in the cavity (see Figure \ref{fig:cavity:mesh}).
\end{itemize}

Bottom:
In order to simulate small size bottom variations, the bed is uneven.
Manning friction law with bottom roughness = 0.015 s/m$^{1/3}$.
Bed isolines and a cross-section of the cavity are shown in Figure
\ref{fig:cavity:mesh}.

\subsection{Physical parameters}

Turbulence: Model of Smagorinski with coefficient of Smagorinski = 0.1.

\subsection{Numerical parameters}

Type of advection:
\begin{itemize}
\item Characteristics on velocities (scheme \#1),
\item Conservative + modified SUPG on depth (mandatory scheme).
\end{itemize}

Type of element: Quasi-bubble triangle for velocities an linear triangle (P1) for $h$.

\begin{itemize}
\item GMRES solver,
\item Accuracy = 10$^{-3}$,
\item Time data:
  \begin{itemize}
\item Time step = 0.2~s.
\item Simulation duration = 200~s.
  \end{itemize}
\end{itemize}

\section{Results}
The velocity pattern and the free surface elevation in the cavity are shown in
Figures \ref{fig:cavity:vel_prof}
\begin{figure}
 \centering
\includegraphicsmaybe{[width=0.85\textwidth]}{../img/velocity_streamlines_15625.png}
\includegraphicsmaybe{[width=0.85\textwidth]}{../img/freesurface_15625.png}
 \caption{Velocity field and free surface in the cavity at time 15,625~s.}\label{fig:cavity:vel_prof}
\end{figure}

The flow is unsteady particularly in the cavity even though the boundary
conditions are steady. Big and small eddies appear and move periodically in
the cavity (see Figures \ref{fig:cavity:evol_small} and \ref{fig:cavity:evol_large}).
The period of big eddies is longer than the one of small eddies.

\begin{figure}
 \centering
\includegraphicsmaybe{[width=0.49\textwidth]}{../img/velocity_streamlines_15425.png}
\includegraphicsmaybe{[width=0.49\textwidth]}{../img/velocity_streamlines_15450.png}
\includegraphicsmaybe{[width=0.49\textwidth]}{../img/velocity_streamlines_15475.png}
 \includegraphicsmaybe{[width=0.49\textwidth]}{../img/velocity_streamlines_15500.png}
 \caption{Evolution of the velocity field in time for Smagorinski -- Small eddy pattern.}\label{fig:cavity:evol_small}
\end{figure}

\begin{figure}
 \centering
\includegraphicsmaybe{[width=0.49\textwidth]}{../img/velocity_streamlines_15525.png}
\includegraphicsmaybe{[width=0.49\textwidth]}{../img/velocity_streamlines_15550.png}
\includegraphicsmaybe{[width=0.49\textwidth]}{../img/velocity_streamlines_15575.png}
\includegraphicsmaybe{[width=0.49\textwidth]}{../img/velocity_streamlines_15600.png}
 \caption{Evolution of the velocity field in time for Smagorinski -- Large eddy pattern.}\label{fig:cavity:evol_large}
\end{figure}

\begin{figure}
 \centering
\includegraphicsmaybe{[width=0.49\textwidth]}{../img/velocity_streamlines_cte_15425.png}
\includegraphicsmaybe{[width=0.49\textwidth]}{../img/velocity_streamlines_cte_15450.png}
\includegraphicsmaybe{[width=0.49\textwidth]}{../img/velocity_streamlines_cte_15475.png}
 \includegraphicsmaybe{[width=0.49\textwidth]}{../img/velocity_streamlines_cte_15500.png}
 \caption{Evolution of the velocity field in time for constant viscosity -- Small eddy pattern.}\label{fig:cavity:evol_cte_small}
\end{figure}

\begin{figure}
 \centering
\includegraphicsmaybe{[width=0.49\textwidth]}{../img/velocity_streamlines_cte_15525.png}
\includegraphicsmaybe{[width=0.49\textwidth]}{../img/velocity_streamlines_cte_15550.png}
\includegraphicsmaybe{[width=0.49\textwidth]}{../img/velocity_streamlines_cte_15575.png}
\includegraphicsmaybe{[width=0.49\textwidth]}{../img/velocity_streamlines_cte_15600.png}
 \caption{Evolution of the velocity field in time for constant viscosity -- Large eddy pattern.}\label{fig:cavity:evol_cte_large}
\end{figure}

\begin{figure}
 \centering
\includegraphicsmaybe{[width=0.49\textwidth]}{../img/velocity_streamlines_keps_15425.png}
\includegraphicsmaybe{[width=0.49\textwidth]}{../img/velocity_streamlines_keps_15450.png}
\includegraphicsmaybe{[width=0.49\textwidth]}{../img/velocity_streamlines_keps_15475.png}
 \includegraphicsmaybe{[width=0.49\textwidth]}{../img/velocity_streamlines_keps_15500.png}
 \caption{Evolution of the velocity field in time for $k-\epsilon$ -- Small eddy pattern.}\label{fig:cavity:evol_keps_small}
\end{figure}

\begin{figure}
 \centering
\includegraphicsmaybe{[width=0.49\textwidth]}{../img/velocity_streamlines_keps_15525.png}
\includegraphicsmaybe{[width=0.49\textwidth]}{../img/velocity_streamlines_keps_15550.png}
\includegraphicsmaybe{[width=0.49\textwidth]}{../img/velocity_streamlines_keps_15575.png}
\includegraphicsmaybe{[width=0.49\textwidth]}{../img/velocity_streamlines_keps_15600.png}
 \caption{Evolution of the velocity field in time for $k-\epsilon$ -- Large eddy pattern.}\label{fig:cavity:evol_keps_large}
\end{figure}

\section{Conclusions}
The phenomena observed on physical model are successfully reproduced by
\telemac{2D} with the Smagorinski model, although hydrodynamics are three
dimensional in reality. In particular, the small and big eddies structures in
the cavity are well represented.
