\chapter{Rain runoff and hydrostatic reconstruction (rain\_runoff)}
\label{chapter:rain_runoff}

\section{Purpose}

The purpose of this test case is to validate the implementation of the
hydrostatic reconstruction modification proposed by \cite{Chen2017}.
This option is used by activating the keyword
\telkey{OPTION OF THE HYDROSTATIC RECONSTRUCTION} = 2 for kinetic, HLLC and WAF
finite volume schemes.
As several authors have highlighted a limitation in the formulation of the
classical hydrostatic reconstruction \cite{Delestre2012},
this option allows a correction to be made to the hydrostatic reconstruction
only at the point where it fails, i.e. when:

\begin{equation}
 \frac{h}{\Delta z}<1,
\end{equation}

where $h$ is the water depth and $\Delta z=\Delta x\partial_xz$ the difference
of elevation between two neighboring nodes.

To reproduce these conditions, a rain induced runoff test case with a straight
channel that can have three different slopes, on which an analytical solution
and experimental data on the outflow is available, is used.

\section{Description}

\subsection{Analytical solution}

An analytical solution can be calculated to represent the hydrograph at the
channel outlet, proposed by \cite{Delestre2010}.
This solution is only available for the rise of the hydrograph and the plateau,
i.e. when the equilibrium state is reached and the amount of rain falling on the
domain is equal to the outflow at the channel outlet.
The amplitude of this plateau is given by:

\begin{equation}
 q_{out}=R.A,
\end{equation}

where $R$ is the rain intensity and $A$ the channel area. 

The rise of the hydrograph is described by:

\begin{equation}
 \partial_tq_{out}=-gh(S_0+S_f),
\end{equation}

where $g$ is the gravity constant, $S_0$ the slope of the channel and
$S_f$ the friction slope which depends on the chosen friction law
(see \ref{t2d:rain_runoff:physical_parameters}).
Then, with the semi-implicit like treatment of the friction term, one can write
the exact solution of the hydrograph as:

\begin{equation}
 q_{out}^{t+1}=\dfrac{q_{out}^{t}+\Delta t.g.R.S_0.T}{1+S_f},
\end{equation}

with $\Delta t$ a time step chosen for the analytical solution and $T$ the total time. 

Finally, to select if one is on the plateau or the rise, one just has to select
the minimum $q_{out}$ value computed with the two formulae.

\subsection{Experimental data}

Experimental data of the outlet discharge are also available in this test case.
Indeed, \cite{Kirstetter2015} provides data from a laboratory flume which is
4.04~m long and 0.115~m wide.
The water leaving the channel is weighed every second to determine the output
quantity for 2~\%, 5~\% and 25~\% slopes.

\subsection{Geometry and mesh}

The channel is 1~m wide and 5~m long.
The mesh is regular along the channel. It is made up with
triangles following the channel slopes.
It is composed of 2,000 triangular elements (1,061 nodes)
and the size of triangles is about 10~cm
(see Figure \ref{t2d:rain_runoff:fig:mesh}).

\begin{figure}[!htbp]
 \centering
 \includegraphicsmaybe{[width=\textwidth]}{../img/mesh.png}
 \caption{Mesh of the channel.}
 \label{t2d:rain_runoff:fig:mesh}
\end{figure}

\subsection{Bathymetry}
The elevation of the bottom follows a slope defined for each geometry.
There are three different slopes studied: 2~\%, 5~\% and 25~\%.
The slope is in the $x$-direction and the maximum elevation is for $x$ = 0~m and
is 0~m.
Figure \ref{t2d:rain_runoff:fig:bathy} shows the bottom elevation for each
slope considered.

\begin{figure}[H]
 \centering
 \includegraphicsmaybe{[width=0.95\textwidth]}{../img/bottom_2p.png}\\
 \includegraphicsmaybe{[width=0.95\textwidth]}{../img/bottom_5p.png}\\
 \includegraphicsmaybe{[width=0.95\textwidth]}{../img/bottom_25p.png}
 \caption{Bathymetry in the channel.}
 \label{t2d:rain_runoff:fig:bathy}
\end{figure}

\subsection{Initial and boundary conditions}

The initial conditions are zero water depth everywhere.

The boundary conditions are defined as in Figure \ref{t2d:rain_runoff:fig:bcd}.

\begin{figure}[H]
 \centering
 \includegraphicsmaybe{[width=0.95\textwidth]}{../img/bcd.png}
 \caption{Boundaries conditions in the channel.}
 \label{t2d:rain_runoff:fig:bcd}
\end{figure}

\subsection{Physical parameters}
\label{t2d:rain_runoff:physical_parameters}

The Poiseuille friction law is used, programmed in the user Fortran subroutine
\telfile{SOURCE\_MOMENT}. This law writes:

\begin{equation}
 S_f=\frac{3\mu u}{gh^2},
\end{equation}

with $\mu$ the water kinematic viscosity set at $10^{-6}$~m$^2$/s
and $u$ the norm of the velocity.

The rain intensity is variable in time and space.
The rain intensity is constant during the 610 first seconds with a value of
25.83~mm/h. It then stops to observe the decreasing of the hydrograph.
When the rain is activated, it is only applied on the 4.04~m downstream the
channel to reproduce the length of the laboratory case \cite{Kirstetter2015}.
This has been programmed in the \telfile{USER\_RAIN} user subroutine. 

The total time of the simulation is 1,000~s.

\subsection{Numerical parameters}

The test case is run with a maximum time step of 0.1~s and a desired Courant
number of 0.9.
The first order Kinetic scheme is used with both of the hydrostatic reconstruction options. 

\section{Results}

Figure \ref{t2d:rain_runoff:fig:result} shows the outlet discharge for both
hydrostatic reconstruction methods compared to analytical solution and experimental data.
The \cite{Chen2017} scheme allows an improvement of the results, in particular
for very steep slopes.
The outlet discharge has been computed with a flux computation 1~cm above the
downstream boundary, and the mass source term is obtained in the \telemac{2D} listing.

\begin{figure}[H]
 \centering
 \includegraphicsmaybe{[width=0.95\textwidth]}{../img/qout_2p.png}\\
 \includegraphicsmaybe{[width=0.95\textwidth]}{../img/qout_5p.png}\\
 \includegraphicsmaybe{[width=0.95\textwidth]}{../img/qout_25p.png}
 \caption{Outlet discharge for each slope vs analytical solution and experimental data.}
 \label{t2d:rain_runoff:fig:result}
\end{figure}




