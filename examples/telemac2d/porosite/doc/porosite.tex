\chapter{Porosity (porosite)}

\section{Description}

This example checks that \telemac{2D} is able to represent porosity.

The configuration is a straight channel 300~m long and 40~m wide
with a flat horizontal bottom without slope.

\subsection{Initial and boundary conditions}

The computation is initialised with a constant elevation equal to 5~m
and no velocity.

The boundary conditions are:
\begin{itemize}
\item For the solid walls, a slip condition on channel banks is used for the
velocities,
\item On the bottom, a Strickler law with friction coefficient equal to
40~m$^{1/3}$/s is prescribed,
\item Upstream a flowrate equal to 100~m$^3$/s is prescribed,
\item Downstream the water level is equal to 5~m.
\end{itemize}

\subsection{Mesh and numerical parameters}

The mesh (Figure \ref{t2d:porosite:fig:meshH})
is made of 10,508 triangular elements (5,354 nodes).
Is is particularly refined around $x$ = 0~m.

\begin{figure}[!htbp]
 \centering
 \includegraphicsmaybe{[width=\textwidth]}{../img/Mesh.png}
 \caption{Horizontal mesh.}
 \label{t2d:porosite:fig:meshH}
\end{figure}

The time step is 0.2~s for a simulated period of 5,000~s.

To solve the advection, the method of characteristics
is used for the velocities (scheme 1).
The conjugate gradient
is used for solving the propagation step (option 1) and
the implicitation coefficients
for depth and velocities are respectively equal to 1 and 0.6.

\subsection{Physical parameters}

The turbulent viscosity is constant with velocity 
diffusivity equal to 0.1~m$^2$/s.

Porosity is applied by setting
\telkey{TIDAL FLATS} = YES (default value) +
\telkey{OPTION FOR THE TREATMENT OF TIDAL FLATS} = 3 and
by implementing the user subroutine \telfile{USER\_CORPOR}:

\begin{equation*}
\textrm{porosity} =
\left\{
  \begin{array}{cl}
    \frac{1}{2} \left(1 + \frac{|x|}{50} \right) & \textrm{if } -50 \le x \le 50, \\
    1 & \textrm{otherwise.}
  \end{array}
\right.
\end{equation*}


\section{Results}

The flow establishes a steady flow where the free surface is lightly higher
at the entrance and significantly drops where porosity is applied,
up to 5~cm (see Figure \ref{t2d:porosite:FreeSurf}).
Thus we can see that at the locations where porosity is lower than 1,
water depth decreases, what is expected by the porosity feature
(read the \telemac{2D} user manual to know more about it).
The flow accelerates up to twice where porosity is applied and then retrieves
quite similar velocity after (see Figure \ref{t2d:porosite:Velo}).

\begin{figure}[H]
  \centering
  \includegraphicsmaybe{[width=0.9\textwidth]}{../img/FreeSurface.png}
  \caption{Free surface at final time step.}
  \label{t2d:porosite:FreeSurf}
\end{figure}

\begin{figure}[H]
  \centering
  \includegraphicsmaybe{[width=0.9\textwidth]}{../img/Velocity.png}
  \caption{Velocity at final time step.}
  \label{t2d:porosite:Velo}
\end{figure}

\section{Conclusions}

\telemac{2D} is capable to model porosity.
