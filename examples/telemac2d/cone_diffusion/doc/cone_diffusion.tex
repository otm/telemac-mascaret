\chapter{Diffusion of tracers in 2D (cone\_diffusion)}

\section{Purpose}
This test shows the performance of the finite volume diffusion schemes of
\telemac{2D} for passive scalar in a time dependent case.
It shows the diffusion of tracers in 2D in a rectangular domain with different
meshes.

%--------------------------------------------------------------------------------------------------
\section{Description}

\subsection{Geometry and mesh}

The dimensions of the domain are [20 $\times$ 20] m$^2$.
Three meshes are used to compare their impact on the results depending 
on the numerical scheme used. The meshes are plotted in
Figure~\ref{fig:cone_diffusion:meshes}.

\subsection{Initial condition}

The water depth is constant in time and in space, equal to 1~m. The velocity is
constant and equal to 0~m/s.
Two tracers are defined in the domain for which initial conditions are
a Gaussian and a Crenel.

\begin{figure}[h!]
\centering
\includegraphicsmaybe{[width=0.48\textwidth]}{../img/diffusiontracer_T1_type2_HEFE_0.png}
\includegraphicsmaybe{[width=0.48\textwidth]}{../img/diffusiontracer_T2_type2_HEFE_0.png}
\caption{Initial conditions for the two tracers.}
\label{t2d:cone_diffusion:initial_conditions}
\end{figure}

\begin{figure}[h!]
\begin{minipage}[t]{0.5\textwidth}
 \centering
 \includegraphicsmaybe{[width=0.9\textwidth]}{../img/diffusiontracer_mesh0_type1.png}
\end{minipage}%
\begin{minipage}[t]{0.5\textwidth}
 \centering
 \includegraphicsmaybe{[width=0.9\textwidth]}{../img/diffusiontracer_mesh0_zoom_type1.png}
\end{minipage}
\begin{minipage}[t]{0.5\textwidth}
 \centering
 \includegraphicsmaybe{[width=0.9\textwidth]}{../img/diffusiontracer_mesh0_type2.png}
\end{minipage}%
\begin{minipage}[t]{0.5\textwidth}
 \centering
 \includegraphicsmaybe{[width=0.9\textwidth]}{../img/diffusiontracer_mesh0_zoom_type2.png}
\end{minipage}
\begin{minipage}[t]{0.5\textwidth}
 \centering
 \includegraphicsmaybe{[width=0.9\textwidth]}{../img/diffusiontracer_mesh0_type3.png}
\end{minipage}%
\begin{minipage}[t]{0.5\textwidth}
 \centering
 \includegraphicsmaybe{[width=0.9\textwidth]}{../img/diffusiontracer_mesh0_zoom_type3.png}
\end{minipage}
  \caption{2D domain and unstructured, regular, distorted meshes (from top to bottom) with zoom view (right).}
  \label{fig:cone_diffusion:meshes}
\end{figure}

\subsection{Boundary conditions}

All the tests are carried out with Neumann boundary conditions on the walls.

\subsection{Physical parameters}

Tracer diffusion is activated and the following keywords are set:
\begin{itemize}
\item\telkey{DIFFUSION OF TRACERS} = TRUE,
\item\telkey{COEFFICIENT FOR DIFFUSION OF TRACERS} = 0.1;0.1.
\end{itemize}
The stability of the explicit finite volume diffusion schemes is garanted by
choosing:
\begin{itemize}
\item\telkey{DESIRED FOURIER NUMBER} = 0.9.
\end{itemize}

\subsection{Numerical parameters}

The simulation time is set to 10~s.
For tracers diffusion, all numerical schemes available in \telemac{2D} finite
volume solver are tested
i.e. the TPF (Two Point Flux) scheme, the RTPF (Reconstructed Two Point Flux)
scheme,
the explicit P1
finite element scheme with mass lumping, designed to work alongside finite
volume hyperbolic solvers (noted here HEFE for Hybrid Explicit Finite Element).
The schemes are selected via the keyword
\telkey{FINITE VOLUME SCHEME FOR TRACER DIFFUSION}.
Additionally the standard implicit finite element method of \telemac{2D} is
computed for the purpose of comparison.

%--------------------------------------------------------------------------------------------------
\section{Results}

\subsection{Computation time}

Simulation times for each of these cases with sequential and parallel runs
(using 4 processors) are shown in Figure ~\ref{fig:cone_diffusion:cputime}.

\begin{figure}[H]
  \centering
  \includegraphicsmaybe{[width=0.8\textwidth]}{../img/t2d_diffusiontracer_cpu_times_type3.png}
  \caption{CPU times.}\label{fig:cone_diffusion:cputime}
\end{figure}

\subsection{Comparison of diffusion schemes}

\begin{figure}[h!]
 \centering
 \includegraphicsmaybe{[width=0.45\textwidth]}{../img/t2d_diffusiontracer_vf_T1_type1_-1.png}
 \includegraphicsmaybe{[width=0.45\textwidth]}{../img/t2d_diffusiontracer_vf_T2_type1_-1.png}
 \caption{Results at final time in the longitudinal centerline of the domain for the
 two tracers in the case of the first mesh.}
 \label{t2d:cone_diffusion:slices_tf}
\end{figure}

\begin{figure}[h!]
\begin{minipage}[t]{0.5\textwidth}
 \centering
 \includegraphicsmaybe{[width=0.95\textwidth]}{../img/diffusiontracer_T1_type1_TPF_-1.png}
\end{minipage}%
\begin{minipage}[t]{0.5\textwidth}
 \centering
 \includegraphicsmaybe{[width=0.95\textwidth]}{../img/diffusiontracer_T1_type1_RTPF1_-1.png}
\end{minipage}
\begin{minipage}[t]{0.5\textwidth}
 \centering
 \includegraphicsmaybe{[width=0.95\textwidth]}{../img/diffusiontracer_T1_type2_TPF_-1.png}
\end{minipage}%
\begin{minipage}[t]{0.5\textwidth}
 \centering
 \includegraphicsmaybe{[width=0.95\textwidth]}{../img/diffusiontracer_T1_type2_RTPF1_-1.png}
\end{minipage}
\begin{minipage}[t]{0.5\textwidth}
 \centering
 \includegraphicsmaybe{[width=0.95\textwidth]}{../img/diffusiontracer_T1_type3_TPF_-1.png}
\end{minipage}%
\begin{minipage}[t]{0.5\textwidth}
 \centering
 \includegraphicsmaybe{[width=0.95\textwidth]}{../img/diffusiontracer_T1_type3_RTPF1_-1.png}
\end{minipage}
  \caption{TPF (left) and RTPF (right) schemes for the unstructured, regular, distorted meshes (from top to bottom).}
  \label{fig:cone_diffusion:T1_comparisons_tf_fv}
\end{figure}

\begin{figure}[h!]
\begin{minipage}[t]{0.5\textwidth}
 \centering
 \includegraphicsmaybe{[width=0.95\textwidth]}{../img/diffusiontracer_T1_type1_HEFE_-1.png}
\end{minipage}%
\begin{minipage}[t]{0.5\textwidth}
 \centering
 \includegraphicsmaybe{[width=0.95\textwidth]}{../img/diffusiontracer_T1_type1_FE_-1.png}
\end{minipage}
\begin{minipage}[t]{0.5\textwidth}
 \centering
 \includegraphicsmaybe{[width=0.95\textwidth]}{../img/diffusiontracer_T1_type2_HEFE_-1.png}
\end{minipage}%
\begin{minipage}[t]{0.5\textwidth}
 \centering
 \includegraphicsmaybe{[width=0.95\textwidth]}{../img/diffusiontracer_T1_type2_FE_-1.png}
\end{minipage}
\begin{minipage}[t]{0.5\textwidth}
 \centering
 \includegraphicsmaybe{[width=0.95\textwidth]}{../img/diffusiontracer_T1_type3_HEFE_-1.png}
\end{minipage}%
\begin{minipage}[t]{0.5\textwidth}
 \centering
 \includegraphicsmaybe{[width=0.95\textwidth]}{../img/diffusiontracer_T1_type3_FE_-1.png}
\end{minipage}
  \caption{HEFE (left) and FE (right) schemes for the unstructured, regular, distorted meshes (from top to bottom).}
  \label{fig:cone_diffusion:T1_comparisons_tf_fe}
\end{figure}

%--------------------------------------------------------------------------------------------------

\section{Conclusion}
\telemac{2D} is able to model passive scalar diffusion problems in shallow water
flows in 2D with finite volume diffusion schemes.
