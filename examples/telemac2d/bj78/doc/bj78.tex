\chapter{Wave driven currents (bj78)}

\section{Description}

This test demonstrates the ability of \telemac{2D} to model wave driven currents
when chaining with a wave file.

The configuration is a 20.9~m ong and 0.8~m wide channel with piecewise affine
bottom (see Figures \ref{t2d:bj78:fig:Bathy1D} and \ref{t2d:bj78:fig:Bottom})
defined by:
\begin{equation*}
\left\{
    \begin{array}{rl}
        \textrm{if } x < -5, & z_b = -0.616,\\
        \textrm{if } -5 \le x < 5, & z_b = -0.616 + 0.05(x+5),\\
        \textrm{if }  5 \le x < 9.4, & z_b = -0.116 - 0.025(x-5),\\
        \textrm{if } x \ge 9.4, & z_b = -0.226 + 0.05(x-9.4),\\
        \textrm{and } & z_b = \max(-0.04, z_b)\\
    \end{array}
\right.
\end{equation*}

\begin{figure}[!htbp]
 \centering
 \includegraphicsmaybe{[width=0.9\textwidth]}{../img/Bathy1D.png}
 \caption{Bottom elevation and initial condition.}
 \label{t2d:bj78:fig:Bathy1D}
\end{figure}

\begin{figure}[!htbp]
 \centering
 \includegraphicsmaybe{[width=0.9\textwidth]}{../img/Bottom.png}
 \caption{Bottom elevation.}
 \label{t2d:bj78:fig:Bottom}
\end{figure}

\subsection{Initial and boundary conditions}

The computation is initialised with a constant elevation equal to 0~m,
no velocity.

The boundary conditions are:
\begin{itemize}
\item For the solid walls, a slip condition on channel banks is used for the
velocities,
\item On the bottom, a Strickler law with friction coefficient equal to
45~m$^{1/3}$/s is prescribed,
\item Upstream conditions are let free for water depth and velocities.
Thompson conditions are used to compute boundary conditions for this
liquid boundary,
\item Downstream is a solid wall with slip condition for velocities.
\end{itemize}

\subsection{Mesh and numerical parameters}

The 2D mesh (Figure \ref{t2d:bj78:fig:mesh})
is made of 1,394 triangular elements (842 nodes).

\begin{figure}[!htbp]
 \centering
 \includegraphicsmaybe{[width=0.9\textwidth]}{../img/Mesh.png}
 \caption{Horizontal mesh.}
 \label{t2d:bj78:fig:mesh}
\end{figure}

The time step is 0.05~s for a simulated period of 20~s.

To solve the advection, the PSI scheme (scheme 5) is used for the velocities.
GMRES
is used for solving the propagation step (option 7) and
the implicitation coefficients
for depth and velocities are let to default values.

\subsection{Physical parameters}

A constant horizontal viscosity for velocity equal to 0.001~m$^2$/s is chosen.

Wave driven currents are taken into account by chaining \telemac{2D} with
a wave file.

\section{Results}

Figure \ref{t2d:bj78:FreeSurf} shows the free surface elevation at the end of
the computation.

\begin{figure}[H]
  \centering
  \includegraphicsmaybe{[width=0.9\textwidth]}{../img/FreeSurface.png}
  \caption{Free surface at final time step.}
  \label{t2d:bj78:FreeSurf}
\end{figure}

Figure \ref{t2d:bj78:FreeSurfTimeSeries} shows the free surface elevation evolution
during the simulation at point of coordinates (10 ; 0.4).

\begin{figure}[H]
  \centering
  \includegraphicsmaybe{[width=0.9\textwidth]}{../img/FreeSurfaceTimeSeries.png}
  \caption{Free surface time series at point (10 ; 0.4).}
  \label{t2d:bj78:FreeSurfTimeSeries}
\end{figure}

Figure \ref{t2d:bj78:FreeSurfSection} shows free surface elevation cross sections
along $x$ for different times.

\begin{figure}[H]
  \centering
  \includegraphicsmaybe{[width=0.9\textwidth]}{../img/FreeSurfaceSection.png}
  \caption{Free surface elevation cross section for different times.}
  \label{t2d:bj78:FreeSurfSection}
\end{figure}

\section{Conclusion}

\telemac{2D} can be used to model wave driven currents when chaining
with a wave file.
