\chapter{donau}

\section{Purpose}
This test case shows a real case application in order to test zonal friction
values given by a table.
The test case exists since at least release v5p5 but was modified for release
v8p2 in order to take into account lateral boundary friction and to compare the
results to measurements.

\section{Description}

\subsection{Geometry and mesh}
The model area is a 7.7 km stretch of the Danube River near Deggendorf in Germany.
In the grid, several regulation structures (groynes, longitudinal dams) and
bridge piers are integrated using node distances up to 60 cm while the mean
node distances are about 6 m in the main channel and about 20 m at the floodplains.
The grid contains 47,543 nodes and 93,209 elements and can be seen in Figure
\ref{t2d:donau:mesh}.
The bathymetry is shown in Figure \ref{t2d:donau:Bathy}.
The flow direction is from left to right.

\begin{figure}[h!]
\centering
\includegraphicsmaybe{[width=\textwidth]}{../img/donau_mesh.png}
\caption{Mesh and boundary conditions.}
\label{t2d:donau:mesh}
\end{figure}


\begin{figure} [h!]
\centering
\includegraphicsmaybe{[width=\textwidth]}{../img/Bathy.png}
\caption{Bathymetry.}
\label{t2d:donau:Bathy}
\end{figure}


\subsection{Initial condition}

The initial conditions for velocities and water depths are read from the
\telkey{PREVIOUS COMPUTATION FILE}.
With this, the simulation starts from a fully developed flow with steady state
conditions.
The initial water levels and scalar velocities are printed in Figures
\ref{t2d:donau:free_surface0} and \ref{t2d:donau:ini_flow}.

\begin{figure} [h!]
\centering
\includegraphicsmaybe{[width=\textwidth]}{../img/FreeSurface0.png}
\caption{Initial water levels.}
\label{t2d:donau:free_surface0}
\end{figure}

\begin{figure} [h!]
\centering
\includegraphicsmaybe{[width=\textwidth]}{../img/ini_flow.png}
\caption{Initial scalar velocities.}
\label{t2d:donau:ini_flow}
\end{figure}

\subsection{Boundary conditions}
The boundary conditions are chosen according low water conditions in September 1999.
At the inlet, the discharge of 218 m$^3$/s is imposed only at the main channel.
At the outlet, the water depth is fixed to 309.205 m+NN.

%In order to avoid instabilities in the beginning the discharge is increased
%from 0 to 218 m$^3$/s in 10 min and remains constant afterwards with the
%subroutine \telfile{user\_q.f}.


\subsection{Physical parameters}
Seven different friction domains are distinguished.
They are marked with friction IDs given in the \telkey{ZONES FILE} so that the
roughness laws and corresponding parameters can be chosen easily in the
\telkey{FRICTION DATA FILE}.
The values are given in Table \ref{tab:donau:fric_parameter}.
The distribution of the roughness coefficients can be seen in Figure
\ref{t2d:donau:friction}.

\begin{table}[H]
\resizebox{0.7\textwidth}{!}{%
 \begin{tabular}{|l|c|c|c|}
\hline \bf{Friction domain} & \bf{friction law} & \bf{parameter (m)} & \bf{friction ID}\\
\hline
\hline main channel & Nikuradse & 0.025 & 99999024\\
\hline groyne fields & Nikuradse & 0.15 & 99999150\\
\hline regulation structures & Nikuradse &0.3 & 99999300\\
\hline site & Nikuradse & 0.5 & 99999500\\
\hline fields & Nikuradse & 0.1& 99999100\\
\hline forest & Nikuradse & 1 & 99999001\\
\hline buildings & Nikuradse & 10& 99999010\\
\hline
\end{tabular}
}
\caption{Friction law and parameter for the different friction domains.}
\label{tab:donau:fric_parameter}
\end{table}

As this is a long existing example, the Nikuradse law is used for all friction
domains. 
With low water levels at the floodplains, the restriction that the equivalent
sand roughness should not be higher than 50 \% of the water depth will not be
fulfilled.
For the forest, buildings and maybe also for the site, it would be better to
choose a vegetation law.  

The Elder turbulence model is chosen and the molecular viscosity is set to its
physical value 10$^{-6}$ m$^2$/s. 

Four bridges cross the modelled river stretch.
The areas of the bridge piers is cut from the grid (see Figure
\ref{t2d:donau:gridpiers}).
The friction for the lateral boundaries is set in the steering file.
The Nikuradse friction law is used \telkey{LAW OF FRICTION ON LATERAL BOUNDARIES = 5}
with an equivalent sand roughness of 1 cm for concrete
\telkey{ROUGHNESS COEFFICIENT OF BOUNDARIES = 0.01}.
For the third bridge pier of the second bridge in flow direction, the roughness
coefficient is increased to 5.0 m.
This is done in the \telkey{BOUNDARY CONDITIONS FILE} giving the values in the
7th column.
This artificial high value is chosen in order to see significant differences
in the flow field around the third and fourth bridge pier.

\begin{figure} [h!]
\centering
\includegraphicsmaybe{[width=\textwidth]}{../img/friction.png}
\caption{Distribution of the roughness parameters.}
\label{t2d:donau:friction}
\end{figure}

\begin{figure} [h!]
\centering
\includegraphicsmaybe{[width=\textwidth]}{../img/gridpiers.png}
\caption{Grid part with bridge piers.}
\label{t2d:donau:gridpiers}
\end{figure}

\subsection{Numerical parameters}
10 min are simulated with a time step of 0.5 s.
Figure \ref{t2d:donau:fluxes} shows the evolution of the inlet and outlet fluxes
which equals at the end of the simulation time.
The characteristics are used for the advection scheme.
The GMRES solver with a solver accuracy of $10^{-6}$ is applied.
Implicit conditions are set for depth
and semi-implicit conditions for the velocities (with 0.55 value).
The classic discretisation of the equations is applied.

\section{Results}
A comparison along the main channel between the measured water levels and the
simulated ones for both configurations is presented in Figure
\ref{t2d:donau:diff_waterlevels}.
The simulated water levels are in a very good agreement to the measurement.

The increased roughness at the third bridge pier in Figure
\ref{t2d:donau:final_flow} leads to smaller velocities.
The third and the fourth bridge pier both stand in the main channel and would
have a very similar velocity distribution.
Due to the different roughness coefficients, the velocities are smaller
at the third bridge pier (numbering from north to south).

\begin{figure} [h!]
\centering
\includegraphicsmaybe{[width=\textwidth]}{../img/fluxes.png}
\caption{Evolution of inlet and outlet fluxes.}
\label{t2d:donau:fluxes}
\end{figure}


%Differences of velocities simulated with classic discretisation or wave euqation (wave equation -classic ) are shown in Figure \ref{t2d:wesel:diff_waterlevels}.
\begin{figure} [h!]
\centering
\includegraphicsmaybe{[width=\textwidth]}{../img/diff_free_surface.png}
\caption{Comparison of measured and simulated final water levels.}
\label{t2d:donau:diff_waterlevels}
\end{figure}

\begin{figure} [h!]
\centering
\includegraphicsmaybe{[width=\textwidth]}{../img/final_flow.png}
\caption{Scalar velocities around bridge piers.}
\label{t2d:donau:final_flow}
\end{figure}

\section{Conclusion}
The example shows a successful simulation of low water steady state conditions
with tidal flats.
The influence of sidewall friction is shown by the comparison of the flow field
downstream of two bridge piers with different sidewall frictions.
The roughness coefficients can easily be modified in the roughness table because
of the use of roughness zones.


%\section{References}

%Karim, F., Kennedy, J. F.: (1981) \textit {Computer – based predictors for sediment discharge and friction factor of alluvial streams}, Report No. 242, Iowa Institute of Hydraulic Research, University of Iowa, Iowa City, Iowa.\newline

%Rátky, Éva: (2006) \textit{Modellierung von Geschiebezugaben am Niederrhein bei Wesel
%mit einem 2D-tiefengemittelten morphologischen Modell}, Thesis, Universität Karlsruhe (TH), Karlsruhe.\newline

%Savova, S.: (2004) \textit {Datengrundlage für den Testfall Wesel-Xanten}, Report, BAW, Karlsruhe.\newline

