This is an example of the use of a txt file coming from a shapefile to define an area with a bathymetry. The file carre.txt defines a carre and a triangle that is read by user_corfon.f
If we want to keep this kind of file we should create a new variable T2D to stock this filename.
