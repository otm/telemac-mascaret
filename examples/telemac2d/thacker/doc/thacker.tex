\chapter{Wet and dry moving transitions (thacker)}

\section{Purpose}
Thacker test cases illustrate that \telemac{2D} is able to simulate wet/dry
moving transitions (comings and goings) over a variable slope.
%This type of moving-boundary solutions
%\textit{« are of great interest in communities interested in tsunami run-up »} \cite{Delestre2013}.
It is also useful to check the numerical diffusion and mass conservation,
as the water height is periodic in time.

\section{Description}

\subsection{Thacker analytical solutions}

\subsubsection{Rotation of a planar surface in a parabolic bowl}

This Thacker solution consists of a planar surface rotating in a paraboloid
reservoir without friction for which analytical solution exists in the bibliography.
The free surface has a periodic motion and remains planar in time.
%\textit{«To visualize this case, one can think of a glass with some liquid in rotation inside »} \cite{Delestre2013}.\\
The bathymetry of the paraboloid reservoir is defined on a square domain of
width $L$, as follows:
\begin{equation}
  z(r) = -H_0\left(1-\dfrac{r^2}{a^2}\right),
\end{equation}
Where $r(x,y) = \sqrt{(x-L/2)^2 + (y-L/2)^2}$ for each $(x,y)$ in
$[0;L] \times [0;L]$.
The bathymetry is illustrated in Figure \ref{fig:thacker:Mesh}.
For the defined bathymetry, the analytical solution of the studied case is given by:
\begin{equation}
  \left\{
    \begin{array}{ll}
        h(x,y,t) = \dfrac{\eta H_0}{a^2} \left(2 \left( x - \frac{L}{2} \right) \cos(\omega t) + 2 \left( y - \frac{L}{2} \right) \sin(\omega t) - \eta \right) - z(x,y) \\
        u(x,y,t) = - \eta \omega \sin(\omega t) = -U_0 \sin(\omega t)\\
        v(x,y,t) =  \eta \omega \cos(\omega t) = U_0 \cos(\omega t)
    \end{array}
    \right.
    \label{eq:thacker:analytical}
\end{equation}

where $H_0$ is the water depth at the central point of the domain for a zero
elevation, $a$ is the distance from the central
point to the zero elevation of the shoreline and $\eta$ is a parameter.
In our specific case, we choose $L$ = 4~m, $H_0$ = 0.1~m, $a$ = 1~m and $\eta$ = 0.5~m.
The angular frequency $\omega$ can therefore be calculated as
$\omega = \sqrt{2gH_0}/a$ = 1.4~rad.s$^{-1}$,
and the period of the free surface evolution is consequently
$T= \frac{2 \pi}{\omega}$ = 4.49~s.
Furthermore, the amplitude of the flow velocity is $U_0 = \eta \omega$ = 0.70~m.s$^{-1}$.\\

\subsubsection{Oscillation of a radially symmetrical paraboloid}

This second analytical solution of Thacker presented in this test case consists
of an oscillating paraboloid surface radially symmetrical without friction.
The bathymetry is the same as the planar thacker solution and
the analytical solution is given by:
\begin{equation}
  \left\{
    \begin{array}{ll}
      h(r,t) = H_0 \left( \dfrac{\sqrt{1- A^2}}{1-A\cos(\omega t)} - 1 - \dfrac{r^2}{a^2}\left(\dfrac{1-A^2}{(1-A\cos(\omega t))^2} -1 \right)\right) - z(r)\\
      u(x,y,t) = \dfrac{1}{1-A\cos(\omega t)} \left( \dfrac{1}{2} \omega \left( x - \dfrac{L}{2} \right) A \sin(\omega t )\right) \\
      v(x,y,t) = \dfrac{1}{1-A\cos(\omega t)} \left( \dfrac{1}{2} \omega \left( y - \dfrac{L}{2} \right) A \sin(\omega t )\right)
    \end{array}
    \right.
    \label{eq:thacker2:analytical}
\end{equation}

where $H_0$ is the water depth at the central point of the domain for a zero
elevation, $a$ is the distance from the central point to the zero elevation of
the shoreline and $r_0$ is the distance from the central point to the point
where shoreline is initially located and $A=(a^2-r_0^2)/(a^2+r_0^2)$.
In our specific case, we choose $L$ = 4~m, $H_0$ = 0.1~m, $a$ = 1~m and $r_0$
= 0.5~m.
The angular frequency $\omega$ can therefore be calculated as
$\omega = \sqrt{8gH_0}/a$ = 2.80~rad, and the period of the free surface evolution
is consequently $T= \frac{2 \pi}{\omega}$ = 2.24~s.

\subsection{Geometry, mesh and bathymetry}

The computational domain is a 4~m $\times$ 4~m square.
A triangular regular mesh is constructed with 19,800 triangular elements and
10,100 nodes (see Figure~\ref{fig:thacker:Mesh}):

\begin{figure}[H]
\begin{minipage}[t]{0.45\textwidth}
 \centering
 \includegraphicsmaybe{[width=\textwidth]}{../img/t2d_thacker_mesh.png}
\end{minipage}
\begin{minipage}[t]{0.55\textwidth}
 \centering
 \includegraphicsmaybe{[width=\textwidth]}{../img/t2d_thacker_bathy.png}
\end{minipage}
\caption{2D-mesh (left) and bathymetry (right) of the Thacker case.}
\label{fig:thacker:Mesh}
\end{figure}

%\begin{figure}[H]
%  \centering
%  \includegraphicsmaybe{[width=0.5\textwidth]}{../img/t2d_thacker_mesh.png}
%  \includegraphicsmaybe{[width=0.5\textwidth]}{../img/t2d_thacker_bathy.png}
%  \caption{2D-mesh of the Thacker case.}\label{fig:thacker:Mesh}
%\end{figure}
%
%The bathymetry shape is shown in Figure~\ref{fig:thacker:Bathy}
%\begin{figure}[H]
%  \centering
%  \includegraphicsmaybe{[width=0.7\textwidth]}{../img/t2d_thacker_bathy.png}
%  \caption{Paraboloid bathymetry of the Thacker case.}\label{fig:thacker:Bathy}
%\end{figure}

\subsection{Initial conditions}

The water depth and velocities are initialized  at time $t$ = 0.0~s with the
analytical solution (Eq.~\ref{eq:thacker:analytical}) and
(Eq.~\ref{eq:thacker2:analytical}) depending on the test case.

\subsection{Boundary conditions}

The boundaries are solid walls everywhere.
There is no friction on the solid walls.

\subsection{Physical parameters}

%The characteristics of the case are therefore following:
%\begin{itemize}
%  \itemsep0em
%\item Reynolds Number \textbf{$R_e = \dfrac{U_0 \times H_0}{\nu} =  7.0 \times 10^4$} where $\nu$ is the kinematic viscosity of water;
%\item Froude Number \textbf{$F_r = \dfrac{U_0}{\sqrt{g \times H_0}} = 0.71$} where $g$ is the gravity acceleration;
%\item Keulegan-Carpenter Number (also called the period number) \textbf{$K_C = \dfrac{U_0 T}{a} = 3.143$};
%\item Strouhal Number \textbf{$S_t = \dfrac{\omega a}{U_0} = 2$}.
%\end{itemize}

%In the simulations, the Coriolis force and the wind effect are not taken into account.
The molecular viscosity is set as constant and equal to 0~m$^2$/s
(\telkey{VELOCITY DIFFUSIVITY} = 0.) and no friction is set to the bottom.

\subsection{Numerical parameters}

For this test case, duration is set to 10~s and several numerical schemes of
advection for velocities are confronted.
The solver used is the conjugate gradient with an accuracy of 10$^{-8}$.
For finite element schemes the treatment of the linear system is set to wave
equation (\telkey{TREATMENT OF THE LINEAR SYSTEM} = 2).
The parameters specific to each case are summed up in
Table~\ref{tab:thacker:cases}.

\begin{table}[H]
  \resizebox{\textwidth}{!}{
    \begin{tabular}{|c|c|c|c|c|}
      \hline Case & Name & Equations & Advection scheme for velocities & Time-step / Desired Courant number \\
      \hline 1 & CHAR & Wave Eq. FE & Characteristics & 0.005~s / - \\
      \hline 2 & NERD & Wave Eq. FE & Edge-based N-scheme & 0.005~s / - \\
      \hline 3 & ERIA & Wave Eq. FE & ERIA scheme & 0.005~s / - \\
      \hline 4 & LIPS & Wave Eq. FE & LIPS scheme & 0.005~s / - \\
      \hline 5 & HLLC & Saint-Venant FV & HLLC order 1 & - /0.8 \\
      \hline 6 & KIN1 & Saint-Venant FV & Kinetic order 1 & - /0.8 \\
      \hline 7 & WAF & Saint-Venant FV & WAF & - /0.8 \\
      \hline
    \end{tabular}
  }
  \caption{List of the simulation parameters used for the seven cases tested in the Thacker example.}
  \label{tab:thacker:cases}
\end{table}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Results - Rotation of a planar surface in a parabolic bowl}

\subsection{First observation}

The water planar surface moves anticlockwise.
Evolution of water depth and velocity during one rotation is illustrated in
Figures \ref{fig:thacker:WDTime} and \ref{fig:thacker:VelocityTime}.

\begin{figure}[h!]
\begin{minipage}[t]{0.5\textwidth}
 \centering
 \includegraphicsmaybe{[width=0.95\textwidth]}{../img/t2d_thacker_nerd_seq_depth_firstobs0.png}
\end{minipage}
\begin{minipage}[t]{0.5\textwidth}
 \centering
 \includegraphicsmaybe{[width=0.95\textwidth]}{../img/t2d_thacker_nerd_seq_depth_firstobs3.png}
\end{minipage}
\begin{minipage}[t]{0.5\textwidth}
 \centering
 \includegraphicsmaybe{[width=0.95\textwidth]}{../img/t2d_thacker_nerd_seq_depth_firstobs6.png}
\end{minipage}
\begin{minipage}[t]{0.5\textwidth}
 \centering
 \includegraphicsmaybe{[width=0.95\textwidth]}{../img/t2d_thacker_nerd_seq_depth_firstobs9.png}
\end{minipage}
  \caption{Water depth for one rotation of the Thacker case with NERD scheme.}
  \label{fig:thacker:WDTime}
\end{figure}

\begin{figure}[H]
\begin{minipage}[t]{0.5\textwidth}
 \centering
 \includegraphicsmaybe{[width=0.95\textwidth]}{../img/t2d_thacker_nerd_seq_vel_firstobs1.png}
\end{minipage}
\begin{minipage}[t]{0.5\textwidth}
 \centering
 \includegraphicsmaybe{[width=0.95\textwidth]}{../img/t2d_thacker_nerd_seq_vel_firstobs3.png}
\end{minipage}
\begin{minipage}[t]{0.5\textwidth}
 \centering
 \includegraphicsmaybe{[width=0.95\textwidth]}{../img/t2d_thacker_nerd_seq_vel_firstobs6.png}
\end{minipage}
\begin{minipage}[t]{0.5\textwidth}
 \centering
 \includegraphicsmaybe{[width=0.95\textwidth]}{../img/t2d_thacker_nerd_seq_vel_firstobs9.png}
\end{minipage}
  \caption{Velocity norm and vector field of the Thacker case with NERD scheme.}
  \label{fig:thacker:VelocityTime}
\end{figure}

\subsection{Computation time}

Simulation times for each of these cases with sequential and parallel runs
(using 4 processors) are shown in Figure ~\ref{fig:thacker:cputime}
\footnote{Keep in mind that these times
are specific to the validation run and the type of processors that were used for this purpose.}.
The WAF case is run only in scalar mode because the parallel version is not
implemented yet.

\begin{figure}[h!]
  \centering
  \includegraphicsmaybe{[width=0.75\textwidth]}{../img/t2d_thacker_cpu_times.png}
  \caption{CPU times.}
  \label{fig:thacker:cputime}
\end{figure}

In general, the finite-volume schemes are much faster than the finite-element
ones with the chosen parameters.
This is to be put in perspective with the better accuracy of the NERD, ERIA
and LIPS
schemes, which actually perform sub-iterations during each time-step.
This performace comparison is thus biased and should be interpreted carefully.
In the example we did not seek the best accuracy for each scheme,
which would have provided us with a different performance result.
The NERD scheme is faster than the ERIA and LIPS schemes, which is expected
since ERIA and LIPS are
a more elaborate version of NERD that provides better accuracy.

%Further more, the sequential and parallel simulations results are compared for cases in the Table~\ref{tab:thacker:SeqPar}.
%That makes it possible to quantify the impact of a different order of operations together with communications between processors.
%\begin{table}[H]
%  \centering
%  \begin{tabular}{|l|}
%    \hline \\
%    \hline  CHAR \\
%    NERD \\
%    ERIA \\
%    KIN1 \\
%    HLLC \\
%    \hline
%  \end{tabular}
%  \begin{tabular}{|c|c|c|}
%    \hline $||\epsilon||_{L^1}$ & $||\epsilon||_{L^2}$ & $||\epsilon||_{L^{\infty}}$ \\
%    \hline
%    \InputIfFileExists{../SeqPar.txt}{}{}\\
%    \hline
%  \end{tabular}
%  \caption{Sequentiel VS Parallel - relative errors on water depth values on the Thacker case.}
%    \label{tab:thacker:SeqPar}
%\end{table}

\subsection{Comparison of schemes}

In order to evaluate the accuracy of the tested advection schemes, the results
are compared to the analytical solution~(\ref{eq:thacker:analytical}).
A first visual comparison in time can be found in Figure~\ref{fig:thacker:SLTime}.
The analytical solution is represented by the light red coloured area.

\begin{figure}[H]
\begin{minipage}[t]{0.5\textwidth}
 \centering
 \includegraphicsmaybe{[width=0.95\textwidth]}{../img/t2d_thacker_schemes_comparison_0.png}
\end{minipage}
\begin{minipage}[t]{0.5\textwidth}
 \centering
 \includegraphicsmaybe{[width=0.95\textwidth]}{../img/t2d_thacker_schemes_comparison_3.png}
\end{minipage}
\begin{minipage}[t]{0.5\textwidth}
 \centering
 \includegraphicsmaybe{[width=0.95\textwidth]}{../img/t2d_thacker_schemes_comparison_6.png}
\end{minipage}
\begin{minipage}[t]{0.5\textwidth}
 \centering
 \includegraphicsmaybe{[width=0.95\textwidth]}{../img/t2d_thacker_schemes_comparison_9.png}
\end{minipage}
  \caption{Comparison of free surface with analytical values on the Thacker case.}
  \label{fig:thacker:SLTime}
\end{figure}

In Figures \ref{fig:thacker:SLU_comparison} and \ref{fig:thacker:SLU_comparison2}
some comparisons of water depth and velocity field are presented.
Elements where water depth is lower than 1~mm have been masked.

\begin{figure}[H]
\begin{minipage}[t]{0.5\textwidth}
 \centering
 \includegraphicsmaybe{[width=0.95\textwidth]}{../img/t2d_thacker_char_seq_depth_9.png}
\end{minipage}
\begin{minipage}[t]{0.5\textwidth}
 \centering
 \includegraphicsmaybe{[width=0.95\textwidth]}{../img/t2d_thacker_char_seq_velocity_9.png}
\end{minipage}
\begin{minipage}[t]{0.5\textwidth}
 \centering
 \includegraphicsmaybe{[width=0.95\textwidth]}{../img/t2d_thacker_nerd_seq_depth_9.png}
\end{minipage}
\begin{minipage}[t]{0.5\textwidth}
 \centering
 \includegraphicsmaybe{[width=0.95\textwidth]}{../img/t2d_thacker_nerd_seq_velocity_9.png}
\end{minipage}
\begin{minipage}[t]{0.5\textwidth}
 \centering
 \includegraphicsmaybe{[width=0.95\textwidth]}{../img/t2d_thacker_eria_seq_depth_9.png}
\end{minipage}
\begin{minipage}[t]{0.5\textwidth}
 \centering
 \includegraphicsmaybe{[width=0.95\textwidth]}{../img/t2d_thacker_eria_seq_velocity_9.png}
\end{minipage}
\begin{minipage}[t]{0.5\textwidth}
 \centering
 \includegraphicsmaybe{[width=0.95\textwidth]}{../img/t2d_thacker_lips_seq_depth_9.png}
\end{minipage}
\begin{minipage}[t]{0.5\textwidth}
 \centering
 \includegraphicsmaybe{[width=0.95\textwidth]}{../img/t2d_thacker_lips_seq_velocity_9.png}
\end{minipage}
  \caption{Comparison of water depth and velocity after one rotation.}
  \label{fig:thacker:SLU_comparison}
\end{figure}

\begin{figure}[H]
\begin{minipage}[t]{0.5\textwidth}
 \centering
 \includegraphicsmaybe{[width=0.95\textwidth]}{../img/t2d_thacker_kin1_seq_depth_9.png}
\end{minipage}
\begin{minipage}[t]{0.5\textwidth}
 \centering
 \includegraphicsmaybe{[width=0.95\textwidth]}{../img/t2d_thacker_kin1_seq_velocity_9.png}
\end{minipage}
\begin{minipage}[t]{0.5\textwidth}
 \centering
 \includegraphicsmaybe{[width=0.95\textwidth]}{../img/t2d_thacker_hllc1_seq_depth_9.png}
\end{minipage}
\begin{minipage}[t]{0.5\textwidth}
 \centering
 \includegraphicsmaybe{[width=0.95\textwidth]}{../img/t2d_thacker_hllc1_seq_velocity_9.png}
\end{minipage}
  \caption{Comparison of water depth and velocity after one rotation.}
  \label{fig:thacker:SLU_comparison2}
\end{figure}

\subsection{Accuracy}

For a more quantitative comparison of schemes, the $L^1$, $L^2$ and $L^\infty$
error norms of the water depth and velocity are calculated at each time step for
each scheme.
$L^2$ errors time series and time integrated $L^1$, $L^2$ and $L^\infty$ errors
are presented in Figures \ref{fig:thacker:ErrNumH}, \ref{fig:thacker:ErrNumU}
and \ref{fig:thacker:ErrNumV} for $H$, $U$ and $V$ respectively.

\begin{figure}[H]
\begin{minipage}[t]{0.45\textwidth}
 \centering
 \includegraphicsmaybe{[width=\textwidth]}{../img/t2d_thacker_error_L2_H.png}
\end{minipage}
\begin{minipage}[t]{0.55\textwidth}
 \centering
 \includegraphicsmaybe{[width=\textwidth]}{../img/t2d_thacker_errors_timeintegral_H.png}
\end{minipage}
  \caption{Error on $H$: timeseries (left) and integrated over time (right).}
  \label{fig:thacker:ErrNumH}
\end{figure}

\begin{figure}[H]
\begin{minipage}[t]{0.45\textwidth}
 \centering
 \includegraphicsmaybe{[width=\textwidth]}{../img/t2d_thacker_error_L2_U.png}
\end{minipage}
\begin{minipage}[t]{0.55\textwidth}
 \centering
 \includegraphicsmaybe{[width=\textwidth]}{../img/t2d_thacker_errors_timeintegral_U.png}
\end{minipage}
  \caption{Error on $U$: timeseries (left) and integrated over time (right).}
  \label{fig:thacker:ErrNumU}
\end{figure}

\begin{figure}[H]
\begin{minipage}[t]{0.45\textwidth}
 \centering
 \includegraphicsmaybe{[width=\textwidth]}{../img/t2d_thacker_error_L2_V.png}
\end{minipage}
\begin{minipage}[t]{0.55\textwidth}
 \centering
 \includegraphicsmaybe{[width=\textwidth]}{../img/t2d_thacker_errors_timeintegral_V.png}
\end{minipage}
  \caption{Error on $V$: timeseries (left) and integrated over time (right).}
  \label{fig:thacker:ErrNumV}
\end{figure}

CHAR scheme is the least accurate for this example with the chosen time step size.
Numerical diffusion is also noticeable for WAF, HLLC and KIN1 schemes, and a
phase difference appears between their results and the analytical solution.
NERD, ERIA and LIPS are in good agreement with the analytical solution, keeping
in mind that to reach that accuracy they actually perform sub-iterations.

\subsection{Positivity of water depth}

The minimum value of the water depth are checked after one rotation.
Results are shown in Figure \ref{t2d:thacker:minmax}.
No negative values are recorded, which shows that the positivity is fulfilled.
In the case of finite volume schemes, positivity is ensured without additionnal
treatment.
With finite element schemes the positivity is ensured with a treatment of
negative depths during simulation (\telkey{TREATMENT OF NEGATIVE DEPTHS} = 2 for
characteristics, NERD and LIPS and 3 for ERIA scheme).

\begin{figure}[H]
\centering
\includegraphicsmaybe{[width=0.9\textwidth]}{../img/t2d_thacker_minT.png}
\caption{Minimum values of water depths after one rotation.}
\label{t2d:thacker:minmax}
\end{figure}

\subsection{Mass balance}

Mass conservation can be checked by calculating the mass in the domain during
time.
The lost mass is calculated as $M_{initial} - M_{final}$.
The evolution of mass for each of the schemes is shown in
Figure~\ref{fig:thacker:VoLTime}.
All the schemes provide a satisfying mass conservation.

\begin{figure}[H]
\centering
  \includegraphicsmaybe{[width=0.9\textwidth]}{../img/t2d_thacker_mass_balance.png}
  \caption{Mass loss for the tested schemes on the Thacker case.}
\label{fig:thacker:VoLTime}
\end{figure}

\subsection{Energy balance}

In this section, the conservation of the mean energy is checked.
In fact, for the Saint-Venant equations, the quantity
$h \frac{||U||^2}{2} + g \frac{h^2}{2}$, called mean or integrated energy, is
conserved, where $||U||$ is the Saint-Venant depth averaged velocity magnitude.
%\begin{equation}
%  ||U||^2 = \left( \left(\frac{1}{h} \int_0^h u~dz\right) \overrightarrow{e_x} \right)^2 + \left( \left(\frac{1}{h} \int_0^h v~dz\right)  \overrightarrow{e_y} \right)^2
%\end{equation}
%Where $u$ and $v$ are the horizontal components of the 3D velocity. \\
The following quantities are studied:
\begin{itemize}
\item Integrated potential energy
  \textbf{$E_p =\int\int_{\Omega_{xy}}\rho_{water} g \frac{h^2}{2} dxdy$} where
  $\Omega_{xy}$ is the 2D domain of simulation: Figure~\ref{fig:thacker:Ep},
\item Integrated kinetic energy
  \textbf{$E_c =\int\int_{\Omega_{xy}} \rho_{water} h \frac{||U||^2}{2} dxdy$}:
  Figure~\ref{fig:thacker:Ec},
\item Integrated mechanical energy \textbf{$E_m = E_p + E_c$}:
  Figure~\ref{fig:thacker:Em}.
\end{itemize}

\begin{figure}[H]
\centering
  \includegraphicsmaybe{[width=0.8\textwidth]}{../img/t2d_thacker_potential_energy.png}
  \caption{Evolution of kinetic energy for the tested schemes on the Thacker case.}
\label{fig:thacker:Ep}
\end{figure}

\begin{figure}[H]
\centering
  \includegraphicsmaybe{[width=0.8\textwidth]}{../img/t2d_thacker_kinetic_energy.png}
  \caption{Evolution of kinetic energy for the tested schemes on the Thacker case.}
\label{fig:thacker:Ec}
\end{figure}

\begin{figure}[H]
\centering
  \includegraphicsmaybe{[width=0.8\textwidth]}{../img/t2d_thacker_total_energy.png}
  \caption{Evolution of mechanical energy for the tested schemes on the Thacker case.}
\label{fig:thacker:Em}
\end{figure}

The LIPS, ERIA and NERD schemes seem to provide the best results.
In fact, the mean energy (also called integrated, total, mechanical) is almost
stable in time.
But overall, all schemes included, no energy creation is observed and the mean
energy in time stays lower than the initial one during the whole simulation.

\subsection{Convergence}

To assess the accuracy of the schemes, computation of error on one mesh is not
sufficient.
In this section a mesh convergence is carried out for each numerical scheme.
From a starting mesh with 121 nodes and 200 elements we divide by 4 each
triangles recursively to generate new meshes.
The first and last meshes used in the convergence study are presented in Figure
\ref{t2d:cone:meshes}.

\begin{figure}[h!]
\begin{minipage}[t]{0.5\textwidth}
 \centering
 \includegraphicsmaybe{[width=0.8\textwidth]}{../img/mesh_0.png}
\end{minipage}
\begin{minipage}[t]{0.5\textwidth}
 \centering
 \includegraphicsmaybe{[width=0.8\textwidth]}{../img/mesh_4.png}
\end{minipage}
 \caption{First and last meshes used in Thacker mesh convergence.}
 \label{t2d:cone:meshes}
\end{figure}

Final time is set to $t_f = T/2$.
With decreasing space step we adjust time step to ensure a constant CFL for each
mesh increment.
In this section two additional numerical schemes are tested
i.e. edge by edge implementation of Leo Potsma scheme (noted LEOP, scheme number 13)
and second order in space kinetic scheme (noted KIN2).

\begin{figure}[H]
\centering
  \includegraphicsmaybe{[width=0.7\textwidth]}{../img/t2d_thacker_errors_tf_finemesh_H_L2_allsc.png}
  \caption{$H$ convergence in $L^2$ norm.}
\label{fig:thacker:ErrNumH_convergence}
\end{figure}

\begin{figure}[H]
\centering
  \includegraphicsmaybe{[width=0.7\textwidth]}{../img/t2d_thacker_errors_timeintegrals_H_mesh4.png}
  \caption{$H$ time integrated errors on finest mesh.}
\label{fig:thacker:ErrNumH_convergence_mesh3}
\end{figure}

Convergence slopes of time integrated error of $H$ in $L^2$ norm are compared in
Figure \ref{fig:thacker:ErrNumH_convergence}.
Time integrated errors on $H$ on the finest mesh are presented in Figure
\ref{fig:thacker:ErrNumH_convergence_mesh3}.
Convergence slopes of error in $L^1$, $L^2$ and $L^\infty$ norm at final time
are plotted for each numerical scheme in Figure \ref{t2d:thacker:mesh_convergence_H}.

\begin{figure}[H]
\begin{minipage}[t]{0.50\textwidth}
 \centering
 \includegraphicsmaybe{[width=\textwidth]}{../img/t2d_thacker_CHARAC_errors_tf_finemesh_H.png}
\end{minipage}
\newline
\begin{minipage}[t]{0.50\textwidth}
 \centering
 \includegraphicsmaybe{[width=\textwidth]}{../img/t2d_thacker_LEOP_errors_tf_finemesh_H.png}
\end{minipage}
\begin{minipage}[t]{0.50\textwidth}
 \centering
 \includegraphicsmaybe{[width=\textwidth]}{../img/t2d_thacker_NERD_errors_tf_finemesh_H.png}
\end{minipage}
\begin{minipage}[t]{0.50\textwidth}
 \centering
 \includegraphicsmaybe{[width=\textwidth]}{../img/t2d_thacker_ERIA_errors_tf_finemesh_H.png}
\end{minipage}
\begin{minipage}[t]{0.50\textwidth}
 \centering
 \includegraphicsmaybe{[width=\textwidth]}{../img/t2d_thacker_LIPS_errors_tf_finemesh_H.png}
\end{minipage}
\begin{minipage}[t]{0.50\textwidth}
 \centering
 \includegraphicsmaybe{[width=\textwidth]}{../img/t2d_thacker_KIN1_errors_tf_finemesh_H.png}
\end{minipage}
\begin{minipage}[t]{0.50\textwidth}
 \centering
 \includegraphicsmaybe{[width=\textwidth]}{../img/t2d_thacker_KIN2_errors_tf_finemesh_H.png}
\end{minipage}
\begin{minipage}[t]{0.50\textwidth}
 \centering
 \includegraphicsmaybe{[width=\textwidth]}{../img/t2d_thacker_HLLC1_errors_tf_finemesh_H.png}
\end{minipage}
\begin{minipage}[t]{0.50\textwidth}
 \centering
 \includegraphicsmaybe{[width=\textwidth]}{../img/t2d_thacker_HLLC2_errors_tf_finemesh_H.png}
\end{minipage}
  \caption{$H$ convergence with different numerical schemes.}
 \label{t2d:thacker:mesh_convergence_H}
\end{figure}

Finite volume schemes converge with a order of convergence near one for
first order kinetic, HLLC and WAF and slightly steeper than one for second order
kinetic scheme.
For finite element schemes, CHAR scheme does not convergence whereas LEOP, NERD
ERIA and LIPS schemes exibit a slope of convergence comprised between one and two.

%\newpage
%\begin{figure}[H]
%\begin{minipage}[t]{0.50\textwidth}
% \centering
% \includegraphicsmaybe{[width=\textwidth]}{../img/t2d_thacker_CHARAC_errors_tf_finemesh_U.png}
%\end{minipage}
%\begin{minipage}[t]{0.50\textwidth}
% \centering
% \includegraphicsmaybe{[width=\textwidth]}{../img/t2d_thacker_LEOP_errors_tf_finemesh_U.png}
%\end{minipage}
%\begin{minipage}[t]{0.50\textwidth}
% \centering
% \includegraphicsmaybe{[width=\textwidth]}{../img/t2d_thacker_NERD_errors_tf_finemesh_U.png}
%\end{minipage}
%\begin{minipage}[t]{0.50\textwidth}
% \centering
% \includegraphicsmaybe{[width=\textwidth]}{../img/t2d_thacker_ERIA_errors_tf_finemesh_U.png}
%\end{minipage}
%\begin{minipage}[t]{0.50\textwidth}
% \centering
% \includegraphicsmaybe{[width=\textwidth]}{../img/t2d_thacker_KIN1_errors_tf_finemesh_U.png}
%\end{minipage}
%\begin{minipage}[t]{0.50\textwidth}
% \centering
% \includegraphicsmaybe{[width=\textwidth]}{../img/t2d_thacker_KIN2_errors_tf_finemesh_U.png}
%\end{minipage}
%\begin{minipage}[t]{0.50\textwidth}
% \centering
% \includegraphicsmaybe{[width=\textwidth]}{../img/t2d_thacker_HLLC_errors_tf_finemesh_U.png}
%\end{minipage}
%\begin{minipage}[t]{0.50\textwidth}
% \centering
% \includegraphicsmaybe{[width=\textwidth]}{../img/t2d_thacker_WAF_errors_tf_finemesh_U.png}
%\end{minipage}
%  \caption{U convergence with different numerical schemes.}
% \label{t2d:thacker:mesh_convergence_U}
%\end{figure}
%
%\newpage
%\begin{figure}[H]
%\begin{minipage}[t]{0.50\textwidth}
% \centering
% \includegraphicsmaybe{[width=\textwidth]}{../img/t2d_thacker_CHARAC_errors_tf_finemesh_V.png}
%\end{minipage}
%\begin{minipage}[t]{0.50\textwidth}
% \centering
% \includegraphicsmaybe{[width=\textwidth]}{../img/t2d_thacker_LEOP_errors_tf_finemesh_V.png}
%\end{minipage}
%\begin{minipage}[t]{0.50\textwidth}
% \centering
% \includegraphicsmaybe{[width=\textwidth]}{../img/t2d_thacker_NERD_errors_tf_finemesh_V.png}
%\end{minipage}
%\begin{minipage}[t]{0.50\textwidth}
% \centering
% \includegraphicsmaybe{[width=\textwidth]}{../img/t2d_thacker_ERIA_errors_tf_finemesh_V.png}
%\end{minipage}
%\begin{minipage}[t]{0.50\textwidth}
% \centering
% \includegraphicsmaybe{[width=\textwidth]}{../img/t2d_thacker_KIN1_errors_tf_finemesh_V.png}
%\end{minipage}
%\begin{minipage}[t]{0.50\textwidth}
% \centering
% \includegraphicsmaybe{[width=\textwidth]}{../img/t2d_thacker_KIN2_errors_tf_finemesh_V.png}
%\end{minipage}
%\begin{minipage}[t]{0.50\textwidth}
% \centering
% \includegraphicsmaybe{[width=\textwidth]}{../img/t2d_thacker_HLLC_errors_tf_finemesh_V.png}
%\end{minipage}
%\begin{minipage}[t]{0.50\textwidth}
% \centering
% \includegraphicsmaybe{[width=\textwidth]}{../img/t2d_thacker_WAF_errors_tf_finemesh_V.png}
%\end{minipage}
%  \caption{V convergence with different numerical schemes.}
% \label{t2d:thacker:mesh_convergence_V}
%\end{figure}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\newpage
\section{Results - Oscillation of a radially symmetrical paraboloid}

%\subsection{First observation}
%
%Evolution of water depth and velocity during one oscillation
%is illustrated in Figures \ref{fig:thacker2:WDTime} and \ref{fig:thacker2:VelocityTime}).
%
%\begin{figure}[h!]
%\begin{minipage}[t]{0.5\textwidth}
% \centering
% \includegraphicsmaybe{[width=\textwidth]}{../img/t2d_thacker2_nerd_seq_depth_firstobs0.png}
%\end{minipage}
%\begin{minipage}[t]{0.5\textwidth}
% \centering
% \includegraphicsmaybe{[width=\textwidth]}{../img/t2d_thacker2_nerd_seq_depth_firstobs3.png}
%\end{minipage}
%\begin{minipage}[t]{0.5\textwidth}
% \centering
% \includegraphicsmaybe{[width=\textwidth]}{../img/t2d_thacker2_nerd_seq_depth_firstobs6.png}
%\end{minipage}
%\begin{minipage}[t]{0.5\textwidth}
% \centering
% \includegraphicsmaybe{[width=\textwidth]}{../img/t2d_thacker2_nerd_seq_depth_firstobs9.png}
%\end{minipage}
%  \caption{Water depth for one rotation of the Thacker case with NERD scheme.}
%  \label{fig:thacker2:WDTime}
%\end{figure}
%
%\begin{figure}[H]
%\begin{minipage}[t]{0.5\textwidth}
% \centering
% \includegraphicsmaybe{[width=\textwidth]}{../img/t2d_thacker2_nerd_seq_vel_firstobs0.png}
%\end{minipage}
%\begin{minipage}[t]{0.5\textwidth}
% \centering
% \includegraphicsmaybe{[width=\textwidth]}{../img/t2d_thacker2_nerd_seq_vel_firstobs3.png}
%\end{minipage}
%\begin{minipage}[t]{0.5\textwidth}
% \centering
% \includegraphicsmaybe{[width=\textwidth]}{../img/t2d_thacker2_nerd_seq_vel_firstobs6.png}
%\end{minipage}
%\begin{minipage}[t]{0.5\textwidth}
% \centering
% \includegraphicsmaybe{[width=\textwidth]}{../img/t2d_thacker2_nerd_seq_vel_firstobs9.png}
%\end{minipage}
%  \caption{Velocity norm and vector field of the Thacker case with NERD scheme.}
%  \label{fig:thacker2:VelocityTime}
%\end{figure}

\subsection{Computation time}

Simulation times for each of these cases with sequential and parallel runs
(using 4 processors) are shown in Figure~\ref{fig:thacker2:cputime}.

\begin{figure}[h!]
  \centering
  \includegraphicsmaybe{[width=0.7\textwidth]}{../img/t2d_thacker2_cpu_times.png}
  \caption{CPU times.}
  \label{fig:thacker2:cputime}
\end{figure}

\subsection{Comparison of schemes}

In order to evaluate the accuracy of the tested advection schemes, the results
are compared to the analytical solution~(\ref{eq:thacker2:analytical}).
A first visual comparison in time can be found in Figure~\ref{fig:thacker2:SLTime}.
The analytical solution is represented by the light blue coloured area.
In Figures \ref{fig:thacker2:SLU_comparison} and \ref{fig:thacker2:SLU_comparison2}
some comparisons of water depth and velocity field are presented.
Elements where water depth is inferior to 1~mm have been masked.

\begin{figure}[H]
\begin{minipage}[t]{0.5\textwidth}
 \centering
 \includegraphicsmaybe{[width=0.9\textwidth]}{../img/t2d_thacker2_schemes_comparison_0.png}
\end{minipage}
\begin{minipage}[t]{0.5\textwidth}
 \centering
 \includegraphicsmaybe{[width=0.9\textwidth]}{../img/t2d_thacker2_schemes_comparison_3.png}
\end{minipage}
\begin{minipage}[t]{0.5\textwidth}
 \centering
 \includegraphicsmaybe{[width=0.9\textwidth]}{../img/t2d_thacker2_schemes_comparison_6.png}
\end{minipage}
\begin{minipage}[t]{0.5\textwidth}
 \centering
 \includegraphicsmaybe{[width=0.9\textwidth]}{../img/t2d_thacker2_schemes_comparison_9.png}
\end{minipage}
  \caption{Comparison of free surface with analytical values on the Thacker2 case.}
  \label{fig:thacker2:SLTime}
\end{figure}

\begin{figure}[H]
\begin{minipage}[t]{0.5\textwidth}
 \centering
 \includegraphicsmaybe{[width=0.95\textwidth]}{../img/t2d_thacker2_char_seq_depth_9.png}
\end{minipage}
\begin{minipage}[t]{0.5\textwidth}
 \centering
 \includegraphicsmaybe{[width=0.95\textwidth]}{../img/t2d_thacker2_char_seq_velocity_9.png}
\end{minipage}
\begin{minipage}[t]{0.5\textwidth}
 \centering
 \includegraphicsmaybe{[width=0.95\textwidth]}{../img/t2d_thacker2_nerd_seq_depth_9.png}
\end{minipage}
\begin{minipage}[t]{0.5\textwidth}
 \centering
 \includegraphicsmaybe{[width=0.95\textwidth]}{../img/t2d_thacker2_nerd_seq_velocity_9.png}
\end{minipage}
\begin{minipage}[t]{0.5\textwidth}
 \centering
 \includegraphicsmaybe{[width=0.95\textwidth]}{../img/t2d_thacker2_eria_seq_depth_9.png}
\end{minipage}
\begin{minipage}[t]{0.5\textwidth}
 \centering
 \includegraphicsmaybe{[width=0.95\textwidth]}{../img/t2d_thacker2_eria_seq_velocity_9.png}
\end{minipage}
\begin{minipage}[t]{0.5\textwidth}
 \centering
 \includegraphicsmaybe{[width=0.95\textwidth]}{../img/t2d_thacker2_lips_seq_depth_9.png}
\end{minipage}
\begin{minipage}[t]{0.5\textwidth}
 \centering
 \includegraphicsmaybe{[width=0.95\textwidth]}{../img/t2d_thacker2_lips_seq_velocity_9.png}
\end{minipage}
  \caption{Comparison of water depth and velocity after one oscillation.}
  \label{fig:thacker2:SLU_comparison}
\end{figure}

\begin{figure}[H]
\begin{minipage}[t]{0.5\textwidth}
 \centering
 \includegraphicsmaybe{[width=0.95\textwidth]}{../img/t2d_thacker2_kin1_seq_depth_9.png}
\end{minipage}
\begin{minipage}[t]{0.5\textwidth}
 \centering
 \includegraphicsmaybe{[width=0.95\textwidth]}{../img/t2d_thacker2_kin1_seq_velocity_9.png}
\end{minipage}
\begin{minipage}[t]{0.5\textwidth}
 \centering
 \includegraphicsmaybe{[width=0.95\textwidth]}{../img/t2d_thacker2_hllc1_seq_depth_9.png}
\end{minipage}
\begin{minipage}[t]{0.5\textwidth}
 \centering
 \includegraphicsmaybe{[width=0.95\textwidth]}{../img/t2d_thacker2_hllc1_seq_velocity_9.png}
\end{minipage}
%\begin{minipage}[t]{0.5\textwidth}
% \centering
% \includegraphicsmaybe{[width=0.95\textwidth]}{../img/t2d_thacker2_waf_seq_depth_9.png}
%\end{minipage}
%\begin{minipage}[t]{0.5\textwidth}
% \centering
% \includegraphicsmaybe{[width=0.95\textwidth]}{../img/t2d_thacker2_waf_seq_velocity_9.png}
%\end{minipage}
  \caption{Comparison of water depth and velocity after one oscillation.}
  \label{fig:thacker2:SLU_comparison2}
\end{figure}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%ùù

\section{Conclusion}
\telemac{2D} is capable of simulating the wet/dry moving transitions with good
accuracy and conservation of mass, energy and positivity of the water depth.
