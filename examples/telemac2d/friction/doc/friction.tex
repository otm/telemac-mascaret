\chapter{Flow in a channel with friction (friction)}

\section{Description}

In this example, a validation of the friction laws implemented in \telemac{2D}
is proposed. We compare the steady state water depth and elevation to
the analytical solution which corresponds to an equilibrium between the bottom
momentum source term
and the friction source term in the Saint-Venant equations, i.e.
\begin{equation}
gh \dfrac{\partial z_b}{\partial x}  = h F_b = - \dfrac{C_f}{2} u^2
\end{equation}
where 
$g$ is the gravitational acceleration,
$z_b$ is the bottom elevation,
$C_f$ is the friction coefficient (which depends on the friction laws),
and $h$, $u$ are the steady state water depth and velocity in the flume.
The configuration is a straight channel $10$km long and $400$m wide
with a mild slope set at $\partial_x z_b=-10^{-4}$, see Figure \ref{t2d:Friction:fig:bottom}.
The water depth is set at $3$m and lineic discharge at $q=2$m$^2$/s.
For each friction laws, the specific coefficient of the laws are adjusted to correspond to
a friction coefficient $C_f=0.0132435$ with a precision of $10^{-12}$.
At steady state, the expected solution is a constant water depth of $3$m along the channel.

\begin{figure}[!htbp]
 \centering
 \includegraphicsmaybe{[width=0.8\textwidth]}{../img/Bottom.png}
 \caption{Bottom elevation.}
 \label{t2d:Friction:fig:bottom}
\end{figure}

\subsection{Initial and boundary conditions}

The computation is initialised with a constant water depth equal to $3$m
and a velocity which corresponds to a constant lineic discharge of $q=2$m$^2$/s.

The boundary conditions are:
\begin{itemize}
\item For the solid walls, a slip condition on channel banks,
\item Upstream a flowrate equal to $800$m$^3$/s is prescribed,
\item Downstream the water level is suggested to be equal to $3$m.
\end{itemize}

Boundary conditions types can be seen in Figure \ref{t2d:Friction:fig:BC}.

\begin{figure}[!htbp]
 \centering
 \includegraphicsmaybe{[width=0.8\textwidth]}{../img/BC.png}
 \caption{Boundary conditions types.}
 \label{t2d:Friction:fig:BC}
\end{figure}

\subsection{Mesh and numerical parameters}

The mesh is made of 3797 triangular elements (2020 nodes),  
The simulated time is $4$h.
The problem is solved using the two kernels of \telemac{2D} i.e.
the finite elements kernel (with the NERD scheme on $u, v$)
and the finite volume kernel (with the HLLC scheme and the
hydrostatic reconstruction).
The main numerical options are summed up in table \ref{tab:friction:cases}.

\begin{table}[H]
  \resizebox{\textwidth}{!}{
    \begin{tabular}{|c|c|c|c|}
      \hline Case & Equations & Advection scheme for velocities & Time-step / Desired Courant number \\
      \hline FE & Wave Eq.     & NERD (Edge-based N-scheme) & $1$s / - \\
      \hline FV & Saint-Venant & HLLC                       & - / $0.9$ \\
      \hline
    \end{tabular}
  }
  \caption{Numerical parameters.}
  \label{tab:friction:cases}
\end{table}


\subsection{Physical parameters}

No diffusion is chosen for this computation (constant horizontal viscosity for velocity equal to 0.~m$^2$/s).
Different
All friction laws of \telemac{2D} are tested and the corresponding coefficients are summed up in table \ref{tab:friction:laws}.

\begin{table}[H]
  \resizebox{\textwidth}{!}{%
    \begin{tabular}{|c|c|c|c|}
      \hline Name & LAW OF BOTTOM FRICTION & FRICTION COEFFICIENT & Description of the coefficient \\
      \hline Haaland         & 1 & $0.2407943514$ & Roughness height: $k_s$ (m) \\
      \hline Chézy           & 2 & $38.490017947$ & Chézy coefficient: $C_c$ (m$^{1/2}$/s) \\
      \hline Strickler       & 3 & $32.049990450$ & Strickler coefficient: $K_s$ (m$^{1/3}$/s) \\
      \hline Manning         & 4 & $0.0312012572$ & Manning coefficient: $n$   (s/m$^{1/3}$) \\
      \hline Nikuradse       & 5 & $0.2427333307$   & Roughness height: $k_s$ (m) \\
      \hline Colebrook-White & 6 & $0.2419536811$ & Roughness height: $k_s$ (m) \\
      \hline
    \end{tabular}
  }
  \caption{List of the friction laws tested and associated coefficient.}
  \label{tab:friction:laws}
\end{table}

\section{Results}

Results obtained with the HLLC scheme
are presented in Figure \ref{t2d:Friction:hllc}
for the water depth and the free surface elevation respectively.
Results obtained with the NERD scheme
are presented in Figure \ref{t2d:Friction:nerd}.
Comparison to the analytical solution shows that the numercial model 
is precise up to several mm in terms of water depth.
Similar results are obtained with all friction laws. 
This shows that the error comes from the discretization error and not by the choice of the friction model,
and could be improved by refining the mesh.

\begin{figure}[H]
\centering
\includegraphicsmaybe{[width=0.85\textwidth]}{../img/final_depth_comparison_fv.png}
\includegraphicsmaybe{[width=0.85\textwidth]}{../img/final_elevation_comparison_fv.png}
\caption{Water depth (top) and free surface elevation (bottom) at final time with the HLLC scheme.}
\label{t2d:Friction:hllc}
\end{figure}

\begin{figure}[H]
\centering
\includegraphicsmaybe{[width=0.85\textwidth]}{../img/final_depth_comparison_fe.png}
\includegraphicsmaybe{[width=0.85\textwidth]}{../img/final_elevation_comparison_fe.png}
\caption{Water depth (top) and free surface elevation (bottom) at final time with the NERD scheme.}
\label{t2d:Friction:nerd}
\end{figure}

\section{Conclusions}

This example shows that \telemac{2D} is able to simulate a flow in a channel
with friction, and that all friction laws are correctly implemented.
The comparison shows that all friction laws yields similar results
compared to the analytical solution.

