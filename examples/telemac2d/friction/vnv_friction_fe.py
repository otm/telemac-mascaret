
"""
Validation script for friction
"""
from vvytel.vnv_study import AbstractVnvStudy
from execution.telemac_cas import TelemacCas, get_dico
from data_manip.extraction.telemac_file import TelemacFile

class VnvStudy(AbstractVnvStudy):
    """
    Class for validation
    """
    def _init(self):
        """
        Defines the general parameter
        """
        self.rank = 2
        self.tags = ['telemac2d']

    def _pre(self):
        """
        Defining the studies
        
        'LAW OF BOTTOM FRICTION':
            '0="PAS DE FROTTEMENT"';
            '1="HAALAND"';
            '2="CHEZY"';
            '3="STRICKLER"';
            '4="MANNING"';
            '5="NIKURADSE"';
            '6="COLEBROOK-WHITE"'

        Remark:
            Friction law coefficient (Ks and ks) are set so that
            Cf =  2.*g*h*S/(u**2) = 0.0132435 is match at 1e-12 precision.
            S is the slope set at 1e-4.
        """
        #======================================================================
        # Haaland law :
        # 
        # seq
        cas = TelemacCas('t2d_friction_fe.cas', get_dico('telemac2d'))
        cas.set('LAW OF BOTTOM FRICTION', 1)
        cas.set('FRICTION COEFFICIENT', 0.24079435142) # roughness height (m)
        self.add_study('haa_seq', 'telemac2d', 't2d_friction_fe.cas', cas=cas)
        # par
        cas = TelemacCas('t2d_friction_fe.cas', get_dico('telemac2d'))
        cas.set('PARALLEL PROCESSORS', 4)
        self.add_study('haa_par', 'telemac2d', 't2d_friction_fe.cas', cas=cas)
        del cas
        
        #======================================================================
        # Chezy law :
        # 
        # seq
        cas = TelemacCas('t2d_friction_fe.cas', get_dico('telemac2d'))
        cas.set('LAW OF BOTTOM FRICTION', 2)
        cas.set('FRICTION COEFFICIENT', 38.4900179474245) # Chezy coef
        self.add_study('che_seq', 'telemac2d', 't2d_friction_fe.cas', cas=cas)
        # par
        cas = TelemacCas('t2d_friction_fe.cas', get_dico('telemac2d'))
        cas.set('PARALLEL PROCESSORS', 4)
        self.add_study('che_par', 'telemac2d', 't2d_friction_fe.cas', cas=cas)
        del cas
        
        #======================================================================
        # Strikler law :
        # 
        # seq
        cas = TelemacCas('t2d_friction_fe.cas', get_dico('telemac2d'))
        cas.set('LAW OF BOTTOM FRICTION', 3)
        cas.set('FRICTION COEFFICIENT', 32.04999045006889) # Strickler coef
        self.add_study('str_seq', 'telemac2d', 't2d_friction_fe.cas', cas=cas)
        # par
        cas = TelemacCas('t2d_friction_fe.cas', get_dico('telemac2d'))
        cas.set('PARALLEL PROCESSORS', 4)
        self.add_study('str_par', 'telemac2d', 't2d_friction_fe.cas', cas=cas)
        del cas

        #======================================================================
        # Manning law :
        # 
        # seq
        cas = TelemacCas('t2d_friction_fe.cas', get_dico('telemac2d'))
        cas.set('LAW OF BOTTOM FRICTION', 4)
        cas.set('FRICTION COEFFICIENT', 0.031201257346953455) # Manning coef
        self.add_study('man_seq', 'telemac2d', 't2d_friction_fe.cas', cas=cas)
        # par
        cas = TelemacCas('t2d_friction_fe.cas', get_dico('telemac2d'))
        cas.set('PARALLEL PROCESSORS', 4)
        self.add_study('man_par', 'telemac2d', 't2d_friction_fe.cas', cas=cas)
        del cas

        #======================================================================
        # Nikuradse :
        # 
        # seq
        cas = TelemacCas('t2d_friction_fe.cas', get_dico('telemac2d'))
        cas.set('LAW OF BOTTOM FRICTION', 5)
        cas.set('FRICTION COEFFICIENT', 0.2427333307) # roughness height (m)
        self.add_study('nik_seq', 'telemac2d', 't2d_friction_fe.cas', cas=cas)
        # par
        cas = TelemacCas('t2d_friction_fe.cas', get_dico('telemac2d'))
        cas.set('PARALLEL PROCESSORS', 4)
        self.add_study('nik_par', 'telemac2d', 't2d_friction_fe.cas', cas=cas)
        del cas

        #======================================================================
        # Colebrook-White law :
        # 
        # seq
        cas = TelemacCas('t2d_friction_fe.cas', get_dico('telemac2d'))
        cas.set('LAW OF BOTTOM FRICTION', 6)
        cas.set('FRICTION COEFFICIENT', 0.2419536811) # roughness height (m)
        self.add_study('col_seq', 'telemac2d', 't2d_friction_fe.cas', cas=cas)
        # par
        cas = TelemacCas('t2d_friction_fe.cas', get_dico('telemac2d'))
        cas.set('PARALLEL PROCESSORS', 4)
        self.add_study('col_par', 'telemac2d', 't2d_friction_fe.cas', cas=cas)
        del cas

    def _check_results(self):
        """
        Post-treatment processes
        """
        #======================================================================
        # Haaland law :
        self.check_epsilons('haa_seq:T2DRES', 'f2d_haa_fe.slf', eps=[1e-6])
        self.check_epsilons('haa_par:T2DRES', 'f2d_haa_fe.slf', eps=[1e-1])
        self.check_epsilons('haa_seq:T2DRES', 'haa_par:T2DRES', eps=[1e-1])
        
        #======================================================================
        # Chezy law :
        self.check_epsilons('che_seq:T2DRES', 'f2d_che_fe.slf', eps=[1e-8])
        self.check_epsilons('che_par:T2DRES', 'f2d_che_fe.slf', eps=[1e-2])
        self.check_epsilons('che_seq:T2DRES', 'che_par:T2DRES', eps=[1e-2])
        
        #======================================================================
        # Strikler law :
        self.check_epsilons('str_seq:T2DRES', 'f2d_str_fe.slf', eps=[1e-8])
        self.check_epsilons('str_par:T2DRES', 'f2d_str_fe.slf', eps=[1e-2])
        self.check_epsilons('str_seq:T2DRES', 'str_par:T2DRES', eps=[1e-2])

        #======================================================================
        # Manning law :
        self.check_epsilons('man_seq:T2DRES', 'f2d_man_fe.slf', eps=[1e-8])
        self.check_epsilons('man_par:T2DRES', 'f2d_man_fe.slf', eps=[1e-2])
        self.check_epsilons('man_seq:T2DRES', 'man_par:T2DRES', eps=[1e-2])

        #======================================================================
        # Nikuradse :
        self.check_epsilons('nik_seq:T2DRES', 'f2d_nik_fe.slf', eps=[1e-8])
        self.check_epsilons('nik_par:T2DRES', 'f2d_nik_fe.slf', eps=[1e-2])
        self.check_epsilons('nik_seq:T2DRES', 'nik_par:T2DRES', eps=[1e-2])

        #======================================================================
        # Colebrook-White law :
        self.check_epsilons('col_seq:T2DRES', 'f2d_col_fe.slf', eps=[1e-8])
        self.check_epsilons('col_par:T2DRES', 'f2d_col_fe.slf', eps=[1e-2])
        self.check_epsilons('col_seq:T2DRES', 'col_par:T2DRES', eps=[1e-2])


    def _post(self):
        """
        Post-treatment processes
        """
        from postel.plot_vnv import vnv_plot1d_polylines, vnv_plot2d

        #======================================================================
        # COMPARISON PLOTS

        # Load all results as a list:
        res_list, res_labels = self.get_study_res(module='T2D', whitelist=['seq'])

        res_labels = [
          'Haaland',
          'Chezy',
          'Strickler',
          'Manning',
          'Nikuradse',
          'Colebrook-White',
          ]

        # Plot water depth 1D:
        vnv_plot1d_polylines(\
            'WATER DEPTH',
            res_list,
            res_labels,
            record=-1,
            ylim=[2.99, 3.01],
            poly=[[0.0, 0.0], [10000.0, 0.0]],
            y_label='$h$ (m)',
            fig_size=(9, 3.),
            ref_name='EXACT DEPTH',
            fig_name='img/final_depth_comparison_fe')

        # Plot free surface 1D:            
        vnv_plot1d_polylines(\
            'FREE SURFACE',
            res_list,
            res_labels,
            record=-1,
            poly=[[0.0, 0.0], [10000.0, 0.0]],
            y_label='$\eta$ (m)',
            fig_size=(9, 3.),
            ref_name='EXACT ELEVATION',
            fig_name='img/final_elevation_comparison_fe')
