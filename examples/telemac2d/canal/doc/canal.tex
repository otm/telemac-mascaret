\chapter{Flow driven by a stage-discharge curve (canal)}

\section{Description}

This example checks that \telemac{2D} is able to deal with stage-discharge
curves.

The chosen configuration is the same as the \telemac{3D} example canal.
This is a straight channel 500~m long and 100~m wide
with a flat horizontal bottom without slope.
Three different cases are studied:
\begin{itemize}
\item An initial computation with prescribed elevation at the exit and
prescribed discharge at the entrance,
\item A computation with prescribed discharge at the entrance and prescribed
elevation using a stage-discharge curve $Z = f(Q)$,
\item A computation with prescribed elevation at the entrance and prescribed
discharge using a stage-discharge curve $Q = f(Z)$.
\end{itemize}
In all cases, the flow establishes a steady flow where the free surface
is influenced by the friction on the bottom.

\subsection{Initial and boundary conditions}

The first computation is initialised with a constant elevation equal to
0.5 m and no velocity.
The two last computations are initialised by a steady flow, the one computed
by the first computation.
See Figures 
\ref{t2d:canal:fig:veloH} and \ref{t2d:canal:fig:freeSurface}.

The boundary conditions are:
\begin{itemize}
\item For the solid walls, a slip condition on channel banks is used for the
velocities,
\item On the bottom, Strickler law with friction coefficient equal to
50~$\text{m}^{1/3}.\text{s}^{-1}$ is prescribed,
\item Upstream a flowrate equal to 50~$\text{m}^{3}.\text{s}^{-1}$ is prescribed
for the first and second computations.
For the third computation, a flowrate is also prescribed but by giving a
stage-discharge curve $Q = f(Z)$,
\item Downstream the water level is equal to 0.5~m for the first and third
computations.
For the second computation, elevation is also prescribed but by giving a
stage-discharge curve $Z = f(Q)$.
\end{itemize}
\begin{figure}[!htbp]
 \centering
 \includegraphicsmaybe{[width=\textwidth]}{../img/veloH.png}
 \caption{Horizontal velocity obtained at the steady state.}
 \label{t2d:canal:fig:veloH}
\end{figure}

\begin{figure}[!htbp]
 \centering
 \includegraphicsmaybe{[width=\textwidth]}{../img/freeSurfac.png}
 \caption{Free surface at the steady state.}
 \label{t2d:canal:fig:freeSurface}
\end{figure}

\subsection{Mesh and numerical parameters}
\bigskip
The mesh (Figure \ref{t2d:canal:fig:meshH})
is made of 551 triangular elements (319 nodes).

\begin{figure}[!htbp]
 \centering
 \includegraphicsmaybe{[width=\textwidth]}{../img/Mesh.png}
 \caption{Horizontal mesh.}
 \label{t2d:canal:fig:meshH}
\end{figure}

The time step is 2~s for a simulated period of 4,000~s for the initial 2D
computation then 2,000~s more for the two computations with stage-discharge
curves.

To solve the advection, the method of characteristics
is used for the velocities (scheme 1).
The conjugate gradient
is used for solving the propagation step (option 1) and
the implicitation coefficients 
for depth and velocities are respectively equal to 1 and 0.55. 

\section{Results}

In Figure \ref{t2d:canal:fig:profiles}, the three free surface profiles
corresponding to each simulation are compared.
These three results are in good agreement.
We can also observe that the flow is completely symmetric without
any influence of the space discretisation.

\begin{figure}[!htbp]
 \centering
 \includegraphicsmaybe{[width=\textwidth]}{../img/free_surface.png}
 \caption{Comparison of the free surface profiles between the
 computations without or with stage-discharge curves. }
 \label{t2d:canal:fig:profiles}
 \end{figure}
 
\section{Conclusion}

To conclude, \telemac{2D} is able to take into account stage-discharge curves.
