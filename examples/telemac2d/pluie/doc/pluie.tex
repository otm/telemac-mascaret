\chapter{Infiltration (pluie)}
\label{chapter:pluie}

\section{The SCS-Curve Number runoff model}

\subsection{Purpose}

The aim of this test case is to check the implementation of the SCS-Curve Number
infiltration model.

\subsection{Theoretical background}

This test case is extracted from \cite{Ligier2016}.
The method described below is directly taken from this paper.

Curve Number runoff model, also known as the SCS Method of Abstractions, has
been developed from 1954 by USA’s Soil Conservation Service (SCS).
This method, which is widely used in the world, aims at computing abstractions
from storm rainfall using a spatially and temporally lumped infiltration loss model.
It gives best results in agricultural watersheds with negligible baseflow \cite{soil1972national}.

The aim of runoff modelling is to assess the hydrologic abstractions $F$ which are
composed of (i) interception storage (vegetation foliage...),
(ii) surface storage, (iii) infiltration, (iv) evaporation and
(v) evapotranspiration.
For short-term storm modelling, which is the Curve Number runoff model's field
of application, abstractions due to infiltration are largely predominant over
other forms which are then disregarded.

The total rain $R_t$ can be divided in the following terms $R_t=R_n+I_i+I_h$,
with $R_n$ the net rainfall contibuting to the runoff, $I_i$ the initial
abstraction and $I_h$ the hydrological abstraction.
The aim of this model is to estimate the total infiltration $I_t=I_i+I_h$,
to deduce the net rainfall contributing to the runoff with $R_n=R_t-I_t$.

$R_n$ can be written as:

\begin{equation}
  R_n=\left\{\begin{array}{l}
    \frac{(R_t-I_i)^2}{R_t-I_i+S} \text{ if } R_t>I_i\\
    0 \text{ else}
  \end{array} \right.
\end{equation}

where:

\begin{equation}
  I_i=\lambda S
\end{equation}

with $\lambda$ the initial abstraction ratio, set to 0.2, and:

\begin{equation}
  S=25.4\left(\frac{1000}{CN}-10\right)
\end{equation}

the potential maximal retention.
$CN$ is the Curve Numbre representing the calibration variable of the model.
Its value can be spatially distributed and has been defined for different types
of land use classes and for four different hydrological soil groups:
Group A (deep sands, deep loess and aggregated silts), Group B (shallow loess,
sandy loams), Group C (clay loams, shallow sandy loams, soils low in organic
content and soils usually high in clay) and Group D (soils that swell
significantly when wet, heavy plastic clays and certain saline soils) and for
three types of hydrologic surface condition of native pasture (poor, fair, and good).

\subsection{Description}

Three examples are provided with
(i) a classic rainfall defined by a constant rainfall intensity without runoff model,
(ii) a classic rainfall defined by a constant rainfall intensity with Curve
Number runoff model using $CN$ values interpolated from a set of points provided
in a formatted data file and
(iii) a rainfall defined by a hyetograph read from a formatted data file with
Curve Number runoff model using $CN$ values stocked in the geometry file as an
additional variable.

\subsubsection{Geometry and mesh}

The model geometry is a square with a side length of 100 meters composed of
5,412 triangular elements.
The computational domain is divided in four parts with $CN$ values of 80, 85,
90 and 95. Figure \ref{pluie:fig:mesh} presents the mesh.

\begin{figure}[!htbp]
 \centering
 \includegraphicsmaybe{[width=0.5\textwidth]}{../img/mesh.png}
 \caption{Mesh of the channel.}
 \label{pluie:fig:mesh}
\end{figure}

\subsubsection{Bathymetry}

The bottom is flat in this test case.

\subsubsection{Initial and boundary conditions}

The initial condition is a zero water depth, and there is no open boundary.

\subsubsection{Physical parameters}

The classic rainfall is defined using the existing keyword
\telkey{RAIN OR EVAPORATION IN MM PER DAY} = 100.0 and for a duration of 6 hours
(\telkey{DURATION OF RAIN OR EVAPORATION IN HOURS} = 6.0) so that the total
rainfall depth is 25 mm.
The hyetograph defined in the last example has an irregular time distribution
but has the same total rainfall depth.
All the examples are run over a simulation period of 8 hours with a time-step of
200 seconds.

The Curve Number runoff model is used with default settings
(\telkey{ANTECEDENT MOISTURE CONDITIONS} = 2 and
\telkey{OPTION FOR INITIAL ABSTRACTION RATIO} = 1).

\subsection{Results}

Figure \ref{pluie:fig:res} shows the results at the final time step for the 3
simulations.

\begin{figure}[!htbp]
 \centering
 \includegraphicsmaybe{[width=0.5\textwidth]}{../img/water-depth1.png}\\
 \includegraphicsmaybe{[width=0.5\textwidth]}{../img/water-depth2.png}\\
 \includegraphicsmaybe{[width=0.5\textwidth]}{../img/water-depth3.png}
 \caption{Water depth at the final time step and volume balance for all test cases.}
 \label{pluie:fig:res}
\end{figure}

\subsection{Conclusion}

\telemac{2D} is able to simulate the infiltration with the SCS-Curve Number runoff model.

\section{The Green-Ampt runoff model}

\subsection{Purpose}

The aim of this test case is to check the implementation of the Green-Ampt runoff
model with a similar implementation as the "pluie" validation example which uses
SCS-CN runoff model for infiltration.

\subsection{Theoretical background}

Green-Ampt model, has been developed by Green and Ampt in 1911 \cite{GreenAmpt1911}.
The test case is based on \cite{Ligier2016}, but using the Green-Ampt model instead of SCS-CN model.

This model asserts that infiltration follows this law:
\begin{equation}
  f(t)=K_S\cdot[\frac{\psi\cdot(\theta_s-\theta_i)}{F(t)}+1]
\end{equation}
with $f$ the capacity of infiltration of the soil, $K_S$ the hydraulic
conductivity of the soil, $\psi$ the suction, $\theta_s$
and $\theta_i$ the water saturated and initial water content. \\
This law is extracted from Darcy's law.
The infiltration is represented with a vertical profile and depends on the
hydraulic conductivity of the soil, the position of the wetted front and the water
depth.
It assumes that as water begins to infiltrate the soil, a line is developed
differentiating between the 'dry' soil with moisture content $\theta_i$ and the
'wet' soil.
As the infiltrated water continues to move through the soil profile in a
vertical direction, the soil moisture changes from the initial content to a
saturated state. \\
The difficulty of using this infiltration law is to calibrate the different
constants that is to say $K_S$, $\psi$, $\theta_i$ and $\theta_s$.
The value of $K_S$ can be spatially distributed and is defined for different
types of soils.
The values should be given by the user in mm/h.
These parameters vary a lot with the properties of the soil (porosity, initial
water storage, vegetation cover...). The suction $\psi$ should be given in m. \\
It is one of the easiest Green-Ampt model to implement and use, there are some
more complex models with several vertical layers than can be set with different
hydraulic conductivity for each layer.
It requires to have data for each layer so more knowledge on the soil properties,
so this implementation has not been chosen for now, but it can be implemented by
modifying the subroutine.

\subsection{Description}

Three examples are provided with
(i) a classic rainfall defined by a constant rainfall intensity without runoff
model,
(ii) a classic rainfall defined by a constant rainfall intensity with Green-Ampt
model using $K_S$ values interpolated from a set of points provided in a
formatted data file and
(iii) a rainfall defined by a hyetograph read from a formatted data file with
Green-Ampt runoff model using $K_S$ values stocked in the geometry file as an
additional variable.

\subsubsection{Geometry and mesh}

The model geometry is a square with a side length of 100 meters composed of
5,416 triangular elements.
The computational domain is divided into four parts with $K_S$ values of 5, 10,
30 and 50 mm/h. Figure \ref{pluiehorton:fig:mesh:KS} presents the mesh and the
division for the $K_S$ coefficients.

\begin{figure}[H]
\begin{minipage}[t]{0.5\textwidth}
 \centering
 \includegraphicsmaybe{[width=\textwidth]}{../img/mesh-fv_ga.png}
\end{minipage}%
\begin{minipage}[t]{0.5\textwidth}
 \centering
 \includegraphicsmaybe{[width=\textwidth]}{../img/ks-fv_ga.png}
\end{minipage}
 \caption{Mesh and $K_S$ coefficients on the domain.}
 \label{pluiehorton:fig:mesh:KS}
\end{figure}

\subsubsection{Bathymetry}

The bottom is flat in this test case.

\subsubsection{Initial and boundary conditions}

The initial condition is a zero water depth, and there is no open boundary.

\subsubsection{Physical parameters}

The classic rainfall is defined using the existing keyword
\telkey{RAIN OR EVAPORATION IN MM PER DAY} = 1000.0 and for a duration of 6
hours (\telkey{DURATION OF RAIN OR EVAPORATION IN HOURS} = 6.0) so that the
total rainfall depth is 250 mm.
The hyetograph defined in the last example has an irregular time distribution,
but has the same total rainfall depth.
All the examples are run over a simulation period of 8 hours with variable time
step for \telkey{COURANT NUMBER} = 0.9 with the finite volume method.
The Green-Ampt model in this test case is used with \telkey{SUCTION} = 0.1 m,
\telkey{INITIAL WATER CONTENT} = 0 m$^3$/m$^3$
and \telkey{SATURATED WATER CONTENT} = 0.2 m$^3$/m$^3$.

\subsection{Results}

Figure \ref{pluiehorton:fig:res} shows the results at the final time step for
the 3 simulations.

\begin{figure}[!htbp]
 \centering
 \includegraphicsmaybe{[width=0.5\textwidth]}{../img/water-depth1-fv_ga.png}\\
 \includegraphicsmaybe{[width=0.5\textwidth]}{../img/runoff2-fv_ga.png}\\
 \includegraphicsmaybe{[width=0.5\textwidth]}{../img/runoff3-fv_ga.png}
 \caption{Water depth for the uniform rain and accumulated runoff for the 2 other cases at the final time step.}
 \label{pluiehorton:fig:res}
\end{figure}

\subsection{Conclusion}
\telemac{2D} is able to simulate the infiltration with the Green-Ampt runoff
model with runoff accumulations that are close to the analytical solutions at a
precision of $10^{-5}$ m.

\section{The Horton runoff model}
\subsection{Purpose}

The aim of this test case is to check the implementation of the Horton runoff
model with a similar implementation as the "pluie" validation example which uses
SCS-CN runoff model for infiltration.

\subsection{Theoretical background}

Horton model, has been developed by Horton in 1933 \cite{Horton1933}.
The test case is based on \cite{Ligier2016}, but using the Horton model instead
of SCS-CN model.

This model asserts that infiltration follows an exponential law:
\begin{equation}
  I(t)=f_c+(f_0-f_c)e^{-kt}
\end{equation}
with $f_c$ the minimum infiltration capacity, $f_0$ the infiltration capacity at
$t=0$, $k$ a positive decreasing constant  and $I(t)$ the infiltration capacity at time t. \\
This law is not extracted from Darcy's law, but comes from experimental results.
It states that infiltration capacity tends to decrease exponentially, until it
reaches a certain limit depending on the infiltration properties of the soil.\\
The difficulty of using this infiltration law is to calibrate the different
constants $f_0$, $f_c$ and $k$.
The value of $f_c$ can be spatially distributed and has been defined for
different types of land use classes and their respective permeability in the literature.
The values should be given by the user in mm/h. The value of $k$ should be given
in s$^{-1}$.\\
These parameters vary a lot with the properties of the soil (porosity, hydraulic
conductivity, initial water storage, vegetation cover...).
They can be measured by in-situ experiments, based on rainfall-discharge
measurements or estimated through the literature. \\
The value of $f_{0}$ can be calculated in function of $f_c$ considering
antecedent rainfall.
If the soil is in normal conditions, then $f_0=2 f_c$ with
\telkey{ANTECEDENT MOISTURE CONDITIONS} = 2 (it is an intermediate state between
dry and saturated that is not based on literature, so it is recommended to use
option 1 or 3 or to change the value in user$\_$fortran in runoff$\_$horton.f
for normal conditions). \\
If the soil is dry at the start of the rainfall event, it is often used the
Holtan model by  using the existing keyword
\telkey{ANTECEDENT MOISTURE CONDITIONS} = 1 and $f_0=4 f_c$.\\
And, if the soil is saturated, it is considered with
\telkey{ANTECEDENT MOISTURE CONDITIONS} = 3 that $f_0=f_c$.\\

Another law in the literature could be implemented for the value of $f_{0}$ and states that :
\begin{equation}
  f_{0}=(f_c-f_r)\cdot\frac{\theta_i-\theta_r}{\theta_s-\theta_r}+f_r
\end{equation}
with $f_{r}$ the infiltration capacity of the soil and $\theta_{r}$ the water
content for dry conditions.
It requires to have even more data on the soil properties, so this implementation
has not been chosen for now, but it can be implemented easily by the user by
modifying the subroutine.

\subsection{Description}

Three examples are provided with
(i) a classic rainfall defined by a constant rainfall intensity without runoff model,
(ii) a classic rainfall defined by a constant rainfall intensity with Horton
model using $f_c$ values interpolated from a set of points provided in a
formatted data file and
(iii) a rainfall defined by a hyetograph read from a formatted data file with
Horton runoff model using $f_c$ values stocked in the geometry file as an
additional variable.

\subsubsection{Geometry and mesh}

The model geometry is a square with a side length of 100 meters composed of
5,416 triangular elements.
The computational domain is divided into four parts with $f_c$ values of 5, 10,
30 and 50.
Figure \ref{pluiehorton:fig:mesh:fc} presents the mesh and the division for the
$f_c$ coefficients.

\begin{figure}[H]
\begin{minipage}[t]{0.5\textwidth}
 \centering
 \includegraphicsmaybe{[width=\textwidth]}{../img/mesh-fv_h.png}
\end{minipage}%
\begin{minipage}[t]{0.5\textwidth}
 \centering
 \includegraphicsmaybe{[width=\textwidth]}{../img/fc-fv_h.png}
\end{minipage}
 \caption{Mesh and $f_c$ coefficients on the domain.}
 \label{pluiehorton:fig:mesh:fc}
\end{figure}

\subsubsection{Bathymetry}

The bottom is flat in this test case.

\subsubsection{Initial and boundary conditions}

The initial condition is a zero water depth, and there is no open boundary.

\subsubsection{Physical parameters}

The classic rainfall is defined using the existing keyword
\telkey{RAIN OR EVAPORATION IN MM PER DAY} = 1000.0 and for a duration of 6 hours
(\telkey{DURATION OF RAIN OR EVAPORATION IN HOURS} = 6.0) so that the total
rainfall depth is 250 mm. The hyetograph defined in the last example has an
irregular time distribution, but has the same total rainfall depth.
All the examples are run over a simulation period of 8 hours with variable time
step for \telkey{COURANT NUMBER} = 0.9 with the finite volume method.
It is considered a normal precedent antecedent rainfall for this test case.
The Horton runoff model is used with default settings
(\telkey{HORTON TIME CONSTANT} = 0.001 s$^{-1}$).

\subsection{Results}

Figure \ref{pluiehorton:fig:res} shows the results at the final time step for
the 3 simulations.

\begin{figure}[!htbp]
 \centering
 \includegraphicsmaybe{[width=0.5\textwidth]}{../img/water-depth1-fv_h.png}\\
 \includegraphicsmaybe{[width=0.5\textwidth]}{../img/runoff2-fv_h.png}\\
 \includegraphicsmaybe{[width=0.5\textwidth]}{../img/runoff3-fv_h.png}
 \caption{Water depth for the uniform rain and accumulated runoff for the 2 other cases at the final time step.}
 \label{pluiehorton:fig:res}
\end{figure}

\subsection{Conclusion}
\telemac{2D} is able to simulate the infiltration with the Horton runoff model
with runoff accumulations that are close to the analytical solutions at a precision of $10^{-5}$ m.
