\chapter{Convergence study for the diffusion of tracers (tracer\_diffusion\_convergence)}


\section{Purpose}
This test shows the performance of the finite volume diffusion schemes of
\telemac{2D} for passive scalar in a time dependent case.
A convergence study is carried out using a simple 1D analytical solution.

%--------------------------------------------------------------------------------------------------
\section{Description}

\subsection{Geometry and mesh}

The dimensions of the domain are [2 $\times$ 20]~m$^2$.
The mesh is made from a regular grid from which all squares are cut in half.

\begin{figure}[h!]
\centering
\includegraphicsmaybe{[width=0.85\textwidth]}{../img/diffusion_mesh0.png}
\caption{2D domain and mesh of the flume\_tracer test case.}
\label{t2d:tracerdiffusion:mesh}
\end{figure}

\subsection{Initial condition}

The water depth is constant in time and in space, equal to 1~m.
The streamwise velocity $u$ is constant and equal to 0~m/s.
The tracer is initially set to 1 in all the domain.

\subsection{Boundary conditions}

At the begining of the test, a Dirichlet boundary condition with a tracer value
of 0 is imposed on the left side
boundary.

\subsection{Physical parameters}

Tracer diffusion is activated and the following keywords are set:
\begin{itemize}
\item\telkey{DIFFUSION OF TRACERS} = TRUE,
\item\telkey{COEFFICIENT FOR DIFFUSION OF TRACERS} = 1.;1.
\end{itemize}
The stability of the explicit finite volume diffusion schemes is garanted by
choosing:
\begin{itemize}
\item\telkey{DESIRED FOURIER NUMBER} = 0.9.
\end{itemize}

\subsection{Numerical parameters}

The simulation time is set to 10~s.
For tracers diffusion, all numerical schemes available in \telemac{2D} finite
volume solver are tested
i.e. the TPF (Two Point Flux) scheme, the RTPF (Reconstructed Two Point Flux)
scheme, 
the explicit P1 
finite element scheme with mass lumping, designed to work alongside finite
volume hyperbolic solvers (noted here HEFE for Hybrid Explicit Finite Element).
The schemes are selected via the keyword
\telkey{FINITE VOLUME SCHEME FOR TRACER DIFFUSION}.

%--------------------------------------------------------------------------------------------------
\section{Results}

\subsection{First observation}

The evolution of the tracer is plotted against time at the centerline of the
flume
in Figure~\ref{fig:tracerdiffusion:T}
respectively with both Dirichlet (left) and Neumann (right) boundary conditions.

\begin{figure}[h!]
 \centering
 \includegraphicsmaybe{[width=0.7\textwidth]}{../img/t2d_diffusion_T1_0.png}
 \includegraphicsmaybe{[width=0.7\textwidth]}{../img/t2d_diffusion_T1_9.png}
 \caption{Diffusion of the tracer at the centerline of the flume (0~s, 9~s).}
 \label{fig:tracerdiffusion:T}
\end{figure}

\subsection{Computation time}

Simulation times for each of these cases with sequential and parallel runs
(using 4 processors) are shown in Figure~\ref{fig:tracerdiffusion:cputime}.

\begin{figure}[H]
  \centering
  \includegraphicsmaybe{[width=0.75\textwidth]}{../img/t2d_diffusion_cpu_times.png}
  \caption{CPU times (Dirichlet conditions case).}
  \label{fig:tracerdiffusion:cputime}
\end{figure}

\subsection{Convergence}

In this section a mesh convergence is carried out to assess the accuracy of the
schemes. 
From a starting mesh we divide by 4 each triangles recursively to generate new
meshes.
The meshes used in the convergence study are presented in
Figure~\ref{t2d:tracerdiffusion:meshes}.

\begin{figure}[h!]
 \centering
 \includegraphicsmaybe{[width=0.95\textwidth]}{../img/mesh_0.png}
 \includegraphicsmaybe{[width=0.95\textwidth]}{../img/mesh_1.png}
 \includegraphicsmaybe{[width=0.95\textwidth]}{../img/mesh_2.png}
 \includegraphicsmaybe{[width=0.95\textwidth]}{../img/mesh_3.png}
 \caption{Meshes used in the mesh convergence.}
 \label{t2d:tracerdiffusion:meshes}
\end{figure}

With decreasing space step, we use a variable time step to ensure a constant
Fourier number
for each mesh increment.

Convergence errors in L$^2$ norm are plotted in
Figure~\ref{fig:tracerdiffusion:ErrNumT_convergence} 
and errors at final time are compared for the finest mesh in
Figure~\ref{fig:tracerdiffusion:ErrNumT_convergence_mesh3}.

The results show that the TPF (Two Point Flux) finite volume scheme does not
converge 
(this scheme is not consistent)
whereas the RTPF (Reconstructed Two Point Flux) finite volume scheme gives a
first order convergence rate.
Finally the HEFE (explicit P1 finite element scheme with mass lumping)
scheme converges with a second order convergence rate.

\begin{figure}[H]
\centering
  \includegraphicsmaybe{[width=0.95\textwidth]}{../img/t2d_flumetracer_errors_tf_T1_L2_allsc.png}
  \caption{T convergence in $L^2$ norm.}
\label{fig:tracerdiffusion:ErrNumT_convergence}
\end{figure}

\begin{figure}[H]
\centering
  \includegraphicsmaybe{[width=0.95\textwidth]}{../img/t2d_flumetracer_T1_errors_tf_mesh3.png}
  \caption{T errors at final time on the finest mesh.}
\label{fig:tracerdiffusion:ErrNumT_convergence_mesh3}
\end{figure}

\begin{figure}[H]
 \centering
 \includegraphicsmaybe{[width=0.65\textwidth]}{../img/t2d_flumetracer_TPF_errors_tf_T1.png}
 \includegraphicsmaybe{[width=0.65\textwidth]}{../img/t2d_flumetracer_RTPF1_errors_tf_T1.png}
 \includegraphicsmaybe{[width=0.65\textwidth]}{../img/t2d_flumetracer_RTPF2_errors_tf_T1.png}
 \includegraphicsmaybe{[width=0.65\textwidth]}{../img/t2d_flumetracer_HEFE_errors_tf_T1.png}
  \caption{T convergence with different numerical schemes.}
 \label{t2d:tracerdiffusion:mesh_convergence_T}
\end{figure}

%--------------------------------------------------------------------------------------------------

\section{Conclusion}
\telemac{2D} is able to model passive scalar diffusion problems in shallow water
flows with finite volume diffusion schemes.
