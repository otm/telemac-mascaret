\chapter{Transient flood flow in the valley of river Culm (culm)}
\section{Purpose}
To illustrate that \telemac{2D} is capable of simulating the real
inundation of a flood plain. Also to show the evolution in time of the 
water surface and velocity patterns in the flood plain.

\subsection{Approach}
This test case represents the plain of the river Culm (Devon, UK) over a 11~km
reach. The river is meandering in its valley. A narrowing concentrates
flow in the middle of the reach represented. The main channel is
approximately 19~m wide. The downstream elevation and the upstream discharge
prescribed vary in time to simulate one flood of the river.

Field data concerning discharge and flooded areas were available for this event.

\section{Description}

\subsection{Geometry and mesh}

The size of the model river is approximately 10~km
with water depth at rest = 0~m.

The mesh is unstructured. It is denser in the bed of the river than in the flood plain.
Triangles are elongated in the direction of flow:
\begin{itemize}
\item 2,019 triangular elements,
\item 1,130 nodes,
\item Maximum size range: from 18 to 258~m.
\end{itemize}

\subsection{Boundaries}
Initially:
\begin{itemize}
\item River entrance: $Q$ = 28.32~m$^3$/s imposed,
\item  River outlet: $H$ = 23.77~m imposed.
 \end{itemize}
During the simulation, the discharge at the entrance and the water level at the outlet are
varying with time and prescribed by the formatted data files.

The lateral boundaries are solid walls with slip condition in the domain.

Bottom: Strickler formula with friction coefficient = 30~m$^{1/3}$/s.

The mesh and the topography are shown in Figures \ref{fig:culm:Mesh} and \ref{fig:culm:Bathy}.
\begin{figure}[H]
 \centering
 \includegraphicsmaybe{[width=.9\textwidth]}{../img/Mesh.png}
  \caption{2D-mesh of the culm case.}\label{fig:culm:Mesh}
\end{figure}


\begin{figure}[H]
 \centering
 \includegraphicsmaybe{[width=.9\textwidth]}{../img/Bathy.png}
  \caption{Bathymetry of the culm case.}\label{fig:culm:Bathy}
\end{figure}

\subsection{Physical parameters}

The model of turbulence is constant viscosity
with velocity diffusivity = 2~m$^2$/s.

\subsection{Numerical parameters}
Type of advection:
\begin{itemize}
\item Methods of characteristics on velocities (scheme \#1),
\item Conservative + modified SUPG on depth (mandatory scheme).
\end{itemize}
The type of element is linear triangle (P1) for $h$ and for velocities.

\begin{itemize}
\item Implicitation for depth and velocities = 1,
\item Solver: Conjugate gradient with solver accuracy 10$^{-4}$,
\item Tidal flats,
\item Time step = 2~s,
\item Simulation duration = 54,000~s (= 15~h).
\end{itemize}

\section{Results}
The water profile in the river varies according to the flood conditions prescribed at the boundaries
(water elevation downstream and discharge upstream). Flooding of the plain occurs during
the simulation. At the peak of the inundation, the water depth is approximately equal to 0.40~m
in the plain close to the river.

\begin{figure}[H]
\begin{minipage}[t]{0.5\textwidth}
 \centering
  \includegraphicsmaybe{[width=\textwidth]}{../img/WaterDepth_0s.png}
\end{minipage}
\begin{minipage}[t]{0.5\textwidth}
 \centering
  \includegraphicsmaybe{[width=\textwidth]}{../img/WaterDepth_10000s.png}
\end{minipage}
\begin{minipage}[t]{0.5\textwidth}
 \centering
  \includegraphicsmaybe{[width=\textwidth]}{../img/WaterDepth_20000s.png}
\end{minipage}
\begin{minipage}[t]{0.5\textwidth}
 \centering
  \includegraphicsmaybe{[width=\textwidth]}{../img/WaterDepth_40000s.png}
\end{minipage}
\caption{Water depth at different times.}
\label{fig:culm:WaterDepths}
\end{figure}

\begin{figure}[H]
 \centering
 \includegraphicsmaybe{[width=.9\textwidth]}{../img/Water_depth.png}
  \caption{Water depth at final time.}\label{fig:culm:WaterDepth}
\end{figure}

\begin{figure}[H]
 \centering
 \includegraphicsmaybe{[width=.9\textwidth]}{../img/Velocity.png}
  \caption{Velocity at final time.}\label{fig:culm:Velocity}
\end{figure}

\begin{figure}[H]
 \centering
 \includegraphicsmaybe{[width=.9\textwidth]}{../img/timeserie.png}
  \caption{Free surface at point (7841.22 ; 6842.13).}\label{fig:culm:timeserie}
\end{figure}

\section{Conclusion}
\telemac{2D} can simulate the flow resulting from the inundation of a river valley by natural floods.
