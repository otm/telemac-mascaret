\chapter{Diffusion of tracers (tracer\_diffusion)}

\section{Purpose}
This test shows the performance of the finite volume diffusion schemes of
\telemac{2D} for passive scalar in a time dependent case.
It shows the diffusion of a tracer (or any other passive scalar) in a flume with
both Dirichlet and Neumann boundary conditions.

%--------------------------------------------------------------------------------------------------
\section{Description}

\subsection{Geometry and mesh}

The dimensions of the domain are $[2 \times 20]~\rm{m}^2$.
The mesh is made from a regular grid from which all squares are cut in half.

\begin{figure}[h!]
\centering
\includegraphicsmaybe{[width=0.85\textwidth]}{../img/diffusion_mesh0.png}
\caption{2D domain and mesh of the tracer\_diffusion test case.}
\label{t2d:tracerdiffusion:mesh}
\end{figure}

\subsection{Initial condition}

The water depth is constant in time and in space, equal to 1~m.
The streamwise velocity $u$ is constant and equal to 0~m/s.
Two tracers are defined in the domain for which initial conditions are
a Gaussian and a Crenel.

\begin{figure}[h!]
\centering
\includegraphicsmaybe{[width=0.95\textwidth]}{../img/diffusion_dir_T1_FE_0.png}
\caption{Initial condition of the first tracer.}
\label{t2d:tracerdiffusion:mesh}
\end{figure}

\begin{figure}[h!]
\centering
\includegraphicsmaybe{[width=0.95\textwidth]}{../img/diffusion_dir_T2_FE_0.png}
\caption{Initial condition of the second tracer.}
\label{t2d:tracerdiffusion:mesh}
\end{figure}

\subsection{Boundary conditions}

All the tests are carried out with both Dirichlet and Neumann boundary
conditions at the inlet and outlet of the domain.

\subsection{Physical parameters}

Tracer diffusion is activated and the following keywords are set:
\begin{itemize}
\item\telkey{DIFFUSION OF TRACERS} = TRUE,
\item\telkey{COEFFICIENT FOR DIFFUSION OF TRACERS} = 1.;1.
\end{itemize}
The stability of the explicit finite volume diffusion schemes is garanted by
choosing:
\begin{itemize}
\item\telkey{DESIRED FOURIER NUMBER} = 0.9.
\end{itemize}

\subsection{Numerical parameters}

The simulation time is set to 10~s.
For tracers diffusion, all numerical schemes available in \telemac{2D} finite
volume solver are tested
i.e. the TPF (Two Point Flux) scheme, the RTPF (Reconstructed Two Point Flux)
scheme,
the explicit P1
finite element scheme with mass lumping, designed to work alongside finite
volume hyperbolic solvers (noted here HEFE for Hybrid Explicit Finite Element).
The schemes are selected via the keyword
\telkey{FINITE VOLUME SCHEME FOR TRACER DIFFUSION}.
Additionally the standard implicit finite element method of \telemac{2D} is
computed for the purpose of comparison.

%--------------------------------------------------------------------------------------------------
\section{Results}

\subsection{Computation time}

Simulation times for each of these cases with sequential and parallel runs
(using 4 processors) are shown in Figure~\ref{fig:tracerdiffusion:cputime}.

\begin{figure}[H]
  \centering
  \includegraphicsmaybe{[width=0.8\textwidth]}{../img/t2d_diffusion_dir_cpu_times.png}
  \caption{CPU times (Dirichlet conditions case).}
  \label{fig:tracerdiffusion:cputime}
\end{figure}

\subsection{Comparison of diffusion schemes}

The evolution of both tracers are plotted against time at the centerline of the flume
in Figures ~\ref{fig:tracerdiffusion:T1_dir} and ~\ref{fig:tracerdiffusion:T2_dir}
respectively with both Dirichlet (left) and Neumann (right) boundary conditions.

\begin{figure}[h!]
\begin{minipage}[t]{0.5\textwidth}
 \centering
 \includegraphicsmaybe{[width=0.95\textwidth]}{../img/t2d_diffusion_dir_T1_0.png}
\end{minipage}
\begin{minipage}[t]{0.5\textwidth}
 \centering
 \includegraphicsmaybe{[width=0.95\textwidth]}{../img/t2d_diffusion_neu_T1_0.png}
\end{minipage}
\begin{minipage}[t]{0.5\textwidth}
 \centering
 \includegraphicsmaybe{[width=0.95\textwidth]}{../img/t2d_diffusion_dir_T1_3.png}
\end{minipage}
\begin{minipage}[t]{0.5\textwidth}
 \centering
 \includegraphicsmaybe{[width=0.95\textwidth]}{../img/t2d_diffusion_neu_T1_3.png}
\end{minipage}
\begin{minipage}[t]{0.5\textwidth}
 \centering
 \includegraphicsmaybe{[width=0.95\textwidth]}{../img/t2d_diffusion_dir_T1_6.png}
\end{minipage}
\begin{minipage}[t]{0.5\textwidth}
 \centering
 \includegraphicsmaybe{[width=0.95\textwidth]}{../img/t2d_diffusion_neu_T1_6.png}
\end{minipage}
\begin{minipage}[t]{0.5\textwidth}
 \centering
 \includegraphicsmaybe{[width=0.95\textwidth]}{../img/t2d_diffusion_dir_T1_9.png}
\end{minipage}
\begin{minipage}[t]{0.5\textwidth}
 \centering
 \includegraphicsmaybe{[width=0.95\textwidth]}{../img/t2d_diffusion_neu_T1_9.png}
\end{minipage}
  \caption{Diffusion of the first tracer with Dirichlet boundary conditions (left) and Neumann boundary conditions (right).}
  \label{fig:tracerdiffusion:T1_dir}
\end{figure}

\begin{figure}[h!]
\begin{minipage}[t]{0.5\textwidth}
 \centering
 \includegraphicsmaybe{[width=0.95\textwidth]}{../img/t2d_diffusion_dir_T2_0.png}
\end{minipage}
\begin{minipage}[t]{0.5\textwidth}
 \centering
 \includegraphicsmaybe{[width=0.95\textwidth]}{../img/t2d_diffusion_neu_T2_0.png}
\end{minipage}
\begin{minipage}[t]{0.5\textwidth}
 \centering
 \includegraphicsmaybe{[width=0.95\textwidth]}{../img/t2d_diffusion_dir_T2_3.png}
\end{minipage}
\begin{minipage}[t]{0.5\textwidth}
 \centering
 \includegraphicsmaybe{[width=0.95\textwidth]}{../img/t2d_diffusion_neu_T2_3.png}
\end{minipage}
\begin{minipage}[t]{0.5\textwidth}
 \centering
 \includegraphicsmaybe{[width=0.95\textwidth]}{../img/t2d_diffusion_dir_T2_6.png}
\end{minipage}
\begin{minipage}[t]{0.5\textwidth}
 \centering
 \includegraphicsmaybe{[width=0.95\textwidth]}{../img/t2d_diffusion_neu_T2_6.png}
\end{minipage}
\begin{minipage}[t]{0.5\textwidth}
 \centering
 \includegraphicsmaybe{[width=0.95\textwidth]}{../img/t2d_diffusion_dir_T2_9.png}
\end{minipage}
\begin{minipage}[t]{0.5\textwidth}
 \centering
 \includegraphicsmaybe{[width=0.95\textwidth]}{../img/t2d_diffusion_neu_T2_9.png}
\end{minipage}
  \caption{Diffusion of the second tracer with Dirichlet boundary conditions (left) and Neumann boundary conditions (right).}
  \label{fig:tracerdiffusion:T2_dir}
\end{figure}

%--------------------------------------------------------------------------------------------------

\section{Conclusion}
\telemac{2D} is able to model passive scalar diffusion problems in shallow water
flows with finite volume diffusion schemes.
