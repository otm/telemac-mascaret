\chapter{Uncovering of a beach (vasque)}
\section{Purpose}
To demonstrate that \telemac{2D} is capable of representing the drying of a
cylindrical beach presenting a pool.
Also to show that \telemac{2D} works properly when disconnected water bodies exist
within the study domain.

\section{Description}
\subsection{Approach}
A grid made of squares split into two triangles is used to represent a
cylindrical beach presenting a pool.
The water initially covers the entire domain (see Figure \ref{fig:vasque:FSinit}).

\begin{figure}[H]
 \centering
 \includegraphicsmaybe{[width=.7\textwidth]}{../img/FreeSurface_t0.png}
  \caption{Initial free surface elevation and bottom elevation.}\label{fig:vasque:FSinit}
\end{figure}

As the tide falls by 0.60~m, a 46~m cross-shore length of the beach is
uncovered, thus leaving the pool full of water at rest.

\subsection{Geometry and mesh}
\begin{itemize}
\item Size of the model: channel = 46~m $\times$ 9~m,
\item Initial water level 0~m.
\end{itemize}

The mesh is regular. It is made up with squares split into two triangles.
\begin{itemize}
\item 828 triangular elements,
\item 470 nodes,
\item Maximum size range: $\sqrt{2}$~m.
\end{itemize}

\subsection{Boundaries}

\begin{itemize}
\item Shoreline: solid wall with slip condition,
%\item Offshore boundary: $Q = -\frac{5 H}{0.6}$ imposed,
\item Offshore boundary: $z = \frac{0.55}{2}\left( \cos(\frac{2\pi}{600}t) -1 \right)$ imposed,
\item Lateral boundaries: solid walls with slip condition in the channel.
\end{itemize}

\subsection{Bottom}
The bottom is defined by $z_b = -0.6+0.01x+0.1e^{\frac{-(x-19)^2}{20}}$.
The Strickler friction formula is used with friction coefficient =~40~m$^{1/3}$/s.

Mesh and topography are shown in Figure \ref{fig:vasque:Mesh} and \ref{fig:vasque:Bottom}.

\begin{figure}[H]
 \centering
 \includegraphicsmaybe{[width=.7\textwidth]}{../img/Mesh.png}
  \caption{2D-mesh of the vasque case.}\label{fig:vasque:Mesh}
\end{figure}
\begin{figure}[H]
 \centering
 \includegraphicsmaybe{[width=.7\textwidth]}{../img/Bottom.png}
  \caption{Bathymetry of the vasque case.}\label{fig:vasque:Bottom}
\end{figure}

\subsection{Physical parameters}

The model of turbulence is constant viscosity
with velocity diffusivity = 10$^{-2}$~m$^2$/s.

\subsection{Numerical parameters}
Type of advection:
\begin{itemize}
\item Methods of characteristics on velocities (scheme \#1),
\item Conservative + modified SUPG on depth (mandatory scheme),
\item Solver accuracy =~10$^{-4}$,
\item Implicitation for depth and for velocity : 0.6,
\item Time step = 1~s,
\item Simulation duration =~300 s.
\end{itemize}

\section{Results}
The water level decreases regularly over the beach. At the end of the
calculation, the water depth is equal to
zero upstream and downstream of the pool, and the water level in the pool
is completely horizontal (see Figures \ref{fig:vasque:FreeSurfacetf}
and \ref{fig:vasque:FreeSurface}).

\begin{figure}[H]
 \centering
 \includegraphicsmaybe{[width=.7\textwidth]}{../img/FreeSurface_tf.png}
  \caption{Free surface elevation at final time (= 300~s).}\label{fig:vasque:FreeSurfacetf}
\end{figure}

\begin{figure}[H]
 \centering
 \includegraphicsmaybe{[width=.7\textwidth]}{../img/FreeSurface_Y5.png}
  \caption{Evolution of the free surface elevation in time.}\label{fig:vasque:FreeSurface}
\end{figure}

If using \telkey{TREATMENT OF NEGATIVE DEPTHS} = 2 (default choice since release
9.0), there is no negative water depths during the computation contrary to the
previous old default choice (= 1) for which there were a few nodes with negative
water depths.

\section{Conclusion}
The uncovering of tidal flats is properly represented by \telemac{2D}.
