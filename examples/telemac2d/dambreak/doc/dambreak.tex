\chapter{Dambreak: Ritter and Stoker}

\section{Purpose}

The purpose of this case is to demonstrate that the \telemac{2D} solution is
able to simulate the propagation of a wave due to a dam break.
We compare \telemac{2D} with two classical analytical solutions:
Ritter's solution and Stoker's solution.

\section{Description}

\subsection{Geometry and mesh}

We consider a channel on length $L$ = 16~m and width $l$ = 0.45~m with a flat
bottom and with no friction.

\begin{figure}[H]
   \centering
   \includegraphicsmaybe{*[width=0.8\textwidth, keepaspectratio=true]}{../img/t2d_ritter_mesh.png}
 \caption{Mesh of the channel.} \label{fig:rittermesh}
\end{figure}

\subsection{Initial conditions}

Let us define the abscissa of the dam by $x_d$.
The initial condition is given by a 1D Riemann problem:

\subsubsection{Dry case:}

\begin{equation}
\begin{split}h(x) = \begin{cases}
h_l > 0 \quad \text{for} \quad 0 \leq x \leq x_d \\
h_r = 0 \quad \text{for} \quad x_d < x \leq L
\end{cases}\end{split}
\end{equation}
with $h_l \geq h_r$ and zero velocity in the channel.

\subsubsection{Wet case:}

\begin{equation}
\begin{split}h(x) = \begin{cases}
h_l \quad \text{for} \quad 0 \leq x \leq x_d \\
h_r \quad \text{for} \quad x_d < x \leq L
\end{cases}\end{split}
\end{equation}

\begin{figure}[htbp]
\centerline{  \begin{tabular}{cc}
\begin{minipage}{.48\linewidth}
\centerline{
  \includegraphicsmaybe{[width=0.9\textwidth]}{../img/t2d_ritter_initial_elevation.png}}
  \caption{Initial condition for Ritter.}
    \label{fig:ritterini}
\end{minipage}
&
\begin{minipage}{.48\linewidth}
\centerline{
\includegraphicsmaybe{[width=0.9\textwidth]}{../img/t2d_stoker_initial_elevation.png}}
\caption{Initial condition for Stoker.}
    \label{fig:stokerini}
\end{minipage}
  \end{tabular}}
\end{figure}

\subsection{Analytical solutions}

Given the 2 Riemann problems defined as initial conditions, the analytical
solution is obtained with characteristics method and is given by:
\subsubsection{Dry case: Ritter's solution}
\begin{equation}
\begin{split}h(x,t) = \begin{cases}
h_l \quad & \text{if} \quad x \leq x_A(t) \\
\dfrac{4}{9g} \left( \sqrt{gh_l} - \dfrac{x-x_0}{2t} \right)^2 \quad & \text{if} \quad x_A(t) \leq x \leq x_B(t) \\
0 \quad & \text{if} \quad x_B(t) \leq x
\end{cases}\end{split}
\end{equation}

\begin{equation}
\begin{split}u(x,t) = \begin{cases}
0 \quad & \text{if} \quad x \leq x_A(t) \\
\dfrac{2}{3} \left( \dfrac{x-x_0}{t} + \sqrt{gh_l} \right) \quad & \text{if} \quad x_A(t) \leq x \leq x_B(t)\\
0 \quad & \text{if} \quad x_B(t) \leq x
\end{cases}\end{split}
\end{equation}

\begin{equation}
\begin{split}\begin{cases}
x_A(t) = x_0 - t \sqrt{g h_l} \\
x_B(t) = x_0 + 2t \sqrt{gh_l} \\
\end{cases}\end{split}
\end{equation}

\subsubsection{Wet case: Stoker's solution}

\begin{equation}
\begin{split}h(x,t) = \begin{cases}
h_l \quad & \text{if} \quad x \leq x_A(t) \\
\dfrac{4}{9g} \left( \sqrt{gh_l} - \dfrac{x-x_0}{2t} \right)^2 \quad & \text{if} \quad x_A(t) \leq x \leq x_B(t) \\
\dfrac{c_m^2}{9} \quad & \text{if} \quad x_B(t) \leq x \leq x_C(t) \\
h_r \quad & \text{if} \quad x_C(t) \leq x
\end{cases}\end{split}
\end{equation}

\begin{equation}
\begin{split}u(x,t) = \begin{cases}
0 \quad & \text{if} \quad x \leq x_A(t)\\
\dfrac{2}{3} \left( \dfrac{x-x_0}{t} + \sqrt{gh_l} \right) \quad & \text{if} \quad x_A(t) \leq x \leq x_B(t) \\
2 \left( \sqrt{gh_l} - c_m\right) \quad & \text{if} \quad x_B(t) \leq x \leq x_C(t)\\
0 \quad & \text{if} \quad x_C(t) \leq x
\end{cases}\end{split}
\end{equation}

\begin{equation}
\text{with :} \quad \begin{split}\begin{cases}
x_A(t) = x_0 - t \sqrt{g h_l} \\
x_B(t) = x_0 + t \left( 2 \sqrt{gh_l} - 3c_m \right) \\
x_C(t) = x_0 + t \left( \dfrac{2c_m^2 \left( \sqrt{gh_l} - c_m \right)}{c_m^2-g h_r} \right)
\end{cases}\end{split}
\end{equation}

$c_m$ being solution of $-8g h_r c_m^2 (\sqrt{gh_l}-c_m)^2+(c_m^2-gh_r)^2(c_m^2+gh-r)$.

\subsection{Boundary conditions}
Boundary conditions are defined by no slip walls on the entry and channel sides
and torrential outflow on the outlet boundary with free water depth and velocity.

\subsection{Physical parameters}

The molecular viscosity is set as constant and equal to 0~m$^2$/s
(\telkey{VELOCITY DIFFUSIVITY} = 0.) and no friction is set to the bottom.

\subsection{Numerical parameters}

For this test case, duration is set to 2.5~s for the Ritter case and
1.5~s for the Stocker case.
Several numerical schemes of advection for velocities are confronted.
The solver used is the conjugate gradient with an accuracy of 10$^{-8}$.
The parameters specific to each case are summed up in
Table~\ref{tab:dambreak:cases}.
\begin{table}[H]
  \resizebox{\textwidth}{!}{%
    \begin{tabular}{|c|c|c|c|c|}
      \hline Case & Name & Equations & Advection scheme for velocities & Time-step / Desired Courant number \\
      \hline 1 & CHAR & Saint-Venant FE & Characteristics & 0.0025~s / - \\
      \hline 2 & NERD & Saint-Venant FE & Edge-based N-scheme & 0.0025~s / - \\
      \hline 3 & ERIA & Saint-Venant FE & ERIA scheme & 0.0025~s / - \\
      \hline 4 & LIPS & Saint-Venant FE & LIPS scheme & 0.0025~s / - \\
      \hline 5 & HLLC & Saint-Venant FV & HLLC order 1 & - /0.8 \\
      \hline 6 & KIN1 & Saint-Venant FV & Kinetic order 1 & - /0.8 \\
      \hline 7 & KIN2 & Saint-Venant FV & Kinetic order 2 & - /0.8 \\
      \hline
    \end{tabular}
  }
  \caption{List of the simulation parameters used for the seven cases tested in the Ritter and Stokes cases.}
  \label{tab:dambreak:cases}
\end{table}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Results for Ritter}


\subsection{Computation time}

Simulation times for each of these cases with sequential and parallel runs
(using 4 processors) are shown in Figure~\ref{fig:ritter:cputime}
\footnote{Keep in mind that these times
are specific to the validation run and the type of processors that were used for this purpose.}.

\begin{figure}[H]
  \centering
  \includegraphicsmaybe{[width=0.9\textwidth]}{../img/t2d_ritter_cpu_times.png}
  \caption{CPU times.}\label{fig:ritter:cputime}
\end{figure}

\subsection{First observation}

Figure \ref{fig:ritter:firstobs1d} illustrates the result obtained for water
depth and velocity after 0.5~s of simulation with the Kinetic finite volume scheme.
The rarefaction wave is clearly visible and well
captured as well as the moving wet/dry transition.
In Figures \ref{fig:ritter:Hfirstobs1d} and  \ref{fig:ritter:Ufirstobs1d},
water depth and velocity are presented in the 2D plane.

\begin{figure}[H]
\begin{minipage}[t]{0.5\textwidth}
 \centering
 \includegraphicsmaybe{[width=\textwidth]}{../img/t2d_ritter_kin1_depth_firstobs100.png}
\end{minipage}
\begin{minipage}[t]{0.5\textwidth}
 \centering
 \includegraphicsmaybe{[width=\textwidth]}{../img/t2d_ritter_kin1_vel_firstobs100.png}
\end{minipage}
  \caption{Water depth and velocity with the Kinetic scheme.}
  \label{fig:ritter:firstobs1d}
\end{figure}

\begin{figure}[H]
 \centering
 \includegraphicsmaybe{[width=\textwidth]}{../img/t2d_ritter_kin1_depth2d_firstobs100.png}
  \caption{Water depth in 2D with the Kinetic scheme.}
  \label{fig:ritter:Hfirstobs1d}
\end{figure}

\begin{figure}[H]
 \centering
 \includegraphicsmaybe{[width=\textwidth]}{../img/t2d_ritter_kin1_vel2d_firstobs100.png}
  \caption{Velocity norm in 2D with the Kinetic scheme.}
  \label{fig:ritter:Ufirstobs1d}
\end{figure}

\subsection{Comparison of schemes}

The results obtained with the different numerical schemes are compared to the
analytical solution for both water depth and velocity.
Comparisons are shown in Figure \ref{fig:ritter:comparison}.

\begin{figure}[H]
\begin{minipage}[t]{0.5\textwidth}
 \centering
 \includegraphicsmaybe{[width=\textwidth]}{../img/t2d_ritter_H_schemes_comparison_50.png}
\end{minipage}
\begin{minipage}[t]{0.5\textwidth}
 \centering
 \includegraphicsmaybe{[width=\textwidth]}{../img/t2d_ritter_U_schemes_comparison_50.png}
\end{minipage}
\begin{minipage}[t]{0.5\textwidth}
 \centering
 \includegraphicsmaybe{[width=\textwidth]}{../img/t2d_ritter_H_schemes_comparison_100.png}
\end{minipage}
\begin{minipage}[t]{0.5\textwidth}
 \centering
 \includegraphicsmaybe{[width=\textwidth]}{../img/t2d_ritter_U_schemes_comparison_100.png}
\end{minipage}
\begin{minipage}[t]{0.5\textwidth}
 \centering
 \includegraphicsmaybe{[width=\textwidth]}{../img/t2d_ritter_H_schemes_comparison_150.png}
\end{minipage}
\begin{minipage}[t]{0.5\textwidth}
 \centering
 \includegraphicsmaybe{[width=\textwidth]}{../img/t2d_ritter_U_schemes_comparison_150.png}
\end{minipage}
  \caption{Comparison of water depth and velocity with analytical solution.}
  \label{fig:ritter:comparison}
\end{figure}

Kinetic and HLLC schemes are able to track the rarefaction wave with a good
precision and are capable of handling wet/dry transition smoothly.
NERD, ERIA and LIPS are also able to track the rarefaction wave schemes
but NERD and ERIA exhibit oscillations on the wet/dry transition.
Finally characteristics are not capable to reproduce the analytical solution.

\subsection{Accuracy}

For a more quantitative comparison of schemes, the $L^1$, $L^2$ and $L^\infty$
error norms of the water depth and velocity are calculated at each time step
for each scheme.
$L^2$ errors time series and time integrated $L^1$, $L^2$ and $L^\infty$ errors
are presented in Figures \ref{fig:ritter:ErrNumH}, \ref{fig:ritter:ErrNumU} and
\ref{fig:ritter:ErrNumV} for $H$, $U$ and $V$ respectively.

\begin{figure}[H]
\begin{minipage}[t]{0.45\textwidth}
 \centering
 \includegraphicsmaybe{[width=\textwidth]}{../img/t2d_ritter_error_L2_H.png}
\end{minipage}
\begin{minipage}[t]{0.55\textwidth}
 \centering
 \includegraphicsmaybe{[width=\textwidth]}{../img/t2d_ritter_errors_timeintegral_H.png}
\end{minipage}
  \caption{Error on $H$: timeseries (left) and integrated over time (right).}
  \label{fig:ritter:ErrNumH}
\end{figure}

\begin{figure}[H]
\begin{minipage}[t]{0.45\textwidth}
 \centering
 \includegraphicsmaybe{[width=\textwidth]}{../img/t2d_ritter_error_L2_U.png}
\end{minipage}
\begin{minipage}[t]{0.55\textwidth}
 \centering
 \includegraphicsmaybe{[width=\textwidth]}{../img/t2d_ritter_errors_timeintegral_U.png}
\end{minipage}
  \caption{Error on $U$: timeseries (left) and integrated over time (right).}
  \label{fig:ritter:ErrNumU}
\end{figure}

\begin{figure}[H]
\begin{minipage}[t]{0.45\textwidth}
 \centering
 \includegraphicsmaybe{[width=\textwidth]}{../img/t2d_ritter_error_L2_V.png}
\end{minipage}
\begin{minipage}[t]{0.55\textwidth}
 \centering
 \includegraphicsmaybe{[width=\textwidth]}{../img/t2d_ritter_errors_timeintegral_V.png}
\end{minipage}
  \caption{Error on $V$: timeseries (left) and integrated over time (right).}
  \label{fig:ritter:ErrNumV}
\end{figure}

For water depth, first order kinetic scheme, HLLC, NERD, LIPS and ERIA exhibit
similar errors but kinetic and HLLC are more precise on velocity.
Finally the time integrated errors show that Kinetic schemes and HLLC are the
most precise schemes, especially the second order kinetic scheme.
This is to be put in perspective with CPU time presented previously
in Figure \ref{fig:ritter:cputime}.

\subsection{Positivity of the water depth}

The minimum values of the water depth are checked during the whole simulation.
Results are shown in Figure \ref{t2d:ritter:minmax}.

\begin{figure}[H]
\centering
\includegraphicsmaybe{[width=0.8\textwidth]}{../img/t2d_ritter_minT.png}
\caption{Minimum values of water depths for the Ritter test case.}
\label{t2d:ritter:minmax}
\end{figure}

No negative values are recorded, which shows that the positivity is fulfilled.
In the case of finite volume schemes, positivity is ensured without additionnal
treatment.
With finite element schemes, the positivity is ensured with a treatment of
negative depths during simulation (\telkey{TREATMENT OF NEGATIVE DEPTHS} = 2
for characteristics, NERD and LIPS and 3 for ERIA scheme).

\subsection{Mass balance}

Mass conservation can be checked by calculating the mass in the domain during
time.
The lost mass is calculated as $M_{initial} - M_{final}$.
The evolution of mass for each of the schemes is shown in
Figure~\ref{fig:ritter:VoLTime}.

\begin{figure}[H]
\centering
  \includegraphicsmaybe{[width=0.75\textwidth]}{../img/t2d_ritter_mass_balance.png}
  \caption{Mass loss for the tested schemes on the Ritter test case.}
\label{fig:ritter:VoLTime}
\end{figure}

\subsection{Energy balance}

In this section, the evolution of the mean energy is checked.
In fact, for the Saint-Venant equations, the quantity
$h \frac{||U||^2}{2} + g \frac{h^2}{2}$, called mean or integrated energy, is
conserved, where $||U||$ is the Saint-Venant depth averaged velocity magnitude.
%\begin{equation}
%  ||U||^2 = \left( \left(\frac{1}{h} \int_0^h u~dz\right) \overrightarrow{e_x} \right)^2 + \left( \left(\frac{1}{h} \int_0^h v~dz\right)  \overrightarrow{e_y} \right)^2
%\end{equation}
%Where $u$ and $v$ are the horizontal components of the 3D velocity. \\
The following quantities are studied:
\begin{itemize}
\item Integrated potential energy
  \textbf{$E_p =\int\int_{\Omega_{xy}}\rho_{water} g \frac{h^2}{2} dxdy$} where
  $\Omega_{xy}$ is the $2D$ domain of simulation: Figure~\ref{fig:ritter:Ep},
\item Integrated kinetic energy
  \textbf{$E_c =\int\int_{\Omega_{xy}} \rho_{water} h \frac{||U||^2}{2} dxdy$}:
  Figure~\ref{fig:ritter:Ec},
\item Integrated mechanical energy \textbf{$E_m = E_p + E_c$}:
  Figure~\ref{fig:ritter:Em}.
\end{itemize}

\begin{figure}[H]
\centering
  \includegraphicsmaybe{[width=0.75\textwidth]}{../img/t2d_ritter_potential_energy.png}
  \caption{Evolution of the potential energy for the tested schemes on the Ritter test case.}
\label{fig:ritter:Ep}
\end{figure}

\begin{figure}[H]
\centering
  \includegraphicsmaybe{[width=0.75\textwidth]}{../img/t2d_ritter_kinetic_energy.png}
  \caption{Evolution of the kinetic energy for the tested schemes on the Ritter test case.}
\label{fig:ritter:Ec}
\end{figure}

\begin{figure}[H]
\centering
  \includegraphicsmaybe{[width=0.75\textwidth]}{../img/t2d_ritter_total_energy.png}
  \caption{Evolution of the mechanical energy for the tested schemes on the Ritter test case.}
\label{fig:ritter:Em}
\end{figure}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\newpage

\section{Results for Stoker}

\subsection{Computation time}

Simulation times for each of these cases with sequential and parallel runs
(using 4 processors) are shown in Figure~\ref{fig:stoker:cputime}
\footnote{Keep in mind that these times
are specific to the validation run and the type of processors that were used for this purpose.}.

\begin{figure}[h!]
  \centering
  \includegraphicsmaybe{[width=0.8\textwidth]}{../img/t2d_stoker_cpu_times.png}
  \caption{CPU times.}\label{fig:stoker:cputime}
\end{figure}

\subsection{First observation}

Figure \ref{fig:ritter:firstobs1d} illustrates the result obtained for water
depth and velocity after 0.5~s of simulation with the Kinetic finite volume scheme.
The left going rarefaction wave and the right going shock are
clearly visible and well captured by the Kinetic scheme.

\begin{figure}[H]
\begin{minipage}[t]{0.5\textwidth}
 \centering
 \includegraphicsmaybe{[width=\textwidth]}{../img/t2d_stoker_kin1_depth_firstobs100.png}
\end{minipage}
\begin{minipage}[t]{0.5\textwidth}
 \centering
 \includegraphicsmaybe{[width=\textwidth]}{../img/t2d_stoker_kin1_vel_firstobs100.png}
\end{minipage}
  \caption{Water depth and velocity with the Kinetic scheme.}
  \label{fig:stoker:firstobs1d}
\end{figure}

\subsection{Comparison of schemes}

The results obtained with the different numerical schemes are compared to the
analytical solution for both water depth and velocity.
Comparisons are shown in Figure \ref{fig:stoker:comparison}.

\begin{figure}[H]
\begin{minipage}[t]{0.5\textwidth}
 \centering
 \includegraphicsmaybe{[width=\textwidth]}{../img/t2d_stoker_H_schemes_comparison_50.png}
\end{minipage}
\begin{minipage}[t]{0.5\textwidth}
 \centering
 \includegraphicsmaybe{[width=\textwidth]}{../img/t2d_stoker_U_schemes_comparison_50.png}
\end{minipage}
\begin{minipage}[t]{0.5\textwidth}
 \centering
 \includegraphicsmaybe{[width=\textwidth]}{../img/t2d_stoker_H_schemes_comparison_100.png}
\end{minipage}
\begin{minipage}[t]{0.5\textwidth}
 \centering
 \includegraphicsmaybe{[width=\textwidth]}{../img/t2d_stoker_U_schemes_comparison_100.png}
\end{minipage}
\begin{minipage}[t]{0.5\textwidth}
 \centering
 \includegraphicsmaybe{[width=\textwidth]}{../img/t2d_stoker_H_schemes_comparison_150.png}
\end{minipage}
\begin{minipage}[t]{0.5\textwidth}
 \centering
 \includegraphicsmaybe{[width=\textwidth]}{../img/t2d_stoker_U_schemes_comparison_150.png}
\end{minipage}
  \caption{Comparison of water depth and velocity with analytical solution.}
  \label{fig:stoker:comparison}
\end{figure}

Kinetic and HLLC schemes are able to track the rarefaction wave and the shock
with a good precision.
Characteristics, NERD, ERIA and LIPS schemes exhibit numerical oscillations
on the shock.
The worst results are obtained with the characteristics which are not capable of
reproducing the analytical solution both in terms of water levels and velocity.

\subsection{Accuracy}

For a more quantitative comparison of schemes, the $L^1$, $L^2$ and $L^\infty$
error norms of the water depth and velocity are calculated at each time step for
each scheme.
$L^2$ errors time series and time integrated $L^1$, $L^2$ and $L^\infty$ errors
are presented in Figures \ref{fig:stoker:ErrNumH}, \ref{fig:stoker:ErrNumU} and
\ref{fig:stoker:ErrNumV} for $H$, $U$ and $V$ respectively.

\begin{figure}[H]
\begin{minipage}[t]{0.45\textwidth}
 \centering
 \includegraphicsmaybe{[width=\textwidth]}{../img/t2d_stoker_error_L2_H.png}
\end{minipage}
\begin{minipage}[t]{0.55\textwidth}
 \centering
 \includegraphicsmaybe{[width=\textwidth]}{../img/t2d_stoker_errors_timeintegral_H.png}
\end{minipage}
  \caption{Error on $H$: timeseries (left) and integrated over time (right).}
  \label{fig:stoker:ErrNumH}
\end{figure}

\begin{figure}[H]
\begin{minipage}[t]{0.45\textwidth}
 \centering
 \includegraphicsmaybe{[width=\textwidth]}{../img/t2d_stoker_error_L2_U.png}
\end{minipage}
\begin{minipage}[t]{0.55\textwidth}
 \centering
 \includegraphicsmaybe{[width=\textwidth]}{../img/t2d_stoker_errors_timeintegral_U.png}
\end{minipage}
  \caption{Error on $U$: timeseries (left) and integrated over time (right).}
  \label{fig:stoker:ErrNumU}
\end{figure}

\begin{figure}[H]
\begin{minipage}[t]{0.45\textwidth}
 \centering
 \includegraphicsmaybe{[width=\textwidth]}{../img/t2d_stoker_error_L2_V.png}
\end{minipage}
\begin{minipage}[t]{0.55\textwidth}
 \centering
 \includegraphicsmaybe{[width=\textwidth]}{../img/t2d_stoker_errors_timeintegral_V.png}
\end{minipage}
  \caption{Error on $V$: timeseries (left) and integrated over time (right).}
  \label{fig:stoker:ErrNumV}
\end{figure}

\subsection{Positivity of the water depth}

The minimum values of the water depth are checked during the whole simulation.
Results are shown in Figure \ref{t2d:stoker:minmax}.

\begin{figure}[H]
\centering
\includegraphicsmaybe{[width=0.8\textwidth]}{../img/t2d_stoker_minT.png}
\caption{Minimum values of water depths for the Stoker test case.}
\label{t2d:stoker:minmax}
\end{figure}

No negative values are recorded, which shows that the positivity is fulfilled.
In the case of finite volume schemes, positivity is ensured without additionnal
treatment.
With finite element schemes the positivity is ensured with a treatment of
negative depths during simulation (\telkey{TREATMENT OF NEGATIVE DEPTHS} = 2 for
characteristics, NERD and LIPS and 3 for ERIA scheme).

\subsection{Mass balance}

Mass conservation can be checked by calculating the mass in the domain during time.
The lost mass is calculated as $M_{initial} - M_{final}$.
The evolution of mass for each of the schemes is shown in
Figure~\ref{fig:stoker:VoLTime}.

\begin{figure}[H]
\centering
  \includegraphicsmaybe{[width=0.75\textwidth]}{../img/t2d_stoker_mass_balance.png}
  \caption{Mass loss for the tested schemes on the Stoker test case.}
\label{fig:stoker:VoLTime}
\end{figure}

\subsection{Energy balance}

In this section, the evolution of the mean energy is checked.
In fact, for the Saint-Venant equations, 
the quantity $h \frac{||U||^2}{2} + g \frac{h^2}{2}$, called mean or integrated
energy, is conserved,
where $||U||$ is the Saint-Venant depth averaged velocity magnitude.
%\begin{equation}
%  ||U||^2 = \left( \left(\frac{1}{h} \int_0^h u~dz\right) \overrightarrow{e_x} \right)^2 + \left( \left(\frac{1}{h} \int_0^h v~dz\right)  \overrightarrow{e_y} \right)^2
%\end{equation}
%Where $u$ and $v$ are the horizontal components of the 3D velocity. \\
The following quantities are studied:
\begin{itemize}
\item Integrated potential energy
  \textbf{$E_p =\int\int_{\Omega_{xy}}\rho_{water} g \frac{h^2}{2} dxdy$} where
  $\Omega_{xy}$ is the $2D$ domain of simulation: Figure~\ref{fig:stoker:Ep},
\item Integrated kinetic energy
  \textbf{$E_c =\int\int_{\Omega_{xy}} \rho_{water} h \frac{||U||^2}{2} dxdy$}:
  Figure~\ref{fig:stoker:Ec},
\item Integrated mechanical energy \textbf{$E_m = E_p + E_c$}:
  Figure~\ref{fig:stoker:Em}.
\end{itemize}

\begin{figure}[H]
\centering
  \includegraphicsmaybe{[width=0.75\textwidth]}{../img/t2d_stoker_potential_energy.png}
  \caption{Evolution of the potential energy for the tested schemes on the Stoker test case.}
\label{fig:stoker:Ep}
\end{figure}

\begin{figure}[H]
\centering
  \includegraphicsmaybe{[width=0.75\textwidth]}{../img/t2d_stoker_kinetic_energy.png}
  \caption{Evolution of the kinetic energy for the tested schemes on the Stoker test case.}
\label{fig:stoker:Ec}
\end{figure}

\begin{figure}[H]
\centering
  \includegraphicsmaybe{[width=0.75\textwidth]}{../img/t2d_stoker_total_energy.png}
  \caption{Evolution of the mechanical energy for the tested schemes on the Stoker test case.}
\label{fig:stoker:Em}
\end{figure}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Conclusions}

With the tested dambreak cases, we have shown that \telemac{2D} is able to
reproduce the propagation of rarefaction and shock waves in good accordance with
analytical solutions.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Reference}

\begin{itemize}
\item J. J. Stoker, Water Waves: The Mathematical Theory with Applications, Interscience, 1957.
\item Ritter, A. (1892). "Die Fortpflanzung der Wasserwellen." Vereine Deutscher Ingenieure Zeitswchrift, Vol. 36, No. 2, 33,
13 Aug., pp. 947-954 (in German).
\end{itemize}
