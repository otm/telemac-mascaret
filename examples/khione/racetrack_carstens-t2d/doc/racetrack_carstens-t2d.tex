\renewcommand{\labelitemi}{$\triangleright$}

\chapter{Validation with Carstens experiments (1966)}

\section{Purpose}

This test case consists in reproducing the experiments carried out by Carstens in 1966 \citep{carstens_1966} 
to demonstrate that \khione is capable of correctly fit with supercooling experimental data.
Two experiments with two different cooling rates are reproduced with \khione 
with both the MSC\footnote{MSC : multiple-size-class} and SSC\footnote{SSC : single-size-class} suspended frazil ice models.

\section{Description}

\subsection{Geometry, mesh and bathymetry}

Carstens experiments [3] were conducted in a recirculating oval flume of about $6$ m long, $30$ cm deep and $20$ cm
wide. Water depth was set to $20$cm and temperature was recorded at approximately $5$ to $10$ cm deep with
a mercury thermometer marked to $0.01$ $^\circ C$.

\begin{figure}[H]
    \begin{center}
        \includegraphicsmaybe{[width=0.95\textwidth]}{../img/mesh.png}
    \end{center}
    \caption{Domain mesh}
    \label{fig:growth_mesh}
\end{figure}

\subsection{Initial conditions}

A momentum source term is introduced to simulate the propeller. The source
term is adjusted in order to reach an hydrodynamic steady state with a mean flow velocity of $0.5$ m.s$^{-1}$.
Water temperature is initially set at $0^\circ C$ as well as the initial frazil volume fraction is set to $0$.

\begin{figure}[H]
    \begin{center}
        \includegraphicsmaybe{[width=0.99\textwidth]}{../img/racetrack2d_-1.png}
    \end{center}
    \caption{Computed hydrodynamic steady state during the experiments. The black box represent the location of the propeller 
momentum source term and the red dot is where the extraction of frazil ice volume fraction and temperature are done.}
    \label{fig:hydro}
\end{figure}

\subsection{Boundary conditions}

The solid boundaries are on each side of the racetrack, considered as solid walls with perfect slip conditions.

\subsection{Atmospheric drivers}

For this test case, linear heat exchanges are used i.e. within the \khione steering file:
\begin{itemize}
	\item\textit{ATMOSPHERE-WATER EXCHANGE MODEL = 0}
\end{itemize}
With this model, the net heat flux received by water trought the water/air interface is expressed as:
\begin{equation}
\phi^* = \phi_R + \alpha + \beta (T_{air} - T_{water})
\end{equation}
where $\phi_R$ the net solar radiation flux, $\alpha$ and $\beta$ are two coefficient. 
To match the exact constant cooling rate as in the experiments (cf. table \ref{table:physical_parameters}), 
$I_{so}$ and $\beta$ are set to zero within the \khione steering file.
Only $\alpha$ is set depending on the coling rate of the experiment (1 or 2) and the water depth i.e.
\textit{WATER-AIR HEAT EXCHANGE CONSTANT = -280} for the first experiment and
\textit{WATER-AIR HEAT EXCHANGE CONSTANT = -110} for the second experiment.

\subsection{Physical parameters}

The physical parameters of the experiment are summed up in the Table \ref{table:physical_parameters}.
\begin{center}
\begin{tabular}{lcccccc}
\hline
Case & $H$ $(m)$ & $U$ $(m/s)$ & Cooling rate $(W/m^3)$ & $S$ $(ppt)$ & $k$ $(m^2/s^2)$ & $\varepsilon$ $(m^2/s^3)$ \\
\hline \hline
Carstens I & $0.2$ & $0.5$ & $1400$ & $0$ & $9.6 \cdot 10^{-4}$ & $1.2 \cdot 10^{-3}$ \\
\hline
Carstens II & $0.2$ & $0.5$ & $550$ & $0$ & $4.8 \cdot 10^{-4}$ & $3.8 \cdot 10^{-4}$ \\
\hline
\end{tabular}
\captionof{table}{Physical parameters of the experiments.}
\label{table:physical_parameters}
\end{center}

Physical properties related to frazil growth rate are defined in the \khione steering file. 
For both the SSC and MSC models, the turbulent parameters are set to constants
and defined with the keywords:
\begin{itemize}
	\item\textit{MODEL FOR ESTIMATION OF TURBULENCE PARAMETERS = 0}
	\item\textit{CONSTANT TURBULENCE PARAMETERS = KT, EPS, ALPHA}
\end{itemize}
where \textit{KT, EPS, ALPHA} depends on the experiment and
stand for turbulent kinetic energy ($k$),
turbulent dissipation rate ($\varepsilon$) and 
turbulent instensity ($\alpha_t$) respectively.
The Nusselt number is then computed within \khione depending on these parameters, 
with \textit{MODEL FOR THE NUSSELT NUMBER = 2}.

\subsubsection{SSC model parameters}
With the SSC model, frazil ice related parameters are set to the following
for both experiments:
\begin{itemize}
	\item\textit{NUMBER OF CLASSES FOR SUSPENDED FRAZIL ICE = 1}
	\item\textit{FRAZIL CRYSTALS RADIUS = 4.1E-4}
	\item\textit{FRAZIL CRYSTALS DIAMETER THICKNESS RATIO = 10.}
\end{itemize}

The initial seeding is defined with a minimum threshold for the number of crystals per unit volume.
\begin{itemize}
  \item\textit{MODEL FOR FRAZIL SEEDING = 1}
  \item\textit{MINIMUM NUMBER OF FRAZIL CRYSTALS = 7.1586E4}
\end{itemize}
The parameter \textit{MINIMUM NUMBER OF FRAZIL CRYSTALS} is set to default values.

\subsubsection{MSC model parameters}
With the MSC model, frazil ice crystal' sizes are defined as:
\begin{itemize}
	\item\textit{NUMBER OF CLASSES FOR SUSPENDED FRAZIL ICE = 10}
	\item\textit{FRAZIL CRYSTALS RADIUS = 1.E-4 ; 1.2915E-4 ; 1.6681E-4 ; 2.1544E-4 ; 2.7826E-4;
3.5938E-4 ; 4.6416E-4 ; 5.9948E-4 ; 7.7426E-4 ; 1.E-3}
	\item\textit{FRAZIL CRYSTALS DIAMETER THICKNESS RATIO = 10.}
\end{itemize}

Similarly to the SSC system, the initial seeding is defined 
with a minimum threshold for the number of crystals.
The parameter \textit{MINIMUM NUMBER OF FRAZIL CRYSTALS}
is defined in the table \ref{table:calibration}.

\begin{center}
\begin{tabular}{lccc}
\hline
Case & $\alpha_{floc}$ $(s^{-1})$ & $n_{max}$ & $N_0$ $(m^{-3})$ \\
\hline \hline
Carstens I & $10^{-4}$ & $8. \times 10^{6}$ & $5.88717 \times 10^{5}$  \\
\hline
Carstens II & $10^{-4}$ & $4. \times 10^{6}$ & $4.86332 \times 10^{5}$  \\
\hline
\end{tabular}
\captionof{table}{Calibrated parameters.}
\label{table:calibration}
\end{center}

Two new processes are taken into account compared to the SSC, 
which are the secondary nucleation and the flocculation processes for which
the parameters are defined as follows:
\begin{itemize}
	\item\textit{MODEL FOR THE BUOYANCY VELOCITY = 2}
	\item\textit{MODEL FOR THE SECONDARY NUCLEATION = 2}
	\item\textit{SECONDARY NUCLEATION NMAX PARAMETER = see table \ref{table:calibration}}
	\item\textit{MODEL FOR THE FLOCCULATION AND BREAKUP = 1}
	\item\textit{FLOCCULATION AFLOC PARAMETER = see table \ref{table:calibration}}
\end{itemize}
The parameters $n_{max}$ and $a_{floc}$ parameters are taken as calibration parameters 
that are tuned to fit with the experimental data.


\subsection{Numerical parameters}

The time step is set to $0.05$s and the simulation time is set to $600$s.

\section{Results}

Total frazil volume fraction and temperature are plotted against time $t^*=t/t_c$ 
where the characteristic time $t_c$ corresponds to the
moment when $90$ percent of the maximum temperature depression is recovered.
Frazil volume fraction is computed as $C^*=C/C(t_c)$.

\subsubsection{SSC model:}

The figure \ref{fig:carstens_monoclass}
show the temperature and the frazil volume fraction results with the SSC 
model against Carstens I and II experimental data.

\begin{figure}[H]
    \begin{center}
        \includegraphicsmaybe{[width=0.49\textwidth]}{../img/carstens1_monoclass.png}
        \includegraphicsmaybe{[width=0.49\textwidth]}{../img/carstens2_monoclass.png}
    \end{center}
    \caption{Simulated temperature and total frazil volume fraction with comparison to Carstens experiments I (left) and II (right).}
    \label{fig:carstens_monoclass}
\end{figure}

\subsubsection{MSC model:}

The figure \ref{fig:carstens_multiclass}
show the temperature and the total frazil volume fraction results with the MSC
model against Carstens I and II experimental data.

\begin{figure}[H]
    \begin{center}
        \includegraphicsmaybe{[width=0.49\textwidth]}{../img/carstens1_multiclass.png}
        \includegraphicsmaybe{[width=0.49\textwidth]}{../img/carstens2_multiclass.png}
    \end{center}
    \caption{Simulated temperature and total frazil volume fraction with comparison to Carstens experiments I (left) and II (right).}
    \label{fig:carstens_multiclass}
\end{figure}

\section{Conclusions}

Both sinlge- and multiple-size-class frazil ice models are able to reproduce the supercooling sequence with adequate calibration of input parameters.

\renewcommand{\labelitemi}{\textbullet}
