\chapter{Test case: flume with a slope}

\section{Purpose}
This test case demonstrates the ability of \khione to model water temperature in
the presence of tidal flats.
A linear thermal budget formulation is used with an air temperature set to -10 °C.

The purpose of this case test is to verify the equation \ref{eq:model:temp_tracer}
is well solved in the presence of tidal flats.

\begin{equation}
  \frac{\partial }{\partial t} \left[T\right]+\nabla.\left[\vec{u}T\right]=
  \dfrac{1}{h}\nabla.(h\nu_{t,T}\nabla \left[T\right])+\frac{\phi_s }{h\rho c_{p}}
\label{eq:model:temp_tracer}
\end{equation}
where
\begin{itemize}
	\item $c_{p}= 4.1855 \times 10^3$ J.kg$^{-1}$.K$^{-1}$ is the specific heat of water,
	\item $\phi_s$ is the net heat flux at the free surface in W.m$^{-2}$.
\end{itemize}

\section{Description}

\subsection{Geometry, mesh and bathymetry}

Two domains are considered herein.
The first domain is a long flume with a slope, boxed within ($x$ = 0, $y$ = 0) and
(x $=$ 16., y = 0.25).
The channel has a slope of 1:2.5 between the elevation 12 m (upstream) and 16 m
(downstream) (upper plot in figure \ref{fig:flume:mesh}).
The second one is cropped at $x$ = 15~m (upper plot in figure \ref{fig:flume:mesh}).

\begin{figure}[H]
 \centering
 \includegraphicsmaybe{[width=0.85\textwidth]}{../img/mesh_full.png}
 \includegraphicsmaybe{[width=0.85\textwidth]}{../img/mesh_crop.png}
 \caption{Domain mesh.}\label{fig:flume:mesh}
\end{figure}

The test cases colormap bathymetry is presentend in figure  \ref{fig:flume:bathy}.
The bathymetry presents small irregularities as shown in \ref{fig:flume:initial}
to verify the effect of these irregularities on water temperature.

\begin{figure}
 \centering
 \includegraphicsmaybe{[width=0.85\textwidth]}{../img/bottom_full.png}
 \includegraphicsmaybe{[width=0.85\textwidth]}{../img/bottom_crop.png}
 \caption{Flumes bathymetry.}\label{fig:flume:bathy}
\end{figure}

\subsubsection{Initial and boundary conditions}

Initial elevation and water temperature are set to 9.0 m and 1.~°C,
respectively, as shown in figure \ref{fig:flume:initial}.

\begin{figure}[H]
 \centering
 \includegraphicsmaybe{[width=0.85\textwidth]}{../img/profile_elevation_full.png}
 \includegraphicsmaybe{[width=0.85\textwidth]}{../img/profile_elevation_crop.png}
 \caption{Initialization.}\label{fig:flume:initial}
\end{figure}

There are three solid boundaries and 1 open boundary.
The solid boundaries are on either side of the length of the flume and downstream
at $x$ = 16.~m (resp. 15.~m), considered as solid walls with perfect slip
conditions.
The open boundary upstream the flume at at $x$ = 0~m is set to sinusoidal wave
with a water temperature set to 1.~°C.
The free surface is described by the equation \ref{eq:SL_inlet}.
\begin{equation}
    H_{upstream}(t) = 9.0 + 2.\sin{\frac{2\pi}{T_{per}t}}
    \label{eq:SL_inlet}
\end{equation}
with $T_{per}$ being the period set to 150.~s.

\subsubsection{Atmospheric drivers}
For this test case, linear heat exchange model is used.
As a reminder the model writes: 

\begin{equation}
    \Phi = \Phi_R + \alpha + \beta (T_{air}-T_{water})
    \label{eq:linear_model}
\end{equation}
with $\Phi_R$ is the net solar radiation flux taken equal to -100,000~W.m$^{-2}$,
and $\alpha$ and $\beta$ are the calibration coefficients set to $0$.
Hence, the exchange flux is constant through time. 

\subsubsection{Physical and numerical parameters}
Frazil formation is not modeled herein so the initial frazil concentration and
the seeding rate are kept at $0.$.
There is no turbulence model nor bottom friction. 

The time step is set to 0.05~s and simulation duration is set to 800~s
(we advise running the test case for higher durations).
The diffusion coefficient is set to its default value 0.
The advection scheme is the focus of this case test. 

\subsubsection{Results}

Herein, only the advection term of the equation is considered as the diffusion
coefficient is set to 0. and the water elevation at open boundary is modeled as
presented in eq. \ref{eq:SL_inlet}.
Two advection schemes are tested: ``Characteristic'' scheme and
the ``NERD'' scheme.

$$\frac{\partial }{\partial t}\left[T\right]+\nabla.\left[\vec{u} T
  \right]= \dfrac{1}{h}  \nabla .(h \nu_{t,T} \nabla \left[T \right] )+
\frac{\phi_s }{h \rho c_{p}}$$

Figure \ref{fig:flume:res} presents a comparison of the results obtained.
The results indicate that the presence or absence of tidal flats influences the
water temperature profile along the flume.

\begin{figure}[H]
 \centering
 \includegraphicsmaybe{[width=0.85\textwidth]}{../img/profile_T_final_crop.png}
 \includegraphicsmaybe{[width=0.85\textwidth]}{../img/profile_T_final_full.png}
 \caption{Comparison of the evolution of water temperature profiles for the
   characteristic advection scheme (blue line), the NERD scheme (orange line)
   both in the presence of tidal flats (a) and in their absence (b).}
 \label{fig:flume:res}
\end{figure}

\subsubsection{Conclusions}

The case highlights the impact of the choice of numerical parameters on the
water temperature results.
The results suggest that it is more appropriate to use NERD-like schemes rather
than the Characteristics scheme for tracers when using Finite Element method,
especially in areas with shallow water depth.
