
"""
Validation script for ice_cover
"""
import datetime
import matplotlib.dates as mdates
import matplotlib.pyplot as plt
from postel.deco_vnv import decoVNV, decoVNV_1d
from vvytel.vnv_study import AbstractVnvStudy
from execution.telemac_cas import TelemacCas, get_dico
from data_manip.extraction.telemac_file import TelemacFile
from data_manip.computation.datetimes import compute_datetimes


class VnvStudy(AbstractVnvStudy):
    """
    Class for validation
    """

    def _init(self):
        """
        Defines the general parameter
        """
        self.rank = 2
        self.tags = ['telemac2d', 'khione']

    def _pre(self):
        """
        Defining the studies
        """
        #----------------------------------------------------------------------
        # cas 1: full flume FE-char
        cas = TelemacCas('t2d_flume-char.cas', get_dico('telemac2d'))
        self.add_study('vnv1_seq', 'telemac2d', 't2d_flume-char.cas', cas)
        # cas 1 parallel
        cas.set('PARALLEL PROCESSORS', 4)
        self.add_study('vnv1_par', 'telemac2d', 't2d_flume-char_par.cas', cas=cas)
        del cas
        #----------------------------------------------------------------------
        # cas 2: full flume FE-nerd
        cas = TelemacCas('t2d_flume-nerd.cas', get_dico('telemac2d'))
        self.add_study('vnv2_seq', 'telemac2d', 't2d_flume-nerd.cas', cas)
        # cas 2 parallel
        cas.set('PARALLEL PROCESSORS', 4)
        self.add_study('vnv2_par', 'telemac2d', 't2d_flume-nerd_par.cas', cas=cas)
        del cas

        #----------------------------------------------------------------------
        # # cas 3: full flume FV
        # cas = TelemacCas('t2d_flume_FV.cas', get_dico('telemac2d'))
        # self.add_study('vnv3_seq', 'telemac2d', 't2d_flume_FV.cas', cas)
        # # cas 3 parallel
        # #cas.set('PARALLEL PROCESSORS', 4)
        # #self.add_study('vnv3_par', 'telemac2d', 't2d_flume_FV_par.cas', cas=cas)
        # del cas
        #----------------------------------------------------------------------
       
        #----------------------------------------------------------------------
        # cas 4: cropped flume FE-char
        cas = TelemacCas('t2d_flume_crop-char.cas', get_dico('telemac2d'))
        self.add_study('vnv4_seq', 'telemac2d', 't2d_flume_crop-char.cas', cas)
        # cas 4 parallel
        cas.set('PARALLEL PROCESSORS', 4)
        self.add_study('vnv4_par', 'telemac2d', 't2d_flume_crop-char_par.cas', cas=cas)
        del cas
        #----------------------------------------------------------------------
        # cas 5: cropped flume FE-nerd
        cas = TelemacCas('t2d_flume_crop-nerd.cas', get_dico('telemac2d'))
        self.add_study('vnv5_seq', 'telemac2d', 't2d_flume_crop-nerd.cas', cas)
        # cas 5 parallel
        cas.set('PARALLEL PROCESSORS', 4)
        self.add_study('vnv5_par', 'telemac2d', 't2d_flume_crop-nerd_par.cas', cas=cas)
        del cas

        #----------------------------------------------------------------------
        # # cas 6: cropped flume FV
        # cas = TelemacCas('t2d_flume_crop_FV.cas', get_dico('telemac2d'))
        # self.add_study('vnv6_seq', 'telemac2d', 't2d_flume_crop_FV.cas', cas)
        # # cas 6 parallel
        # #cas.set('PARALLEL PROCESSORS', 4)
        # #self.add_study('vnv6_par', 'telemac2d', 't2d_flume_crop_FV_par.cas', cas=cas)
        # del cas

    def _check_results(self):
        """
        Post-treatment processes
        """

        # Comparison with the last time frame of the reference file.
        self.check_epsilons('vnv1_seq:T2DRES',
                            'f2d_flume-char.slf',
                            eps=[4.e-4])

        # Comparison with the last time frame of the reference file.
        self.check_epsilons('vnv1_par:T2DRES',
                            'f2d_flume-char.slf',
                            eps=[2.e-3])

        # Comparison between sequential and parallel run.
        self.check_epsilons('vnv1_seq:T2DRES',
                            'vnv1_par:T2DRES',
                            eps=[2.e-3])

        # Comparison with the last time frame of the reference file.
        self.check_epsilons('vnv2_seq:T2DRES',
                            'f2d_flume-nerd.slf',
                            eps=[2.e-3])

        # Comparison with the last time frame of the reference file.
        self.check_epsilons('vnv2_par:T2DRES',
                            'f2d_flume-nerd.slf',
                            eps=[3.e-3])

        # Comparison between sequential and parallel run.
        self.check_epsilons('vnv2_seq:T2DRES',
                            'vnv2_par:T2DRES',
                            eps=[3.e-3])

        # Comparison with the last time frame of the reference file.
        self.check_epsilons('vnv4_seq:T2DRES',
                            'f2d_flume_crop-char.slf',
                            eps=[7.e-4])

        # Comparison with the last time frame of the reference file.
        self.check_epsilons('vnv4_par:T2DRES',
                            'f2d_flume_crop-char.slf',
                            eps=[5.e-4])

        # Comparison between sequential and parallel run.
        self.check_epsilons('vnv4_seq:T2DRES',
                            'vnv4_par:T2DRES',
                            eps=[6.e-4])

        # Comparison with the last time frame of the reference file.
        self.check_epsilons('vnv5_seq:T2DRES',
                            'f2d_flume_crop-nerd.slf',
                            eps=[5.e-4])

        # Comparison with the last time frame of the reference file.
        self.check_epsilons('vnv5_par:T2DRES',
                            'f2d_flume_crop-nerd.slf',
                            eps=[5.e-4])

        # Comparison between sequential and parallel run.
        self.check_epsilons('vnv5_seq:T2DRES',
                            'vnv5_par:T2DRES',
                            eps=[5.e-4])

    def _post(self):
        """
        Post-treatment processes
        """
        from postel.plot_vnv import vnv_plot2d, vnv_plot1d_polylines


        #======================================================================
        # GET TELEMAC RESULT FILES:
        #
        geom, _ = self.get_study_res('vnv1_seq:T2DGEO', load_bnd=True)
        res, _ = self.get_study_res('vnv1_seq:T2DRES')
        geom_crop, _ = self.get_study_res('vnv4_seq:T2DGEO', load_bnd=True)
        res_crop, _ = self.get_study_res('vnv4_seq:T2DRES')
        # Load all results as a list:
        res_list, res_labels = self.get_study_res(module='T2D', whitelist=['seq'])
        ice_list, ice_labels = self.get_study_res(module='ICE', whitelist=['seq'])

        #======================================================================
        # DESCRIPTION PLOTS:
        #
        #Plotting mesh
        vnv_plot2d(\
            'BOTTOM',
            geom,
            record=0,
            plot_mesh=True,
            annotate_bnd=True,
            filled_contours=False,
            fig_size=(14, 2.5),
            fig_name='img/mesh_full',
            x_label='$x$ $(m)$',
            y_label='$y$ $(m)$',)
        
        vnv_plot2d(\
            'BOTTOM',
            geom_crop,
            record=0,
            plot_mesh=True,
            annotate_bnd=True,
            filled_contours=False,
            fig_size=(14, 2.5),
            fig_name='img/mesh_crop',
            x_label='$x$ $(m)$',
            y_label='$y$ $(m)$',)
        
        # Plotting bottom
        vnv_plot2d(\
            'BOTTOM',
            geom,
            record=0,
            filled_contours=True,
            fig_size=(14, 2.5),
            fig_name='img/bottom_full',
            x_label='$x$ $(m)$',
            y_label='$y$ $(m)$',
            cbar_label='Bottom (m)')
        
        vnv_plot2d(\
            'BOTTOM',
            geom_crop,
            record=0,
            filled_contours=True,
            fig_size=(14, 2.5),
            fig_name='img/bottom_crop',
            x_label='$x$ $(m)$',
            y_label='$y$ $(m)$',
            cbar_label='Bottom (m)')

        # Plotting bottom and elevation profile
        vnv_plot1d_polylines(\
            'FREE SURFACE',
            res,
            legend_labels='free surface',
            record=-1,
            poly=[[0., 0.15], [16., 0.15]],
            poly_number=[100],
            fig_size=(14, 3),
            y_label='$z$ $(m)$',
            x_label='$x$ $(m)$',
            fig_name='img/profile_elevation_full',
            plot_bottom=True)

        vnv_plot1d_polylines(\
            'FREE SURFACE',
            res_crop,
            legend_labels='free surface',
            record=-1,
            poly=[[0., 0.15], [16., 0.15]],
            poly_number=[100],
            fig_size=(14, 3),
            y_label='$z$ $(m)$',
            x_label='$x$ $(m)$',
            fig_name='img/profile_elevation_crop',
            plot_bottom=True)

 
        # 1D profiles:
        #var_plt = ['FREE SURFACE', 'BOTTOM', 'TEMPERATURE']
        #lab_plt = ['water free surface', 'Bottom', 'water temperature']
        var_plt = ['TEMPERATURE']
        lab_plt = ['water temperature']
        records = [-1]
        res_labels_ = ['Full: FE, Char scheme', 'Full: FE, NERD scheme', 'Full: Finite Volume',\
                       'Crop: FE, Char scheme', 'Crop: FE, NERD scheme', 'Crop: Finite Volume']
    
        for record in records:
            vnv_plot1d_polylines(\
                    'TEMPERATURE', res_list[0:2],
                    res_labels_[0:2],
                    record=record,
                    poly=[[0., 0.15], [16., 0.15]],
                    poly_number=[100],
                    xlim=[0.,16.1],   
                    fig_size=(14, 3),
                    y_label='$T(°C)$',
                    x_label='$x$ $(m)$',
                    fig_name='img/profile_T_final_full',
                    plot_bottom=False)
            
        for record in records:
            vnv_plot1d_polylines(\
                    'TEMPERATURE', res_list[2:],
                    res_labels_[3:],
                    record=record,
                    poly=[[0., 0.15], [16., 0.15]],
                    poly_number=[100],
                    xlim=[0.,16.1],   
                    fig_size=(14, 3),
                    y_label='$T(°C)$',
                    x_label='$x$ $(m)$',
                    fig_name='img/profile_T_final_crop',
                    plot_bottom=False)
