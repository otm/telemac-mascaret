\renewcommand{\labelitemi}{$\triangleright$}

\chapter{Border ice formation}

\section{Purpose}
This test case demonstrates that \khione is capable of modeling border ice formation and its interactions with
temperature, frazil and hydrodynamics.
A linear thermal budget formulation is used with an air temperature set to $-15^{\circ} C$. Water is running down a meandering trapezoidal flume.

\section{Description}

\subsection{Geometry, mesh and bathymetry}
The domain is a sinusoïdal flume with a trapezoidal bottom, boxed within $(x=-60, y=-30)$ and $(x=320, y=25)$.
The mesh of the domain was created with a uniform density and with $23960$ elements and $12600$ vertices (cf. figure \ref{fig:mesh}).

\begin{figure}[H]
    \begin{center}
        \includegraphicsmaybe{[width=\textwidth]}{../img/mesh.png}
    \end{center}
    \caption{Domain mesh}
    \label{fig:mesh}
\end{figure}

\subsection{Initial conditions}

A hydrodynamics steady state condition is established first separately to reach a constant and uniform elevation of $2.5m$ and a constant and uniform discharge of $10m^3/s$.
Water temperature is initially set at $0.002^\circ C$ and frazil volume fraction at $10^{-4}$. These values are also used for the upstream boundary condition.

\subsection{Boundary conditions}

There are two solid boundaries and 2 open boundaries.
The solid boundaries are on either side of the length of the flume, considered as solid walls with perfect slip conditions.
A constant discharge of $5.0m^3/s$ is imposed at the upstream and downstream boundaries with a temperature set to $0.002^{\circ} C$.
A first simulation is carried out with for the hydrodynamics only to get to a steady state equilibrium of a constant and uniform elevation of $2.5m$ and a constant and uniform discharge of $5m^3/s$ along the entire length of the flume.

\subsection{Atmospheric drivers}

For this test case, linear heat exchanges are used i.e. within the \khione steering file:
\begin{itemize}
	\item\textit{ATMOSPHERE-WATER EXCHANGE MODEL = 0}
	\item\textit{SOLAR CONSTANT = 0.}
	\item\textit{WATER-AIR HEAT EXCHANGE CONSTANT = -50.}
	\item\textit{WATER-AIR HEAT EXCHANGE COEFFICIENT = 20.}
\end{itemize}
Air temperature is set to a constant of $-15^{\circ}$ during the simulation.

\subsection{Physical parameters}

In order to balance the slope and the discharge through the flume, a Manning friction law is used, with a coefficient of $0.025$.

To activate the simulation of border ice processes, the following keywords are added to the \khione steering file:
\begin{itemize}
	\item\textit{BORDER ICE COVER = YES}
    \item\textit{ICE COVER IMPACT ON HYDRODYNAMIC = YES}
\end{itemize}

Border ice formation is triggered by three conditions. Water surface temperature $T_{ws}$ must be inferior to  $T_f-T_{cr}$, with $T_f$ the fusion temperature of water and $T_{cr}$ an empirical critical temperature defined by $-1.1^{\circ} C$ by default. Aditionnally, local depth averaged velocity must be inferior to a critical velocity $U_{cr}$ defined by $0.07 m/s$ by default. Finally frazil buoyancy velocity $U_b$ must be superior to the local vertical turbulence velocity $U'_z$. $T_{cr}$ and $U_{cr}$ can be modified in the \khione steering file via the following keywords:
\begin{itemize}
	\item\textit{CRITICAL VELOCITY FOR STATIC BORDER ICE = 0.07}
	\item\textit{CRITICAL WATER TEMPERATURE FOR STATIC BORDER ICE = -1.1}
\end{itemize}
The variables $U'_z$ and $T_{ws}$ are determined via empirical relations. $T_{ws}$ depends on the channel width (fixed to $15m$ by default) that can be adjusted via the following keyword in the \khione steering file:
\begin{itemize}
	\item\textit{CHANNEL WIDTH FOR THE COMPUTATION OF SURFACE TEMPERATURE = 25.}
\end{itemize}

\subsection{Numerical parameters}
The time step is set to $1s$ and the number of timestep is set to $1800$ which leads to a simulation time of $30$ minutes.

\section{Results}

On th following figures bathymetry, velocity, temperature, frazil volume fraction, surface ice type and ice cover thickness are plotted respectively. During the simulation, a layer of static ice cover develops on the river banks which have a significant effect on the water temperature as it tends to isulate the river. As a consequence this tends to lower the amount of suspended frasil ice being formed along the river. Note that there is no mass exchange between suspended frazil ice and ice cover in this simulation.

\begin{figure}[H]
\begin{minipage}[t]{\textwidth}
 \centering
 \includegraphicsmaybe{[width=\textwidth]}{../img/bottom.png}
\end{minipage}
\begin{minipage}[t]{\textwidth}
 \centering
 \includegraphicsmaybe{[width=\textwidth]}{../img/U-2d_scalarmap_15min.png}
\end{minipage}
\begin{minipage}[t]{\textwidth}
 \centering
 \includegraphicsmaybe{[width=\textwidth]}{../img/T-2d_scalarmap_15min.png}
\end{minipage}
\begin{minipage}[t]{\textwidth}
 \centering
 \includegraphicsmaybe{[width=\textwidth]}{../img/Cf-2d_scalarmap_15min.png}
\end{minipage}
\begin{minipage}[t]{\textwidth}
 \centering
 \includegraphicsmaybe{[width=\textwidth]}{../img/Ice-2d_scalarmap_15min.png}
\end{minipage}
\begin{minipage}[t]{\textwidth}
 \centering
 \includegraphicsmaybe{[width=\textwidth]}{../img/titot-2d_scalarmap_15min.png}
\end{minipage}
  \caption{Bathymetry, velocity, temperature, frazil volume fraction, surface ice type and ice cover thickness at $t=15$ minutes}\label{fig:res}
\end{figure}

\begin{figure}[H]
\begin{minipage}[t]{\textwidth}
 \centering
 \includegraphicsmaybe{[width=\textwidth]}{../img/bottom.png}
\end{minipage}
\begin{minipage}[t]{\textwidth}
 \centering
 \includegraphicsmaybe{[width=\textwidth]}{../img/U-2d_scalarmap_30min.png}
\end{minipage}
\begin{minipage}[t]{\textwidth}
 \centering
 \includegraphicsmaybe{[width=\textwidth]}{../img/T-2d_scalarmap_30min.png}
\end{minipage}
\begin{minipage}[t]{\textwidth}
 \centering
 \includegraphicsmaybe{[width=\textwidth]}{../img/Cf-2d_scalarmap_30min.png}
\end{minipage}
\begin{minipage}[t]{\textwidth}
 \centering
 \includegraphicsmaybe{[width=\textwidth]}{../img/Ice-2d_scalarmap_15min.png}
\end{minipage}
\begin{minipage}[t]{\textwidth}
 \centering
 \includegraphicsmaybe{[width=\textwidth]}{../img/titot-2d_scalarmap_30min.png}
\end{minipage}
  \caption{Bathymetry, velocity, temperature, frazil volume fraction, surface ice type and ice cover thickness at $t=30$ minutes}\label{fig:res}
\end{figure}

\section{Conclusions}
This test case shows the ability of \khione to model the formation of border ice along a channel. Once border ice is formed, \khione is able to simulate the thermal expansion or decay of the ice cover depending on thermal fluxes on the surface and water temperature. The effect of surface ice has a significant impact on both hydrodynamcis and temperature wich therefore also affect frazil ice formation.

\renewcommand{\labelitemi}{\textbullet}
