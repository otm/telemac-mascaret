\renewcommand{\labelitemi}{$\triangleright$}

\chapter{Supercooling and frazil ice growth in a trapezoidal flume}

\section{Purpose}
This test case demonstrates that \khione is capable of correctly reproduce
supercooling and frazil ice growth in presence of dry/wet interfaces.
Water is running down a trapezoidal flume over $400$ m long at a gentle slope of 5:10,000. A linear thermal budget formulation is used with an air temperature set to $-15^{\circ} C$. At the end of the simulation temperature and frazil ice reach a steady state in the domain.

\section{Description}

\subsection{Geometry, mesh and bathymetry}
The domain is a long trapezoidal flume, boxed within $(x=0, y=0)$ and $(x=400, y=36)$, its length being $400$ m along the x-axis and its width being $36$ m along the y-axis. The channel has a slope of 5:10,000 between the elevation $0.2$ m (upstream) and $0$ m (downstream).

\begin{figure}[H]
    \begin{center}
        \includegraphicsmaybe{[width=\textwidth]}{../img/mesh.png}
    \end{center}
    \caption{Domain mesh}
    \label{fig:mesh}
\end{figure}

\subsection{Initial conditions}

A hydrodynamics steady state condition is established first separately to reach a constant and uniform water depth of $1.75$ m in the middle of the section and a constant and uniform discharge of $30.38$m$^3$/s.
Water temperature is initially set at $0^\circ C$. Initial frazil volume fraction is set to $0$. These values are also used for the upstream boundary condition.

\begin{figure}[H]
    \begin{center}
        \includegraphicsmaybe{[width=0.85\textwidth]}{../img/profile_elevation.png}
    \end{center}
    \caption{Initial elevation profile}
    \label{fig:growth_profile}
\end{figure}

\subsection{Boundary conditions}

There are two solid boundaries and 2 open boundaries.
The solid boundaries are on either side of the length of the flume, considered as solid walls with perfect slip conditions.
The open boundaries are at $(x=0)$ for the upstream and at $(x=400)$ for the downstream of the flume. A constant discharge of $30.38$m$^3$/s is imposed at the upstream boundary with a temperature set to $0^{\circ} C$. A constant water level is set at $1.6154m$ at the downstream boundary.
A first simulation is carried out with for the hydrodynamics only to get to a steady state equilibrium of a constant and uniform water depth of $1.6154m$ and a constant and uniform discharge of $300m^3/s$ along the entire length of the flume.

\subsection{Atmospheric drivers}

For this test case, linear heat exchanges are used i.e. within the \khione steering file:
\begin{itemize}
	\item\textit{ATMOSPHERE-WATER EXCHANGE MODEL = 0}
\end{itemize}

With this model, the net heat flux received by water trought the water/air interface is expressed as:
\begin{equation}
\phi^* = \phi_R + \alpha + \beta (T_{air} - T_{water})
\end{equation}
where $\phi_R$ the net solar radiation flux, which depends on the solar constant $I_{so}$ and the latitude but
also atmospheric properties like the nebulosity and the visibility. $\alpha$ and $\beta$ are two coefficient that
are set to $-50W.m^{-2}$ and $20W.m^{-2}.K^{-1}$ by default.
Air temperature is set to a constant during the simulation:
\begin{itemize}
	\item\textit{AIR TEMPERATURE      = -15.0}
\end{itemize}

\subsection{Physical parameters}

In order to balance the slope and the discharge through the flume, a Manning friction law is used, with a coefficient of $0.025$.

Physical properties related to frazil growth rate are defined in the \khione steering file. 
In this test case, $N_u$, $r$, $R$ are defined with their default values i.e.
\begin{itemize}
	\item\textit{NUSSELT NUMBER = 4.}
	\item\textit{FRAZIL CRYSTALS RADIUS = 1.2E-4}
	\item\textit{FRAZIL CRYSTALS DIAMETER THICKNESS RATIO  = 10.}
\end{itemize}

\subsection{Numerical parameters}
The time step is set to $0.5s$ and the number of timestep is set to $7200$ which leads to a simulation time of $1h$.

\section{Results}

The figure \ref{fig:growth_temp} show the temperature and the frazil volume fraction the centerline of the flume at final time.
The figure \ref{fig:growth_temp_rb} show the temperature and the frazil volume fraction the right bank of the flume at final time.

\begin{figure}[H]
    \begin{center}
        \includegraphicsmaybe{[width=0.49\textwidth]}{../img/profile_T.png}
        \includegraphicsmaybe{[width=0.49\textwidth]}{../img/profile_Cf.png}
    \end{center}
    \caption{Water temperature (left) and frazil volume fraction (right) along the centerline of the flume (y=36)}
    \label{fig:growth_temp}
\end{figure}
\begin{figure}[H]
    \begin{center}
        \includegraphicsmaybe{[width=0.49\textwidth]}{../img/profile_T_rightbank.png}
        \includegraphicsmaybe{[width=0.49\textwidth]}{../img/profile_Cf_rightbank.png}
    \end{center}
    \caption{Water temperature (left) and frazil volume fraction (right) along the right bank of the flume (y=5)}
    \label{fig:growth_temp_rb}
\end{figure}

On figures \ref{fig:2d_frazil} and \ref{fig:2d_temperature} the frazil concentration and temperature are plotted along the flume at final time.

\begin{figure}[H]
    \begin{center}
        \includegraphicsmaybe{[width=\textwidth]}{../img/T-2d_scalarmap.png}
    \end{center}
    \caption{Water temperature along the flume at final time}
    \label{fig:2d_temperature}
\end{figure}

\begin{figure}[H]
    \begin{center}
        \includegraphicsmaybe{[width=\textwidth]}{../img/Cf-2d_scalarmap.png}
    \end{center}
    \caption{Frazil ice concentration along the flume at final time}
    \label{fig:2d_frazil}
\end{figure}

The figure \ref{fig:growth_Cf_timeseries} show the temperature and the frazil volume fraction against time the centerline of the flume at final time.
The figure \ref{fig:growth_Cf_timeseries_rightbank} show the temperature and the frazil volume fraction against time  the right bank of the flume at final time.

\begin{figure}[H]
    \begin{center}
        \includegraphicsmaybe{[width=0.8\textwidth]}{../img/timeseries_TCf_at_xy=200_18.png}
    \end{center}
    \caption{Water temperature and frazil growth against time at ($x=200$, $y=18$)}
    \label{fig:growth_Cf_timeseries}
\end{figure}

\begin{figure}[H]
    \begin{center}
        \includegraphicsmaybe{[width=0.8\textwidth]}{../img/timeseries_TCf_at_xy=200_5.png}
    \end{center}
    \caption{Water temperature and frazil growth against time at ($x=200$, $y=5$)}
    \label{fig:growth_Cf_timeseries_rightbank}
\end{figure}

\section{Conclusions}

This test case highlights the heterogeneity of frazil ice growth depending on water depth. 
At constant air temperature, lower water depth increases the volumetric cooling rate of water, 
leading to a quicker frazil ice growth dynamic.

\renewcommand{\labelitemi}{\textbullet}
