\chapter{bosse-t2d}
%

% - Purpose & Problem description:
%     These first two parts give reader short details about the test case,
%     the physical phenomena involved and specify how the numerical solution will be validated
%
\section{Purpose}
The capabilities of the morphodynamics module at reproducing the evolution of an initially symmetric, isolated bedform subject to unidirectional flow is considered in this test case.
%In the first subcase, the evolution is considered for a finite time which is smaller than the breaking time (i.e. shock formation). In the second subcase, the simulation time is set in order to be larger than the breaking time so that a discontinuity appears in the solution.\\
The test is useful to check that the bedform propagates with the right celerity on one hand and that the numerical schemes can handle shock formation on the other hand.

\section{Description}

\subsection{Geometry and Mesh}
%
The channel is $16$m long and $1$m width.
The computational domain is discretized with irregular triangular elements with an average size equal to $0.2$ m. It is made up by $534$ nodes and $896$ elements (cf. figure \ref{fig:bosse-t2d:mesh}).

\begin{figure}[H]
 \centering
 \includegraphicsmaybe{[width=\textwidth]}{../img/gai_bosse-t2d_mesh.png}
 \caption{Geometry and mesh of bosse-t2d test case.}
 \label{fig:bosse-t2d:mesh}
\end{figure}
%
\subsection{Bathymetry}
%
The bottom is flat with a finite amplitude perturbation of the bed level (see figure \ref{fig:bosse-t2d:bottom}) given by equation:
\begin {equation}
z_b=\left\{
\begin{array}{ll}
\displaystyle
0.1\sin^2\left(\frac{\pi (x-2)}{8}\right), & \text{si 2m $\leq x \leq$ 10m}  \\
\displaystyle
 0, & \text{otherwise} \label{eq:topographie_initiale} \\
\end{array}
\right.
\end{equation}

\begin{figure}[H]
 \centering
 \includegraphicsmaybe{[width=\textwidth]}{../img/InitialBottom.png}
 \caption{1D profile of initial bed level.}
 \label{fig:bosse-t2d:bottom}
\end{figure}

\subsection{Initial conditions}
%
The velocity field is set to zero and the water surface elevation is set to $0.6$~m.
%
\subsection{Boundary conditions}
%
At the left boundary we set a discharge equal to $0.25$ m$^3$s$^{-1}$ and an equilibrium sediment discharge. At the right boundary, the water surface elevation is set to $0.6$ m and a free condition is set for sediments.

Lateral boundaries are considered as solid walls without friction.
%

% - Numerical parameters:
%     This part is used to specify the numerical parameters used
%     (adaptive time step, mass-lumping when necessary...)
%
\subsection{Analytical solution}
%
The sediment transport capacity ($q_s$) can be expressed as $q_s = AV^m$ (see \citet{Kubatko2008}) with $A$ and $m$ constants and $V$ the average fluid velocity (m\,s$^{-1}$)), assuming the unit fluid discharge as constant $q$ (m$^2$s$^{-1}$).
The Exner equation can then be written as:
\begin{equation}
\frac{\partial z_b}{\partial t}+\frac{1}{1-\lambda}\frac{\partial}{\partial x}A \left(\frac{q}{ Z_s-z_b}\right)^m=0.
\end{equation}
with $z_b$ the bed elevation ($m$), $\lambda$ the porosity ($/$), and $Z_s$ the free surface elevation ($m$). \\
We rewrite the above equation as:
%%\begin{equation}
%%\frac{\partial z_b}{\partial t}+\frac{\partial }{\partial x}A\left(\frac{q}{Z_s-z_b} \right)^m=0
%%\end{equation}
\begin{equation*}
\frac{\partial z_b}{\partial t}+\frac{1}{1-\lambda}\frac{\partial }{\partial z_b} A\left(\frac{q}{Z_s-z_b} \right)^m\frac{\partial z_b}{\partial x}=0
\end{equation*}
that is:
\begin{equation*}
\frac{\partial z_b}{\partial t}+\frac{1}{1-\lambda}\left(\frac{Amq^m}{(Z_s-z_b)^{m+1}} \right)\frac{\partial z_b}{\partial x}=0
\end{equation*}
also written as:
\begin{equation}
\frac{\partial z_b}{\partial t}+c(z_b)\frac{\partial z_b}{\partial x}=0
\label{eq:evolution_fond}\end{equation}
with $c(z_b)$ the bed celerity, or propagation speed of the bed, (m\,s$^{-1}$), expressed by:
\begin{equation}
c (z_b)=\frac{1}{1-\lambda}\frac{m A q^{m}}{ (Z_s-z_b)^{m+1}}
\label{eq:cel}
\end{equation}

Giving an initial condition $z_b(x,0)=z_{b,0}(x)$, the equation \eqref{eq:evolution_fond} has the following exact solution :
\begin{equation}
z_b(x,t)=z_b(x,0)(x-c(z_b)t)
\end{equation}
till the time of wave breaking (i.e. shock formation), see also ~\citet{Kubatko2008}:
\begin{equation}
t=-\frac{1}{\min \limits_{-2\leq x \leq 10} (F' (x))}
\end{equation}
with $F(x)=c (z_b (x,t=0))$.

We choose to expresse the solid discharge with the Engelund-Hansen formula:
  \begin{equation*}
  q_s=0.1 \left[\left(s-1\right)gd^3\right]^{1/2}\frac{\theta^{\frac{5}{2}}}{c_f}
  \end{equation*}
 with $c_f$ the adimensional friction coefficient, $\theta$ the Shields number ($\theta=\frac{\tau_b}{(\rho_s-\rho)gd}$) and $s$ the relative density of sediment.
By explicitation of the Shields number, we obtain:
 \begin{equation*}
  q_s=\frac{0.1}{c_f} \left[\left(s-1\right)gd^3\right]^{1/2}\left(\frac{1}{2}\frac{ c_f}{(s-1)gd} \right)^{5/2}||\vec{u}||^5
 \end{equation*}
Then, the parameters to compute the bed celerity are: \\
$A=\frac{0.1}{c_f}\left[\left( s-1\right)gd^3 \right]^{1/2}\left[\frac{c_f}{2g\left( s-1\right)d} \right]^{5/2}$ and $m=5$.


\subsection{Physical parameters}
%

The bottom friction is described by the Stricker law, with a coefficient equal to $50~m^{1/3}s^{-1}$. The turbulence is modelled by a constant viscosity law with a diffusion coefficient equal to $\nu=10^{-6}~m^2s^{-1}$.

The bed is composed of sediments with a constant median diameter $d_m=0.15~$ mm and a porosity equal to $0.375$.


%
%
\subsection{Numerical parameters}

The time step is set to $\Delta t=0.1~$s and the duration is equal to $35000~$s.
The sediment transport capacity is computed with the Engelund and Hansen formula. Bedload is modeled with two different schemes:
\begin{itemize}
\item a finite element method (default option)
\item an upwind finite volume method
\end{itemize}
Note that to choose an upwind finite volume method, user must use \telkey{FINITE VOLUMES = YES} and \telkey{UPWINDING FOR BEDLOAD = 1}.
% - Results:
%     We comment in this part the numerical results against the reference ones,
%     giving understanding keys and making assumptions when necessary.
%
%
\section{Results}
The results obtained for the two schemes are compared to the exact solution computed with the method of characteristics. Comparisons are shown at two differents times in figure \ref{fig:bosse-t2d:solution}.

The figure on the left shows the solution before shock formation. In this case, numerical results show that the bedform moves with the same celerity for finite element and finite volume scheme, which is slightly faster than the one given by the exact solution.
The figure on the right shows the solution once shock appears. In this case the finite element scheme shows some oscillations, while finite volume scheme presents a smooth solution.
\begin{figure}[H]
 \begin{minipage}[t]{0.5\textwidth}
  \centering
  \includegraphicsmaybe{[width=\textwidth]}{../img/bottom10.png}
 \end{minipage}%
 \begin{minipage}[t]{0.5\textwidth}
  \centering
  \includegraphicsmaybe{[width=\textwidth]}{../img/bottom35.png}
 \end{minipage}
 \caption{Bed elevation: comparison between numerical solutions and exact solution computed with characteristics method at different times.}
 \label{fig:bosse-t2d:solution}
\end{figure}


%TO DO: Add error computation between analytical and numerical solution?
\section{Conclusion}
This test shows that \gaia{} can reproduce the evolution of a bedform, subject to unidirectional flow, capturing shocks formation.
%
%
%
%


% Here is an example of how to include the graph generated by validateTELEMAC.py
% They should be in test_case/img
%\begin{figure} [!h]
%\centering
%\includegraphics[scale=0.3]{../img/mygraph.png}
% \caption{mycaption}\label{mylabel}
%\end{figure}


