/----------------------------------------------------------------------
/ Lyn test case: hydrodynamics initialization
/----------------------------------------------------------------------
TITLE = 'TELEMAC 3D : LYN TEST CASE'
BOUNDARY CONDITIONS FILE  : geo_lyn.cli
GEOMETRY FILE             : geo_lyn.slf
3D RESULT FILE            : r3d_init.slf
2D RESULT FILE            : r2d_init.slf
/----------------------------------------------------------------------
/ General options
/----------------------------------------------------------------------
TIME STEP = 0.5
NUMBER OF TIME STEPS = 2000
GRAPHIC PRINTOUT PERIOD = 500
LISTING PRINTOUT PERIOD = 100
MESH TRANSFORMATION = 1
NUMBER OF HORIZONTAL LEVELS = 12
VARIABLES FOR 2D GRAPHIC PRINTOUTS = 'U,V,H,B,US'
VARIABLES FOR 3D GRAPHIC PRINTOUTS = 'Z,U,V,W,NUZ,K,EPS'
MASS-BALANCE = YES
/----------------------------------------------------------------------
/ Initial and boundary conditions
/----------------------------------------------------------------------
INITIAL CONDITIONS = 'CONSTANT DEPTH'
INITIAL DEPTH = 0.0645
PRESCRIBED FLOWRATES = 0.; 0.011164
VELOCITY VERTICAL PROFILES = 0;2
TIDAL FLATS = NON
/----------------------------------------------------------------------
/ Friction
/----------------------------------------------------------------------
FRICTION COEFFICIENT FOR THE BOTTOM = 0.0037D0
LAW OF BOTTOM FRICTION = 5
/----------------------------------------------------------------------
/ Turbulence
/----------------------------------------------------------------------
/ laminar
HORIZONTAL TURBULENCE MODEL = 1
COEFFICIENT FOR HORIZONTAL DIFFUSION OF VELOCITIES = 1.E-5
/ k-epsilon
VERTICAL TURBULENCE MODEL = 3
OPTION FOR THE BOUNDARY CONDITIONS OF K-EPSILON = 2
COEFFICIENT FOR VERTICAL DIFFUSION OF VELOCITIES = 1.D-6
SCHEME OPTION FOR ADVECTION OF K-EPSILON = 2
SCHEME OPTION FOR ADVECTION OF TRACERS = 2
/----------------------------------------------------------------------
/ Numerical parameters
/----------------------------------------------------------------------
SCHEME OPTION FOR ADVECTION OF VELOCITIES = 2
SOLVER FOR DIFFUSION OF VELOCITIES = 1
PRECONDITIONING FOR DIFFUSION OF VELOCITIES = 2
ACCURACY FOR DIFFUSION OF VELOCITIES = 1.E-6
MAXIMUM NUMBER OF ITERATIONS FOR DIFFUSION OF VELOCITIES = 200
IMPLICITATION FOR DEPTH = 1.0
IMPLICITATION FOR VELOCITIES = 1.0
/keywords related to propagations are related to wave equation (for computation of water depth)
MAXIMUM NUMBER OF ITERATIONS FOR PROPAGATION = 200
/conjugate gradient
SOLVER FOR PROPAGATION = 1
/diagonal preconditioning
PRECONDITIONING FOR PROPAGATION = 2
ACCURACY FOR PROPAGATION = 1.E-6
MASS-LUMPING FOR DEPTH   = 1.
MASS-LUMPING FOR VELOCITIES = 1.
MASS-LUMPING FOR DIFFUSION = 1.
FREE SURFACE GRADIENT COMPATIBILITY = 0.5
/
/ DEFAULT VALUES UNTIL V8P0 KEPT FOR NON REGRESSION
SOLVER FOR PPE = 1
ACCURACY FOR PPE = 1.E-4
ACCURACY FOR DIFFUSION OF K-EPSILON = 1.E-6
