\chapter{WC\_BOMC4: experiments of Wilcock and Crowe (2003)}
\section{Validation with the experiments of WC-2003}
The implementation of the WC-2003 formula is validated with running a model which reproduces two of the experiments of Wilcock and Crowe \cite{Wilcock2003}, and comparing the sediment transport rates obtained numerically with appliying the formula analytically.
To this goal, experiments BOMC2 and BOMC4 are selected, which are characterized by very low and moderate sediment transport rates, respectively. With discretizing the initial GSD into one, two, five and ten size fractions of sediment, the fractional transport rates are computed with Sisyphe for BOMC2 and BOMC4 experiments. The initial GSD has been chosen as the bulk GSD used in the experiments but also as the surface sediment sampled in the end of the experiments \cite{Wilcock2003,Cordier2016tuc}. Several methods used for discretizing the GSD have been compared. The recirculation of sediment is considered for each of these experiments. More details on this work can be found in \cite{Cordier2016tuc}.

Here, we show only a single comparison between the analytical results and the numerical results aiming to reproduce experiment BOMC4, where the GSD used in the numerical model corresponds to the bulk sediment and the GSD is discretized into 5 classes of sediment where the fraction volumes content of sediment are initially equal for each size fraction ({\it i.e.} $F_{a,i}=0.2$, $\forall i \in [1:5]$). Here, the coefficient to calibrate the formula of WC-2003 is set equal to $\alpha_b=1$. The spatially averaged shear velocity obtained at the end of the numerical run ($\bar{u_*}=0.084$ m/s) is used as input variable to compute analytically the fractional transport rates.

The simulation is run for short-term simulations of 20 s so that bed topography remains almost unchanged, sorting of sediment is avoided and consequently computed sediment transport rates remain nearly uniform over space. The benchmark source files of this numerical test are available in the folder \texttt{WC\_BOMC4}. The files used to process the numerical results and to compute the averaged fractional transport rates and shear velocity are found in \texttt{WC\_BOMC4/mean\_qb\_WC\_5CL.py} and \texttt{WC\_BOMC4/mean\_shear\_velocity\_WC\_5CL}, respectively.

The analytical procedure is available in \texttt{WC\_BOMC4/Analytical\_model\_WC2003\_BOMC4.xlsx}. The comparison between the fractional and total transport rates of sediment obtained analytically and numerically (Tab. \ref{tab:WC2003}) show that the model of WC-2003 is well implemented in the TMS.
      
\begin{table}[h!]
\caption[]{\centering Comparison between analytical and numerical results, showing that the formula of WC-2003 is well implemented in the TMS.}
\label{tab:WC2003}
\centering
\begin{tabular}{ | c || c | c |}
\hline
   Bedload [m$^2$/s] & Analytical & Numerical \\\hline\hline
   Total  $\bar{q_{b0}}$ & $6.77\cdot10^{-6}$ & $6.78\cdot10^{-6}$ \\\hline
   $\bar{q_{b0,1}}$ & $3.02\cdot10^{-6}$ & $3.02\cdot10^{-6}$ \\
   $\bar{q_{b0,2}}$ & $2.02\cdot10^{-6}$ & $2.02\cdot10^{-6}$ \\
   $\bar{q_{b0,3}}$ & $1.19\cdot10^{-6}$ & $1.19\cdot10^{-6}$ \\
   $\bar{q_{b0,4}}$ & $5.38\cdot10^{-7}$ & $5.41\cdot10^{-7}$ \\
   $\bar{q_{b0,5}}$ & $7.35\cdot10^{-9}$ & $7.48\cdot10^{-9}$ \\ \hline
\end{tabular}
\end{table}

\section{Implementation of sediment recirculation}
Laboratory experiments of sediment transport measurement {\it e.g.} \cite{Parker1993,Wilcock1993,Wilcock2003} and channel morphodynamics with dunes and bars {\it e.g.} \cite{Lanzoni2000a,Lanzoni2000b,Kleinhans2004,Crosato2012} often rely on the use of sediment recirculating straight flumes. In laboratory, different arrangements can be used for recirculating the water and the sediment \cite{Southard2006}. The fundamental differences found between using a sediment feeding flume and sediment recirculating flume has been well documented in the literature \cite{Wilcock1993,Parker1993,Wilcock2003,Kleinhans2004} and continue to spark debate \cite{Bettess1995}.
For both types of flumes, the upstream water discharge, the downstream water depth and the initial longitudinal bed slope are specified and uniform flow can be maintained \cite{Kleinhans2004}. The difference arises when the rate and composition of the input sediment in the feeding experiment differ from the ones of the sediment recirculating experiments, which depend on the downstream exiting sediment. For the sediment recirculating flume, the boundary condition for sediment supply is periodical, so that the recirculating flume has an additional degree of freedom in the system \cite{Kleinhans2004}.

Using a recirculating flume instead of a feeding flume lead to a different morphodynamic equilibrium when graded sediment is used \cite{Parker1993,Wilcock1993,Bettess1995,Wilcock2003,Kleinhans2004}.
Indeed, in the beginning of the experiments with a feeding flume presenting a mild slopping bed, fine sediment is more mobilized than coarser sediment, leading to deposition of coarse sediment upstream and progressive increase of fine sediment content downstream, called {\it downstream fining} \cite{Hoey1994,Seal1997,ToroEscobar2000}. At the equilibrium, the sediment feeding flume is forced to transport the same rate of coarse sediment than the input rate to follow the equal mobility condition. This equilibrium condition can be only reached by an ajdustement of the longitudinal bed slope, which increases in order to increase bed shear stress and mobilize the coarsest sediment.

On the other hand, the results can be strongly different with using a sediment recirculating flume with the same initial conditions and hydraulic boundaries. In the beginning of the experiments, only the fine fraction of sediment is mobilized and migrates out of the flume, before being re-injected upstream.
In this case, the coarse sediment can form an immobile layer referred to as lag deposit \cite{Kleinhans2004}. As a result, this condition deviates strongly from the hypothesis of equal mobility, with respect to the sediment feeding experiment. The longitudinal equilibrium bed slope is found less modified in comparison with the sediment feeding experiment and the bed shear stress is averagedly lower.

The previous analyses outline the importance of dissociating the sediment feeding from the sediment recirculating for long-term morphodynamics studies. Previous numerical studies reproducing \cite{Defina2003,Qian2016} Lanzoni's experiments used sediment feeding boundary conditions instead of recirculating sediment. Mendoza {\it et al.} \cite{Mendoza2016} and Cordier {\it et al.} \cite{Cordier2017} considered recirculation of sediment in order to get as close as possible to Lanzoni's laboratory conditions and investigate the long-term morphodynamics of bars with this configuration.

\subsection{Implementation of sediment recirculation in Sisyphe (not in library)}
The implementation of sediment recirculation is performed with modifying the subroutine \texttt{conlit.f}.
The procedure consists on computing the solid transport rates exiting the downstream boundary at the previous time step of computation for each size fraction (here \texttt{NSICLA}=2) of sediment (lines 40$\rightarrow$41 and 61$\rightarrow$62), and redistributing the same volume of sediment at each upstream computational node (lines 108$\rightarrow$110). This last step requires to specify manually the number of upstream nodes at line 109. In the case where $t=0$ s ({\it i.e.} first time-step), the prescribed fractional transport rates are set equal to 0. In the case where the transport rate is close to the zero machine, we impose a transport rate equal to 0. See the subroutine \texttt{user\_fortran/conlit.f} for further details.
      

