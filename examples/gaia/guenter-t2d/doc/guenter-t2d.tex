\chapter{guenter-t2d}
%

% - Purpose & Problem description:
%     These first two parts give reader short details about the test case,
%     the physical phenomena involved and specify how the numerical solution will be validated
%
\section{Purpose}
The aim of this test case is to evaluate the ability of \gaia{} to represent the sediment distribution in an experimental flume.

\section{Description}
This test case represents 2 of the 10 flume experiments done in \cite{gunter1971kritische}. The experiments are noted experiment 3 and 5 in the original paper. The experiments represent a straight flume with a constant slope, an imposed discharge at the upstream and an imposed water elevation downstream. The initial sediment distribution is also different between the experiment. The measured data used here are the final bed equilibrium slope, and the final sediment distribution for each experiment.

\section{Physical parameters}

The experiments last approximatively a month of physical time but the equilibrium is reached within 10 to 30 hours. The duration of the computation is set to 50 hours which seems to be a good compromise between computational time and physical duration.
%
A Manning-Strickler's friction law is used on the bottom with a coefficient of 55 m$^{1/3}$/s.
%
The turbulence model used is the Elder's model, and the velocity diffusivity is set to 10$^{-6}$.

The sediment are non-cohesive and are transported with the Meyer Peter Muller formula. A hiding factor is taken into account in a user fortran and corresponds to the Parker–Klingeman formulation \cite{ParkerKlingeman1982}:
\begin{equation}
 H=\left(\dfrac{d_i}{d_m}\right)^{-0.8},
\end{equation}

where $d_i$ is the sediment grain diameter and $d_m$ the total mean diameter for the bed material in the active layer.

The sediment distribution is composed of 5 class with an equal distribution of 20\% each. The mean diameter is then modified to respect the distribution given in the experiment. The table \ref{guenter-t2d:tab:distrib} gives the initial distribution measured and the corresponding distribution applied to the model.

\begin{table}[h]
\centering
\begin{tabular}{|*{9}{c|}}
\hline
\multirow{4}{*}{Experiment 3} & \multirow{2}{*}{experiment} & d[cm] & 0.102 & 0.2 & 0.31 & 0.41 & 0.52 & 0.6 \\
 \cline{3-9}
 & & \% & 35.9 & 56.7 & 68.6 & 86.1 & 92.8 & 100 \\
 \cline{2-9}
 & \multirow{2}{*}{model} & d[cm] & 0.0285 & 0.0707 & 0.1798 & 0.3234 & 0.56 & \\
 \cline{3-9}
 & & \% & 20 & 20 & 20 & 20 & 20 & \\
\hline
\multirow{4}{*}{Experiment 5} & \multirow{2}{*}{experiment} & d[cm] & 0.102 & 0.2 & 0.31 & 0.41 & 0.52 & 0.6 \\
 \cline{3-9}
 & & \% & 57.6 & 78 & 87.5 & 92.7 & 98.3 & 100 \\
 \cline{2-9}
 & \multirow{2}{*}{model} & d[cm] & 0.0354 & 0.0708 & 0.1135 & 0.2232 & 0.5534 &\\
 \cline{3-9}
 & & \% & 20 & 20 & 20 & 20 & 20 & \\
\hline
\end{tabular}
\caption{Initial sediment distribution.}
\label{guenter-t2d:tab:distrib}
\end{table}

\subsection{Geometry and Mesh}
%
Figure \ref{guenter-t2d:fig:bottom} shows the initial bottom elevation and the mesh for the two experiment. The initial slope of the experiment 3 is 0.25\% and of experiment 5 is 0.35\%.

\begin{figure}[H]
 \centering
 \includegraphicsmaybe{[width=0.95\textwidth]}{../img/bottom3.png}\\
 \includegraphicsmaybe{[width=0.95\textwidth]}{../img/bottom5.png}
 \caption{Bottom elevation and mesh for exp 3 (top) and exp 5 (bottom).}
 \label{guenter-t2d:fig:bottom}
\end{figure}
%
\section{Initial and Boundary Conditions}

\subsection{Initial conditions}
%
The initial condition is set with a previous computation file. This file is generated for a hydraulic computation only, with a computational time which allows to reach a steady state.
%
\subsection{Boundary conditions}
%
For the experiment 3, the imposed discharge at the upstream is 0.056 m$^3$/s and the downstream imposed elevation is 1.098 m, knowing that the initial bottom elevation is 1 m.

For the experiment 5, the imposed discharge at the upstream is 0.031 m$^3$/s and the downstream imposed elevation is 1.0653 m, knowing that the initial bottom elevation is 1 m.
%
\section{Numerical parameters}
%
The SUPG finite element scheme is used to compute hydraulic variables from the shallow water equations and a finite volume centred scheme is used to solve the Exner equation.
%
\section{Results}
%
Figure \ref{guenter-t2d:fig:evol} shows the bottom evolution at the end of the simulation.

\begin{figure}[H]
 \centering
 \includegraphicsmaybe{[width=0.95\textwidth]}{../img/bottom_evol3.png}\\
 \includegraphicsmaybe{[width=0.95\textwidth]}{../img/bottom_evol5.png}
 \caption{Bottom evolution for exp 3 (top) and exp 5 (bottom).}
 \label{guenter-t2d:fig:evol}
\end{figure}

Figure \ref{guenter-t2d:fig:final} shows the bottom elevation at the end of the simulation.

\begin{figure}[H]
 \centering
 \includegraphicsmaybe{[width=0.95\textwidth]}{../img/bottom_final3.png}\\
 \includegraphicsmaybe{[width=0.95\textwidth]}{../img/bottom_final5.png}
 \caption{Final bottom elevation for exp 3 (top) and exp 5 (bottom).}
 \label{guenter-t2d:fig:final}
\end{figure}

Figure \ref{guenter-t2d:fig:profile} shows a longitudinal profile of the initial bed elevation, final bed elevation compared to experimental equilibrium slope.

\begin{figure}[H]
 \centering
 \includegraphicsmaybe{[width=0.95\textwidth]}{../img/bed_evol3.png}\\
 \includegraphicsmaybe{[width=0.95\textwidth]}{../img/bed_evol5.png}
 \caption{Final bottom elevation for exp 3 (top) and exp 5 (bottom) compared to the experimental equilibrium slope.}
 \label{guenter-t2d:fig:profile}
\end{figure}

Figure \ref{guenter-t2d:fig:distrib} shows the initial distribution and the final distribution compared to the measured one.

\begin{figure}[H]
 \centering
 \includegraphicsmaybe{[width=0.95\textwidth]}{../img/distrib3.png}\\
 \includegraphicsmaybe{[width=0.95\textwidth]}{../img/distrib5.png}
 \caption{Sediment distribution for exp 3 (top) and exp 5 (bottom).}
 \label{guenter-t2d:fig:distrib}
\end{figure}

%
\section{Conclusion}

The model is able to reproduce correctly both the erosion slope and the sediment distribution at the end of the simulation, and those while the calibration parameters are the same for both experiment.
