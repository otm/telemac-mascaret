\chapter{flume\_bc}
%

% - Purpose & Problem description:
%     These first two parts give reader short details about the test case,
%     the physical phenomena involved and specify how the numerical solution
%     will be validated
%
\section{Purpose}

The aim of this test case is to verify the solid discharge imposition in a
upstream open boundary.

\section{Description}

This test case is inspired by the laboratory experiment of
\cite{soni1981laboratory}. This is a straight channel for which a quantity of
sediment is injected at the entrance.

\section{Physical parameters}

The simulation duration is 30~minutes, which correspond to 1800~s.
%
A Manning-Strickler's friction law is used on the bottom with a coefficient of
62~m$^{1/3}$/s.
%
No turbulence model is used.

The sediment are non-cohesive and are transported with the Meyer Peter Muller
formula. The considered sediment diameter is 0.32~mm with a density of 2650~
kg/m$^3$.

No skin friction correction is considered.

\subsection{Geometry and Mesh}
%
The length of the flume is 30~m and its width is 1~m. Figure \ref{flumebc:mesh}
 shows the mesh with a size of 0.1~m. The slope is 0.427 \% and figure
\ref{flumebc:bottom} shows the bottom elevation in the domain at the initial
time.

\begin{figure}[H]
 \centering
 \includegraphicsmaybe{[width=0.95\textwidth]}{../img/mesh.png}
 \caption{Mesh of the flume.}
 \label{flumebc:mesh}
\end{figure}

\begin{figure}[H]
 \centering
 \includegraphicsmaybe{[width=0.95\textwidth]}{../img/bottom.png}
 \caption{Initial bottom elevation on the flume.}
 \label{flumebc:bottom}
\end{figure}
%
\section{Initial and Boundary Conditions}

\subsection{Initial conditions}
%
The initial condition for the water depth is a constant depth of 0.041 m in the
 flume. The initial velocity is set to $u=\frac{q}{h}$ with $q$ the imposed
discharge 0.0355 m$^3$ and $h$ the initial water depth. This results in a
initial velocity of 0.8659 m/s in the x-axis orientation and 0 m/s in the y-axis
 orientation.
%
\subsection{Boundary conditions}
%
The impose discharge at the upstream is 0.0355 m$^3$/s and the downstream
imposed elevation is 0.0528 m.
%
The solid discharge is imposed in the upstream boundary with a value of 6.4528~
$\times$~10$^{-5}$ kg/s. In case of a bedload boundary file, the value of the
imposed discharge is interpolated from 0 m$^3$/s at the initial time to 6.4528~
$\times$~10$^{-5}$ kg/s at the final time.
%
6 scenarios are tested and the table \ref{flumebc:scenarii} describes them.
%
\begin{table}[h]
\centering
\begin{tabular}{|*{5}{c|}}
\hline
Case & Solid discharge & Sediment classes & Classes initial fraction &
Distribution \\
\hline
1 & Constant & NCO & 1. & Free \\
\hline
2 & Constant & NCO,NCO & 0.4,0.6 & Free \\
\hline
3 & Variable & NCO & 1. & Free \\
\hline
4 & Variable & NCO,NCO & 0.4,0.6 & Free \\
\hline
5 & Constant & CO,NCO,NCO & 0.2,0.3,0.5 & Free \\
\hline
6 & Constant & NCO,NCO & 0.4,0.6 & Imposed (0.6,0.4)\\
\hline
\end{tabular}
\caption{Cases tested.}
\label{flumebc:scenarii}
\end{table}
%
\section{Numerical parameters}
%
The Kinetic finite volume scheme is used to compute hydraulic variables from the
 shallow water equations and finite volume upwind scheme is used to solve Exner
 equation.
%
\section{Results}
%
\subsection{Case 1}
%
Figure \ref{flumebc:profile} shows the longitudinal profile of the bottom
elevation at the end of the simulation.
%
\begin{figure}[H]
 \centering
 \includegraphicsmaybe{[width=0.95\textwidth]}{../img/profile.png}
 \caption{Final bottom elevation on the flume.}
 \label{flumebc:profile}
\end{figure}
%
Figure \ref{flumebc:discharge1} shows the imposed discharge during the time,
extracted from the listing.
%
\begin{figure}[H]
 \centering
 \includegraphicsmaybe{[width=0.95\textwidth]}{../img/discharge1.png}
 \caption{Solid discharge imposed during the simulation.}
 \label{flumebc:discharge1}
\end{figure}
%
\subsection{Case 2}
%
Figure \ref{flumebc:discharge2} shows the imposed solid discharge for each class
 during the time, extracted from the listing.
%
\begin{figure}[H]
 \centering
 \includegraphicsmaybe{[width=0.95\textwidth]}{../img/discharge2.png}
 \caption{Solid discharge imposed during the simulation.}
 \label{flumebc:discharge2}
\end{figure}
%
\subsection{Case 3}
%
Figure \ref{flumebc:discharge3} shows the imposed solid discharge during the
 simulation, extracted from the listing.
%
\begin{figure}[H]
 \centering
 \includegraphicsmaybe{[width=0.95\textwidth]}{../img/discharge3.png}
 \caption{Solid discharge imposed during the simulation.}
 \label{flumebc:discharge3}
\end{figure}
%
\subsection{Case 4}
%
Figure \ref{flumebc:discharge4} shows the imposed solid discharge for each class
 during the simulation, extracted from the listing.
%
\begin{figure}[H]
 \centering
 \includegraphicsmaybe{[width=0.95\textwidth]}{../img/discharge4.png}
 \caption{Solid discharge imposed during the simulation.}
 \label{flumebc:discharge4}
\end{figure}
%
\subsection{Case 5}
%
Figure \ref{flumebc:discharge5} shows the imposed solid discharge for each class
 during the simulation, extracted from the listing.
%
\begin{figure}[H]
 \centering
 \includegraphicsmaybe{[width=0.95\textwidth]}{../img/discharge5.png}
 \caption{Solid discharge imposed during the simulation.}
 \label{flumebc:discharge5}
\end{figure}
%
\subsection{Case 6}
%
Figure \ref{flumebc:discharge6} shows the imposed solid discharge during the
simulation, extracted from the listing.
%
\begin{figure}[H]
 \centering
 \includegraphicsmaybe{[width=0.95\textwidth]}{../img/discharge6.png}
 \caption{Solid discharge imposed during the simulation.}
 \label{flumebc:discharge6}
\end{figure}
%
\section{Conclusion}
%
The model is able to impose a solid discharge:
\begin{itemize}
\item constant or variable in time;
\item with several classes, even with simulation with both cohesive and
non-cohesive material;
\item with a prescribed sediment distribution, or a free sediment distribution.
\end{itemize}
