Here comes the explanation of the different cases tested in hippodrome-t2d.
By default the discretisation for the Exner equation is Finite Element:

Cases with bedload only
- gaia_1NCOb.cas: 1 layer, 1 non-cohesive sediment
- gaia_4NCOb.cas: 1 layer, 4 non-cohesive sediments
- gaia_1NCOb_vf.cas: 1 layer, 1 non-cohesive sediment, Finite Volume formulation
- gaia_4NCOb_vf.cas: 1 layer, 4 non-cohesive sediments, Finite Volume formulation
- gaia_4NCOb_strat_vf.cas: 3 layers, 4 non-cohesive sediments, Finite Volume formulation

Cases with suspension only
- gaia_1COs.cas: 1 layer, 1 cohesive sediment
- gaia_1NCOs.cas: 1 layer, 1 non-cohesive sediment


