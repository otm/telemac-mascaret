\chapter{waq2d\_micropol}

\section{Pollutant transport: one-step reversible model}

\subsection{Purpose}

This case is an example of the use of the MICROPOL module of \waqtel
coupled with \telemac{2D} for one-step reversible interactions between SPM and
micro-pollutant species.

\subsection{Description}

A square basin at rest is considered (length and width = 10~m)
with flat bathymetry and elevation at 0~m.

\subsection{Computational options}

\subsection{Mesh}

The triangular mesh is composed of 272 triangular elements and 159 nodes
(see Figure \ref{fig:waq2d_micropol:pol_mesh}).

\begin{figure}[H]
 \centering
 \includegraphicsmaybe{[width=0.6\textwidth]}{../img/pol/res_mesh.png}
\caption{Mesh.}
 \label{fig:waq2d_micropol:pol_mesh}
\end{figure}

\subsection{Physical parameters}
No diffusion neither for hydrodynamics or tracers.\\
The MICROPOL module is activated by setting \telkey{WATER QUALITY PROCESS} = 7
in the \telemac{2D} \telkey{STEERING FILE}.\\

5 tracers are considered:
\begin{itemize}
\item suspended sediments ($SS$) (kg/m\textsuperscript{3}),
\item bed sediments ($SF$) (kg/m\textsuperscript{2}),
\item micro-pollutant species in dissolved form ($C$) (Bq/m\textsuperscript{3}),
\item fraction adsorbed by suspended sediments ($C_{ss}$) (Bq/m\textsuperscript{3}),
\item fraction adsorbed by bed sediments ($C_{ff}$) (Bq/m\textsuperscript{2}).
\end{itemize}

The settling velocity $w$ (m/s) and the exponential decay constant $L$
(s\textsuperscript{-1}) are adapted in each test case in order to individually
model sedimentation, sorption, desorption and decay. The distribution
coefficient $K_d$ (L/g) is set at a value of 1. All other parameters
are taken with the default values in the \waqtel \telkey{STEERING FILE}.

\subsection{Initial and Boundary Conditions}
The initial water depth is 1~m with a fluid at rest.
The initial values for tracers are homogeneous in each test cases:\\
\begin{itemize}
  \item
    The first test case is a simple sorption with no sedimentation or erosion.

    ($SS$, $SF$, $C$, $C_{SS}$, $C_{ff}$) = (1, 0, 1, 0, 0) and ($w$, $L$) = (0, 0).\\
  \item
    Test case 2 evaluates deposition of SPM in the basin at rest.

($SS$, $SF$, $C$, $C_{SS}$, $C_{ff}$) = (1, 0, 0, 0, 0) and ($w$, $L$) = (4E-7, 0).\\
\item
  The third test case evaluates both sorption of micropollutants from
  dissolved form toward SPM, and deposition.

($SS$, $SF$, $C$, $C_{SS}$, $C_{ff}$) = (1, 0, 1, 0, 0) and ($w$, $L$) = (4E-7, 0).\\
  \item
    Test case 4 evaluates both desorption of micropollutants from SPM
    toward dissolved form, and deposition.

 ($SS$, $SF$, $C$, $C_{SS}$, $C_{ff}$) = (1, 0, 0, 1, 0) and ($w$, $L$) = (4E-7, 0).\\
  \item
    Test case 5 is equivalent to test case 4 but a law of exponential
    micropollutant decay is added.

($SS$, $SF$, $C$, $C_{SS}$, $C_{ff}$) = (1, 0, 0, 1, 0) and ($w$, $L$) = (4E-7, 1.13E-7).\\
\end{itemize}

There are only closed lateral boundaries with free slip condition
and no
friction at the bottom.

\subsection{General parameters}

The time step is 1~h = 3,600~s for a simulated period of 3,200~h $\approx$ 133~days.

\subsection{Analytical solution}
\label{Analytical}
An analytical solution can be calculated in the case of simple steady hydrodynamic conditions:
\begin{itemize}
\item With an unitary constant water depth ($h=1$~m) and no erosion ($U=0$~m/s) the deposition flux $SED$ follows:
  $$SED = w~SS$$
  and the suspended sediment concentration ($SS$) is:

  $$ SS(t) = SS_0~e^{-wt}$$

  Similarly the bottom sediments follow:
  $$SF(t)=SF_{0} +SS_0(1-e^{-wt})$$
\item In the case where the settling velocity $w$ is nil, there is no
  sediment evolution and the exchanges remain exclusively in the
  micropollutant tracers. By setting the initial concentration of
  micropollutants adsorbed by SPM ($C_{ss}$) at 1~Bq/m\textsuperscript{3}
  and 0 otherwise, an analytical solution under these conditions is:
  $$ C_{ss}(t)=\frac{SS_0}{SS_0+1}(1-e^{-k_{-1}(SS_0+1)t})$$
  $$ C(t)=\frac{1}{SS_0+1}(SS_0+e^{-k_{-1}(SS_0+1)t})$$
\end{itemize}


\subsection{Results}

Next figures show the 5 tracers evolution along time for each test case.
MICROPOL module results are compared with the analytical solutions from
\ref{Analytical}.

\paragraph{}
Figure \ref{fig:waq2d_micropol:pol_1} shows the results of a simple sorption
with no sedimentation or erosion.

\begin{figure} [H]
\centering
\includegraphicsmaybe{[width=\textwidth]}{../img/pol/res_tracers_1_0_1_0_0_w_0.png}
\caption{Tracers evolution for test case 1.}
 \label{fig:waq2d_micropol:pol_1}
\end{figure}

\paragraph{}
Figure \ref{fig:waq2d_micropol:pol_2} evaluates deposition of SPM in the
basin at rest.

\begin{figure} [H]
\centering
\includegraphicsmaybe{[width=\textwidth]}{../img/pol/res_tracers_1_0_0_0_0.png}
 \caption{Tracers evolution for test case 2.}
 \label{fig:waq2d_micropol:pol_2}
\end{figure}

\paragraph{}
Figure \ref{fig:waq2d_micropol:pol_3} displays both sorption of micropollutants
from dissolved form toward SPM, and deposition results.

\begin{figure} [H]
\centering
\includegraphicsmaybe{[width=\textwidth]}{../img/pol/res_tracers_1_0_1_0_0.png}
\caption{Tracers evolution for test case 3.}
 \label{fig:waq2d_micropol:pol_3}
\end{figure}

\paragraph{}
In Figure \ref{fig:waq2d_micropol:pol_4} is shown both desorption
of micropollutants from SPM toward dissolved form, and deposition.

\begin{figure} [H]
\centering
\includegraphicsmaybe{[width=\textwidth]}{../img/pol/res_tracers_1_0_0_1_0.png}
 \caption{Tracers evolution for test case 4.}
 \label{fig:waq2d_micropol:pol_4}
\end{figure}

In this particular case, the finite volumes kernel has also been tested
with a different set of parameters to represent the same processes
with a shorter simulation, and the results are shown in Figure
\ref{fig:waq2d_micropol:pol_4_fv}.

\begin{figure} [H]
\centering
\includegraphicsmaybe{[width=\textwidth]}{../img/pol/res_tracers_1_0_0_1_0_fv.png}
 \caption{Tracers evolution for test case 4 in finite volumes.}
 \label{fig:waq2d_micropol:pol_4_fv}
\end{figure}


\paragraph{}
Results of Figure \ref{fig:waq2d_micropol:pol_5} are equivalent to test
case 4 but a law of exponential micropollutant decay is added.

\begin{figure} [H]
\centering
\includegraphicsmaybe{[width=\textwidth]}{../img/pol/res_tracers_1_0_0_1_0_decay.png}
 \caption{Tracers evolution for test case 5.}
 \label{fig:waq2d_micropol:pol_5}
\end{figure}

\section{Pollutant transport: two-step reversible model}

\subsection{Purpose}

This case is an example of the use of the MICROPOL module of \waqtel
coupled with \telemac{2D} for two-step reversible interactions between SPM and
micro-pollutant species.

\subsection{Description}

A square basin at rest is considered (length and width = 10~m)
with flat bathymetry and elevation at 0~m.

\subsection{Computational options}

\subsection{Mesh}

The triangular mesh is composed of 272 triangular elements and 159 nodes
(see Figure \ref{fig:waq2d_micropol:kin2_mesh}).

\begin{figure}[H]
 \centering
 \includegraphicsmaybe{[width=0.6\textwidth]}{../img/pol/res_mesh.png}
\caption{Mesh.}
 \label{fig:waq2d_micropol:kin2_mesh}
\end{figure}

\subsection{Physical parameters}
No diffusion neither for hydrodynamics or tracers.\\
The MICROPOL module is activated by setting \telkey{WATER QUALITY PROCESS} = 7
in the \telemac{2D} \telkey{STEERING FILE}.\\
The two-step reversible model is activated by setting the keyword \telkey{KINETIC EXCHANGE MODEL} = 2 in the \waqtel \telkey{STEERING FILE}.\\

7 tracers are considered:
\begin{itemize}
\item suspended sediments ($SS$) (kg/m\textsuperscript{3}),
\item bed sediments ($SF$) (kg/m\textsuperscript{2}),
\item micro-pollutant species in dissolved form ($C$) (Bq/m\textsuperscript{3}),
\item fraction adsorbed by suspended sediments weak site ($C_{ss1}$)
  (Bq/m\textsuperscript{3}),
\item fraction adsorbed by bed sediments weak site ($C_{ff1}$)
  (Bq/m\textsuperscript{2}).
\item fraction adsorbed by suspended sediments strong site ($C_{ss2}$)
  (Bq/m\textsuperscript{3}),
\item fraction adsorbed by bed sediments strong site ($C_{ff2}$)
  (Bq/m\textsuperscript{2}).
\end{itemize}

The settling velocity $w$ (m/s) and the exponential decay constant $L$
(s\textsuperscript{-1}) are adapted in each test case in order to individually
model sedimentation, sorption, desorption and decay. The distribution
coefficients $K_d$ and $K_{d2}$ (L/g) are set at a value of 2. The sorption reaction
rates $k_1$ and $k_2$ (L/g/s) are set at a value of 0.1. All other parameters
are taken with the default values in the \waqtel \telkey{STEERING FILE}.

\subsection{Initial and Boundary Conditions}
The initial water depth is 1~m with a fluid at rest.
The initial values for tracers are homogeneous in each test cases:\\
\begin{itemize}
  \item
    The first test case is a simple sorption with no sedimentation or erosion.

    ($SS$, $SF$, $C$, $C_{SS1}$, $C_{ff1},C_{ss2},C_{ff2}$) = (1, 0, 1, 0, 0, 0, 0) and ($w$, $L$) = (0, 0).\\
  \item
    Test case 2 evaluates deposition of SPM in the basin at rest.

($SS$, $SF$, $C$, $C_{SS1}$, $C_{ff1},C_{ss2},C_{ff2}$) = (1, 0, 0, 0, 0, 0, 0) and ($w$, $L$) = (4E-7, 0).\\
\item
  The third test case evaluates both sorption of micropollutants from
  dissolved form toward SPM, and deposition.

($SS$, $SF$, $C$, $C_{SS1}$, $C_{ff1},C_{ss2},C_{ff2}$) = (1, 0, 1, 0, 0, 0, 0) and ($w$, $L$) = (4E-7, 0).\\
  \item
    Test case 4 evaluates both desorption of micropollutants from SPM
    toward dissolved form, and deposition.

 ($SS$, $SF$, $C$, $C_{SS1}$, $C_{ff1},C_{ss2},C_{ff2}$) = (1, 0, 0, 1, 0, 0, 0) and ($w$, $L$) = (4E-7, 0).\\
  \item
    Test case 5 is equivalent to test case 4 but a law of exponential
    micropollutant decay is added.

($SS$, $SF$, $C$, $C_{SS1}$, $C_{ff1},C_{ss2},C_{ff2}$) = (1, 0, 0, 1, 0, 0, 0) and ($w$, $L$) = (4E-7, 1.13E-7).\\
\end{itemize}

There are only closed lateral boundaries with free slip condition
and no friction at the bottom.

\subsection{General parameters}

The time step is 0.1~s for a simulated period of 125~s.

\subsection{Analytical solution}
\label{Analytical}
An analytical solution can be calculated in the case of simple steady hydrodynamic conditions:
\begin{itemize}
\item In the case where the settling velocity $w$ is nil, there is no
  sediment evolution and the exchanges remain exclusively in the
  micropollutant tracers. By setting the initial concentration of
  micropollutants in the water ($C$) at 1~Bq/m\textsuperscript{3}
  and 0 otherwise, an analytical solution under these conditions is:
  $$ C(t)=\frac{1}{7}(1+(3+\sqrt{2})e^{\frac{-3+\sqrt{2}}{10}t}+(3-\sqrt{2})e^{-\frac{3+\sqrt{2}}{10}t})$$
  $$ C_{ss1}(t)=\frac{1}{7}(2-(1-2\sqrt{2})e^{\frac{-3+\sqrt{2}}{10}t}-(1+2\sqrt{2})e^{-\frac{3+\sqrt{2}}{10}t})$$
  $$ C_{ss2}(t)=\frac{1}{7}(4-(2+3\sqrt{2})e^{\frac{-3+\sqrt{2}}{10}t}-(2-3\sqrt{2})e^{-\frac{3+\sqrt{2}}{10}t})$$
\end{itemize}


\subsection{Results}

Next figures show the 5 tracers evolution along time for each test case.
MICROPOL module results are compared with the analytical solutions from
\ref{Analytical}.

\paragraph{}
Figure \ref{fig:waq2d_micropol:kin2_1} shows the results of a simple sorption
with no sedimentation or erosion.

\begin{figure} [H]
\centering
\includegraphicsmaybe{[width=\textwidth]}{../img/kin2/res_tracers_1_0_1_0_0_w_0.png}
\caption{Tracers evolution for test case 1.}
 \label{fig:waq2d_micropol:kin2_1}
\end{figure}

\paragraph{}
Figure \ref{fig:waq2d_micropol:kin2_2} evaluates deposition of SPM in the
basin at rest.

\begin{figure} [H]
\centering
\includegraphicsmaybe{[width=\textwidth]}{../img/kin2/res_tracers_1_0_0_0_0.png}
 \caption{Tracers evolution for test case 2.}
 \label{fig:waq2d_micropol:kin2_2}
\end{figure}

\paragraph{}
Figure \ref{fig:waq2d_micropol:kin2_3} displays both sorption of micropollutants
from dissolved form toward SPM, and deposition results.

\begin{figure} [H]
\centering
\includegraphicsmaybe{[width=\textwidth]}{../img/kin2/res_tracers_1_0_1_0_0.png}
\caption{Tracers evolution for test case 3.}
 \label{fig:waq2d_micropol:kin2_3}
\end{figure}

\paragraph{}
In Figure \ref{fig:waq2d_micropol:kin2_4} is shown both desorption
of micropollutants from SPM toward dissolved form, and deposition.

\begin{figure} [H]
\centering
\includegraphicsmaybe{[width=\textwidth]}{../img/kin2/res_tracers_1_0_0_1_0.png}
 \caption{Tracers evolution for test case 4.}
 \label{fig:waq2d_micropol:kin2_4}
\end{figure}

\paragraph{}
Results of Figure \ref{fig:waq2d_micropol:kin2_5} are equivalent to test
case 4 but a law of exponential micropollutant decay is added.

\begin{figure} [H]
\centering
\includegraphicsmaybe{[width=\textwidth]}{../img/kin2/res_tracers_1_0_0_1_0_decay.png}
 \caption{Tracers evolution for test case 5.}
 \label{fig:waq2d_micropol:kin2_5}
\end{figure}

\section{Conclusion}

The MICROPOL module of \waqtel can be coupled with \telemac{2D}.
