\chapter{waq3d\_micropol}

\section{sedimentation}

\subsection{Purpose}

This case is an example of the use of the MICROPOL module of \waqtel
coupled with \telemac{3D}. 

\subsection{Description}

Exchanges between suspended sediments and bed sediments are studied here. A concentration of suspended sediment is initially placed on top of the water column and settling and deposition processes are observed.
A 50~m long and 2.5~m wide rectangular basin at rest is considered
with flat bathymetry and elevation at 0~m.

\subsection{Computational options}

\subsubsection{Mesh}
5 superimposed layers are regularly spaced vertically.
The triangular mesh is composed of 500 triangular elements and
306 nodes (see Figure \ref{fig:waq3d_micropol:mesh_sed} and \ref{fig:waq3d_micropol:mesh_sed_sec}).

\begin{figure}[H]
 \centering
 \includegraphicsmaybe{[width=0.95\textwidth]}{../img/sed/res_mesh.png}
\caption{Horizontal mesh}
 \label{fig:waq3d_micropol:mesh_sed}
\end{figure}

\begin{figure}[H]
 \centering
 \includegraphicsmaybe{[width=0.95\textwidth]}{../img/sed/res_mesh_section.png}
\caption{Vertical mesh}
 \label{fig:waq3d_micropol:mesh_sed_sec}
\end{figure}

\subsubsection{Physical parameters}
There is no diffusion for hydrodynamics and tracer variables.\\
The MICROPOL module is activated by setting \telkey{WATER QUALITY PROCESS} = 7
in the \telemac{3D} \telkey{STEERING FILE}.\\

2 tracers are considered:
\begin{itemize}
\item suspended sediments ($SS$) (kg/m\textsuperscript{3}),

\item bed sediments ($SF$) (kg/m\textsuperscript{2}).
\end{itemize}

The settling velocity $w$ is set at 4.10$^{-3}$~m/s in the \waqtel \telkey{STEERING FILE}.

\subsubsection{Initial and Boundary Conditions}
The initial water depth is 1~m with a fluid at rest.
The initial values for the suspended sediment tracer is set at a concentration of 1~kg/m\textsuperscript{3} in the top layer ($z$ = 1~m) (see Figure \ref{fig:waq3d_micropol:ss_i}). The bed sediment tracer is initially set to zero.

\begin{figure}[H]
 \centering
 \includegraphicsmaybe{[width=0.95\textwidth]}{../img/sed/SS_i.png}
\caption{Suspended sediment initial concentrations}
 \label{fig:waq3d_micropol:ss_i}
\end{figure}
\subsubsection{General parameters}

The time step is 1~s for a simulated period of 1,000~s $\approx$ 17~min.

\subsection{Results}

Next figures show the suspended sediment ($SS$) (kg/m\textsuperscript{3}) and bed sediment (kg/m\textsuperscript{2}) evolution along time at different water depths. The blue dashed line represents the water depth divided by the sediment settling velocity, it can help to estimate the time magnitude for the SPM to reach the bottom layer.

\begin{figure}[H]
 \centering
 \includegraphicsmaybe{[width=0.95\textwidth]}{../img/sed/res_sed.png}
\caption{Suspended sediment and bed sediment evolution}
 \label{fig:waq3d_micropol:res_sed}
\end{figure}

\paragraph{}
At $t$ = 1,000~s only residual suspended sediments are still being deposited in the bottom layer:

\begin{figure}[H]
 \centering
 \includegraphicsmaybe{[width=0.95\textwidth]}{../img/sed/SS_f.png}
\caption{Suspended sediment final concentrations}
 \label{fig:waq3d_micropol:ss_f}
\end{figure}


\subsection{Conclusion}

The MICROPOL module of \waqtel can be coupled with \telemac{3D} for sediment settling deposition.


\section{bump}

\subsection{Description}
Exchanges between suspended sediments and bed sediments are studied here. An initial bump of bed sediment is placed at the bottom layer under a constant water velocity, erosion process is observed.
A 10~m long and 2.5~m wide rectangular basin at rest is considered
with flat bathymetry and elevation at 0~m.

\subsection{Computational options}

\subsubsection{Mesh}
10 superimposed layers are regularly spaced vertically.
The triangular mesh is composed of 400 triangular elements and
231 nodes (see Figure \ref{fig:waq3d_micropol:mesh_bump} and \ref{fig:waq3d_micropol:mesh_bump_sec}).


\begin{figure}[H]
 \centering
 \includegraphicsmaybe{[width=0.95\textwidth]}{../img/bump/res_mesh.png}
\caption{Horizontal mesh}
 \label{fig:waq3d_micropol:mesh_bump}
\end{figure}

\begin{figure}[H]
 \centering
 \includegraphicsmaybe{[width=0.95\textwidth]}{../img/bump/res_mesh_section.png}
\caption{Vertical mesh}
 \label{fig:waq3d_micropol:mesh_bump_sec}
\end{figure}

\subsubsection{Physical parameters}
There is no diffusion for hydrodynamics variables.\\
The MICROPOL module is activated by setting \telkey{WATER QUALITY PROCESS} = 7
in the \telemac{3D} \telkey{STEERING FILE}.\\

2 tracers are considered:
\begin{itemize}
\item suspended sediments ($SS$) (kg/m\textsuperscript{3}),

\item bed sediments ($SF$) (kg/m\textsuperscript{2}).
\end{itemize}

The settling velocity $w$ is set at 0.02~m/s  in the \waqtel \telkey{STEERING FILE}.\\
Critical shear stresses of resuspension and sedimentation $\tau_r$ and $\tau_s$ are set to 0.01~Pa.\\
The erosion rate (or Partheniades constant) $e$ is set at 10$^{-3}$~kg/m$^2$/s.


\subsubsection{Initial and Boundary Conditions}
\begin{itemize}
  
  \item The initial water depth is 1~m.
  \item The initial values for the suspended sediment tracer is set to zero and the initial values of the bed sediment are placed as a polynomial concentration:$$SF(t=0) = \max(0,5(x-7)(3-x))$$
  \item For the solid walls, a slip condition is used.
  \item Upstream flowrate equal to 1.5 m$^3$.s$^{-1}$ is imposed.
  \item Downstream, the water level boundary condition is equal to 1~m.
\end{itemize}

\begin{figure}[H]
 \centering
 \includegraphicsmaybe{[width=0.95\textwidth]}{../img/bump/SF_i.png}
\caption{Bed sediment initial concentrations}
 \label{fig:waq3d_micropol:sf_i_bump}
\end{figure}

\subsubsection{General parameters}

The time step is 0.1~s for a simulated period of 150~s.

\subsection{Results}

Next figures show the suspended sediment ($SS$) (kg/m\textsuperscript{3}) and bed sediment (kg/m\textsuperscript{3}) evolution along time on a vertical cross section.

\begin{figure}[H]
 \centering
 \includegraphicsmaybe{[width=0.95\textwidth]}{../img/bump/SS_mid1.png}
 \includegraphicsmaybe{[width=0.95\textwidth]}{../img/bump/SS_mid2.png}
 \includegraphicsmaybe{[width=0.95\textwidth]}{../img/bump/SS_f.png}

\caption{Suspended sediment concentrations at t = 5, 75 and 150~s}
 \label{fig:waq3d_micropol:ss_m}
\end{figure}

And bed sediment final concentrations:

\begin{figure}[H]
 \centering
 \includegraphicsmaybe{[width=0.95\textwidth]}{../img/bump/SF_f.png}

\caption{Bed sediment final concentrations}
 \label{fig:waq3d_micropol:ss_f_bump}
\end{figure}

\subsection{Conclusion}

The MICROPOL module of \waqtel, coupled with \telemac{3D}, is able to represent sediment erosion.

\section{Pollutant transport: one-step reversible model}

\subsection{Purpose}

This case is an example of the use of the MICROPOL module of \waqtel
coupled with \telemac{3D} for one-step reversible interactions between SPM and
micro-pollutant species.

\subsection{Description}

A 50~m long and 2.5~m wide rectangular basin at rest is considered
with flat bathymetry and elevation at 0~m.

\subsection{Computational options}

\subsubsection{Mesh}

5 layers are regularly spaced vertically.
The triangular mesh is composed of 500 triangular elements and
306 nodes (see Figure \ref{fig:waq3d_micropol:mesh_pol} and \ref{fig:waq3d_micropol:mesh_pol_sec}).

\begin{figure}[H]
 \centering
 \includegraphicsmaybe{[width=0.95\textwidth]}{../img/pol/res_mesh.png}
\caption{Horizontal mesh}
 \label{fig:waq3d_micropol:mesh_pol}
\end{figure}

\begin{figure}[H]
 \centering
 \includegraphicsmaybe{[width=0.95\textwidth]}{../img/pol/res_mesh_section.png}
\caption{Vertical mesh}
 \label{fig:waq3d_micropol:mesh_pol_sec}
\end{figure}

\subsubsection{Physical parameters}
No diffusion neither for hydrodynamics or tracers is considered.\\
The MICROPOL module is activated by setting \telkey{WATER QUALITY PROCESS} = 7
in the \telemac{3D} \telkey{STEERING FILE}.\\

5 tracers are considered:
\begin{itemize}
\item suspended sediments ($SS$) (kg/m\textsuperscript{3}),
\item bed sediments ($SF$) (kg/m\textsuperscript{2}),
\item micro-pollutant species in dissolved form ($C$) (Bq/m\textsuperscript{3}),
\item fraction adsorbed by suspended sediments ($C_{SS}$) (Bq/m\textsuperscript{3}),
\item fraction adsorbed by bed sediments ($C_{ff}$) (Bq/m\textsuperscript{2}).
\end{itemize}

The settling velocity $w$ (m/s) and the exponential decay constant $L$
(s\textsuperscript{-1}) are adapted in each test case in order to individually
model sedimentation, sorption, desorption and decay. The distribution
coefficient $K_d$ (l/g) is set at a value of 1 and the constant of kinetic
desorption $k_{-1}$ (s\textsuperscript{-1}) is set at a value of 0.01. All other parameters
are taken with the default values in the \waqtel \telkey{STEERING FILE}.

\subsubsection{Initial and Boundary Conditions}
The initial water depth is 1~m with a fluid at rest.
The initial values for tracers are homogeneous in each test cases:\\
\begin{itemize}
  \item
    The first test case is a simple sorption with no sedimentation nor erosion.

    ($SS$, $SF$, $C$, $C_{SS}$, $C_{ff}$) = (1, 0, 1, 0, 0) and ($w$, $L$) = (0, 0).\\
  \item
    Test case 2 evaluates deposition of SPM in the basin at rest.

($SS$, $SF$, $C$, $C_{SS}$, $C_{ff}$) = (1, 0, 0, 0, 0) and ($w$, $L$) = (0.07, 0).\\
\item
  The third test case evaluates both sorption of micropollutants from
  dissolved form toward SPM, and deposition.

($SS$, $SF$, $C$, $C_{SS}$, $C_{ff}$) = (1, 0, 1, 0, 0) and ($w$, $L$) = (0.07, 0).\\
  \item
    Test case 4 evaluates both desorption of micropollutants from SPM
    toward dissolved form, and deposition.

 ($SS$, $SF$, $C$, $C_{SS}$, $C_{ff}$) = (1, 0, 0, 1, 0) and ($w$, $L$) = (0.07, 0).\\
  \item
    Test case 5 is equivalent to test case 4 but a law of exponential
    micropollutant decay is added.

($SS$, $SF$, $C$, $C_{SS}$, $C_{ff}$) = (1, 0, 0, 1, 0) and ($w$, $L$) = (0.07, 8.3E-3).\\
\end{itemize}

There are only closed lateral boundaries with free slip condition
and no friction at the bottom.

\subsubsection{General parameters}

The time step is 0.1~s for a simulated period of 125~s.

\subsubsection{Analytical solution}
\label{Analytical}
An analytical solution can be calculated in the case of simple steady hydrodynamic conditions:
\begin{itemize}
\item In the case where the settling velocity $w$ is nil, there is no
  sediment evolution and the exchanges remain exclusively in the
  micropollutant tracers. By setting the initial concentration of
  micropollutants adsorbed by SPM ($C_{SS}$) at 1~Bq/m\textsuperscript{3}
  and 0 otherwise, an analytical solution under these conditions is:
  $$ C_{SS}(t)=\frac{SS_0}{SS_0+1}(1-e^{-k_{-1}(SS_0+1)t})$$
  $$ C(t)=\frac{1}{SS_0+1}(SS_0+e^{-k_{-1}(SS_0+1)t})$$
\end{itemize}

\subsection{Results}

Next figures show the 5 tracers evolution at the bottom layer ($z$ = 0~m) along time for each test case.
MICROPOL module results are compared with the analytical solutions from
\ref{Analytical}.

\paragraph{}
Figure \ref{fig:waq3d_micropol:sorp} shows the results of a simple sorption
with no sedimentation or erosion.

\begin{figure} [H]
\centering
\includegraphicsmaybe{[width=\textwidth]}{../img/pol/res_sorp.png}
\caption{Tracers evolution for test case 1}
 \label{fig:waq3d_micropol:sorp}
\end{figure}

\paragraph{}
Figure \ref{fig:waq3d_micropol:sed} evaluates deposition of SPM in the
basin at rest. Depending on the bottom layer thickness, suspended sediments concentration accumulates before reaching the bed sediment layer.

\begin{figure} [H]
\centering
\includegraphicsmaybe{[width=\textwidth]}{../img/pol/res_sed.png}
 \caption{Tracers evolution for test case 2}
 \label{fig:waq3d_micropol:sed}
\end{figure}

\paragraph{}
Figure \ref{fig:waq3d_micropol:sorp_desorp} displays both sorption of micropollutants
from dissolved form toward SPM, and deposition results.

\begin{figure} [H]
\centering
\includegraphicsmaybe{[width=\textwidth]}{../img/pol/res_sed_sorp.png}
\caption{Tracers evolution for test case 3}
 \label{fig:waq3d_micropol:sorp_desorp}
\end{figure}

\paragraph{}
In Figure \ref{fig:waq3d_micropol:sed_desorp} are shown both desorption
of micropollutants from SPM toward dissolved form, and deposition.

\begin{figure} [H]
\centering
\includegraphicsmaybe{[width=\textwidth]}{../img/pol/res_sed_desorp.png}
 \caption{Tracers evolution for test case 4}
 \label{fig:waq3d_micropol:sed_desorp}
\end{figure}

\paragraph{}
Results of Figure \ref{fig:waq2d_micropol:sed_desorp_decay} are equivalent to test
case 4 but a law of exponential micropollutant decay is added.

\begin{figure} [H]
\centering
\includegraphicsmaybe{[width=\textwidth]}{../img/pol/res_sed_desorp_decay.png}
 \caption{Tracers evolution for test case 5}
 \label{fig:waq2d_micropol:sed_desorp_decay}
\end{figure}

\subsection{Conclusion}

The MICROPOL module of \waqtel can be coupled with \telemac{3D} for micropollutant and sediment interactions.

\section{Pollutant transport: two-step reversible model}

\subsection{Purpose}

This case is an example of the use of the MICROPOL module of \waqtel
coupled with \telemac{3D} for two-step reversible interactions between SPM and
micro-pollutant species.

\subsection{Description}

A 50~m long and 2.5~m wide rectangular basin at rest is considered
with flat bathymetry and elevation at 0~m.

\subsection{Computational options}

\subsubsection{Mesh}

5 layers are regularly spaced vertically.
The triangular mesh is composed of 500 triangular elements and
306 nodes (see Figure \ref{fig:waq3d_micropol:mesh_pol} and \ref{fig:waq3d_micropol:mesh_pol_sec}).

\begin{figure}[H]
 \centering
 \includegraphicsmaybe{[width=0.95\textwidth]}{../img/pol/res_mesh.png}
\caption{Horizontal mesh}
 \label{fig:waq3d_micropol:mesh_pol}
\end{figure}

\begin{figure}[H]
 \centering
 \includegraphicsmaybe{[width=0.95\textwidth]}{../img/pol/res_mesh_section.png}
\caption{Vertical mesh}
 \label{fig:waq3d_micropol:mesh_pol_sec}
\end{figure}

\subsubsection{Physical parameters}
No diffusion neither for hydrodynamics or tracers is considered.\\
The MICROPOL module is activated by setting \telkey{WATER QUALITY PROCESS} = 7
in the \telemac{3D} \telkey{STEERING FILE}.\\
The two-step reversible model is activated by setting the keyword \telkey{KINETIC EXCHANGE MODEL} = 2 in the \waqtel \telkey{STEERING FILE}.\\

7 tracers are considered:
\begin{itemize}
\item suspended sediments ($SS$) (kg/m\textsuperscript{3}),
\item bed sediments ($SF$) (kg/m\textsuperscript{2}),
\item micro-pollutant species in dissolved form ($C$) (Bq/m\textsuperscript{3}),
\item fraction adsorbed by suspended sediments weak site ($C_{ss1}$)
  (Bq/m\textsuperscript{3}),
\item fraction adsorbed by bed sediments weak site ($C_{ff1}$)
  (Bq/m\textsuperscript{2}).
\item fraction adsorbed by suspended sediments strong site ($C_{ss2}$)
  (Bq/m\textsuperscript{3}),
\item fraction adsorbed by bed sediments strong site ($C_{ff2}$)
  (Bq/m\textsuperscript{2}).
\end{itemize}

The settling velocity $w$ (m/s) and the exponential decay constant $L$
(s\textsuperscript{-1}) are adapted in each test case in order to individually
model sedimentation, sorption, desorption and decay. The distribution
coefficients $K_d$ and $K_{d2}$ (L/g) are set at a value of 2. The sorption reaction
rates $k_1$ and $k_2$ (L/g/s) are set at a value of 0.1. All other parameters
are taken with the default values in the \waqtel \telkey{STEERING FILE}.

\subsubsection{Initial and Boundary Conditions}
The initial water depth is 1~m with a fluid at rest.
The initial values for tracers are homogeneous in each test cases:\\
\begin{itemize}
  \item
    The first test case is a simple sorption with no sedimentation or erosion.

    ($SS$, $SF$, $C$, $C_{SS1}$, $C_{ff1},C_{ss2},C_{ff2}$) = (1, 0, 1, 0, 0, 0, 0) and ($w$, $L$) = (0, 0).\\
  \item
    Test case 2 evaluates deposition of SPM in the bassin at rest.

($SS$, $SF$, $C$, $C_{SS1}$, $C_{ff1},C_{ss2},C_{ff2}$) = (1, 0, 0, 0, 0, 0, 0) and ($w$, $L$) = (4E-7, 0).\\
\item
  The third test case evaluates both sorption of micropollutants from
  dissolved form toward SPM, and deposition.

($SS$, $SF$, $C$, $C_{SS1}$, $C_{ff1},C_{ss2},C_{ff2}$) = (1, 0, 1, 0, 0, 0, 0) and ($w$, $L$) = (4E-7, 0).\\
  \item
    Test case 4 evaluates both desorption of micropollutants from SPM
    toward dissolved form, and deposition.

 ($SS$, $SF$, $C$, $C_{SS1}$, $C_{ff1},C_{ss2},C_{ff2}$) = (1, 0, 0, 1, 0, 0, 0) and ($w$, $L$) = (4E-7, 0).\\
  \item
    Test case 5 is equivalent to test case 4 but a law of exponential
    micropollutant decay is added.

($SS$, $SF$, $C$, $C_{SS1}$, $C_{ff1},C_{ss2},C_{ff2}$) = (1, 0, 0, 1, 0, 0, 0) and ($w$, $L$) = (4E-7, 1.13E-7).\\
\end{itemize}

There are only closed lateral boundaries with free slip condition
and no friction at the bottom.

\subsubsection{General parameters}

The time step is 0.1~s for a simulated period of 125~s.

\subsubsection{Analytical solution}
\label{Analytical}
An analytical solution can be calculated in the case of simple steady hydrodynamic conditions:
\begin{itemize}
\item In the case where the settling velocity $w$ is nil, there is no
  sediment evolution and the exchanges remain exclusively in the
  micropollutant tracers. By setting the initial concentration of
  micropollutants in the water ($C$) at 1~Bq/m\textsuperscript{3}
  and 0 otherwise, an analytical solution under these conditions is:
  $$ C(t)=\frac{1}{7}(1+(3+\sqrt{2})e^{\frac{-3+\sqrt{2}}{10}t}+(3-\sqrt{2})e^{-\frac{3+\sqrt{2}}{10}t})$$
  $$ C_{ss1}(t)=\frac{1}{7}(2-(1-2\sqrt{2})e^{\frac{-3+\sqrt{2}}{10}t}-(1+2\sqrt{2})e^{-\frac{3+\sqrt{2}}{10}t})$$
  $$ C_{ss2}(t)=\frac{1}{7}(4-(2+3\sqrt{2})e^{\frac{-3+\sqrt{2}}{10}t}-(2-3\sqrt{2})e^{-\frac{3+\sqrt{2}}{10}t})$$
\end{itemize}

\subsection{Results}

Next figures show the 5 tracers evolution at the bottom layer ($z$ = 0~m) along time for each test case.
MICROPOL module results are compared with the analytical solutions from \ref{Analytical}.

\paragraph{}
Figure \ref{fig:waq3d_micropol:kin2_sorp} shows the results of a simple sorption
with no sedimentation or erosion.

\begin{figure} [H]
\centering
\includegraphicsmaybe{[width=\textwidth]}{../img/kin2/res_sorp.png}
\caption{Tracers evolution for test case 1}
 \label{fig:waq3d_micropol:kin2_sorp}
\end{figure}

\paragraph{}
Figure \ref{fig:waq3d_micropol:kin2_sed} evaluates deposition of SPM in the
basin at rest. Depending on the bottom layer thickness, suspended sediments concentration accumulates before reaching the bed sediment layer.

\begin{figure} [H]
\centering
\includegraphicsmaybe{[width=\textwidth]}{../img/kin2/res_sed.png}
 \caption{Tracers evolution for test case 2}
 \label{fig:waq3d_micropol:kin2_sed}
\end{figure}

\paragraph{}
Figure \ref{fig:waq3d_micropol:kin2_sorp_desorp} displays both sorption of micropollutants
from dissolved form toward SPM, and deposition results.

\begin{figure} [H]
\centering
\includegraphicsmaybe{[width=\textwidth]}{../img/kin2/res_sed_sorp.png}
\caption{Tracers evolution for test case 3}
 \label{fig:waq3d_micropol:kin2_sorp_desorp}
\end{figure}

\paragraph{}
In Figure \ref{fig:waq3d_micropol:kin2_sed_desorp} are shown both desorption
of micropollutants from SPM toward dissolved form, and deposition.

\begin{figure} [H]
\centering
\includegraphicsmaybe{[width=\textwidth]}{../img/kin2/res_sed_desorp.png}
 \caption{Tracers evolution for test case 4}
 \label{fig:waq3d_micropol:kin2_sed_desorp}
\end{figure}

\paragraph{}
Results of Figure \ref{fig:waq2d_micropol:kin2_sed_desorp_decay} are equivalent to test
case 4 but a law of exponential micropollutant decay is added.

\begin{figure} [H]
\centering
\includegraphicsmaybe{[width=\textwidth]}{../img/kin2/res_sed_desorp_decay.png}
 \caption{Tracers evolution for test case 5}
 \label{fig:waq2d_micropol:kin2_sed_desorp_decay}
\end{figure}

\subsection{Conclusion}

The MICROPOL module of \waqtel can be coupled with \telemac{3D} for micropollutant and sediment interactions.
