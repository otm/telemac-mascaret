
"""
Validation script for trigrid_mail_det
"""
from vvytel.vnv_study import AbstractVnvStudy

class VnvStudy(AbstractVnvStudy):
    """
    Class for validation
    """

    def _init(self):
        """
        Defines the general parameter
        """
        self.rank = 1
        self.tags = ['stbtel']

    def _pre(self):
        """
        Defining the studies
        """

        # trigrid_mail_tot scalar mode
        self.add_study('vnv_1',
                       'stbtel',
                       'stb_det1.cas')

        # trigrid_mail_tot scalar mode
        self.add_study('vnv_2',
                       'stbtel',
                       'stb_det2.cas')


    def _check_results(self):
        """
        Post-treatment processes
        """


    def _post(self):
        """
        Post-treatment processes
        """
        from postel.plot_vnv import vnv_plot2d
        # Getting file
        res_vnv_1_stbres, _ = self.get_study_res('vnv_1:STBRES')
        res_vnv_2_stbres, _ = self.get_study_res('vnv_2:STBRES')

        #Plotting mesh
        vnv_plot2d('',
                   res_vnv_1_stbres,
                   plot_mesh=True,
                   xlim=[950, 1175],
                   ylim=[350, 575],
                   fig_size=(10, 8),
                   fig_name='img/mesh1')

        # Plotting BOTTOM at 0
        vnv_plot2d('BOTTOM',
                   res_vnv_1_stbres,
                   record=0,
                   filled_contours=True,
                   cbar_label='Bottom elevation (m)',
                   xlim=[950, 1175],
                   ylim=[350, 575],
                   fig_size=(10, 7),
                   fig_name='img/bathy1')

        #Plotting mesh
        vnv_plot2d('',
                   res_vnv_2_stbres,
                   plot_mesh=True,
                   xlim=[950, 1175],
                   ylim=[350, 575],
                   fig_size=(10, 8),
                   fig_name='img/mesh2')

        # Plotting BOTTOM at 0
        vnv_plot2d('BOTTOM',
                   res_vnv_2_stbres,
                   record=0,
                   filled_contours=True,
                   cbar_label='Bottom elevation (m)',
                   xlim=[950, 1175],
                   ylim=[350, 575],
                   fig_size=(10, 7),
                   fig_name='img/bathy2')

        # Closing file
        res_vnv_2_stbres.close()
        res_vnv_1_stbres.close()
