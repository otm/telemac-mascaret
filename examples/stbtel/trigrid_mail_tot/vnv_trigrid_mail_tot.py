
"""
Validation script for trigrid_mail_tot
"""
from vvytel.vnv_study import AbstractVnvStudy
from execution.telemac_cas import TelemacCas, get_dico

class VnvStudy(AbstractVnvStudy):
    """
    Class for validation
    """

    def _init(self):
        """
        Defines the general parameter
        """
        self.rank = 1
        self.tags = ['stbtel']

    def _pre(self):
        """
        Defining the studies
        """

        # trigrid_mail_tot scalar mode
        self.add_study('vnv_1',
                       'stbtel',
                       'stb_tot.cas')


    def _check_results(self):
        """
        Post-treatment processes
        """


    def _post(self):
        """
        Post-treatment processes
        """
        from postel.plot_vnv import vnv_plot2d
        # Getting file
        res_vnv_1_stbres, _ = self.get_study_res('vnv_1:STBRES')

        # Plotting BOTTOM at 0
        vnv_plot2d('BOTTOM',
                   res_vnv_1_stbres,
                   record=0,
                   filled_contours=True,
                   cbar_label='Bottom elevation (m)',
                   fig_size=(20, 5),
                   fig_name='img/bathy')

        #Plotting mesh
        vnv_plot2d('',
                   res_vnv_1_stbres,
                   plot_mesh=True,
                   fig_size=(20, 5),
                   fig_name='img/mesh')

        # Closing file
        res_vnv_1_stbres.close()
