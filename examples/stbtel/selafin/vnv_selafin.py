
"""
Validation script for selafin
"""
from vvytel.vnv_study import AbstractVnvStudy
from execution.telemac_cas import TelemacCas, get_dico

class VnvStudy(AbstractVnvStudy):
    """
    Class for validation
    """

    def _init(self):
        """
        Defines the general parameter
        """
        self.rank = 0
        self.tags = ['stbtel']

    def _pre(self):
        """
        Defining the studies
        """

        # selafin scalar mode
        self.add_study('vnv_1',
                       'stbtel',
                       'stb_selafin.cas')



    def _check_results(self):
        """
        Post-treatment processes
        """


    def _post(self):
        """
        Post-treatment processes
        """
        from postel.plot_vnv import vnv_plot2d
        # Getting file
        res_vnv_1_stbgeo, _ = self.get_study_res('vnv_1:STBGEO')
        res_vnv_1_stbres, _ = self.get_study_res('vnv_1:STBRES')

        # Plotting WATER DEPTH at 0
        vnv_plot2d('HAUTEUR D\'EAU',
                   res_vnv_1_stbgeo,
                   record=0,
                   filled_contours=True,
                   plot_mesh=True,
                   cbar_label='Water depth (m)',
                   vmax=0.2,
                   nv=2,
                   fig_size=(20, 5),
                   fig_name='img/ini_mesh')

        #Plotting mesh
        vnv_plot2d('',
                   res_vnv_1_stbres,
                   plot_mesh=True,
                   fig_size=(20, 5),
                   fig_name='img/dry_mesh')

        # Closing file
        res_vnv_1_stbres.close()
        res_vnv_1_stbgeo.close()
