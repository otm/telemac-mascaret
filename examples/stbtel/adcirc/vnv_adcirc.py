
"""
Validation script for adcirc
"""
from vvytel.vnv_study import AbstractVnvStudy
from execution.telemac_cas import TelemacCas, get_dico

class VnvStudy(AbstractVnvStudy):
    """
    Class for validation
    """

    def _init(self):
        """
        Defines the general parameter
        """
        self.rank = 1
        self.tags = ['stbtel']

    def _pre(self):
        """
        Defining the studies
        """

        # adcirc scalar mode
        self.add_study('vnv_1',
                       'stbtel',
                       'stb.cas')



    def _check_results(self):
        """
        Post-treatment processes
        """


    def _post(self):
        """
        Post-treatment processes
        """
        from postel.plot_vnv import vnv_plot2d
        # Getting file
        res_vnv_1_stbres, _ = self.get_study_res('vnv_1:STBRES')

        #Plotting mesh
        vnv_plot2d('',
                   res_vnv_1_stbres,
                   plot_mesh=True,
                   fig_size=(13, 12),
                   fig_name='img/serafin_mesh')

        # Closing file
        res_vnv_1_stbres.close()
