\chapter{Cohesive channel}\label{chapter:cohesive channel}

\section{Erosion}

\subsection{Purpose}

The purpose of this test case is to validate the calculation of the
erosion flux and the evolution of the suspended sediment concentration
along the reach.

\subsection{Description}

To obtain an analytical solution, the bathymetry evolution
is not updated due to erosion (fixed bottom). The hydraulic kernel
REZO is used. We are interested in the erosion of the
bottom in a rectangular channel. Friction is considered to be zero
on the bottom and on the walls.

\subsection{Physical parameters}

The river is a rectangular channel with flat bottom, length $L$, width
$La$, zero slope.

 \begin{table}[h!]
   \begin{center}
   \caption{Parameters of test case: erosion in a rectangular channel}
       \begin{tabular}{|c|c|c|c|}
       \hline
$L$ & 1500 & m & Length of the channel\\ \hline
$La$ & 50 & m & Width of the channel\\ \hline
$M$ & 10$^{-2}$ & kg m$^{-2}$ s$^{-1}$ & Partheniades constant\\ \hline
$tau_{ce}$ & 0.01 & Pa & Critical erosion stress\\ \hline
$C_0$ & 0 & g/l & Concentration entering the channel\\ \hline
$Q$ & 50 & m$^{3}$ s$^{-1}$ & Flow rate into the channel\\ \hline
$H$ & 4.5 & m & Head of water downstream\\ \hline
$K_p $ & 85 & m$^{1/3}$ s$^{-1}$ & Skin Strickler coefficient\\ \hline
        \end{tabular}
     \end{center}
\end{table}

We consider the flow initially as uniform and permanent with an initial
concentration of zero sediment.

Being in river flow, we impose two boundary conditions:

\begin{itemize}
\item upstream, a constant flow $Q$ and concentration $C_0$;
\item downstream, a constant elevation and a free sediment flow.
\end{itemize}

To highlight erosion, additional assumptions must be made: Deposition
must be zero, therefore the settling velocity of the mud will be zero.

\subsection{Analytical solution}

The equation to solve is:
\begin{equation}
U \frac{\partial C}{\partial x} - k_x \frac{\partial^2 C}{\partial x^2} =
\frac{ \Phi_{erosion}}{ A},
\label{eq:sol}
\end{equation}

with as boundary conditions $C(x=0)=C_0$ and
$\left( \frac{\partial C}{\partial x} \right)_{x=L}=0$,
with $H$, the water depth, and the erosion flux according to
Partheniades law, the equation (\ref{eq:sol}) becomes:
\begin{equation}
U \frac{\partial C}{\partial x} - k_x \frac{\partial^2 C}{\partial x^2} =
\frac{ M (\tau/\tau_{ce}-1)}{ H}.
\label{eq:solero2}
\end{equation}

In \textbf{the case where the diffusion is zero}, $k_x=0 $, and the
equation (\ref{eq:solero2}) turns into a simple first-order
differential equation, whose solution is:
\begin{equation}
C(x)=\alpha \frac{x}{L}+ C_0,
\label{eq:sol3}
\end{equation}

where $ \alpha = \frac{LM (\tau/\tau_{ce}-1) }{UH} $,
where
$\tau= \rho g H(x,y) J = \frac{\rho g H(x,y) u^2}{K_p^2 R_h^{4/3}}$

\textbf{If the scattering is not zero}, $k_x\neq 0$, the equation
(\ref{eq:solero2}) is rewritten in terms of the Peclet number,
$Pe=\frac{UL}{k_x}$, and $\alpha$. The Peclet number represents
the ratio of convection to diffusion. The equation is then written:

\begin{equation}
\frac{1}{Pe} \frac{\partial^2 C}{\partial x^2} -
\frac{1}{L} \frac{\partial C}{\partial x} =- \frac{ _\alpha}{ L^2},
\label{eq:sol4}
\end{equation}

whose solution can be written:

\begin{equation}
C(x)=C_0+\alpha \frac{x}{L} + \frac{\alpha}{Pe}
\left( e^{-Pe}-e^{-Pe (1-x/L)} \right).
\label{eq:}
\end{equation}

We study the results of this test case for two different values of
Peclet number : an infinite Peclet number (there is no diffusion)
and a Peclet number of 1 (the diffusion is as important as the
convection). The corresponding diffusion coefficients are 0 and 333.3
m$^2$/s.

For a Peclet equal to 1, diffusion has the effect of decreasing the
concentration of SS from the first meters of the reach, and
consequently the concentration at the end of the channel is three
times lower than the case without diffusion.

\subsection{Numerical parameters}

The duration is set to 136 000 s and the time step is 108 s.
The discretization of the reach is 30 m.

\subsection{Results}

Figure \ref{fig:erosion:nodiff} and \ref{fig:erosion:diff} show the
suspended sediment concentration along the channel simulated, in
comparison with the analytical solution.

\begin{figure}[h]
 \centering
 \includegraphicsmaybe{[width=0.7\textwidth]}{../img/profile_conc.png}
 \caption{Concentration along the channel without diffusion,
  simulation vs analytical solution.}
 \label{fig:erosion:nodiff}
\end{figure}

\begin{figure}[h]
 \centering
 \includegraphicsmaybe{[width=0.7\textwidth]}{../img/profile_conc_diff.png}
 \caption{Concentration along the channel with diffusion,
  simulation vs analytical solution.}
 \label{fig:erosion:diff}
\end{figure}

\subsection{Conclusion}

The erosion flux is well represented.

\section{Deposition}

\subsection{Purpose}

The purpose of this test case is to validate the calculation of the
deposition flux and the evolution of the suspended sediment load
concentration along the reach.

\subsection{Description}

In order to obtain an analytical solution, the bathymetry evolution
is not updated after the deposition (fixed bottom). We place ourselves
here in the case of a river flow. The hydraulic calculation kernel
REZO is therefore used.
We are interested in a deposition phenomenon with entry of SS in the
reach. Friction is considered to be nil on the bottom and
on the walls.

\subsection{Physical parameters}

One considers a rectangular channel with flat bottom, of length $L$,
width $La$, and zero slope.

\begin{table}[h!] %
\begin{center}
\begin{tabular}{|c|c|c|c|} \hline
$L$&1500&m&Length of the channel \\ \hline
$La$&50&m&Width of the channel \\ \hline
$ws$&1.5 10$^{-4}$&m/s&Settling velocity of the mud \\ \hline
$\tau_{cd}$&0.1&Pa&Critical deposition shear stress \\ \hline
$C0$&1&g/l&Inflow concentration \\ \hline
$Q$&10&m$^{3}$ s$^{-1}$&Channel discharge \\ \hline
$H$&4.5&m&Water depth \\ \hline
$Kp$&85&m$^{1/3}$ s$^{-1}$&Skin friction coefficient \\ \hline
\end{tabular}
\caption{ Parameters of deposition test case. }
\end{center}
\label{}
\end{table}

Being in fluvial flow, one imposes two boundary conditions:
\begin{itemize}
\item upstream, a constant flow and concentration $Q$ and $C0$;
\item downstream, a constant elevation and a free sediment flow.
\end{itemize}

Since we will be highlighting deposition, additional assumptions must
be made:
\begin{itemize}
\item the erosion must be null, that is why the coefficient of
Parthéniades will be taken null;
\item we study an established regime, to obtain the update of
the geometry of the bottom of the channel following the deposit
is not carried out.
\end{itemize}

\subsection{Analytical solution}

Under these assumptions and this geometry, the equation to be treated
becomes:

\begin{equation}
U \frac{\partial C}{\partial x} -
k_x \frac{\partial^2 C}{\partial x^2},
= \frac{ \Phi_{depot}}{ A}
\label{eq:transport-depot}
\end{equation}

with the boundary conditions defined as $C(x=0)=C_0$
and $\left(  \frac{\partial C}{\partial x}\right)_{x=L}=0 $,
with $H$ the water depth, and the deposition flux given by the Krone
law, equation \eqref{eq:transport-depot} becomes :
\begin{equation}
U \frac{\partial C}{\partial x} -
k_x \frac{\partial^2 C}{\partial x^2}.
= \frac{ C w_s (1- \tau/\tau_{cd})}{ H}
\label{eq:sol2}
\end{equation}

Under the assumptions of non-variation of the geometry, this
differential equation has an analytical solution because these
coefficients are constant.

In the case of zero diffusion, $k_x=0$, the equation
(\ref{eq:transport-depot}) turns into a simple first order
differential equation:
\begin{equation}
\frac{\partial C}{\partial x}=- \frac{\alpha}{L} C,
\label{eq:}
\end{equation}

where $ \alpha = \frac{Lw_s (1-\tau/\tau_{cd})  }{UH} $ .\\

 dont la solution est :
\begin{equation}
C(x)=C_0 e^{-\alpha x /L}.
\label{eq:sol4}
\end{equation}

If $k_x\neq 0$, the equation (\ref{eq:transport-depot}) is written
depending on the Peclet number, $Pe=\frac{UL}{k_x}$, and on $\alpha$:

\begin{equation}
\frac{1}{Pe} \frac{\partial^2 C}{\partial x^2} -
\frac{1}{L} \frac{\partial C}{\partial x} =
- \frac{ _\alpha}{ L^2},
\label{eq:sol-transport}
\end{equation}

then the solution writes:

\begin{equation}
C(x)= \frac{ \omega_2 e^{\omega_2} e^{\omega_1 x/L} -
\omega_1 e^{\omega_1} e^{\omega_2 x/L}}{ \omega_2 e^{\omega_2}-
\omega_1 e^{\omega_1}  },
\label{eq:}
\end{equation}

with
\begin{equation}
\omega _1 = \frac{ 1 + \sqrt{1+4 \alpha/Pe}  }{2/Pe},
\label{eq:}
\end{equation}
\begin{equation}
\omega _2 = \frac{ 1 - \sqrt{1+4 \alpha/Pe}  }{2/Pe}.
\label{eq:}
\end{equation}

As for erosion, the phenomenon of diffusion is introduced with a Peclet
number of 1 (with a value of diffusion coefficient equal to 75
m$^2$/s), an infinite Peclet number is also tested ($k_x=0$ m$^2$/s).

\subsection{Numerical parameters}

The duration is set to 300 000 s and the time step is 80 s.
The discretization of the reach is 30 m.

\subsection{Results}

Figure \ref{fig:depot:nodiff} and \ref{fig:depot:diff} show the
suspended sediment concentration along the channel simulated, in
comparison with the analytical solution.

\begin{figure}[h]
 \centering
 \includegraphicsmaybe{[width=0.7\textwidth]}{../img/profile_conc_depot.png}
 \caption{Concentration along the channel without diffusion,
  simulation vs analytical solution.}
 \label{fig:depot:nodiff}
\end{figure}

\begin{figure}[h]
 \centering
 \includegraphicsmaybe{[width=0.7\textwidth]}{../img/profile_conc_diff_depot.png}
 \caption{Concentration along the channel with diffusion,
  simulation vs analytical solution.}
 \label{fig:depot:diff}
\end{figure}

\subsection{Conclusion}

The deposition flux is well represented.
