
"""
Validation script for Deposit channel suspension
"""
from vvytel.vnv_study import AbstractVnvStudy
from execution.telemac_cas import TelemacCas, get_dico

class VnvStudy(AbstractVnvStudy):
    """
    Class for validation of Deposit channel suspension
    """

    def _init(self):
        """
        Defines the general parameter
        """
        self.rank = 2
        self.tags = ['mascaret', 'courlis']

    def _pre(self):
        """
        Defining the studies
        """
        # Deposit channel suspension test case non-permanent kernel
        self.add_study('vnv_1',
                       'mascaret',
                       'rezodt.xcas')

    def _check_results(self):
        """
        Post-treatment processes
        """

        # Comparison with reference file
        self.check_epsilons('vnv_1:listing_courlis',
                            'ref.listingcourlis',
                            eps=[1.E-8],
                            masc=True)


    def _post(self):
        """
        Post-treatment processes
        """
