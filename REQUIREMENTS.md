# Prerequisites and dependencies

Here are the minimum versions of the TELEMAC prerequisites and dependencies
needed to compile and run its modules, as well as for pre/post-processing and
validation.

For compilation and execution only, i.e. in sequential mode and without the
Python API, the only requirements are a Fortran compiler and a Python
interpreter with NumPy and SciPy.

The versions indicated below are the minimum versions for which the TELEMAC
system has been validated. However, the system should work with more recent
versions of most of these dependencies.

Under Linux, with the exception of AED2 and GOTM, most software and libraries
can be installed using the OS package manager. However, users of Debian and
Debian-based distributions must build MED manually in order to get adequate
Fortran support for this library.

## Mandatory

- Python 3.8+
- NumPy 1.19+
- Scipy 1.5+

## Optionals

To use post-processing features and to validate the system:
- Matplotlib 3.5+

To add parallelism support:
- METIS 5.1.0
- MPI:
    - Open MPI (Linux only)
    - MPICH
    - Intel MPI
    - MS-MPI (Windows only)

To add MED support:
- HDF5 1.10+
- [MED 4.1](https://gitlab.pam-retd.fr/otm/telemac-mascaret/-/package_files/443/download)

To add MUMPS support:
- OpenBLAS 0.3.3+
- ScaLAPACK 2.0.2+
- MUMPS 4.10+

To add AED2 support:
- [AED2 1.1.0-telemac](https://gitlab.pam-retd.fr/otm/telemac-mascaret/-/package_files/444/download)

To add GOTM support:
- [GOTM 2019-06-14-opentelemac](https://gitlab.pam-retd.fr/otm/telemac-mascaret/-/package_files/445/download)

The following Python packages might also be needed for some of the Python
modules and scripts:
- Mpi4py
- Jupyter
- openpyxl
- OWSLib
- pandas
- pyproj
- Rtree
- Seaborn
- GDAL
- Fiona
- Rasterio
- Shapely
- GeoPandas
- Pylint
- doxypypy
