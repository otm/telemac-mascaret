"""@author TELEMAC-MASCARET Consortium

   @brief Create an atmospheric conditions file from a weather data file
          (mesh format) by interpolating on a given geometry file.
"""

from os import path

import numpy as np

from data_manip.conversion.convert_utm import to_latlon
from data_manip.extraction.telemac_file import TelemacFile
from utils.progressbar import ProgressBar
from utils.exceptions import TelemacException


def generate_atm_parser(subparser):
    """
    Add options for generate_atm to a given parser.

    @param subparser (ArgumentParser) A parser

    @return (ArgumentParser) The updated parser
    """
    parser = subparser.add_parser(
        "generate_atm",
        help="Map weather type data (varying in space and time) contained "
        "into a mesh file, onto the geometry of another mesh",
    )
    parser.add_argument(
        "geo_file",
        default="",
        help="Geometry file on which to map the weather type file",
    )
    parser.add_argument(
        "weather_file",
        default="",
        help="Weather data file from which to extract weather informations",
    )
    parser.add_argument(
        "atm_file",
        default="",
        help="Output file containing the weather information applied on the "
        "geometry file",
    )
    parser.add_argument(
        "--utm2ll",
        dest="utm2ll",
        default=None,
        help="Write the output mesh coordinates in lat-long, assuming a "
        "geometry file in UTM and a weather file in lat-long.",
    )

    return subparser


def generate_atm(geo_file, weather_file, atm_file, utm2ll=""):
    """
    Create an atmospheric conditions file from a weather data file (mesh format)
    by interpolating on a given geometry file.

    Args:
        geo_file (str): Geometry file.
        weather_file (str): Mesh file containing the weather data.
        atm_file (str): Name of the atmospheric file to generate.
        utm2ll (str): If not empty, must contain the UTM zone number and letter
        of the geometry file coordinate system, e.g. '28H', to write the
        coordinates of the output mesh in lat-long. In this case, the weather
        file must be in lat-long.
    """
    # Check for the geometry file
    if not path.exists(geo_file):
        raise TelemacException(
            "... the provided geometry file does not seem to exist: "
            f"{geo_file}\n\n"
        )

    # Get the mesh coordinates by converting them to lat-long if necessary
    geo = TelemacFile(geo_file)
    if utm2ll:
        zone = int(utm2ll[:-1])
        zone_letter = utm2ll[-1]
        x, y = to_latlon(geo.meshx, geo.meshy, zone, zone_letter)
        x_orig, y_orig = to_latlon(geo.x_orig, geo.y_orig, zone, zone_letter)
    else:
        x = geo.meshx
        y = geo.meshy
        x_orig = geo.x_orig
        y_orig = geo.y_orig

    # Check for the weather file
    if not path.exists(weather_file):
        raise TelemacException(
            "... the provided weather file does not seem to exist: "
            f"{weather_file}\n\n"
        )
    weather = TelemacFile(weather_file)

    # Create the atmopsheric file
    atm = TelemacFile(atm_file, access="w", overwrite=True)

    # Meta data and variable names
    title = ""
    varnames = []
    varunits = []
    if "WIND VELOCITY U" in weather.varnames or "WINDX" in weather.varnames:
        varnames.append("WINDX")
        varunits.append("M/S")
    if "WIND VELOCITY V" in weather.varnames or "WINDY" in weather.varnames:
        varnames.append("WINDY")
        varunits.append("M/S")
    if "SURFACE PRESSURE" in weather.varnames or "PATM" in weather.varnames:
        varnames.append("PATM")
        varunits.append("UI")
    if "AIR TEMPERATURE " in weather.varnames or "TAIR" in weather.varnames:
        varnames.append("TAIR")
        varunits.append("DEGREES")
    if len(varnames) == 0:
        raise TelemacException(
            "There are no meteorological variables to convert!"
        )

    # Write the header
    nvar = len(varnames)
    atm.set_header(title, nvar, varnames, varunits)

    # Set the weather mesh size and connectivity
    datetime = weather.datetime
    ndim = weather.ndim
    typ_elem = weather.typ_elem
    nplan = weather.nplan
    ndp3 = weather.ndp3
    nptfr = geo.nptfr
    nptir = geo.nptir
    npoin3 = geo.npoin2 * nplan if nplan > 1 else geo.npoin2
    knolg = geo.knolg

    if nplan > 1:
        nelem3 = geo.nelem2 * (nplan - 1)
        ikle3 = np.repeat(
            geo.npoin2 * np.arange(nplan - 1), geo.nelem2 * ndp3
        ).reshape((geo.nelem2 * (nplan - 1), ndp3)) + np.tile(
            np.add(
                np.tile(geo.ikle2, 2),
                np.repeat(geo.npoin2 * np.arange(2), geo.ndp2),
            ),
            (nplan - 1, 1),
        )
        ipob3 = np.ravel(
            np.add(
                np.repeat(geo.ipob2, nplan).reshape((geo.npoin2, nplan)),
                geo.npoin2 * np.arange(nplan),
            ).T
        )
    else:
        nelem3 = geo.nelem2
        ikle3 = geo.ikle3
        ipob3 = geo.ipob3

    # Write the mesh
    atm.set_mesh(
        ndim,
        typ_elem,
        ndp3,
        nptfr,
        nptir,
        nelem3,
        npoin3,
        ikle3,
        ipob3,
        knolg,
        x,
        y,
        nplan,
        datetime[:3],
        datetime[3:],
        x_orig,
        y_orig,
        geo.meshz,
    )

    # Write the atmospheric data, one time step at a time to support large files
    points = np.column_stack((geo.meshx, geo.meshy))
    ntimestep = len(weather.times)
    pbar = ProgressBar(maxval=ntimestep).start()
    for record, time in enumerate(weather.times):
        for var_idx, (var, unit) in enumerate(zip(varnames, varunits)):
            data = weather.get_data_on_points(var, record, points)
            atm.add_data(var, unit, time, record, var_idx == 0, data)
        pbar.update(record)
    pbar.finish()

    # Close atm_file
    atm.close()
