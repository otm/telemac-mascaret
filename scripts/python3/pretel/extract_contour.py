""" Functions to extract the contour of a mesh """

from collections import OrderedDict, defaultdict

import numpy as np
from matplotlib.path import Path

from data_manip.extraction.telemac_file import TelemacFile


def sort_contours(contours):
    """
    Generate a dictionary of sorted contours where the key is the index of an
    outer contour, and the values are its corresponding inner contours.

    @param contours (list) List of contours.

    @return dict Dictionary of inner contour indices for each outer contour.
    """
    # Initialize an empty dictionary to store the result
    sorted_contours = {}
    all_inner_contours = []

    # Loop over all contours
    for i, contour in enumerate(contours):
        # Create a Path object for the outer contour
        outer_contour = Path(np.array(contour))

        # Initialize an empty list to store the indices of the inner contours
        inner_contours = []

        # Loop over all other contours
        for j, other_contour in enumerate(contours):
            # Skip if it's the same contour
            if i == j:
                continue

            # Create a Path object for the potential inner contour
            inner_contour = Path(np.array(other_contour))

            # Check if the outer contour contains the inner contour
            if outer_contour.contains_path(inner_contour):
                # If it does, add the index of the inner contour to the list
                # and remove it from the list of sorted contours
                inner_contours.append(j)

        all_inner_contours.extend(inner_contours)
        sorted_contours[i] = inner_contours

    for i in all_inner_contours:
        sorted_contours.pop(i, None)

    return sorted_contours


def signed_area(contour):
    """
    Calculate the signed area of a polygon using the Shoelace theorem.

    @param contour (list) List of vertex coordinates forming the polygon.
    @return float The signed area of the polygon.
    """
    x = np.array(contour)[:, 0]
    y = np.array(contour)[:, 1]
    return 0.5 * np.sum(x[:-1] * y[1:] - x[1:] * y[:-1])


def extract_boundaries(triangles):
    """
    Extract the boundaries of a mesh defined by a list of triangles.

    @param triangles (np.ndarray) A 1D array of triangles with each triangle
    being an array of 3 node IDs.

    @return np.ndarray A 1D array of edges.
    """
    # Extract and sort all edges
    edges = np.sort(
        np.vstack(
            [triangles[:, [0, 1]], triangles[:, [0, 2]], triangles[:, [1, 2]]]
        ),
        axis=1
    ).astype(np.int64)

    # Use the Cantor pairing function to create edge IDs
    k1, k2 = edges[:, 0], edges[:, 1]
    edge_ids = (k1 + k2) * (k1 + k2 + 1) // 2 + k2

    # Extract unique edges
    edge_ids, indices, counts = np.unique(
        edge_ids, return_index=True, return_counts=True
    )
    indices = indices[counts == 1]
    return edges[indices]


def get_first_point(tel, bnd_points):
    """
    Determine the southwest points among points given trough their indexes

    @param tel (TelemacFile) a Telemac file
    @param bnd_points (numpy array of shape (number of points)) contains indexes
    of points
    @return first_bnd_pt_index (integer) index of the southwest point
    """

    x_plus_y = tel.meshx[bnd_points] + tel.meshy[bnd_points]

    southwest_bnd_pts_index = bnd_points[
        np.where(x_plus_y == x_plus_y.min())[0]
    ]

    if southwest_bnd_pts_index.shape[0] == 1:
        first_bnd_pt_index = southwest_bnd_pts_index[0]
    else:
        first_bnd_pt_index = southwest_bnd_pts_index[
            np.where(
                tel.meshx[southwest_bnd_pts_index]
                == tel.meshx[southwest_bnd_pts_index].min()
            )[0][0]
        ]

    return first_bnd_pt_index

def extract_contour(tel_file_path: str):
    """
    Extract the contour of a given mesh.

    @param tel_file_path (str) Path to the mesh file

    @returns (list) A list of polygons, one for each domain.
    """
    tel = TelemacFile(tel_file_path)
    vertices = np.vstack((tel.meshx, tel.meshy)).T

    # Extract boundary edges and nodes
    boundary_edges = extract_boundaries(tel.ikle2)
    boundary_nodes = set(np.unique(np.hstack(boundary_edges)))

    # Create a list of the neighbours of each node for quick search
    node_neighbours = defaultdict(set)
    for edge in boundary_edges:
        node_neighbours[edge[0]].add(edge[1])
        node_neighbours[edge[1]].add(edge[0])

    # Group boundary nodes coordinates into contours
    contours = []
    while len(boundary_nodes) > 0:
        next_vertex = get_first_point(tel, np.array(list(boundary_nodes)))
        boundary_nodes.remove(next_vertex)
        contour = [vertices[next_vertex].tolist()]
        while True:
            neighbours = node_neighbours[next_vertex].intersection(
                boundary_nodes
            )
            if len(neighbours) == 0:
                break
            next_vertex = neighbours.pop()
            boundary_nodes.remove(next_vertex)
            contour.append(vertices[next_vertex].tolist())

        # Ensure the contour is closed and append it to the list of contours
        contour.append(contour[0])
        contours.append(contour)

    # Build the list of domains while ensuring proper contours orientation
    sorted_contours = sort_contours(contours)
    domains = []
    for outer, inners in sorted_contours.items():
        domain = []
        contour = contours[outer]
        area = signed_area(contour)
        if area < 0:
            contour = contour[::-1]
        domain.append(contour)
        if len(inners) > 0:
            for i in inners:
                contour = contours[i]
                area = signed_area(contour)
                if area > 0:
                    contour = contour[::-1]
                domain.append(contour)
        domains.append(domain)

    return domains


def write_gis_file(
    polygons, file_name="contour.shp", output_driver="ESRI Shapefile", crs=None
):
    """
    Write polygon GIS file (multiple polygon is allowed)

    @param polygon (list of lists of tuples) list of polygons which are
    described by their vertex coordinates (external vertices are clockwise
    sorting and holes are anticlockwise sorting)
    @param file_name (string) name of the written file (can contain a path with
    os.sep)
    @param crs_given (boolean) optional parameter to indicate the presence of a
    coordinate reference system (default: False)
    @param output_driver (string) define the format of the GIS file used by
    fiona.  To know available drivers, use fiona.available_drivers attribute of
    Fiona library
    @param crs (string) EPSG (European Petroleum Survey Group) code of the
    coordinate reference system
    """
    import fiona

    schema = {
        "geometry": "Polygon",
        "properties": OrderedDict([("name", "str")]),
    }

    dict_polygons = []

    for i, polygon in enumerate(polygons):

        dict_poly = {
            "geometry": {"type": "Polygon", "coordinates": polygon},
            "properties": OrderedDict([("name", "polygon_" + str(i))]),
        }

        dict_polygons.append(dict_poly)

    with fiona.open(
        file_name, "w", driver=output_driver, crs=crs, schema=schema
    ) as shp:
        for dict_poly in dict_polygons:
            shp.write(dict_poly)


def main():
    import os

    from utils.files import recursive_glob

    modules = ["telemac2d", "telemac3d", "gaia", "tomawac"]

    for module in modules:
        med_files = recursive_glob(
            os.path.join(os.environ["HOMETEL"], "examples", module), "*.med"
        )
        slf_files = recursive_glob(
            os.path.join(os.environ["HOMETEL"], "examples", module), "*.slf"
        )

        geo_files = med_files + slf_files

        folder = os.path.join(".", module)

        if not os.path.isdir(folder):
            os.mkdir(folder)

        exception_prefix = [
            "f2d",
            "r2d",
            "t2d",
            "ini",
            "pre",
            "fsp",
            "spe",
            "f3d",
            "r3d",
            "t3d",
            "ref",
        ]

        for geo in geo_files:
            prefix = geo.split(os.sep)[-1].split(".")[-2]
            mesh_format = geo.split(os.sep)[-1].split(".")[-1]
            geo_name = prefix + "_" + mesh_format + ".shp"
            if prefix == "fom_spe_friction" or prefix[0:3] in exception_prefix:
                continue

            geo_folder = os.sep.join(geo.split(os.sep)[:-1])
            geo_name = os.path.join(folder, geo_name)
            if mesh_format == "slf":
                cli_file = os.path.join(geo_folder, prefix + ".cli")
            else:
                cli_file = os.path.join(geo_folder, prefix + ".bnd")
            if os.path.isfile(cli_file):
                bnd_file = cli_file
            else:
                bnd_file = None
            if not os.path.isfile(geo_name):
                print(geo, bnd_file)
                domains_bnd = extract_contour(geo)

                write_gis_file(domains_bnd, file_name=geo_name)

                print("\n")


if __name__ == "__main__":
    main()
