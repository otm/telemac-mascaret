"""@author TELEMAC-MASCARET Consortium

   Manipulation of Telemac files (mesh, results files)
"""
from collections import OrderedDict
from datetime import datetime
from os import path, remove

import numpy as np

from data_manip.conversion import convert_utm as utm
from data_manip.extraction.telemac_file import TelemacFile
from pretel.meshes import needs_double_precision
from utils.exceptions import TelemacException
from utils.progressbar import ProgressBar

MERGE_KINDS = ['time', 'var']

def scan(tel_file, bnd_file, data):
    """
    Ascii dump of content of TelemacFile

    @param tel_file (str) Name of the telemac file
    @param bnd_file (str) Name of the boundary file
    @param data (boolean) If True display data information as well
    """

    res = TelemacFile(tel_file, bnd_file=bnd_file)

    res.print_info(full=data)

def diff(tel_file1, tel_file2, diff_file):
    """
    Creates a file that is the difference between two files (file1 - file2) of
    the same shape (same mesh same number of variable and same number of
    records)

    @param tel_file1 (str) Name of the first file
    @param tel_file1 (str) Name of the second file
    @param diff_file (str) Name of the file containing the difference
    """
    res1 = TelemacFile(tel_file1)
    res2 = TelemacFile(tel_file2)
    if path.exists(diff_file):
        print(" ~> Deleting eixisting diff_file: {}".format(diff_file))
        remove(diff_file)
    dif = TelemacFile(diff_file, access='w')

    res1.read()
    res2.read()

    if res1._values.shape != res2._values.shape:
        raise TelemacException(
            "Different shape between files (ntimestep, nvar, npoin):\n"
            "{}: {}\n{}: {}"
            .format(tel_file1, res1._values.shape,
                    tel_file2, res2._values.shape))

    dif.read(res1)

    for time in range(res1.ntimestep):
        for var in range(res1.nvar):
            dif._values[time, var, :] -= res2._values[time, var, :]

    dif.write()

def diff_ascii(tel_file1, tel_file2, epsilon):
    """
    print a summary of the diff between two files

    @param tel_file1 (str) Path of file1
    @param tel_file2 (str) Path of file2
    @param epsilon (float) Threshold below which two values are consireded
    equals
    """

    res1 = TelemacFile(tel_file1)
    fname1 = path.basename(res1.file_name)
    res2 = TelemacFile(tel_file2)
    fname2 = path.basename(res2.file_name)

    failed = False

    print('\n\nHeader differences: \n'+72*'~'+'\n')

    # ~~> File formats
    if res1.endian != res2.endian:
        print('\n  <> File endianess:\n')
        print('     + {} is in {}'.format(fname1, res1.endian))
        print('     + {} is in {}'.format(fname2, res2.endian))
    if res1.fformat != res2.fformat:
        print('\n  <> File formats:\n')
        precision = "SINGLE" if res1 in ['SERAFIN'] else "DOUBLE"
        print('     + {} is {} PRECISION'\
              .format(fname1, precision))
        precision = "SINGLE" if res2 in ['SERAFIN'] else "DOUBLE"
        print('     + {} is {} PRECISION'\
              .format(fname2, precision))

    # ~~> File geometries
    mes = ''
    if res1.nelem2 != res2.nelem2:
        mes = mes + '     + nelem2 = {} in {}\n'.format(res1.nelem2, fname1)
        mes = mes + '     * nelem2 = {} in {}\n'.format(res2.nelem2, fname2)
        mes = mes + '\n'
    if res1.npoin2 != res2.npoin2:
        mes = mes + '     + npoin2 = {} in {}\n'.format(res1.npoin2, fname1)
        mes = mes + '     * npoin2 = {} in {}\n'.format(res2.npoin2, fname2)
        mes = mes + '\n'
    if res1.nplan != res2.nplan:
        mes = mes + '     + nplan = {} in {}\n'.format(res1.nplan, fname1)
        mes = mes + '     * nplan = {} in {}\n'.format(res2.nplan, fname2)
        mes = mes + '\n'
    if mes != '':
        print('\n  <> Geometry:\n'+mes)
        failed = True
        print('\n  /!\\different geometries. The files are not comparables.\n')
        return failed

    # ~~> File trangulations
    diff1 = res1.ikle2 - res2.ikle2
    if np.argwhere(diff1 > [0, 0, 0]):
        print('\n  <> 2D Triangulation:\n')
        print('     + number of mismatches: '+\
                repr(len(np.argwhere(diff1 == [0, 0, 0]).T[0])))
        print('     + mismatched elements: '+\
                repr(np.argwhere(diff1 == [0, 0, 0]).T[0][::3]))

    # ~~> File geo-localisation
    diff1 = np.sqrt(np.power((res1.meshx-res2.meshx), 2) + \
                    np.power((res1.meshy-res2.meshy), 2))
    points = np.argwhere(diff1 > epsilon).ravel()
    if points:
        print('\n  <> Geo-Localisation:\n')
        print('     + maximum distance between points : '+\
              str(max(diff1[points])))
        pt_th = 100*len(points)/len(diff1)
        print('     + number of points above clocelyness threshold : '+\
              str(len(points))+' (i.points. '+str(pt_th)+'% of points)')
        print('     + node numbers : '+
              repr(np.arange(res1.npoin3)[points]))

    # ~~> File contents

# <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<
# ~~~~ Data differences ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

    var_failed = True
    mes = '\n  <> List of variable names:\n'
    cmn_vars = []
    mes = mes + '\n     + '+fname1
    for var in res1.varnames:
        if var in res2.varnames:
            mes = mes + '\n        = '+var
            cmn_vars.append(var)
        else:
            mes = mes + '\n        * '+var
            var_failed = True
    mes = mes + '\n     + '+fname2
    for var in res2.varnames:
        if var in res1.varnames:
            mes = mes + '\n        = '+var
        else:
            mes = mes + '\n        * '+var
            var_failed = True
    print(mes)

    if not cmn_vars:
        failed = True
        print('\n  /!\\ no common variables. The files are not comparables.\n')

    # ~~> File reference dates and times
    if max(np.array(res1.datetime) - np.array(res2.datetime)) > 0:
        print('\n  <> Different reference dates:')
        print('     + {}: {}'.format(fname1, res1.datetime))
        print('     + {}: {}'.format(fname2, res2.datetime))

    # ~~> File time frames
    time_failed = False
    mes = '\n  <> List of time frames:\n'
    if res1.ntimestep != res2.ntimestep:
        print('\n  <> Different Number of time steps:')
        print('     + {}: {}'.format(fname1, res1.ntimestep))
        print('     + {}: {}'.format(fname2, res2.ntimestep))
        time_failed = True
    # ~~> correct if not bypassed
    diff1 = np.setdiff1d(res1.times, res2.times)
    if diff1.size != 0:
        time_failed = True
        mes = mes + '\n     + frames only in '+fname1+' : '+\
                ', '.join(['{0:.2f}'.format(i) for i in diff1])
    diff1 = np.setdiff1d(res2.times, res1.times)
    if diff1.size != 0:
        time_failed = True
        mes = mes + '\n     + frames only in '+fname2+' : '+\
                ', '.join(['{0:.2f}'.format(i) for i in diff1])
    times = np.intersect1d(res1.times, res2.times)
    if times.size != 0:
        mes = mes + '\n     + frames in both files: '+\
              ', '.join([str(i) for i in times])
        print(mes)
    else:
        failed = True
        print('\n  /!\\ no common time frames. '\
                'The files are not comparables.\n')
        return failed

    if failed:
        return failed

    print('\n\nData differences: \n'+72*'~'+'\n')

    # Do compraison for common times and variables
    data_failed = False
    for time in times:
        itime1 = np.where(res1.times == time)[0][0]
        itime2 = np.where(res2.times == time)[0][0]
        for var in cmn_vars:
            var1 = res1.get_data_value(var, itime1)
            var2 = res2.get_data_value(var, itime2)
            time1 = res1.times[itime1]
            time2 = res2.times[itime2]
            diff1 = np.absolute(var1 - var2).ravel()
            points = np.argwhere(diff1 > epsilon).ravel()
            if points.size != 0:
                data_failed = True
                print("\n  <> Frame: {itime1} / {itime2} (times: {time1} "
                      "/ {time2}), Variable: {var}\n"\
                      .format(itime1=itime1,
                              itime2=itime2,
                              time1=time1,
                              time2=time2,
                              var=var))
                print('     + max difference: ', max(diff1[points]))
                print('     + number of values above threshold : '+
                      str(len(points))+' (i.points. '+
                      str(100*len(points)/len(diff1))+
                      '% of points)')
                print('     + node numbers :          '+
                      repr(np.arange(res1.npoin3)[points]))
                print('     + values at those nodes : '+
                      repr(diff1[np.arange(res1.npoin3)[points]]))
    if not data_failed:
        print('  <> No differences in data to the epsilon: {}'.format(epsilon))

    return data_failed or var_failed or time_failed or failed


def alter(input_file, output_file,
          bnd_file=None,
          title=None, in_datetime=None,
          toggle_precision=False,
          toggle_endian=False,
          add_x=0., mul_x=1., add_y=0., mul_y=1.,
          orig=[0, 0],
          rotate=None, rot_pt=[0, 0], center=False,
          proj=None,
          sph2ll=False, ll2sph=False,
          longitude=None, latitude=None,
          utm2ll=False, ll2utm=False,
          zone=None, zone_letter=None,
          in_vars=None, rename_var=None,
          modif_var=None, add_var=0., mul_var=1.,
          times=None,
          tfrom=0, tstep=1, tend=-1,
          reset_time=False, add_time=0., mul_time=1.,
          auto_precision=True,
          force=False):
    """
    Apply modifications to a TelemacFile to create a new one

    @param input_file (str) Input file
    @param output_file (str) Output file
    @param bnd_file (str) Boundary file
    @param title (str) Title to set
    @param in_datetime (str) Date and time to set (format: YYYY-MM-DD HH:MM:SS)
    @param input_file (str) Input file
    @param toggle_precision (bool) If True will invert precision
    @param toggle_endian (bool) If True will invert endianess
    @param add_x (float) Value added to X coordinates (X=mul_x*X+add_x)
    @param mul_x (float) Value multiplicated to X coordinates (X=mul_x*X+add_x)
    @param add_y (float) Value added to Y coordinates (Y=mul_y*Y+add_y)
    @param mul_y (float) Value multiplicated to Y coordinates (Y=mul_y*Y+add_y)
    @param orig (int, int) Origin of coordinates (x_orig, y_orig in file)
    @param rotate (float) Angle for rotation (counter clockwise) in degree
    @param rot_pt (float, float) Coordinates of the point arond which to do the
    rotation (default (0,0))
    @param center (bool) If True will use center of the mesh as rotation point
    @param sph2ll (bool) Converter coordinates from spherical to long/lat
    @param ll2sph (bool) Converter coordinates from long/lat to spherical
    @param longitude (int) longitude for conversion
    @param latitude (int) atitude for conversion
    @param utm2ll (bool) Converter coordinates from UTM to long/lat
    @param ll2utm (bool) Converter coordinates from long/lat to UTM
    @param zone (int) zone for UTM conversion
    @param zone_letter (int) zone letter for UTM conversion
    @param in_vars (str) List of variables to keep in file ("," separated)
    @param rename_var (str) Command to rename a variable
                            (format: old_name=new_name)
    @param modif_var (str) Name of the variable to modify
                           (VAR=mul_var*VAR+add_var)
    @param add_var (float) Value added to variable
    @param mul_var (float) Value multiplicated to variable
    @param times (int) list of times to add in the output file
    @param tfrom (int) First record to add in output file
    @param tstep (int) Step between each record to add in output file
    @param tend (int) Last Record to add in output file
    @param reset_time (bool) If True reset first record time to zero and
                             substract its value to all the other times
    @param add_time (float) Value added to times (T=mul_time*T+add_time)
    @param mul_time (float) Value multiplicated to times (T=mul_time*T+add_time)
    @param force (bool) Will remove output file if it already exist
    """
    inres = TelemacFile(input_file, bnd_file=bnd_file)

    fformat = inres.fformat
    if toggle_precision:
        if inres.fformat == 'SERAFIN':
            print(" ~> Switching to double precision")
            fformat = 'SERAFIND'
        elif inres.fformat == 'SERAFIND':
            print(" ~> Switching to single precision")
            fformat = 'SERAFIN'
        else:
            print("Precision can only be switched if the file "
                  "is in SERAFIN or SERAFIND")

    if toggle_endian:
        if inres.fformat not in ['SERAFIN', 'SERAFIND']:
            print("Endianess can only be switched if the file "
                  "is in SERAFIN or SERAFIND")
        else:
            endian = inres.get_endianess()
            if endian == 'LITTLE_ENDIAN':
                print(" ~> Switching to BIG_ENDIAN")
                inres.set_endianess('BIG_ENDIAN')
            else:
                print(" ~> Switching to LITTLE_ENDIAN")
                inres.set_endianess('LITTLE_ENDIAN')

    # Changing title
    if title is None:
        title = inres.title
    else:
        print(" ~> New title: "+title)

    # Changing date
    if in_datetime is not None:
        my_datetime = datetime.strptime(in_datetime, '%Y-%m-%d %H:%M:%S')
        date = [my_datetime.year, my_datetime.month, my_datetime.day,
                my_datetime.hour, my_datetime.minute, my_datetime.second]
        print(" ~> New date: {}".format(my_datetime))
    else:
        date = inres.datetime

    # Defining origin
    out_orig = [inres.x_orig, inres.y_orig]

    if orig != [0, 0]:
        out_orig = orig

    coord_modif = False

    if mul_x != 1. or add_x != 0.:
        print(" ~> modification of coord x: {}*x+{}".format(mul_x, add_x))
        meshx = inres.meshx*mul_x + add_x
        coord_modif = True
    else:
        meshx = inres.meshx

    if mul_y != 1. or add_y != 0.:
        print(" ~> modification of coord y: {}*y+{}".format(mul_y, add_y))
        meshy = inres.meshy*mul_y + add_y
        coord_modif = True
    else:
        meshy = inres.meshy

    if rotate is not None:
        if center:
            rot_x = (np.max(meshx) - np.min(meshx))/2
            rot_y = (np.max(meshy) - np.min(meshy))/2
        else:
            rot_x = rot_pt[0]
            rot_y = rot_pt[1]
        print(" ~> Rotation of {} around ({},{})".format(rotate, rot_x, rot_y))
        angle = rotate * np.pi/180.
        x = np.cos(angle)*(meshx -rot_x) - np.sin(angle)*(meshy -rot_y)
        y = np.sin(angle)*(meshx -rot_x) + np.cos(angle)*(meshy -rot_y)
        meshx = x + rot_x
        meshy = y + rot_y
        coord_modif = True

    if proj is not None:
        try:
            from pyproj import Transformer, crs
        except ImportError:
            raise TelemacException("pyproj is mandatory to perform geographic "
                                   "coordinate conversions")

        projs = proj.split("/")
        if len(projs) != 2:
            raise TelemacException("Wrong coordinate systems format. It "
                                   "should be written as follows: CRS1/CRS2")
        crs_from = crs.CRS(projs[0])
        crs_to = crs.CRS(projs[1])
        print(f" ~> Converting from {crs_from.name} to {crs_to.name}")
        transformer = Transformer.from_crs(crs_from, crs_to)
        meshx, meshy = transformer.transform(meshx, meshy)
        coord_modif = True

    # converter mesh coordinates
    if sph2ll:
        print(" ~> Converting from spherical to Longitude/Latitude")
        meshx, meshy = spherical2longlat(meshx, meshy, longitude, latitude)
        coord_modif = True
    if ll2sph:
        print(" ~> Converting from Longitude/Latitude to spherical")
        meshx, meshy = longlat2spherical(meshx, meshy, longitude, latitude)
        coord_modif = True
    if ll2utm:
        print(" ~> Converting from Longitude/Latitude to UTM")
        meshx, meshy = longlat2utm(meshx, meshy, zone, zone_letter)
        coord_modif = True
    if utm2ll:
        print(" ~> Converting from UTM to Longitude/Latitude")
        meshx, meshy = utm2longlat(meshx, meshy, zone, zone_letter)
        coord_modif = True

    if coord_modif and fformat == "SERAFIN" and auto_precision:
        is_double = needs_double_precision(meshx, meshy, inres.ikle3)
        if is_double:
            print("WARNING:")
            print("Changing format to SERAFIND because segment are too small")
            fformat = "SERAFIND"

    # Comma-separated list of variable to keep
    if in_vars is not None:
        out_vars = []
        in_varnames = in_vars.split(",")
        print(" ~> Modif in variables:")
        for name, unit in zip(inres.varnames, inres.varunits):
            if name in in_varnames:
                print(" "*4+"Using {} {}".format(name, unit))
                out_vars.append((name, unit))
            else:
                print(" "*4+"Removing {} {}".format(name, unit))
    else:
        out_vars = list(zip(inres.varnames, inres.varunits))
    in_vars = out_vars

    # Renaming variable
    if rename_var is not None:
        old_name, new_name = rename_var.split("=")
        print("{} -> {}".format(old_name, new_name))

        out_vars = []
        renamed = False
        for name, unit in in_vars:
            name2 = name.replace(old_name, new_name)
            if name2 != name:
                renamed = True
                print(" ~> Renaming {} -> {}".format(name, name2))
            out_vars.append((name2, unit))
        if not renamed:
            raise TelemacException(
                "Issue with you renaming no variable matched "
                "the conversion:\n{}".format(rename_var))

    # Records to keep (from, stop, step)
    if times is not None:
        out_records = []
        for itime, in_time in enumerate(inres.times):
            for time in times:
                if abs(time - in_time) < 1e-06:
                    out_records.append(itime)

        if out_records == []:
            raise TelemacException(\
                "Could not find times matching your input:\n"\
                "files times: {}\n"\
                "Selected times: {}".format(inres.times, times))
    else:
        if tend < 0:
            tend = inres.ntimestep + tend
        if tfrom < 0:
            tfrom = inres.ntimestep + tfrom

        out_records = range(tfrom, tend+1, tstep)

    # Build times array
    out_times = inres.times[out_records]

    if len(out_times) != inres.ntimestep:
        records = ["{}".format(i) for i in out_records]
        print(" ~> New range of records: [{}]".format(", ".join(records)))
        print(" ~> New range of times: {}".format(out_times))

    if reset_time:
        out_times -= out_times[0]
        print(" ~> reseting time from 0: {}".format(out_times))

    out_times = out_times*mul_time + add_time

    if mul_time != 1. or add_time != 0.:
        print(" ~> modification of times: {}*time+{}"\
              .format(mul_time, add_time))

    # Compute out_bnd_file name
    if bnd_file is not None:
        root, _ = path.splitext(output_file)
        out_ext = '.bnd' if fformat == 'MED' else '.cli'
        out_bnd_file = root + out_ext
    else:
        out_bnd_file = None

    outres = TelemacFile(
        output_file,
        bnd_file=out_bnd_file,
        fformat=fformat,
        access='w',
        overwrite=force,
    )
    nvar = len(out_vars)
    varnames = [name[0] for name in out_vars]
    varunits = [unit[1] for unit in out_vars]
    outres.set_header(title, nvar, varnames, varunits)
    outres.set_mesh(inres.ndim, inres.typ_elem, inres.ndp3,
                    inres.nptfr, inres.nptir, inres.nelem3,
                    inres.npoin3, inres.ikle3, inres.ipob3,
                    inres.knolg, meshx, meshy,
                    inres.nplan, date[:3], date[3:],
                    out_orig[0], out_orig[1], inres.meshz)

    if modif_var in varnames:
        print(f' ~> Modifying {modif_var}: {mul_var}*val+{add_var}')

    ntimestep = len(out_records)
    pbar = ProgressBar(maxval=ntimestep).start()
    for itime, record in enumerate(out_records):
        out_time = out_times[itime]
        for ivar, (in_varname, _) in enumerate(in_vars):
            value = inres.get_data_value(in_varname, record)
            if in_varname == modif_var:
                value = value * mul_var + add_var
            if rotate is not None and 'VELOCITY' in in_varname:
                vitx = inres.get_data_value('VELOCITY U', record)
                vity = inres.get_data_value('VELOCITY V', record)
                if in_varname == 'VELOCITY U':
                    value = np.cos(angle) * vitx - np.sin(angle) * vity
                elif in_varname == 'VELOCITY V':
                    value = np.sin(angle) * vitx + np.cos(angle) * vity
            out_varname, out_varunit = out_vars[ivar]
            outres.add_data(
                out_varname,
                out_varunit,
                out_time,
                itime,
                ivar == 0,
                value,
            )
        pbar.update(itime)
    pbar.finish()

    if bnd_file is not None:
        outres.import_group_info(inres)

    inres.close()
    outres.close()


def spherical2longlat(x, y, longitude, latitude):
    """
    convert coordinates from spherical to longitude and latitude

    @param x (np.array) X coordinates
    @param y (np.array) Y coordinates
    @param longitude (float) Longitude
    @param latitude (float) Latitude

    """
    radius = 6371000.
    long0 = np.deg2rad(longitude)
    lat0 = np.deg2rad(latitude)
    const = np.tan(lat0/2. + np.pi/4.)
    meshx = np.rad2deg(x/radius + long0)
    expo = np.exp(y/radius)
    meshy = np.rad2deg(2.*np.arctan(const*expo) - np.pi/2.)

    return meshx, meshy

def longlat2spherical(x, y, longitude, latitude):
    """
    convert coordinates from Longitude and latitude to Spherical

    @param x (np.array) X coordinates
    @param y (np.array) Y coordinates
    @param longitude (float) Longitude
    @param latitude (float) Latitude

    """
    radius = 6371000.
    long0 = np.deg2rad(longitude)
    lat0 = np.deg2rad(latitude)
    meshx = radius * (np.deg2rad(x) - long0)
    meshy = radius * \
              (np.log(np.tan(np.deg2rad(y)/2. + np.pi/4.)) \
                      - np.log(np.tan(lat0/2. + np.pi/4.)))

    return meshx, meshy

def longlat2utm(x, y, zone=None, zone_letter=None):
    """
    convert coordinates from Longitude and latitude to UTM

    @param x (np.array) X coordinates
    @param y (np.array) Y coordinates
    @param zone (float) Zone
    @param zone_letter (float) Zone letter

    @returns (x, y) converted coordinates
    """

    meshx, meshy, zone, zone_letter = \
              utm.from_latlon(x, y,
                              force_zone_number=zone,
                              force_zone_letter=zone_letter)
    return meshx, meshy

def utm2longlat(x, y, zone, zone_letter):
    """
    convert coordinates from UTM to Longitude and latitude

    @param x (np.array) X coordinates
    @param y (np.array) Y coordinates
    @param zone (float) Zone
    @param zone_letter (float) Zone letter

    @returns (x, y) converted coordinates
    """
    meshx, meshy = \
              utm.to_latlon(x, y,
                            zone, zone_letter)

    return meshx, meshy

def merge(input_files, output_file, bnd_file, kind='time', force=False):
    """
    Merge time or variable records of several files into one.

    @param input_files (list) List of input files to merge
    @param bnd_file (str) Boundary file
    @param output_file (sre) Name of output file
    @param kind (str) - 'time' All files must have the same
                        variables and it will merge the times (in incresing
                        order)
                      - 'var' All files must have the same times ands it will
                        merge the variables
    @param force (bool) Remove the output file if it already exists
    """

    # First check that the mesh are identical
    in_ress = OrderedDict()
    in_res0 = TelemacFile(input_files[0], bnd_file=bnd_file)

    for ffile in input_files[1:]:
        in_ress[ffile] = TelemacFile(ffile)

    npoin3 = in_res0.npoin3
    nelem3 = in_res0.nelem3
    ndp3 = in_res0.ndp3
    ikle3 = in_res0.ikle3

    for ffile, in_res in in_ress.items():
        if in_res.npoin3 != npoin3:
            raise TelemacException(
                "Not the same number of points in {}:{} should be {}"\
                .format(ffile, in_res.npoin3, npoin3))
        if in_res.nelem3 != nelem3:
            raise TelemacException(
                "Not the same number of elements in {}:{} should be {}"\
                .format(ffile, in_res.nelem3, nelem3))
        if in_res.ndp3 != ndp3:
            raise TelemacException(
                "Not the same number of points in {}:{} should be {}"\
                .format(ffile, in_res.ndp3, ndp3))
        if np.any(in_res.ikle3 != ikle3):
            raise TelemacException(
                "Not the same connectivity in {}".format(ffile))

    if kind == 'time':
        # Check that we have the same variables
        varnames = in_res0.varnames
        for ffile, in_res in in_ress.items():
            if in_res.varnames != varnames:
                raise TelemacException(
                    "Not the same variables in {}:{} should be {}"\
                    .format(ffile, in_res.varnames, varnames))
    elif kind == 'var':
        # Check that we have the same times
        times = in_res0.times
        for ffile, in_res in in_ress.items():
            if np.any(in_res.times != times):
                raise TelemacException(
                    "Not the same times in {}:{} should be {}"\
                    .format(ffile, in_res.times, times))
    else:
        raise TelemacException(\
            "Unknown option for kind: {} should be within {}"
            .format(kind, MERGE_KINDS))

    if kind == 'var':
        out_vars = OrderedDict()
        # Initialize with variables from in_res0
        for var, unit in zip(in_res0.varnames, in_res0.varunits):
            out_vars[var] = (unit, in_res0)

        # Identify new variables with their corresponding file
        for in_res in in_ress.values():
            for var, unit in zip(in_res.varnames, in_res.varunits):
                if var not in out_vars:
                    out_vars[var] = (unit, in_res)

        # Times dont matter here
        out_times = []
        for record, time in enumerate(in_res0.times):
            out_times.append((time, record, None))

    elif kind == 'time':
        time_list = []

        # Initialize with times from in_res0
        out_times = []
        for record, time in enumerate(in_res0.times):
            out_times.append((time, record, in_res0))
            time_list.append(time)

        # Append time records from other files
        for in_res in in_ress.values():
            for record, time in enumerate(in_res.times):
                if time not in time_list:
                    out_times.append((time, record, in_res))
                    time_list.append(time)
                else:
                    print(
                        f"Skipping duplicate time {time} from {in_res.file_name}"
                    )

        # Sort time records
        out_times.sort(key=lambda t: t[0])

        # Var dont matter here
        out_vars = OrderedDict()
        for var, unit in zip(in_res0.varnames, in_res0.varunits):
            out_vars[var] = (unit, None)
    else:
        raise TelemacException(
            f"Unknown option for kind: {kind} should be within {MERGE_KINDS}")

    out_res = TelemacFile(output_file, access='w', overwrite=force)
    nvar = len(out_vars)
    varnames = out_vars.keys()
    varunits = [unit[0] for unit in out_vars.values()]
    out_res.set_header(in_res0.title, nvar, varnames, varunits)
    out_res.set_mesh(in_res0.ndim, in_res0.typ_elem, in_res0.ndp3,
                     in_res0.nptfr, in_res0.nptir, in_res0.nelem3,
                     in_res0.npoin3, in_res0.ikle3, in_res0.ipob3,
                     in_res0.knolg, in_res0.meshx, in_res0.meshy,
                     in_res0.nplan, in_res0.datetime[:3], in_res0.datetime[3:],
                     in_res0.x_orig, in_res0.y_orig, in_res0.meshz)

    ntimestep = len(out_times)
    pbar = ProgressBar(maxval=ntimestep).start()
    for out_record, (time, record, time_res) in enumerate(out_times):
        if kind == 'time':
            in_res = time_res
        for var_idx, (var, (unit, var_res)) in enumerate(out_vars.items()):
            if kind == 'var':
                in_res = var_res
            out_res.add_data(
                var,
                unit,
                time,
                out_record,
                var_idx == 0,
                in_res.get_data_value(var, record),
            )
        pbar.update(out_record)
    pbar.finish()

    if bnd_file is not None:
        out_res.import_group_info(in_res0)

    out_res.close()
    in_res0.close()
    for in_res in in_ress.values():
        in_res.close()
