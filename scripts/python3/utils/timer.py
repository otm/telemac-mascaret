import time
from functools import wraps


def timeit(func):
    """
    Simple decorator used to benchmark any Python function.
    Usage: just put the @timeit decorator above a function definition.
    It will display the function execution time once it has finished.
    """
    @wraps(func)
    def timeit_wrapper(*args, **kwargs):
        start_time = time.perf_counter()
        result = func(*args, **kwargs)
        end_time = time.perf_counter()
        total_time = end_time - start_time
        print(f'Function {func.__name__} took {total_time:.5f} seconds')
        return result
    return timeit_wrapper