#!/usr/bin/env python3
"""@author TELEMAC-MASCARET Consortium
"""
import sys

from runcode import main

if __name__ == "__main__":
    sys.exit(main('telemac3d'))
