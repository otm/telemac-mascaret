# -*- coding: utf-8 -*-
"""
    Python wrapper to the Fortran APIs of TOMAWAC

    Author(s): Fabrice Zaoui, Yoann Audouin, Cedric Goeury, Renaud Barate

    Copyright EDF 2016
"""

from telapy.api.api_module import ApiModule
from utils.exceptions import TelemacException


class Tomawac(ApiModule):
    """The TOMAWAC Python class for APIs"""
    _instanciated = False

    def __new__(cls, *args, **kwargs):
        if cls._instanciated:
            raise TelemacException("a Tomawac instance already exists")
        instance = ApiModule.__new__(cls)
        cls._instanciated = True
        return instance

    def __init__(
        self,
        casfile,
        user_fortran=None,
        lang=2,
        stdout=6,
        comm=None,
        log_lvl="INFO",
        recompile=True,
    ):
        """
        Constructor for Tomawac

        @param casfile (str) Name of the steering file
        @param user_fortran (str) Name of the user Fortran
        @param lang (int) Language for output (1: French, 2:English)
        @param stdout (int) Where to put the listing
        @param comm (MPI.Comm) MPI communicator
        @param log_lvl (str) Logger level
        @param recompile (bool) If true recompile the API
        """
        super(Tomawac, self).__init__(
            "wac",
            casfile,
            user_fortran,
            lang,
            stdout,
            comm,
            recompile,
            log_lvl=log_lvl,
        )

    def __del__(self):
        """
        Destructor
        """
        Tomawac._instanciated = False
