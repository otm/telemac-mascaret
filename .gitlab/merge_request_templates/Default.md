<!-- 
Please give a name and fully describe your Merge Request:
- Use a concise name for your MR, do not use the commit message as the MR name.
- Provide a full description of your changes in the "Description" section below. -->

### Reference issue
<!-- If this MR is related to a specific issue, use the GitLab syntax #<issue number>.
If it fixes the issue, write "Fix #issue". -->

### Description
<!--Please explain your changes in detail.-->

### Checklist
<!--A comprehensive list of tasks involved in getting your MR approved and merged.-->

**All Merge Requests:**
- [ ] Update `NEWS.txt` to describe your changes.
- [ ] Run `compile_telemac.py --check` to check FORTRAN coding conventions.
- [ ] Run `pylint` to check Python coding conventions.
- [ ] If you added new files, run `compile_telemac.py --clean --rescan`  and commit the updated
`cmdf` file(s) accordingly.
- [ ] Run `compile_telemac.py --clean` for both normal and debug configurations.
- [ ] Run `validate_telemac.py` for both normal and debug configurations.
- [ ] Run `validate_telemac.py --notebook` for both normal and debug configurations.
- [ ] Run `doc_telemac.py` if there are any modifications in the documentation.
- [ ] Run `damocles.py --eficas` if there are any modifications in the dictionaries.

**In addition, for a new feature:**
- [ ] Check that your feature works in both serial and parallel modes.
- [ ] Add at least one test case to check the functionality (with documentation, graphics and
VnV script)
- [ ] Update the documentation for the module in which your feature will be available.
