### Summary
<!-- Please provide a concise description of the feature you would like to be implemented,
including the TELEMAC module(s) that are concerned. -->

### Why is this feature useful?
<!-- Explain why such a feature would be helpful not only for your particular case, but also for
other TELEMAC users.-->

### How to implement it?
<!-- If you have a good knowledge of the TELEMAC system, explain what might be the best way to add
the new functionality.-->

### Additional resources
<!-- Provide any additional resources (use case, documentation, links...) that might be useful to
implement the feature.-->

