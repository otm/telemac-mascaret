<!--
Important!

Before opening a bug report, be sure that there isn't already a corresponding issue, using GitLab
Issues search feature:

https://gitlab.pam-retd.fr/otm/telemac-mascaret/-/issues?scope=all&state=opened

If you have found a matching issue but have additional information that may help narrow down the
cause of the bug, please update the existing issue with a new comment.-->

### Summary
<!-- Please provide a concise description of the bug encountered.-->

### Environment
<!-- Please provide your development environment here-->
- **Operating System**: Windows/Linux/MacOS
- **TELEMAC version**: V8P5R0

### Steps to reproduce
<!-- Describe precisely how to reproduce the bug.
Please provide your .cas file if you encountered this bug while running any of the TELEMAC modules,
and any other files that might be necessary, such as your boundary conditions file or your mesh
file if not too heavy.
If the bug is related to Python scripting, please provide a minimal Python script leading to the
bug.-->

<!-- Command used (if the bug is related to the execution of a TELEMAC module) -->
```
telemac2d.py my_case.cas --ncsize 4
```

<!-- OR-->

<!-- Python script (if the bug is related to Python scripting)-->
```Python
...
```

### What is the current *bug* behaviour?
<!-- Describe what actually happens.-->

### What is the expected *correct* behaviour?
<!-- Describe what would be expected instead.-->

### Relevant logs and/or screenshots
<!-- Add relevant TELEMAC or Python outputs.
Either copy the error output enclosed within " ```` ", or provide a screenshot.
-->

```
...
```

### Possible fixes
<!-- If you can, please provide additional information information to help narrow down the cause,
such as potential ways to fix the bug, or the last known working version if this is a
regression.-->

