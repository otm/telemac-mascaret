# ARRAKIS

## Description:

Finite Volume methods for the Saint-Venant-Exner equations in 1D channel with variable rectangular cross sections.

$$ \partial_t h + \partial_x hu = - hu \dfrac{B'}{B}, $$
$$ \partial_t hu + \partial_x \left( hu^2 + \frac{gh^2}{2} \right) = - hu^2 \dfrac{B'}{B} -gh\partial_x z_b -gh J, $$
$$ \partial_t z_b + \xi \partial_x q_s = - \xi q_s \dfrac{B'}{B}, $$

where

- $h$ : the water depth,
- $u$ : the water velocity,
- $z_b$ : the bottom elevation,
- $B$ : the channel width,
- $J=q|q|/(K_s^2 h^2 R_h^{4/3})$ : the friction slope,
- $K_s$ : the Strikler coefficient, 
- $R_h=hB/(B+2h)$ : the hydraulic radius,
- $q_s$ : the solid flux.

## Quick installation:

* install gfortran and python
* install python prerequisites:

```
pip install -r requirements.txt
```

* add the following lines in your ~/.bashrc (replace <path> by the path to your telemac-mascaret installation repertory):

```
export ARRAKIS_ROOT='<path>/telemac-mascaret/optionals/addons/arrakis'
export PYTHONPATH=$PYTHONPATH:<path>/telemac-mascaret/optionals/addons/arrakis/scripts
export PATH=$PATH:<path>/telemac-mascaret/optionals/addons/arrakis/scripts/arrakis
```

* compile the code:
```
mkdir build
cd build
cmake ..
make
```

## How to use:

* Run using a .yml input file

```
arrakis.py <my_case>.yml
```

* Use the quick plot option of arrakis 

```
plot_arrakis.py <my_case>.yml
```

* add -b to plot mobile sediment bed if bedload is activated

```
plot_arrakis.py <my_case>.yml -b
```

## Compilation options:

* compile the code in debug mode : 
```
make clean
cmake -DCMAKE_BUILD_TYPE=Debug ..
make
```

* compile the code in optim mode : 
```
make clean
cmake -DCMAKE_BUILD_TYPE=Optim ..
make
```

* compile the code in release mode : 
```
make clean
cmake -DCMAKE_BUILD_TYPE= ..
make
```

## Run the valdiation:

* Run full validation

```
validate_arrakis.py
```

## Integration checklist for developpers:

* Compile in normal and debug mode
* Clean the code until no warnings
* Run full validation in normal and debug mode
* If validation succeed, make a merge request

