#!/usr/bin/env python
import os
import argparse
import numpy as np
import matplotlib.pylab as plt
from arrakis import *

class TracerCanal(Study):

    def pre(self):
        self.set_dir(file=__file__)
        self.name = "tracer_canal"
        self.cases = []

        # Ordre 1
        case_inputs = read_yml_input_file(os.path.join(self.local_dir, "tracer_canal.yml"))
        case_inputs['OUTPUT PARAMETERS']['RESULT FILE'] = 'Ord.1'
        case_inputs['NUMERICAL PARAMETERS']['TIME SCHEME'] = 2
        case_inputs['NUMERICAL PARAMETERS']['SPACE ORDER'] = 1
        case_inputs['NUMERICAL PARAMETERS']['SPACE ORDER FOR TRACERS'] = 1
        self.cases.append(case_inputs)
        
        # Ordre 2
        case_inputs = read_yml_input_file(os.path.join(self.local_dir, "tracer_canal.yml"))
        case_inputs['OUTPUT PARAMETERS']['RESULT FILE'] = 'Ord.2'
        case_inputs['NUMERICAL PARAMETERS']['TIME SCHEME'] = 2
        case_inputs['NUMERICAL PARAMETERS']['SPACE ORDER'] = 2
        case_inputs['NUMERICAL PARAMETERS']['SPACE ORDER FOR TRACERS'] = 2
        self.cases.append(case_inputs)

        # Ordre 3
        case_inputs = read_yml_input_file(os.path.join(self.local_dir, "tracer_canal.yml"))
        case_inputs['OUTPUT PARAMETERS']['RESULT FILE'] = 'Ord.3'
        case_inputs['NUMERICAL PARAMETERS']['TIME SCHEME'] = 2
        case_inputs['NUMERICAL PARAMETERS']['SPACE ORDER'] = 2
        case_inputs['NUMERICAL PARAMETERS']['SPACE ORDER FOR TRACERS'] = 3
        self.cases.append(case_inputs)

        # Ordre 5
        case_inputs = read_yml_input_file(os.path.join(self.local_dir, "tracer_canal.yml"))
        case_inputs['OUTPUT PARAMETERS']['RESULT FILE'] = 'Ord.5'
        case_inputs['NUMERICAL PARAMETERS']['TIME SCHEME'] = 2
        case_inputs['NUMERICAL PARAMETERS']['SPACE ORDER'] = 2
        case_inputs['NUMERICAL PARAMETERS']['SPACE ORDER FOR TRACERS'] = 5
        self.cases.append(case_inputs)

    def post(self, show=True):
        """
        Post
        """
        set_rcparams()
        c0, c1, c2 = get_default_color_palette()
        markers = get_default_markers()
        colors = c0 + c1 + c2
        fig, ax = plt.subplots(1, 1, figsize=(8, 3))

        # Loop on cases:
        for i, case in enumerate(self.cases):
            RES = case['OUTPUT PARAMETERS']['RESULT FILE']
            
            file_10 = os.path.join(self.local_dir, 'RESU/'+RES+'ini.dat')
            file_1 = os.path.join(self.local_dir, 'RESU/'+RES+'fin.dat')
            restr10 = np.loadtxt(file_10, delimiter=',', skiprows=2)
            restr1 = np.loadtxt(file_1, delimiter=',', skiprows=2)

            # arrays
            x = restr1[:,0]
            T10 = restr10[:,4]
            T1 = restr1[:,4]
            
            # T ini
            if i==0:
                ax.plot(x, T10, label='T(t=0)', color='k', ls='--')

            # plot
            ax.plot(x, T1, label=RES, color=c0[i], marker='o', markersize=2)

        ax.set_ylabel("$T_1$ (-)")
        ax.set_xlabel("$x$ (m)")
        ax.legend(loc=1)
        ax.grid()
        plt.savefig("FIG/{}.png".format(self.name), dpi=300)
        if show:
            plt.show()

if __name__ == '__main__':

    # Parse arguments
    parser = argparse.ArgumentParser()
    parser.add_argument('-r', '--run', default=False, action="store_true", help='run arrakis')
    parser.add_argument('-p', '--post', default=False, action="store_true", help='post processing')
    parser.add_argument('-c', '--check', default=False, action="store_true", help='check results')
    parser.add_argument('--reset-ref', default=False, action="store_true", help='WARNING: only use in extreme necessity')
    args = parser.parse_args()

    # execute study
    study = TracerCanal()
    study.execute(args)
