import numpy as np
import matplotlib.pylab as plt
from arrakis import *

def friction_coef_strikler(h, q, J):
    return (q/h)*np.sqrt(1./(J*(h**(4./3.))))
    
def friction_coef_chezy(h, q, J):
    return (q/h)*np.sqrt(1./(J*h))

def flowrate_strikler(h, J, Ks):
    return np.sqrt(J*(Ks**2)*(h**(10./3.)))

def zb(x):
    return 5. - (5./10000.)*x

if __name__ == '__main__':

    # build mesh:
    print("~~> create mesh")
    build_geo_file(
        nx=100,
        xa=0.,
        xb=10000.,
        geo_file="MESH.geo",
        zb_funct=zb,
        verbose=False)

    # build initial condition:
    print("~~> create initial condition")

    q0 = flowrate_strikler(
        h=5.,
        J=5./10000.,
        Ks=30.)
    print("q0 = ", q0)

    build_ini_file(
        geo_file="MESH.geo",
        ini_file="INIC.ini",
        h=5.,
        q=q0,
        verbose=False)
