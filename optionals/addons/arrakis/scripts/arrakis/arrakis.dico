

#====================================================#
#  CASE INPUT DATA <ARRAKIS> $2.0                    #
#====================================================#


#====================================================#
TIME PARAMETERS:
#====================================================#
  MAXIMAL NUMBER OF ITERATIONS [int]                 : 10000000
  DURATION [float]                                   : 100
  TIME STEP [float]                                  : 1.
  if variable time step:
  - specified value is ignored (use CFL NUMBER instead)
  - only used for the frequency of output writing
  
  VARIABLE TIME STEP [bool]                          : yes
  no : constant time step
  yes: variable time step
  
  CFL NUMBER [float]                                 : 0.9


#====================================================#
OUTPUT PARAMETERS:
#====================================================#
  RESULT FILE [str]                                  : RES
  LISTING FREQUENCY [int]                            : 1
  OUTPUT FREQUENCY [int]                             : 1
  VARIABLES [str] (maximum 8 variables)              : ZB,H,U,,,,,,
  ZB : bottom elevation [m]
  H  : water depth [m]
  U  : water velocity [m/s]
  Q  : water discharge [m^2/s]
  FR : Froude number [-]
  FC : friction coefficient [-]
  B  : sediment depth [m]
  ZF : hard ground elevation [m]
  QS : solid discharge [m^2/s]
  TB : bottom shear stress (tau) [Pa]
  TS : dimensionless bottom shear stress (tau*)
  L  : rectangular channel width [m] 
  A  : wetted area, A=H*L [m^2]
  AS : sediment area, A=b*L [m^2]
  QL : section water discharge, QL = Q*L [m^3/s]
  Ti : tracer number i (1<=i<=99) [-]
  A1 : auxiliary array 1 (for debug) [-]
  A2 : auxiliary array 2 (for debug) [-]

  PROBES [int]                                       : 0
  PROBES COORDINATES [float]                         : 0.
  PROBES VARIABLES [str]                             : H,,,,,,,,
  PROBES FILE NAME                                   : PROBE

  SNAPSHOTS [int]                                    : 0
  SNAPSHOTS TIMES [float]                            : 0.


#====================================================#
MESH PARAMETERS:
#====================================================#
  NUMBER OF NODES [int]                              : 100
  CHANNEL XA [float]                                 : 0.
  CHANNEL XB [float]                                 : 10.

  MESH FILE                                          : no
  no : do not use mesh file
  yes: use mesh file 
  (CHANNEL XA, XB and NUMBER OF NODES ignored)
  
  MESH FILE NAME                                     : MESH
  Remark: file must have the extension .geo

  VARIABLE CHANNEL WIDTH                             : no
  no : constant channel width, solves the SV1D eq.
  yes: varianle channel width, solves the SV1D eq. with
  variable rectangular cross section (new source terms)


#====================================================#
PHYSICAL PARAMETERS:
#====================================================#
  BOTTOM FRICTION LAW [int]                          : 0
  0: None 
  1: Fanning 
  2: Chezy 
  3: Strickler

  BOTTOM FRICTION COEFFICIENT [float]                : 0.

  DIFFUSION OF VELOCITY                              : no
  no: No diffusion of velocity
  yes: diffusion of velocity

  VELOCITY DIFFUSIVITY                               : 1.E-6


#====================================================#
NUMERICAL PARAMETERS:
#====================================================#
  ADVECTION SCHEME [int]                             : 2
  1: Roe
  2: HLL (Harten Lax Van-Leer)
  3: ACU (Audusse Chalon Ung)
  4: Kinetic

  TIME SCHEME [int]                                  : 1
  1: Euler explicit (order 1)
  2: Heun (order 2)
  3: RK2 (order 2, experimental, only SV)
  4: RK3 (order 3, experimental, only SV)

  SPACE ORDER [int]                                  : 1
  1: first order
  2: second order
  3: third order (experimental)
  5: Fifth order (WENO)

  SPACE ORDER FOR TRACERS [int]                      : 1
  1: first order
  2: second order
  3: third order (experimental)
  5: Fifth order (WENO)


#====================================================#
INITIAL CONDITION:
#====================================================#
  INITIAL FILE                                       : no
  no : dont use ini file
  yes: use ini file (INITIAL CONDITION ignored)
  Remark: zb defined from mesh file is ignored
  Remark: ini file must contain: x, zb, h, u
  Remark: ini file mesh nodes can be different,
  in which case linear interpolation is performed
  to get the solution on the current simulation mesh

  INITIAL FILE NAME                                  : INI
  Remark: file must have the extension .ini

  INITIAL CONDITION [int]                            : 1
  0 : ALL NULL
  1 : CONSTANT ELEVATION and CONSTANT FLOWRATE
  2 : CONSTANT WATER DEPTH and CONSTANT FLOWRATE

  CONSTANT WATER DEPTH                               : 1.
  CONSTANT ELEVATION                                 : 1.
  CONSTANT FLOWRATE                                  : 0.


#====================================================#
BOUNDARY CONDITIONS:
#====================================================#
  VELOCITY OR FLOWRATE FORMULATION [int]             : 1
  0: velocity (imposed flowrate ignored)
  1: flowrate (imposed velocity ignored)

  LEFT BOUNDARY CONDITION [int]                      : 3
  1: Wall
  2: h imposed (Fluvial)
  3: q imposed (Fluvial)
  4: h and q imposed (Torrential)
  5: none imposed (Torrential)
  6: Neumann
  7: Periodic

  LEFT DEPTH IMPOSED [float]                         : 1.
  LEFT VELOCITY IMPOSED [float]                      : -0.1
  LEFT FLOWRATE IMPOSED [float]                      : -0.1
  LEFT BOUNDARY FILE [bool]                          : no
  no : liquid boundary file not used
  yes: liquid boundary file used, imposed values ignored
  LEFT BOUNDARY FILE NAME [str]                      : BCL
  Remark: file must have the extension .liq

  RIGHT BOUNDARY CONDITION [int]                     : 2
  1: Wall
  2: h imposed (Fluvial)
  3: q imposed (Fluvial)
  4: h and q imposed (Torrential)
  5: none imposed (Torrential)
  6: Homogeneous Neumann (null gradients)
  7: Periodic

  RIGHT DEPTH IMPOSED [float]                        : 1.
  RIGHT VELOCITY IMPOSED [float]                     : 0.1
  RIGHT FLOWRATE IMPOSED [float]                     : 0.1
  RIGHT BOUNDARY FILE [bool]                         : no
  no : liquid boundary file not used
  yes: liquid boundary file used, imposed values ignored
  RIGHT BOUNDARY FILE NAME [str]                     : BCR
  Remark: file must have the extension .liq


#====================================================#
TRACER PARAMETERS:
#====================================================#
  NUMBER OF TRACERS [int]                            : 0

  INITIAL TRACER VALUES                              : 0.

  LEFT TRACER BOUNDARY CONDITION [int]               : 1
  1: Dirichlet (tracer value imposed)
  2: Homogeneous Neumann (null gradients)
  3: Periodic

  LEFT TRACER VALUE IMPOSED [float]                  : 0.

  RIGHT TRACER BOUNDARY CONDITION [int]              : 1
  1: Dirichlet (tracer value imposed)
  2: Homogeneous Neumann (null gradients)
  3: Periodic

  RIGHT TRACER VALUE IMPOSED [float]                 : 0.

  DIFFUSION OF TRACERS                               : no
  no: No diffusion of tracers
  yes: diffusion of tracers

  TRACER DIFFUSIVITY [float]                         : 1.E-6


#====================================================#
BEDLOAD PARAMETERS:
#====================================================#
  ACTIVATE BEDLOAD [bool]                            : no
  no : no bedload (SV equation)
  yes: activate bedload (SVE equations)

  LEFT BEDLOAD BOUNDARY CONDITION [int]              : 2
  1: Dirichlet (qs imposed)
  2: Homogeneous Neumann (null gradients)
  3: Periodic
  4: Equilibrium slope (qs = f(Jeq))

  LEFT SOLID FLUX [float]                            : 0.
  Imposed solid flux qs (for Dirichlet condition: 1)

  LEFT EQUILIBRIUM SLOPE [float]                     : 0.005
  Imposed equilibrium slope Jep (for bnd condition :4)

  RIGHT BEDLOAD BOUNDARY CONDITION [int]             : 2
  1: Dirichlet (qs imposed)
  2: Homogeneous Neumann (null gradients)
  3: Periodic
  4: Equilibrium slope (qs = f(Jeq))

  RIGHT SOLID FLUX [float]                           : 0.
  Imposed solid flux qs (for Dirichlet condition: 1)

  RIGHT EQUILIBRIUM SLOPE [float]                    : 0.005
  Imposed equilibrium slope Jep (for bnd condition :4)
  
  BEDLOAD FORMULA [int]                              : 1
  1: Grass (1981)
  2: Grass Modified
  3: Meyer-Peter and Muller (1948)
  4: Standardized MPM, Zanke and Roland (2020)
  5: Lefort (2014)
  6: Recking (2013)
  7: Recking (2015)
  8: Engelund and Hansen (1967)
  9: Van Rijn (1984)

  BEDLOAD CALIBRATION COEFFICIENT [float]            : 1.
  
  SEDIMENT POROSITY [float]                          : 0.5
  SEDIMENT DENSITY [float]                           : 2000.
  SEDIMENT D50 [float]                               : 1.E-2
  Median diameter in meters (default: 0.)

  SEDIMENT D16 [float]                               : 0.
  Diameter for which 16% of grains are smaller
  Only for Lefort (default: 0.5d50)

  SEDIMENT DM  [float]                               : 0.
  Mean diameter in meters (default: 1.1d50)
  Only for Lefort and Recking

  SEDIMENT D84 [float]                               : 0.
  Diameter for which 84% of grains are smaller 
  Only for Lefort and Recking (default: 2.1d50)

  GRASS AG [float]                                   : 0.001
  GRASS MG [float] 1<=mg<=4                          : 3.
  
  MPM SKIN STRICKLER [float]                         : 85.
  MPM CRITICAL SHIELDS [float]                       : 0.047
  
  STD MPM COEF A [float]                             : 10.24
  STD MPM COEF B [float]                             : 0.7


#====================================================#
ADVANCED NUMERICAL PARAMETERS:
#====================================================#

  UNLOCK ADVANCED PARAMETERS [bool]                  : no

  RECONSTRUCTION METHOD [int]                        : 2
  1: gradient
  2: variables

  H FLUX LIMITER [int]                               : 1
  1: Min Mod
  2: Van Albada
  3: Van Leer
  4: MC
  5: Super Bee
  6: Generalized Min Mod

  U FLUX LIMITER [int]                               : 1
  T FLUX LIMITER [int]                               : 1

  HYDROSTATIC RECONSTRUCTION [int]                   : 1
  0: No reconstructions
  1: Audusse and al. (default for SV)
  2: Chen and Noelle
  3: Centered 
  4: Centered Roe (default for ROE-SVE)
  5: Upwind Roe (Vazquez-Cendon)

  BOTTOM FRICTION SCHEME [int]                       : 3
  1: Explicit (unstable)
  2: Semi-implicit
  3: implicit

  VARIABLE WIDTH SCHEME [int]                        : 2
  1: 1st order FD (upwind, experimental) 
  2: 2nd order FD (Centered, default for HLL, ACU)
  3: 3rd order FD (experimental)
  4: 4th order FD (only for constant dx, experimental)
  5: Upwind Roe (Vazquez-Cendon, default for ROE)
  
  SVE WAVES APPROXIMATION                            : 1
  1: Cardano's method (default with ROE-SVE)
  2: Soares-Frazao and Zech approximation (default with HLL-SVE)
  3: Nickalls' approximation (default with ACU-SVE)

  BEDLOAD FLUX DERIVATIVES [int]                     : 0
  0: exact formula
  1: finite differences (forward)
  2: finite differences (backward)
  3: finite differences (centered)
  4: finite differences (optim)

  NON ERODIBLE BED FLUX LIMITER [int]                : 1
  0: no qs limitor
  1: add qs limitor to prevent zb<zf

  NON ERODIBLE BED DAMPING REGION [int]              : 1
  0: no damping of qs
  1: qs = psi*qs if 0<zb-zf<eps_b with 0<psi<1

  NON ERODIBLE BED DAMPING REGION DEPTH [float]      : 0.1
  Depth of the damping region

