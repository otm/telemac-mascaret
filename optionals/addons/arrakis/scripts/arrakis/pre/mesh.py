import numpy as np

class Mesh1D():
    """
    !~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    Mesh 1D
    !~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    @param nx (int): number of cells (default: 100)
    @param xa (float): absc. of left boundary interface (default: 0.)
    @param xb (float): absc. of right boundary interface (default: 10.)
    @param density[0:nx+1] (float): array of density (default: None)
                                    used to define cell size
    @attribute nx (float): number of cells
    @attribute x [0:nx+2] (float): abscissa of cells (center)
    @attribute dx[0:nx+1] (float): space between cell centers
    @attribute xa (float): absc. of left boundary interface
    @attribute xb (float): absc. of right boundary interface
    !~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    Convention:

    0   xA    1        i-1        i        i+1        n   xB   n+1
    o----|----o-- ... --o----|----o----|----o-- ... --o----|----o
     x0  I0   x1       xi-1  Ii-1  xi  Ii   xi+1      xn   In   xn+1
     <-------> <------->           <--------> dx[i]   <--------->
        dx[0]    dx[1]        <-------->                 dx[n]
                                 ci[i] : cell size

    Ghost cells: x[0] and x[nx+1]
    o--|--o--    ...      --o--|--o
    0  A  1                nx  B nx+1

    !~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    """
    def __init__(self, nx=100, xa=0., xb=10., density=None, verbose=False):
        """
        Initialize mesh
        """
        const_dx = (xb-xa)/nx
        self.nx = nx
        self.xa = xa
        self.xb = xb
        self.x = np.zeros(nx+2)
        self.dx = np.zeros(nx+1)

        # Constant cell size
        # ~~~~~~~~~~~~~~~~~~
        for i in range(0, self.nx+2):
            self.x[i] = self.xa + 0.5*const_dx + (i-1)*const_dx
            if i>0:
                self.dx[i-1] = self.x[i] - self.x[i-1]

        # Variable cell size (keep same nx)
        # ~~~~~~~~~~~~~~~~~~
        if density is not None:
            # deform with density function
            eps_density = 1.e-3 # to avoid huge dx
            for i in range(0, self.nx+1):
                self.dx[i] /= max(eps_density, density(self.x[i]))

            # compute new xb and scaling factor
            self.x[0] = self.xa - 0.5*self.dx[0]
            for i in range(0, nx+1):
                self.x[i+1] = self.x[i] + self.dx[i]
            newxb = self.x[-1] - 0.5*self.dx[-1]
            scale = (self.xb-self.xa)/(newxb-self.xa)

            # scale
            for i in range(0, self.nx+2):
                self.x[i] = self.xa + (self.x[i]-self.xa)*scale

            # final checks
            assert self.x[1]  > self.xa
            assert self.x[nx] < self.xb

        # verbose
        if verbose:
            print("x = :", self.x[1:nx+1])

