import numpy as np

def build_ini_file(geo_file="MESH.geo",
                   ini_file="INIC.ini",
                   h=1., h_funct=None,
                   u=0., u_funct=None,
                   q=0., q_funct=None,
                   t=0., t_funct=None, ntrac=0,
                   friction=0., friction_funct=None,
                   time=0., eps=1.e-6, verbose=False):
    """
    !~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    Create initial condition file
    !~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    @param geo_file (str): geometry file (default: "MESH.geo")
    @param ini_file (str): initial condition file (default: "INI.ini")
    @param h (float): constant water depth (default: 0.)
    @param h_funct (function): water depth function (default: None)
    @param u (float): constant velocity (default: 0.)
    @param u_funct (function): velocity function (default: None)
    @param q (float): constant flow rate (default: 0.)
    @param q_funct (function): flow rate function (default: None)
    @param t (float): constant tracers (default: 0.)
    @param t_funct (function): tracers function (default: None)
    @param ntrac (int): number of tracers (default: 0)
    @param time (function): initial time (default: 0.)
    @param eps (float): epsilon for u=q/h (default: 1.e-6)
    !~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~    
    """
    # check wether velocity or flowrate is given
    if u!=0. and q!=0.:
        raise ValueError("Given either u or q")
    if u_funct is not None and q_funct is not None:
        raise ValueError("Given either u_funct or q_funct")
    friction_flag = bool(friction!=0. or friction_funct)
        
    # read variable in file
    f = open(geo_file, "r")
    f.readline()
    var = f.readline().strip().split(",")
    f.close()
    if 'X' not in var:
        raise ValueError("X not in mesh file")

    # read geometry file abscissa
    data = np.loadtxt(geo_file, skiprows=2)
    x  = data[:, 0]
    nx = len(x)

    # create initial conditions
    # ~~~~~~~~~~~~~~~~~~~~~~~~~
    if friction_flag:
        nfields = 3 + ntrac
    else:
        nfields = 2 + ntrac
    array = np.empty((nfields, nx), dtype='d')
    field_labels = ['' for i in range(nfields)]

    for i in range(nx):
        l = 0

        # init water depth
        field_labels[l] = "H"
        if h_funct is not None:
            array[l, i] = h_funct(x[i])
        else:
            array[l, i] = h
        l += 1

        # flow rate or velocity
        if u!=0. or u_funct is not None:
            # init velocity
            field_labels[l] = "U"
            if u_funct is not None:
                array[l, i] = u_funct(x[i])
            else:
                array[l, i] = u
        else:
            # init flow rate
            field_labels[l] = "Q"
            if q_funct is not None:
                array[l, i] = q_funct(x[i])
            else:
                array[l, i] = q
        l += 1

        # init water depth
        if friction_flag:
            field_labels[l] = "FC"
            if friction_funct is not None:
                array[l, i] = friction_funct(x[i])
            else:
                array[l, i] = friction
            l += 1

        # tracers
        if ntrac > 0:
            for k in range(ntrac):
                field_labels[l+k] = "T{}".format(k+1)
                if t_funct is not None:
                    array[l+k, i] = t_funct(x[i], k)
                else:
                    array[l+k, i] = t

    if verbose:
        print("fields :", array)
        print("labels :", field_labels)

    # write ini file
    # ~~~~~~~~~~~~~~
    f = open(ini_file, "w")
    f.write("{}, {} \n".format(time, nx))
    # write labels
    f.write("X,")
    for j in range(nfields):
        f.write("{:}".format(field_labels[j]))
        if j<nfields-1:
            f.write(",")
        else:
            f.write("\n")
    # write fields
    for i in range(nx):
        f.write("{:.8f}, ".format(x[i]))
        for j in range(nfields):
            f.write("{:}".format(array[j, i]))
            if j<nfields-1:
                f.write(", ")
            else:
                f.write("\n")
    return 0

