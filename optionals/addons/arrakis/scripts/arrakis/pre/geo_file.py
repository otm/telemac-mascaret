import numpy as np
from .mesh import Mesh1D

def build_geo_file(geo_file="MESH.geo",
                   mesh=None, nx=100, xa=0., xb=10., density=None,
                   erodible_bed=False, variable_width=False,
                   zb= 0., zb_funct=None,
                   Bx= 1., Bx_funct=None,
                   zf=-1., zf_funct=None,
                   verbose=False):
    """ 
    !~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    Build geometry file : contains only internal nodes x[1,nx]

        xA    x1               xi              xn   xB   
    o----|----o--  ...  --|----o----|--  ...  --o----|----o
    !~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    @param geo_file (str): geometry file (default: "mesh.geo")
    @param nx (int): number of cells (default: 100)
    @param xa (int): abscissa of left boundary interface (default: 0.)
    @param xb (int): abscissa of right boundary interface (default: 10.)
    @param density[0:nx+1] (float): array containing density (default: None)
                                    used to define cell size
    @param erodible_bed (bool): write zf in mesh file (default: False)
    @param variable_width (bool): write width Bx in mesh file (default: False)
    @param zb (float): constant bottom (default: 0.)
    @param zb_funct (function): bottom function (default: None)
    @param zf (float): constant hard bottom (default: -1.)
    @param zf_funct (function): hard bottom function (default: None)
    @param Bx (float): constant width (default: 1.)
    @param Bx_funct (function): width function (default: None)
    !~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    """
    # build 1d mesh
    if mesh is None:
        mesh1d = Mesh1D(nx=nx, xa=xa, xb=xb, 
                        density=density, 
                        verbose=verbose)
    else:
        mesh1d = mesh
    x = mesh1d.x[1:-1]

    zb_arr = np.empty(nx, dtype='d')
    zf_arr = np.empty(nx, dtype='d')
    Bx_arr = np.empty(nx, dtype='d')

    # init bottom and width - loop on mesh nodes
    for i in range(nx):

        # create sediment bed
        if zb_funct is not None:
            zb_arr[i] = zb_funct(x[i])
        else:
            zb_arr[i] = zb

        # create non erodible bed
        if erodible_bed:
            if zf_funct is not None:
                zf_arr[i] = zf_funct(x[i])
            else:
                zf_arr[i] = zf

        # create width
        if variable_width:
            if Bx_funct is not None:
                Bx_arr[i] = Bx_funct(x[i])
            else:
                Bx_arr[i] = Bx

    # write .geo file
    if variable_width:
        f = open(geo_file, "w")
        f.write("# Mesh file, Nx={} \n".format(nx))
        f.write("X,ZB,L \n")
        for i in range(nx):
            f.write("{:.8f} {:} {:} \n"\
                .format(x[i], zb_arr[i], Bx_arr[i]))
        f.close()

        if erodible_bed:
            f = open(geo_file, "w")
            f.write("# Mesh file, Nx={} \n".format(nx))
            f.write("X,ZB,ZF,L \n")
            for i in range(nx):
                f.write("{:.8f} {:} {:} {:} \n"\
                    .format(x[i], zb_arr[i], zf_arr[i], Bx_arr[i]))
            f.close()
    else:
        f = open(geo_file, "w")
        f.write("# Mesh file, Nx={} \n".format(nx))
        f.write("X,ZB \n")
        for i in range(nx):
            f.write("{:.8f} {:} \n"\
                .format(x[i], zb_arr[i]))
        f.close()

        if erodible_bed:
            f = open(geo_file, "w")
            f.write("# Mesh file, Nx={} \n".format(nx))
            f.write("X,ZB,ZF \n")
            for i in range(nx):
                f.write("{:.8f} {:} {:} \n"\
                    .format(x[i], zb_arr[i], zf_arr[i]))
            f.close()

    # verbose            
    if verbose:
        print("zb = :", zb_arr)
        if erodible_bed:
            print("zf = :", zf_arr)
        if variable_width:
            print("Bx = :", Bx_arr)

    return 0
