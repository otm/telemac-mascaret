


      ! ****************************************************************
      module module_alloc
      ! ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
      ! Allocation of arrays
      ! ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
      implicit none
      ! ----------------------------------------------------------------



      contains



      ! ****************************************************************
      subroutine allocation_geom
      ! ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
      ! Tables allocation
      ! ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
      use module_data, only: nx, dx, ci, x, zb, zf, Lx, dLx, Rh, &
     &                       bedload, variable_width
      implicit none      
      ! ----------------------------------------------------------------
      ! allocation of geometrical parameters
      ! ----------------------------------------------------------------
      allocate(dx (0:nx))
      allocate(ci (1:nx))       ! ghost cells not included
      allocate(x  (0:nx+1))     ! ghost cells included
      allocate(zb (0:nx+1))
      allocate(Rh (0:nx+1))
      allocate(Lx (0:nx+1))
      if (bedload.eqv..True.) then
        allocate(zf (0:nx+1))
      endif
      if (variable_width.eqv..True.) then
        allocate(dLx(0:nx+1))
      endif
      ! ----------------------------------------------------------------
      return 
      end subroutine
      ! ****************************************************************




      ! ****************************************************************
      subroutine allocation
      ! ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
      ! Tables allocation
      ! ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
      use module_data, only: nx, h, u, q, t, ht, &
     &                       ntrac, ordre, &
     &                       aux1, aux2, fric, &
     &                       fluh_cell, fluh_src, &
     &                       fluq_cell, fluq_src, &
     &                       flut_cell, flut_src, & 
     &                       corr_bp, corr_bm, &
     &                       corr_hp, corr_hm, &
     &                       corr_up, corr_um, &
     &                       corr_tp, corr_tm, &
     &                       bedload, qsed, &
     &                       fluqsed_cell, fluqsed_src, &
     &                       taub, taus
      implicit none
      ! ----------------------------------------------------------------
      ! main variables
      ! ----------------------------------------------------------------
      allocate(h            (0:nx+1))
      allocate(u            (0:nx+1))
      allocate(q            (0:nx+1))
      allocate(fluh_cell    (1:nx))
      allocate(fluq_cell    (1:nx))
      allocate(fluh_src     (1:nx))
      allocate(fluq_src     (1:nx))
      allocate(fric         (0:nx+1))
      ! ----------------------------------------------------------------
      ! auxiliary arrays
      ! ----------------------------------------------------------------
      allocate(aux1         (0:nx+1))
      allocate(aux2         (0:nx+1))
      ! ----------------------------------------------------------------
      ! tracers
      ! ----------------------------------------------------------------
      if (ntrac.gt.0) then
        allocate(t          (0:nx+1,ntrac))
        allocate(ht         (0:nx+1,ntrac))
        allocate(flut_cell  (1:nx,ntrac))
        allocate(flut_src   (1:nx,ntrac))
      endif
      ! ----------------------------------------------------------------
      ! hydro second order
      ! ----------------------------------------------------------------
      if (ordre.ge.2) then
        allocate(corr_bp    (1:nx))
        allocate(corr_bm    (1:nx))
        allocate(corr_hp    (1:nx))
        allocate(corr_hm    (1:nx))
        allocate(corr_up    (1:nx))
        allocate(corr_um    (1:nx))
        if (ntrac.gt.0) then
          allocate(corr_tp  (1:nx,ntrac))
          allocate(corr_tm  (1:nx,ntrac))
        endif
      endif
      ! ----------------------------------------------------------------
      ! bedload
      ! ----------------------------------------------------------------
      if (bedload.eqv..True.) then
        allocate(qsed         (0:nx+1))
        allocate(taub         (0:nx+1))
        allocate(taus         (0:nx+1))
        allocate(fluqsed_cell (1:nx))
        allocate(fluqsed_src  (1:nx))
      endif
      ! ----------------------------------------------------------------
      return 
      end subroutine
      ! ****************************************************************




      ! ****************************************************************
      subroutine deallocation
      ! ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
      ! Tables allocation
      ! ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
      use module_data, only: x, ci, dx, zb, h, u, q, t, ht, &
     &                       ntrac, ordre, &
     &                       aux1, aux2, fric, &
     &                       fluh_cell, fluh_src, &
     &                       fluq_cell, fluq_src, &
     &                       flut_cell, flut_src, & 
     &                       corr_bp, corr_bm, &
     &                       corr_hp, corr_hm, &
     &                       corr_up, corr_um, &
     &                       corr_tp, corr_tm, &
     &                       bedload, zf, qsed, &
     &                       fluqsed_cell, fluqsed_src, &
     &                       taub, taus, &
     &                       variable_width, Lx, dLx, Rh
      implicit none
      ! ----------------------------------------------------------------
      ! Geometry
      ! ----------------------------------------------------------------
      deallocate(x)
      deallocate(ci)
      deallocate(dx)
      deallocate(zb)
      deallocate(Rh)
      deallocate(Lx)
      if (bedload.eqv..True.) then
        deallocate(zf)
      endif
      if (variable_width.eqv..True.) then
        deallocate(dLx)
      endif
      ! ----------------------------------------------------------------
      ! main variables
      ! ----------------------------------------------------------------
      deallocate(h)
      deallocate(u)
      deallocate(q)
      deallocate(fluh_cell)
      deallocate(fluq_cell)
      deallocate(fluh_src)
      deallocate(fluq_src)
      deallocate(fric)
      ! ----------------------------------------------------------------
      ! auxiliary arrays
      ! ----------------------------------------------------------------
      deallocate(aux1)
      deallocate(aux2)
      ! ----------------------------------------------------------------
      ! tracers
      ! ----------------------------------------------------------------
      if (ntrac.gt.0) then
        deallocate(t)
        deallocate(ht)
        deallocate(flut_cell)
        deallocate(flut_src)
      endif
      ! ----------------------------------------------------------------
      ! hydro second order
      ! ----------------------------------------------------------------
      if (ordre.ge.2) then
        deallocate(corr_bp)
        deallocate(corr_bm)
        deallocate(corr_hp)
        deallocate(corr_hm)
        deallocate(corr_up)
        deallocate(corr_um)
        if (ntrac.gt.0) then
          deallocate(corr_tp)
          deallocate(corr_tm)
        endif
      endif
      ! ----------------------------------------------------------------
      ! bedload
      ! ----------------------------------------------------------------
      if (bedload.eqv..True.) then
        deallocate(qsed)
        deallocate(taub)
        deallocate(taus)
        deallocate(fluqsed_cell)
        deallocate(fluqsed_src)
      endif
      ! ----------------------------------------------------------------
      return 
      end subroutine
      ! ****************************************************************




      end module
      ! ****************************************************************
