


      ! ****************************************************************
      module module_init
      ! ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
      ! Discretisation and Initialisation module
      ! ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
      use module_data, only: dp, eps
      implicit none
      ! ----------------------------------------------------------------



      contains




      ! ****************************************************************
      ! ****************************************************************
      ! ****************************************************************
      ! ****************************************************************
      !                       GEOMETRY AND MESH
      ! ****************************************************************
      ! ****************************************************************
      ! ****************************************************************
      ! ****************************************************************




      ! ****************************************************************
      subroutine init_geometry
      ! ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
      ! Discretisation parameters initialisation
      ! ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
      ! 
      ! Convention:
      ! 
      !  0   xA    1        i-1        i        i+1        n   xB   n+1
      !  o----|----o-- ... --o----|----o----|----o-- ... --o----|----o
      !  x0  I0   x1       xi-1  Ii-1  xi  Ii   xi+1      xn   In   xn+1
      !  <-------> <------->           <--------> dx[i]    <--------> 
      !     dx[0]    dx[1]         <-------->                 dx[n]
      !                              ci[i] : cell size
      ! 
      ! Ghost cells:
      ! 
      !  o--|--o--    ...      --o--|--o
      !  0 xA  1                nx  xB nx+1
      ! 
      ! ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
      use module_data, only: nx, dx, ci, xa, xb, x, zb, &
     &                       zf, zf_default, minci, &
     &                       use_mesh_file, mesh_file, bedload, &
     &                       variable_width, Lx, dLx, bnd_extrapolation
      use module_io, only: split_string
      use module_alloc, only: allocation_geom
      implicit none
      integer                               :: i
      integer                               :: io, nlines, header_lines
      real(kind=dp)                         :: dxcst
      character(len=255)                    :: var_line
      character(len=3), dimension(10)       :: vars
      ! ----------------------------------------------------------------
      ! No mesh file (constant dx, Lx)
      ! ----------------------------------------------------------------
      if (use_mesh_file.eqv..False.) then
        call allocation_geom
        dxcst = (xb-xa)/real(nx)
        dx(0) = dxcst
        do i=1,nx
          dx(i) = dxcst
          ci(i) = dxcst
        enddo
        minci = dxcst
        do i=0,nx+1
          ! initialize node abscissa
          x(i) = xa + (dx(0)/2d0) + (i-1)*dx(0)
          ! initialize bottom elevation
          zb(i) = 0.d0
          ! initialize hard bottom elevation if sediment transport
          if (bedload.eqv..True.) then
            zf(i) = min(zf_default, zb(i))
          endif
          ! initialize channel width
          Lx(i) = 1.d0
          if (variable_width.eqv..True.) then
            dLx(i)= 0.d0
          endif
        enddo
      ! ----------------------------------------------------------------
      ! Mesh file
      ! ----------------------------------------------------------------
      else
        write(*,*) ''
        write(*,*) '°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°'
        write(*,*) ' Reading mesh file   '
        write(*,*) '°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°'
        write(*,*) ''
        ! ~~~~~~~~~~~~~~~~~~~
        ! read mesh file size
        ! ~~~~~~~~~~~~~~~~~~~
        header_lines = 2
        nlines = 0
        open(unit=1, file=trim(mesh_file)//'.geo')
        do
          read(1, *, iostat=io)
          if (io/=0) exit
          nlines = nlines + 1
        enddo
        nx = nlines - header_lines
        write(*,*) 'number of cells = ', nx
        call allocation_geom
        ! ~~~~~~~~~~~
        ! read header
        ! ~~~~~~~~~~~
        close(unit=1)
        open(unit=1, file=trim(mesh_file)//'.geo')
        read(1, *)
        read(1, '(A)') var_line
        ! check header variables
        call split_string(var_line, vars)
        if (trim(vars(1)).ne.'X') then
          write(*,*) '++++++++++++++++++++++++++++++++++++++++++++'
          write(*,*) ' Error reading the mesh file, '
          write(*,*) ' must contain X,ZB'
          write(*,*) ' Stop.'
          write(*,*) '++++++++++++++++++++++++++++++++++++++++++++'
          stop
        endif
        if (trim(vars(2)).ne.'ZB') then
          write(*,*) '++++++++++++++++++++++++++++++++++++++++++++'
          write(*,*) ' Error reading the mesh file, '
          write(*,*) ' must contain X,ZB'
          write(*,*) ' Stop.'
          write(*,*) '++++++++++++++++++++++++++++++++++++++++++++'
          stop
        endif
        if ((trim(vars(3)).ne.'ZF').and. &
       &    (bedload.eqv..True.).and. &
       &    (variable_width.eqv..False.)) then
          write(*,*) '++++++++++++++++++++++++++++++++++++++++++++'
          write(*,*) ' Error reading the mesh file, '
          write(*,*) ' must contain X,ZB,ZF'
          write(*,*) ' Stop.'
          write(*,*) '++++++++++++++++++++++++++++++++++++++++++++'
          stop
        endif
        if ((trim(vars(3)).ne.'L').and. &
       &    (bedload.eqv..False.).and. &
       &    (variable_width.eqv..True.)) then
          write(*,*) '++++++++++++++++++++++++++++++++++++++++++++'
          write(*,*) ' Error reading the mesh file, '
          write(*,*) ' must contain X,ZB,L'
          write(*,*) ' Stop.'
          write(*,*) '++++++++++++++++++++++++++++++++++++++++++++'
          stop
        endif
        if ((trim(vars(4)).ne.'L').and. &
       &    (bedload.eqv..True.).and. &
       &    (variable_width.eqv..True.)) then
          write(*,*) '++++++++++++++++++++++++++++++++++++++++++++'
          write(*,*) ' Error reading the mesh file, '
          write(*,*) ' must contain X,ZB,ZF,L'
          write(*,*) ' Stop.'
          write(*,*) '++++++++++++++++++++++++++++++++++++++++++++'
          stop
        endif
        ! ~~~~~~~~~~~~~~~~~~~~~~
        ! read mesh file content
        ! ~~~~~~~~~~~~~~~~~~~~~~
        do i=1,nx
          ! SV1D :
          ! no bedload, constant width
          if ((bedload.eqv..False.).and. &
       &      (variable_width.eqv..False.)) then
            read(1, *) x(i), zb(i)
            Lx(i) = 1.d0

          ! SV1DR :
          ! no bedload, variable width
          else if ((bedload.eqv..False.).and. &
       &           (variable_width.eqv..True.)) then
            read(1, *) x(i), zb(i), Lx(i)

          ! SVE1D :
          ! bedload, constant width
          else if ((bedload.eqv..True.).and. &
       &           (variable_width.eqv..False.)) then
            read(1, *) x(i), zb(i), zf(i)
            Lx(i) = 1.d0

          ! SVE1DR :
          ! bedload, variable width
          else if ((bedload.eqv..True.).and. &
       &           (variable_width.eqv..True.)) then
            read(1, *) x(i), zb(i), zf(i), Lx(i)
          endif
        enddo
        close(unit = 1)
        ! ~~~~~~~~~~~~~~~~~~~~~~~~~~~~
        ! define values on ghost cells
        ! ~~~~~~~~~~~~~~~~~~~~~~~~~~~~
        if (bnd_extrapolation.eq.0) then
          ! set equal to value on neighboring cell
          zb(0)    = zb(1)
          zb(nx+1) = zb(nx)
          Lx(0)    = Lx(1)
          Lx(nx+1) = Lx(nx)
          if (bedload.eqv..True.) then
            zf(0)    = zf(1)
            zf(nx+1) = zf(nx)
          endif
        else
          ! preserve longitudinal gradient
          zb(0)    = zb(1) - (zb(2) - zb(1))*dx(1)
          zb(nx+1) = zb(nx)+ (zb(nx)- zb(nx-1))*dx(nx-1)
          Lx(0)    = Lx(1) - (Lx(2) - Lx(1))*dx(1)
          Lx(nx+1) = Lx(nx)+ (Lx(nx)- Lx(nx-1))*dx(nx-1)
          if (bedload.eqv..True.) then
            zf(0)    = zf(1) - (zf(2) - zf(1))*dx(1)
            zf(nx+1) = zf(nx)+ (zf(nx)- zf(nx-1))*dx(nx-1)
          endif
        endif
        ! ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
        ! define discretization parameters
        ! ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
        do i=1,nx-1
          dx(i) = x(i+1)-x(i)
        enddo
        dx(0)  = dx(1)
        dx(nx) = dx(nx-1)
        x(0)    = x(1) -dx(0)
        x(nx+1) = x(nx)+dx(nx)
        do i=1,nx
          ci(i) = 0.5d0*(dx(i-1)+dx(i))
        enddo
        ! ~~~~~~~~~~~~~~~~~~~~~~~
        ! define xA and xB from x
        ! ~~~~~~~~~~~~~~~~~~~~~~~
        xa = minval(x)
        xb = maxval(x)
        ! ~~~~~~~~~~~~~
        ! Initialize L'
        ! ~~~~~~~~~~~~~
        ! L' is defined at interfaces between cells dL(i) <- L'(i+1/2)
        if (variable_width.eqv..True.) then
          do i=0,nx
            dLx(i) = (Lx(i+1)-Lx(i))/dx(i)
          enddo
        end if
      end if
      ! ----------------------------------------------------------------
      return
      end subroutine
      ! ****************************************************************




      ! ****************************************************************
      ! ****************************************************************
      ! ****************************************************************
      ! ****************************************************************
      !                          INITIAL CONDITIONS
      ! ****************************************************************
      ! ****************************************************************
      ! ****************************************************************
      ! ****************************************************************




      ! ****************************************************************
      subroutine init_variables()
      ! ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
      ! Initialisation 
      ! ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
      use module_data, only: use_ini_file, ini_file, &
    &                        bedload, ordre, pi, nx, eps_num, &
    &                        x, dx, zf, zf_default, zb, &
    &                        variable_width, Lx, Rh, &
    &                        h, u, q, t, ht, fric, frict_coef, &
    &                        qsed, taub, taus, aux1, aux2, &
    &                        ntrac, type_init, &
    &                        corr_bp, corr_bm, &
    &                        corr_hp, corr_hm, &
    &                        corr_up, corr_um, &
    &                        corr_tp, corr_tm, &
    &                        const_h, const_e, const_q,  const_t, &
    &                        bedload, g, rhos, rho, d50, tau_adim, &
    &                        bnd_extrapolation, ua, ub, qa, qb
      use module_io, only: integer_to_string, split_string, count_vars
      implicit none
      integer                                :: i, j, k, l, npoin
      integer                                :: ndata, io
      real(kind=dp)                          :: initime, a0, a1
      real(kind=dp), pointer                 :: x_tmp (:)
      real(kind=dp), pointer                 :: f_tmp (:, :)
      real(kind=dp), pointer                 :: f_int (:, :)
      character(len=255)                     :: line
      character(len=255)                     :: var_line
      character(len=3), dimension(100)       :: vars
      character(len=4)                       :: char_trac
      ! ----------------------------------------------------------------
      ! Constant initialization for main arrays
      ! ----------------------------------------------------------------
      ! CONSTANT ELEVATION and CONSTANT FLOWRATE
      ! ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
      if (type_init==1) then
        do i=0,nx+1
          ! main arrays
          h(i)  = max(0.d0, const_e - zb(i))
          q(i)  = const_q
          u(i)  = q(i)/max(h(i), eps)
          ! tracers
          if (ntrac.gt.0) then
            do k=1,ntrac
              t(i,k) = const_t(k)
              ht(i,k) = t(i,k)*h(i)
            enddo
          endif
        enddo
      ! CONSTANT WATER DEPTH and CONSTANT FLOWRATE
      ! ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
      else if (type_init==2) then
        do i=0,nx+1
          ! main arrays
          h(i)  = max(0.d0, const_h)
          q(i)  = const_q
          u(i)  = q(i)/max(h(i), eps)
          ! tracers
          if (ntrac.gt.0) then
            do k=1,ntrac
              t(i,k) = const_t(k)
              ht(i,k) = t(i,k)*h(i)
            enddo
          endif
        enddo  
      ! ALL NULL
      ! ~~~~~~~~
      else 
        do i=0,nx+1
          ! main arrays
          h(i)  = 0.d0
          q(i)  = 0.d0
          u(i)  = 0.d0
          ! tracers
          if (ntrac.gt.0) then
            do k=1,ntrac
              t(i,k) = 0.d0
              ht(i,k) = 0.d0
            enddo
          endif
        enddo
      endif 
      ! ----------------------------------------------------------------
      ! initialise other arrays and parameters
      ! ----------------------------------------------------------------
      ! initialize friction coef
      do i=0,nx+1
        fric(i) = frict_coef
      enddo
      ! initialize bedload
      if (bedload.eqv..True.) then
        do i=0,nx+1
          qsed(i) = 0d0
          taub(i) = 0d0
          taus(i) = 0d0
        enddo
        ! init adim factor : tau_adim = taus/taub
        tau_adim = 1d0/((rhos-rho)*g*d50)
      endif
      ! hydraulic radius
      do i=0,nx+1
        ! if variable_width init hydraulic radius
        if (variable_width.eqv..True.) then
          Rh(i) = (h(i)*Lx(i))/(Lx(i) + 2.d0*h(i))
        else
          Rh(i) = h(i)
        endif
      enddo
      ! flux sign convention for boundary conditions
      ua = -ua
      ub = -ub
      qa = -qa
      qb = -qb
      ! initialise auxiliary arrays
      do i=0,nx+1
        aux1(i) = 0d0
        aux2(i) = 0d0
      enddo
      ! initialise second order reconstructions
      if (ordre.ge.2) then
        do i=1,nx
          corr_bp(i) = 0.d0
          corr_bm(i) = 0.d0
          corr_hp(i) = 0.d0
          corr_hm(i) = 0.d0
          corr_up(i) = 0.d0
          corr_um(i) = 0.d0
          if (ntrac.gt.0) then
            do k=1,ntrac
              corr_tp(i, k) = 0.0d0
              corr_tm(i, k) = 0.0d0
            enddo
          endif
        enddo
      endif
      ! ----------------------------------------------------------------
      ! Continuation files (overwrite constant init)
      ! ----------------------------------------------------------------
      if(use_ini_file.eqv..True.) then
        ! initialize variables of ini files to blank
        do i=1,size(vars)
          vars(i) = ''
        enddo
        ! Read continuation file
        ! ~~~~~~~~~~~~~~~~~~~~~~
        write(*,*) ''
        write(*,*) '°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°'
        write(*,*) ' Reading ini file  '
        write(*,*) '°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°'
        write(*,*) ''
        open(unit=2, file=trim(ini_file)//'.ini')
        read(2, *) initime, npoin
        read(2, '(A)') var_line
        call split_string(var_line, vars) ! read var names 
        call count_vars(vars, ndata) ! detect the number of var
        call check_ini_inputs(vars) ! checks var in file
        ! allocate tmp tables
        allocate(x_tmp(1:npoin))
        allocate(f_tmp(1:npoin, 1:ndata))
        allocate(f_int(0:nx+1,  1:ndata))
        ! read file content
        do i=1,npoin
          read(2, '(A)', iostat=io) line
          read(line, *, iostat=io) f_tmp(i,1:ndata)
          x_tmp(i) = f_tmp(i,1)
        enddo
        close(unit = 2)
        ! interpolate data on mesh (f_tmp->f_int)
        ! ~~~~~~~~~~~~~~~~~~~~~~~~
        do l=1,ndata
          ! define values on internal cells
          do j=1,nx ! loop on internal cells
            if (x(j).le.x_tmp(1)) then
              f_int(j,l) = f_tmp(1,l)
            elseif (x(j).ge.x_tmp(npoin)) then
              f_int(j,l) = f_tmp(npoin,l)
            else
              do i=1,npoin-1 ! loop on temporary ini array
                if ((x(j).ge.x_tmp(i)).and.(x(j).le.x_tmp(i+1))) then
                  a1 = (x(j)-x_tmp(i))/(x_tmp(i+1)-x_tmp(i))
                  a0 = 1.d0 - a1
                  f_int(j,l) = a0*f_tmp(i,l) + a1*f_tmp(i+1,l)
                endif
              enddo
            endif
          enddo
          ! define values on ghost cells
          if (bnd_extrapolation.eq.0) then
            f_int(0,l)    = f_int(1,l)
            f_int(nx+1,l) = f_int(nx,l)
          else
            f_int(0,l)    = f_int(1,l) - &
     &                     (f_int(2,l)-f_int(1,l))*dx(1)
            f_int(nx+1,l) = f_int(nx,l)+ &
     &                     (f_int(nx,l)-f_int(nx-1,l))*dx(nx-1)
          endif
        enddo
        ! assignment of variables depending on labels
        ! ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
        do l=1,ndata
          ! ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
          if (trim(vars(l)).eq.'ZB') then
          ! ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
            zb(0:nx+1) = f_int(0:nx+1,l)
          ! ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
          elseif (trim(vars(l)).eq.'H') then
          ! ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
            h(0:nx+1) = f_int(0:nx+1,l)
          ! ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
          elseif (trim(vars(l)).eq.'U') then
          ! ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
            u(0:nx+1) = f_int(0:nx+1,l)
            do j=0,nx+1
              q(j) = h(j)*u(j)
            enddo
          ! ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
          elseif (trim(vars(l)).eq.'Q') then
          ! ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
            q(0:nx+1) = f_int(0:nx+1,l)
            do j=0,nx+1
              if (h(j)>eps) then
                u(j) = q(j)/max(0.d0, h(j))
              else
                u(j) = 0d0
              endif
            enddo
          ! ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
          elseif (trim(vars(l)).eq.'FC') then
          ! ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
            fric(0:nx+1) = f_int(0:nx+1,l)
          ! ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
          elseif (trim(vars(l)).eq.'ZF') then
          ! ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
            zf(0:nx+1) = f_int(0:nx+1,l)
          endif
          ! ~~~~~~~
          ! tracers
          ! ~~~~~~~
          if (ntrac.gt.0) then
            do k=1,ntrac
              char_trac = integer_to_string(k)
              if ((trim(vars(l)).eq."T"//char_trac(4:4)).or. &
     &            (trim(vars(l)).eq."T"//char_trac(3:4))) then
                t(0:nx+1,k) = f_int(0:nx+1, l)
                do j=0,nx+1
                  ht(j,k) = h(j)*t(j,k)
                enddo
              endif
            enddo
          endif ! end if trac
        enddo ! end assignment
        ! 
        ! deallocate tmp tables
        deallocate(x_tmp)
        deallocate(f_tmp)
        deallocate(f_int)
      endif ! end if use_ini_file
      ! ----------------------------------------------------------------
      return
      end subroutine
      ! ****************************************************************




      ! ****************************************************************
      subroutine check_ini_inputs(vars)
      ! ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
      ! checks variables in continuation file
      ! ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
      use module_data, only: use_mesh_file, bedload
      implicit none
      character(len=3), dimension(:), intent(in) :: vars
      logical    :: miss_h, miss_u, miss_q
      logical    :: miss_zb, miss_zf
      integer    :: k
      ! ----------------------------------------------------------------
      miss_h = .True.
      miss_u = .True.
      miss_q = .True.
      miss_zb = .True.
      miss_zf = .True.
      ! ----------------------------------------------------------------
      do k=1,size(vars)
        if (trim(vars(k)).eq.'ZB') then
          miss_zb = .False.
        else if (trim(vars(k)).eq.'H') then
          miss_h = .False.
        else if (trim(vars(k)).eq.'U') then
          miss_u = .False.
        else if (trim(vars(k)).eq.'Q') then
          miss_q = .False.
        else if (trim(vars(k)).eq.'ZF') then
          miss_zf = .False.
        endif
      enddo
      if (miss_h.eqv..True.) then
        write(*,*) '++++++++++++++++++++++++++++++++++++++++++++'
        write(*,*) ' Error reading hydro ini file, '
        write(*,*) ' must contain H'
        write(*,*) ' Stop.'
        write(*,*) '++++++++++++++++++++++++++++++++++++++++++++'
        stop
      endif
      if ((miss_u.eqv..True.).and.(miss_q.eqv..True.)) then
        write(*,*) '++++++++++++++++++++++++++++++++++++++++++++'
        write(*,*) ' Error reading hydro ini file, '
        write(*,*) ' must contain U or Q'
        write(*,*) ' Stop.'
        write(*,*) '++++++++++++++++++++++++++++++++++++++++++++'
        stop
      endif
      if ((miss_zb.eqv..True.).and.(use_mesh_file.eqv..False.)) then
        write(*,*) '++++++++++++++++++++++++++++++++++++++++++++'
        write(*,*) ' Hydro ini file do not contain ZB,'
        write(*,*) ' and no mesh file given, '
        write(*,*) ' ZB will be initiallized at 0. m'
        write(*,*) '++++++++++++++++++++++++++++++++++++++++++++'
      endif
      if ((miss_zf.eqv..True.).and. &
     &    (use_mesh_file.eqv..False.).and. &
     &    (bedload.eqv..True.)) then
        write(*,*) '++++++++++++++++++++++++++++++++++++++++++++'
        write(*,*) ' Hydro ini file do not contain ZF,'
        write(*,*) ' and no mesh file given, '
        write(*,*) ' ZF will be initiallized at -0.1 m'
        write(*,*) '++++++++++++++++++++++++++++++++++++++++++++'
      endif
      ! ----------------------------------------------------------------
      return
      end subroutine
      ! ****************************************************************




      end module
      ! ****************************************************************
