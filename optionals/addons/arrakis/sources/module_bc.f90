


      ! ****************************************************************
      module module_bc
      ! ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
      ! Boundary Conditions 
      ! ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
      use module_data, only: dp, nx, g
      implicit none
      ! ----------------------------------------------------------------



      contains




      ! ****************************************************************
      ! ****************************************************************
      ! ****************************************************************
      ! ****************************************************************
      !                    BOUNDARY CONDITIONS  
      ! ****************************************************************
      ! ****************************************************************
      ! ****************************************************************
      ! ****************************************************************




      ! ****************************************************************
      subroutine time_interp_bc(time, time_arr, val_arr, val)
      ! ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
      ! boundary conditions
      ! ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
      use module_data, only: 
      implicit none
      real(kind=dp), intent(in)              :: time
      real(kind=dp), intent(in)              :: time_arr(:)
      real(kind=dp), intent(in)              :: val_arr(:)
      real(kind=dp), intent(out)             :: val
      real(kind=dp)                          :: alpha
      integer                                :: i, ni
      logical                                :: found_value
      ! ----------------------------------------------------------------
      ni = size(val_arr)
      val = 0.d0
      found_value = .False.
      do i = 1, ni-1
        if ((time_arr(i) <= time).and.(time < time_arr(i+1))) then
          alpha = (time_arr(i+1)-time)/(time_arr(i+1)-time_arr(i))
          val = alpha*val_arr(i) + (1.d0-alpha)*val_arr(i+1)
          found_value = .True.
        end if
      end do
      ! ----------------------------------------------------------------
      if (found_value.eqv..False.) then
        write(*,*) '++++++++++++++++++++++++++++++++++++++++++++'
        write(*,*) ' ERROR READING LIQ BOUNDARY FILE, CHECK TIME'
        write(*,*) '++++++++++++++++++++++++++++++++++++++++++++'
        stop
      end if
      ! ----------------------------------------------------------------
      return
      end subroutine
      ! ****************************************************************




      ! ****************************************************************
      subroutine compute_cdl
      ! ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
      ! boundary conditions
      ! ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
      use module_data, only: ntrac, time, zb, &
    &                        bctype_l, bctypet_l, ha, ua, qa, ta, &
    &                        bctype_r, bctypet_r, hb, ub, qb, tb, &
    &                        use_bcfile_l, use_bcfile_r, &
    &                        time_bcl, time_bcr, field_bcl, field_bcr, &
    &                        bctypeqs_l, bctypeqs_r, &
    &                        vars_bcl, vars_bcr, &
    &                        nvars_bcl, nvars_bcr, &
    &                        qsa, qsb, Jeqa, Jeqb
      use module_io, only: integer_to_string
      implicit none
      real(kind=dp)    :: val_a, val_b
      integer          :: k, l
      character(len=4) :: char_trac
      ! ----------------------------------------------------------------
      ! LEFT BOUNDARY CONDITION
      ! ----------------------------------------------------------------
      ! ghost cell data from liq boundary file (time interpolation)
      ! **************************************
      if (use_bcfile_l.eqv..True.) then
        ! loop on fields in bc file
        do k=2,nvars_bcl ! skip time
          ! time interpolation
          call time_interp_bc(time, time_bcl, field_bcl(:,k-1), val_a)
          ! replace ghost cell imposed value with interpolated one
          if (vars_bcl(k).eq.'H') then
            ha = val_a
          endif
          if (vars_bcl(k).eq.'ZS') then
            ha = val_a - zb(0)
          endif
          if (vars_bcl(k).eq.'Q') then
            qa =-val_a
          endif
          if (vars_bcl(k).eq.'U') then
            ua =-val_a
          endif
          ! tracers (tracer index up to 99 here)
          if (ntrac.gt.0) then
            do l=1,ntrac
              char_trac = integer_to_string(l)
              if ((vars_bcl(k).eq."T"//char_trac(4:4)).or. &
             &    (vars_bcl(k).eq."T"//char_trac(3:4))) then
                ta(l) = val_a
              endif
            enddo
          endif ! end if trac
        enddo ! end of field loop
        ! check values
        if (ha.lt.0d0) then
          write(*,*) '++++++++++++++++++++++++++++++++++++++++++++'
          write(*,*) ' ERROR IN LIQ BOUNDARY FILE,                '
          write(*,*) ' IMPOSED WATER DEPTH H < 0                  '
          write(*,*) '++++++++++++++++++++++++++++++++++++++++++++'
          stop
        endif
      end if
      ! compute fluxes of left boundary
      ! *******************************
      call flux_cdl_ghostcell( 1, bctype_l, bctypet_l, bctypeqs_l, &
     &                        ha, ua, qa, ta, qsa, Jeqa)
      ! ----------------------------------------------------------------
      ! RIGHT BOUNDARY CONDITION
      ! ----------------------------------------------------------------
      ! ghost cell data from liq boundary file (time interpolation)
      ! *******************************
      if (use_bcfile_r.eqv..True.) then
        ! loop on fields in bc file
        do k=2,nvars_bcr ! skip time
          ! time interpolation
          call time_interp_bc(time, time_bcr, field_bcr(:,k-1), val_b)
          ! replace ghost cell imposed value with interpolated one
          if (vars_bcr(k).eq.'H') then
            hb = val_b
          endif
          if (vars_bcr(k).eq.'ZS') then
            hb = val_b - zb(nx+1)
          endif
          if (vars_bcr(k).eq.'Q') then
            qb =-val_b
          endif
          if (vars_bcr(k).eq.'U') then
            ub =-val_b
          endif
          ! tracers (tracer index up to 99 here)
          if (ntrac.gt.0) then
            do l=1,ntrac
              char_trac = integer_to_string(l)
              if ((vars_bcr(k).eq."T"//char_trac(4:4)).or. &
             &    (vars_bcr(k).eq."T"//char_trac(3:4))) then
                tb(l) = val_b
              endif
            enddo
          endif ! end if trac
        enddo ! end of field loop
        ! check values
        if (hb.lt.0d0) then
          write(*,*) '++++++++++++++++++++++++++++++++++++++++++++'
          write(*,*) ' ERROR IN LIQ BOUNDARY FILE,                '
          write(*,*) ' IMPOSED WATER DEPTH H < 0                  '
          write(*,*) '++++++++++++++++++++++++++++++++++++++++++++'
          stop
        endif
      end if
      ! compute fluxes of right boundary
      ! ********************************
      call flux_cdl_ghostcell(nx, bctype_r, bctypet_r, bctypeqs_r, &
     &                        hb, ub, qb, tb, qsb, Jeqb)
      ! ----------------------------------------------------------------
      return
      end subroutine
      ! ****************************************************************




      ! ****************************************************************
      subroutine flux_cdl_ghostcell(i, typcl, typclt, typclqs, &
     &                              hu, uu, qu, tu, qsu, Jeq)
      ! ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
      ! boundary conditions
      ! 
      ! Ghost cells:
      !  u : user imposed values
      !  i : internal
      !  e : external
      ! 
      !   Wu=ha,qa                             Wu=hb,qb <- user defined
      !    o---|---o---     ...      ---o---|---o
      !    0   A   1                   nx   B  nx+1
      !   We       Wi                  Wi       We
      ! 
      ! ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
      use module_data, only: eps, h, u, t, ntrac, clqoru, flut_cell, &
    &                        fric, bedload, qsed, &
    &                        bedload_formula, &
    &                        Lx, Rh, fric
      use module_bedload, only: qs
      implicit none
      integer, intent(in)          :: i
      integer, intent(in)          :: typcl, typclqs
      integer, intent(in)          :: typclt(:)
      real(kind=dp), intent(in)    :: hu, uu, qu, qsu, Jeq
      real(kind=dp), intent(in)    :: tu(:)
      real(kind=dp)                :: he, ue, qe, ce, te
      real(kind=dp)                :: hi, ui, ci
      real(kind=dp)                :: fh_bord, fq_bord, ft_bord
      real(kind=dp)                :: qsedi, qsede, ueq
      real(kind=dp)                :: fqsed_bord
      real(kind=dp)                :: lamb1, lamb2, regime
      integer                      :: k
      ! ----------------------------------------------------------------
      ! hydrodynamic boundary conditions
      ! ----------------------------------------------------------------
      hi = h(i)
      ! vitesse normale à la frontiere:
      if (i.eq.1) then
        ui = -u(i) ! rotation
      elseif (i.eq.nx) then
        ui = u(i)
      endif
      ! ~~~~~~~~~~~~~
      ! wall boundary
      ! ~~~~~~~~~~~~~
      if (typcl == 1) then
        he = hi
        ! strong imposition:
        !ui = 0d0
        !ue = ui
        ! weak imposition:
        ue = -ui
      ! ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
      ! h imposed (fluvial or torrential inflow)
      ! ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
      elseif (typcl == 2) then
        he = hu
        ci = sqrt(g*hi)
        ce = sqrt(g*he)
        lamb1 = ui - ci
        lamb2 = ui + ci
        regime = lamb1*lamb2
        if ((regime.lt.0d0).or.(ui.le.0d0)) then
          ! fluvial or torrential inflow
          if (hi>eps) then
            if (regime.lt.0d0) then
              ! fluvial: conservation of 2-Riemann invariant 
              ue = ui + 2.d0*(ci-ce)
            else
              ! torrential inflow: imposition of h and u
              if (clqoru.eq.1) then
                  qe = qu
                  ue = qe/max(eps, he)
              else
                  ue = uu
              endif
            endif
          else
            ! dry cell
            ue = 0d0
          endif
        else
          ! torrential outflow: no need to impose h nor u
          he = 0d0
          ue = ui
        endif
      ! ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
      ! u imposed (fluvial or torrential inflow)
      ! ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
      elseif (typcl == 3) then
        if (clqoru.eq.1) then
            qe = qu
            ue = qe/max(eps, hi)
        else
            ue = uu
        endif
        ci = sqrt(g*hi)
        lamb1 = ui - ci
        lamb2 = ui + ci
        regime = lamb1*lamb2
        if ((regime.lt.0d0).or.(ui.le.0d0)) then
          ! fluvial or torrential inflow
          if (regime.lt.0d0) then
            ! fluvial: conservation of 2-Riemann invariant 
            he = ((ui - ue + 2d0*sqrt(g*hi))**2)/(4d0*g)
          else
            ! torrential inflow: imposition of h and u
            he = hu
          endif
        else
          ! torrential outflow: no need to impose h nor u
          he = 0d0
          ue = ui
        endif
      ! ~~~~~~~~~~~~~~~~~~~~~~~~
      ! torrential inlet -> h, u 
      ! ~~~~~~~~~~~~~~~~~~~~~~~~
      elseif (typcl == 4) then
        he = hu
        if (clqoru.eq.1) then
            ue = qu/max(eps, he)
        else
            ue = uu
        endif
      ! ~~~~~~~~~~~~~~~~~~~~~~~~~
      ! torrential outlet -> none 
      ! ~~~~~~~~~~~~~~~~~~~~~~~~~
      elseif (typcl == 5) then
        he = 0d0 ! hi
        ue = ui
      ! ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
      ! Neuman on h and u (strong imposition)
      ! ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
      elseif (typcl == 6) then
        he = hi
        ue = ui
      ! ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
      ! Neuman on h and u (strong imposition)
      ! ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
      elseif (typcl == 7) then
        if (i.eq.1) then
          he = h(nx)
          ue =-u(nx)
        elseif (i.eq.nx) then
          he = h(1)
          ue = u(1)
        endif
      ! ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
      else
        write(*,*) '++++++++++++++++++++++++++++++++++++++++++++'
        write(*,*) ' Unknown LEFT or RIGHT BOUNDARY CONDITION   '
        write(*,*) '++++++++++++++++++++++++++++++++++++++++++++'
        stop
      endif
      ! ----------------------------------------------------------------
      ! bedload boundary conditions:
      ! ----------------------------------------------------------------
      if (bedload.eqv..True.) then
        ! bedload flux on last cell
        if (i.eq.1) then
          qsedi = -qsed(i) ! rotation
        elseif (i.eq.nx) then
          qsedi = qsed(i)
        endif
        ! ~~~~~~~~~~~~~~~~~~~~~
        ! Dirichlet, qs imposed
        ! ~~~~~~~~~~~~~~~~~~~~~
        if(typclqs.eq.1) then
          if (i.eq.1) then
            qsede = -1d0*qsu
          else
            qsede = qsu
          endif
        ! ~~~~~~~~~~~~~~~~~~~~~~~~~~~~
        ! Dirichlet, equilibrium slope
        ! ~~~~~~~~~~~~~~~~~~~~~~~~~~~~
        else if(typclqs.eq.4) then
          ! Grass formula -> Jeq replaced by equilibrium velocity
          if (bedload_formula.le.2) then
            ueq = sqrt(Jeq*fric(i)**2*Rh(i)**(4d0/3d0))
            qsede = qs(h(i), ueq, Lx(i), 0d0, fric(i))
          ! Other formulas (which depend on J)
          else
            qsede = qs(h(i), u(i), Lx(i), Jeq, fric(i))
          endif
          if (i.eq.1) then
            qsede = -1d0*qsede
          endif
        ! ~~~~~~
        ! Neuman
        ! ~~~~~~
        elseif(typclqs.eq.2) then
          if (i.eq.1) then
            qsede = -qsed(i)
          elseif (i.eq.nx) then
            qsede = qsed(i)
          endif
        ! ~~~~~~~~
        ! Periodic
        ! ~~~~~~~~
        elseif(typclqs.eq.3) then
          if (i.eq.1) then
            qsede = -qsed(nx)
          elseif (i.eq.nx) then
            qsede = qsed(1)
          endif
        else
          write(*,*) '++++++++++++++++++++++++++++++++++++++++++++++++++++'
          write(*,*) ' Unknown LEFT or RIGHT BEDLOAD BOUNDARY CONDITION   '
          write(*,*) '++++++++++++++++++++++++++++++++++++++++++++++++++++'
          stop
        endif
      endif  

      ! ----------------------------------------------------------------
      ! Flux computation
      ! ----------------------------------------------------------------
      ! Flux Shallow Water system
      ! *************************
      if (bedload.eqv..False.) then
        call flux_bc_sv(i, fh_bord, fq_bord, hi, he, ui, ue)

      ! Flux Shallow Water Exner system
      ! *******************************
      else 
        call flux_bc_sve(i, fh_bord, fq_bord, fqsed_bord, &
     &                   hi, he, ui, ue, qsedi, qsede)
      endif

      ! ----------------------------------------------------------------
      ! tracers boundary conditions:
      ! ----------------------------------------------------------------
      if (ntrac.gt.0) then
        do k=1,ntrac
          ! ~~~~~~~~~
          ! Dirichlet
          ! ~~~~~~~~~
          if (typclt(k) == 1) then
            te = tu(k)
          ! ~~~~~~
          ! Neuman
          ! ~~~~~~
          elseif (typclt(k) == 2) then
            te = t(i,k)
          ! ~~~~~~~~
          ! Periodic
          ! ~~~~~~~~
          elseif (typclt(k) == 3) then
            if (i.eq.1) then
              te = t(nx,k)
            elseif (i.eq.nx) then
              te = t(1,k)
            endif
          else
            write(*,*) '++++++++++++++++++++++++++++++++++++++++++++++++++'
            write(*,*) ' Unknown LEFT or RIGHT TRACERBOUNDARY CONDITION   '
            write(*,*) '++++++++++++++++++++++++++++++++++++++++++++++++++'
            stop
          endif
          ! ~~~~~~~
          ! outflow
          ! ~~~~~~~
          if (fh_bord.ge.0d0) then
            ft_bord = t(i,k)*fh_bord
          ! ~~~~~~
          ! inflow
          ! ~~~~~~
          else
            ft_bord = te*fh_bord
          endif
          flut_cell(i,k) = flut_cell(i,k) + ft_bord
        enddo
      endif
      ! ----------------------------------------------------------------
      return
      end subroutine
      ! ****************************************************************





      ! ****************************************************************
      subroutine flux_bc_sv(i, fhg, fqg, hg, hd, ug, ud)
      ! ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
      ! Compute boundary fluxes for the Shallow Water system
      ! ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
      use module_data, only: hydrostarec, zb, bnd_flusrc, &
     &                       fluh_cell, fluq_cell, &
     &                       fluh_src, fluq_src, &
     &                       variable_width   
      use module_fluxes_sv, only: flux_sv, flux_src_zb, flux_src_width
      implicit none
      integer, intent(in)          :: i
      real(kind=dp), intent(inout) :: fhg, fqg
      real(kind=dp)                :: fhd, fqd
      real(kind=dp)                :: fhg_src, fhd_src
      real(kind=dp)                :: fqg_src, fqd_src
      real(kind=dp), intent(in)    :: hg, hd, ug, ud
      real(kind=dp)                :: zbg, zbd
      real(kind=dp)                :: sgn
      real(kind=dp)                :: dzp, dzm, hip, him
      real(kind=dp)                :: etag, etad, zip12
      integer                      :: hydrec = 0
      ! ----------------------------------------------------------------
      ! inverse rotation in case of left boudary
      if (i.eq.1) then
        sgn = -1d0
        zbg = zb(i)
        zbd = zb(i)
      else
        sgn = 1d0
        zbg = zb(i)
        zbd = zb(i)
      endif
      ! ----------------------------------------------------------------
      ! compute flu src in bc
      if (bnd_flusrc.eq.1) then
        hydrec = hydrostarec
      else
        hydrec = 0
      endif
      ! ----------------------------------------------------------------
      ! 
      ! hydrostatic reconstruction
      ! --------------------------
      ! Audusse & al. A fast and stable well-balanced scheme
      ! with hydrostatic reconstruction for shallow water flows
      if (hydrec.eq.1) then
        dzm = max(0.d0, zbd- zbg)
        him = max(0.d0, hg - dzm)
        dzp = max(0.d0, zbg- zbd)
        hip = max(0.d0, hd - dzp)
      ! Chen and Noelle. A new hydrostatic reconstruction
      ! scheme based on subcell reconstructions
      elseif (hydrec.eq.2) then
        etag = hg + zbg
        etad = hd + zbd
        zip12 = min(max(zbg, zbd), min(etag, etad))
        him = min(etag - zip12, hg)
        hip = min(etad - zip12, hd)
      else
        him = hg
        hip = hd
      endif
      ! 
      ! shallow water flux
      ! ------------------
      call flux_sv(fhg, fhd, fqg, fqd, him, hip, ug, ud, zbg, zbd)
      fluh_cell(i) = fluh_cell(i) + fhg
      fluq_cell(i) = fluq_cell(i) + fqg*sgn
      ! 
      ! bathy source
      ! ------------
      if (bnd_flusrc.eq.1) then
        call flux_src_zb(fhg_src, fhd_src, fqg_src, fqd_src, &
     &                   hg, hd, him, hip, zbg, zbd, ug, ud)
        fluh_src(i) = fluh_src(i) + fhg_src
        fluq_src(i) = fluq_src(i) + fqg_src
      endif
      ! 
      ! width source
      ! ------------
      if ((bnd_flusrc.eq.1).and.(variable_width.eqv..True.)) then
        call flux_src_width(i, fhg_src, fhd_src, fqg_src, fqd_src, &
     &                      hg, hd, ug, ud)
        fluh_src(i) = fluh_src(i) + fhg_src
        fluq_src(i) = fluq_src(i) + fqg_src
      endif
      ! ----------------------------------------------------------------
      return
      end subroutine
      ! ****************************************************************





      ! ****************************************************************
      subroutine flux_bc_sve(i, fhg, fqg, fqsg, hg, hd, ug, ud, qsg, qsd)
      ! ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
      ! Compute boundary fluxes for the Shallow Water system
      ! ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
      use module_data, only: hydrostarec, zb, bnd_flusrc, &
     &                       fluh_cell, fluq_cell, fluqsed_cell, &
     &                       fluh_src, fluq_src, fluqsed_src, &
     &                       variable_width
      use module_fluxes_sv,  only: flux_src_zb
      use module_fluxes_sve, only: flux_sve, flux_src_zb_sve, &
     &                             flux_src_width_sve
      implicit none
      integer, intent(in)          :: i
      real(kind=dp), intent(inout) :: fhg, fqg, fqsg
      real(kind=dp)                :: fhd, fqd, fqsd
      real(kind=dp)                :: fhg_src, fhd_src
      real(kind=dp)                :: fqg_src, fqd_src
      real(kind=dp)                :: fqsg_src, fqsd_src
      real(kind=dp), intent(in)    :: hg, hd, ug, ud, qsg, qsd
      real(kind=dp)                :: zbg, zbd
      real(kind=dp)                :: dzp, dzm, hip, him
      real(kind=dp)                :: etag, etad, zip12
      real(kind=dp)                :: sgn
      integer                      :: hydrec = 0
      ! ----------------------------------------------------------------
      ! inverse rotation in case of left boudary
      if (i.eq.1) then
        sgn = -1d0
        zbg = zb(i)
        zbd = zb(i)
      else
        sgn = 1d0
        zbg = zb(i)
        zbd = zb(i)
      endif
      ! ----------------------------------------------------------------
      ! compute flu src in bc
      if (bnd_flusrc.eq.1) then
        hydrec = hydrostarec
      else
        hydrec = 0
      endif
      ! ----------------------------------------------------------------
      ! 
      ! hydrostatic reconstruction
      ! --------------------------
      ! Audusse & al. A fast and stable well-balanced scheme
      ! with hydrostatic reconstruction for shallow water flows
      if (hydrec.eq.1) then
        dzm = max(0.d0, zbd- zbg)
        him = max(0.d0, hg - dzm)
        dzp = max(0.d0, zbg- zbd)
        hip = max(0.d0, hd - dzp)
      ! Chen and Noelle. A new hydrostatic reconstruction
      ! scheme based on subcell reconstructions
      elseif (hydrec.eq.2) then
        etag = hg + zbg
        etad = hd + zbd
        zip12 = min(max(zbg, zbd), min(etag, etad))
        him = min(etag - zip12, hg)
        hip = min(etad - zip12, hd)
      else
        him = hg
        hip = hd
      endif
      ! 
      ! Flux SWE
      ! --------
      call flux_sve(fhg, fhd, fqg, fqd, fqsg, fqsd, i, &
     &              him, hip, ug, ud, zbg, zbd, qsg, qsd)

      ! Flux update
      ! -----------
      fluh_cell(i)    = fluh_cell(i)    + fhg
      fluq_cell(i)    = fluq_cell(i)    + fqg*sgn
      fluqsed_cell(i) = fluqsed_cell(i) + fqsg

      ! bathy source
      ! ------------
      if (bnd_flusrc.eq.1) then
        call flux_src_zb_sve(fhg_src, fhd_src, fqg_src, fqd_src, &
     &                       fqsg_src, fqsd_src, &
     &                       hg, hd, him, hip, zbg, zbd)
        fluh_src(i)    = fluh_src(i)    + fhg_src
        fluq_src(i)    = fluq_src(i)    + fqg_src
        fluqsed_src(i) = fluqsed_src(i) + fqsg_src
      endif
      ! 
      ! width source
      ! ------------
      if ((bnd_flusrc.eq.1).and.(variable_width.eqv..True.)) then
        call flux_src_width_sve(fhg_src, fhd_src, fqg_src, fqd_src, &
     &                          fqsg_src, fqsd_src, i, &
     &                          hg, hd, ug, ud, qsg, qsd)
        fluh_src(i)    = fluh_src(i)   + fhg_src
        fluq_src(i)    = fluq_src(i)   + fqg_src
        fluqsed_src(i) = fluqsed_src(i)+ fqsg_src
      endif
      ! ----------------------------------------------------------------
      return
      end subroutine
      ! ****************************************************************




      end module
      ! ****************************************************************
