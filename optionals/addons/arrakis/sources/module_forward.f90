


      ! ****************************************************************
      module module_forward
      ! ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
      ! Time update module
      ! ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
      use module_data, only: dp, dt, ci, eps, nx, g
      implicit none
      ! ----------------------------------------------------------------



      contains



      ! ****************************************************************
      ! ****************************************************************
      ! ****************************************************************
      ! ****************************************************************
      !                      TIME UPDATE FUNCTIONS
      ! ****************************************************************
      ! ****************************************************************
      ! ****************************************************************
      ! ****************************************************************




      !*****************************************************************
      subroutine forward
      ! ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
      ! solution increment over one time step
      ! ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
      use module_data, only: time_scheme, bedload
      implicit none
      ! ----------------------------------------------------------------
      ! Solver for Saint-Venant equations
      ! ----------------------------------------------------------------
      if (bedload.eqv..False.) then
        ! ************
        ! Euler scheme
        ! ************
        if (time_scheme.eq.1) then
          call update_sv_euler
        ! ***********
        ! Heun scheme
        ! ***********
        elseif (time_scheme.eq.2) then
          call update_sv_heun
        ! *********************************
        ! Runge-Kutta (second order) scheme
        ! *********************************
        elseif (time_scheme.eq.3) then
          call update_sv_rk2
        ! ********************************
        ! Runge-Kutta (third order) scheme
        ! ********************************
        elseif (time_scheme.eq.4) then
          call update_sv_rk3
        else
          write(*,*) '++++++++++++++++++++++++++++++++++++++++++++'
          write(*,*) ' Unknown TIME SCHEME for SV                 '
          write(*,*) '++++++++++++++++++++++++++++++++++++++++++++'
          stop
        endif
      ! ----------------------------------------------------------------
      ! Solver for Saint-Venant-Exner equations
      ! ----------------------------------------------------------------
      else
        ! ************
        ! Euler scheme
        ! ************
        if (time_scheme.eq.1) then
          call update_sve_euler
        ! ***********
        ! Heun scheme
        ! ***********
        elseif (time_scheme.eq.2) then
          call update_sve_heun
        else
          write(*,*) '++++++++++++++++++++++++++++++++++++++++++++'
          write(*,*) ' Unknown TIME SCHEME for SVE                '
          write(*,*) '++++++++++++++++++++++++++++++++++++++++++++'
          stop
        endif
      ! ----------------------------------------------------------------
      endif 
      ! ----------------------------------------------------------------
      end subroutine  
      !*****************************************************************




      !*****************************************************************
      subroutine update_u(h, q, u)
      ! ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
      ! update of non-conservative variables: u from q
      ! ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
      implicit none
      real(kind=dp), intent(in)    :: h
      real(kind=dp), intent(inout) :: q
      real(kind=dp), intent(out)   :: u
      logical, parameter           :: clip_q = .True.
      ! ----------------------------------------------------------------
      if (h>eps) then
        u = q/h
      else
        u = 0d0
        if (clip_q.eqv..True.) then
          q = 0d0
        endif
      endif
      ! ----------------------------------------------------------------
      end subroutine  
      !*****************************************************************




      !*****************************************************************
      subroutine update_t(h, ht, t)
      ! ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
      ! update of non-conservative variables: t from ht
      ! ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
      implicit none
      real(kind=dp), intent(in)   :: h, ht
      real(kind=dp), intent(out)  :: t
      ! ----------------------------------------------------------------
      if (h>eps) then
        t = ht/h
      else
        t = ht/eps
      endif
      ! ----------------------------------------------------------------
      end subroutine  
      !*****************************************************************
      



      !*****************************************************************
      subroutine update_Rh(h, Lx, Rh)
      ! ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
      ! update hydraulic radius
      ! ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
      use module_data, only: variable_width, eps
      implicit none
      ! ----------------------------------------------------------------
      real(kind=dp), intent(in)        :: h
      real(kind=dp), intent(in)        :: Lx
      real(kind=dp), intent(inout)     :: Rh
      ! ----------------------------------------------------------------
      if (variable_width.eqv..True.) then
        Rh = max((h*Lx)/(Lx + 2.d0*h), eps)
      else
        Rh = max(h, eps)
      endif
      ! ----------------------------------------------------------------
      return
      end subroutine
      !*****************************************************************




      ! ****************************************************************
      ! ****************************************************************
      ! ****************************************************************
      ! ****************************************************************
      !                SAINT-VENANT FINITE VOLUME UPDATE
      ! ****************************************************************
      ! ****************************************************************
      ! ****************************************************************
      ! ****************************************************************




      !*****************************************************************
      subroutine update_sv_euler
      ! ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
      ! Update step - Euler
      ! ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
      use module_data, only: Rh, Lx, zb, h, u, q, t, ht, fric, &
     &                       ntrac, ordre, ordre_trac, &
     &                       fluh_cell, fluh_src, &
     &                       fluq_cell, fluq_src, &
     &                       flut_cell, flut_src
      use module_timestep,   only: init_time_step, adjust_time_step
      use module_high_order, only: compute_reconstructions
      use module_fluxes_sv,  only: init_fluxes, compute_flux_sw
      use module_bc,         only: compute_cdl
      use module_sources,    only: source_friction
      implicit none
      integer                     :: i, k
      real(kind=dp)               :: fact
      ! ----------------------------------------------------------------
      call init_time_step(h, u)
      call init_fluxes
      if ((ordre.ge.2).or.(ordre_trac.ge.2)) then
        call compute_reconstructions(zb, h, u, q, t)
      endif
      call compute_flux_sw(zb, h, u, t)
      call adjust_time_step
      call compute_cdl
      ! ----------------------------------------------------------------
      do i=1,nx
        fact = dt/ci(i)
        ! update of h, q
        h(i) = h(i) - fact*( fluh_cell(i) + fluh_src(i) )
        q(i) = q(i) - fact*( fluq_cell(i) + fluq_src(i) )
        ! update of ht
        if (ntrac>0) then
          do k=1,ntrac
            ht(i,k) = ht(i,k) - fact*( flut_cell(i,k) + flut_src(i,k) )
            call update_t(h(i), ht(i,k), t(i,k))
          enddo
        endif
        ! Sources
        call update_Rh(h(i), Lx(i), Rh(i))
        call source_friction(h(i), Rh(i), q(i), fric(i))
        ! update u
        call update_u(h(i), q(i), u(i))
      enddo
      ! ----------------------------------------------------------------
      end subroutine  
      !*****************************************************************




      !*****************************************************************
      subroutine update_sv_heun
      ! ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
      ! Update step - Heun 
      ! ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
      use module_data, only: Rh, Lx, zb, h, u, q, t, ht, fric, &
     &                       ntrac, ordre, ordre_trac, &
     &                       fluh_cell, fluh_src, &
     &                       fluq_cell, fluq_src, &
     &                       flut_cell, flut_src
      use module_timestep,   only: init_time_step, adjust_time_step
      use module_high_order, only: compute_reconstructions
      use module_fluxes_sv,  only: init_fluxes, compute_flux_sw
      use module_bc,         only: compute_cdl
      use module_sources,    only: source_friction
      implicit none
      real(kind=dp)                :: h0   (0:nx+1)
      real(kind=dp)                :: q0   (0:nx+1)
      real(kind=dp)                :: ht0  (0:nx+1,ntrac)
      real(kind=dp)                :: dt1, dt2, aux1, aux2, gamma
      real(kind=dp)                :: fact
      integer                      :: iord, mord
      integer                      :: i, k
      ! ----------------------------------------------------------------
      ! time order
      !   if mord==1: Euler scheme (1st order)
      !   if mord==2: Heun scheme (second order)
      mord = 2
      ! ----------------------------------------------------------------
      if (mord==2) then
        do i=0,nx+1
          h0(i) = h(i)
          q0(i) = q(i)
          if (ntrac>0) then
            do k=1,ntrac
              ht0(i,k) = ht(i,k)
            enddo
          endif
        enddo
      endif
      ! ----------------------------------------------------------------
      ! Heun iterations (iord=1: first, iord=2: second)
      ! ----------------------------------------------------------------
      do iord=1,mord
        call init_time_step(h, u)
        call init_fluxes
        if ((ordre.ge.2).or.(ordre_trac.ge.2)) then
          call compute_reconstructions(zb, h, u, q, t)
        endif
        call compute_flux_sw(zb, h, u, t)
        call adjust_time_step
        call compute_cdl
        if (iord==1) then
          dt1 = dt
        endif
        ! --------------------------------------------------------------
        do i=1,nx
          fact = dt/ci(i)
          ! update of h, q
          h(i) = h(i) - fact*( fluh_cell(i) + fluh_src(i) )
          q(i) = q(i) - fact*( fluq_cell(i) + fluq_src(i) )
          ! update of ht
          if (ntrac>0) then
            do k=1,ntrac
              ht(i,k) = ht(i,k)- fact*(flut_cell(i,k) + flut_src(i,k))
              call update_t(h(i), ht(i,k), t(i,k))
            enddo
          endif
          ! sources
          call update_Rh(h(i), Lx(i), Rh(i))
          call source_friction(h(i), Rh(i), q(i), fric(i))
          ! update u
          call update_u(h(i), q(i), u(i))
        enddo
      enddo
      ! ----------------------------------------------------------------
      ! Heun update:
      ! ----------------------------------------------------------------
      if (mord==2) then
        dt2=dt
        aux1 = 2d0*dt1*dt2
        aux2 = dt1+dt2
        dt = aux1/aux2
        gamma = aux1/aux2**2
        do i=1,nx
          ! update of h, q
          h(i) = (1d0-gamma)*h0(i) + gamma*h(i)
          q(i) = (1d0-gamma)*q0(i) + gamma*q(i)
          ! update of ht
          if (ntrac>0) then
            do k=1,ntrac
              ht(i,k) = (1d0-gamma)*ht0(i,k) + gamma*ht(i,k)
              call update_t(h(i), ht(i,k), t(i,k))
            enddo
          endif
          ! update u
          call update_u(h(i), q(i), u(i))
        enddo
      endif
      ! ----------------------------------------------------------------
      end subroutine  
      !*****************************************************************




      !*****************************************************************
      subroutine update_sv_rk2
      ! ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
      ! Update step - Second order TVD Runge-Kutta
      ! Gottlieb, S. & Shu, C.-W. Total variation diminishing Runge-Kutta
      ! schemes Mathematics of computation, 1998, 67, 73-85
      ! ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
      use module_data, only: Rh, Lx, zb, h, u, q, t, ht, fric, &
     &                       ntrac, ordre, ordre_trac, &
     &                       fluh_cell, fluh_src, &
     &                       fluq_cell, fluq_src, &
     &                       flut_cell, flut_src
      use module_timestep,   only: init_time_step, adjust_time_step
      use module_high_order, only: compute_reconstructions
      use module_fluxes_sv,  only: init_fluxes, compute_flux_sw
      use module_bc,         only: compute_cdl
      use module_sources,    only: source_friction
      implicit none
      real(kind=dp)                :: h1  (0:nx+1)
      real(kind=dp)                :: u1  (0:nx+1)
      real(kind=dp)                :: q1  (0:nx+1)
      real(kind=dp)                :: t1  (0:nx+1,1:ntrac)
      real(kind=dp)                :: ht1 (0:nx+1,1:ntrac)
      real(kind=dp)                :: fact
      integer                      :: i, k
      ! ----------------------------------------------------------------
      ! First step RK2
      ! ----------------------------------------------------------------
      call init_time_step(h, u)       ! Only at first step
      call init_fluxes
      if ((ordre.ge.2).or.(ordre_trac.ge.2)) then
        call compute_reconstructions(zb, h, u, q, t)
      endif
      call compute_flux_sw(zb, h, u, t)
      call adjust_time_step       ! Only at first step
      call compute_cdl
      ! ----------------------------------------------------------------
      do i=1,nx
        fact = dt/ci(i)
        h1(i) = h(i) - fact*( fluh_cell(i) + fluh_src(i) )
        q1(i) = q(i) - fact*( fluq_cell(i) + fluq_src(i) )
        ! update of ht
        if (ntrac>0) then
          do k=1,ntrac
            ht1(i,k) = ht(i,k) - fact*(flut_cell(i,k) + flut_src(i,k))
            call update_t(h1(i), ht1(i,k), t1(i,k))
          enddo
        endif
        ! sources
        call update_Rh(h1(i), Lx(i), Rh(i))
        call source_friction(h1(i), Rh(i), q1(i), fric(i))
        ! update u
        call update_u(h1(i), q1(i), u1(i))
      enddo
      ! ----------------------------------------------------------------
      ! Second step RK2
      ! ----------------------------------------------------------------
      call init_fluxes
      if ((ordre.ge.2).or.(ordre_trac.ge.2)) then
        call compute_reconstructions(zb, h1, u1, q1, t1)
      endif
      call compute_flux_sw(zb, h1, u1, t)
      call compute_cdl
      ! ----------------------------------------------------------------
      do i=1,nx
        fact = dt/ci(i)
        h(i) = 0.5d0*(h(i) + h1(i) - fact*( fluh_cell(i) + fluh_src(i) ))
        q(i) = 0.5d0*(q(i) + q1(i) - fact*( fluq_cell(i) + fluq_src(i) ))
        ! update of ht
        if (ntrac>0) then
          do k=1,ntrac
            ht1(i,k) = 0.5d0*(ht(i,k) + ht1(i,k) &
     &                        -fact*(flut_cell(i,k) + flut_src(i,k)))
            call update_t(h(i), ht(i,k), t(i,k))
          enddo
        endif
        ! sources
        call update_Rh(h(i), Lx(i), Rh(i))
        call source_friction(h(i), Rh(i), q(i), fric(i))
        ! update u
        call update_u(h(i), q(i), u(i))
      enddo
      ! ----------------------------------------------------------------
      end subroutine  
      !*****************************************************************




      !*****************************************************************
      subroutine update_sv_rk3
      ! ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
      ! Update step - Third order TVD Runge-Kutta
      ! Gottlieb, S. & Shu, C.-W. Total variation diminishing Runge-Kutta
      ! schemes Mathematics of computation, 1998, 67, 73-85
      ! ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
      use module_data, only: Rh, Lx, zb, h, u, q, t, ht, fric, &
     &                       ntrac, ordre, ordre_trac, &
     &                       fluh_cell, fluh_src, &
     &                       fluq_cell, fluq_src, &
     &                       flut_cell, flut_src
      use module_timestep,   only: init_time_step, adjust_time_step
      use module_high_order, only: compute_reconstructions
      use module_fluxes_sv,  only: init_fluxes, compute_flux_sw
      use module_bc,         only: compute_cdl
      use module_sources,    only: source_friction
      implicit none
      real(kind=dp)                :: h0  (0:nx+1)
      real(kind=dp)                :: q0  (0:nx+1)
      real(kind=dp)                :: h2  (0:nx+1)
      real(kind=dp)                :: u2  (0:nx+1)
      real(kind=dp)                :: q2  (0:nx+1)
      real(kind=dp)                :: t2  (0:nx+1,1:ntrac)
      real(kind=dp)                :: ht0 (0:nx+1,1:ntrac)
      real(kind=dp)                :: ht2 (0:nx+1,1:ntrac)
      real(kind=dp)                :: fact
      integer                      :: i, k
      ! ----------------------------------------------------------------
      ! First step RK3 -> W1 (strored in W)
      ! ----------------------------------------------------------------
      do i=0,nx+1
        h0(i) = h(i)
        q0(i) = q(i)
        if (ntrac>0) then
          do k=1,ntrac
            ht0(i,k) = ht(i,k)
          enddo
        endif
      enddo
      call init_time_step(h, u)  ! Only at first step
      call init_fluxes
      if ((ordre.ge.2).or.(ordre_trac.ge.2)) then
        call compute_reconstructions(zb, h, u, q, t)
      endif
      call compute_flux_sw(zb, h, u, t)
      call adjust_time_step     ! Only at first step
      call compute_cdl
      ! ----------------------------------------------------------------
      do i=1,nx
        fact = dt/ci(i)
        h(i) = h(i) - fact*( fluh_cell(i) + fluh_src(i) )
        q(i) = q(i) - fact*( fluq_cell(i) + fluq_src(i) )
        ! update of ht
        if (ntrac>0) then
          do k=1,ntrac
            ht(i,k) = ht(i,k) - fact*(flut_cell(i,k) + flut_src(i,k))
            call update_t(h(i), ht(i,k), t(i,k))
          enddo
        endif
        ! sources
        call update_Rh(h(i), Lx(i), Rh(i))
        call source_friction(h(i), Rh(i), q(i), fric(i))
        ! update u
        call update_u(h(i), q(i), u(i))
      enddo
      ! ----------------------------------------------------------------
      ! Second step RK3 -> W2
      ! ----------------------------------------------------------------
      call init_fluxes
      if ((ordre.ge.2).or.(ordre_trac.ge.2)) then
        call compute_reconstructions(zb, h, u, q, t)
      endif
      call compute_flux_sw(zb, h, u, t)
      call compute_cdl
      ! ----------------------------------------------------------------
      do i=1,nx
        fact = dt/ci(i)
        h(i) = h(i) - fact*( fluh_cell(i) + fluh_src(i) )
        q(i) = q(i) - fact*( fluq_cell(i) + fluq_src(i) )
        ! update of ht
        if (ntrac>0) then
          do k=1,ntrac
            ht(i,k) = ht(i,k) - fact*(flut_cell(i,k) + flut_src(i,k))
            call update_t(h(i), ht(i,k), t(i,k))
          enddo
        endif
        ! sources
        call update_Rh(h(i), Lx(i), Rh(i))
        call source_friction(h(i), Rh(i), q(i), fric(i))
        ! update u
        call update_u(h(i), q(i), u(i))
        ! --------------------------------------------------------------
        ! maj RK step
        h2(i) = (3d0/4d0)*h0(i) + (1d0/4d0)*h(i)
        q2(i) = (3d0/4d0)*q0(i) + (1d0/4d0)*q(i)
        call update_u(h2(i), q2(i), u2(i))
        if (ntrac>0) then
          do k=1,ntrac
            ht2(i,k) = (3d0/4d0)*ht0(i,k) + (1d0/4d0)*ht(i,k)
            call update_t(h2(i), ht2(i,k), t2(i,k))
          enddo
        endif
      enddo
      ! ----------------------------------------------------------------
      ! Third step RK3 -> Wn+1
      ! ----------------------------------------------------------------
      call init_fluxes
      if ((ordre.ge.2).or.(ordre_trac.ge.2)) then
        call compute_reconstructions(zb, h2, u2, q2, t2)
      endif
      call compute_flux_sw(zb, h2, u2, t2)
      call compute_cdl
      ! ----------------------------------------------------------------
      do i=1,nx
        fact = dt/ci(i)
        h2(i) = h2(i) - fact*( fluh_cell(i) + fluh_src(i) )
        q2(i) = q2(i) - fact*( fluq_cell(i) + fluq_src(i) )
        ! update of ht
        if (ntrac>0) then
          do k=1,ntrac
            ht2(i,k) = ht2(i,k) - fact*(flut_cell(i,k) + flut_src(i,k))
            call update_t(h2(i), ht2(i,k), t2(i,k))
          enddo
        endif
        ! sources
        call update_Rh(h2(i), Lx(i), Rh(i))
        call source_friction(h2(i), Rh(i), q2(i), fric(i))
        ! update u
        call update_u(h2(i), q2(i), u2(i))
        ! --------------------------------------------------------------
        ! maj RK step
        h(i) = (1d0/3d0)*h0(i) + (2d0/3d0)*h2(i)
        q(i) = (1d0/3d0)*q0(i) + (2d0/3d0)*q2(i)
        call update_u(h(i), q(i), u(i))
        if (ntrac>0) then
          do k=1,ntrac
            ht(i,k) = (1d0/3d0)*ht0(i,k) + (2d0/3d0)*ht2(i,k)
            call update_t(h(i), ht(i,k), t(i,k))
          enddo
        endif
      enddo
      ! ----------------------------------------------------------------
      end subroutine  
      !*****************************************************************





      ! ****************************************************************
      ! ****************************************************************
      ! ****************************************************************
      ! ****************************************************************
      !                   SAINT-VENANT-EXNER UPDATE
      ! ****************************************************************
      ! ****************************************************************
      ! ****************************************************************
      ! ****************************************************************





      !*****************************************************************
      subroutine update_sve_euler
      ! ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
      ! Update step - Euler for the SWE system
      ! ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
      use module_data, only: Rh, Lx, zf, zb, h, u, q, t, ht, fric, &
     &                       ntrac, ordre, ordre_trac, &
     &                       fluh_cell, fluh_src, &
     &                       fluq_cell, fluq_src, &
     &                       flut_cell, flut_src, &
     &                       fluqsed_cell, fluqsed_src
      use module_timestep,   only: init_time_step, adjust_time_step
      use module_high_order, only: compute_reconstructions
      use module_fluxes_sv,  only: init_fluxes
      use module_fluxes_sve, only: init_fluxes_bedload, &
     &                             compute_flused_limiter, &
     &                             compute_flux_swe
      use module_bedload,    only: compute_bedload_solidfluxes
      use module_bc,         only: compute_cdl
      use module_sources,    only: source_friction
      implicit none
      real(kind=dp)               :: fluqsed_lim(1:nx)
      real(kind=dp)               :: fact
      integer                     :: i, k
      ! ----------------------------------------------------------------
      call init_time_step(h, u)
      call init_fluxes
      call init_fluxes_bedload()
      call compute_bedload_solidfluxes(zf, zb, h, u)
      if ((ordre.ge.2).or.(ordre_trac.ge.2)) then
        call compute_reconstructions(zb, h, u, q, t)
      endif
      call compute_flux_swe(zb, h, u, t)
      call compute_cdl
      call adjust_time_step
      call compute_flused_limiter(zf, zb, fluqsed_lim)
      ! ----------------------------------------------------------------
      do i=1,nx
        fact = dt/ci(i)
        ! update of h, q
        h(i)  = h(i)  - fact*( fluh_cell(i) + fluh_src(i) )
        q(i)  = q(i)  - fact*( fluq_cell(i) + fluq_src(i) )
        ! update of zb
        zb(i) = zb(i) - fact*(min(fluqsed_cell(i) + fluqsed_src(i), &
     &                            fluqsed_lim(i)))
        ! update of ht
        if (ntrac>0) then
          do k=1,ntrac
            ht(i,k) = ht(i,k) - fact*( flut_cell(i,k) + flut_src(i,k) )
          enddo
        endif
        ! sources
        call update_Rh(h(i), Lx(i), Rh(i))
        call source_friction(h(i), Rh(i), q(i), fric(i))
        ! update u
        call update_u(h(i), q(i), u(i))
      enddo
      ! ----------------------------------------------------------------
      end subroutine  
      !*****************************************************************




      !*****************************************************************
      subroutine update_sve_heun
      ! ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
      ! Update step - Heun for the SWE system
      ! ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
      use module_data, only: Rh, Lx, zf, zb, h, u, q, t, ht, fric, &
     &                       ntrac, ordre, ordre_trac, &
     &                       fluh_cell, fluh_src, &
     &                       fluq_cell, fluq_src, &
     &                       flut_cell, flut_src, &
     &                       fluqsed_cell, fluqsed_src
      use module_timestep,   only: init_time_step, adjust_time_step
      use module_high_order, only: compute_reconstructions
      use module_fluxes_sv,  only: init_fluxes
      use module_fluxes_sve, only: init_fluxes_bedload, &
     &                             compute_flused_limiter, &
     &                             compute_flux_swe
      use module_bedload,    only: compute_bedload_solidfluxes
      use module_bc,         only: compute_cdl
      use module_sources,    only: source_friction
      implicit none
      real(kind=dp)               :: h0   (0:nx+1)
      real(kind=dp)               :: q0   (0:nx+1)
      real(kind=dp)               :: zb0  (0:nx+1)
      real(kind=dp)               :: ht0  (0:nx+1,ntrac)
      real(kind=dp)               :: fluqsed_lim(1:nx)
      real(kind=dp)               :: dt1, dt2, aux1, aux2, gamma
      real(kind=dp)               :: fact
      integer                     :: iord, mord
      integer                     :: i, k
      ! ----------------------------------------------------------------
      ! time order
      !   if mord==1: Euler scheme (1st order)
      !   if mord==2: Heun scheme (second order)
      mord = 2
      ! ----------------------------------------------------------------
      if (mord==2) then
        do i=0,nx+1
          h0(i)  = h(i)
          q0(i)  = q(i)
          zb0(i) = zb(i)
          if (ntrac>0) then
            do k=1,ntrac
              ht0(i,k) = ht(i,k)
            enddo
          endif
        enddo
      endif
      ! ----------------------------------------------------------------
      ! Heun iterations (iord=1: first, iord=2: second)
      ! ----------------------------------------------------------------
      do iord=1,mord
        call init_time_step(h, u)
        call init_fluxes
        call init_fluxes_bedload()
        call compute_bedload_solidfluxes(zf, zb, h, u)
        if ((ordre.ge.2).or.(ordre_trac.ge.2)) then
          call compute_reconstructions(zb, h, u, q, t)
        endif
        call compute_flux_swe(zb, h, u, t)
        call compute_cdl
        call adjust_time_step
        if (iord==1) then
          dt1 = dt
        endif
        call compute_flused_limiter(zf, zb, fluqsed_lim)
        ! ----------------------------------------------------------------
        do i=1,nx
          fact = dt/ci(i)
          ! update of h, q
          h(i)  = h(i)  - fact*( fluh_cell(i) + fluh_src(i) )
          q(i)  = q(i)  - fact*( fluq_cell(i) + fluq_src(i) )
          ! update of zb
          zb(i) = zb(i) - fact*(min(fluqsed_cell(i) + fluqsed_src(i), &
     &                              fluqsed_lim(i)))
          ! update of ht
          if (ntrac>0) then
            do k=1,ntrac
              ht(i,k) = ht(i,k) - fact*( flut_cell(i,k) + flut_src(i,k) )
            enddo
          endif
          ! sources
          call update_Rh(h(i), Lx(i), Rh(i))
          call source_friction(h(i), Rh(i), q(i), fric(i))
          ! update u
          call update_u(h(i), q(i), u(i))
        enddo
      enddo
      ! ----------------------------------------------------------------
      ! Heun update:
      ! ----------------------------------------------------------------
      if (mord==2) then
        dt2=dt
        aux1 = 2d0*dt1*dt2
        aux2 = dt1+dt2
        dt = aux1/aux2
        gamma = aux1/aux2**2
        do i=1,nx
          ! update of h, q
          h(i)  = (1d0-gamma)*h0(i)  + gamma*h(i)
          q(i)  = (1d0-gamma)*q0(i)  + gamma*q(i)
          zb(i) = (1d0-gamma)*zb0(i) + gamma*zb(i)
          ! update of ht
          if (ntrac>0) then
            do k=1,ntrac
              ht(i,k) = (1d0-gamma)*ht0(i,k) + gamma*ht(i,k)
              call update_t(h(i), ht(i,k), t(i,k))
            enddo
          endif
          ! update u
          call update_u(h(i), q(i), u(i))
        enddo
      endif
      ! ----------------------------------------------------------------
      end subroutine  
      !*****************************************************************




      end module
      ! ****************************************************************
