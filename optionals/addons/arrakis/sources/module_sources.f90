


      ! ****************************************************************
      module module_sources
      ! ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
      ! Sources module (friction)
      ! ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
      use module_data, only: dp, nx, g
      implicit none
      ! ----------------------------------------------------------------



      contains



      ! ****************************************************************
      ! ****************************************************************
      ! ****************************************************************
      ! ****************************************************************
      !                          SOURCES HYDRO
      ! ****************************************************************
      ! ****************************************************************
      ! ****************************************************************
      ! ****************************************************************




      !*****************************************************************
      subroutine source_friction(h, Rh, q, fc)
      ! ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
      ! time splitting scheme (i.e. friction solved after advection)
      ! time schemes for friction: explicit / semi-implicit / implicit
      ! ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
      use module_data, only: frict_law, frict_scheme, dt, &
     &                       eps, eps_num
      implicit none
      real(kind=dp), intent(in)        :: h
      real(kind=dp), intent(in)        :: Rh
      real(kind=dp), intent(in)        :: fc
      real(kind=dp), intent(inout)     :: q
      real(kind=dp)                    :: af
      ! ----------------------------------------------------------------
      ! no friction if dry cell or near zero hydraulic radius
      if (q.ne.0 .and. h.ge.eps .and. Rh.ge.eps_num) then
        ! --------------------------------------------------------------
        if(frict_law.eq.0) then
          af = 0.d0
        ! Fanning :
        elseif(frict_law.eq.1) then
          af = dt*fc/(h*abs(q))
        ! Chezy :
        elseif (frict_law.eq.2) then
          af = dt*g/((fc**2.d0)*h*Rh)
        ! Strickler :
        elseif (frict_law.eq.3) then
          af = dt*g/((fc**2.d0)*h*(Rh**(4.d0/3.d0)))
        else
          write(*,*) '++++++++++++++++++++++++++++++++++++++++++++'
          write(*,*) ' Unknown BOTTOM FRICTION LAW                '
          write(*,*) ' Stop.'
          write(*,*) '++++++++++++++++++++++++++++++++++++++++++++'
          stop
        endif
        ! --------------------------------------------------------------
        if ((frict_law.ne.0).and.(fc.gt.0)) then
          ! Explicit :
          if(frict_scheme.eq.1) then
            q = q*(1 - af*abs(q))
          ! Semi-implicit :
          elseif(frict_scheme.eq.2) then
            q = q/(1.d0 + af*abs(q))
          ! Implicit :
          elseif(frict_scheme.eq.3) then
            if(q.gt.0) then
              q = (-1.d0 + dsqrt(1.d0 + 4.d0*af*q))/(2.d0*af)
            else
              q = ( 1.d0 - dsqrt(1.d0 - 4.d0*af*q))/(2.d0*af)
            endif
          else
            write(*,*) '++++++++++++++++++++++++++++++++++++++++++++'
            write(*,*) ' Unknown BOTTOM FRICTION SCHEME             '
            write(*,*) ' Stop.'
            write(*,*) '++++++++++++++++++++++++++++++++++++++++++++'
            stop
          endif
        endif 
        ! --------------------------------------------------------------
      endif ! q!=0, h>0, Rh>0
      ! ----------------------------------------------------------------
      return
      end subroutine
      !*****************************************************************




      !*****************************************************************
      subroutine source_width_sv(i, h, q)
      ! ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
      ! Source term for rectangular variable cross section
      ! S = -(q, q**2/h)*(L'/L)
      ! WARNING: width source move to module_flux
      ! ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
      use module_data, only: variable_width, width_scheme, Lx, dx, dt, &
     &                       eps
      implicit none
      integer, intent(in)               :: i
      real(kind=dp), intent(inout)      :: h
      real(kind=dp), intent(inout)      :: q
      real(kind=dp)                     :: coef_src_h
      real(kind=dp)                     :: coef_src_q
      real(kind=dp)                     :: dL_im12, dL_ip12, dLi, aux
      ! ----------------------------------------------------------------
      if (variable_width.eqv..True.) then
        ! no source term if dry cell
        if(q.ne.0 .and. h.ge.eps) then
          ! ***************
          ! Centered scheme
          ! ***************
          if(width_scheme.eq.1) then
            ! width variation at interfaces i-1/2 and i+1/2
            dL_im12 = (Lx(i) - Lx(i-1))/dx(i-1)
            dL_ip12 = (Lx(i+1) - Lx(i))/dx(i)
            ! centered scheme
            dLi = 0.5d0*(dL_im12 + dL_ip12)
            aux = dLi/Lx(i)
          else
            write(*,*) '++++++++++++++++++++++++++++++++++++++++++++'
            write(*,*) ' Unknown VARIABLE WIDTH SCHEME '
            write(*,*) ' Stop.'
            write(*,*) '++++++++++++++++++++++++++++++++++++++++++++'
            stop
          endif
          ! update
          coef_src_h = -q
          coef_src_q = -q**2/max(eps, h)
          h = h + dt*coef_src_h*aux
          q = q + dt*coef_src_q*aux
        endif
      endif
      ! ----------------------------------------------------------------
      return
      end subroutine
      !*****************************************************************




      !*****************************************************************
      subroutine source_width_e(i, zb)
      ! ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
      ! Source term for rectangular variable cross section
      ! S = -(porosite*qs)*(L'/L)
      ! ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
      use module_data, only: variable_width, width_scheme, Lx, dx, dt, &
     &                       eps, porosite, qsed
      implicit none
      integer, intent(in)               :: i
      real(kind=dp), intent(inout)      :: zb
      real(kind=dp)                     :: coef_src_b
      real(kind=dp)                     :: dL_im12, dL_ip12, dLi, aux
      ! ----------------------------------------------------------------
      if (variable_width.eqv..True.) then
        if(qsed(i).ne.0) then
          ! ***************
          ! Centered scheme
          ! ***************
          if(width_scheme.eq.1) then
            ! width variation at interfaces i-1/2 and i+1/2
            dL_im12 = (Lx(i) - Lx(i-1))/dx(i-1)
            dL_ip12 = (Lx(i+1) - Lx(i))/dx(i)
            ! centered scheme
            dLi = 0.5d0*(dL_im12 + dL_ip12)
            aux = dLi/Lx(i)
          else
            write(*,*) '++++++++++++++++++++++++++++++++++++++++++++'
            write(*,*) ' Unknown VARIABLE WIDTH SCHEME '
            write(*,*) ' Stop.'
            write(*,*) '++++++++++++++++++++++++++++++++++++++++++++'
            stop
          endif
          ! update
          coef_src_b = -porosite*qsed(i)
          zb = zb + dt*coef_src_b*aux
        endif
      endif
      ! ----------------------------------------------------------------
      return
      end subroutine
      !*****************************************************************




      ! ****************************************************************
      ! ****************************************************************
      ! ****************************************************************
      ! ****************************************************************
      !                         TRACER SOURCES
      ! ****************************************************************
      ! ****************************************************************
      ! ****************************************************************
      ! ****************************************************************




!      !*****************************************************************
!      subroutine source_tracers(h, ht)
!      ! ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
!      use module_data, only: dt
!      implicit none
!      real(kind=dp),intent(in)         :: h
!      real(kind=dp),intent(inout)      :: ht
!      integer                          :: k
!      ! ----------------------------------------------------------------
!      ! TODO
!      ! ----------------------------------------------------------------
!      return
!      end subroutine
!      !*****************************************************************




      end module
      ! ****************************************************************
