


      ! ****************************************************************
      module module_io
      ! ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
      ! Input / Output module
      ! ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
      use module_data, only: dp
      implicit none
      ! ----------------------------------------------------------------



      contains




      ! ****************************************************************
      ! ****************************************************************
      ! ****************************************************************
      ! ****************************************************************
      !                    INPUT PARAMETERS READER
      ! ****************************************************************
      ! ****************************************************************
      ! ****************************************************************
      ! ****************************************************************




      ! ****************************************************************
      subroutine read_inputs()
      ! ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
      ! Read input file
      ! ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
      use module_data
      implicit none
      character*201              :: line
      logical                    :: existence
      character*20               :: version
      character(len=255)         :: cwd
      character(len=255)         :: tmp
      ! ----------------------------------------------------------------
      ! READ MAIN INPUT FILE
      ! ----------------------------------------------------------------
      call getcwd(cwd)
      ficdon = trim(cwd)//'/run.yml'
      inquire(file=ficdon,exist=existence)
      if(.not. existence) then
        write(*,*) ' ' 
        write(*,*) '+++++++++++++++++++++++++++++++++++++++++++++'  
        write(*,*) ' WARNING : reading error' 
        write(*,*) '+++++++++++++++++++++++++++++++++++++++++++++'  
        write(*,*) '+ input data not available'
        write(*,*) '+++++++++++++++++++++++++++++++++++++++++++++' 
        stop
      endif
      ! ----------------------------------------------------------------
      open(unit=65,file=ficdon,status='unknown')
        read(65,'(a)') line
        read(65,'(a)') line
        read(65,'(a)') line
        read(65,'(a)') line
        if(index(line,'<ARRAKIS>')==0)then
          write(*,*) '+++++++++++++++++++++++++++++++++++++++++++++'
          write(*,*) ' WARNING :incompatible data file'
          write(*,*) '+++++++++++++++++++++++++++++++++++++++++++++'
          stop
        endif
        if(index(line,'$'   )==0) version = 'non'
        if(index(line,'$1.0')/=0) version = '1.0'
        if(index(line,'$1.1')/=0) version = '1.1'
        if(index(line,'$1.2')/=0) version = '1.2'
        if(index(line,'$1.3')/=0) version = '1.3'
        if(index(line,'$1.4')/=0) version = '1.4'
        if(index(line,'$2.0')/=0) version = '2.0'
      close(65)
      select case(trim(adjustl(version)))
        case('2.0')
          write(*,*) '°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°'
          write(*,*) ' Input data version : ', trim(adjustl(version))
          write(*,*) '°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°'
          write(*,*) ''
          call read_data()
        case default
          write(*,*) ' ' 
          write(*,*) '+++++++++++++++++++++++++++++++++++++++++++++'  
          write(*,*) ' WARNING : reading error' 
          write(*,*) '+++++++++++++++++++++++++++++++++++++++++++++'  
          write(*,*) '+ Unknown or unsupported version...'
          write(*,*) '+ Check your input data file (.yml)'
          write(*,*) '+++++++++++++++++++++++++++++++++++++++++++++' 
          stop             
      end select
      ! ----------------------------------------------------------------
      ! global path to files
      tmp = trim(cwd)//'/RESU/'//trim(res_file)
      res_file = tmp
      tmp = trim(cwd)//'/'//trim(ini_file)
      ini_file = tmp
      tmp = trim(cwd)//'/RESU/'//trim(pbr_file)
      pbr_file = tmp
      ! ----------------------------------------------------------------
      ! READ BOUNDARY CONDITION FILES      
      ! ----------------------------------------------------------------
      call read_bc
      ! ----------------------------------------------------------------
      return
      end subroutine
      ! ****************************************************************




      ! ****************************************************************
      subroutine read_data()
      ! ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
      ! Read input file
      ! ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
      use module_data
      implicit none
      character*201              :: line
      integer                    :: i, err
      character*200              :: s
      character(len=255)         :: var_line
      ! ----------------------------------------------------------------
      open(unit=65,file=ficdon,status='unknown')
        s='debut'
        read(65,'(a)',iostat=err) line ; if(err/=0) call lstop(s,line)
        read(65,'(a)',iostat=err) line ; if(err/=0) call lstop(s,line)
        read(65,'(a)',iostat=err) line ; if(err/=0) call lstop(s,line)
        read(65,'(a)',iostat=err) line ; if(err/=0) call lstop(s,line)
        read(65,'(a)',iostat=err) line ; if(err/=0) call lstop(s,line)
        read(65,'(a)',iostat=err) line ; if(err/=0) call lstop(s,line)
        read(65,'(a)',iostat=err) line ; if(err/=0) call lstop(s,line)         
        ! --------------------------------------------------------------
        s='Managment of computation'
        read(65,'(a)',iostat=err) line ; if(err/=0) call lstop(s,line)
        read(65,'(a)',iostat=err) line ; if(err/=0) call lstop(s,line)
        read(65,'(a)',iostat=err) line ; if(err/=0) call lstop(s,line)
        ! --------------------------------------------------------------
        ! Managment of computation
        ! --------------------------------------------------------------
        call tab55(line); read(65,*,iostat=err) nitermax ; if(err/=0) call lstop(s,line)
        call tab55(line); read(65,*,iostat=err) time_max ; if(err/=0) call lstop(s,line)
        call tab55(line); read(65,*,iostat=err) dt_const ; if(err/=0) call lstop(s,line)
        call tab55(line); read(65,*,iostat=err) var_dt   ; if(err/=0) call lstop(s,line)
        call tab55(line); read(65,*,iostat=err) cfl      ; if(err/=0) call lstop(s,line)
        ! --------------------------------------------------------------

        ! --------------------------------------------------------------
        s='Managment of outputs'
        read(65,'(a)',iostat=err) line ; if(err/=0) call lstop(s,line)
        read(65,'(a)',iostat=err) line ; if(err/=0) call lstop(s,line)
        read(65,'(a)',iostat=err) line ; if(err/=0) call lstop(s,line)
        read(65,'(a)',iostat=err) line ; if(err/=0) call lstop(s,line)
        read(65,'(a)',iostat=err) line ; if(err/=0) call lstop(s,line)
        ! --------------------------------------------------------------
        ! Managment of outputs
        ! --------------------------------------------------------------
        do i=1,size(vars_outputs)
          vars_outputs(i) = ''
        enddo
        call tab55(line); read(65,*,iostat=err) res_file    ; if(err/=0) call lstop(s,line)
        call tab55(line); read(65,*,iostat=err) ndisplay    ; if(err/=0) call lstop(s,line)
        call tab55(line); read(65,*,iostat=err) nrecord     ; if(err/=0) call lstop(s,line)
        call tab55(line); read(65,'(A)',iostat=err) var_line; if(err/=0) call lstop(s,line)
        call split_string(var_line, vars_outputs)
        call count_vars(vars_outputs, nvars_outputs)
        ! Probes
        do i=1,size(vars_outputs)
          vars_probes(i) = ''
        enddo
        call tab55(line); read(65,*,iostat=err) probes      ; if(err/=0) call lstop(s,line)
        allocate(probes_x (probes))
        call tab55(line); read(65,*,iostat=err) probes_x(1:probes) &
                                              & ; if(err/=0) call lstop(s,line)
        call tab55(line); read(65,'(A)',iostat=err) var_line; if(err/=0) call lstop(s,line)
        call split_string(var_line, vars_probes)
        call count_vars(vars_probes, nvars_probes)
        call tab55(line); read(65,*,iostat=err) pbr_file    ; if(err/=0) call lstop(s,line)
        ! Snapshots
        call tab55(line); read(65,*,iostat=err) snaps       ; if(err/=0) call lstop(s,line)
        allocate(snaps_t (snaps))
        call tab55(line); read(65,*,iostat=err) snaps_t(1:snaps) &
                                              & ; if(err/=0) call lstop(s,line)
        snaps_i = 1
        ! --------------------------------------------------------------

        ! --------------------------------------------------------------
        s='Mesh'
        read(65,'(a)',iostat=err) line ; if(err/=0) call lstop(s,line)
        read(65,'(a)',iostat=err) line ; if(err/=0) call lstop(s,line)
        read(65,'(a)',iostat=err) line ; if(err/=0) call lstop(s,line)
        read(65,'(a)',iostat=err) line ; if(err/=0) call lstop(s,line)
        read(65,'(a)',iostat=err) line ; if(err/=0) call lstop(s,line)
        ! --------------------------------------------------------------
        ! Mesh
        ! --------------------------------------------------------------
        call tab55(line); read(65,*,iostat=err) nx ; if(err/=0) call lstop(s,line)
        call tab55(line); read(65,*,iostat=err) xa ; if(err/=0) call lstop(s,line)
        call tab55(line); read(65,*,iostat=err) xb ; if(err/=0) call lstop(s,line)
        call tab55(line); read(65,*,iostat=err) use_mesh_file ; if(err/=0) call lstop(s,line)
        call tab55(line); read(65,*,iostat=err) mesh_file     ; if(err/=0) call lstop(s,line)
        call tab55(line); read(65,*,iostat=err) variable_width; if(err/=0) call lstop(s,line)
        ! --------------------------------------------------------------

        ! --------------------------------------------------------------
        s='Physical parameters'
        read(65,'(a)',iostat=err) line ; if(err/=0) call lstop(s,line)
        read(65,'(a)',iostat=err) line ; if(err/=0) call lstop(s,line)
        read(65,'(a)',iostat=err) line ; if(err/=0) call lstop(s,line)
        read(65,'(a)',iostat=err) line ; if(err/=0) call lstop(s,line)
        read(65,'(a)',iostat=err) line ; if(err/=0) call lstop(s,line)
        ! --------------------------------------------------------------
        !  Physical parameters
        ! --------------------------------------------------------------
        call tab55(line); read(65,*,iostat=err) frict_law    ; if(err/=0) call lstop(s,line)
        call tab55(line); read(65,*,iostat=err) frict_coef   ; if(err/=0) call lstop(s,line)
        call tab55(line); read(65,*,iostat=err) diffusion_u  ; if(err/=0) call lstop(s,line)
        call tab55(line); read(65,*,iostat=err) nu_u         ; if(err/=0) call lstop(s,line)
        ! --------------------------------------------------------------

        ! --------------------------------------------------------------
        s='Numerical parameters'
        read(65,'(a)',iostat=err) line ; if(err/=0) call lstop(s,line)
        read(65,'(a)',iostat=err) line ; if(err/=0) call lstop(s,line)
        read(65,'(a)',iostat=err) line ; if(err/=0) call lstop(s,line)
        read(65,'(a)',iostat=err) line ; if(err/=0) call lstop(s,line)
        read(65,'(a)',iostat=err) line ; if(err/=0) call lstop(s,line)
        ! --------------------------------------------------------------
        !  Numerical parameters
        ! --------------------------------------------------------------
        call tab55(line); read(65,*,iostat=err) method      ; if(err/=0) call lstop(s,line)
        call tab55(line); read(65,*,iostat=err) time_scheme ; if(err/=0) call lstop(s,line)
        call tab55(line); read(65,*,iostat=err) ordre       ; if(err/=0) call lstop(s,line)
        call tab55(line); read(65,*,iostat=err) ordre_trac  ; if(err/=0) call lstop(s,line)
        ! --------------------------------------------------------------

        ! --------------------------------------------------------------
        s='Initial condition'
        read(65,'(a)',iostat=err) line ; if(err/=0) call lstop(s,line)
        read(65,'(a)',iostat=err) line ; if(err/=0) call lstop(s,line)
        read(65,'(a)',iostat=err) line ; if(err/=0) call lstop(s,line)
        read(65,'(a)',iostat=err) line ; if(err/=0) call lstop(s,line)
        read(65,'(a)',iostat=err) line ; if(err/=0) call lstop(s,line)
        ! --------------------------------------------------------------
        ! Initial condition
        ! --------------------------------------------------------------
        call tab55(line); read(65,*,iostat=err) use_ini_file ; if(err/=0) call lstop(s,line)
        call tab55(line); read(65,*,iostat=err) ini_file     ; if(err/=0) call lstop(s,line)
        call tab55(line); read(65,*,iostat=err) type_init    ; if(err/=0) call lstop(s,line)
        call tab55(line); read(65,*,iostat=err) const_h      ; if(err/=0) call lstop(s,line)
        call tab55(line); read(65,*,iostat=err) const_e      ; if(err/=0) call lstop(s,line)
        call tab55(line); read(65,*,iostat=err) const_q      ; if(err/=0) call lstop(s,line)
        ! --------------------------------------------------------------

        ! --------------------------------------------------------------
        s='Boundary conditions'
        read(65,'(a)',iostat=err) line ; if(err/=0) call lstop(s,line)
        read(65,'(a)',iostat=err) line ; if(err/=0) call lstop(s,line)
        read(65,'(a)',iostat=err) line ; if(err/=0) call lstop(s,line)
        read(65,'(a)',iostat=err) line ; if(err/=0) call lstop(s,line)
        read(65,'(a)',iostat=err) line ; if(err/=0) call lstop(s,line)
        ! --------------------------------------------------------------
        !  Boundary conditions
        ! --------------------------------------------------------------
        call tab55(line); read(65,*,iostat=err) clqoru    ; if(err/=0) call lstop(s,line)
        call tab55(line); read(65,*,iostat=err) bctype_l  ; if(err/=0) call lstop(s,line)
        call tab55(line); read(65,*,iostat=err) ha        ; if(err/=0) call lstop(s,line)
        call tab55(line); read(65,*,iostat=err) ua        ; if(err/=0) call lstop(s,line)
        call tab55(line); read(65,*,iostat=err) qa        ; if(err/=0) call lstop(s,line)
        call tab55(line); read(65,*,iostat=err) use_bcfile_l ; if(err/=0) call lstop(s,line)
        call tab55(line); read(65,*,iostat=err) bcfile_l  ; if(err/=0) call lstop(s,line)
        call tab55(line); read(65,*,iostat=err) bctype_r  ; if(err/=0) call lstop(s,line)
        call tab55(line); read(65,*,iostat=err) hb        ; if(err/=0) call lstop(s,line)
        call tab55(line); read(65,*,iostat=err) ub        ; if(err/=0) call lstop(s,line)
        call tab55(line); read(65,*,iostat=err) qb        ; if(err/=0) call lstop(s,line)
        call tab55(line); read(65,*,iostat=err) use_bcfile_r ; if(err/=0) call lstop(s,line)
        call tab55(line); read(65,*,iostat=err) bcfile_r  ; if(err/=0) call lstop(s,line)
        ! --------------------------------------------------------------

        ! --------------------------------------------------------------
        s='Tracers'
        read(65,'(a)',iostat=err) line ; if(err/=0) call lstop(s,line)
        read(65,'(a)',iostat=err) line ; if(err/=0) call lstop(s,line)
        read(65,'(a)',iostat=err) line ; if(err/=0) call lstop(s,line)
        read(65,'(a)',iostat=err) line ; if(err/=0) call lstop(s,line)
        read(65,'(a)',iostat=err) line ; if(err/=0) call lstop(s,line)
        ! --------------------------------------------------------------
        !  Tracers
        ! --------------------------------------------------------------
        call tab55(line); read(65,*,iostat=err) ntrac     ; if(err/=0) call lstop(s,line)
        allocate(const_t    (ntrac))
        allocate(bctypet_l  (ntrac))
        allocate(bctypet_r  (ntrac))
        allocate(ta         (ntrac))
        allocate(tb         (ntrac))
        allocate(nu_t       (ntrac))
        call tab55(line); read(65,*,iostat=err) const_t(1:ntrac)  ; if(err/=0) call lstop(s,line)
        call tab55(line); read(65,*,iostat=err) bctypet_l(1:ntrac); if(err/=0) call lstop(s,line)
        call tab55(line); read(65,*,iostat=err) ta(1:ntrac)       ; if(err/=0) call lstop(s,line)
        call tab55(line); read(65,*,iostat=err) bctypet_r(1:ntrac); if(err/=0) call lstop(s,line)
        call tab55(line); read(65,*,iostat=err) tb(1:ntrac)       ; if(err/=0) call lstop(s,line)
        call tab55(line); read(65,*,iostat=err) diffusion_t       ; if(err/=0) call lstop(s,line)
        call tab55(line); read(65,*,iostat=err) nu_t(1:ntrac)     ; if(err/=0) call lstop(s,line)

        
        ! --------------------------------------------------------------
        s='Bedload parameters'
        read(65,'(a)',iostat=err) line ; if(err/=0) call lstop(s,line)
        read(65,'(a)',iostat=err) line ; if(err/=0) call lstop(s,line)
        read(65,'(a)',iostat=err) line ; if(err/=0) call lstop(s,line)
        read(65,'(a)',iostat=err) line ; if(err/=0) call lstop(s,line)
        read(65,'(a)',iostat=err) line ; if(err/=0) call lstop(s,line)
        ! --------------------------------------------------------------
        ! Bedload
        ! --------------------------------------------------------------
        call tab55(line); read(65,*,iostat=err) bedload            ; if(err/=0) call lstop(s,line)
        ! boundary conditions
        call tab55(line); read(65,*,iostat=err) bctypeqs_l         ; if(err/=0) call lstop(s,line)
        call tab55(line); read(65,*,iostat=err) qsa                ; if(err/=0) call lstop(s,line)        
        call tab55(line); read(65,*,iostat=err) Jeqa               ; if(err/=0) call lstop(s,line)
        call tab55(line); read(65,*,iostat=err) bctypeqs_r         ; if(err/=0) call lstop(s,line)
        call tab55(line); read(65,*,iostat=err) qsb                ; if(err/=0) call lstop(s,line)
        call tab55(line); read(65,*,iostat=err) Jeqb               ; if(err/=0) call lstop(s,line)
        ! bedload formulas
        call tab55(line); read(65,*,iostat=err) bedload_formula    ; if(err/=0) call lstop(s,line)
        call tab55(line); read(65,*,iostat=err) bedload_coef       ; if(err/=0) call lstop(s,line)
        call tab55(line); read(65,*,iostat=err) porosite           ; if(err/=0) call lstop(s,line)
        call tab55(line); read(65,*,iostat=err) rhos               ; if(err/=0) call lstop(s,line)
        call tab55(line); read(65,*,iostat=err) d50                ; if(err/=0) call lstop(s,line)
        call tab55(line); read(65,*,iostat=err) d16                ; if(err/=0) call lstop(s,line)
        call tab55(line); read(65,*,iostat=err) dm                 ; if(err/=0) call lstop(s,line)
        call tab55(line); read(65,*,iostat=err) d84                ; if(err/=0) call lstop(s,line)
        call tab55(line); read(65,*,iostat=err) Ag                 ; if(err/=0) call lstop(s,line)
        call tab55(line); read(65,*,iostat=err) mg                 ; if(err/=0) call lstop(s,line)
        call tab55(line); read(65,*,iostat=err) Kp                 ; if(err/=0) call lstop(s,line)
        call tab55(line); read(65,*,iostat=err) tau_c              ; if(err/=0) call lstop(s,line)
        call tab55(line); read(65,*,iostat=err) smpm_a             ; if(err/=0) call lstop(s,line)
        call tab55(line); read(65,*,iostat=err) smpm_b             ; if(err/=0) call lstop(s,line)
        
        ! --------------------------------------------------------------
        s='Advanced numerical parameters'
        read(65,'(a)',iostat=err) line ; if(err/=0) call lstop(s,line)
        read(65,'(a)',iostat=err) line ; if(err/=0) call lstop(s,line)
        read(65,'(a)',iostat=err) line ; if(err/=0) call lstop(s,line)
        read(65,'(a)',iostat=err) line ; if(err/=0) call lstop(s,line)
        read(65,'(a)',iostat=err) line ; if(err/=0) call lstop(s,line)
        ! --------------------------------------------------------------
        !  Advanced numerical parameters
        ! --------------------------------------------------------------
        call tab55(line); read(65,*,iostat=err) advanced_params    ; if(err/=0) call lstop(s,line)
        call tab55(line); read(65,*,iostat=err) rec_method         ; if(err/=0) call lstop(s,line)
        call tab55(line); read(65,*,iostat=err) muscl_ilim_h       ; if(err/=0) call lstop(s,line)
        call tab55(line); read(65,*,iostat=err) muscl_ilim_u       ; if(err/=0) call lstop(s,line)
        call tab55(line); read(65,*,iostat=err) muscl_ilim_t       ; if(err/=0) call lstop(s,line)
        call tab55(line); read(65,*,iostat=err) hydrostarec        ; if(err/=0) call lstop(s,line)
        call tab55(line); read(65,*,iostat=err) frict_scheme       ; if(err/=0) call lstop(s,line)
        call tab55(line); read(65,*,iostat=err) width_scheme       ; if(err/=0) call lstop(s,line)
        call tab55(line); read(65,*,iostat=err) swe_roots_approx   ; if(err/=0) call lstop(s,line)
        call tab55(line); read(65,*,iostat=err) qs_derivatives     ; if(err/=0) call lstop(s,line)
        call tab55(line); read(65,*,iostat=err) qs_lim             ; if(err/=0) call lstop(s,line)
        call tab55(line); read(65,*,iostat=err) eps_b_damping      ; if(err/=0) call lstop(s,line)
        call tab55(line); read(65,*,iostat=err) eps_b              ; if(err/=0) call lstop(s,line)
        ! --------------------------------------------------------------
      close(65)
      ! ----------------------------------------------------------------
      return 
      end subroutine
      ! ****************************************************************




      ! ****************************************************************
      subroutine check_inputs()
      ! ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
      ! - Checks values of inputs parameters if specified by user
      ! - Set default values of inputs parameters if not specified
      ! ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
      use module_data
      implicit none
      logical                    :: stop_trigger=.FALSE.
      integer                    :: k
      ! ----------------------------------------------------------------
      ! *********************
      ! Check time parameters
      ! *********************
      ! DURATION
      if(time_max.le.0d0)then
        write(*,*) '++++++++++++++++++++++++++++++++++++++++++++'
        write(*,*) ' ERROR : "DURATION" SHOULD BE > 0.          '
        write(*,*) '++++++++++++++++++++++++++++++++++++++++++++'
        stop_trigger = .TRUE.
      endif
      ! MAXIMAL NUMBER OF ITERATIONS
      if(nitermax.le.0)then
        write(*,*) '++++++++++++++++++++++++++++++++++++++++++++'
        write(*,*) ' ERROR : "MAXIMAL NUMBER OF ITERATIONS"     '
        write(*,*) '         SHOULD BE >= 1                     '
        write(*,*) '++++++++++++++++++++++++++++++++++++++++++++'
        stop_trigger = .TRUE.
      endif
      ! TIME STEP
      if(dt_const.le.0)then
        write(*,*) '++++++++++++++++++++++++++++++++++++++++++++'
        write(*,*) ' ERROR : "TIME STEP" SHOULD BE > 0          '
        write(*,*) '++++++++++++++++++++++++++++++++++++++++++++'
        stop_trigger = .TRUE.
      endif
      ! VARIABLE TIME STEP
      if(var_dt.eqv..False.)then
        write(*,*) '++++++++++++++++++++++++++++++++++++++++++++'
        write(*,*) ' WARNING : "VARIABLE TIME STEP" = False     ' 
        write(*,*) '           CFL CONDITION IS DISCARDED,      '
        write(*,*) '           YOU MAY ENCOUNTER INSTABILITIES  ' 
        write(*,*) '++++++++++++++++++++++++++++++++++++++++++++'
      endif
      ! CFL NUMBER
      if(cfl.gt.1)then
        write(*,*) '++++++++++++++++++++++++++++++++++++++++++++'
        write(*,*) ' WARNING : "CFL NUMBER" SHOULD BE < 1       '
        write(*,*) '++++++++++++++++++++++++++++++++++++++++++++'
      endif
      if(cfl.le.0)then
        write(*,*) '++++++++++++++++++++++++++++++++++++++++++++'
        write(*,*) ' ERROR : "CFL NUMBER" SHOULD BE > 0         '
        write(*,*) '++++++++++++++++++++++++++++++++++++++++++++'
        stop_trigger = .TRUE.
      endif
      ! ***********************
      ! Check output parameters
      ! ***********************
      ! RESULT FILE
      if(trim(res_file).eq.'')then
        write(*,*) '++++++++++++++++++++++++++++++++++++++++++++'
        write(*,*) ' ERROR : "RESULT FILE" NAME NOT VALID       '
        write(*,*) '++++++++++++++++++++++++++++++++++++++++++++'
        stop_trigger = .TRUE.
      endif
      ! LISTING FREQUENCY
      if(ndisplay.lt.0)then
        write(*,*) '++++++++++++++++++++++++++++++++++++++++++++'
        write(*,*) ' ERROR : "LISTING FREQUENCY" SHOULD BE >= 0 '
        write(*,*) '++++++++++++++++++++++++++++++++++++++++++++'
        stop_trigger = .TRUE.
      endif
      ! OUTPUT FREQUENCY
      if(nrecord.lt.0)then
        write(*,*) '++++++++++++++++++++++++++++++++++++++++++++'
        write(*,*) ' ERROR : "OUTPUT FREQUENCY" SHOULD BE >= 0  '
        write(*,*) '++++++++++++++++++++++++++++++++++++++++++++'
        stop_trigger = .TRUE.
      endif
      ! VARIABLES
      ! TODO add list of possible names and error if unrecognized
      ! PROBES
      if(probes.lt.0)then
        write(*,*) '++++++++++++++++++++++++++++++++++++++++++++'
        write(*,*) ' ERROR : "PROBES" SHOULD BE >= 0            '
        write(*,*) '++++++++++++++++++++++++++++++++++++++++++++'
        stop_trigger = .TRUE.
      endif
      ! PROBES COORDINATES
      do k=1,probes
        if((probes_x(k).lt.xa).or.(probes_x(k).gt.xb))then
          write(*,*) '++++++++++++++++++++++++++++++++++++++++++++'
          write(*,*) ' ERROR : "PROBES COORDINATES"               '
          write(*,*) '         SHOULD BE INSIDE DOMAIN (XA<.<XB)  '
          write(*,*) '++++++++++++++++++++++++++++++++++++++++++++'
          stop_trigger = .TRUE.
        endif
      enddo
      ! PROBES VARIABLES vars_probes
      ! TODO add list of possible names and error if unrecognized
      ! PROBES FILE NAME
      if(trim(pbr_file).eq.'')then
        write(*,*) '++++++++++++++++++++++++++++++++++++++++++++'
        write(*,*) ' ERROR : "PROBES FILE NAME" NOT VALID       '
        write(*,*) '++++++++++++++++++++++++++++++++++++++++++++'
        stop_trigger = .TRUE.
      endif
      ! SNAPSHOTS
      if(snaps.lt.0)then
        write(*,*) '++++++++++++++++++++++++++++++++++++++++++++'
        write(*,*) ' ERROR : "SNAPSHOTS" SHOULD BE >= 0         '
        write(*,*) '++++++++++++++++++++++++++++++++++++++++++++'
        stop_trigger = .TRUE.
      endif
      ! SNAPSHOTS TIMES
      do k=1,snaps
        if((snaps_t(k).lt.0.).or.(snaps_t(k).gt.time_max))then
          write(*,*) '++++++++++++++++++++++++++++++++++++++++++++'
          write(*,*) ' ERROR : "SNAPSHOTS TIMES"                  '
          write(*,*) '         SHOULD BE 0. < . < DURATION        '
          write(*,*) '++++++++++++++++++++++++++++++++++++++++++++'
          stop_trigger = .TRUE.
        endif
      enddo
      ! *********************
      ! Check mesh parameters
      ! *********************
      if(nx<=0 .or. xb<=xa)then
        write(*,*) '++++++++++++++++++++++++++++++++++++++++++++'
        write(*,*) ' ERROR : XB SHOULD BE > XA AND NX > 0       '
        write(*,*) '++++++++++++++++++++++++++++++++++++++++++++'
        stop_trigger = .TRUE.
      endif
      ! MESH FILE NAME
      if((use_mesh_file.eqv..True.).and.(trim(mesh_file).eq.''))then
        write(*,*) '++++++++++++++++++++++++++++++++++++++++++++'
        write(*,*) ' ERROR : "MESH FILE NAME" NOT VALID         '
        write(*,*) '++++++++++++++++++++++++++++++++++++++++++++'
        stop_trigger = .TRUE.
      endif
      ! *************************
      ! Check physical parameters
      ! *************************
      ! BOTTOM FRICTION LAW
      if((frict_law.lt.0).or.(frict_law.gt.3))then
        write(*,*) '++++++++++++++++++++++++++++++++++++++++++++'
        write(*,*) ' ERROR : "BOTTOM FRICTION LAW" SHOULD BE 0<=.<=3 '
        write(*,*) '++++++++++++++++++++++++++++++++++++++++++++'
        stop_trigger = .TRUE.
      endif
      ! BOTTOM FRICTION COEFFICIENT
      if((frict_coef.lt.0.))then
        write(*,*) '++++++++++++++++++++++++++++++++++++++++++++'
        write(*,*) ' ERROR : "BOTTOM FRICTION COEFFICIENT"      '
        write(*,*) '          SHOULD BE > 0.                    '
        write(*,*) '++++++++++++++++++++++++++++++++++++++++++++'
        stop_trigger = .TRUE.
      endif
      ! VELOCITY DIFFUSIVITY
      if(nu_u.lt.0)then
        write(*,*) '++++++++++++++++++++++++++++++++++++++++++++'
        write(*,*) ' ERROR : "VELOCITY DIFFUSIVITY"             '
        write(*,*) '         SHOULD BE >= 0.                    '
        write(*,*) '++++++++++++++++++++++++++++++++++++++++++++'
        stop_trigger = .TRUE.
      endif
      ! **************************
      ! Check numerical parameters
      ! **************************
      ! ADVECTION SCHEME
      if((method.lt.1).or.(method.gt.4))then
        write(*,*) '++++++++++++++++++++++++++++++++++++++++++++'
        write(*,*) ' ERROR : "ADVECTION SCHEME" SHOULD BE 1<=.<=4'
        write(*,*) '++++++++++++++++++++++++++++++++++++++++++++'
        stop_trigger = .TRUE.
      endif
      ! TIME SCHEME
      if((time_scheme.lt.1).or.(time_scheme.gt.4))then
        write(*,*) '++++++++++++++++++++++++++++++++++++++++++++'
        write(*,*) ' ERROR : "TIME SCHEME" SHOULD BE 1<=.<=4    '
        write(*,*) '++++++++++++++++++++++++++++++++++++++++++++'
        stop_trigger = .TRUE.
      endif
      if((time_scheme.eq.3).or.(time_scheme.eq.4)) then
        write(*,*) '++++++++++++++++++++++++++++++++++++++++++++'
        write(*,*) ' WARNING : TIME SCHEME = 3 or 4             '
        write(*,*) '           is experimental                  '
        write(*,*) '++++++++++++++++++++++++++++++++++++++++++++'
      endif
      ! SPACE ORDER
      if((ordre.lt.1).or.(ordre.eq.4).or.(ordre.gt.5))then
        write(*,*) '++++++++++++++++++++++++++++++++++++++++++++'
        write(*,*) ' ERROR : "SPACE ORDER" SHOULD BE 1,2,3 or 5'
        write(*,*) '++++++++++++++++++++++++++++++++++++++++++++'
        stop_trigger = .TRUE.
      endif
      if((ordre.eq.3)) then
        write(*,*) '++++++++++++++++++++++++++++++++++++++++++++'
        write(*,*) ' WARNING : SPACE ORDER = 3                  '
        write(*,*) '           is experimental                  '
        write(*,*) '++++++++++++++++++++++++++++++++++++++++++++'
      endif
      ! SPACE ORDER FOR TRACERS
      if((ordre_trac.lt.1).or.(ordre_trac.eq.4).or.(ordre_trac.gt.5))then
        write(*,*) '++++++++++++++++++++++++++++++++++++++++++++'
        write(*,*) ' ERROR : "SPACE ORDER FOR TRACERS"          '
        write(*,*) '         SHOULD BE 1,2,3 or 5               '
        write(*,*) '++++++++++++++++++++++++++++++++++++++++++++'
        stop_trigger = .TRUE.
      endif
      if((ordre_trac.eq.3)) then
        write(*,*) '++++++++++++++++++++++++++++++++++++++++++++'
        write(*,*) ' WARNING : SPACE ORDER FOR TRACERS = 3      '
        write(*,*) '           is experimental                  '
        write(*,*) '++++++++++++++++++++++++++++++++++++++++++++'
      endif
      ! ***********************
      ! Check initial condition
      ! ***********************
      ! INITIAL FILE NAME
      if((use_ini_file.eqv..True.).and.(trim(ini_file).eq.''))then
        write(*,*) '++++++++++++++++++++++++++++++++++++++++++++'
        write(*,*) ' ERROR : "INITIAL FILE NAME" NOT VALID      '
        write(*,*) '++++++++++++++++++++++++++++++++++++++++++++'
        stop_trigger = .TRUE.
      endif
      ! INITIAL CONDITION
      if((type_init.lt.0).or.(type_init.gt.2))then
        write(*,*) '++++++++++++++++++++++++++++++++++++++++++++'
        write(*,*) ' ERROR : "INITIAL CONDITION" SHOULD BE 0<=.<=2 '
        write(*,*) '++++++++++++++++++++++++++++++++++++++++++++'
        stop_trigger = .TRUE.
      endif
      ! CONSTANT WATER DEPTH
      if(const_h.le.0d0)then
        write(*,*) '++++++++++++++++++++++++++++++++++++++++++++'
        write(*,*) ' ERROR : "CONSTANT WATER DEPTH" SHOULD BE > 0.'
        write(*,*) '++++++++++++++++++++++++++++++++++++++++++++'
        stop_trigger = .TRUE.
      endif
      !CONSTANT ELEVATION no checks
      !CONSTANT FLOWRATE no checks
      ! *************************
      ! Check boundary conditions
      ! *************************
      ! VELOCITY OR FLOWRATE FORMULATION
      if((clqoru.ne.0).and.(clqoru.ne.1))then
        write(*,*) '++++++++++++++++++++++++++++++++++++++++++++'
        write(*,*) ' ERROR : "VELOCITY OR FLOWRATE FORMULATION" '
        write(*,*) '         SHOULD BE 0 OR 1                   '
        write(*,*) '++++++++++++++++++++++++++++++++++++++++++++'
        stop_trigger = .TRUE.
      endif
      ! LEFT BOUNDARY CONDITION
      if((bctype_l.lt.1).or.(bctype_l.gt.7))then
        write(*,*) '++++++++++++++++++++++++++++++++++++++++++++'
        write(*,*) ' ERROR : "LEFT BOUNDARY CONDITION"          '
        write(*,*) '         SHOULD BE 1<=.<=7                  '
        write(*,*) '++++++++++++++++++++++++++++++++++++++++++++'
        stop_trigger = .TRUE.
      endif
      ! LEFT DEPTH IMPOSED ha
      if(ha.lt.0d0)then
        write(*,*) '++++++++++++++++++++++++++++++++++++++++++++'
        write(*,*) ' ERROR : "LEFT DEPTH IMPOSED" SHOULD BE >= 0.'
        write(*,*) '++++++++++++++++++++++++++++++++++++++++++++'
        stop_trigger = .TRUE.
      endif
      ! LEFT VELOCITY IMPOSED no checks
      ! LEFT FLOWRATE IMPOSED no checks
      ! LEFT BOUNDARY FILE NAME bcfile_l
      if((use_bcfile_l.eqv..True.).and.(trim(bcfile_l).eq.''))then
        write(*,*) '++++++++++++++++++++++++++++++++++++++++++++'
        write(*,*) ' ERROR : "LEFT BOUNDARY FILE NAME" NOT VALID '
        write(*,*) '++++++++++++++++++++++++++++++++++++++++++++'
        stop_trigger = .TRUE.
      endif
      ! RIGHT BOUNDARY CONDITION
      if((bctype_r.lt.1).or.(bctype_r.gt.7))then
        write(*,*) '++++++++++++++++++++++++++++++++++++++++++++'
        write(*,*) ' ERROR : "RIGHT BOUNDARY CONDITION"          '
        write(*,*) '         SHOULD BE 1<=.<=7                  '
        write(*,*) '++++++++++++++++++++++++++++++++++++++++++++'
        stop_trigger = .TRUE.
      endif
      ! RIGHT DEPTH IMPOSED
      if(hb.lt.0d0)then
        write(*,*) '++++++++++++++++++++++++++++++++++++++++++++'
        write(*,*) ' ERROR : "RIGHT DEPTH IMPOSED" SHOULD BE >= 0.'
        write(*,*) '++++++++++++++++++++++++++++++++++++++++++++'
        stop_trigger = .TRUE.
      endif
      ! RIGHT VELOCITY IMPOSED no checks
      ! RIGHT FLOWRATE IMPOSED no checks
      ! RIGHT BOUNDARY FILE NAME bcfile_r
      if((use_bcfile_r.eqv..True.).and.(trim(bcfile_r).eq.''))then
        write(*,*) '++++++++++++++++++++++++++++++++++++++++++++'
        write(*,*) ' ERROR : "LEFT BOUNDARY FILE NAME" NOT VALID '
        write(*,*) '++++++++++++++++++++++++++++++++++++++++++++'
        stop_trigger = .TRUE.
      endif
      ! ***********************
      ! Check tracer parameters
      ! ***********************
      ! NUMBER OF TRACERS
      if(ntrac.lt.0)then
        write(*,*) '++++++++++++++++++++++++++++++++++++++++++++'
        write(*,*) ' ERROR : "NUMBER OF TRACERS" SHOULD BE >= 0'
        write(*,*) '++++++++++++++++++++++++++++++++++++++++++++'
        stop_trigger = .TRUE.
      endif
      do k=1,ntrac
        ! INITIAL TRACER VALUES no checks
        ! LEFT TRACER BOUNDARY CONDITION bctypet_l(1:ntrac)
        if((bctypet_l(k).lt.1).or.(bctypet_l(k).gt.3))then
          write(*,*) '++++++++++++++++++++++++++++++++++++++++++++'
          write(*,*) ' ERROR : "LEFT TRACER BOUNDARY CONDITION"   '
          write(*,*) '         SHOULD BE 1<=.<=3                  '
          write(*,*) '++++++++++++++++++++++++++++++++++++++++++++'
          stop_trigger = .TRUE.
        endif
        ! LEFT TRACER VALUE IMPOSED no checks
        ! RIGHT TRACER BOUNDARY CONDITION
        if((bctypet_r(k).lt.1).or.(bctypet_r(k).gt.3))then
          write(*,*) '++++++++++++++++++++++++++++++++++++++++++++'
          write(*,*) ' ERROR : "RIGHT TRACER BOUNDARY CONDITION"   '
          write(*,*) '         SHOULD BE 1<=.<=3                  '
          write(*,*) '++++++++++++++++++++++++++++++++++++++++++++'
          stop_trigger = .TRUE.
        endif
        ! RIGHT TRACER VALUE IMPOSED no checks
        ! TRACER DIFFUSIVITY
        if(nu_t(k).lt.0d0)then
          write(*,*) '++++++++++++++++++++++++++++++++++++++++++++'
          write(*,*) ' ERROR : "TRACER DIFFUSIVITY"               '
          write(*,*) '         SHOULD BE >= 0.                    '
          write(*,*) '++++++++++++++++++++++++++++++++++++++++++++'
          stop_trigger = .TRUE.
        endif
      enddo
      ! ************************
      ! Check bedload parameters
      ! ************************
      if (bedload.eqv..True.) then
        ! LEFT BEDLOAD BOUNDARY CONDITION
        if((bctypeqs_l.lt.1).or.(bctypeqs_l.gt.4))then
          write(*,*) '++++++++++++++++++++++++++++++++++++++++++++'
          write(*,*) ' ERROR : "LEFT BEDLOAD BOUNDARY CONDITION"  '
          write(*,*) '         SHOULD BE 1<=.<=4                  '
          write(*,*) '++++++++++++++++++++++++++++++++++++++++++++'
          stop_trigger = .TRUE.
        endif
        ! LEFT SOLID FLUX no checks
        ! LEFT EQUILIBRIUM SLOPE no checks
        ! RIGHT BEDLOAD BOUNDARY CONDITION
        if((bctypeqs_r.lt.1).or.(bctypeqs_r.gt.4))then
          write(*,*) '++++++++++++++++++++++++++++++++++++++++++++'
          write(*,*) ' ERROR : "RIGHT BEDLOAD BOUNDARY CONDITION"  '
          write(*,*) '         SHOULD BE 1<=.<=4                  '
          write(*,*) '++++++++++++++++++++++++++++++++++++++++++++'
          stop_trigger = .TRUE.
        endif
        ! RIGHT SOLID FLUX no checks
        ! RIGHT EQUILIBRIUM SLOPE no checks
        ! BEDLOAD FORMULA
        if((bedload_formula.lt.1).or.(bedload_formula.gt.9))then
          write(*,*) '++++++++++++++++++++++++++++++++++++++++++++'
          write(*,*) ' ERROR : "BEDLOAD FORMULA"                  '
          write(*,*) '         SHOULD BE 1<=.<=9                  '
          write(*,*) '++++++++++++++++++++++++++++++++++++++++++++'
          stop_trigger = .TRUE.
        endif
        if (bedload_formula.gt.2) then
          ! if not Grass, require Tau and friction law 2 or 3
          if ((frict_law.eq.0).or.(frict_law.eq.1)) then
            write(*,*) '++++++++++++++++++++++++++++++++++++++++++++'
            write(*,*) ' ERROR : FRICTION LAW INCOMPATIBLE WITH SVE '
            write(*,*) '++++++++++++++++++++++++++++++++++++++++++++'
            stop_trigger = .TRUE.
          endif
        endif
        ! BEDLOAD CALIBRATION COEFFICIENT
        if(bedload_coef.lt.0d0)then
          write(*,*) '++++++++++++++++++++++++++++++++++++++++++++'
          write(*,*) ' ERROR : "BEDLOAD CALIBRATION COEFFICIENT " '
          write(*,*) '         SHOULD BE >= 0.                    '
          write(*,*) '++++++++++++++++++++++++++++++++++++++++++++'
          stop_trigger = .TRUE.
        endif
        ! SEDIMENT POROSITY
        if(porosite.lt.0d0)then
          write(*,*) '++++++++++++++++++++++++++++++++++++++++++++'
          write(*,*) ' ERROR : "SEDIMENT POROSITY" SHOULD BE >= 0.'
          write(*,*) '++++++++++++++++++++++++++++++++++++++++++++'
          stop_trigger = .TRUE.
        endif
        ! SEDIMENT DENSITY
        if(rhos.lt.0d0)then
          write(*,*) '++++++++++++++++++++++++++++++++++++++++++++'
          write(*,*) ' ERROR : "SEDIMENT DENSITY" SHOULD BE >= 0. '
          write(*,*) '++++++++++++++++++++++++++++++++++++++++++++'
          stop_trigger = .TRUE.
        endif
        ! SEDIMENT D50
        if(d50.lt.0d0)then
          write(*,*) '++++++++++++++++++++++++++++++++++++++++++++'
          write(*,*) ' ERROR : "SEDIMENT D50" SHOULD BE >= 0.     '
          write(*,*) '++++++++++++++++++++++++++++++++++++++++++++'
          stop_trigger = .TRUE.
        endif
        ! SEDIMENT D16
        if(d16.lt.0d0)then
          write(*,*) '++++++++++++++++++++++++++++++++++++++++++++'
          write(*,*) ' ERROR : "SEDIMENT D16" SHOULD BE >= 0.     '
          write(*,*) '++++++++++++++++++++++++++++++++++++++++++++'
          stop_trigger = .TRUE.
        elseif(d16.eq.0d0) then 
          d16 = 0.5d0*d50
          if (bedload_formula.eq.5) then
            write(*,*) '++++++++++++++++++++++++++++++++++++++++++++'
            write(*,*) 'WARNING : d16 not given, set d16 = 0.5d0*d50'
            write(*,*) '++++++++++++++++++++++++++++++++++++++++++++'
          endif
        endif
        ! SEDIMENT DM
        if(dm.lt.0d0)then
          write(*,*) '++++++++++++++++++++++++++++++++++++++++++++'
          write(*,*) ' ERROR : "SEDIMENT DM" SHOULD BE >= 0.      '
          write(*,*) '++++++++++++++++++++++++++++++++++++++++++++'
          stop_trigger = .TRUE.
        elseif(dm.eq.0d0) then 
          dm = 1.1d0*d50
          if ((bedload_formula.eq.5).or. &
     &        (bedload_formula.eq.6).or. &
     &        (bedload_formula.eq.7)) then
            write(*,*) '++++++++++++++++++++++++++++++++++++++++++++'
            write(*,*) 'WARNING : dm not given, set dm = 1.1d0*d50  '
            write(*,*) '++++++++++++++++++++++++++++++++++++++++++++'
          endif
        endif
        ! SEDIMENT D84
        if(d84.lt.0d0)then
          write(*,*) '++++++++++++++++++++++++++++++++++++++++++++'
          write(*,*) ' ERROR : "SEDIMENT D84" SHOULD BE >= 0.     '
          write(*,*) '++++++++++++++++++++++++++++++++++++++++++++'
          stop_trigger = .TRUE.
        elseif(d84.eq.0d0) then 
          d84 = 2.1d0*d50
          if ((bedload_formula.eq.5).or. &
     &        (bedload_formula.eq.6).or. &
     &        (bedload_formula.eq.7)) then
            write(*,*) '++++++++++++++++++++++++++++++++++++++++++++'
            write(*,*) 'WARNING : d84 not given, set d84 = 2.1d0*d50'
            write(*,*) '++++++++++++++++++++++++++++++++++++++++++++'
          endif
        endif
        ! GRASS AG
        if(Ag.lt.0d0)then
          write(*,*) '++++++++++++++++++++++++++++++++++++++++++++'
          write(*,*) ' ERROR : "GRASS AG" SHOULD BE >= 0.     '
          write(*,*) '++++++++++++++++++++++++++++++++++++++++++++'
          stop_trigger = .TRUE.
        endif
        ! GRASS MG 
        if((mg.lt.1).or.(mg.gt.4)) then
          write(*,*) '++++++++++++++++++++++++++++++++++++++++++++'
          write(*,*) ' ERROR : "GRASS MG" SHOULD BE 1<=.<=4       '
          write(*,*) '++++++++++++++++++++++++++++++++++++++++++++'
          stop_trigger = .TRUE.
        endif
        ! MPM SKIN STRICKLER
        if(Kp.lt.0d0)then
          write(*,*) '++++++++++++++++++++++++++++++++++++++++++++'
          write(*,*) ' ERROR : "MPM SKIN STRICKLER" SHOULD BE >= 0.'
          write(*,*) '++++++++++++++++++++++++++++++++++++++++++++'
          stop_trigger = .TRUE.
        endif
        ! MPM CRITICAL SHIELDS
        if(tau_c.lt.0d0)then
          write(*,*) '++++++++++++++++++++++++++++++++++++++++++++'
          write(*,*) ' ERROR : "MPM CRITICAL SHIELDS" SHOULD BE >= 0.'
          write(*,*) '++++++++++++++++++++++++++++++++++++++++++++'
          stop_trigger = .TRUE.
        endif
        ! STD MPM COEF A
        if(smpm_a.lt.0d0)then
          write(*,*) '++++++++++++++++++++++++++++++++++++++++++++'
          write(*,*) ' ERROR : "STD MPM COEF A" SHOULD BE >= 0.'
          write(*,*) '++++++++++++++++++++++++++++++++++++++++++++'
          stop_trigger = .TRUE.
        endif
        ! STD MPM COEF B
        if(smpm_b.lt.0d0)then
          write(*,*) '++++++++++++++++++++++++++++++++++++++++++++'
          write(*,*) ' ERROR : "STD MPM COEF B" SHOULD BE >= 0.'
          write(*,*) '++++++++++++++++++++++++++++++++++++++++++++'
          stop_trigger = .TRUE.
        endif
      endif ! end if bedload
      ! *************************************
      ! Default advanced numercial parameters
      ! *************************************
      ! set default values if locked
      if (advanced_params.eqv..False.) then
        ! RECONSTRUCTION METHOD
        rec_method = 2 ! force reconstruction of variables
        ! H FLUX LIMITER, U FLUX LIMITER, V FLUX LIMITER
        muscl_ilim_h = 1 ! force minmod
        muscl_ilim_u = 1
        muscl_ilim_t = 1
        ! HYDROSTATIC RECONSTRUCTION
        if (method.eq.3) then
          hydrostarec = 0 ! ACU: no rec
        else
          hydrostarec = 1 ! Other schemes
        endif
        if ((method.eq.1).and.(bedload.eqv..True.)) then
          hydrostarec = 4 ! ROE-SVE force hydrostarec=4
        endif
        ! BOTTOM FRICTION SCHEME
        frict_scheme = 3 ! implicit
        ! VARIABLE WIDTH SCHEME
        if ((method.eq.1).and.(bedload.eqv..True.)) then
          width_scheme = 5
        else
          width_scheme = 2
        endif
        ! SVE WAVES APPROXIMATION
        if (method.eq.1) then
          swe_roots_approx = 1 ! Cardan for ROE-SVE
        else if (method.eq.2) then
          swe_roots_approx = 2 ! SFZ for HLL-SVE
        else if (method.eq.3) then
          swe_roots_approx = 3 ! Nickalls for ACU-SVE
        endif
        ! BEDLOAD FLUX DERIVATIVES
        if ((qs_derivatives.eq.0).and.    & 
       &   ((bedload_formula.eq.5).or.    &
       &    (bedload_formula.eq.6).or.    &
       &    (bedload_formula.eq.7))) then
          qs_derivatives = 1
        endif
        ! NON ERODIBLE BED FLUX LIMITER
        qs_lim = 1
        ! NON ERODIBLE BED DAMPING REGION
        eps_b_damping = 1
        ! NON ERODIBLE BED DAMPING REGION DEPTH
        eps_b = 0.1
      ! ************************************
      ! Checks advanced numercial parameters
      ! ************************************
      ! unlocked advanced params
      else
        write(*,*) '++++++++++++++++++++++++++++++++++++++++++++'
        write(*,*) ' WARNING : you have unlocked advanced       '
        write(*,*) ' numerical parameters,                      '
        write(*,*) ' make sure you known what you are doing !   '
        write(*,*) '++++++++++++++++++++++++++++++++++++++++++++'
        ! RECONSTRUCTION METHOD
        if((rec_method.lt.1).or.(rec_method.gt.2))then
          write(*,*) '++++++++++++++++++++++++++++++++++++++++++++'
          write(*,*) ' ERROR : "RECONSTRUCTION METHOD"            '
          write(*,*) '         SHOULD BE 1 or 2                   '
          write(*,*) '++++++++++++++++++++++++++++++++++++++++++++'
          stop_trigger = .TRUE.
        endif
        ! H FLUX LIMITER
        if((muscl_ilim_h.lt.1).or.(muscl_ilim_h.gt.6))then
          write(*,*) '++++++++++++++++++++++++++++++++++++++++++++'
          write(*,*) ' ERROR : "H FLUX LIMITER"                   '
          write(*,*) '         SHOULD BE 1<=.<=6                  '
          write(*,*) '++++++++++++++++++++++++++++++++++++++++++++'
          stop_trigger = .TRUE.
        endif
        ! U FLUX LIMITER
        if((muscl_ilim_u.lt.1).or.(muscl_ilim_u.gt.6))then
          write(*,*) '++++++++++++++++++++++++++++++++++++++++++++'
          write(*,*) ' ERROR : "U FLUX LIMITER"                   '
          write(*,*) '         SHOULD BE 1<=.<=6                  '
          write(*,*) '++++++++++++++++++++++++++++++++++++++++++++'
          stop_trigger = .TRUE.
        endif
        ! T FLUX LIMITER
        if((muscl_ilim_t.lt.1).or.(muscl_ilim_t.gt.6))then
          write(*,*) '++++++++++++++++++++++++++++++++++++++++++++'
          write(*,*) ' ERROR : "T FLUX LIMITER"                   '
          write(*,*) '         SHOULD BE 1<=.<=6                  '
          write(*,*) '++++++++++++++++++++++++++++++++++++++++++++'
          stop_trigger = .TRUE.
        endif
        ! HYDROSTATIC RECONSTRUCTION
        if((hydrostarec.lt.0).or.(hydrostarec.gt.5))then
          write(*,*) '++++++++++++++++++++++++++++++++++++++++++++'
          write(*,*) ' ERROR : "HYDROSTATIC RECONSTRUCTION"       '
          write(*,*) '         SHOULD BE 0<=.<=5                  '
          write(*,*) '++++++++++++++++++++++++++++++++++++++++++++'
          stop_trigger = .TRUE.
        endif
        if (hydrostarec.eq.3) then
          write(*,*) '++++++++++++++++++++++++++++++++++++++++++++'
          write(*,*) ' WARNING : Centered scheme for bottom is    '
          write(*,*) '           is not well-balanced.            '
          write(*,*) '++++++++++++++++++++++++++++++++++++++++++++'
        endif
        if ((method.eq.3).and.(hydrostarec.ne.0)) then
          write(*,*) '++++++++++++++++++++++++++++++++++++++++++++'
          write(*,*) ' WARNING : ACU scheme use its own reconstruction, '
          write(*,*) ' hydrostatic reconstruction forced to 0.    '
          write(*,*) '++++++++++++++++++++++++++++++++++++++++++++'
          hydrostarec = 0
        endif
        if ((method.eq.1).and.(hydrostarec.lt.4).and.(bedload.eqv..True.)) then
          write(*,*) '++++++++++++++++++++++++++++++++++++++++++++'
          write(*,*) ' WARNING : ROE-SVE scheme is supposed to work '
          write(*,*) ' with either  HYDROSTATIC RECONSTRUCTION = 4 or 5 '
          write(*,*) '++++++++++++++++++++++++++++++++++++++++++++'
        endif
        if ((method.eq.2).and.(hydrostarec.ne.1).and.(bedload.eqv..True.)) then
          write(*,*) '++++++++++++++++++++++++++++++++++++++++++++'
          write(*,*) ' WARNING : HLL-SVE scheme is supposed to work '
          write(*,*) ' with HYDROSTATIC RECONSTRUCTION = 1 '
          write(*,*) '++++++++++++++++++++++++++++++++++++++++++++'
        endif
        ! BOTTOM FRICTION SCHEME
        if((frict_scheme.lt.1).or.(frict_scheme.gt.3))then
          write(*,*) '++++++++++++++++++++++++++++++++++++++++++++'
          write(*,*) ' ERROR : "BOTTOM FRICTION SCHEME"           '
          write(*,*) '         SHOULD BE 1<=.<=3                  '
          write(*,*) '++++++++++++++++++++++++++++++++++++++++++++'
          stop_trigger = .TRUE.
        endif
        ! VARIABLE WIDTH SCHEME
        if((width_scheme.lt.1).or.(width_scheme.gt.5))then
          write(*,*) '++++++++++++++++++++++++++++++++++++++++++++'
          write(*,*) ' ERROR : "VARIABLE WIDTH SCHEME"            '
          write(*,*) '         SHOULD BE 1<=.<=5                  '
          write(*,*) '++++++++++++++++++++++++++++++++++++++++++++'
          stop_trigger = .TRUE.
        endif
        if((width_scheme.eq.1).or.(width_scheme.eq.3) &
       &                      .or.(width_scheme.eq.4)) then
          write(*,*) '++++++++++++++++++++++++++++++++++++++++++++'
          write(*,*) ' WARNING : VARIABLE WIDTH SCHEME = 1, 3 or 4'
          write(*,*) '           is experimental                  '
          write(*,*) '++++++++++++++++++++++++++++++++++++++++++++'
        endif
        ! SVE WAVES APPROXIMATION
        if((swe_roots_approx.lt.1).or.(swe_roots_approx.gt.3))then
          write(*,*) '++++++++++++++++++++++++++++++++++++++++++++'
          write(*,*) ' ERROR : "SVE WAVES APPROXIMATION"          '
          write(*,*) '         SHOULD BE 1<=.<=3                  '
          write(*,*) '++++++++++++++++++++++++++++++++++++++++++++'
          stop_trigger = .TRUE.
        endif
        if((method.eq.1).and.(swe_roots_approx.ne.1)) then
          write(*,*) '++++++++++++++++++++++++++++++++++++++++++++'
          write(*,*) ' WARNING : SVE WAVES APPROXIMATION = 1      '
          write(*,*) '           IS ADVISED WITH ROE-SVE          '
          write(*,*) '++++++++++++++++++++++++++++++++++++++++++++'
        elseif((method.eq.2).and.(swe_roots_approx.ne.2)) then
          write(*,*) '++++++++++++++++++++++++++++++++++++++++++++'
          write(*,*) ' WARNING : SVE WAVES APPROXIMATION = 2      '
          write(*,*) '           IS ADVISED WITH HLL-SVE          '
          write(*,*) '++++++++++++++++++++++++++++++++++++++++++++'
        elseif((method.eq.3).and.(swe_roots_approx.ne.3)) then
          write(*,*) '++++++++++++++++++++++++++++++++++++++++++++'
          write(*,*) ' WARNING : SVE WAVES APPROXIMATION = 3      '
          write(*,*) '           IS ADVISED WITH ACU-SVE          '
          write(*,*) '++++++++++++++++++++++++++++++++++++++++++++'
        endif
        
        ! BEDLOAD FLUX DERIVATIVES
        if((qs_derivatives.lt.0).or.(qs_derivatives.gt.4))then
          write(*,*) '++++++++++++++++++++++++++++++++++++++++++++'
          write(*,*) ' ERROR : "BEDLOAD FLUX DERIVATIVES"         '
          write(*,*) '         SHOULD BE 0<=.<=4                  '
          write(*,*) '++++++++++++++++++++++++++++++++++++++++++++'
          stop_trigger = .TRUE.
        endif
        if ((qs_derivatives.eq.0).and.    & 
       &   ((bedload_formula.eq.5).or.    &
       &    (bedload_formula.eq.6).or.    &
       &    (bedload_formula.eq.7))) then
          qs_derivatives = 1
          write(*,*) '++++++++++++++++++++++++++++++++++++++++++++'
          write(*,*) ' WARNING : Exact bedload flux derivatives   '
          write(*,*) ' not implemented for this formula.          '
          write(*,*) ' Switching to finite differences.           '
          write(*,*) '++++++++++++++++++++++++++++++++++++++++++++'
        endif
        ! NON ERODIBLE BED FLUX LIMITER
        if((qs_lim.ne.0).and.(qs_lim.ne.1))then
          write(*,*) '++++++++++++++++++++++++++++++++++++++++++++'
          write(*,*) ' ERROR : "NON ERODIBLE BED FLUX LIMITER"    '
          write(*,*) '         SHOULD BE 0 or 1                   '
          write(*,*) '++++++++++++++++++++++++++++++++++++++++++++'
          stop_trigger = .TRUE.
        endif
        if(qs_lim.eq.0)then
          write(*,*) '++++++++++++++++++++++++++++++++++++++++++++'
          write(*,*) ' WARNING : "NON ERODIBLE BED FLUX LIMITER"  '
          write(*,*) ' IS 0, POSSIBLE VIOLATION OF NON ERODIBLE BED '
          write(*,*) '++++++++++++++++++++++++++++++++++++++++++++'
        endif
        ! NON ERODIBLE BED DAMPING REGION
        if((eps_b_damping.ne.0).and.(eps_b_damping.ne.1))then
          write(*,*) '++++++++++++++++++++++++++++++++++++++++++++'
          write(*,*) ' ERROR : "NON ERODIBLE BED DAMPING REGION"  '
          write(*,*) '         SHOULD BE 0 or 1                   '
          write(*,*) '++++++++++++++++++++++++++++++++++++++++++++'
          stop_trigger = .TRUE.
        endif
        if(eps_b_damping.eq.0)then
          write(*,*) '++++++++++++++++++++++++++++++++++++++++++++'
          write(*,*) ' WARNING : "NON ERODIBLE BED DAMPING REGION" '
          write(*,*) ' IS 0, QS IS ALWAYS COMPUTED,               '
          write(*,*) ' EVEN IF NO MOBILE SEDIMENTS                '
          write(*,*) '++++++++++++++++++++++++++++++++++++++++++++'
        endif
        ! NON ERODIBLE BED DAMPING REGION DEPTH
        if(eps_b.lt.0d0)then
          write(*,*) '++++++++++++++++++++++++++++++++++++++++++++'
          write(*,*) ' ERROR : "NON ERODIBLE BED DAMPING REGION DEPTH '
          write(*,*) '         SHOULD BE >= 0.                    '
          write(*,*) '++++++++++++++++++++++++++++++++++++++++++++'
          stop_trigger = .TRUE.
        endif
      endif
      ! ----------------------------------------------------------------
      if(stop_trigger.eqv..TRUE.) then
        write(*,*) '********************************************'
        write(*,*) ' Verify keywords, see arrakis.dico          '
        write(*,*) '********************************************'
        stop 
      endif
      ! ----------------------------------------------------------------
      return
      end subroutine
      ! ****************************************************************




      ! ****************************************************************
      subroutine lstop(c,l)
      ! ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
      !
      ! ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
      character(*) :: c
      character(*) :: l
      integer n,m
      n=len(trim(adjustl(c)))
      m=len(trim(adjustl(l)))
      write(*,*) '++++++++++++++++++++++++++++++++++++++++++++'
      write(*,*) ' ERROR : during reading of input data file. '
      write(*,*) ' Check format.'
      write(*,*) ' Marker : ',c(1:n)
      write(*,*) ' Line   : ',l(1:m)
      write(*,*) ' Stop.'
      write(*,*) '++++++++++++++++++++++++++++++++++++++++++++'
      stop
      end subroutine
      ! ****************************************************************





      ! ****************************************************************
      ! ****************************************************************
      ! ****************************************************************
      ! ****************************************************************
      !                   BOUNDARY CONDITION FILES
      ! ****************************************************************
      ! ****************************************************************
      ! ****************************************************************
      ! ****************************************************************




      ! ****************************************************************
      subroutine read_bc
      ! ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
      ! boundary conditions files
      ! ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
      use module_data, only: use_bcfile_l, bcfile_l, &
    &                        use_bcfile_r, bcfile_r, &
    &                        vars_bcl, nvars_bcl, &
    &                        vars_bcr, nvars_bcr, &
    &                        time_bcl, time_bcr, &
    &                        field_bcl, field_bcr, &
    &                        bctype_l, bctype_r
      implicit none
      integer                                :: i, nlines, io
      character(len=255)                     :: var_line
      ! ----------------------------------------------------------------
      ! initialize variables of BC files to blank
      do i=1,size(vars_bcl)
        vars_bcl(i) = ''
      enddo
      do i=1,size(vars_bcr)
        vars_bcr(i) = ''
      enddo
      ! read left liquid boundary file (default: BCL.liq)
      if (use_bcfile_l.eqv..True.) then
        write(*,*) ''
        write(*,*) '°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°'
        write(*,*) ' Reading left boundary condition file       '
        write(*,*) '°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°'
        write(*,*) ''
        ! get the number of lines
        nlines = 0
        open(unit = 101, file = trim(bcfile_l) // '.liq')
        do
          read(101, *, iostat=io)
          if (io/=0) exit
          nlines = nlines + 1
        end do
        close(unit = 101)
        nlines = nlines - 3 ! remove header count
        ! read content
        open(unit = 101, file = trim(bcfile_l) // '.liq')
        read(101, *)
        read(101, '(A)') var_line
        read(101, *)
        call split_string(var_line, vars_bcl)
        call count_vars(vars_bcl, nvars_bcl) ! number of fiels to read
        ! allocate tmp tables
        allocate(time_bcl  (1:nlines))
        allocate(field_bcl (1:nlines, nvars_bcl-1))
        ! read file content
        do i = 1, nlines
          read(101, *) time_bcl(i), field_bcl(i,:)
        end do
        close(unit = 101)
        ! check that given data at cdl is consistent with bc type
        call check_bc_inputs(vars_bcl, bctype_l)
      endif
      ! ----------------------------------------------------------------
      ! read right liquid boundary file (default: BCR.liq)
      if (use_bcfile_r.eqv..True.) then
        write(*,*) ''
        write(*,*) '°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°'
        write(*,*) ' Reading right boundary condition file      '
        write(*,*) '°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°'
        write(*,*) ''
        ! get the number of lines
        nlines = 0
        open(unit = 201, file = trim(bcfile_r) // '.liq')
        do
          read(201, *, iostat=io)
          if (io/=0) exit
          nlines = nlines + 1
        end do
        close(unit = 201)
        nlines = nlines - 3 ! remove header count
        ! read content
        open(unit = 201, file = trim(bcfile_r) // '.liq')
        read(201, *)
        read(201, '(A)') var_line
        read(201, *)
        call split_string(var_line, vars_bcr)
        call count_vars(vars_bcr, nvars_bcr) ! number of fiels to read
        ! allocate tmp tables
        allocate(time_bcr  (1:nlines))
        allocate(field_bcr (1:nlines, nvars_bcr-1))
        ! read file content
        do i = 1, nlines
          read(201, *) time_bcr(i), field_bcr(i,:)
        end do
        close(unit = 201)
        ! check that given data at cdl is consistent with bc type
        call check_bc_inputs(vars_bcr, bctype_r)
      endif
      ! ----------------------------------------------------------------
      return
      end subroutine
      ! ****************************************************************




      ! ****************************************************************
      subroutine check_bc_inputs(vars_bc, bctype)
      ! ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
      ! Check that given data at cdl is consistent with bc type
      ! ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
      use module_data, only: clqoru
      implicit none
      character(len=3), dimension(:), intent(in) :: vars_bc
      integer,                        intent(in) :: bctype
      ! ----------------------------------------------------------------
      ! Check that first column is time
      if (trim(vars_bc(1)).ne.'T') then
        write(*,*) '++++++++++++++++++++++++++++++++++++++++++++'
        write(*,*) ' ERROR READING LIQ BOUNDARY FILE,           '
        write(*,*) ' MISSING TIME T                             '
        write(*,*) '++++++++++++++++++++++++++++++++++++++++++++'
        stop
      endif
      ! Check imposed H or ZS
      if ((bctype == 2).or.(bctype == 4)) then
        if ((trim(vars_bc(2)).ne.'H').and. &
     &      (trim(vars_bc(2)).ne.'ZS')) then
          write(*,*) '++++++++++++++++++++++++++++++++++++++++++++'
          write(*,*) ' ERROR READING LIQ BOUNDARY FILE, PROVIDE  '
          write(*,*) ' EITHER WATER DEPTH H OR ELEVATION ZS       '
          write(*,*) '++++++++++++++++++++++++++++++++++++++++++++'
          stop
        endif
      endif
      ! Check imposed Q or U
      if ((bctype == 3).or.(bctype == 4)) then
        ! Check imposed Q
        if ((clqoru.eq.1).and. &
     &      (trim(vars_bc(2)).ne.'Q').and. &
     &      (trim(vars_bc(3)).ne.'Q')) then
          write(*,*) '++++++++++++++++++++++++++++++++++++++++++++'
          write(*,*) ' ERROR READING LIQ BOUNDARY FILE,           '
          write(*,*) ' PROVIDE Q                                  '
          write(*,*) '++++++++++++++++++++++++++++++++++++++++++++'
          stop
        endif
        ! Check imposed U
        if ((clqoru.eq.0).and. &
     &      (trim(vars_bc(2)).ne.'U').and. &
     &      (trim(vars_bc(3)).ne.'U')) then
          write(*,*) '++++++++++++++++++++++++++++++++++++++++++++'
          write(*,*) ' ERROR READING LIQ BOUNDARY FILE,           '
          write(*,*) ' PROVIDE U                                  '
          write(*,*) '++++++++++++++++++++++++++++++++++++++++++++'
          stop
        endif
      endif
      ! ----------------------------------------------------------------
      return
      end subroutine
      ! ****************************************************************





      ! ****************************************************************
      ! ****************************************************************
      ! ****************************************************************
      ! ****************************************************************
      !                     OUTPUTS
      ! ****************************************************************
      ! ****************************************************************
      ! ****************************************************************
      ! ****************************************************************




      !*****************************************************************
      subroutine display
      ! ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
      ! Writes solution into data files
      ! ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
      use module_data, only: ndisplay, niter, dt, time, dt_const, eps_num
      implicit none
      real(kind=dp) :: dt_output, rec_time
      logical       :: write_condition
      ! ----------------------------------------------------------------
      ! write condition
      !condition = mod(niter,ndisplay)==0 ! old condition on iterations
      dt_output = ndisplay*dt_const
      rec_time = ceiling((time-dt)/dt_output)*dt_output
      if ((rec_time.gt.(time-dt)) .and. &
     &    (rec_time.le.time)) then
        write_condition = .True.
      else
        write_condition = .False.
      endif
      ! listing output
      if(write_condition.eqv..True.)then
         write(*, '(a,e14.6,a,i6,a,e14.6)') 'dt=',dt, '/ niter =',niter,'/ time=',time     
      endif
      ! ----------------------------------------------------------------
      end subroutine
      !*****************************************************************




      !*****************************************************************
      subroutine  write_result
      ! ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
      ! Writes solutions into data files
      ! ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
      use module_data, only: when, nrecord, time, dt, dt_const, &
        & tstart, irec, &
        & nx, x, res_file, vars_outputs, nvars_outputs, &
        & probes, probes_x, pbr_file, vars_probes, nvars_probes, &
        & snaps, snaps_i, snaps_t, dformat
      implicit none
      real(kind=dp)                        :: fi (1:nx,1:nvars_outputs)
      real(kind=dp)                        :: fp (1:nx,1:nvars_probes)
      real(kind=dp)                        :: fp_interp (1:nvars_probes)
      real(kind=dp)                        :: alpha, rec_time, dt_output
      integer                              :: i, j, k, l
      character(len=4)                     :: kchar
      character(len=255)                   :: probe_file
      logical                              :: found_value, write_condition
      ! ----------------------------------------------------------------
      ! write condition
      !condition = mod(niter,nrecord)==0 ! old condition on iterations
      dt_output = nrecord*dt_const
      rec_time = ceiling((time-dt)/dt_output)*dt_output
      if ((rec_time.gt.(time-dt)) .and. &
     &    (rec_time.le.time)) then
        write_condition = .True.
      else
        write_condition = .False.
      endif
      ! ----------------------------------------------------------------
      ! WRITE MAIN RESULT FILE
      ! fi -> contains the selected variables to write
      ! ----------------------------------------------------------------
      ! load fields to write
      if((when == 0).or.(write_condition.eqv..True.).or.(when == 2))then
        do l=1,nvars_outputs
          call prepare_output_var(vars_outputs(l), fi(:,l))
        enddo
      endif
      ! ----------------------------------------------------------------
      if(when == 0) then
        irec = 0
        call write_sol(x(1:nx), fi, trim(res_file) // "ini.dat")
      endif
      ! ----------------------------------------------------------------
      if(write_condition.eqv..True.)then
        irec = irec + 1
        call write_sol(x(1:nx), fi, &
          & trim(res_file) // &
          & trim(integer_to_string(irec)) // ".dat")
      endif
      ! ----------------------------------------------------------------
      if(when == 2) then
        irec = irec + 1
        call write_sol(x(1:nx), fi, &
          & trim(res_file) // "fin.dat")
      endif
      ! ----------------------------------------------------------------
      ! WRITE SNAPSHOTS RESULT FILE
      ! fi -> contains the selected variables to write
      ! ----------------------------------------------------------------
      if (snaps_i.le.snaps) then
        if ( (snaps_t(snaps_i).gt.(time-dt)) .and. &
       &     (snaps_t(snaps_i).le.(time)) ) then
          ! get variables
          do l=1,nvars_outputs
            call prepare_output_var(vars_outputs(l), fi(:,l))
          enddo
          ! write
          call write_sol(x(1:nx), fi, &
            & trim(res_file) // "_SNAP" // &
            & trim(integer_to_string(snaps_i)) // ".dat")
          snaps_i = snaps_i + 1
        endif
      endif
      ! ----------------------------------------------------------------
      ! WRITE PROBES
      ! fp -> contains the probe selected variables to write
      ! ----------------------------------------------------------------
      if (probes.gt.0) then
        ! get variables
        do l=1,nvars_probes
          call prepare_output_var(vars_probes(l), fp(:,l))
        enddo
        ! loop on probes
        do k=1,probes
          ! interpolate variable at probe location
          found_value = .False.
          do i=0,nx
            if ((x(i) <= probes_x(k)).and.(probes_x(k) < x(i+1))) then
              found_value = .True.
              alpha = (x(i+1)-probes_x(k))/(x(i+1)-x(i))
              do j=1,nvars_probes
                fp_interp(j) = alpha*fp(i,j) + (1.d0-alpha)*fp(i+1,j)
              enddo
            endif
          enddo
          if (found_value.eqv..False.) then
            write(*,*) '++++++++++++++++++++++++++++++++++++++++++++'
            write(*,*) ' ERROR WRITING PROBE FILE, '
            write(*,*) ' CHECK PROBE COORDINATES'
            write(*,*) '++++++++++++++++++++++++++++++++++++++++++++'
            stop
          end if
          ! probe file name
          write(kchar,'(I4.4)') k
          probe_file = trim(pbr_file)//'_'//trim(kchar)//".dat"
          ! open probe file
          if ((time.eq.tstart).and.(when == 0)) then
            ! write header if first start
            open(unit = 34, file=probe_file)
            write(34, *) '# Probe file'
            write(34, fmt='(a4, a1)', advance='no') 'Time', ','
            do j=1,nvars_probes
              if (j<nvars_probes) then
                write(34, fmt='(a, a1)', advance='no') &
     &                               trim(adjustl(vars_probes(j))), ','
              else
                write(34, fmt='(a)') trim(adjustl(vars_probes(j)))
              endif
            enddo
          else
            open(unit = 34, file=probe_file, access='append')
          endif
          ! write probe file
          write(34, fmt='('//dformat//', A)', advance='no') time, ','
          do j=1,nvars_probes
            if (j<nvars_probes) then
              write(34, fmt='('//dformat//', A)', advance='no') &
     &                                         fp_interp(j), ','
            else
              write(34, fmt='('//dformat//')') fp_interp(j)
            endif
          enddo
          close(34)
        enddo
      endif
      ! ----------------------------------------------------------------
      end subroutine
      !*****************************************************************




      !*****************************************************************
      subroutine prepare_output_var(var_in, f)
      ! ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
      ! do
      ! -> check var name
      ! -> prepare the corresponding variable (compute if not primitive)
      ! -> load var in temporary array f for printout
      ! ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
      !  ZB : bottom elevation
      !  H  : water depth 
      !  U  : water depth-averaged velocity 
      !  Q  : water depth-averaged discharge
      !  FC : friction coefficient
      !  B  : sediment depth (if bedload)
      !  ZF : hard bottom elevation (if bedload)
      !  QS : solid discharge (if bedload)
      !  TB : bottom shear stress
      !  TS : dimensionless bottom shear stress tau* (if bedload)
      !  L  : rectangular channel width (if variable_width)
      !  A  : wetted area, A=H*L (if variable_width)
      !  AS : sediment area, A=(zb-zf)*L (if variable_width)
      !  QL : section water discharge, QL = Q*L (if variable_width)
      !  Ti : tracer number i (1<=i<=99)
      !  A1 : auxiliary array 1 (for debug)
      !  A2 : auxiliary array 2 (for debug)
      ! ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
      use module_data, only: nx, zb, h, u, q, zf, fric, qsed, &
     &                       aux1, aux2, taub, taus, tau_adim, Lx, &
     &                       bedload, variable_width, g, eps, &
     &                       ntrac, t
      use module_bedload, only: tau
      implicit none
      character(len=3), intent(in)                :: var_in
      real(kind=dp),    intent(inout)             :: f (1:nx)
      integer                                     :: i, k
      character(len=3)                            :: var
      character(len=4)                            :: char_trac
      logical                                     :: main_var_flag
      logical                                     :: tracer_flag
      ! ----------------------------------------------------------------
      var = trim(adjustr(var_in)) ! remove trailing blank space
      main_var_flag = .FALSE.
      tracer_flag = .TRUE.
      ! ----------------------------------------------------------------
      if (var.ne.'') then
        ! READ MAIN VARIABLES
        ! ~~~~~~~~~~~~~~~~~~~~~~~~~~
        if (trim(var).eq.' ZB') then
        ! ~~~~~~~~~~~~~~~~~~~~~~~~~~
          f = zb(1:nx)
        ! ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
        elseif (trim(var).eq.'  H') then
        ! ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
          f = h(1:nx)
        ! ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
        elseif (trim(var).eq.'  U') then
        ! ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
          f = u(1:nx)
        ! ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
        elseif (trim(var).eq.'  Q') then
        ! ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
          f = q(1:nx)
        ! ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
        elseif (trim(var).eq.' FR') then
        ! ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
          do i=1,nx
            if (h(i).ge.eps) then
              f(i) = u(i)/sqrt(g*h(i))
            else
              f(i) = 0d0
            endif
          enddo
        ! ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
        elseif (trim(var).eq.' FC') then
        ! ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
          f = fric(1:nx)
        ! ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
        elseif (trim(var).eq.' ZF') then
        ! ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
          if (bedload.eqv..False.) then
            write(*,*) '++++++++++++++++++++++++++++++++++++++++++++'
            write(*,*) ' ERROR : Unable to print ZF, BEDLOAD = FALSE'
            write(*,*) '++++++++++++++++++++++++++++++++++++++++++++'
            stop
          endif
          f = zf(1:nx)
        ! ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
        elseif (trim(var).eq.'  B') then
        ! ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
          if (bedload.eqv..False.) then
            write(*,*) '++++++++++++++++++++++++++++++++++++++++++++'
            write(*,*) ' ERROR : Unable to print B, BEDLOAD = FALSE'
            write(*,*) '++++++++++++++++++++++++++++++++++++++++++++'
            stop
          endif
          ! not a primitive variable, compute res
          do i=1,nx
            f(i) = max(0.d0, zb(i)- zf(i))
          enddo
        ! ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
        elseif (trim(var).eq.' QS') then
        ! ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
          if (bedload.eqv..False.) then
            write(*,*) '++++++++++++++++++++++++++++++++++++++++++++'
            write(*,*) ' ERROR : Unable to print QS, BEDLOAD = FALSE'
            write(*,*) '++++++++++++++++++++++++++++++++++++++++++++'
            stop
          endif
          f = qsed(1:nx)
        ! ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
        elseif (trim(var).eq.' TB') then
        ! ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
          ! not a primitive variable, compute res
          do i=1,nx
            taub(i) = tau(h(i), u(i), Lx(i), 0d0, fric(i))
          enddo
          f = taub(1:nx)
        ! ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
        elseif (trim(var).eq.' TS') then
        ! ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
          if (bedload.eqv..False.) then
            write(*,*) '++++++++++++++++++++++++++++++++++++++++++++'
            write(*,*) ' ERROR : Unable to print TS, BEDLOAD = FALSE'
            write(*,*) '++++++++++++++++++++++++++++++++++++++++++++'
            stop
          endif
          ! not a primitive variable, compute res
          do i=1,nx
            taub(i) = tau(h(i), u(i), Lx(i), 0d0, fric(i))
            taus(i) = taub(i)*tau_adim
          enddo
          f = taus(1:nx)
        ! ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
        elseif (trim(var).eq.'  L') then
        ! ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
          if (variable_width.eqv..False.) then
            write(*,*) '++++++++++++++++++++++++++++++++++++++++++++'
            write(*,*) ' ERROR : Unable to print L, '
            write(*,*) ' VARIABLE CHANNEL WIDTH = FALSE'
            write(*,*) '++++++++++++++++++++++++++++++++++++++++++++'
            stop
          endif
          f = Lx(1:nx)
        ! ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
        elseif (trim(var).eq.'  A') then
        ! ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
          if (variable_width.eqv..False.) then
            write(*,*) '++++++++++++++++++++++++++++++++++++++++++++'
            write(*,*) ' ERROR : Unable to print A, '
            write(*,*) ' VARIABLE CHANNEL WIDTH = FALSE'
            write(*,*) '++++++++++++++++++++++++++++++++++++++++++++'
            stop
          endif
          ! not a primitive variable
          f = h(1:nx)*Lx(1:nx)
        ! ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
        elseif (trim(var).eq.' QL') then
        ! ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
          if (variable_width.eqv..False.) then
            write(*,*) '++++++++++++++++++++++++++++++++++++++++++++'
            write(*,*) ' ERROR : Unable to print QL, '
            write(*,*) ' VARIABLE CHANNEL WIDTH = FALSE'
            write(*,*) '++++++++++++++++++++++++++++++++++++++++++++'
            stop
          endif
          ! not a primitive variable
          f = q(1:nx)*Lx(1:nx)
        ! ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
        elseif (trim(var).eq.' AS') then
        ! ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
          if (variable_width.eqv..False.) then
            write(*,*) '++++++++++++++++++++++++++++++++++++++++++++'
            write(*,*) ' ERROR : Unable to print AS, '
            write(*,*) ' VARIABLE CHANNEL WIDTH = FALSE'
            write(*,*) '++++++++++++++++++++++++++++++++++++++++++++'
            stop
          endif
          if (bedload.eqv..False.) then
            write(*,*) '++++++++++++++++++++++++++++++++++++++++++++'
            write(*,*) ' ERROR : Unable to print AS, BEDLOAD = FALSE'
            write(*,*) '++++++++++++++++++++++++++++++++++++++++++++'
            stop
          endif
          ! not a primitive variable, compute res
          do i=1,nx
            f(i) = max(0.d0, zb(i)- zf(i))*Lx(i)
          enddo
        ! ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
        elseif (trim(var).eq.' A1') then
        ! ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
          f = aux1(1:nx)
        ! ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
        elseif (trim(var).eq.' A2') then
        ! ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
          f = aux2(1:nx)
        else
          main_var_flag = .TRUE.
        endif
        ! ~~~~~~~~~~~~
        ! READ TRACERS
        ! ~~~~~~~~~~~~
        if (ntrac.gt.0) then
          do k=1,ntrac
            char_trac = integer_to_string(k)
            if ((trim(var).eq." T"//char_trac(4:4)).or. &
           &    (trim(var).eq."T"//char_trac(3:4))) then
              f = t(1:nx, k)
              tracer_flag = .FALSE.
            endif
          enddo
        endif 
        ! ~~~~~
        ! ERROR
        ! ~~~~~
        if ((main_var_flag.eqv..TRUE.).and.(tracer_flag.eqv..TRUE.)) then
          write(*,*) '++++++++++++++++++++++++++++++++++++++++++++'
          write(*,*) ' ERROR : Unkown variable name, see dico VARIABLES '
          write(*,*) '++++++++++++++++++++++++++++++++++++++++++++'
          stop
        endif
      endif
      ! ----------------------------------------------------------------
      end subroutine
      !*****************************************************************




      !*****************************************************************
      subroutine  write_sol(f0, fi, fichier)
      ! ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
      ! Writes two vectors in a specified file. File will be owerwritten
      ! if existing
      ! ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
      use module_data, only: nx, time, vars_outputs, nvars_outputs, &
     &                       dformat 
      implicit none
      real(kind=dp),   intent(in)          :: f0 (1:nx)
      real(kind=dp),   intent(in)          :: fi (1:nx,1:nvars_outputs)
      character(len=*),intent(in)          :: fichier
      integer                              :: i, j
      ! ----------------------------------------------------------------
      open(unit = 24, file = fichier)
      ! write header with field names
      write(24, *) time, ',', nx
      write(24, fmt='(a1, a1)', advance='no') 'X', ','
      do j=1,nvars_outputs
        if (j<nvars_outputs) then
          write(24, fmt='(a, a1)' , advance='no') &
     &                         trim(adjustl(vars_outputs(j))), ','
        else
          write(24, fmt='(a)') trim(adjustl(vars_outputs(j)))
        endif
      enddo
      ! write fields 
      do i=1,nx
        write(24, fmt='('//dformat//', A)', advance='no') f0(i), ','
        do j=1,nvars_outputs
          if (j<nvars_outputs) then
            write(24, fmt='('//dformat//', A)', advance='no') fi(i,j), ','
          else
            write(24, fmt='('//dformat//')') fi(i,j)
          endif
        enddo
      end do
      close(24)
      ! ----------------------------------------------------------------
      end subroutine
      !*****************************************************************
 
 
 

      ! ****************************************************************
      ! ****************************************************************
      ! ****************************************************************
      ! ****************************************************************
      !                     UTILS
      ! ****************************************************************
      ! ****************************************************************
      ! ****************************************************************
      ! ****************************************************************




      !*****************************************************************
      function integer_to_string(n0) result(chaine)
      ! ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
      ! Convert an integer lower than 9999 into a string
      ! ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
      implicit none
      !Input :  + no : integer, must be < 10000
      !Outup :  + string
      integer, intent(in) :: n0
      character(len=4) :: chaine
      integer :: n
      ! ----------------------------------------------------------------
      n = n0
      chaine(4:4) = achar(mod(n,10)+48)
      n = n/10
      chaine(3:3) = achar(mod(n,10)+48)
      n = n/10
      chaine(2:2) = achar(mod(n,10)+48)
      n = n/10
      chaine(1:1) = achar(mod(n,10)+48)
      ! ----------------------------------------------------------------
      end function integer_to_string
      !*****************************************************************




      ! ****************************************************************
      subroutine tab55(line)
      ! ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
      ! 
      ! ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
      character(*) :: line
      read(65,'(A)') line
      backspace(65)
      read(65,'(t55)',advance='no')
      end subroutine
      ! ****************************************************************




      ! ****************************************************************
      subroutine split_string(str, split_str)
      ! ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
      ! split string containing ',' into several char of len(2)
      ! ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
      implicit none
      character(len=255), intent(in)                :: str
      character(len=3), dimension(:), intent(out)   :: split_str
      integer :: i, n, start, finish
      ! ----------------------------------------------------------------
      ! initialize the counter
      n = 1
      ! initialize the start of the substring
      start = 1
      ! loop over the string
      do i=1,len_trim(str)
        ! if a comma is found or end of string, extract the substring
        if (str(i:i)==','.or.i==len_trim(str)) then
          finish = i-1
          if (i==len_trim(str)) finish=i
          if (str(start:finish).eq.',') then
            split_str(n) = ''
          else
            split_str(n) = str(start:finish)
          endif
          n = n+1
          start = i+1
        endif
      enddo
      ! ----------------------------------------------------------------
      end subroutine
      ! ****************************************************************




      ! ****************************************************************
      subroutine count_vars(vars, nvars)
      ! ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
      ! Count the number of char of len(3) different than blank ''
      ! ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
      implicit none
      character(len=3), dimension(:), intent(in)  :: vars
      integer,                        intent(out) :: nvars
      integer                                     :: i
      character(len=3)                            :: var
      ! ----------------------------------------------------------------
      nvars = 0
      do i=1,size(vars)
        var = vars(i)
        if (len_trim(var).ne.0) then
          nvars = nvars + 1
        endif
      enddo
      ! ----------------------------------------------------------------
      end subroutine
      ! ****************************************************************


     

      end module
      ! ****************************************************************
