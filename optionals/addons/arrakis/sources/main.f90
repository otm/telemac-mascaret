




      ! ****************************************************************
      ! Program Arrakis
      ! 
      ! 1D Finite Volume methods for the Saint-Venant-Exner equations
      ! 
      ! ****************************************************************
      program main
      use module_data
      use module_alloc,   only: allocation, deallocation
      use module_io,      only: read_inputs, check_inputs, &
     &                          display, write_result
      use module_init,    only: init_geometry, init_variables
      use module_forward, only: forward
      ! ----------------------------------------------------------------
      implicit none
      ! ----------------------------------------------------------------

      write(*,*) ''
      write(*,*) '같같같같같같같같같같같같같같같같같같같같같같같같같같�'
      write(*,*) '�                    Arrakis                        �'
      write(*,*) '같같같같같같같같같같같같같같같같같같같같같같같같같같�'
      write(*,*) ''

      ! ----------------------------------------------------------------
      ! Initialisation
      ! ----------------------------------------------------------------
      time = tstart ; when = 0
      call read_inputs
      call init_geometry
      call allocation
      call init_variables
      call check_inputs
      call write_result
      ! ----------------------------------------------------------------

      write(*,*) ''
      write(*,*) '같같같같같같같같같같같같같같같같같같같같같같같같같같�'
      write(*,*) '�              starting iterations ...              �'
      write(*,*) '같같같같같같같같같같같같같같같같같같같같같같같같같같�'
      write(*,*) ''

      ! ----------------------------------------------------------------
      ! Temporal iterations
      ! ----------------------------------------------------------------
      niter = 0 ; when = 1
      do while (niter<nitermax .and. time<time_max)
        niter = niter+1
        call forward
        time = time + dt
        call display
        call write_result
      enddo
      ! ----------------------------------------------------------------

      write(*,*) ''
      write(*,*) '같같같같같같같같같같같같같같같같같같같같같같같같같같�'
      write(*,*) '같같같같같�        Finalisation         같같같같같같�'
      write(*,*) '같같같같같같같같같같같같같같같같같같같같같같같같같같�'
      write(*,*) ''

      ! ----------------------------------------------------------------
      ! Finalisation 
      ! ----------------------------------------------------------------
      when = 2
      print*, 'niter_fin = ', niter
      call write_result
      call deallocation
      ! ----------------------------------------------------------------

      write(*,*) ''
      write(*,*) '같같같같같같같같같같같같같같같같같같같같같같같같같같�'
      write(*,*) '같같같같같같같          End            같같같같같같같'
      write(*,*) '같같같같같같같같같같같같같같같같같같같같같같같같같같�'
      write(*,*) ''

      end program
      ! ****************************************************************





