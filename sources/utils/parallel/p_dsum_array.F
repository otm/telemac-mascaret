!                   ***********************
                    SUBROUTINE P_DSUM_ARRAY
!                   ***********************
!
     &(MYPART, N, VALUES)
!
!***********************************************************************
! PARALLEL   V9P0
!***********************************************************************
!
!brief    Sum values from all processes and distributes the result back
!+        to all processes
!
!~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
!| MYPART       |-->| SEND BUFFER.
!| N            |-->| SIZE IF THE BUFFERS
!| VALUES       |<--| RECV BUFFER.
!~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
!
      USE DECLARATIONS_PARALLEL
      USE DECLARATIONS_SPECIAL
      IMPLICIT NONE
!
!+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
!
      INTEGER, INTENT(IN)  :: N
      DOUBLE PRECISION, INTENT(IN)  :: MYPART(N)
      DOUBLE PRECISION, INTENT(OUT) :: VALUES(N)
!
!+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
!
#if defined HAVE_MPI
      INTEGER IERR
!
!-----------------------------------------------------------------------
!
      IERR = 0
      VALUES = 0
      CALL MPI_ALLREDUCE(MYPART,VALUES,N,MPI_DOUBLE_PRECISION,
     &                   MPI_SUM,COMM,IERR)
!
      IF(IERR.NE.0) THEN
        WRITE(LU,*) 'P_DSUM_ARRAY: ERROR IN MPI_ALLREDUCE'
        WRITE(LU,*) 'MPI ERROR ',IERR
        CALL PLANTE(1)
        STOP
      ENDIF
#else
      WRITE(LU,*)'CALL OF P_DSUM_ARRAY IN ITS VOID VERSION'
      VALUES=MYPART
!
!-----------------------------------------------------------------------
!
#endif
!
!-----------------------------------------------------------------------
!
      RETURN
      END
