! AeroBulk / 2016 / L. Brodeau
!
!   When using AeroBulk to produce scientific work, please acknowledge with the following citation:
!
!   Brodeau, L., B. Barnier, S. Gulev, and C. Woods, 2016: Climatologically
!   significant effects of some approximations in the bulk parameterizations of
!   turbulent air-sea fluxes. J. Phys. Oceanogr., doi:10.1175/JPO-D-16-0169.1.
!
!
MODULE modz_aerobulks

   USE DECLARATIONS_SPECIAL

   IMPLICIT NONE

   INTERFACE pot_temp
      MODULE PROCEDURE pot_temp_vctr, pot_temp_sclr
   END INTERFACE pot_temp

   INTERFACE virt_temp
      MODULE PROCEDURE virt_temp_vctr, virt_temp_sclr
   END INTERFACE virt_temp

   INTERFACE Theta_from_z_P0_T_q
      MODULE PROCEDURE Theta_from_z_P0_T_q_vctr, Theta_from_z_P0_T_q_sclr
   END INTERFACE Theta_from_z_P0_T_q

   INTERFACE visc_air
      MODULE PROCEDURE visc_air_vctr, visc_air_sclr
   END INTERFACE visc_air

   INTERFACE rho_air
      MODULE PROCEDURE rho_air_vctr, rho_air_sclr
   END INTERFACE rho_air

   INTERFACE q_sat
      MODULE PROCEDURE q_sat_vctr, q_sat_sclr
   END INTERFACE q_sat

   INTERFACE e_sat
      MODULE PROCEDURE e_sat_vctr, e_sat_sclr
   END INTERFACE e_sat

   INTERFACE e_sat_ice
      MODULE PROCEDURE e_sat_ice_vctr, e_sat_ice_sclr
   END INTERFACE e_sat_ice

   INTERFACE One_on_L
      MODULE PROCEDURE One_on_L_vctr, One_on_L_sclr
   END INTERFACE One_on_L

   INTERFACE Ri_bulk
      MODULE PROCEDURE Ri_bulk_vctr, Ri_bulk_sclr
   END INTERFACE Ri_bulk

   INTERFACE L_vap
      MODULE PROCEDURE L_vap_vctr, L_vap_sclr
   END INTERFACE L_vap

   INTERFACE cp_air
      MODULE PROCEDURE cp_air_vctr, cp_air_sclr
   END INTERFACE cp_air

   INTERFACE alpha_sw
      MODULE PROCEDURE alpha_sw_vctr, alpha_sw_sclr
   END INTERFACE alpha_sw

   INTERFACE update_qnsol_tau
      MODULE PROCEDURE update_qnsol_tau_vctr, update_qnsol_tau_sclr
   END INTERFACE update_qnsol_tau

   INTERFACE bulk_formula
      MODULE PROCEDURE bulk_formula_vctr, bulk_formula_sclr
   END INTERFACE bulk_formula

   INTERFACE qlw_net
      MODULE PROCEDURE qlw_net_vctr, qlw_net_sclr
   END INTERFACE qlw_net

   INTERFACE psi_m_ecmwf
      MODULE PROCEDURE psi_m_ecmwf_vctr, psi_m_ecmwf_sclr
   END INTERFACE psi_m_ecmwf

   INTERFACE psi_h_ecmwf
      MODULE PROCEDURE psi_h_ecmwf_vctr, psi_h_ecmwf_sclr
   END INTERFACE psi_h_ecmwf

   INTERFACE first_guess_coare
      MODULE PROCEDURE first_guess_coare_vctr, first_guess_coare_sclr
   END INTERFACE first_guess_coare

   INTERFACE psi_m_coare
      MODULE PROCEDURE psi_m_coare_vctr, psi_m_coare_sclr
   END INTERFACE psi_m_coare

   INTERFACE psi_h_coare
      MODULE PROCEDURE psi_h_coare_vctr, psi_h_coare_sclr
   END INTERFACE psi_h_coare

   PUBLIC

   DOUBLE PRECISION, PARAMETER, PRIVATE :: &
                                !! Constants for Goff formula in the presence of ice:
      &      rAg_i = -9.09718D0, &
      &      rBg_i = -3.56654D0, &
      &      rCg_i = 0.876793D0, &
      &      rDg_i = LOG10(6.1071D0)

   DOUBLE PRECISION, PARAMETER, PRIVATE :: rc_louis  = 5.D0
   DOUBLE PRECISION, PARAMETER, PRIVATE :: rc2_louis = rc_louis * rc_louis
   DOUBLE PRECISION, PARAMETER, PRIVATE :: ram_louis = 2.D0 * rc_louis
   DOUBLE PRECISION, PARAMETER, PRIVATE :: rah_louis = 3.D0 * rc_louis

   DOUBLE PRECISION, PARAMETER, PRIVATE :: repsilon = 1.D-6

   ! THINGS THAT NEED TO BE GIVEN A VALUE, anh have the same name and type as in NEMO...
   ! Stupid values here to prevent to ommit to give them a value:

   !! Space dimmension:
   INTEGER,  PARAMETER            :: jpk = 1

   !! Time dimmension:
   INTEGER,  PARAMETER            :: nit000 = 1
   INTEGER,                  SAVE :: nitend = 1 !: time steps (aka # of calls) at which to end

   LOGICAL, SAVE :: l_use_skin_schemes = .FALSE. !: do we use the cool-skin / warm-layer skin schemes ?

   !! Type of air humidity in use:
   CHARACTER(len=2),         SAVE :: ctype_humidity = 'sh' !: Default: spec. humidity [kg/kg ] => 'sh'
   !!                                                      !: * relative humidity         [%]  => 'rh'
   !!                                                      !: * dew-point temperature     [K]  => 'dp'

   DOUBLE PRECISION, DIMENSION(jpk), SAVE :: gdept_1d = (/ 1.D0 /) !: depth at which SST is measured [m]
   DOUBLE PRECISION,                 SAVE :: rdt = 3600. !: time step for the cool-skin/warm-layer parameterization  [s]
   INTEGER,                  SAVE :: nb_iter=5  !: number of itteration in the bulk algorithm

   !! General constants:
   DOUBLE PRECISION, PARAMETER, PUBLIC :: grav  = 9.8D0     !: acceleration of gravity    [m.s^-2]
   DOUBLE PRECISION, PARAMETER, PUBLIC :: rpi   = 3.141592653589793D0
   !!
   DOUBLE PRECISION, PARAMETER, PUBLIC :: roce_alb0  = 0.066D0   !: Default sea surface albedo over ocean when nothing better is available

   !! Physics constants:
   DOUBLE PRECISION, PARAMETER, PUBLIC ::emiss_w = 0.98D0     !: Long-wave (thermal) emissivity of sea-water []
   DOUBLE PRECISION, PARAMETER, PUBLIC ::emiss_i = 0.996D0       !:  "   for ice and snow => but Rees 1993 suggests can be lower in winter on fresh snow... 0.72 ...
   DOUBLE PRECISION, PARAMETER, PUBLIC ::stefan = 5.67D-8 !: Stefan Boltzman constant

   !! Thermodynamics / Water:
   DOUBLE PRECISION, PARAMETER, PUBLIC :: rt0  = 273.15D0      !: freezing point of fresh water [K]
   DOUBLE PRECISION, PARAMETER, PUBLIC :: rtt0 = 273.16D0      !: triple point                  [K]
   !!
   DOUBLE PRECISION, PARAMETER, PUBLIC :: rCp0_w  = 4190.D0    !: Specific heat capacity of seawater (ECMWF 4190) [J/K/kg]
   DOUBLE PRECISION, PARAMETER, PUBLIC :: rho0_w = 1025.D0     !: Density of sea-water  (ECMWF->1025)             [kg/m^3]
   DOUBLE PRECISION, PARAMETER, PUBLIC :: rnu0_w = 1.D-6     !: kinetic viscosity of water                      [m^2/s]
   DOUBLE PRECISION, PARAMETER, PUBLIC :: rk0_w  = 0.6D0       !: thermal conductivity of water (at 20C)          [W/m/K]

   !! Thermodynamics:
   DOUBLE PRECISION, PARAMETER, PUBLIC :: rCp_dry = 1005.0D0   !: Specic heat of dry air, constant pressure      [J/K/kg]
   DOUBLE PRECISION, PARAMETER, PUBLIC :: rCp_vap = 1860.0D0   !: Specic heat of water vapor, constant pressure  [J/K/kg]
   !!
   DOUBLE PRECISION, PARAMETER, PUBLIC :: R_dry = 287.05D0     !: Specific gas constant for dry air              [J/K/kg]
   DOUBLE PRECISION, PARAMETER, PUBLIC :: R_vap = 461.495D0    !: Specific gas constant for water vapor          [J/K/kg]
   DOUBLE PRECISION, PARAMETER, PUBLIC :: R_gas = 8.314510D0   !: Universal molar gas constant                   [J/mol/K]
   !!
   DOUBLE PRECISION, PARAMETER, PUBLIC :: rmm_dryair = 28.9647D-3             !: dry air molar mass / molecular weight     [kg/mol]
   DOUBLE PRECISION, PARAMETER, PUBLIC :: rmm_water  = 18.0153D-3             !: water   molar mass / molecular weight           [kg/mol]
   !!
   DOUBLE PRECISION, PARAMETER, PUBLIC :: rpoiss_dry = R_dry / rCp_dry  !: Poisson constant for dry air
   DOUBLE PRECISION, PARAMETER, PUBLIC :: rgamma_dry = grav  / rCp_dry  !: dry adabiatic lapse rate [K/m]

   DOUBLE PRECISION, PARAMETER, PUBLIC :: reps0 = R_dry/R_vap      !: ratio of gas constant for dry air and water vapor => ~ 0.622
   DOUBLE PRECISION, PARAMETER, PUBLIC :: rctv0 = R_vap/R_dry - 1.D0 !: for virtual temperature (== (1-eps)/eps) => ~ 0.608
   !!
   DOUBLE PRECISION, PARAMETER, PUBLIC :: rLevap = 2.46D6   !: Latent heat of vaporization for sea-water in   [J/kg]
   DOUBLE PRECISION, PARAMETER, PUBLIC :: rLsub  = 2.834D6  !: Latent heat of sublimation for ice at 0 deg.C  [J/kg]

   !! Some defaults:
   DOUBLE PRECISION, PARAMETER, PUBLIC :: Patm  = 101000.D0 !: reference atmospheric pressure at sea-level            [Pa]
   DOUBLE PRECISION, PARAMETER, PUBLIC :: rho0_a = 1.2D0    !: Approx. of density of air                          [kg/m^3]

   !! Bulk model:
   DOUBLE PRECISION, PARAMETER, PUBLIC :: vkarmn  = 0.4D0         !: Von Karman's constant
   DOUBLE PRECISION, PARAMETER, PUBLIC :: vkarmn2 = 0.4D0*0.4D0  !: Von Karman's constant ^2
   DOUBLE PRECISION, PARAMETER, PUBLIC :: rdct_qsat_salt = 0.98D0 !: factor to apply to q_sat(SST) to account for salt in estimation of sat. spec. hum.
   DOUBLE PRECISION, PARAMETER, PUBLIC :: z0_sea_max = 0.0025D0   !: maximum realistic value for roughness length of sea-surface... [m]

   !! Cool-skin warm-layer:
   DOUBLE PRECISION, PARAMETER, PUBLIC :: rcst_cs = -16.D0*9.80665D0*rho0_w*rCp0_w*rnu0_w*rnu0_w*rnu0_w/(rk0_w*rk0_w) !: for cool-skin parameteri$
   !                              => see eq.(14) in Fairall et al. 1996   (eq.(6) of Zeng aand Beljaars is WRONG! (typo?)
   DOUBLE PRECISION, PARAMETER, PUBLIC :: sq_radrw = SQRT(rho0_a/rho0_w)

   DOUBLE PRECISION, PARAMETER, PUBLIC :: Cx_min = 0.1D-3 ! smallest value allowed for bulk transfer coefficients (usually in stable conditions with now wind)

   CHARACTER(len=200), PARAMETER :: cform_err = '(" *** E R R O R :  ")'

   !! For sanity check + guess of type of humidity we need an acceptable min and max
   !! On input field:
   DOUBLE PRECISION, PARAMETER :: ref_sst_min = 270.D0   , ref_sst_max = 320.D0    ! SST [K]
   DOUBLE PRECISION, PARAMETER :: ref_taa_min = 180.D0   , ref_taa_max = 330.D0    ! Absolute air temperature [K]
   DOUBLE PRECISION, PARAMETER :: ref_sha_min = 0.D0     , ref_sha_max = 0.08D0    ! Specific humidity of air [kg/kg]
   DOUBLE PRECISION, PARAMETER :: ref_dpt_min = 150.D0   , ref_dpt_max = 330.D0    ! Dew-point temperature [K]
   DOUBLE PRECISION, PARAMETER :: ref_rlh_min = 0.D0     , ref_rlh_max = 100.D0    ! Relative humidity [%]
   DOUBLE PRECISION, PARAMETER :: ref_slp_min = 80000.D0 , ref_slp_max = 110000.D0 ! Sea-level atmospheric presssure [Pa]
   DOUBLE PRECISION, PARAMETER :: ref_wnd_min = 0.D0     , ref_wnd_max = 50.D0     ! Scalar wind speed [m/s]
   DOUBLE PRECISION, PARAMETER :: ref_rsw_min = 0.D0     , ref_rsw_max = 1500.0D0  ! Downwelling shortwave radiation [W/m^2]
   DOUBLE PRECISION, PARAMETER :: ref_rlw_min = 0.D0     , ref_rlw_max =  750.0D0  ! Downwelling longwave  radiation [W/m^2]

   !! On computed fluxes:
   DOUBLE PRECISION, PARAMETER :: ref_tau_max = 10.D0 ! Wind stress [N/m2]

   !! input variable names within AeroBulk:

   CHARACTER(len=32), PUBLIC :: &
         cv_sst    = 'xxx', & ! SST                    [K]
         cv_patm   = 'xxx', & ! sea-level pressure     [Pa]
         cv_t_air  = 'xxx', & ! absolute temperature   [K]
         cv_q_air  = 'xxx', & ! specific humidity      [kg/kg]
         cv_rh_air = 'xxx', & ! relative humidity      [%]
         cv_dp_air = 'xxx', & ! dew-point temperature  [K]
         cv_wndspd = 'xxx', & ! wind speed module      [m/s]
         cv_u_wnd  = 'xxx', & ! zonal wind speed comp. [m/s]
         cv_v_wnd  = 'xxx', & ! reidional "  "    "    [m/s]
         cv_radsw  = 'xxx', & ! downwelling shortw. rad. [W/m^2]
         cv_radlw  = 'xxx'    ! downwelling longw.  rad. [W/m^2]

   CHARACTER(len=8), PARAMETER :: clbl = 'ECMWF'

   !! ECMWF own values for given constants, taken form IFS documentation...
   DOUBLE PRECISION, PARAMETER, PUBLIC :: charn0_ecmwf = 0.018D0    ! Charnock constant (pretty high value here !!!
   !                                          !    =>  Usually 0.011 for moderate winds)
   DOUBLE PRECISION, PARAMETER ::   zi0     = 1000.D0   ! scale height of the atmospheric boundary layer...1
   DOUBLE PRECISION, PARAMETER ::   Beta0    = 1.D0     ! gustiness parameter ( = 1.25 in COAREv3)
   DOUBLE PRECISION, PARAMETER ::   alpha_M = 0.11D0    ! For roughness length (smooth surface term)
   DOUBLE PRECISION, PARAMETER ::   alpha_H = 0.40D0    ! (Chapter 3, p.34, IFS doc Cy31r1)
   DOUBLE PRECISION, PARAMETER ::   alpha_Q = 0.62D0    !

   !! Warm-layer related arrays:
   DOUBLE PRECISION, ALLOCATABLE, SAVE, DIMENSION(:,:), PUBLIC :: &
                              dT_wl,     &  !: dT due to warm-layer effect => difference between "almost surface (right below viscous layer, z=delta)
                                !!                                     !: and depth of bulk SST (z=gdept_1d(1))
                              Hz_wl         !: depth of warm-layer [m]   ! for now constant for ECMWF...

   DOUBLE PRECISION, PARAMETER, PUBLIC :: rd0  = 3.D0    !: Depth scale [m] of warm layer, "d" in Eq.11 (Zeng & Beljaars 2005)
   DOUBLE PRECISION, PARAMETER         :: zRhoCp_w = rho0_w*rCp0_w
   !
   DOUBLE PRECISION, PARAMETER         :: rNuwl0 = 0.5D0  !: Nu (exponent of temperature profile) Eq.11
   !                                            !: (Zeng & Beljaars 2005) !: set to 0.5 instead of
   !                                            !: 0.3 to respect a warming of +3 K in calm
   !                                            !: condition for the insolation peak of +1000W/m^2
   !!----------------------------------------------------------------------

   PUBLIC :: AEROBULK_MODEL
   PRIVATE :: AEROBULK_INIT, aerobulk_compute
   PRIVATE :: ECMWF_INIT, TURB_ECMWF, psi_m_ecmwf, psi_h_ecmwf
   PRIVATE :: CS_ECMWF, WL_ECMWF

CONTAINS

   SUBROUTINE set_variable_names_default()
      cv_sst    = 'sst'
      cv_patm   = 'msl'
      cv_t_air  = 't_air'
      cv_q_air  = 'q_air'
      cv_rh_air = 'rh_air'
      cv_dp_air = 'dp_air'
      cv_wndspd = 'wndspd'
      cv_u_wnd  = 'u10'
      cv_v_wnd  = 'v10'
      cv_radsw  = 'ssrd'
      cv_radlw  = 'strd'
   END SUBROUTINE set_variable_names_default

   SUBROUTINE set_variable_names_ecmwf()
      cv_sst    = 'sst'
      cv_patm   = 'msl'
      cv_t_air  = 't2m'
      cv_q_air  = 'q2m'
      cv_rh_air = 'rh2m'
      cv_dp_air = 'd2m'
      cv_wndspd = 'wndspd'
      cv_u_wnd  = 'u10'
      cv_v_wnd  = 'v10'
      cv_radsw  = 'ssrd'
      cv_radlw  = 'strd'
   END SUBROUTINE set_variable_names_ecmwf

   SUBROUTINE ctl_stop( cd1, cd2, cd3, cd4, cd5, cd6, cd7, cd8, cd9, cd10 )
      !!----------------------------------------------------------------------
      !!                  ***  ROUTINE  ctl_stop  ***
      !!
      !! ** Purpose :   print in ocean.outpput file a error message and
      !!                increment the error number (nstop) by one.
      !!----------------------------------------------------------------------
      CHARACTER(len=*), INTENT(in), OPTIONAL ::  cd1, cd2, cd3, cd4, cd5
      CHARACTER(len=*), INTENT(in), OPTIONAL ::  cd6, cd7, cd8, cd9, cd10
      INTEGER, PARAMETER :: numout=6
      !!----------------------------------------------------------------------
      !
      !nstop = nstop + 1

      ! force to open ocean.output file
      !IF( numout == 6 ) CALL ctl_opn( numout, 'ocean.output', 'APPEND', 'FORMATTED', 'SEQUENTIAL', -1, 6, .FALSE. )

      WRITE(LU,cform_err)
      IF( PRESENT(cd1 ) )   WRITE(LU,*) TRIM(cd1)
      IF( PRESENT(cd2 ) )   WRITE(LU,*) TRIM(cd2)
      IF( PRESENT(cd3 ) )   WRITE(LU,*) TRIM(cd3)
      IF( PRESENT(cd4 ) )   WRITE(LU,*) TRIM(cd4)
      IF( PRESENT(cd5 ) )   WRITE(LU,*) TRIM(cd5)
      IF( PRESENT(cd6 ) )   WRITE(LU,*) TRIM(cd6)
      IF( PRESENT(cd7 ) )   WRITE(LU,*) TRIM(cd7)
      IF( PRESENT(cd8 ) )   WRITE(LU,*) TRIM(cd8)
      IF( PRESENT(cd9 ) )   WRITE(LU,*) TRIM(cd9)
      IF( PRESENT(cd10) )   WRITE(LU,*) TRIM(cd10)
      WRITE(LU,*) ''
      STOP
      !
   END SUBROUTINE ctl_stop

   !===============================================================================================
   FUNCTION pot_temp_sclr( pTa, pPz,  pPref )
      !!------------------------------------------------------------------------
      !!                           ***  FUNCTION pot_temp  ***
      !!
      !! Poisson's equation to obtain potential temperature from absolute temperature, pressure,
      !! and the reference (sea-level) pressure.
      !!
      !! Air parcel is at height `z` m above sea-level where the pressure is `ppz`,
      !! its absolute temperature is `pTa`.
      !! `pPref` is the reference pressure at sea level aka P0.
      !!
      !! Author: L. Brodeau, June 2021 / AeroBulk
      !!         (https://github.com/brodeau/aerobulk/)
      !!------------------------------------------------------------------------
      DOUBLE PRECISION, INTENT(in)           :: pTa            !: absolute air temperature at `z` m above sea level  [K]
      DOUBLE PRECISION, INTENT(in)           :: pPz            !: pressure at `z` m above sea level                  [Pa]
      DOUBLE PRECISION, INTENT(in), OPTIONAL :: pPref          !: reference pressure (sea-level)                     [Pa]
      DOUBLE PRECISION                       :: pot_temp_sclr  !: potential air temperature at `z` m above sea level [K]
      !!
      DOUBLE PRECISION :: zPref = Patm
      !!-------------------------------------------------------------------
      IF( PRESENT(pPref) ) zPref = pPref
      pot_temp_sclr = pTa * ( zPref / pPz )**rpoiss_dry
      !!
   END FUNCTION pot_temp_sclr

   FUNCTION pot_temp_vctr( pTa, pPz,  pPref )
      DOUBLE PRECISION, DIMENSION(:,:), INTENT(in)           :: pTa
      DOUBLE PRECISION, DIMENSION(:,:), INTENT(in)           :: pPz
      DOUBLE PRECISION, DIMENSION(:,:), INTENT(in), OPTIONAL :: pPref
      DOUBLE PRECISION, DIMENSION(SIZE(pTa,1),SIZE(pTa,2))                       :: pot_temp_vctr
      !!-------------------------------------------------------------------
      IF( PRESENT(pPref) ) THEN
         pot_temp_vctr = pTa * ( pPref / pPz )**rpoiss_dry
      ELSE
         pot_temp_vctr = pTa * ( Patm  / pPz )**rpoiss_dry
      END IF
   END FUNCTION pot_temp_vctr
   !===============================================================================================


   !===============================================================================================
   FUNCTION virt_temp_sclr( pTa, pqa )
      !!------------------------------------------------------------------------
      !!
      !! Compute the (absolute/potential) VIRTUAL temperature, based on the
      !! (absolute/potential) temperature and specific humidity
      !!
      !! If input temperature is absolute then output virtual temperature is absolute
      !! If input temperature is potential then output virtual temperature is potential
      !!
      !! Author: L. Brodeau, June 2019 / AeroBulk
      !!         (https://github.com/brodeau/aerobulk/)
      !!------------------------------------------------------------------------
      DOUBLE PRECISION             :: virt_temp_sclr !: virtual temperature [K]
      DOUBLE PRECISION, INTENT(in) :: pTa       !: absolute or potential air temperature [K]
      DOUBLE PRECISION, INTENT(in) :: pqa       !: specific humidity of air   [kg/kg]
      !!-------------------------------------------------------------------
      virt_temp_sclr = pTa * (1.D0 + rctv0*pqa)
      !!
      !! This is exactly the same thing as:
      !! virt_temp_sclr = pTa * ( pwa + reps0) / (reps0*(1.+pwa))
      !! with wpa (mixing ration) defined as : pwa = pqa/(1.-pqa)
      !!
   END FUNCTION virt_temp_sclr
   !!
   FUNCTION virt_temp_vctr( pTa, pqa )
      DOUBLE PRECISION, DIMENSION(:,:), INTENT(in) :: pTa !: absolute or potential air temperature [K]
      DOUBLE PRECISION, DIMENSION(:,:), INTENT(in) :: pqa !: specific humidity of air   [kg/kg]
      DOUBLE PRECISION, DIMENSION(SIZE(pTa,1),SIZE(pTa,2))             :: virt_temp_vctr !: virtual temperature [K]
      virt_temp_vctr(:,:) = pTa(:,:) * (1.D0 + rctv0*pqa(:,:))
   END FUNCTION virt_temp_vctr
   !===============================================================================================

   !===============================================================================================
   FUNCTION Pz_from_P0_tz_qz_sclr( pz, pslp, pTa, pqa,  l_ice )
      !!-------------------------------------------------------------------------------
      !!                           ***  FUNCTION Pz_from_P0_tz_qz  ***
      !!
      !! ** Purpose : compute air pressure at height `pz` m above sea level,
      !!              based barometric equation, from absolute air temperature
      !!              and specific humidity at height `pz` and sea-level pressure
      !!
      !! ** Author: G. Samson,  Feb  2021
      !!            L. Brodeau, June 2021
      !!-------------------------------------------------------------------------------
      DOUBLE PRECISION, INTENT(in)           :: pz               ! height above sea-level             [m]
      DOUBLE PRECISION, INTENT(in)           :: pslp             ! pressure at sea-level (z=0)        [Pa]
      DOUBLE PRECISION, INTENT(in)           :: pTa              ! air abs. temperature at z=`pz`     [K]
      DOUBLE PRECISION, INTENT(in)           :: pqa              ! air specific humidity at z=`pz`    [kg/kg]
      LOGICAL , INTENT(in), OPTIONAL :: l_ice            ! sea-ice presence
      DOUBLE PRECISION                       :: Pz_from_P0_tz_qz_sclr  ! pressure at `pz` m above sea level [Pa]
      !!
      DOUBLE PRECISION           :: zpa, zxm, zqsat, zf
      INTEGER            :: it
      LOGICAL            :: lice = .FALSE. ! sea-ice presence
      INTEGER, PARAMETER :: niter = 3      ! iteration indice and number
      !!-------------------------------------------------------------------------------
      IF( PRESENT(l_ice) ) lice = l_ice
      !!
      zpa = pslp              ! first guess of air pressure at zt   [Pa]
      DO it = 1, niter
         zqsat = q_sat( pTa, zpa, l_ice=lice )                               ! saturation specific humidity [kg/kg]
         zf    = pqa/zqsat
         zxm   = (1.D0 - zf) * rmm_dryair + zf * rmm_water    ! moist air molar mass [kg/mol]
         zpa   = pslp * EXP( -grav * zxm * pz / ( R_gas * pTa ) )
      END DO
      !!
      Pz_from_P0_tz_qz_sclr = zpa
      !!
   END FUNCTION Pz_from_P0_tz_qz_sclr

   FUNCTION Pz_from_P0_tz_qz_vctr( pz, pslp, pTa, pqa,  l_ice )
      DOUBLE PRECISION,                     INTENT(in) :: pz               ! height above sea-level             [m]
      DOUBLE PRECISION, DIMENSION(:,:), INTENT(in) :: pslp             ! pressure at sea-level (z=0)        [Pa]
      DOUBLE PRECISION, DIMENSION(:,:), INTENT(in) :: pTa              ! air abs. temperature at z=`pz`     [K]
      DOUBLE PRECISION, DIMENSION(:,:), INTENT(in) :: pqa              ! air specific humidity at z=`pz`    [kg/kg]
      LOGICAL , OPTIONAL          , INTENT(in) :: l_ice            ! sea-ice presence
      DOUBLE PRECISION, DIMENSION(SIZE(pTa,1),SIZE(pTa,2))             :: Pz_from_P0_tz_qz_vctr  ! pressure at `pz` m above sea level [Pa]
      !!
      INTEGER :: ji, jj         ! loop indices
      LOGICAL :: lice = .FALSE. ! presence of ice ?
      !!
      IF( PRESENT(l_ice) ) lice = l_ice
      DO jj = 1, SIZE(pTa,2)
         DO ji = 1, SIZE(pTa,1)
            Pz_from_P0_tz_qz_vctr(ji,jj) = Pz_from_P0_tz_qz_sclr( pz, pslp(ji,jj), pTa(ji,jj), pqa(ji,jj),  l_ice=lice )
         END DO
      END DO
   END FUNCTION Pz_from_P0_tz_qz_vctr

   !===============================================================================================

   !===============================================================================================
   FUNCTION Theta_from_z_P0_T_q_sclr( pz, pslp, pTa, pqa )
      !!-------------------------------------------------------------------------------
      !!                           ***  FUNCTION Theta_from_z_P0_T_q  ***
      !!
      !! ** Purpose : Converts absolute temperature at height `pz` to potential temperature,
      !!              using sea-level pressure and specific humidity at heaight `pz`
      !!
      !! ** Author: G. Samson,  Feb  2021
      !!            L. Brodeau, June 2021
      !!-------------------------------------------------------------------------------
      DOUBLE PRECISION, INTENT(in) :: pz               ! height above sea-level             [m]
      DOUBLE PRECISION, INTENT(in) :: pslp             ! pressure at sea-level (z=0)        [Pa]
      DOUBLE PRECISION, INTENT(in) :: pTa              ! air abs. temperature at z=`pz`     [K]
      DOUBLE PRECISION, INTENT(in) :: pqa              ! air specific humidity at z=`pz`    [kg/kg]
      DOUBLE PRECISION             :: Theta_from_z_P0_T_q_sclr ! air pot. temperature at z=`pz` [K]
      !!
      DOUBLE PRECISION           :: zPz
      !!-------------------------------------------------------------------------------
      zPz = Pz_from_P0_tz_qz_sclr( pz, pslp, pTa, pqa ) ! pressure at z=`pz`
      !!
      Theta_from_z_P0_T_q_sclr = pot_temp_sclr( pTa, zPz,  pPref=pslp )
      !!
   END FUNCTION Theta_from_z_P0_T_q_sclr

   FUNCTION Theta_from_z_P0_T_q_vctr( pz, pslp, pTa, pqa )
      DOUBLE PRECISION,                     INTENT(in) :: pz
      DOUBLE PRECISION, DIMENSION(:,:), INTENT(in) :: pslp
      DOUBLE PRECISION, DIMENSION(:,:), INTENT(in) :: pTa
      DOUBLE PRECISION, DIMENSION(:,:), INTENT(in) :: pqa
      DOUBLE PRECISION, DIMENSION(SIZE(pTa,1),SIZE(pTa,2))             :: Theta_from_z_P0_T_q_vctr
      !!-------------------------------------------------------------------------------
      Theta_from_z_P0_T_q_vctr = pot_temp_vctr( pTa, Pz_from_P0_tz_qz_vctr( pz, pslp, pTa, pqa ),  pPref=pslp )
   END FUNCTION Theta_from_z_P0_T_q_vctr
   !===============================================================================================

   !===============================================================================================
   FUNCTION rho_air_sclr( pTa, pqa, pslp )
      !!-------------------------------------------------------------------------------
      !!                           ***  FUNCTION rho_air_sclr  ***
      !!
      !! ** Purpose : compute density of (moist) air using the eq. of state of the atmosphere
      !!
      !! ** Author: L. Brodeau, June 2016 / AeroBulk (https://github.com/brodeau/aerobulk/)
      !!-------------------------------------------------------------------------------
      DOUBLE PRECISION, INTENT(in) :: pTa           ! absolute air temperature             [K]
      DOUBLE PRECISION, INTENT(in) :: pqa            ! air specific humidity   [kg/kg]
      DOUBLE PRECISION, INTENT(in) :: pslp           ! pressure in                [Pa]
      DOUBLE PRECISION             :: rho_air_sclr   ! density of moist air   [kg/m^3]
      !!-------------------------------------------------------------------------------
      rho_air_sclr = MAX( pslp / (R_dry*pTa * ( 1.D0 + rctv0*pqa )) , 0.8D0 )
      !!
   END FUNCTION rho_air_sclr
   !!
   FUNCTION rho_air_vctr( pTa, pqa, pslp )
      DOUBLE PRECISION, DIMENSION(:,:), INTENT(in) :: pTa         ! absolute air temperature             [K]
      DOUBLE PRECISION, DIMENSION(:,:), INTENT(in) :: pqa          ! air specific humidity   [kg/kg]
      DOUBLE PRECISION, DIMENSION(:,:), INTENT(in) :: pslp          ! pressure in                [Pa]
      DOUBLE PRECISION, DIMENSION(SIZE(pTa,1),SIZE(pTa,2))             :: rho_air_vctr ! density of moist air   [kg/m^3]
      !!-------------------------------------------------------------------------------
      rho_air_vctr = MAX( pslp / (R_dry*pTa * ( 1.D0 + rctv0*pqa )) , 0.8D0 )
   END FUNCTION rho_air_vctr

   !===============================================================================================
   FUNCTION visc_air_sclr(pTa)
      !!----------------------------------------------------------------------------------
      !! Air kinetic viscosity (m^2/s) given from air temperature in Kelvin
      !!
      !! ** Author: L. Brodeau, june 2016 / AeroBulk (https://github.com/brodeau/aerobulk/)
      !!----------------------------------------------------------------------------------
      DOUBLE PRECISION             :: visc_air_sclr   ! kinetic viscosity (m^2/s)
      DOUBLE PRECISION, INTENT(in) :: pTa       ! absolute air temperature in [K]
      DOUBLE PRECISION ::   ztc, ztc2   ! local scalar
      !!----------------------------------------------------------------------------------
      ztc  = pTa - rt0   ! absolute air temp, in deg. C
      ztc2 = ztc*ztc
      visc_air_sclr = 1.326D-5*(1.D0 + 6.542D-3*ztc + 8.301D-6*ztc2 - 4.84D-9*ztc2*ztc)
      !!
   END FUNCTION visc_air_sclr
   !!
   FUNCTION visc_air_vctr(pTa)
      DOUBLE PRECISION, DIMENSION(:,:), INTENT(in) ::   pTa       ! absolute air temperature in [K]
      DOUBLE PRECISION, DIMENSION(SIZE(pTa,1),SIZE(pTa,2))             ::   visc_air_vctr   ! kinetic viscosity (m^2/s)
      INTEGER  ::   ji, jj      ! dummy loop indices
      DO jj = 1, SIZE(pTa,2)
         DO ji = 1, SIZE(pTa,1)
            visc_air_vctr(ji,jj) = visc_air_sclr( pTa(ji,jj) )
         END DO
      END DO
   END FUNCTION visc_air_vctr
   !===============================================================================================

   !===============================================================================================
   FUNCTION L_vap_sclr( psst )
      !!---------------------------------------------------------------------------------
      !!                           ***  FUNCTION L_vap_sclr  ***
      !!
      !! ** Purpose : Compute the latent heat of vaporization of water from temperature
      !!
      !! ** Author: L. Brodeau, june 2016 / AeroBulk (https://github.com/brodeau/aerobulk/)
      !!----------------------------------------------------------------------------------
      DOUBLE PRECISION             :: L_vap_sclr  ! latent heat of vaporization   [J/kg]
      DOUBLE PRECISION, INTENT(in) :: psst        ! water temperature               [K]
      !!----------------------------------------------------------------------------------
      L_vap_sclr = (  2.501D0 - 0.00237D0 * ( psst - rt0)  ) * 1.D6
      !!
   END FUNCTION L_vap_sclr

   FUNCTION L_vap_vctr( psst )
      DOUBLE PRECISION, DIMENSION(:,:), INTENT(in) :: psst        ! water temperature             [K]
      DOUBLE PRECISION, DIMENSION(SIZE(psst,1),SIZE(psst,2))             :: L_vap_vctr  ! latent heat of vaporization [J/kg]
      L_vap_vctr = (  2.501D0 - 0.00237D0 * ( psst(:,:) - rt0)  ) * 1.D6
   END FUNCTION L_vap_vctr
   !===============================================================================================

   !===============================================================================================
   FUNCTION cp_air_sclr( pqa )
      !!-------------------------------------------------------------------------------
      !!                           ***  FUNCTION cp_air_sclr  ***
      !!
      !! ** Purpose : Compute specific heat (Cp) of moist air
      !!
      !! ** Author: L. Brodeau, june 2016 / AeroBulk (https://github.com/brodeau/aerobulk/)
      !!-------------------------------------------------------------------------------
      DOUBLE PRECISION, INTENT(in) :: pqa           ! air specific humidity         [kg/kg]
      DOUBLE PRECISION             :: cp_air_sclr   ! specific heat of moist air   [J/K/kg]
      !!-------------------------------------------------------------------------------
      cp_air_sclr = rCp_dry + rCp_vap * pqa
      !!
   END FUNCTION cp_air_sclr
   !!
   FUNCTION cp_air_vctr( pqa )
      DOUBLE PRECISION, DIMENSION(:,:), INTENT(in) :: pqa      ! air specific humidity         [kg/kg]
      DOUBLE PRECISION, DIMENSION(SIZE(pqa,1),SIZE(pqa,2))             :: cp_air_vctr   ! specific heat of moist air   [J/K/kg]
      cp_air_vctr = rCp_dry + rCp_vap * pqa
   END FUNCTION cp_air_vctr
   !===============================================================================================

   !===============================================================================================
   FUNCTION One_on_L_sclr( pThta, pqa, pus, pts, pqs )
      !!------------------------------------------------------------------------
      !!
      !! Evaluates the 1./(Obukhov length) from air temperature,
      !! air specific humidity, and frictional scales u*, t* and q*
      !!
      !! Author: L. Brodeau, June 2019 / AeroBulk
      !!         (https://github.com/brodeau/aerobulk/)
      !!------------------------------------------------------------------------
      DOUBLE PRECISION             :: One_on_L_sclr !: 1./(Obukhov length) [m^-1]
      DOUBLE PRECISION, INTENT(in) :: pThta     !: reference potential temperature of air [K]
      DOUBLE PRECISION, INTENT(in) :: pqa      !: reference specific humidity of air   [kg/kg]
      DOUBLE PRECISION, INTENT(in) :: pus      !: u*: friction velocity [m/s]
      DOUBLE PRECISION, INTENT(in) :: pts, pqs !: \theta* and q* friction aka turb. scales for temp. and spec. hum.
      DOUBLE PRECISION ::     zqa          ! local scalar
      !!-------------------------------------------------------------------
      zqa = (1.D0 + rctv0*pqa)
      !
      ! The main concern is to know whether, the vertical turbulent flux of virtual temperature, < u' theta_v' > is estimated with:
      !  a/  -u* [ theta* (1 + 0.61 q) + 0.61 theta q* ] => this is the one that seems correct! chose this one!
      !                      or
      !  b/  -u* [ theta*              + 0.61 theta q* ]
      !
      One_on_L_sclr = grav*vkarmn*( pts*zqa + rctv0*pThta*pqs ) / MAX( pus*pus*pThta*zqa , 1.D-9 )
      !
      One_on_L_sclr = SIGN( MIN(ABS(One_on_L_sclr),200.D0), One_on_L_sclr ) ! (prevent FPE from stupid values over masked regions...)
      !!
   END FUNCTION One_on_L_sclr
   !!
   FUNCTION One_on_L_vctr( pThta, pqa, pus, pts, pqs )
      DOUBLE PRECISION, DIMENSION(:,:), INTENT(in) :: pThta     !: reference potential temperature of air [K]
      DOUBLE PRECISION, DIMENSION(:,:), INTENT(in) :: pqa      !: reference specific humidity of air   [kg/kg]
      DOUBLE PRECISION, DIMENSION(:,:), INTENT(in) :: pus      !: u*: friction velocity [m/s]
      DOUBLE PRECISION, DIMENSION(:,:), INTENT(in) :: pts, pqs !: \theta* and q* friction aka turb. scales for temp. and spec. hum.
      DOUBLE PRECISION, DIMENSION(SIZE(pThta,1),SIZE(pThta,2))             :: One_on_L_vctr !: 1./(Obukhov length) [m^-1]
      INTEGER  ::   ji, jj         ! dummy loop indices
      !!-------------------------------------------------------------------
      DO jj = 1, SIZE(pThta,2)
         DO ji = 1, SIZE(pThta,1)
            One_on_L_vctr(ji,jj) = One_on_L_sclr( pThta(ji,jj), pqa(ji,jj), pus(ji,jj), pts(ji,jj), pqs(ji,jj) )
         END DO
      END DO
   END FUNCTION One_on_L_vctr
   !===============================================================================================

   !===============================================================================================
   FUNCTION Ri_bulk_sclr( pz, psst, pThta, pssq, pqa, pub,  pTa_layer, pqa_layer )
      !!----------------------------------------------------------------------------------
      !! Bulk Richardson number according to "wide-spread equation"...
      !!
      !!    Reminder: the Richardson number is the ratio "buoyancy" / "shear"
      !!
      !! ** Author: L. Brodeau, June 2019 / AeroBulk (https://github.com/brodeau/aerobulk/)
      !!----------------------------------------------------------------------------------
      DOUBLE PRECISION             :: Ri_bulk_sclr
      DOUBLE PRECISION, INTENT(in) :: pz    ! height above the sea (aka "delta z")  [m]
      DOUBLE PRECISION, INTENT(in) :: psst  ! SST                                   [K]
      DOUBLE PRECISION, INTENT(in) :: pThta  ! pot. air temp. at height "pz"         [K]
      DOUBLE PRECISION, INTENT(in) :: pssq  ! 0.98*q_sat(SST)                   [kg/kg]
      DOUBLE PRECISION, INTENT(in) :: pqa   ! air spec. hum. at height "pz"     [kg/kg]
      DOUBLE PRECISION, INTENT(in) :: pub   ! bulk wind speed                     [m/s]
      DOUBLE PRECISION, INTENT(in), OPTIONAL :: pTa_layer ! when possible, a better guess of absolute temperature WITHIN the layer [K]
      DOUBLE PRECISION, INTENT(in), OPTIONAL :: pqa_layer ! when possible, a better guess of specific humidity    WITHIN the layer [kg/kg]
      LOGICAL  :: l_ptqa_l_prvd = .FALSE.
      DOUBLE PRECISION :: zdthv, ztv, zsstv  ! local scalars
      !!-------------------------------------------------------------------
      IF( PRESENT(pTa_layer) .AND. PRESENT(pqa_layer) ) l_ptqa_l_prvd=.TRUE.
      !
      zsstv = virt_temp_sclr( psst, pssq )          ! virtual SST (absolute==potential because z=0!)
      !
      zdthv = virt_temp_sclr( pThta, pqa  ) - zsstv  ! air-sea delta of "virtual potential temperature"
      !
      !! ztv: estimate of the ABSOLUTE virtual temp. within the layer
      IF( l_ptqa_l_prvd ) THEN
         ztv = virt_temp_sclr( pTa_layer, pqa_layer )
      ELSE
         ztv = 0.5D0*( zsstv + virt_temp_sclr( pThta-rgamma_dry*pz, pqa ) )
      END IF
      !
      Ri_bulk_sclr = grav*zdthv*pz / ( ztv*pub*pub )      ! the usual definition of Ri_bulk_sclr
      !!
   END FUNCTION Ri_bulk_sclr
   !!
   FUNCTION Ri_bulk_vctr( pz, psst, pThta, pssq, pqa, pub,  pTa_layer, pqa_layer )
      DOUBLE PRECISION                    , INTENT(in) :: pz
      DOUBLE PRECISION, DIMENSION(:,:), INTENT(in) :: psst
      DOUBLE PRECISION, DIMENSION(:,:), INTENT(in) :: pThta
      DOUBLE PRECISION, DIMENSION(:,:), INTENT(in) :: pssq
      DOUBLE PRECISION, DIMENSION(:,:), INTENT(in) :: pqa
      DOUBLE PRECISION, DIMENSION(:,:), INTENT(in) :: pub
      DOUBLE PRECISION, DIMENSION(:,:), INTENT(in), OPTIONAL :: pTa_layer
      DOUBLE PRECISION, DIMENSION(:,:), INTENT(in), OPTIONAL :: pqa_layer
      DOUBLE PRECISION, DIMENSION(SIZE(psst,1),SIZE(psst,2))             :: Ri_bulk_vctr
      LOGICAL  :: l_ptqa_l_prvd = .FALSE.
      INTEGER  ::   ji, jj
      IF( PRESENT(pTa_layer) .AND. PRESENT(pqa_layer) ) l_ptqa_l_prvd=.TRUE.
      DO jj = 1, SIZE(psst,2)
         DO ji = 1, SIZE(psst,1)
            IF( l_ptqa_l_prvd ) THEN
               Ri_bulk_vctr(ji,jj) = Ri_bulk_sclr( pz, psst(ji,jj), pThta(ji,jj), pssq(ji,jj), pqa(ji,jj), pub(ji,jj), &
                  &                                pTa_layer=pTa_layer(ji,jj ),  pqa_layer=pqa_layer(ji,jj ) )
            ELSE
               Ri_bulk_vctr(ji,jj) = Ri_bulk_sclr( pz, psst(ji,jj), pThta(ji,jj), pssq(ji,jj), pqa(ji,jj), pub(ji,jj) )
            END IF
         END DO
      END DO
   END FUNCTION Ri_bulk_vctr
   !===============================================================================================

   !===============================================================================================
   FUNCTION e_sat_sclr( pTa )
      !!----------------------------------------------------------------------------------
      !!                   ***  FUNCTION e_sat_sclr  ***
      !!                  < SCALAR argument version >
      !! ** Purpose : water vapor at saturation in [Pa]
      !!              Based on accurate estimate by Goff, 1957
      !!
      !! ** Author: L. Brodeau, june 2016 / AeroBulk (https://github.com/brodeau/aerobulk/)
      !!
      !!    Note: what rt0 should be here, is 273.16 (triple point of water) and not 273.15 like here
      !!----------------------------------------------------------------------------------
      DOUBLE PRECISION             ::   e_sat_sclr   ! water vapor at saturation   [kg/kg]
      DOUBLE PRECISION, INTENT(in) ::   pTa    ! absolute air temperature                  [K]
      DOUBLE PRECISION ::   zta, ztmp   ! local scalar
      !!----------------------------------------------------------------------------------
      zta = MAX( pTa , 180.D0 )   ! absolute air temp., prevents fpe0 errors dute to unrealistically low values over masked regions...
      ztmp = rt0 / zta   !LOLO: rt0 or rtt0 ???? (273.15 vs 273.16 )
      !
      ! Vapour pressure at saturation [Pa] : WMO, (Goff, 1957)
      e_sat_sclr = 100.D0*( 10.D0**( 10.79574D0*(1.D0 - ztmp) - 5.028D0*LOG10(zta/rt0)        &
             + 1.50475D0*10.D0**(-4)*(1.D0 - 10.D0**(-8.2969D0*(zta/rt0 - 1.D0)) )  &
             + 0.42873D0*10.D0**(-3)*(10.D0**(4.76955D0*(1.D0 - ztmp)) - 1.D0) + 0.78614D0) )
      !
   END FUNCTION e_sat_sclr
   !!
   FUNCTION e_sat_vctr(pTa)
      DOUBLE PRECISION, DIMENSION(:,:), INTENT(in) :: pTa    !: temperature [K]
      DOUBLE PRECISION, DIMENSION(SIZE(pTa,1),SIZE(pTa,2))             :: e_sat_vctr !: vapour pressure at saturation  [Pa]
      INTEGER  ::   ji, jj         ! dummy loop indices
      DO jj = 1, SIZE(pTa,2)
         DO ji = 1, SIZE(pTa,1)
            e_sat_vctr(ji,jj) = e_sat_sclr(pTa(ji,jj))
         END DO
      END DO
   END FUNCTION e_sat_vctr
   !===============================================================================================

   !===============================================================================================
   FUNCTION e_sat_ice_sclr(pTa)
      !!---------------------------------------------------------------------------------
      !! Same as "e_sat" but over ice rather than water!
      !!---------------------------------------------------------------------------------
      DOUBLE PRECISION             :: e_sat_ice_sclr !: vapour pressure at saturation in presence of ice [Pa]
      DOUBLE PRECISION, INTENT(in) :: pTa
      !!
      DOUBLE PRECISION :: zta, zle, ztmp
      !!---------------------------------------------------------------------------------
      zta = MAX( pTa , 180.D0 )   ! absolute air temp., prevents fpe0 errors dute to unrealistically low values over masked regions...
      ztmp = rtt0/zta
      !!
      zle  = rAg_i*(ztmp - 1.D0) + rBg_i*LOG10(ztmp) + rCg_i*(1.D0 - zta/rtt0) + rDg_i
      !!
      e_sat_ice_sclr = 100.D0 * 10.D0**zle
   END FUNCTION e_sat_ice_sclr
   !!
   FUNCTION e_sat_ice_vctr(pTa)
      !! Same as "e_sat" but over ice rather than water!
      DOUBLE PRECISION, DIMENSION(:,:), INTENT(in) :: pTa
      DOUBLE PRECISION, DIMENSION(SIZE(pTa,1),SIZE(pTa,2)) :: e_sat_ice_vctr !: vapour pressure at saturation in presence of ice [Pa]
      INTEGER  :: ji, jj
      !!----------------------------------------------------------------------------------
      DO jj = 1, SIZE(pTa,2)
         DO ji = 1, SIZE(pTa,1)
            e_sat_ice_vctr(ji,jj) = e_sat_ice_sclr( pTa(ji,jj) )
         END DO
      END DO
   END FUNCTION e_sat_ice_vctr
   !!
   !===============================================================================================

   !===============================================================================================
   FUNCTION q_sat_sclr( pTa, pslp,  l_ice )
      !!---------------------------------------------------------------------------------
      !!                           ***  FUNCTION q_sat_sclr  ***
      !!
      !! ** Purpose : Conputes specific humidity of air at saturation
      !!
      !! ** Author: L. Brodeau, june 2016 / AeroBulk (https://github.com/brodeau/aerobulk/)
      !!----------------------------------------------------------------------------------
      DOUBLE PRECISION :: q_sat_sclr
      DOUBLE PRECISION, INTENT(in) :: pTa  !: absolute temperature of air [K]
      DOUBLE PRECISION, INTENT(in) :: pslp  !: atmospheric pressure        [Pa]
      LOGICAL,  INTENT(in), OPTIONAL :: l_ice  !: we are above ice
      DOUBLE PRECISION :: ze_s
      LOGICAL  :: lice
      !!----------------------------------------------------------------------------------
      lice = .FALSE.
      IF( PRESENT(l_ice) ) lice = l_ice
      IF( lice ) THEN
         ze_s = e_sat_ice( pTa )
      ELSE
         ze_s = e_sat( pTa ) ! Vapour pressure at saturation (Goff) :
      END IF
      q_sat_sclr = reps0*ze_s/(pslp - (1.D0 - reps0)*ze_s)
   END FUNCTION q_sat_sclr
   !!
   FUNCTION q_sat_vctr( pTa, pslp,  l_ice )
      DOUBLE PRECISION, DIMENSION(:,:), INTENT(in) :: pTa  !: absolute temperature of air [K]
      DOUBLE PRECISION, DIMENSION(:,:), INTENT(in) :: pslp  !: atmospheric pressure        [Pa]
      LOGICAL,  INTENT(in), OPTIONAL :: l_ice  !: we are above ice
      DOUBLE PRECISION, DIMENSION(SIZE(pTa,1),SIZE(pTa,2)) :: q_sat_vctr
      LOGICAL  :: lice
      INTEGER  :: ji, jj
      !!----------------------------------------------------------------------------------
      lice = .FALSE.
      IF( PRESENT(l_ice) ) lice = l_ice
      DO jj = 1, SIZE(pTa,2)
         DO ji = 1, SIZE(pTa,1)
            q_sat_vctr(ji,jj) = q_sat_sclr( pTa(ji,jj) , pslp(ji,jj), l_ice=lice )
         END DO
      END DO
   END FUNCTION q_sat_vctr
   !===============================================================================================

   !===============================================================================================
   FUNCTION q_air_rh(prha, pTa, pslp)
      !!----------------------------------------------------------------------------------
      !! Specific humidity of air out of Relative Humidity
      !!
      !! ** Author: L. Brodeau, june 2016 / AeroBulk (https://github.com/brodeau/aerobulk/)
      !!----------------------------------------------------------------------------------
      DOUBLE PRECISION, DIMENSION(:,:), INTENT(in) :: prha        !: relative humidity       [%]
      DOUBLE PRECISION, DIMENSION(:,:), INTENT(in) :: pTa         !: absolute air temperature [K]
      DOUBLE PRECISION, DIMENSION(:,:), INTENT(in) :: pslp        !: atmospheric pressure    [Pa]
      DOUBLE PRECISION, DIMENSION(SIZE(pTa,1),SIZE(pTa,2))             :: q_air_rh
      !
      INTEGER  ::   ji, jj      ! dummy loop indices
      DOUBLE PRECISION ::   ze      ! local scalar
      !!----------------------------------------------------------------------------------
      !
      DO jj = 1, SIZE(pTa,2)
         DO ji = 1, SIZE(pTa,1)
            ze = 0.01D0*prha(ji,jj)*e_sat_sclr(pTa(ji,jj))
            q_air_rh(ji,jj) = ze*reps0/MAX( pslp(ji,jj) - (1.D0 - reps0)*ze , 1.D0 )
         END DO
      END DO
      !
   END FUNCTION q_air_rh

   FUNCTION q_air_dp(da, slp)
      !!
      !! Air specific humidity from dew point temperature
      DOUBLE PRECISION, DIMENSION(:,:), INTENT(in) :: da   !: dew-point temperature   [K]
      DOUBLE PRECISION, DIMENSION(:,:), INTENT(in) :: slp  !: atmospheric pressure    [Pa]
      DOUBLE PRECISION, DIMENSION(SIZE(da,1),SIZE(da,2)) :: q_air_dp  !: kg/kg
      !!
      q_air_dp = MAX( e_sat(da) , 0.D0 ) ! q_air_dp is e_sat !!!
      q_air_dp = q_air_dp*reps0/MAX( slp - (1.D0 - reps0)*q_air_dp, 1.D0 )  ! MAX() are solely here to prevent NaN
      !!                                                                   ! over masked regions with silly values
   END FUNCTION q_air_dp

   !===============================================================================================
   SUBROUTINE UPDATE_QNSOL_TAU_SCLR( pzu, pts, pqs, pThta, pqa, pust, ptst, pqst, pwnd, pUb, pslp, prlw, &
                                    pQns, pTau,    Qlat )
      !!----------------------------------------------------------------------------------
      !! Purpose: returns the non-solar heat flux to the ocean aka "Qlat + Qsen + Qlw"
      !!          and the module of the wind stress => pTau = Tau
      !! ** Author: L. Brodeau, Sept. 2019 / AeroBulk (https://github.com/brodeau/aerobulk/)
      !!----------------------------------------------------------------------------------
      DOUBLE PRECISION, INTENT(in)  :: pzu  ! height above the sea-level where all this takes place (normally 10m)
      DOUBLE PRECISION, INTENT(in)  :: pts  ! water temperature at the air-sea interface [K]
      DOUBLE PRECISION, INTENT(in)  :: pqs  ! satur. spec. hum. at T=pts   [kg/kg]
      DOUBLE PRECISION, INTENT(in)  :: pThta ! potential air temperature at z=pzu [K]
      DOUBLE PRECISION, INTENT(in)  :: pqa  ! specific humidity at z=pzu [kg/kg]
      DOUBLE PRECISION, INTENT(in)  :: pust ! u*
      DOUBLE PRECISION, INTENT(in)  :: ptst ! t*
      DOUBLE PRECISION, INTENT(in)  :: pqst ! q*
      DOUBLE PRECISION, INTENT(in)  :: pwnd ! wind speed module at z=pzu [m/s]
      DOUBLE PRECISION, INTENT(in)  :: pUb  ! bulk wind speed at z=pzu (inc. pot. effect of gustiness etc) [m/s]
      DOUBLE PRECISION, INTENT(in)  :: pslp ! sea-level atmospheric pressure [Pa]
      DOUBLE PRECISION, INTENT(in)  :: prlw ! downwelling longwave radiative flux [W/m^2]
      !
      DOUBLE PRECISION, INTENT(out) :: pQns ! non-solar heat flux to the ocean aka "Qlat + Qsen + Qlw" [W/m^2]]
      DOUBLE PRECISION, INTENT(out) :: pTau ! module of the wind stress [N/m^2]
      !
      DOUBLE PRECISION, OPTIONAL, INTENT(out) :: Qlat
      !
      DOUBLE PRECISION :: zdt, zdq, zCd, zCh, zCe, zz0, zQlat, zQsen, zQlw
      !!----------------------------------------------------------------------------------
      zdt = pThta - pts ;  zdt = SIGN( MAX(ABS(zdt),1.D-6), zdt )
      zdq = pqa - pqs ;  zdq = SIGN( MAX(ABS(zdq),1.D-9), zdq )
      zz0 = pust/pUb
      zCd = zz0*zz0
      zCh = zz0*ptst/zdt
      zCe = zz0*pqst/zdq

      CALL BULK_FORMULA_SCLR( pzu, pts, pqs, pThta, pqa, zCd, zCh, zCe, &
                             pwnd, pUb, pslp,                         &
                             pTau, zQsen, zQlat )

      zQlw = qlw_net_sclr( prlw, pts ) ! Net longwave flux

      pQns = zQlat + zQsen + zQlw

      IF( PRESENT(Qlat) ) Qlat = zQlat

   END SUBROUTINE UPDATE_QNSOL_TAU_SCLR
   !!
   SUBROUTINE UPDATE_QNSOL_TAU_VCTR( pzu, pts, pqs, pThta, pqa, pust, ptst, pqst, pwnd, pUb, pslp, prlw, &
                                    pQns, pTau,    Qlat)
      !!----------------------------------------------------------------------------------
      !! Purpose: returns the non-solar heat flux to the ocean aka "Qlat + Qsen + Qlw"
      !!          and the module of the wind stress => pTau = Tau
      !! ** Author: L. Brodeau, Sept. 2019 / AeroBulk (https://github.com/brodeau/aerobulk/)
      !!----------------------------------------------------------------------------------
      DOUBLE PRECISION,                     INTENT(in)  :: pzu  ! height above the sea-level where all this takes place (normally 10m)
      DOUBLE PRECISION, DIMENSION(:,:), INTENT(in)  :: pts  ! water temperature at the air-sea interface [K]
      DOUBLE PRECISION, DIMENSION(:,:), INTENT(in)  :: pqs  ! satur. spec. hum. at T=pts   [kg/kg]
      DOUBLE PRECISION, DIMENSION(:,:), INTENT(in)  :: pThta  ! potential air temperature at z=pzu [K]
      DOUBLE PRECISION, DIMENSION(:,:), INTENT(in)  :: pqa  ! specific humidity at z=pzu [kg/kg]
      DOUBLE PRECISION, DIMENSION(:,:), INTENT(in)  :: pust ! u*
      DOUBLE PRECISION, DIMENSION(:,:), INTENT(in)  :: ptst ! t*
      DOUBLE PRECISION, DIMENSION(:,:), INTENT(in)  :: pqst ! q*
      DOUBLE PRECISION, DIMENSION(:,:), INTENT(in)  :: pwnd ! wind speed module at z=pzu [m/s]
      DOUBLE PRECISION, DIMENSION(:,:), INTENT(in)  :: pUb  ! bulk wind speed at z=pzu (inc. pot. effect of gustiness etc) [m/s]
      DOUBLE PRECISION, DIMENSION(:,:), INTENT(in)  :: pslp ! sea-level atmospheric pressure [Pa]
      DOUBLE PRECISION, DIMENSION(:,:), INTENT(in)  :: prlw ! downwelling longwave radiative flux [W/m^2]
      !
      DOUBLE PRECISION, DIMENSION(:,:), INTENT(out) :: pQns ! non-solar heat flux to the ocean aka "Qlat + Qsen + Qlw" [W/m^2]]
      DOUBLE PRECISION, DIMENSION(:,:), INTENT(out) :: pTau ! module of the wind stress [N/m^2]
      !
      DOUBLE PRECISION, DIMENSION(:,:), OPTIONAL, INTENT(out) :: Qlat
      !
      DOUBLE PRECISION :: zQlat
      INTEGER  ::   ji, jj     ! dummy loop indices
      LOGICAL  :: lrQlat=.false.
      !!----------------------------------------------------------------------------------
      lrQlat = PRESENT(Qlat)
      DO jj = 1, SIZE(pts,2)
         DO ji = 1, SIZE(pts,1)
            CALL UPDATE_QNSOL_TAU_SCLR( pzu, pts(ji,jj),  pqs(ji,jj),  pThta(ji,jj), pqa(ji,jj),  &
                                           pust(ji,jj), ptst(ji,jj), pqst(ji,jj), pwnd(ji,jj), &
                                            pUb(ji,jj),  pslp(ji,jj), prlw(ji,jj),              &
                                           pQns(ji,jj), pTau(ji,jj),              Qlat=zQlat   )
            IF( lrQlat ) Qlat(ji,jj) = zQlat
         END DO
      END DO
   END SUBROUTINE UPDATE_QNSOL_TAU_VCTR
   !===============================================================================================

   !===============================================================================================
   SUBROUTINE BULK_FORMULA_SCLR( pzu, pts, pqs, pThta, pqa, &
                                pCd, pCh, pCe,            &
                                pwnd, pUb, pslp,          &
                                pTau, pQsen, pQlat,       &
                                pEvap, prhoa, l_ice      )
      !!----------------------------------------------------------------------------------
      DOUBLE PRECISION,                     INTENT(in)  :: pzu  ! height above the sea-level where all this takes place (normally 10m)
      DOUBLE PRECISION, INTENT(in)  :: pts  ! water temperature at the air-sea interface [K]
      DOUBLE PRECISION, INTENT(in)  :: pqs  ! satur. spec. hum. at T=pts   [kg/kg]
      DOUBLE PRECISION, INTENT(in)  :: pThta  ! potential air temperature at z=pzu [K]
      DOUBLE PRECISION, INTENT(in)  :: pqa  ! specific humidity at z=pzu [kg/kg]
      DOUBLE PRECISION, INTENT(in)  :: pCd
      DOUBLE PRECISION, INTENT(in)  :: pCh
      DOUBLE PRECISION, INTENT(in)  :: pCe
      DOUBLE PRECISION, INTENT(in)  :: pwnd ! wind speed module at z=pzu [m/s]
      DOUBLE PRECISION, INTENT(in)  :: pUb  ! bulk wind speed at z=pzu (inc. pot. effect of gustiness etc) [m/s]
      DOUBLE PRECISION, INTENT(in)  :: pslp ! sea-level atmospheric pressure [Pa]
      !!
      DOUBLE PRECISION, INTENT(out) :: pTau  ! module of the wind stress [N/m^2]
      DOUBLE PRECISION, INTENT(out) :: pQsen !  [W/m^2]
      DOUBLE PRECISION, INTENT(out) :: pQlat !  [W/m^2]
      !!
      DOUBLE PRECISION, INTENT(out), OPTIONAL :: pEvap ! Evaporation [kg/m^2/s] (usually <0, as ocean loses water!)
      DOUBLE PRECISION, INTENT(out), OPTIONAL :: prhoa ! Air density at z=pzu [kg/m^3]
      LOGICAL,  INTENT(in),  OPTIONAL :: l_ice  !: we are above ice
      !!
      DOUBLE PRECISION :: zta, zrho, zUrho, zevap
      LOGICAL  :: lice
      !!----------------------------------------------------------------------------------
      lice = .FALSE.
      IF( PRESENT(l_ice) ) lice = l_ice

      !! Need zta, absolute temperature at pzu (formula to estimate rho_air needs absolute temperature, not the potential temperature "pThta")
      zta  = pThta - rgamma_dry*pzu   ! Absolute temp. is slightly colder...
      zrho = rho_air(zta, pqa, pslp)
      zrho = rho_air(zta, pqa, pslp-zrho*grav*pzu) ! taking into account that we are pzu m above the sea level where SLP is given!

      zUrho = pUb*MAX(zrho, 1.D0)     ! rho*U10

      pTau = zUrho * pCd * pwnd ! Wind stress module ( `pwnd` here because `pUb` already in `zUrho`

      zevap = zUrho * pCe * (pqa - pqs)
      pQsen = zUrho * pCh * (pThta - pts) * cp_air(pqa)

      IF( lice) THEN
         pQlat =      rLsub * zevap
         IF( PRESENT(pEvap) ) pEvap = MIN( zevap , 0.D0 )
      ELSE
         pQlat = L_vap(pts) * zevap
         IF( PRESENT(pEvap) ) pEvap = zevap
      END IF

      IF( PRESENT(prhoa) ) prhoa = zrho

   END SUBROUTINE BULK_FORMULA_SCLR
   !!
   SUBROUTINE BULK_FORMULA_VCTR( pzu, pts, pqs, pThta, pqa, &
                                pCd, pCh, pCe,           &
                                pwnd, pUb, pslp,         &
                                pTau, pQsen, pQlat,      &
                                pEvap, prhoa, l_ice )
      !!----------------------------------------------------------------------------------
      DOUBLE PRECISION,                     INTENT(in)  :: pzu  ! height above the sea-level where all this takes place (normally 10m)
      DOUBLE PRECISION, DIMENSION(:,:), INTENT(in)  :: pts  ! water temperature at the air-sea interface [K]
      DOUBLE PRECISION, DIMENSION(:,:), INTENT(in)  :: pqs  ! satur. spec. hum. at T=pts   [kg/kg]
      DOUBLE PRECISION, DIMENSION(:,:), INTENT(in)  :: pThta  ! potential air temperature at z=pzu [K]
      DOUBLE PRECISION, DIMENSION(:,:), INTENT(in)  :: pqa  ! specific humidity at z=pzu [kg/kg]
      DOUBLE PRECISION, DIMENSION(:,:), INTENT(in)  :: pCd
      DOUBLE PRECISION, DIMENSION(:,:), INTENT(in)  :: pCh
      DOUBLE PRECISION, DIMENSION(:,:), INTENT(in)  :: pCe
      DOUBLE PRECISION, DIMENSION(:,:), INTENT(in)  :: pwnd ! wind speed module at z=pzu [m/s]
      DOUBLE PRECISION, DIMENSION(:,:), INTENT(in)  :: pUb  ! bulk wind speed at z=pzu (inc. pot. effect of gustiness etc) [m/s]
      DOUBLE PRECISION, DIMENSION(:,:), INTENT(in)  :: pslp ! sea-level atmospheric pressure [Pa]
      !!
      DOUBLE PRECISION, DIMENSION(:,:), INTENT(out) :: pTau  ! module of the wind stress [N/m^2]
      DOUBLE PRECISION, DIMENSION(:,:), INTENT(out) :: pQsen !  [W/m^2]
      DOUBLE PRECISION, DIMENSION(:,:), INTENT(out) :: pQlat !  [W/m^2]
      !!
      DOUBLE PRECISION, DIMENSION(:,:), INTENT(out), OPTIONAL :: pEvap ! Evaporation [kg/m^2/s]
      DOUBLE PRECISION, DIMENSION(:,:), INTENT(out), OPTIONAL :: prhoa ! Air density at z=pzu [kg/m^3]
      LOGICAL,  INTENT(in),  OPTIONAL :: l_ice  !: we are above ice
      !!
      DOUBLE PRECISION :: zevap, zrho
      LOGICAL  :: lice=.FALSE., lrE=.FALSE., lrR=.FALSE.
      INTEGER  :: ji, jj
      CHARACTER(len=256) :: cmsg
      !!----------------------------------------------------------------------------------
      lice = PRESENT(l_ice)
      lrE  = PRESENT(pEvap)
      lrR  = PRESENT(prhoa)
      DO jj = 1, SIZE(pts,2)
         DO ji = 1, SIZE(pts,1)
            CALL BULK_FORMULA_SCLR( pzu, pts(ji,jj), pqs(ji,jj), pThta(ji,jj), pqa(ji,jj), &
                                   pCd(ji,jj), pCh(ji,jj), pCe(ji,jj),                  &
                                   pwnd(ji,jj), pUb(ji,jj), pslp(ji,jj),                &
                                   pTau(ji,jj), pQsen(ji,jj), pQlat(ji,jj),             &
                                   pEvap=zevap, prhoa=zrho, l_ice=lice )
            IF( lrE ) pEvap(ji,jj) = zevap
            IF( lrR ) prhoa(ji,jj) = zrho

            !!LOLO/alpha: sanity check on computed fluxes:
            IF( pTau(ji,jj) > ref_tau_max ) THEN
               !WRITE(cmsg,'(" => ",f8.2," N/m^2 ! At ji, jj = ", i4.4,", ",i4.4)') pTau(ji,jj), ji, jj
               CALL ctl_stop( 'BULK_FORMULA_VCTR()@mod_phymbl: wind stress too strong!', cmsg )
            END IF

         END DO
      END DO
   END SUBROUTINE BULK_FORMULA_VCTR
   !===============================================================================================

   !===============================================================================================
   FUNCTION alpha_sw_sclr( psst )
      !!---------------------------------------------------------------------------------
      !!                           ***  FUNCTION alpha_sw_sclr  ***
      !!
      !! ** Purpose : ROUGH estimate of the thermal expansion coefficient of sea-water at the surface (P =~ 1010 hpa)
      !!
      !! ** Author: L. Brodeau, june 2016 / AeroBulk (https://github.com/brodeau/aerobulk/)
      !!----------------------------------------------------------------------------------
      DOUBLE PRECISION, INTENT(in) ::   psst   ! sea-water temperature                   [K]
      DOUBLE PRECISION             ::   alpha_sw_sclr   ! thermal expansion coefficient of sea-water [1/K]
      !!----------------------------------------------------------------------------------
      alpha_sw_sclr = 2.1D-5 * MAX( psst - rt0 + 3.2D0 , 0.D0 )**0.79D0
      !!
   END FUNCTION alpha_sw_sclr
   !!
   FUNCTION alpha_sw_vctr( psst )
      DOUBLE PRECISION, DIMENSION(:,:), INTENT(in) ::   psst   ! water temperature                [K]
      DOUBLE PRECISION, DIMENSION(SIZE(psst,1),SIZE(psst,2))             ::   alpha_sw_vctr   ! thermal expansion coefficient of sea-water [1/K]
      alpha_sw_vctr = 2.1D-5 * MAX( psst(:,:) - rt0 + 3.2D0 , 0.D0 )**0.79D0
   END FUNCTION alpha_sw_vctr
   !===============================================================================================

   !===============================================================================================
   FUNCTION qlw_net_sclr( pdwlw, pts,  l_ice )
      !!---------------------------------------------------------------------------------
      !!                           ***  FUNCTION qlw_net_sclr  ***
      !!
      !! ** Purpose : Estimate of the net longwave flux at the surface
      !!----------------------------------------------------------------------------------
      DOUBLE PRECISION :: qlw_net_sclr
      DOUBLE PRECISION, INTENT(in) :: pdwlw !: downwelling longwave (aka infrared, aka thermal) radiation [W/m^2]
      DOUBLE PRECISION, INTENT(in) :: pts   !: surface temperature [K]
      LOGICAL,  INTENT(in), OPTIONAL :: l_ice  !: we are above ice
      DOUBLE PRECISION :: zemiss, zt2
      LOGICAL  :: lice
      !!----------------------------------------------------------------------------------
      lice = .FALSE.
      IF( PRESENT(l_ice) ) lice = l_ice
      IF( lice ) THEN
         zemiss = emiss_i
      ELSE
         zemiss = emiss_w
      END IF
      zt2 = pts*pts
      qlw_net_sclr = zemiss*( pdwlw - stefan*zt2*zt2)  ! zemiss used both as the IR albedo and IR emissivity...
      !!
   END FUNCTION qlw_net_sclr
   !!
   FUNCTION qlw_net_vctr( pdwlw, pts,  l_ice )
      DOUBLE PRECISION, DIMENSION(:,:), INTENT(in) :: pdwlw !: downwelling longwave (aka infrared, aka thermal) radiation [W/m^2]
      DOUBLE PRECISION, DIMENSION(:,:), INTENT(in) :: pts   !: surface temperature [K]
      DOUBLE PRECISION, DIMENSION(SIZE(pts,1),SIZE(pts,2)) :: qlw_net_vctr
      LOGICAL,  INTENT(in), OPTIONAL :: l_ice  !: we are above ice
      LOGICAL  :: lice
      INTEGER  :: ji, jj
      lice = .FALSE.
      IF( PRESENT(l_ice) ) lice = l_ice
      DO jj = 1, SIZE(pts,2)
         DO ji = 1, SIZE(pts,1)
            qlw_net_vctr(ji,jj) = qlw_net_sclr( pdwlw(ji,jj) , pts(ji,jj), l_ice=lice )
         END DO
      END DO
   END FUNCTION qlw_net_vctr
   !===============================================================================================

   !===============================================================================================
   SUBROUTINE check_unit_consistency( cfield, Xval)

      !! Ignore values where mask==0

      CHARACTER(len=*),                     INTENT(in) :: cfield
      DOUBLE PRECISION,   DIMENSION(:,:),           INTENT(in) :: Xval

      INTEGER(1), DIMENSION(:,:), ALLOCATABLE :: imask
      LOGICAL,    DIMENSION(:,:), ALLOCATABLE :: lmask
      INTEGER :: nx, ny
      CHARACTER(len=64) :: cunit
      DOUBLE PRECISION :: zmean, zmin, zmax
      LOGICAL  :: l_too_large=.FALSE., l_too_small=.FALSE., l_mean_outside=.FALSE.

      nx = SIZE(Xval,1)
      ny = SIZE(Xval,2)
      ALLOCATE( lmask(nx,ny), imask(nx,ny) )
      imask(:,:) =   1
      lmask(:,:) = .TRUE.

      zmean = SUM( Xval * REAL(imask,8) ) / SUM( REAL(imask,8) )

      SELECT CASE (TRIM(cfield))

      CASE('sst','SST','Ts')
         zmax = ref_sst_max
         zmin = ref_sst_min
         cunit = 'K'

      CASE('t_air','taa','t2m','T2M')
         zmax = ref_taa_max
         zmin = ref_taa_min
         cunit = 'K'

      CASE('q_air','sh','sha','q2m','Q2M')
         zmax = ref_sha_max
         zmin = ref_sha_min
         cunit = 'kg/kg'

      CASE('rh_air','rh','RH','rlh','rha')
         zmax = ref_rlh_max
         zmin = ref_rlh_min
         cunit = 'kg/kg'

      CASE('dp_air','dp','d2m','D2M')
         zmax = ref_dpt_max
         zmin = ref_dpt_min
         cunit = 'kg/kg'

      CASE('slp','mslp','MSL','msl','P')
         zmax = ref_slp_max
         zmin = ref_slp_min
         cunit = 'Pa'

      CASE('u10','v10')
         zmax =   ref_wnd_max
         zmin = - ref_wnd_max
         cunit = 'm/s'

      CASE('wnd','wind','w10','W10')
         zmax = ref_wnd_max
         zmin = ref_wnd_min
         cunit = 'm/s'

      CASE('rad_sw')
         zmax = ref_rsw_max
         zmin = ref_rsw_min
         cunit = 'W/m^2'

      CASE('rad_lw')
         zmax = ref_rlw_max
         zmin = ref_rlw_min
         cunit = 'W/m^2'

      CASE DEFAULT
         WRITE(LU,'(" *** ERROR (check_unit_consistency@mod_phymbl): we do not know field `",a,"` !")') TRIM(cfield)
         STOP
      END SELECT

      l_too_large     = ( MAXVAL(Xval, MASK=lmask) > zmax )
      l_too_small    = ( MINVAL(Xval, MASK=lmask) < zmin )
      l_mean_outside = ( (zmean < zmin) .OR. (zmean > zmax) )

      IF ( l_too_large .OR. l_too_small .OR. l_mean_outside ) THEN
         WRITE(LU,'(" *** ERROR (check_unit_consistency@mod_phymbl): field `",a,"` does not seem to be in ",a," !")') &
              TRIM(cfield), TRIM(cunit)
         WRITE(LU,'(" min value = ", es10.3," max value = ", es10.3," mean value = ", es10.3)')&
              MINVAL(Xval), MAXVAL(Xval), zmean
         STOP
      END IF
      DEALLOCATE( imask, lmask )
      !!
   END SUBROUTINE check_unit_consistency


   FUNCTION type_of_humidity( Xval, mask )
      !!
      !! Guess the type of humidity contained into array Xval
      !! based on the mean, min and max values of the field!
      !! Values where mask==0 are ignored !
      !!
      DOUBLE PRECISION,   DIMENSION(:,:), INTENT(in) :: Xval
      INTEGER(1), DIMENSION(:,:), INTENT(in) :: mask
      CHARACTER(len=2)                       :: type_of_humidity !: => 'sh', 'rh' or 'dp'
      !!
      LOGICAL, DIMENSION(:,:), ALLOCATABLE :: lmask
      INTEGER :: nx, ny
      DOUBLE PRECISION :: zmean, zmin, zmax
      !!
      nx = SIZE(mask,1)
      ny = SIZE(mask,2)
      ALLOCATE( lmask(nx,ny) )
      lmask(:,:) = .FALSE.
      WHERE( mask==1 ) lmask = .TRUE.

      zmean =    SUM( Xval * REAL(mask,8) ) / SUM( REAL(mask,8) )
      zmin  = MINVAL( Xval,  MASK=lmask )
      zmax  = MAXVAL( Xval,  MASK=lmask )

      type_of_humidity = '00'

      IF(     (zmean >= ref_sha_min).AND.(zmean < ref_sha_max).AND.(zmin >= ref_sha_min).AND.(zmax < ref_sha_max) ) THEN
         !! Specific humidity! [kg/kg]
         type_of_humidity = 'sh'

      ELSEIF( (zmean >= ref_dpt_min).AND.(zmean < ref_dpt_max).AND.(zmin >= ref_dpt_min).AND.(zmax < ref_dpt_max) ) THEN
         !! Dew-point temperature [K]
         type_of_humidity = 'dp'

      ELSEIF( (zmean >= ref_rlh_min).AND.(zmean <= ref_rlh_max).AND.(zmin >= ref_rlh_min).AND.(zmax <= ref_rlh_max) ) THEN
         !! Relative humidity [%]
         type_of_humidity = 'rh'

      ELSE
         WRITE(LU,*) 'ERROR: type_of_humidity()@mod_aerobulk_compute => un-identified humidity type!'
         WRITE(LU,*) '   ==> we could not identify the humidity type based on the mean, min & max of the field:'
         STOP
      END IF

      DEALLOCATE( lmask )

   END FUNCTION type_of_humidity


   FUNCTION delta_skin_layer_sclr( palpha, pQd, pustar_a,  Qlat )
      !!---------------------------------------------------------------------
      !! Computes the thickness (m) of the viscous skin layer.
      !! Based on Fairall et al., 1996
      !!
      !! Fairall, C. W., Bradley, E. F., Godfrey, J. S., Wick, G. A.,
      !! Edson, J. B., and Young, G. S. ( 1996), Cool‐skin and warm‐layer
      !! effects on sea surface temperature, J. Geophys. Res., 101( C1), 1295-1308,
      !! doi:10.1029/95JC03190.
      !!
      !! L. Brodeau, october 2019
      !!---------------------------------------------------------------------
      DOUBLE PRECISION,           INTENT(in) :: palpha   ! thermal expansion coefficient of sea-water (SST accurate enough!)
      DOUBLE PRECISION,           INTENT(in) :: pQd   ! (<0!) part of `Qnet` absorbed in the WL [W/m^2] => term "Q + Rs*fs" in eq.6 of Fairall et al. 1996
      DOUBLE PRECISION,           INTENT(in) :: pustar_a ! friction velocity in the air (u*) [m/s]
      DOUBLE PRECISION, OPTIONAL, INTENT(in) :: Qlat    ! latent heat flux [W/m^2]
      DOUBLE PRECISION                       :: delta_skin_layer_sclr
      !!---------------------------------------------------------------------
      DOUBLE PRECISION :: zusw, zusw2, zlamb, zQd, ztf, ztmp
      !!---------------------------------------------------------------------
      zQd = pQd
      IF( PRESENT(Qlat) ) zQd = pQd + 0.026D0*MIN(Qlat,0.D0)*rCp0_w/rLevap/palpha ! LOLO: Double check sign + division by palpha !!! units are okay!

      ztf = 0.5D0 + SIGN(0.5D0, zQd)  ! Qabs < 0 => cooling of the viscous layer => ztf = 0 (regular case)
      !                                 ! Qabs > 0 => warming of the viscous layer => ztf = 1 (ex: weak evaporation and strong positive sensible heat flux)
      !
      zusw  = MAX(pustar_a, 1.D-4) * sq_radrw    ! u* in the water
      zusw2 = zusw*zusw
      !
      zlamb = 6.D0*( 1.D0 + MAX(palpha*rcst_cs/(zusw2*zusw2)*zQd, 0.D0)**0.75D0 )**(-1.D0/3.D0) ! see Eq.(14) in Fairall et al., 1996
      !  => zlamb is not used when Qd > 0, and since rcst_cs < 0, we just use this "MAX" to prevent FPE errors (something_negative)**0.75
      !
      ztmp = rnu0_w/zusw
      delta_skin_layer_sclr = (1.D0-ztf) *     zlamb*ztmp           &  ! regular case, Qd < 0, see Eq.(12) in Fairall et al., 1996
                        +   ztf     * MIN(6.D0*ztmp , 0.007D0)  ! when Qd > 0
      !!
   END FUNCTION delta_skin_layer_sclr

   SUBROUTINE AEROBULK_INIT( psst, pta, pha, pU, pV, pslp,  l_use_skin, prsw, prlw )
      !!===========================================================================================
      !! 1. Set the official 2D shape of the problem based on the `psst` array =>[Ni,Nj]
      !! 2. Check on size agreement between input arrays (must all be [Ni,Nj])
      !! 3. Allocate and fill the `imask` array to disregar "apparent problematic" regions...
      !! 4. Decide the type of humidity in use: specific? relative? dew-point? (saved and shared via mod_const)
      !!
      DOUBLE PRECISION, DIMENSION(:,:), INTENT(in)  :: psst  !: sea surface temperature             [K]
      DOUBLE PRECISION, DIMENSION(:,:), INTENT(in)  :: pta   !: absolute air temperature at z=zt    [K]
      DOUBLE PRECISION, DIMENSION(:,:), INTENT(in)  :: pha   !: air humidity at z=zt, can be:
      !!                                                - specific humidity                 [kg/kg]
      !!                                                - dew-point temperature             [K]
      !!                                                - relative humidity                 [%]
      !!                                               => type is normally be recognized based on value range
      DOUBLE PRECISION, DIMENSION(:,:), INTENT(in)  :: pU, pV !: wind vector components at z=zu     [m/s]
      DOUBLE PRECISION, DIMENSION(:,:), INTENT(in)  :: pslp   !: sea-level atmospheric pressure     [Pa]
      !!
      !! OPTIONAL Input
      LOGICAL,                  INTENT(in), OPTIONAL :: l_use_skin
      DOUBLE PRECISION, DIMENSION(:,:), INTENT(in), OPTIONAL :: prsw   !: downwelling shortwave radiation  [W/m^2]
      DOUBLE PRECISION, DIMENSION(:,:), INTENT(in), OPTIONAL :: prlw   !: downwelling  longwave radiation  [W/m^2]
      !!==================================================================================================
      LOGICAL :: lsrad, lskin
      INTEGER :: Ni, Nj, np
      CHARACTER(len=64) :: chum_ln
      INTEGER(1), DIMENSION(:,:), ALLOCATABLE :: imask   !: mask array: masked=>0, elsewhere=>1
      !!==================================================================================================
      lsrad = .FALSE.
      lsrad = ( PRESENT(prsw) .AND. PRESENT(prlw) )

      lskin = .FALSE.
      IF( PRESENT(l_use_skin) ) lskin = l_use_skin

      ! 2.
      Ni = SIZE(psst,1)
      Nj = SIZE(psst,2)

      ! 3.
      IF( ANY(SHAPE(pta) /=(/Ni,Nj/)) ) CALL ctl_stop(' AEROBULK_INIT => SST and t_air arrays do not agree in shape!')
      IF( ANY(SHAPE(pha) /=(/Ni,Nj/)) ) CALL ctl_stop(' AEROBULK_INIT => SST and hum_air arrays do not agree in shape!')
      IF( ANY(SHAPE(pU)  /=(/Ni,Nj/)) ) CALL ctl_stop(' AEROBULK_INIT => SST and U arrays do not agree in shape!')
      IF( ANY(SHAPE(pV)  /=(/Ni,Nj/)) ) CALL ctl_stop(' AEROBULK_INIT => SST and V arrays do not agree in shape!')
      IF( ANY(SHAPE(pslp)/=(/Ni,Nj/)) ) CALL ctl_stop(' AEROBULK_INIT => SST and SLP arrays do not agree in shape!')
      IF( lsrad ) THEN
         IF( ANY(SHAPE(prsw)/=(/Ni,Nj/)) ) CALL ctl_stop(' AEROBULK_INIT => SST and Rad_SW arrays do not agree in shape!')
         IF( ANY(SHAPE(prlw)/=(/Ni,Nj/)) ) CALL ctl_stop(' AEROBULK_INIT => SST and Rad_LW arrays do not agree in shape!')
      END IF

      ! 4. Allocation and creation of the mask
      ALLOCATE( imask(Ni,Nj) )
      !WRITE(6,*)'    *** Filling the `mask` array...'
      imask(:,:) = 1
      WHERE( (psst < ref_sst_min) .OR. (psst > ref_sst_max) ) imask = 0 ! silly SST
      WHERE( ( pta < ref_taa_min) .OR. (pta  > ref_taa_max) ) imask = 0 ! silly air temperature
      WHERE( (pslp < ref_slp_min) .OR. (pslp > ref_slp_max) ) imask = 0 ! silly atmospheric pressure
      WHERE(     SQRT( pU*pU + pV*pV )       > ref_wnd_max  ) imask = 0 ! silly scalar wind speed
      IF( lsrad ) THEN
         WHERE( ( prsw < ref_rsw_min) .OR. (prsw  > ref_rsw_max) ) imask = 0 ! silly air temperature
         WHERE( ( prlw < ref_rlw_min) .OR. (prlw  > ref_rlw_max) ) imask = 0 ! silly air temperature
      END IF
      np = SUM(INT(imask,4))  ! number of valid points (convert to INT(4) before SUM otherwize SUM is INT(1) => overflow!!!)
      IF( np == Ni*Nj ) THEN
         !WRITE(6,*)'        ==> no points need to be masked! :)'
      ELSEIF ( np > 0 ) THEN
         !WRITE(6,*)'        ==> number of points to mask: ', Ni*Nj-np, ' (out of ',Ni*Nj,')'
      ELSE
         CALL ctl_stop( 'the whole domain is masked!', 'check unit consistency of input fields')
         !STOP
      END IF

      ! 5. Type of humidity provided?
      ctype_humidity = type_of_humidity( pha, mask=imask )
      SELECT CASE(ctype_humidity)
      CASE('sh')
         chum_ln = 'specific humidity [kg/kg]'
      CASE('rh')
         chum_ln = 'relative humidity [%]'
      CASE('dp')
         chum_ln = 'dew-point temperature [K]'
      CASE DEFAULT
         CALL ctl_stop( ' AEROBULK_INIT => humidty type "',ctype_humidity,'" is unknown!!!' )
      END SELECT

      ! 6. Check unit consistency of input fields:
      CALL check_unit_consistency( 'sst',   psst)
      CALL check_unit_consistency( 't_air', pta)
      CALL check_unit_consistency( 'slp',   pslp)
      CALL check_unit_consistency( 'u10',   pU)
      CALL check_unit_consistency( 'v10',   pV)
      CALL check_unit_consistency( 'wnd',   SQRT(pU*pU + pV*pV))
      CALL check_unit_consistency( ctype_humidity, pha)
      IF( lsrad ) THEN
         CALL check_unit_consistency( 'rad_sw',   prsw)
         CALL check_unit_consistency( 'rad_lw',   prlw)
      END IF

      DEALLOCATE( imask )

   END SUBROUTINE AEROBULK_INIT



   SUBROUTINE AEROBULK_MODEL( jt, Nt, &
                             zt, zu, sst, t_zt,   &
                             hum_zt, U_zu, V_zu, slp,    &
                             QL, QH, Tau_x, Tau_y, Evap, &
                             Niter, l_use_skin, rad_sw, rad_lw, T_s  )
      !!======================================================================================
      !!
      !! INPUT :
      !! -------
      !!    *  jt,Nt  : current time snaphot and total number of time snaphots to go for
      !!    *  calgo  : what bulk algorithm to use => 'coare'/'ncar'/'ecmwf'
      !!    *  zt     : height for temp. & spec. hum. of air (usually 2 or 10) [m]
      !!    *  zu     : height for wind (usually 10)                           [m]
      !!    *  sst    : SST                                                    [K]
      !!    *  t_zt   : ABSOLUTE air temperature at zt                         [K]
      !!    *  hum_zt : air humidity at zt, can be given as:
      !!                - specific humidity                                [kg/kg]
      !!                - dew-point temperature                                [K]
      !!                - relative humidity                                    [%]
      !!               => type should normally be recognized based on value range
      !!    *  U_zu   : zonal wind speed at zu                                 [m/s]
      !!    *  V_zu   : meridional wind speed at zu                            [m/s]
      !!    *  slp    : mean sea-level pressure                                [Pa] ~101000 Pa
      !!
      !! OUTPUT :
      !! --------
      !!    *  QL     : Latent heat flux                                     [W/m^2]
      !!    *  QH     : Sensible heat flux                                   [W/m^2]
      !!    *  Tau_x  : zonal wind stress                                    [N/m^2]
      !!    *  Tau_y  : zonal wind stress                                    [N/m^2]
      !!    *  Evap    : Evaporation                                          [mm/s] aka [kg/m^2/s] (usually <0, as ocean loses water!)
      !!
      !! OPTIONAL Input:
      !! ---------------
      !!    *  Niter  : number of itterattions in the bulk algorithm (default is 4)
      !!    *  l_use_skin: should we use the cool-skin & warm-layer schemes fot skin temperature ?
      !!    *  rad_sw : downwelling shortwave radiation at the surface (>0)   [W/m^2]
      !!    *  rad_lw : downwelling longwave radiation at the surface  (>0)   [W/m^2]
      !!
      !! OPTIONAL Output:
      !! ---------------
      !!    *  T_s    : skin temperature                                      [K]
      !!
      !!====================================================================================================
      INTEGER,                  INTENT(in)  :: jt, Nt
      DOUBLE PRECISION,                 INTENT(in)  :: zt, zu
      DOUBLE PRECISION, DIMENSION(:,:), INTENT(in)  :: sst, t_zt, hum_zt, U_zu, V_zu, slp
      DOUBLE PRECISION, DIMENSION(:,:), INTENT(out) :: QL, QH, Tau_x, Tau_y, Evap
      !! Optional input:
      INTEGER,                  INTENT(in), OPTIONAL :: Niter
      LOGICAL,                  INTENT(in), OPTIONAL :: l_use_skin
      DOUBLE PRECISION, DIMENSION(:,:), INTENT(in), OPTIONAL :: rad_sw, rad_lw
      !! Optional output:
      DOUBLE PRECISION, DIMENSION(:,:), INTENT(out),OPTIONAL :: T_s
      !!
      !! Local:
      LOGICAL :: lskin, lsrad
      !!====================================================================================================

      IF( PRESENT(Niter) )      nb_iter = Niter  ! Updating number of itterations (define in mod_const)

      lskin = .FALSE.
      IF( PRESENT(l_use_skin) ) lskin = l_use_skin

      lsrad = .FALSE.
      lsrad = ( PRESENT(rad_sw) .AND. PRESENT(rad_lw) ) ! if theser 2 are provided we NORMALLY plan to use the CSWL schemes!

      IF( jt <1 ) CALL ctl_stop('AEROBULK_MODEL => jt < 1 !??', 'we are in a Fortran world here...')

      IF( lsrad ) THEN

         IF( jt==1 ) CALL AEROBULK_INIT( sst, t_zt, hum_zt, U_zu, V_zu, slp,&
              l_use_skin=lskin, prsw=rad_lw, prlw=rad_lw )

         CALL AEROBULK_COMPUTE( jt, zt, zu, sst, t_zt, &
                               hum_zt, U_zu, V_zu, slp,      &
                               QL, QH, Tau_x, Tau_y,         &
                               rad_sw=rad_sw, rad_lw=rad_lw, T_s=T_s, Evp=Evap )

      ELSE

         IF( jt==1 ) CALL AEROBULK_INIT( sst, t_zt, hum_zt, U_zu, V_zu, slp,  l_use_skin=lskin )

         CALL AEROBULK_COMPUTE( jt, zt, zu, sst, t_zt, &
                               hum_zt, U_zu, V_zu, slp,  &
                               QL, QH, Tau_x, Tau_y,     &
                               Evp=Evap)

      END IF


   END SUBROUTINE AEROBULK_MODEL

   SUBROUTINE aerobulk_compute( jt, zt, zu, sst, t_zt, &
                               hum_zt, U_zu, V_zu, slp,  &
                               QL, QH, Tau_x, Tau_y,     &
                               rad_sw, rad_lw, T_s, Evp )
      !!======================================================================================
      !!
      !! INPUT :
      !! -------
      !!    *    jt   : current time record (<=nitend)
      !!    *  zt     : height for temperature and spec. hum. of air           [m]
      !!    *  zu     : height for wind (10m = traditional anemometric height  [m]
      !!    *  sst    : bulk SST                                               [K]
      !!    *  t_zt   : ABSOLUTE air temperature at zt                         [K]
      !!    *  hum_zt : air humidity at zt, can be given as:
      !!                - specific humidity                                [kg/kg]
      !!                - dew-point temperature                                [K]
      !!                - relative humidity                                    [%]
      !!               => type should normally be recognized based on value range
      !!    *  U_zu   : zonal wind speed at zu                                 [m/s]
      !!    *  V_zu   : meridional wind speed at zu                            [m/s]
      !!    *  slp    : mean sea-level pressure                                [Pa] ~101000 Pa
      !!
      !! OPTIONAL INPUT
      !! --------------  (these 2 are only needed when using CSWL schemes...)
      !!    *  rad_sw : downwelling shortwave radiation at the surface (>0)   [W/m^2]
      !!    *  rad_lw : downwelling longwave radiation at the surface  (>0)   [W/m^2]
      !!
      !! OUTPUT :
      !! --------
      !!    *  QL     : Latent heat flux                                     [W/m^2]
      !!    *  QH     : Sensible heat flux                                   [W/m^2]
      !!    *  Tau_x  : zonal wind stress                                    [N/m^2]
      !!    *  Tau_y  : meridional wind stress                               [N/m^2]
      !!
      !! OPTIONAL OUTPUT
      !! ---------------
      !!    *  T_s  : skin temperature    [K]    (only when l_use_skin_schemes=TRUE)
      !!    *  Evp : evaporation         [mm/s] aka [kg/m^2/s] (usually <0, as ocean loses water!)
      !!
      !!
      !!============================================================================
      !!
      !! I/O ARGUMENTS:
      INTEGER,                    INTENT(in)  :: jt
      DOUBLE PRECISION,                   INTENT(in)  :: zt, zu
      DOUBLE PRECISION,   DIMENSION(:,:), INTENT(in)  :: sst, t_zt, hum_zt, U_zu, V_zu, slp
      DOUBLE PRECISION,   DIMENSION(:,:), INTENT(out) :: QL, QH, Tau_x, Tau_y
      DOUBLE PRECISION,   DIMENSION(:,:), INTENT(in), OPTIONAL :: rad_sw, rad_lw
      DOUBLE PRECISION,   DIMENSION(:,:), INTENT(out),OPTIONAL :: T_s, Evp
      !!
      DOUBLE PRECISION,   DIMENSION(:,:), ALLOCATABLE  ::  &
              zWzu,            & !: Scalar wind speed at zu m
            zSSQ,              & !: Specific humidiyt at the air-sea interface
            zCd, zCh, zCe,     & !: bulk transfer coefficients
           zThtzt, zQzt,       & !: POTENTIAL air temperature and specific humidity at zt meters
           zThtzu, zQzu,       & !: POTENTIAL air temperature and specific humidity at zu meters
           zTs, zqs, zEvap,    & !:
           zTaum,              & !: wind stress module
           zUblk,              & !: Bulk scalar wind speed (zWzu corrected for low wind and unstable conditions)
           ztmp                  !: temporary array
      !
      INTEGER          :: Ni, Nj
      !!------------------------------------------------------------------------------

      Ni = SIZE(sst,1)
      Nj = SIZE(sst,2)

      ALLOCATE ( zWzu(Ni,Nj),   zSSQ(Ni,Nj),                &
                 zCd(Ni,Nj),    zCh(Ni,Nj),   zCe(Ni,Nj),  &
                zThtzt(Ni,Nj), zQzt(Ni,Nj),                &
                zThtzu(Ni,Nj), zQzu(Ni,Nj),                &
                zUblk(Ni,Nj),  zTs(Ni,Nj),    zqs(Ni,Nj),  &
                zEvap(Ni,Nj),  zTaum(Ni,Nj),  ztmp(Ni,Nj)  )

      ! Conversion to specific humidity when needed:
      SELECT CASE(ctype_humidity)
      CASE('sh')
         zQzt(:,:) =           hum_zt(:,:)                        ! already specific humidity!
      CASE('dp')
         zQzt(:,:) = q_air_dp( hum_zt(:,:),            MAX(slp(:,:),50000.D0) ) ! dew-point to specific humidity
      CASE('rh')
         zQzt(:,:) = q_air_rh( hum_zt(:,:), t_zt(:,:), MAX(slp(:,:),50000.D0) ) ! relative to specific humidity
      CASE DEFAULT
         WRITE(LU,*) 'ERROR: mod_aerobulk_compute.f90 => humidty type "',ctype_humidity,'" is unknown!!!' ; STOP
      END SELECT

      !! Scalar wind:
      zWzu = sqrt( U_zu*U_zu + V_zu*V_zu )

      !! Computing specific humidity at saturation at sea surface temperature :
      zSSQ (:,:) = rdct_qsat_salt*q_sat(sst, slp)

      !! Approximate potential temperarure at zt meters above sea surface:
      !zThtzt = t_zt + gamma_moist(t_zt, zQzt)*zt  !BAD! and should have used `gamma_dry` anyway...
      zThtzt = Theta_from_z_P0_T_q( zt, slp, t_zt, zQzt )

      !! Mind that TURB_COARE* and TURB_ECMWF will modify SST and SSQ if their
      !! respective Cool Skin Warm Layer parameterization is used
      zTs = sst
      zqs = zSSQ

      ztmp(:,:) = 0.D0   ! longitude fixed to 0, (for COAREx when using cool-skin/warm-layer)

      IF( l_use_skin_schemes ) THEN
         CALL TURB_ECMWF ( jt, zt, zu, zTs, zThtzt, zqs, zQzt, zWzu, .TRUE., .TRUE.,  &
                          zCd, zCh, zCe, zThtzu, zQzu, zUblk,      &
                          Qsw=(1.D0 - roce_alb0)*rad_sw, rad_lw=rad_lw, slp=slp  )
      ELSE
         CALL TURB_ECMWF ( jt, zt, zu, zTs, zThtzt, zqs, zQzt, zWzu, .FALSE., .FALSE.,  &
                          zCd, zCh, zCe, zThtzu, zQzu, zUblk)
      END IF

      !! Skin temperature:
      !! IF( l_use_skin_schemes ) => zTs and zqs have been updated from SST to skin temperature !

      CALL BULK_FORMULA( zu, zTs, zqs, zThtzu, zQzu, zCd, zCh, zCe, zWzu, zUblk, slp, &
                                          zTaum, QH, QL, pEvap=zEvap )

      !! Wind stress vector from wind stress module
      !!    (zTaum = rho * Cd * zUblk * zWzu)
      Tau_x(:,:) = 0.D0
      Tau_y(:,:) = 0.D0
      WHERE( zWzu > 1.D-3 )
         Tau_x = zTaum / zWzu * U_zu
         Tau_y = zTaum / zWzu * V_zu
      END WHERE

      IF( PRESENT(T_s) ) T_s(:,:)   = zTs(:,:)

      IF( PRESENT(Evp) ) Evp(:,:) = zEvap(:,:)

      DEALLOCATE ( zWzu, zSSQ, zCd, zCh, zCe, zThtzt, zQzt, zThtzu, zQzu, &
                  zUblk, zTs, zqs, zEvap, zTaum, ztmp  )

   END SUBROUTINE aerobulk_compute

   SUBROUTINE ECMWF_INIT(nx, ny, l_use_wl)
      !!---------------------------------------------------------------------
      !!                  ***  FUNCTION ecmwf_init  ***
      !!
      !! INPUT :
      !! -------
      !!    * l_use_wl : use the warm-layer parameterization
      !!---------------------------------------------------------------------
      INTEGER, INTENT(in) :: nx, ny   ! shape of the domain
      LOGICAL, INTENT(in) :: l_use_wl ! use the warm-layer parameterization
      INTEGER :: ierr
      !!---------------------------------------------------------------------
      IF( l_use_wl ) THEN
         ierr = 0
         ALLOCATE ( dT_wl(nx,ny), Hz_wl(nx,ny), STAT=ierr )
         IF( ierr > 0 ) CALL ctl_stop( ' '//clbl//'_INIT => allocation of Tau_ac, Qnt_ac, dT_wl & Hz_wl failed!' )
         dT_wl(:,:)  = 0.D0
         Hz_wl(:,:)  = rd0 ! (rd0, constant, = 3m is default for Zeng & Beljaars)
      ENDIF
      !IF( l_use_cs ) THEN
      !   ierr = 0
      !   ALLOCATE ( dT_cs(nx,ny), STAT=ierr )
      !   IF( ierr > 0 ) CALL ctl_stop( ' '//clbl//'_INIT => allocation of dT_cs failed!' )
      !   dT_cs(:,:) = -0.25D0  ! First guess of skin correction
      !ENDIF
   END SUBROUTINE ECMWF_INIT

   SUBROUTINE ECMWF_EXIT(l_use_wl)
      !!---------------------------------------------------------------------
      !!                  ***  FUNCTION ecmwf_exit  ***
      !!
      !! INPUT :
      !! -------
      !!    * l_use_wl : use the warm-layer parameterization
      !!---------------------------------------------------------------------
      LOGICAL , INTENT(in) ::   l_use_wl ! use the warm-layer parameterization
      INTEGER :: ierr
      !!---------------------------------------------------------------------
      IF( l_use_wl ) THEN
         ierr = 0
         DEALLOCATE ( dT_wl, Hz_wl, STAT=ierr )
         IF( ierr > 0 ) CALL ctl_stop( ' '//clbl//'_EXIT => deallocation of Tau_ac, Qnt_ac, dT_wl & Hz_wl failed!' )
      ENDIF
   END SUBROUTINE ECMWF_EXIT

   SUBROUTINE TURB_ECMWF( kt, zt, zu, T_s, t_zt, q_s, q_zt, U_zu, l_use_cs, l_use_wl, &
                            Cd, Ch, Ce, t_zu, q_zu, Ubzu,                               &
                            Qsw, rad_lw, slp, pdT_cs,                                   & ! optionals for cool-skin (and warm-layer)
                            pdT_wl, pHz_wl,                                             & ! optionals for warm-layer only
                            CdN, ChN, CeN, xz0, xu_star, xL, xUN10 )
      !!----------------------------------------------------------------------
      !!                      ***  ROUTINE  turb_ecmwf  ***
      !!
      !! ** Purpose :   Computes turbulent transfert coefficients of surface
      !!                fluxes according to IFS doc. (cycle 45r1)
      !!                If relevant (zt /= zu), adjust temperature and humidity from height zt to zu
      !!                Returns the effective bulk wind speed at zu to be used in the bulk formulas
      !!
      !!                Applies the cool-skin warm-layer correction of the SST to T_s
      !!                if the net shortwave flux at the surface (Qsw), the downwelling longwave
      !!                radiative fluxes at the surface (rad_lw), and the sea-leve pressure (slp)
      !!                are provided as (optional) arguments!
      !!
      !! INPUT :
      !! -------
      !!    *  kt   : current time step (starts at 1)
      !!    *  zt   : height for temperature and spec. hum. of air            [m]
      !!    *  zu   : height for wind speed (usually 10m)                     [m]
      !!    *  t_zt : potential air temperature at zt                         [K]
      !!    *  q_zt : specific humidity of air at zt                          [kg/kg]
      !!    *  U_zu : scalar wind speed at zu                                 [m/s]
      !!    * l_use_cs : use the cool-skin parameterization
      !!    * l_use_wl : use the warm-layer parameterization
      !!
      !! INPUT/OUTPUT:
      !! -------------
      !!    *  T_s  : always "bulk SST" as input                              [K]
      !!              -> unchanged "bulk SST" as output if CSWL not used      [K]
      !!              -> skin temperature as output if CSWL used              [K]
      !!
      !!    *  q_s  : SSQ aka saturation specific humidity at temp. T_s       [kg/kg]
      !!              -> doesn't need to be given a value if skin temp computed (in case l_use_cs=True or l_use_wl=True)
      !!              -> MUST be given the correct value if not computing skint temp. (in case l_use_cs=False or l_use_wl=False)
      !!
      !! OPTIONAL INPUT:
      !! ---------------
      !!    *  Qsw    : net solar flux (after albedo) at the surface (>0)     [W/m^2]
      !!    *  rad_lw : downwelling longwave radiation at the surface  (>0)   [W/m^2]
      !!    *  slp    : sea-level pressure                                    [Pa]
      !!
      !! OPTIONAL OUTPUT:
      !! ----------------
      !!    * pdT_cs  : SST increment "dT" for cool-skin correction           [K]
      !!    * pdT_wl  : SST increment "dT" for warm-layer correction          [K]
      !!    * pHz_wl  : thickness of warm-layer                               [m]
      !!
      !! OUTPUT :
      !! --------
      !!    *  Cd     : drag coefficient
      !!    *  Ch     : sensible heat coefficient
      !!    *  Ce     : evaporation coefficient
      !!    *  t_zu   : pot. air temperature adjusted at wind height zu       [K]
      !!    *  q_zu   : specific humidity of air        //                    [kg/kg]
      !!    *  Ubzu  : bulk wind speed at zu                                 [m/s]
      !!
      !! OPTIONAL OUTPUT:
      !! ----------------
      !!    * CdN      : neutral-stability drag coefficient
      !!    * ChN      : neutral-stability sensible heat coefficient
      !!    * CeN      : neutral-stability evaporation coefficient
      !!    * xz0      : return the aerodynamic roughness length (integration constant for wind stress) [m]
      !!    * xu_star  : return u* the friction velocity                    [m/s]
      !!    * xL       : return the Obukhov length                          [m]
      !!    * xUN10    : neutral wind speed at 10m                          [m/s]
      !!
      !! ** Author: L. Brodeau, June 2019 / AeroBulk (https://github.com/brodeau/aerobulk/)
      !!----------------------------------------------------------------------------------
      INTEGER,  INTENT(in   )                 ::   kt       ! current time step
      DOUBLE PRECISION, INTENT(in   )                 ::   zt       ! height for t_zt and q_zt                    [m]
      DOUBLE PRECISION, INTENT(in   )                 ::   zu       ! height for U_zu                             [m]
      DOUBLE PRECISION, INTENT(inout), DIMENSION(:,:) ::   T_s      ! sea surface temperature                [Kelvin]
      DOUBLE PRECISION, INTENT(in   ), DIMENSION(:,:) ::   t_zt     ! potential air temperature              [Kelvin]
      DOUBLE PRECISION, INTENT(inout), DIMENSION(:,:) ::   q_s      ! sea surface specific humidity           [kg/kg]
      DOUBLE PRECISION, INTENT(in   ), DIMENSION(:,:) ::   q_zt     ! specific air humidity at zt             [kg/kg]
      DOUBLE PRECISION, INTENT(in   ), DIMENSION(:,:) ::   U_zu     ! relative wind module at zu                [m/s]
      LOGICAL , INTENT(in   )                 ::   l_use_cs ! use the cool-skin parameterization
      LOGICAL , INTENT(in   )                 ::   l_use_wl ! use the warm-layer parameterization
      DOUBLE PRECISION, INTENT(  out), DIMENSION(:,:) ::   Cd       ! transfer coefficient for momentum         (tau)
      DOUBLE PRECISION, INTENT(  out), DIMENSION(:,:) ::   Ch       ! transfer coefficient for sensible heat (Q_sens)
      DOUBLE PRECISION, INTENT(  out), DIMENSION(:,:) ::   Ce       ! transfert coefficient for evaporation   (Q_lat)
      DOUBLE PRECISION, INTENT(  out), DIMENSION(:,:) ::   t_zu     ! pot. air temp. adjusted at zu               [K]
      DOUBLE PRECISION, INTENT(  out), DIMENSION(:,:) ::   q_zu     ! spec. humidity adjusted at zu           [kg/kg]
      DOUBLE PRECISION, INTENT(  out), DIMENSION(:,:) ::   Ubzu    ! bulk wind speed at zu                     [m/s]
      !
      DOUBLE PRECISION, INTENT(in   ), OPTIONAL, DIMENSION(:,:) ::   Qsw      !             [W/m^2]
      DOUBLE PRECISION, INTENT(in   ), OPTIONAL, DIMENSION(:,:) ::   rad_lw   !             [W/m^2]
      DOUBLE PRECISION, INTENT(in   ), OPTIONAL, DIMENSION(:,:) ::   slp      !             [Pa]
      DOUBLE PRECISION, INTENT(  out), OPTIONAL, DIMENSION(:,:) ::   pdT_cs
      DOUBLE PRECISION, INTENT(  out), OPTIONAL, DIMENSION(:,:) ::   pdT_wl   !             [K]
      DOUBLE PRECISION, INTENT(  out), OPTIONAL, DIMENSION(:,:) ::   pHz_wl   !             [m]
      !
      DOUBLE PRECISION, INTENT(  out), OPTIONAL, DIMENSION(:,:) ::   CdN
      DOUBLE PRECISION, INTENT(  out), OPTIONAL, DIMENSION(:,:) ::   ChN
      DOUBLE PRECISION, INTENT(  out), OPTIONAL, DIMENSION(:,:) ::   CeN
      DOUBLE PRECISION, INTENT(  out), OPTIONAL, DIMENSION(:,:) ::   xz0  ! Aerodynamic roughness length   [m]
      DOUBLE PRECISION, INTENT(  out), OPTIONAL, DIMENSION(:,:) ::   xu_star  ! u*, friction velocity
      DOUBLE PRECISION, INTENT(  out), OPTIONAL, DIMENSION(:,:) ::   xL  ! zeta (zu/L)
      DOUBLE PRECISION, INTENT(  out), OPTIONAL, DIMENSION(:,:) ::   xUN10  ! Neutral wind at zu
      !
      INTEGER :: Ni, Nj, ji, jj, jit
      LOGICAL :: l_zt_equal_zu = .FALSE.      ! if q and t are given at same height as U
      !
      DOUBLE PRECISION, DIMENSION(:,:), ALLOCATABLE :: xSST     ! to back up the initial bulk SST
      !
      DOUBLE PRECISION :: zdt, zdq, zus, zus2, zUzu, zts, zqs, zNu_a, z1oL, zdT_cs
      DOUBLE PRECISION :: zRib, zpsi_m_u, zpsi_h_u, zpsi_h_t
      DOUBLE PRECISION :: zz0, zz0t, zz0q, zpsi_h_z0t, zpsi_h_z0q, zpsi_m_z0, zzeta_u, zzeta_t
      DOUBLE PRECISION :: zlog_10, zlog_ztu, zlog_z0, zlog_zu, zlog_z0t, zlog_z0q
      DOUBLE PRECISION :: zFm, zFh, zFq, zQns
      DOUBLE PRECISION :: ztmp0, ztmp1
      !
      LOGICAL ::  lreturn_cdn=.FALSE., lreturn_chn=.FALSE., lreturn_cen=.FALSE., &
                 lreturn_z0=.FALSE., lreturn_ustar=.FALSE., lreturn_L=.FALSE., lreturn_UN10=.FALSE.
      CHARACTER(len=40), PARAMETER :: crtnm = 'turb_ecmwf@mod_blk_ecmwf.f90'
      !!----------------------------------------------------------------------------------
      Ni = SIZE(T_s,1)
      Nj = SIZE(T_s,2)

      IF( kt == nit000 ) CALL ECMWF_INIT( Ni, Nj,  l_use_wl )

      lreturn_cdn   =  PRESENT(CdN)
      lreturn_chn   =  PRESENT(ChN)
      lreturn_cen   =  PRESENT(CeN)
      lreturn_z0    =  PRESENT(xz0)
      lreturn_ustar =  PRESENT(xu_star)
      lreturn_L     =  PRESENT(xL)
      lreturn_UN10  =  PRESENT(xUN10)

      l_zt_equal_zu = ( ABS(zu - zt) < 0.01D0 )

      !! Initializations for cool skin and warm layer:
      IF( l_use_cs .AND. (.NOT.(PRESENT(Qsw) .AND. PRESENT(rad_lw) .AND. PRESENT(slp))) ) &
            CALL ctl_stop( '['//TRIM(crtnm)//'] => ' , 'you need to provide Qsw, rad_lw & slp to use cool-skin param!' )

      IF( l_use_wl .AND. (.NOT.(PRESENT(Qsw) .AND. PRESENT(rad_lw) .AND. PRESENT(slp))) ) &
            CALL ctl_stop( '['//TRIM(crtnm)//'] => ' , 'you need to provide Qsw, rad_lw & slp to use warm-layer param!' )

      IF( l_use_cs .OR. l_use_wl ) THEN
         ALLOCATE ( xSST(Ni,Nj) )
         xSST = T_s ! backing up the bulk SST
         IF( l_use_cs ) T_s = T_s - 0.25D0   ! First guess of correction
         q_s    = rdct_qsat_salt*q_sat(MAX(T_s, 200.D0), slp) ! First guess of q_s
      ENDIF

      !! Constants:
      zlog_10  = LOG(10.D0)
      zlog_zu  = LOG(zu)
      zlog_ztu = LOG(zt/zu)

      DO jj = 1, Nj
         DO ji = 1, Ni

            zUzu = U_zu(ji,jj)

            CALL FIRST_GUESS_COARE( zt, zu, T_s(ji,jj), t_zt(ji,jj), q_s(ji,jj), q_zt(ji,jj), zUzu, &
                                   charn0_ecmwf,  zus, zts, zqs, &
                                   t_zu(ji,jj), q_zu(ji,jj), Ubzu(ji,jj),  pz0=zz0 )

            zlog_z0 = LOG(zz0)
            znu_a   = visc_air(t_zt(ji,jj)) ! Air viscosity (m^2/s) at zt given from temperature in (K)

            !! Pot. temp. difference (and we don't want it to be 0!)
            zdt = t_zu(ji,jj) - T_s(ji,jj) ;   zdt = SIGN( MAX(ABS(zdt),1.D-6), zdt )
            zdq = q_zu(ji,jj) - q_s(ji,jj) ;   zdq = SIGN( MAX(ABS(zdq),1.D-9), zdq )

            !! First guess of inverse of Obukov length (1/L) :
            z1oL    = One_on_L( t_zu(ji,jj), q_zu(ji,jj), zus, zts, zqs )
            zzeta_u = zu*z1oL
            zzeta_t = zt*z1oL

            zz0t    = MIN( MAX(ABS(  1.D0 / ( 0.1D0*EXP(vkarmn/(0.00115D0/( vkarmn/(zlog_10-zlog_z0) ))) )   ), 1.D-9) , 1.D0 )
            zlog_z0t = LOG(zz0t)

            !! Functions such as  u* = Ubzu*vkarmn/zFm
            zFm = zlog_zu - zlog_z0  - psi_m_ecmwf_sclr(zzeta_u) + psi_m_ecmwf_sclr( zz0*z1oL)
            zpsi_h_u = psi_h_ecmwf_sclr(zzeta_u)
            zFh = zlog_zu - zlog_z0t - zpsi_h_u + psi_h_ecmwf_sclr(zz0t*z1oL)

            !! ITERATION BLOCK
            DO jit = 1, nb_iter

               !! Bulk Richardson Number at z=zu (Eq. 3.25)
               zRib = Ri_bulk( zu, T_s(ji,jj), t_zu(ji,jj), q_s(ji,jj), q_zu(ji,jj), Ubzu(ji,jj) ) ! Bulk Richardson Number (BRN)

               !! New estimate of the inverse of the Obukhon length (z1oL == zeta/zu) :
               z1oL = zRib*zFm*zFm/zFh / zu     ! From Eq. 3.23, Chap.3.2.3, IFS doc - Cy40r1
               !! Note: it is slightly different that the L we would get with the usual
               z1oL   = SIGN( MIN(ABS(z1oL),200.D0), z1oL ) ! (prevent FPE from stupid values from masked region later on...)

               zzeta_u  = zu*z1oL
               zpsi_m_u = psi_m_ecmwf_sclr(zzeta_u)
               zpsi_h_u = psi_h_ecmwf_sclr(zzeta_u)

               zzeta_t  = zt*z1oL
               zpsi_h_t = psi_h_ecmwf_sclr(zzeta_t)

               !! Update zFm with new z1oL:
               zFm = zlog_zu -zlog_z0 - zpsi_m_u + psi_m_ecmwf_sclr(zz0*z1oL) ! LB: should be "zu+z0" rather than "zu" alone, but z0 is tiny wrt zu!

               !! Need to update roughness lengthes:
               zus = Ubzu(ji,jj)*vkarmn/zFm
               zus2  = zus*zus
               ztmp0  = znu_a/zus
               zz0     = MIN( ABS( alpha_M*ztmp0 + charn0_ecmwf*zus2/grav ) , 0.001D0)
               zz0t    = MIN( ABS( alpha_H*ztmp0                           ) , 0.001D0)   ! eq.3.26, Chap.3, p.34, IFS doc - Cy31r1
               zz0q    = MIN( ABS( alpha_Q*ztmp0                           ) , 0.001D0)

               zlog_z0  = LOG(zz0 )
               zlog_z0t = LOG(zz0t)
               zlog_z0q = LOG(zz0q)

               zpsi_m_z0   = psi_m_ecmwf_sclr(zz0 *z1oL)  ! LB: should be "zu+z0" rather than "zu" alone, but z0 is tiny wrt zu!
               zpsi_h_z0t  = psi_h_ecmwf_sclr(zz0t*z1oL)  !              "                           "
               zpsi_h_z0q  = psi_h_ecmwf_sclr(zz0q*z1oL)  !              "                           "

               !! Update wind at zu with convection-related wind gustiness in unstable conditions (Chap. 3.2, IFS doc - Cy40r1, Eq.3.17 and Eq.3.18 + Eq.3.8)
               ztmp0 = Beta0*Beta0*zus2*(MAX(-zi0*z1oL/vkarmn,0.D0))**(2.D0/3.D0) ! square of wind gustiness contribution  (combining Eq. 3.8 and 3.18, hap.3, IFS doc - Cy31r1)
               !!   ! Only true when unstable (L<0) => when zRib < 0 => explains "-" before zi0
               Ubzu(ji,jj) = MAX(SQRT(U_zu(ji,jj)*U_zu(ji,jj) + ztmp0), 0.2D0)        ! include gustiness in bulk wind speed
               ! => 0.2 prevents Ubzu to be 0 in stable case when U_zu=0.

               !! Need to update "theta" and "q" at zu in case they are given at different heights
               !! as well the air-sea differences:
               IF( .NOT. l_zt_equal_zu ) THEN
                  ztmp0  = zpsi_h_u - zpsi_h_z0t
                  ztmp1  = vkarmn/(zlog_zu - zlog_z0t - ztmp0)
                  zts = zdt*ztmp1
                  ztmp1  = zlog_ztu + ztmp0 - zpsi_h_t + zpsi_h_z0t
                  t_zu(ji,jj)   = t_zt(ji,jj) - zts/vkarmn*ztmp1

                  zpsi_h_z0q  = zpsi_h_z0q
                  ztmp0  = zpsi_h_u - zpsi_h_z0q
                  ztmp1  = vkarmn/(zlog_zu - zlog_z0q - ztmp0)
                  zqs = zdq*ztmp1
                  ztmp1  = zlog_ztu + ztmp0 - zpsi_h_t + zpsi_h_z0q
                  q_zu(ji,jj)   = q_zt(ji,jj) - zqs/vkarmn*ztmp1
               ENDIF

               !! Updating because of updated z0 and z0t and new z1oL...
               zFm = zlog_zu - zlog_z0  - zpsi_m_u + zpsi_m_z0
               zFh = zlog_zu - zlog_z0t - zpsi_h_u + zpsi_h_z0t

               IF( l_use_cs ) THEN
                  !! Cool-skin contribution
                  CALL UPDATE_QNSOL_TAU( zu, T_s(ji,jj), q_s(ji,jj), t_zu(ji,jj), q_zu(ji,jj), zus, zts, zqs, &
                                        U_zu(ji,jj), Ubzu(ji,jj), slp(ji,jj), rad_lw(ji,jj), zQns, ztmp0 )  ! Tau -> ztmp0

                  CALL CS_ECMWF( Qsw(ji,jj), zQns, zus, xSST(ji,jj), zdT_cs )
                  IF( PRESENT(pdT_cs) ) pdT_cs(ji,jj) = zdT_cs
                  T_s(ji,jj) = xSST(ji,jj) + zdT_cs
                  IF( l_use_wl ) T_s(ji,jj) = T_s(ji,jj) + dT_wl(ji,jj)
                  q_s(ji,jj) = rdct_qsat_salt*q_sat(MAX(T_s(ji,jj), 200.D0), slp(ji,jj))
               ENDIF

               IF( l_use_wl ) THEN
                  !! Warm-layer contribution
                  CALL UPDATE_QNSOL_TAU( zu, T_s(ji,jj), q_s(ji,jj), t_zu(ji,jj), q_zu(ji,jj), zus, zts, zqs, &
                       U_zu(ji,jj), Ubzu(ji,jj), slp(ji,jj), rad_lw(ji,jj), zQns, ztmp0)  ! Tau -> ztmp0
                  CALL WL_ECMWF( ji, jj, Qsw(ji,jj), zQns, zus, xSST(ji,jj) )
                  !! Updating T_s and q_s !!!
                  T_s(ji,jj) = xSST(ji,jj) + dT_wl(ji,jj)
                  IF( l_use_cs ) T_s(ji,jj) = T_s(ji,jj) + zdT_cs
                  q_s(ji,jj) = rdct_qsat_salt*q_sat(MAX(T_s(ji,jj), 200.D0), slp(ji,jj))
               ENDIF

               IF( l_use_cs .OR. l_use_wl .OR. (.NOT. l_zt_equal_zu) ) THEN
                  zdt = t_zu(ji,jj) - T_s(ji,jj) ;  zdt = SIGN( MAX(ABS(zdt),1.D-6), zdt )
                  zdq = q_zu(ji,jj) - q_s(ji,jj) ;  zdq = SIGN( MAX(ABS(zdq),1.D-9), zdq )
               ENDIF

            END DO !DO jit = 1, nb_iter

            ! compute transfer coefficients at zu :
            zFq = zlog_zu - zlog_z0q - zpsi_h_u + zpsi_h_z0q

            Cd(ji,jj) = MAX( vkarmn2/(zFm*zFm) , Cx_min )
            Ch(ji,jj) = MAX( vkarmn2/(zFm*zFh) , Cx_min )
            Ce(ji,jj) = MAX( vkarmn2/(zFm*zFq) , Cx_min )

            !! Optional output
            IF( lreturn_cdn .OR. lreturn_chn .OR. lreturn_cen ) ztmp0 = 1.D0/(zlog_zu - zlog_z0)
            IF( lreturn_cdn )   CdN(ji,jj) = MAX( vkarmn2*ztmp0*ztmp0 , Cx_min )
            IF( lreturn_chn .OR. lreturn_cen ) ztmp1 = vkarmn2*ztmp0/(zlog_zu - zlog_z0t)
            IF( lreturn_chn )   ChN(ji,jj) = MAX( ztmp1 , Cx_min )
            IF( lreturn_cen )   CeN(ji,jj) = MAX( ztmp1 , Cx_min )

            IF( lreturn_z0 )        xz0(ji,jj) = zz0
            IF( lreturn_ustar ) xu_star(ji,jj) = zus
            IF( lreturn_L )          xL(ji,jj) = 1.D0 / z1oL
            IF( lreturn_UN10 )    xUN10(ji,jj) = zus/vkarmn*(zlog_10 - zlog_z0)

         END DO
      END DO

      IF( l_use_wl .AND. PRESENT(pdT_wl) ) pdT_wl = dT_wl
      IF( l_use_wl .AND. PRESENT(pHz_wl) ) pHz_wl = Hz_wl

      IF( l_use_cs .OR. l_use_wl ) DEALLOCATE ( xSST )

      IF( kt == nitend ) CALL ECMWF_EXIT( l_use_wl )

   END SUBROUTINE turb_ecmwf

   !!===============================================================================================
   FUNCTION psi_m_ecmwf_sclr( pzeta )
      !!--------------------------------------------------------------------------------------------
      !! Universal profile stability function for momentum
      !!     ECMWF / as in IFS cy31r1 documentation, available online
      !!     at ecmwf.int
      !!
      !! pzeta : stability paramenter, z/L where z is altitude measurement
      !!         and L is M-O length
      !!
      !! ** Author: L. Brodeau, June 2016 / AeroBulk (https://github.com/brodeau/aerobulk/)
      !!--------------------------------------------------------------------------------------------
      DOUBLE PRECISION, INTENT(in) :: pzeta
      DOUBLE PRECISION             :: psi_m_ecmwf_sclr
      !!
      DOUBLE PRECISION :: zta, zx2, zx, ztmp, zpsi_unst, zpsi_stab, zstab, zc
      !!--------------------------------------------------------------------------------------------
      zc = 5.D0/0.35D0

      zta = pzeta
      CALL cap_zeta( zta )

      ! *** Unstable (Paulson 1970)    [eq.3.20, Chap.3, p.33, IFS doc - Cy31r1] :
      zx2 = SQRT( ABS(1.D0 - 16.D0*zta) )  ! (1 - 16z)^0.5
      zx  = SQRT(zx2)                        ! (1 - 16z)^0.25
      ztmp = 1.D0 + zx
      zpsi_unst = LOG( 0.125D0*ztmp*ztmp*(1.D0 + zx2) ) - 2.D0*ATAN( zx ) + 0.5D0*rpi

      ! *** Stable                   [eq.3.22, Chap.3, p.33, IFS doc - Cy31r1] :
      zpsi_stab = -2.D0/3.D0*(zta - zc)*EXP(-0.35D0*zta) &
                 - zta - 2.D0/3.D0*zc
      !
      zstab = 0.5D0 + SIGN(0.5D0, zta) ! zta > 0 => zstab = 1
      !
      psi_m_ecmwf_sclr =         zstab    * zpsi_stab &  ! (zta > 0) Stable
                       + (1.D0 - zstab) * zpsi_unst    ! (zta < 0) Unstable
      !
   END FUNCTION psi_m_ecmwf_sclr

   FUNCTION psi_m_ecmwf_vctr( pzeta )
      !!--------------------------------------------------------------------------------------------
      DOUBLE PRECISION, DIMENSION(:,:), INTENT(in) :: pzeta
      DOUBLE PRECISION, DIMENSION(SIZE(pzeta,1),SIZE(pzeta,2)) :: psi_m_ecmwf_vctr
      INTEGER  ::   ji, jj    ! dummy loop indices
      DOUBLE PRECISION :: zta, zx2, zx, ztmp, zpsi_unst, zpsi_stab, zstab, zc
      !!--------------------------------------------------------------------------------------------
      zc = 5.D0/0.35D0
      !
      DO jj = 1, SIZE(pzeta,2)
         DO ji = 1, SIZE(pzeta,1)

            zta = pzeta(ji,jj)
            CALL cap_zeta( zta )

            ! *** Unstable (Paulson 1970)    [eq.3.20, Chap.3, p.33, IFS doc - Cy31r1] :
            zx2 = SQRT( ABS(1.D0 - 16.D0*zta) )  ! (1 - 16z)^0.5
            zx  = SQRT(zx2)                          ! (1 - 16z)^0.25
            ztmp = 1.D0 + zx
            zpsi_unst = LOG( 0.125D0*ztmp*ztmp*(1.D0 + zx2) ) - 2.D0*ATAN( zx ) + 0.5D0*rpi

            ! *** Stable                   [eq.3.22, Chap.3, p.33, IFS doc - Cy31r1] :
            zpsi_stab = -2.D0/3.D0*(zta - zc)*EXP(-0.35D0*zta) &
                      - zta - 2.D0/3.D0*zc
            !
            zstab = 0.5D0 + SIGN(0.5D0, zta) ! zta > 0 => zstab = 1
            !
            psi_m_ecmwf_vctr(ji,jj) =         zstab  * zpsi_stab &  ! (zta > 0) Stable
                             + (1.D0 - zstab) * zpsi_unst    ! (zta < 0) Unstable
         END DO
      END DO
   END FUNCTION psi_m_ecmwf_vctr
   !!===============================================================================================

   !!===============================================================================================
   FUNCTION psi_h_ecmwf_sclr( pzeta )
      !!--------------------------------------------------------------------------------------------
      !! Universal profile stability function for temperature and humidity
      !!     ECMWF / as in IFS cy31r1 documentation, available online
      !!     at ecmwf.int
      !!
      !! pzeta : stability paramenter, z/L where z is altitude measurement
      !!         and L is M-O length
      !!
      !! ** Author: L. Brodeau, June 2016 / AeroBulk (https://github.com/brodeau/aerobulk/)
      !!--------------------------------------------------------------------------------------------
      DOUBLE PRECISION, INTENT(in) :: pzeta
      DOUBLE PRECISION             :: psi_h_ecmwf_sclr
      !!
      DOUBLE PRECISION ::  zta, zx2, zpsi_unst, zpsi_stab, zstab, zc
      !!--------------------------------------------------------------------------------------------
      zc = 5.D0/0.35D0

      zta = pzeta
      CALL cap_zeta( zta )

      ! *** Unstable (Paulson 1970)   [eq.3.20, Chap.3, p.33, IFS doc - Cy31r1] :
      zx2 = SQRT( ABS(1.D0 - 16.D0*zta) )  ! (1 -16z)^0.5
      zpsi_unst = 2.D0*LOG( 0.5D0*(1.D0 + zx2) )
      !
      ! *** Stable [eq.3.22, Chap.3, p.33, IFS doc - Cy31r1] :
      zpsi_stab = -2.D0/3.D0*(zta - zc)*EXP(-0.35D0*zta) &
                - ABS(1.D0 + 2.D0/3.D0*zta)**1.5D0 - 2.D0/3.D0*zc + 1.D0
      !! LB: added ABS() to avoid NaN values when unstable, which contaminates the unstable solution...
      !
      zstab = 0.5D0 + SIGN(0.5D0, zta) ! zta > 0 => zstab = 1
      !
      psi_h_ecmwf_sclr =        zstab     * zpsi_stab   &  ! (zta > 0) Stable
                       + (1.D0 - zstab) * zpsi_unst      ! (zta < 0) Unstable
      !
   END FUNCTION psi_h_ecmwf_sclr

   FUNCTION psi_h_ecmwf_vctr( pzeta )
      !!--------------------------------------------------------------------------------------------
      DOUBLE PRECISION, DIMENSION(:,:), INTENT(in) :: pzeta
      DOUBLE PRECISION, DIMENSION(SIZE(pzeta,1),SIZE(pzeta,2)) :: psi_h_ecmwf_vctr
      INTEGER  ::   ji, jj     ! dummy loop indices
      DOUBLE PRECISION ::  zta, zx2, zpsi_unst, zpsi_stab, zstab, zc
      !!--------------------------------------------------------------------------------------------
      zc = 5.D0/0.35D0
      !
      DO jj = 1, SIZE(pzeta,2)
         DO ji = 1, SIZE(pzeta,1)

            zta = pzeta(ji,jj)
            CALL cap_zeta( zta )

            ! *** Unstable (Paulson 1970)   [eq.3.20, Chap.3, p.33, IFS doc - Cy31r1] :
            zx2 = SQRT( ABS(1.D0 - 16.D0*zta) )  ! (1 -16z)^0.5
            zpsi_unst = 2.D0*LOG( 0.5D0*(1.D0 + zx2) )
            !
            ! *** Stable [eq.3.22, Chap.3, p.33, IFS doc - Cy31r1] :
            zpsi_stab = -2.D0/3.D0*(zta - zc)*EXP(-0.35D0*zta) &
                      - ABS(1.D0 + 2.D0/3.D0*zta)**1.5D0 - 2.D0/3.D0*zc + 1.D0
            !
            ! LB: added ABS() to avoid NaN values when unstable, which contaminates the unstable solution...
            !
            zstab = 0.5D0 + SIGN(0.5D0, zta) ! zta > 0 => zstab = 1
            !
            psi_h_ecmwf_vctr(ji,jj) =         zstab  * zpsi_stab &  ! (zta > 0) Stable
                             + (1.D0 - zstab) * zpsi_unst    ! (zta < 0) Unstable
         END DO
      END DO
   END FUNCTION psi_h_ecmwf_vctr
   !!===============================================================================================

   SUBROUTINE cap_zeta( pzeta )
      !!--------------------------------------------------------------------------------------------
      DOUBLE PRECISION, INTENT(inout) :: pzeta
      DOUBLE PRECISION ::  zta
      !!--------------------------------------------------------------------------------------------
      zta = MAX( pzeta , -50.D0 ) ! => regions where `zeta<-50.` are given value -50 (still unrealistic but numerically safe?)
      !                            !  ==> prevents numerical problems such as overflows...
      zta = MIN(  zta ,   5.D0 )  !`zeta` plateaus at 5 in very stable conditions (L>0 and small!), inherent to ECMWF algo!
      !
      pzeta = zta
   END SUBROUTINE cap_zeta

   SUBROUTINE CS_ECMWF( pQsw, pQnsol, pustar, pSST, pdT_cs )
      !!---------------------------------------------------------------------
      !!
      !! Cool-skin parameterization, based on Fairall et al., 1996:
      !!
      !! Fairall, C. W., Bradley, E. F., Godfrey, J. S., Wick, G. A.,
      !! Edson, J. B., and Young, G. S. ( 1996), Cool‐skin and warm‐layer
      !! effects on sea surface temperature, J. Geophys. Res., 101( C1), 1295-1308,
      !! doi:10.1029/95JC03190.
      !!
      !!------------------------------------------------------------------
      !!
      !!  **   INPUT:
      !!     *pQsw*       surface net solar radiation into the ocean     [W/m^2] => >= 0 !
      !!     *pQnsol*     surface net non-solar heat flux into the ocean [W/m^2] => normally < 0 !
      !!     *pustar*     friction velocity u*                           [m/s]
      !!     *pSST*       bulk SST (taken at depth gdept_1d(1))          [K]
      !!------------------------------------------------------------------
      DOUBLE PRECISION, INTENT(in) :: pQsw   ! net solar a.k.a shortwave radiation into the ocean (after albedo) [W/m^2]
      DOUBLE PRECISION, INTENT(in) :: pQnsol ! non-solar heat flux to the ocean [W/m^2]
      DOUBLE PRECISION, INTENT(in) :: pustar ! friction velocity, temperature and humidity (u*,t*,q*)
      DOUBLE PRECISION, INTENT(in) :: pSST   ! bulk SST [K]
      DOUBLE PRECISION, INTENT(out):: pdT_cs !: dT due to cool-skin effect => temperature difference between
      !!                             !: air-sea interface (z=0) and right below viscous layer (z=delta)
      !!---------------------------------------------------------------------
      INTEGER  :: jc
      DOUBLE PRECISION :: zQabs, zdelta, zfr
      !!---------------------------------------------------------------------
      zQabs = pQnsol ! first guess of heat flux absorbed within the viscous sublayer of thicknes delta,
      !              !   => we DO not miss a lot assuming 0 solar flux absorbed in the tiny layer of thicknes zdelta...

      zdelta = delta_skin_layer_sclr( alpha_sw(pSST), zQabs, pustar )

      DO jc = 1, 4 ! because implicit in terms of zdelta...
         ! Solar absorption, Eq.(5) Zeng & Beljaars, 2005:
         zfr = MAX( 0.065D0 + 11.D0*zdelta - 6.6D-5/zdelta*(1.D0 - EXP(-zdelta/8.D-4)) , 0.01D0 )
         !              =>  (WARNING: 0.065 rather than 0.137 in Fairal et al. 1996)
         zQabs = pQnsol + zfr*pQsw
         zdelta = delta_skin_layer_sclr( alpha_sw(pSST), zQabs, pustar )
      END DO

      pdT_cs = zQabs*zdelta/rk0_w   ! temperature increment, yes dT_cs can actually > 0, if Qabs > 0 (rare but possible!)

   END SUBROUTINE CS_ECMWF

   SUBROUTINE WL_ECMWF( ki, kj, pQsw, pQnsol, pustar, pSST,  pustk )
      !!---------------------------------------------------------------------
      !!
      !!  Warm-Layer scheme according to Zeng & Beljaars, 2005 (GRL)
      !!  " A prognostic scheme of sea surface skin temperature for modeling and data assimilation "
      !!
      !!  STIL NO PROGNOSTIC EQUATION FOR THE DEPTH OF THE WARM-LAYER!
      !!
      !!    As included in IFS Cy45r1   /  E.C.M.W.F.
      !!     ------------------------------------------------------------------
      !!
      !!  **   INPUT:
      !!     *pQsw*       surface net solar radiation into the ocean     [W/m^2] => >= 0 !
      !!     *pQnsol*     surface net non-solar heat flux into the ocean [W/m^2] => normally < 0 !
      !!     *pustar*     friction velocity u*                           [m/s]
      !!     *pSST*       bulk SST  (taken at depth gdept_1d(1))         [K]
      !!---------------------------------------------------------------------
      INTEGER , INTENT(in) :: ki, kj
      DOUBLE PRECISION, INTENT(in) :: pQsw     ! surface net solar radiation into the ocean [W/m^2]     => >= 0 !
      DOUBLE PRECISION, INTENT(in) :: pQnsol   ! surface net non-solar heat flux into the ocean [W/m^2] => normally < 0 !
      DOUBLE PRECISION, INTENT(in) :: pustar   ! friction velocity [m/s]
      DOUBLE PRECISION, INTENT(in) :: pSST     ! bulk SST at depth gdept_1d(1) [K]
      !!
      DOUBLE PRECISION, OPTIONAL, INTENT(in) :: pustk ! surface Stokes velocity [m/s]
      !
      INTEGER :: jc
      !
      DOUBLE PRECISION :: &
          zHwl,    &  !: thickness of the warm-layer [m]
          ztcorr,  &  !: correction of dT w.r.t measurement depth of bulk SST (first T-point)
          zalpha, & !: thermal expansion coefficient of sea-water [1/K]
          zdTwl_b, zdTwl_n, & ! temp. diff. between "almost surface (right below viscous layer) and bottom of WL
          zfr, zeta, &
          zusw, zusw2, &
          zLa, zfLa, &
          flg, zwf, zQabs, &
          zA, zB, zL1, zL2, &
           zcst0, zcst1, zcst2, zcst3
      !!
      LOGICAL :: l_pustk_known
      !!---------------------------------------------------------------------
      l_pustk_known = ( PRESENT(pustk) )

      zHwl = Hz_wl(ki,kj) ! first guess for warm-layer depth, and final choice because `Hz_wl` fixed in present ECMWF algo (as opposed to COARE)
      !!                  ! it is constant! => rd0 == 3m ! Zeng & Beljaars....

      !! Previous value of dT / warm-layer, adapted to depth:
      flg = 0.5D0 + SIGN( 0.5D0 , gdept_1d(1)-zHwl )               ! => 1 when gdept_1d(1)>zHwl (dT_wl(ki,kj) = zdTwl) | 0 when z_s$
      ztcorr = flg + (1.D0 - flg)*gdept_1d(1)/zHwl
      zdTwl_b = MAX ( dT_wl(ki,kj) / ztcorr , 0.D0 )
      ! zdTwl is the difference between "almost surface (right below viscous layer) and bottom of WL (here zHwl)
      ! pdT         "                          "                                    and depth of bulk SST (here gdept_1d(1))!
      !! => but of course in general the bulk SST is taken shallower than zHwl !!! So correction less pronounced!
      !! => so here since pdT is difference between surface and gdept_1d(1), need to increase fof zdTwl !

      zalpha = alpha_sw( pSST ) ! thermal expansion coefficient of sea-water (SST accurate enough!)


      ! *** zfr = Fraction of solar radiation absorbed in warm layer (-)
      zfr = 1.D0 - 0.28D0*EXP(-71.5D0*zHwl) - 0.27D0*EXP(-2.8D0*zHwl) - 0.45D0*EXP(-0.07D0*zHwl)  !: Eq. 8.157

      zQabs = zfr*pQsw + pQnsol       ! tot heat absorbed in warm layer

      zusw  = MAX( pustar, 1.D-4 ) * sq_radrw    ! u* in the water
      zusw2 = zusw*zusw

      ! Langmuir:
      IF ( l_pustk_known ) THEN
         zLa = SQRT(zusw/MAX(pustk,1.D-6))
      ELSE
         zla = 0.3D0
      END IF
      zfLa = MAX( zla**(-2.D0/3.D0) , 1.D0 )   ! Eq.(6)

      zwf = 0.5D0 + SIGN(0.5D0, zQabs)  ! zQabs > 0. => 1.  / zQabs < 0. => 0.

      zcst1 = vkarmn*grav*zalpha

      ! 1/L when zQabs > 0 :
      zL2 = zcst1*zQabs / (zRhoCp_w*zusw2*zusw)

      zcst2 = zcst1 / ( 5.D0*zHwl*zusw2 )  !OR: zcst2 = zcst1*rNuwl0 / ( 5.D0*zHwl*zusw2 ) ???

      zcst0 = rdt * (rNuwl0 + 1.D0) / zHwl

      zA = zcst0 * zQabs / ( rNuwl0 * zRhoCp_w )

      zcst3 = -zcst0 * vkarmn * zusw * zfLa

      !! Sorry about all these constants ( constant w.r.t zdTwl), it's for
      !! the sake of optimizations... So all these operations are not done
      !! over and over within the iteration loop...

      !! T R U L L Y   I M P L I C I T => needs itteration
      !! => have to itterate just because the 1/(Obukhov length), zL1, uses zdTwl when zQabs < 0..
      !!    (without this term otherwize the implicit analytical solution is straightforward...)
      zdTwl_n = zdTwl_b
      DO jc = 1, 10

         zdTwl_n = 0.5D0 * ( zdTwl_n + zdTwl_b ) ! semi implicit, for faster convergence

         ! 1/L when zdTwl > 0 .AND. zQabs < 0 :
         zL1 =         SQRT( zdTwl_n * zcst2 ) ! / zusw !!! Or??? => vkarmn * SQRT( zdTwl_n*grav*zalpha/( 5.D0*zHwl ) ) / zusw
         !zL1 = vkarmn*SQRT( zdTwl_n       *grav*zalpha        / ( 5.D0*zHwl ) ) / zusw   ! => vkarmn outside, not inside zcst1 (just for this particular line) ???

         ! Stability parameter (z/L):
         zeta =  (1.D0 - zwf) * zHwl*zL1   +   zwf * zHwl*zL2

         zB = zcst3 / PHI(zeta)

         zdTwl_n = MAX ( zdTwl_b + zA + zB*zdTwl_n , 0.D0 )  ! Eq.(6)

      END DO

      !! Update:
      dT_wl(ki,kj) = zdTwl_n * ztcorr

   END SUBROUTINE WL_ECMWF

   FUNCTION PHI( pzeta)
      !!---------------------------------------------------------------------
      !!
      !! Takaya et al., 2010
      !!  Eq.(5)
      !! L. Brodeau, october 2019
      !!---------------------------------------------------------------------
      DOUBLE PRECISION                :: PHI
      DOUBLE PRECISION, INTENT(in)    :: pzeta    ! stability parameter
      !!---------------------------------------------------------------------
      DOUBLE PRECISION :: ztf, zzt2
      !!---------------------------------------------------------------------
      !
      zzt2 = pzeta*pzeta
      !
      ztf = 0.5D0 + SIGN(0.5D0, pzeta)  ! zeta > 0 => ztf = 1
      !                                   ! zeta < 0 => ztf = 0
      PHI =      ztf     * ( 1.D0 + (5.D0*pzeta + 4.D0*zzt2)/(1.D0 + 3.D0*pzeta + 0.25D0*zzt2) ) &   ! zeta > 0
           + (1.D0 - ztf) * 1.D0/SQRT( 1.D0 - 16.D0*(-ABS(pzeta)) )                             ! zeta < 0
      !
   END FUNCTION PHI

   !===============================================================================================
   SUBROUTINE FIRST_GUESS_COARE_SCLR( zt, zu, psst, t_zt, pssq, q_zt, U_zu, pcharn, &
                                     pus, pts, pqs, t_zu, q_zu, Ubzu,  pz0 )
      !!----------------------------------------------------------------------
      !!                      ***  ROUTINE  FIRST_GUESS_COARE_SCLR  ***
      !!
      !! ** Purpose :  Computes fairly accurate first guess of u*, theta* and q*
      !!               by means of the method developed by Fairall et al in the
      !!               COARE family of algorithms
      !!               Purpose is to limit the number of itteration needed...
      !!
      !! INPUT :
      !! -------
      !!    *  zt   : height for temperature and spec. hum. of air            [m]
      !!    *  zu   : height for wind speed (usually 10m)                     [m]
      !!    *  psst  : bulk SST                                                [K]
      !!    *  t_zt : potential air temperature at zt                         [K]
      !!    *  pssq  : SSQ aka saturation specific humidity at temp. psst       [kg/kg]
      !!    *  q_zt : specific humidity of air at zt                          [kg/kg]
      !!    *  U_zu : scalar wind speed at zu                                 [m/s]
      !!    *  pcharn: Charnock parameter
      !!
      !! OUTPUT :
      !! --------
      !!    *  pus    : FIRST GUESS of u* aka friction velocity                            [m/s]
      !!    *  pts    : FIRST GUESS of theta*                                               [K]
      !!    *  pqs    : FIRST GUESS of q* aka friction velocity                            [kg/kg]
      !!    *  t_zu   : FIRST GUESS of pot. air temperature adjusted at wind height zu      [K]
      !!    *  q_zu   : FIRST GUESS of specific humidity of air        //                   [kg/kg]
      !!    *  Ubzu   : FIRST GUESS of bulk wind speed at zu                                [m/s]
      !!
      !! ** Author: L. Brodeau, May 2021 / AeroBulk (https://github.com/brodeau/aerobulk/)
      !!----------------------------------------------------------------------------------
      DOUBLE PRECISION, INTENT(in)  ::   zt
      DOUBLE PRECISION, INTENT(in)  ::   zu
      DOUBLE PRECISION, INTENT(in)  ::   psst
      DOUBLE PRECISION, INTENT(in)  ::   t_zt
      DOUBLE PRECISION, INTENT(in)  ::   pssq
      DOUBLE PRECISION, INTENT(in)  ::   q_zt
      DOUBLE PRECISION, INTENT(in)  ::   U_zu
      DOUBLE PRECISION, INTENT(in)  ::   pcharn
      !!
      DOUBLE PRECISION, INTENT(out) ::   pus
      DOUBLE PRECISION, INTENT(out) ::   pts
      DOUBLE PRECISION, INTENT(out) ::   pqs
      DOUBLE PRECISION, INTENT(out) ::   t_zu
      DOUBLE PRECISION, INTENT(out) ::   q_zu
      DOUBLE PRECISION, INTENT(out) ::   Ubzu
      DOUBLE PRECISION, INTENT(out), OPTIONAL :: pz0    ! roughness length [m]
      !
      LOGICAL :: l_zt_equal_zu = .FALSE.      ! if q and t are given at same height as U
      !
      DOUBLE PRECISION :: zdt, zdq, zUb, zCd, zus, zts, zqs, zNu_a, zRib
      DOUBLE PRECISION :: zlog_zt_o_zu, zlog_10, zlog_zt, zlog_zu, zlog_z0, zlog_z0t, z1_o_sqrt_Cd10, zcc, zcc_ri, z1_o_Ribcu
      DOUBLE PRECISION :: zprf, zc_a, zc_b
      DOUBLE PRECISION :: zz0, zz0t, zstab, zzeta_u, zzeta_t
      DOUBLE PRECISION :: ztmp
      !
      DOUBLE PRECISION, PARAMETER :: zzi0=600.D0, zBeta0=1.2D0 ! COARE values, after all it's a coare method...
      !
      CHARACTER(len=43), PARAMETER :: crtnm = 'FIRST_GUESS_COARE_SCLR@mod_common_coare.f90'
      !!----------------------------------------------------------------------------------
      l_zt_equal_zu = ( ABS(zu - zt) < 0.01D0 )

      !! First guess of temperature and humidity at height zu:
      t_zu = MAX( t_zt ,  180.D0 )   ! who knows what's given on masked-continental regions...
      q_zu = MAX( q_zt , 1.D-6 )   !               "

      zz0 = 0.0001D0 ! "rough" first guess of roughness length of sea surface...

      !! Constants:
      zlog_10 = LOG(10.D0)
      zlog_zt = LOG(zt)
      zlog_zu = LOG(zu)
      zlog_zt_o_zu = LOG(zt/zu)
      zc_a = 0.035D0*LOG(10.D0/zz0)/LOG(zu/zz0)   !       "                    "               "
      zc_b = 0.004D0*zzi0*zBeta0*zBeta0*zBeta0

      !! Air-sea differences (and we don't want them to be 0...)
      zdt = t_zu - psst ;   zdt = SIGN( MAX(ABS(zdt),1.D-6), zdt )
      zdq = q_zu - pssq ;   zdq = SIGN( MAX(ABS(zdq),1.D-9), zdq )

      zNu_a = visc_air(t_zu) ! Air viscosity (m^2/s) at zt given from temperature in (K)

      zUb = SQRT(U_zu*U_zu + 0.5D0*0.5D0) ! initial guess for wind gustiness contribution

      zus = zc_a*zUb

      !! Update roughness length:
      zz0     = pcharn*zus*zus/grav + 0.11D0*zNu_a/zus
      zz0     = MIN( MAX(ABS(zz0), 1.D-8) , 1.D0 )      ! (prevents FPE from stupid values from masked region later on)
      zlog_z0 = LOG(zz0)

      !! First guess of Cd
      zCd          = (vkarmn/(zlog_zu - zlog_z0))**2
      z1_o_sqrt_Cd10 =       (zlog_10 - zlog_z0)/vkarmn  ! sum less costly than product: log(a/b) == log(a) - log(b)

      !! Temperature/humidity roughness lengthes:
      zz0t  = 10.D0 / EXP( vkarmn/( 0.00115D0*z1_o_sqrt_Cd10 ) )
      zz0t    = MIN( MAX(ABS(zz0t), 1.D-8) , 1.D0 )         ! (prevents FPE from stupid values from masked region later on)
      zlog_z0t = LOG(zz0t)

      !! Bulk Richardson Number (BRN)
      zRib = Ri_bulk( zu, psst, t_zu, pssq, q_zu, zUb )

      !! First estimate of zeta_u, depending on the stability, ie sign of BRN (zRib):
      zcc = vkarmn2/(zCd*(zlog_zt - zlog_z0t))
      zcc_ri  = zcc*zRib
      z1_o_Ribcu = -zc_b/zu
      zstab   = 0.5D0 + SIGN( 0.5D0 , zRib )
      zzeta_u = (1.D0 - zstab) *   zcc_ri / (1.D0 + zRib*z1_o_Ribcu) & !  Ri_bulk < 0, Unstable
           +        zstab      * ( zcc_ri + 27.D0/9.D0*zRib*zRib )    !  Ri_bulk > 0, Stable

      !! u*, theta*, q* :
      zus  = MAX ( zUb*vkarmn/(zlog_zu - zlog_z0  - psi_m_coare(zzeta_u)) , 1.D-9 ) ! (MAX => prevents FPE from stupid values from masked region later on)
      ztmp = vkarmn/(zlog_zu - zlog_z0t - psi_h_coare(zzeta_u))
      zts  = zdt*ztmp
      zqs  = zdq*ztmp

      !! Adjustment of theta and q from zt to zu if relevant:
      IF( .NOT. l_zt_equal_zu ) THEN
         zzeta_t = zt*zzeta_u/zu
         zprf = LOG(zt/zu) + psi_h_coare(zzeta_u) - psi_h_coare(zzeta_t)
         t_zu = t_zt - zts/vkarmn*zprf
         q_zu = q_zt - zqs/vkarmn*zprf
         q_zu = (0.5D0 + SIGN(0.5D0,q_zu))*q_zu ! prevents negative humidity...
         !!
         !! Update of theta and q air-sea differences and theta*, q* :
         zdt = t_zu - psst  ; zdt = SIGN( MAX(ABS(zdt),1.D-6), zdt )
         zdq = q_zu - pssq  ; zdq = SIGN( MAX(ABS(zdq),1.D-9), zdq )
         zts = zdt*ztmp
         zqs = zdq*ztmp
      ENDIF

      !! Output result:
      pus  = zus
      pts  = zts
      pqs  = zqs
      Ubzu = zUb

      IF( PRESENT(pz0) ) THEN
         !! Again, because new zus:
         zz0 = pcharn*zus*zus/grav + 0.11D0*zNu_a/zus
         pz0 = MIN( MAX(ABS(zz0), 1.D-8) , 1.D0 )    ! (prevents FPE from stupid values from masked region later on)
      END IF

   END SUBROUTINE FIRST_GUESS_COARE_SCLR

   SUBROUTINE FIRST_GUESS_COARE_VCTR( zt, zu, psst, t_zt, pssq, q_zt, U_zu, pcharn, &
                                     pus, pts, pqs, t_zu, q_zu, Ubzu,  qz0 )
      !!----------------------------------------------------------------------------------
      DOUBLE PRECISION, INTENT(in)                  ::   zt
      DOUBLE PRECISION, INTENT(in)                  ::   zu
      DOUBLE PRECISION, INTENT(in),  DIMENSION(:,:) ::   psst
      DOUBLE PRECISION, INTENT(in),  DIMENSION(:,:) ::   t_zt
      DOUBLE PRECISION, INTENT(in),  DIMENSION(:,:) ::   pssq
      DOUBLE PRECISION, INTENT(in),  DIMENSION(:,:) ::   q_zt
      DOUBLE PRECISION, INTENT(in),  DIMENSION(:,:) ::   U_zu
      DOUBLE PRECISION, INTENT(in),  DIMENSION(:,:) ::   pcharn
      !!
      DOUBLE PRECISION, INTENT(out), DIMENSION(:,:) ::   pus
      DOUBLE PRECISION, INTENT(out), DIMENSION(:,:) ::   pts
      DOUBLE PRECISION, INTENT(out), DIMENSION(:,:) ::   pqs
      DOUBLE PRECISION, INTENT(out), DIMENSION(:,:) ::   t_zu
      DOUBLE PRECISION, INTENT(out), DIMENSION(:,:) ::   q_zu
      DOUBLE PRECISION, INTENT(out), DIMENSION(:,:) ::   Ubzu
      DOUBLE PRECISION, INTENT(out), DIMENSION(:,:), OPTIONAL :: qz0    ! roughness length [m]
      !
      INTEGER  :: ji, jj
      DOUBLE PRECISION :: zz0
      !!----------------------------------------------------------------------------------
      DO jj = 1, SIZE(psst,2)
         DO ji = 1, SIZE(psst,1)
            CALL FIRST_GUESS_COARE_SCLR( zt, zu, psst(ji,jj), t_zt(ji,jj), pssq(ji,jj), q_zt(ji,jj), U_zu(ji,jj), pcharn(ji,jj), &
                                        pus(ji,jj), pts(ji,jj), pqs(ji,jj), t_zu(ji,jj), q_zu(ji,jj), Ubzu(ji,jj),  pz0=zz0 )
            IF( PRESENT(qz0) ) qz0(ji,jj) = zz0
         END DO
      END DO
   END SUBROUTINE FIRST_GUESS_COARE_VCTR
   !!==============================================================================================

   !!==============================================================================================
   FUNCTION psi_m_coare_sclr( pzeta )
      !!----------------------------------------------------------------------------------
      !! ** Purpose: compute the universal profile stability function for momentum
      !!             COARE 3.0, Fairall et al. 2003
      !!             pzeta : stability paramenter, z/L where z is altitude
      !!                     measurement and L is M-O length
      !!       Stability function for wind speed and scalars matching Kansas and free
      !!       convection forms with weighting f convective form, follows Fairall et
      !!       al (1996) with profile constants from Grachev et al (2000) BLM stable
      !!       form from Beljaars and Holtslag (1991)
      !!
      !! ** Author: L. Brodeau, June 2016 / AeroBulk (https://github.com/brodeau/aerobulk/)
      !!----------------------------------------------------------------------------------
      DOUBLE PRECISION :: psi_m_coare_sclr
      DOUBLE PRECISION, INTENT(in) :: pzeta
      !!
      DOUBLE PRECISION :: zphi_m, zphi_c, zpsi_k, zpsi_c, zf, zc, zstb
      !!----------------------------------------------------------------------------------
      zphi_m = ABS(1.D0 - 15.D0*pzeta)**.25D0    !!Kansas unstable
      !
      zpsi_k = 2.D0*LOG((1.D0 + zphi_m)*0.5D0) + LOG((1.D0 + zphi_m*zphi_m)*0.5D0)   &
          - 2.D0*ATAN(zphi_m) + 0.5D0*rpi
      !
      zphi_c = ABS(1.D0 - 10.15D0*pzeta)**.3333D0                   !!Convective
      !
      zpsi_c = 1.5D0*LOG((1.D0 + zphi_c + zphi_c*zphi_c)/3.D0) &
              - 1.7320508D0*ATAN((1.D0 + 2.D0*zphi_c)/1.7320508D0) + 1.813799447D0
      !
      zf = pzeta*pzeta
      zf = zf/(1.D0 + zf)
      zc = MIN(50.D0, 0.35D0*pzeta)
      zstb = 0.5D0 + SIGN(0.5D0, pzeta)
      !
      psi_m_coare_sclr = (1.D0 - zstb) * ( (1.D0 - zf)*zpsi_k + zf*zpsi_c ) & ! (pzeta < 0)
                    -   zstb  * ( 1.D0 + 1.D0*pzeta     &                ! (pzeta > 0)
                                   + 0.6667D0*(pzeta - 14.28D0)/EXP(zc) + 8.525D0 )  !     "
      !!
   END FUNCTION psi_m_coare_sclr

   FUNCTION psi_m_coare_vctr( pzeta )
      !!----------------------------------------------------------------------------------
      !! ** Purpose: compute the universal profile stability function for momentum
      !!             COARE 3.0, Fairall et al. 2003
      !!             pzeta : stability paramenter, z/L where z is altitude
      !!                     measurement and L is M-O length
      !!       Stability function for wind speed and scalars matching Kansas and free
      !!       convection forms with weighting f convective form, follows Fairall et
      !!       al (1996) with profile constants from Grachev et al (2000) BLM stable
      !!       form from Beljaars and Holtslag (1991)
      !!
      !! ** Author: L. Brodeau, June 2016 / AeroBulk (https://github.com/brodeau/aerobulk/)
      !!----------------------------------------------------------------------------------
      DOUBLE PRECISION, DIMENSION(:,:), INTENT(in)             :: pzeta
      DOUBLE PRECISION, DIMENSION(SIZE(pzeta,1),SIZE(pzeta,2)) :: psi_m_coare_vctr
      !
      INTEGER  ::   ji, jj    ! dummy loop indices
      DOUBLE PRECISION :: zta, zphi_m, zphi_c, zpsi_k, zpsi_c, zf, zc, zstab
      !!----------------------------------------------------------------------------------
      DO jj = 1, SIZE(pzeta,2)
         DO ji = 1, SIZE(pzeta,1)
            !
            zta = pzeta(ji,jj)
            !
            zphi_m = ABS(1.D0 - 15.D0*zta)**.25D0    !!Kansas unstable
            !
            zpsi_k = 2.D0*LOG((1.D0 + zphi_m)*0.5D0) + LOG((1.D0 + zphi_m*zphi_m)*0.5D0)   &
                - 2.D0*ATAN(zphi_m) + 0.5D0*rpi
            !
            zphi_c = ABS(1.D0 - 10.15D0*zta)**.3333D0                   !!Convective
            !
            zpsi_c = 1.5D0*LOG((1.D0 + zphi_c + zphi_c*zphi_c)/3.D0) &
                    - 1.7320508D0*ATAN((1.D0 + 2.D0*zphi_c)/1.7320508D0) + 1.813799447D0
            !
            zf = zta*zta
            zf = zf/(1.D0 + zf)
            zc = MIN(50.D0, 0.35D0*zta)
            zstab = 0.5D0 + SIGN(0.5D0, zta)
            !
            psi_m_coare_vctr(ji,jj) = (1.D0 - zstab) * ( (1.D0 - zf)*zpsi_k + zf*zpsi_c ) & ! (zta < 0)
                               -   zstab     * ( 1.D0 + 1.D0*zta     &                ! (zta > 0)
                                        + 0.6667D0*(zta - 14.28D0)/EXP(zc) + 8.525D0 )  !     "
         END DO
      END DO
   END FUNCTION psi_m_coare_vctr
   !!==============================================================================================

   !!==============================================================================================
   FUNCTION psi_h_coare_sclr( pzeta )
      !!---------------------------------------------------------------------
      !! Universal profile stability function for temperature and humidity
      !! COARE 3.0, Fairall et al. 2003
      !!
      !! pzeta : stability paramenter, z/L where z is altitude measurement
      !!         and L is M-O length
      !!
      !! Stability function for wind speed and scalars matching Kansas and free
      !! convection forms with weighting f convective form, follows Fairall et
      !! al (1996) with profile constants from Grachev et al (2000) BLM stable
      !! form from Beljaars and Holtslag (1991)
      !!
      !! Author: L. Brodeau, June 2016 / AeroBulk
      !!         (https://github.com/brodeau/aerobulk/)
      !!----------------------------------------------------------------
      DOUBLE PRECISION :: psi_h_coare_sclr
      DOUBLE PRECISION, INTENT(in) :: pzeta
      !!
      DOUBLE PRECISION :: zphi_h, zphi_c, zpsi_k, zpsi_c, zf, zc, zstb
      !!----------------------------------------------------------------
      zphi_h = SQRT(ABS(1.D0 - 15.D0*pzeta))  !! Kansas unstable   (zphi_h = zphi_m**2 when unstable, zphi_m when stable)
      !
      zpsi_k = 2.D0*LOG((1.D0 + zphi_h)*0.5D0)
      !
      zphi_c = (ABS(1.D0 - 34.15D0*pzeta))**.3333D0   !! Convective
      !
      zpsi_c = 1.5D0*LOG((1.D0 + zphi_c + zphi_c*zphi_c)/3.D0) &
             -1.7320508D0*ATAN((1.D0 + 2.D0*zphi_c)/1.7320508D0) + 1.813799447D0
      !
      zf = pzeta*pzeta
      zf = zf/(1.D0 + zf)
      zc = MIN(50.D0,0.35D0*pzeta)
      zstb = 0.5D0 + SIGN(0.5D0, pzeta)
      !
      psi_h_coare_sclr = (1.D0-zstb) * ( (1.D0 - zf)*zpsi_k + zf*zpsi_c ) &
                           -zstb  * ( (ABS(1.D0 + 2.D0*pzeta/3.D0))**1.5D0     &
                                     + .6667D0*(pzeta - 14.28D0)/EXP(zc) + 8.525D0 )
      !!
   END FUNCTION psi_h_coare_sclr

   FUNCTION psi_h_coare_vctr( pzeta )
      !!---------------------------------------------------------------------
      !! Universal profile stability function for temperature and humidity
      !! COARE 3.0, Fairall et al. 2003
      !!
      !! pzeta : stability paramenter, z/L where z is altitude measurement
      !!         and L is M-O length
      !!
      !! Stability function for wind speed and scalars matching Kansas and free
      !! convection forms with weighting f convective form, follows Fairall et
      !! al (1996) with profile constants from Grachev et al (2000) BLM stable
      !! form from Beljaars and Holtslag (1991)
      !!
      !! Author: L. Brodeau, June 2016 / AeroBulk
      !!         (https://github.com/brodeau/aerobulk/)
      !!----------------------------------------------------------------
      DOUBLE PRECISION, DIMENSION(:,:), INTENT(in)             :: pzeta
      DOUBLE PRECISION, DIMENSION(SIZE(pzeta,1),SIZE(pzeta,2)) :: psi_h_coare_vctr
      !
      INTEGER  ::   ji, jj     ! dummy loop indices
      DOUBLE PRECISION :: zta, zphi_h, zphi_c, zpsi_k, zpsi_c, zf, zc, zstab
      !!----------------------------------------------------------------
      DO jj = 1, SIZE(pzeta,2)
         DO ji = 1, SIZE(pzeta,1)
            !
            zta = pzeta(ji,jj)
            !
            zphi_h = SQRT(ABS(1.D0 - 15.D0*zta))  !! Kansas unstable   (zphi_h = zphi_m**2 when unstable, zphi_m when stable)
            !
            zpsi_k = 2.D0*LOG((1.D0 + zphi_h)*0.5D0)
            !
            zphi_c = (ABS(1.D0 - 34.15D0*zta))**.3333D0   !! Convective
            !
            zpsi_c = 1.5D0*LOG((1.D0 + zphi_c + zphi_c*zphi_c)/3.D0) &
                   -1.7320508D0*ATAN((1.D0 + 2.D0*zphi_c)/1.7320508D0) + 1.813799447D0
            !
            zf = zta*zta
            zf = zf/(1.D0 + zf)
            zc = MIN(50.D0,0.35D0*zta)
            zstab = 0.5D0 + SIGN(0.5D0, zta)
            !
            psi_h_coare_vctr(ji,jj) = (1.D0 - zstab) * ( (1.D0 - zf)*zpsi_k + zf*zpsi_c ) &
                               -   zstab     * ( (ABS(1.D0 + 2.D0*zta/3.D0))**1.5D0     &
                                          + .6667D0*(zta - 14.28D0)/EXP(zc) + 8.525D0 )
         END DO
      END DO
   END FUNCTION psi_h_coare_vctr

END MODULE modz_aerobulks
