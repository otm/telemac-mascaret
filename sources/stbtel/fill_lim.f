!                       *******************
                        SUBROUTINE FILL_LIM
!                       *******************
!
     & (NPTFR,NPTFRX,NTRAC,LIHBOR,LIUBOR,LIVBOR,LITBOR,HBOR,UBOR,VBOR,
     &  CHBORD,TBOR,ATBOR,BTBOR,NBOR,OLD_NBOR,KP1BOR)
!
!***********************************************************************
! STBTEL
!***********************************************************************
!
!brief    Fills in the boundary conditions arrays based on the
!+        information from the coarsest mesh
!
!~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
!| NPTFR          |-->| Number of boundary points of the original mesh
!| NPTFRX         |-->| Maximum number of boundary points
!| NTRAC          |-->| Number of tracers
!| LIHBOR         |<->| Type of boundary conditions on depth
!| LIUBOR         |<->| Type of boundary conditions on u
!| LIVBOR         |<->| Type of boundary conditions on v
!| LITBOR         |<->| Type of boundary conditions on tracer
!| HBOR           |<->| Prescribed boundary condition on depth
!| UBOR           |<->| Prescribed boundary condition on velocity u
!| VBOR           |<->| Prescribed boundary condition on velocity v
!| CHBORD         |<->| Friction coefficient at boundary
!| TBOR           |<->| Prescribed boundary condition on velocity tracer
!| ATBOR          |<->| Boundary condition coefficient for tracer flux
!| BTBOR          |<->| Boundary condition coefficient for tracer flux
!| NBOR           |-->| Boundary numbering of the refined mesh
!| OLD_NBOR       |-->| Boundary numbering of the original mesh
!| KP1BOR         |-->| Points following and preceding a boundary point
!~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
!
      USE BIEF
      USE DECLARATIONS_SPECIAL
      IMPLICIT NONE

      INTEGER, INTENT(IN)   :: NPTFR
      INTEGER, INTENT(IN)   :: NPTFRX
      INTEGER, INTENT(IN)   :: NTRAC
      INTEGER,INTENT(INOUT) :: LIHBOR(NPTFRX)
      INTEGER,INTENT(INOUT) :: LIUBOR(NPTFRX)
      INTEGER,INTENT(INOUT) :: LIVBOR(NPTFRX)
      INTEGER,INTENT(INOUT) :: LITBOR(NPTFRX)
      DOUBLE PRECISION, INTENT(INOUT) :: UBOR(NPTFRX,2)
      DOUBLE PRECISION, INTENT(INOUT) :: VBOR(NPTFRX,2)
      DOUBLE PRECISION, INTENT(INOUT) :: HBOR(NPTFRX)
      DOUBLE PRECISION, INTENT(INOUT) :: CHBORD(NPTFRX)
      DOUBLE PRECISION, INTENT(INOUT) :: TBOR(NPTFRX)
      DOUBLE PRECISION, INTENT(INOUT) :: ATBOR(NPTFRX)
      DOUBLE PRECISION, INTENT(INOUT) :: BTBOR(NPTFRX)
      INTEGER, INTENT(IN) :: NBOR(NPTFRX)
      INTEGER, INTENT(IN) :: OLD_NBOR(NPTFRX)
      INTEGER, INTENT(IN) :: KP1BOR(NPTFRX)

      INTEGER I, P1, P2
      INTEGER VAL_P1, VAL_P2, IDX

      LOGICAL :: REORDER, FOUND
      INTEGER, ALLOCATABLE :: CONV(:)
      INTEGER J, NODE1, NODE2

! CHECK IF THE NBOR OF THE ORIGINAL MESH HAS THE SAME OREDERING AS
! THE ONE IN THE REFINED MESH THAT WENT THROUGH RANBO
      REORDER = .FALSE.
      DO I=1,NPTFR
        IF (OLD_NBOR(I) .NE. NBOR(2*(I-1)+1)) THEN
          REORDER = .TRUE.
          EXIT
        ENDIF
      ENDDO

      ALLOCATE(CONV(NPTFR))

      IF (REORDER) THEN
        DO I=1,NPTFR
          NODE1 = NBOR(2*I)
          NODE2 = NBOR(2*(I-1)+1)
          FOUND = .FALSE.
          DO J=1,NPTFR
            IF (OLD_NBOR(J).EQ.NODE1.OR.OLD_NBOR(J).EQ.NODE2) THEN
              FOUND = .TRUE.
              EXIT
            ENDIF
          ENDDO
          IF(.NOT.FOUND) THEN
            WRITE(LU,*) 'COULD NOT FIND', J, NODE1, NODE2
            CALL PLANTE(1)
          ENDIF
          CONV(I) = J
        ENDDO
      ELSE
        DO I=1,NPTFR
          CONV(I) = I
        ENDDO
      ENDIF

      LIHBOR(1:NPTFR*2:2)=LIHBOR(CONV)
      LIUBOR(1:NPTFR*2:2)=LIUBOR(CONV)
      LIVBOR(1:NPTFR*2:2)=LIVBOR(CONV)

      CHBORD(1:NPTFR*2:2)=CHBORD(CONV)
      HBOR(1:NPTFR*2:2)=HBOR(CONV)
      UBOR(1:NPTFR*2:2,1)=UBOR(CONV,1)
      VBOR(1:NPTFR*2:2,1)=VBOR(CONV,1)
      UBOR(1:NPTFR*2:2,2)=UBOR(CONV,2)
      VBOR(1:NPTFR*2:2,2)=VBOR(CONV,2)

      IF (NTRAC.GT.0) THEN
        LITBOR(1:NPTFR*2:2)=LITBOR(CONV)
        TBOR(1:NPTFR*2:2)=TBOR(CONV)
        ATBOR(1:NPTFR*2:2)=ATBOR(CONV)
        BTBOR(1:NPTFR*2:2)=BTBOR(CONV)
      ENDIF
!
      DEALLOCATE(CONV)
!
!     FILLING THE POINT IN THE MIDDLE OF EACH SEGMENT
      DO I=1,NPTFR

        P1 = 2*I-1
!     USING KP1BOR TO IDENTIFY THE NEXT POINT (NOT 2*I+1 FOR I=NPTFR)
        P2 = KP1BOR(2*I)
!
        VAL_P1 = LIHBOR(P1)*1000 +
     &           LIUBOR(P1)*100  +
     &           LIVBOR(P1)*10
        IF(NTRAC.GT.0) THEN
          VAL_P1 = VAL_P1 + LITBOR(P1)*1
        ENDIF

        VAL_P2 = LIHBOR(P2)*1000 +
     &           LIUBOR(P2)*100  +
     &           LIVBOR(P2)*10
        IF(NTRAC.GT.0) THEN
          VAL_P2 = VAL_P2 + LITBOR(P2)*1
        ENDIF

!       IF SAME TYPE ON EACH POINT APPLY THE SAME TYPE
        IF (VAL_P1.EQ.VAL_P2) THEN
          IDX = P1
!       IF ONE OF THE POINTS IS A SOLID POINT TAKING THAT ONE
        ELSE IF(LIHBOR(P1).EQ.2) THEN
          IDX = P1
        ELSEIF(LIHBOR(P2).EQ.2) THEN
          IDX = P2
!       OTHERWISE TAKING THE SMALLEST ONE
        ELSEIF(VAL_P1.LT.VAL_P2) THEN
          IDX = P1
        ELSE
          IDX = P2
        ENDIF

        LIHBOR(2*I)=LIHBOR(IDX)
        LIUBOR(2*I)=LIUBOR(IDX)
        LIVBOR(2*I)=LIVBOR(IDX)
        CHBORD(2*I)=CHBORD(IDX)
        HBOR(2*I)=HBOR(IDX)
        UBOR(2*I,1)=UBOR(IDX,1)
        VBOR(2*I,1)=VBOR(IDX,1)
        UBOR(2*I,2)=UBOR(IDX,2)
        VBOR(2*I,2)=VBOR(IDX,2)
!
        IF(NTRAC.GT.0) THEN
          LITBOR(2*I)=LITBOR(IDX)
          ATBOR(2*I)=ATBOR(IDX)
          BTBOR(2*I)=BTBOR(IDX)
        ENDIF
!
      ENDDO
!
      END SUBROUTINE
