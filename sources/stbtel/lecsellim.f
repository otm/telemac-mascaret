!                   ********************
                    SUBROUTINE LECSELLIM
!                   ********************
!
     & (NLIM,LIHBOR,LIUBOR,LIVBOR,HBOR,UBOR,VBOR,CHBORD,LITBOR,TBOR,
     &  ATBOR,BTBOR,NBOR,NPMAX,NPTFR,NCOLOR)
!
!***********************************************************************
! STBTEL
!***********************************************************************
!
!brief    Read a boundary conditions file and stores its data in the
!+        given arrays
!
!~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
!| NLIM           |-->| Logical unit of boundary conditions file
!| LIHBOR         |<->| Type of boundary conditions on depth
!| LIUBOR         |<->| Type of boundary conditions on u
!| LIVBOR         |<->| Type of boundary conditions on v
!| HBOR           |<->| Prescribed boundary condition on depth
!| UBOR           |<->| Prescribed boundary condition on velocity u
!| VBOR           |<->| Prescribed boundary condition on velocity v
!| CHBORD         |<->| Friction coefficient at boundary
!| LITBOR         |<->| Type of boundary conditions on tracer
!| TBOR           |<->| Prescribed boundary condition on tracer
!| ATBOR          |<->| Boundary condition coefficient for tracer flux
!| BTBOR          |<->| Boundary condition coefficient for tracer flux
!| NBOR           |<->| Boundary numbering
!| NPMAX          |-->| Maximum number of points
!| NPTFR          |<--| Number of boundary points
!| NCOLOR         |<--| Array of node colours
!~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
!
      USE BIEF
      USE DECLARATIONS_SPECIAL
      USE DECLARATIONS_STBTEL, ONLY: FFORMAT, TYP_BND_ELEM
      USE INTERFACE_HERMES
      IMPLICIT NONE
!
!+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
!
      INTEGER, INTENT(IN)    :: NLIM
      INTEGER, INTENT(IN)    :: NPMAX
      INTEGER, INTENT(INOUT) :: LIHBOR(NPMAX)
      INTEGER, INTENT(INOUT) :: LIUBOR(NPMAX)
      INTEGER, INTENT(INOUT) :: LIVBOR(NPMAX)
      INTEGER, INTENT(INOUT) :: LITBOR(NPMAX)
      DOUBLE PRECISION, INTENT(INOUT) :: HBOR(NPMAX)
      INTEGER, INTENT(INOUT) :: NBOR(NPMAX)
      INTEGER, INTENT(OUT) :: NPTFR
      DOUBLE PRECISION, INTENT(INOUT) :: UBOR(NPMAX,2)
      DOUBLE PRECISION, INTENT(INOUT) :: VBOR(NPMAX,2)
      DOUBLE PRECISION, INTENT(INOUT) :: CHBORD(NPMAX)
      DOUBLE PRECISION, INTENT(INOUT) :: TBOR(NPMAX)
      DOUBLE PRECISION, INTENT(INOUT) :: ATBOR(NPMAX)
      DOUBLE PRECISION, INTENT(INOUT) :: BTBOR(NPMAX)
      INTEGER, INTENT(INOUT) :: NCOLOR(NPMAX)
!
!+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
!
      INTEGER :: I, IERR
!
      CALL GET_BND_NPOIN(FFORMAT,NLIM,TYP_BND_ELEM,NPTFR,IERR)

      CALL GET_BND_VALUE(FFORMAT, NLIM, TYP_BND_ELEM, NPTFR, LIHBOR,
     &                   LIUBOR, LIVBOR, HBOR, UBOR(:,1), VBOR(:,1),
     &                   CHBORD, .TRUE., LITBOR, TBOR, ATBOR, BTBOR,
     &                   NPTFR, IERR)
      WRITE(LU,*) 'NPTFR FROM LECSELLIM ', NPTFR
      CALL CHECK_CALL(IERR, 'LECSELIM:GET_BND_VALUE')

      CALL GET_BND_NUMBERING(FFORMAT, NLIM, TYP_BND_ELEM, NPTFR,
     &                       NBOR, IERR)
      CALL CHECK_CALL(IERR, 'LECSELIM:GET_BND_NUMBERING')

      DO I=1,NPTFR
        NCOLOR(I) = I
      ENDDO
      UBOR(:,2) = UBOR(:, 1)
      VBOR(:,2) = VBOR(:, 1)

      END SUBROUTINE
