!                   **********************
                    SUBROUTINE WRITESELLIM
!                   **********************
!
     & (NLIM,LIHBOR,LIUBOR,LIVBOR,HBOR,UBOR,VBOR,CHBORD,LITBOR,TBOR,
     &  ATBOR,BTBOR,NBOR,NPMAX,NPTFR)
!
!***********************************************************************
! STBTEL
!***********************************************************************
!
!brief    Write boundary conditions arrays in a given file
!
!~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
!| NLIM           |-->| Logical unit of a boundary conditions file
!| LIHBOR         |<->| Type of boundary conditions on depth
!| LIUBOR         |<->| Type of boundary conditions on u
!| LIVBOR         |<->| Type of boundary conditions on v
!| HBOR           |<->| Prescribed boundary condition on depth
!| UBOR           |<->| Prescribed boundary condition on velocity u
!| VBOR           |<->| Prescribed boundary condition on velocity v
!| CHBORD         |<->| Friction coefficient at boundary
!| LITBOR         |<->| Type of boundary conditions on tracer
!| TBOR           |<->| Prescribed boundary condition on velocity tracer
!| ATBOR          |<->| Boundary condition coefficient for tracer flux
!| BTBOR          |<->| Boundary condition coefficient for tracer flux
!| NBOR           |<->| Boundary numbering
!| NPMAX          |-->| Maximum number of points
!| NPTFR          |-->| Number of boundary points
!~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
!
      USE DECLARATIONS_SPECIAL
      USE DECLARATIONS_STBTEL, ONLY: OUT_FORMAT, TYP_BND_ELEM
!
      IMPLICIT NONE
!
!
!+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
!
      INTEGER, INTENT(IN)    :: NLIM
      INTEGER, INTENT(IN)    :: NPTFR
      INTEGER, INTENT(IN)    :: NPMAX
      INTEGER, INTENT(INOUT) :: LIUBOR(NPMAX)
      INTEGER, INTENT(INOUT) :: LIVBOR(NPMAX)
      INTEGER, INTENT(INOUT) :: LIHBOR(NPMAX)
      INTEGER, INTENT(INOUT) :: LITBOR(NPMAX)
      INTEGER, INTENT(INOUT) :: NBOR(NPMAX)
      DOUBLE PRECISION,  INTENT(INOUT) :: UBOR(NPMAX)
      DOUBLE PRECISION,  INTENT(INOUT) :: VBOR(NPMAX)
      DOUBLE PRECISION,  INTENT(INOUT) :: HBOR(NPMAX)
      DOUBLE PRECISION,  INTENT(INOUT) :: CHBORD(NPMAX)
      DOUBLE PRECISION,  INTENT(INOUT) :: TBOR(NPMAX)
      DOUBLE PRECISION,  INTENT(INOUT) :: ATBOR(NPMAX)
      DOUBLE PRECISION,  INTENT(INOUT) :: BTBOR(NPMAX)
!
!+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
!
      INTEGER I, IERR
      INTEGER, ALLOCATABLE :: NCOLOR(:)

      ALLOCATE(NCOLOR(NPTFR))
      DO I=1,NPTFR
        NCOLOR(I) = I
      ENDDO
!
      CALL SET_BND(OUT_FORMAT, NLIM, TYP_BND_ELEM, NPTFR, 1, NBOR,
     &             NPTFR, LIHBOR, LIUBOR, LIVBOR, HBOR, UBOR, VBOR,
     &             CHBORD, LITBOR, TBOR, ATBOR, BTBOR, NCOLOR, IERR)
      CALL CHECK_CALL(IERR, 'WRITESELLIM:SET_BND')

      DEALLOCATE(NCOLOR)
!
      END SUBROUTINE
