!                   **********************
                    SUBROUTINE USER_FLOT3D
!                   **********************
!
     &(XFLOT,YFLOT,ZFLOT,NFLOT,NFLOT_MAX,X,Y,Z,IKLE,NELEM,NELMAX,NPOIN,
     & NPLAN,TAGFLO,CLSFLO,SHPFLO,SHZFLO,ELTFLO,ETAFLO,MESH3D,LT,NIT,AT)
!
!***********************************************************************
! TELEMAC3D
!***********************************************************************
!
!brief    This subroutine is called at every time step, and the user can
!+        add or remove particles as in the example given
!
!history  J-M HERVOUET (EDF R&D, LNHE)
!+        26/02/2013
!+        V6P3
!+    First version.
!
!history  Y. AUDOUIN (LNHE)
!+        22/10/18
!+        V8P1
!+   Creation from FLOT3D
!
!~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
!| AT             |-->| TIME
!| CLSFLO         |<->| CLASS FOR EACH FLOAT
!| ELTFLO         |<->| NUMBERS OF ELEMENTS WHERE ARE THE FLOATS
!| ETAFLO         |<->| LEVELS WHERE ARE THE FLOATS
!| IKLE           |-->| CONNECTIVITY TABLE
!| LT             |-->| CURRENT TIME STEP
!| MESH3D         |<->| 3D MESH STRUCTURE
!| NELEM          |-->| NUMBER OF ELEMENT
!| NELMAX         |-->| MAXIMUM NUMBER OF ELEMENTS
!| NFLOT          |-->| NUMBER OF FLOATS
!| NFLOT_MAX      |-->| MAXIMUM NUMBER OF FLOATS
!| NIT            |-->| NUMBER OF TIME STEPS
!| NPLAN          |-->| NUMBER OF PLANES
!| NPOIN          |-->| NUMBER OF POINTS IN THE MESH
!| SHPFLO         |<->| BARYCENTRIC COORDINATES OF FLOATS IN THEIR
!|                |   | ELEMENTS.
!| SHZFLO         |<->| BARYCENTRIC COORDINATES OF FLOATS IN THEIR LEVEL
!| TAGFLO         |<->| TAG FOR EACH FLOAT
!| X              |-->| ABSCISSAE OF POINTS IN THE MESH
!| XFLOT          |<->| ABSCISSAE OF FLOATING BODIES
!| Y              |-->| ORDINATES OF POINTS IN THE MESH
!| YFLOT          |<->| ORDINATES OF FLOATING BODIES
!| Z              |-->| ELEVATIONS OF POINTS IN THE MESH
!| ZFLOT          |<->| ELEVATIONS OF FLOATING BODIES
!~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
!
      USE BIEF
      USE STREAMLINE, ONLY : ADD_PARTICLE,DEL_PARTICLE
      USE DECLARATIONS_TELEMAC3D, ONLY : PROTYP,LONGIT,LATIT
      USE INTERFACE_TELEMAC3D, EX_USER_FLOT3D => USER_FLOT3D
!
      IMPLICIT NONE
!
!+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
!
      INTEGER, INTENT(IN)             :: NPOIN,NIT,NFLOT_MAX,LT,NPLAN
      INTEGER, INTENT(IN)             :: NELEM,NELMAX
      INTEGER, INTENT(IN)             :: IKLE(NELMAX,*)
      INTEGER, INTENT(INOUT)          :: NFLOT
      INTEGER, INTENT(INOUT)          :: TAGFLO(NFLOT_MAX)
      INTEGER, INTENT(INOUT)          :: CLSFLO(NFLOT_MAX)
      INTEGER, INTENT(INOUT)          :: ELTFLO(NFLOT_MAX)
      INTEGER, INTENT(INOUT)          :: ETAFLO(NFLOT_MAX)
      DOUBLE PRECISION, INTENT(IN)    :: X(NPOIN),Y(NPOIN),Z(NPOIN),AT
      DOUBLE PRECISION, INTENT(INOUT) :: XFLOT(NFLOT_MAX)
      DOUBLE PRECISION, INTENT(INOUT) :: YFLOT(NFLOT_MAX)
      DOUBLE PRECISION, INTENT(INOUT) :: ZFLOT(NFLOT_MAX)
      DOUBLE PRECISION, INTENT(INOUT) :: SHPFLO(3,NFLOT_MAX)
      DOUBLE PRECISION, INTENT(INOUT) :: SHZFLO(NFLOT_MAX)
      TYPE(BIEF_MESH) , INTENT(INOUT) :: MESH3D
!
!+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
!
!     DECLARATIONS FOR AN EXAMPLE TO GIVE COORDINATES OF DROGUES IN
!     LONGITUDE/LATITUDE, NOT MERCATOR PROJECTION
!
!     DOUBLE PRECISION, PARAMETER :: R=6.37D6
!     DOUBLE PRECISION PS4,TAN1,TAN2,LONGIRAD,LONG,LAT,XXX,YYY
!
!     INTRINSIC LOG,TAN,ATAN
!
!-----------------------------------------------------------------------
!
!
!     EXAMPLE : AT ITERATION 1 AND EVERY 10 ITERATIONS BEFORE 600 S
!               A PARTICLE IS RELEASED WITH COORDINATES
!               X=-220.
!               Y=400.D0+LT/3.D0
!               AND TAG NUMBER LT (LT IS THE TIME STEP NUMBER)
!
!     IF(LT.LE.600.AND.(10*(LT/10).EQ.LT.OR.LT.EQ.1)) THEN
!       CALL ADD_PARTICLE(-220.D0,400.D0+LT/3.D0,259.D0+LT/100.D0,
!    &                    LT,1,NFLOT,
!    &                    NFLOT_MAX,XFLOT,YFLOT,ZFLOT,TAGFLO,CLSFLO,
!    &                    SHPFLO,SHZFLO,ELTFLO,ETAFLO,MESH3D,NPLAN,
!    &                    0.D0,0.D0,0.D0,0.D0,0,0)
!     ENDIF
!
!     EXAMPLE : PARTICLE WITH TAG 20 REMOVED AT ITERATION 600
!
!     IF(LT.EQ.600) THEN
!        CALL DEL_PARTICLE(20,NFLOT,NFLOT_MAX,
!    &                     XFLOT,YFLOT,ZFLOT,TAGFLO,CLSFLO,SHPFLO,SHZFLO,
!    &                     ELTFLO,ETAFLO,MESH%TYPELM)
!     ENDIF
!
!-----------------------------------------------------------------------
!
!     EXAMPLE : AT ITERATION 1 AND EVERY 10 ITERATIONS BEFORE 600 S
!               A PARTICLE IS RELEASED WITH COORDINATES
!               X = -7.5D0
!               Y = 47.5D0+LT/3000.D0
!               COORDINATES GIVEN IN LONGITUDE/LATITUDE
!               AND TAG NUMBER LT (LT IS THE TIME STEP NUMBER)
!               CONVERSION IN MERCATOR PROJECTION IS TO BE KEPT
!               AS TELEMAC-3D WORKS IN THAT PROJECTION AT THAT TIME
!
!     PS4 = ATAN(1.D0)
!     LONGIRAD = LONGIT*PS4/45.D0
!     TAN2 = TAN(LATIT*PS4/90.D0+PS4)
!
!     IF(LT.LE.600.AND.(10*(LT/10).EQ.LT.OR.LT.EQ.1)) THEN
!!      COORDINATES OF DROGUES TO BE RELEASED IN LONGITUDE/LATITUDE
!       LONG = -7.5D0
!       LAT  = 47.5D0+LT/3000.D0
!!      CONVERSION FROM LONGITUDE/LATITUDE TO MERCATOR PROJECTION
!!      DO NOT CHANGE THE 3 FOLLOWING LINES
!       XXX  = R*(LONG*PS4/45.D0-LONGIRAD)
!       TAN1 = TAN(LAT*PS4/90.D0+PS4)
!       YYY  = R*(LOG(TAN1)-LOG(TAN2))
!
!       CALL ADD_PARTICLE(XXX,YYY,259.D0+LT/100.D0,
!    &                    LT,1,NFLOT,
!    &                    NFLOT_MAX,XFLOT,YFLOT,ZFLOT,TAGFLO,CLSFLO,
!    &                    SHPFLO,SHZFLO,ELTFLO,ETAFLO,MESH3D,NPLAN,
!    &                    0.D0,0.D0,0.D0,0.D0,0,0)
!     ENDIF
!
!-----------------------------------------------------------------------
!
      RETURN
      END
