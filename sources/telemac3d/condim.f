!                   *****************
                    SUBROUTINE CONDIM
!                   *****************
!
!
!***********************************************************************
! TELEMAC3D
!***********************************************************************
!
!brief    INITIALISES VELOCITY, DEPTH AND TRACERS.
!
!history  JACEK A. JANKOWSKI PINXIT
!+        **/03/1999
!+
!+   FORTRAN95 VERSION
!
!history  J-M HERVOUET(LNH)
!+        11/12/2000
!+        V5P1
!+   TELEMAC 3D VERSION 5.1
!
!history
!+        20/04/2007
!+
!+   ADDED INITIALISATION OF DPWAVE
!
!history
!+        23/01/2009
!+
!+   ADDED CHECK OF ZSTAR
!
!history
!+        16/03/2010
!+
!+   NEW OPTIONS FOR BUILDING THE MESH IN CONDIM, SEE BELOW
!
!history  J-M HERVOUET(LNHE)
!+        05/05/2010
!+        V6P0
!+   SUPPRESSED INITIALISATION OF DPWAVE
!
!history  N.DURAND (HRW), S.E.BOURBAN (HRW)
!+        13/07/2010
!+        V6P0
!+   Translation of French comments within the FORTRAN sources into
!+   English comments
!
!history  N.DURAND (HRW), S.E.BOURBAN (HRW)
!+        21/08/2010
!+        V6P0
!+   Creation of DOXYGEN tags for automated documentation and
!+   cross-referencing of the FORTRAN sources
!
!history  M.S.TURNBULL (HRW), N.DURAND (HRW), S.E.BOURBAN (HRW)
!+        C.-T. PHAM (LNHE)
!+        19/07/2012
!+        V6P2
!+   Addition of the TPXO tidal model by calling CONDI_TPXO
!+   (the TPXO model being coded in module TPXO)
!
!history  C.-T. PHAM (LNHE), M.S.TURNBULL (HRW)
!+        02/11/2012
!+        V6P3
!+   Correction of bugs when initialising velocity with TPXO
!+   or when sea levels are referenced with respect to Chart Datum (CD)
!
!history  C.-T. PHAM (LNHE)
!+        03/09/2015
!+        V7P1
!+   Change in the number of arguments when calling CONDI_TPXO
!
!history  C.-T. PHAM (LNHE)
!+        24/03/2017
!+        V7P3
!+   Split of CONDIM: calls of subroutines to define initial conditions
!    for different variables, one subroutine pro variable
!
!~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
!~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
!
      USE INTERFACE_TELEMAC3D, EX_CONDIM => CONDIM
      USE DECLARATIONS_TELEMAC
      USE DECLARATIONS_TELEMAC3D
      USE METEO_TELEMAC, ONLY: PATMOS
      USE BIEF
      USE TPXO
!
!     USE DECLARATIONS_SPECIAL
      IMPLICIT NONE
!
!+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-
!
!
!+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-
!
      INTEGER IPLAN,I,J,ISIGMA,IZPLAN
      DOUBLE PRECISION DU,DL,TMP
!
!-----------------------------------------------------------------------
!
!     ORIGIN OF TIME USING THE KEYWORD INITIAL TIME FROM V8P5
!
      IF(.NOT.SUIT2) AT = AT0
!
!     INITIALISES H, THE WATER DEPTH
!
      IF(.NOT.SUIT2) THEN
!
        IF(CDTINI(1:10).EQ.'COTE NULLE'.OR.
     &     CDTINI(1:14).EQ.'ZERO ELEVATION') THEN
          CALL OS('X=C     ', X=H, C=0.D0)
          CALL OV('X=X-Y   ', X=H%R, Y=Z, DIM1=NPOIN2)
        ELSEIF(CDTINI(1:14).EQ.'COTE CONSTANTE'.OR.
     &         CDTINI(1:18).EQ.'CONSTANT ELEVATION') THEN
          CALL OS('X=C     ', X=H, C=COTINI)
          CALL OV('X=X-Y   ', X=H%R, Y=Z, DIM1=NPOIN2)
        ELSEIF(CDTINI(1:13).EQ.'HAUTEUR NULLE'.OR.
     &         CDTINI(1:10).EQ.'ZERO DEPTH') THEN
          CALL OS('X=C     ', X=H, C=0.D0)
        ELSEIF(CDTINI(1:17).EQ.'HAUTEUR CONSTANTE'.OR.
     &         CDTINI(1:14).EQ.'CONSTANT DEPTH') THEN
          CALL OS('X=C     ', X=H, C=HAUTIN)
        ELSEIF(CDTINI(1:25).EQ.'ALTIMETRIE SATELLITE TPXO'.OR.
     &         CDTINI(1:24).EQ.'TPXO SATELLITE ALTIMETRY') THEN
          CALL OS('X=-Y    ', X=H, Y=ZF)
          CALL CONDI_TPXO(NPOIN2,MESH2D%NPTFR,MESH2D%NBOR%I,
     &                    X2%R,Y2%R,H%R,U2D%R,V2D%R,
     &                    LIHBOR%I,LIUBOL%I,KENT,KENTU,
     &                    GEOSYST,NUMZONE,T3DL93,LATIT,LONGIT,
     &                    T3D_FILES,T3DBB1,T3DBB2,
     &                    MARDAT,MARTIM,INTMICON,MSL,
     &                    TIDALTYPE,BOUNDARY_COLOUR,ICALHWG,
     &                    I_ORIG,J_ORIG,HMIN_VIT_IC,VITINI_TPXO,
     &                    VIT_IN_T3DBB2,PRESSBC,PATMOS%R,GRAV,RHO0)
        ELSEIF(CDTINI(1:13).EQ.'PARTICULIERES'.OR.
     &         CDTINI(1:10).EQ.'PARTICULAR'.OR.
     &         CDTINI(1:07).EQ.'SPECIAL') THEN
          ! USER FUNCTION
          CALL USER_CONDI3D_H
        ELSE
          WRITE(LU,*) 'CONDIM: INITIAL CONDITION UNKNOWN: ',CDTINI
          CALL PLANTE(1)
          STOP
        ENDIF
      ELSE
        WRITE(LU,*) 'DEPTH IS READ IN THE BINARY FILE 1'
      ENDIF
!
!     CLIPS H
!
      DO I=1,NPOIN2
        H%R(I)=MAX(H%R(I),0.D0)
      ENDDO
!
      CALL OS('X=Y     ', X=HN, Y=H)
!
!-----------------------------------------------------------------------
!
!     DATA TO BUILD VERTICAL COORDINATES IN CALCOT
!
!     STANDARD BELOW IS: EVENLY SPACED PLANES, NO OTHER DATA REQUIRED
!
!     MESH TRANSFORMATION 4 CAN BE STEERED BY TRANSF_COEF KEYWORD
!     WITHOUT IMPLEMENTING ANYTHING

!     COPY MESH TRANSFER FROM STEERING FILE
      DO IPLAN=1,NPLAN
        TRANSF_PLANE%I(IPLAN)=TRANSF_TMP(IPLAN)
      ENDDO

      IF(TRANSF.EQ.4) THEN
        ! MESH STRETCHING USING TANH PROFILE (AS IN GOTM)
        DO IPLAN=1,NPLAN
          TRANSF_PLANE%I(IPLAN)=4
        ENDDO
        IF(ANY(TRANSF_COEF.GT.0.D0)) THEN
          DL = TRANSF_COEF(1)
          DU = TRANSF_COEF(2)
          DO IPLAN=1,NPLAN
            TMP = DBLE(IPLAN-1)/DBLE(NPLAN-1)
            ZSTAR%R(IPLAN) = (TANH((DL+DU)*TMP-DL) + TANH(DL))
     &                      /(TANH(DL)+TANH(DU))
          ENDDO
        ELSE
          !WITH BOTH COEFFICIENTS 0, THE SIGMA COORDINATES ARE EQUALLY DIVIDED
          DO IPLAN=1,NPLAN
            ZSTAR%R(IPLAN) = DBLE(IPLAN-1)/DBLE(NPLAN-1)
          ENDDO
        ENDIF
      ENDIF
!
!     APPLY LEVELS FROM THE STEERING FILE
!
      IF(NZSTAR.GT.0.OR.NZPLANE.GT.0) THEN
        ISIGMA = 0
        IZPLAN = 0
        DO IPLAN=1,NPLAN
          IF(TRANSF_TMP(IPLAN).EQ.2) THEN
            ISIGMA = ISIGMA+1
            ZSTAR%R(IPLAN) = ZSTAR_TMP(ISIGMA)
          ELSEIF(TRANSF_TMP(IPLAN).EQ.3) THEN
            IZPLAN = IZPLAN+1
            ZPLANE%R(IPLAN) = ZPLANE_TMP(IZPLAN)
          ENDIF
        ENDDO
      ENDIF

      DEALLOCATE(TRANSF_TMP)
      DEALLOCATE(ZSTAR_TMP)
      DEALLOCATE(ZPLANE_TMP)

!     OTHERWISE: USER_MESH_TRANSF
      ! USER FUNCTION
      CALL USER_MESH_TRANSF

!     FURTHER CHECKS

!     MESH TRANSFER THAT SHOULD NOT BE MIXED
      IF(ANY(TRANSF_PLANE%I.EQ.0).AND.ANY(TRANSF_PLANE%I.NE.0)) THEN
        WRITE(LU,*) 'MESH TRANSFER = 0 MUST BE USED FOR ',
     &    'ALL VERTICAL LEVELS OR NONE'
        CALL PLANTE(1)
      ENDIF
      IF(ANY(TRANSF_PLANE%I.EQ.4).AND.ANY(TRANSF_PLANE%I.NE.4)) THEN
        WRITE(LU,*) 'MESH TRANSFER = 4 MUST BE USED FOR ',
     &    'ALL VERTICAL LEVELS OR NONE'
        CALL PLANTE(1)
      ENDIF
      IF(ANY(TRANSF_PLANE%I.EQ.5).AND.ANY(TRANSF_PLANE%I.NE.5)) THEN
        WRITE(LU,*) 'MESH TRANSFER = 5 MUST BE USED FOR ',
     &    'ALL VERTICAL LEVELS OR NONE'
        CALL PLANTE(1)
      ENDIF

!     MESH TRANSFER THAT SHOULD NOT BE ADJACENT
      DO I=1,NPLAN-1
        IF((TRANSF_PLANE%I(I).EQ.1.AND.TRANSF_PLANE%I(I+1).EQ.2)
     & .OR.(TRANSF_PLANE%I(I).EQ.2.AND.TRANSF_PLANE%I(I+1).EQ.1))
     &   THEN
          WRITE(LU,*) 'MESH TRANSFER = 1 AND MESH TRANSFER = 2',
     &      'IN LEVELS ', I, I+1, ' MUST NOT BE ADJACENT'
          CALL PLANTE(1)
        ENDIF
      ENDDO

!     INCREASING VALUES OF Z AND SIGMA
      DO I=1,NPLAN-1
        IF((TRANSF_PLANE%I(I).EQ.2.AND.TRANSF_PLANE%I(I+1).EQ.2)
     &  .AND.ZSTAR%R(I+1).LE.ZSTAR%R(I)) THEN
          WRITE(LU,*) 'SIGMA COORDINATE (ZSTAR) DOES NOT INCREASE ',
     &     'BETWEEN LEVELS ', I, I+1
          CALL PLANTE(1)
        ENDIF
      ENDDO
      DO I=2,NPLAN-2
        IF((TRANSF_PLANE%I(I).EQ.3.AND.TRANSF_PLANE%I(I+1).EQ.3)
     &  .AND.ZPLANE%R(I+1).LE.ZPLANE%R(I)) THEN
          WRITE(LU,*) 'Z COORDINATE (ZPLANE) DOES NOT INCREASE ',
     &     'BETWEEN LEVELS ', I, I+1
          CALL PLANTE(1)
        ENDIF
      ENDDO

!     SIGMA COORDINATES BETWEEN 0 AND 1
      DO I=1,NPLAN
        IF(TRANSF_PLANE%I(I).EQ.2.AND.
     &     (ZSTAR%R(I).LT.0.D0.OR.ZSTAR%R(I).GT.1.D0)) THEN
          WRITE(LU,*)
     &      'SIGMA COORDINATE (ZSTAR) MUST BE BETWEEN 0 AND 1',
     &     ' IN LEVEL ',I
          CALL PLANTE(1)
        ENDIF
      ENDDO

!     SIGMA COORDINATES AT 0 AND 1 AT THE BOTTOM AND TOP
      IF(TRANSF_PLANE%I(1).EQ.2.AND.(ABS(ZSTAR%R(1)).GT.1.D-9)) THEN
        WRITE(LU,*) 'SIGMA COORDINATE AT THE BOTTOM AUTOMATICALLY ',
     &              'SET TO 0'
        ZSTAR%R(1) = 0.D0
      ENDIF
      IF(TRANSF_PLANE%I(NPLAN).EQ.2.AND.
     &   (ABS(ZSTAR%R(NPLAN)-1.D0).GT.1.D-9)) THEN
        WRITE(LU,*) 'SIGMA COORDINATE AT THE SURFACE AUTOMATICALLY ',
     &              'SET TO 1'
        ZSTAR%R(NPLAN) = 1.D0
      ENDIF
!
!***********************************************************************
!
!     COMPUTES ELEVATIONS
!     IF IT IS A CONTINUATION, WILL BE DONE AFTER CALLING 'SUITE'
!
      IF(DEBU) CALL CALCOT(Z,H%R)
!
!***********************************************************************
!
!     INITIALISES VELOCITIES
!
      IF(SUIT2) THEN
        DO I=1,NPLAN
          DO J=1,NPOIN2
            U%R((I-1)*NPOIN2+J)=U2D%R(J)
            V%R((I-1)*NPOIN2+J)=V2D%R(J)
          ENDDO
        ENDDO
      ELSEIF(CDTINI(1:25).EQ.'ALTIMETRIE SATELLITE TPXO'.OR.
     &       CDTINI(1:24).EQ.'TPXO SATELLITE ALTIMETRY') THEN
!
!     U2D, V2D PREVIOUSLY COMPUTED WITH CONDI_TPXO
!
        DO I=1,NPLAN
          DO J=1,NPOIN2
            U%R((I-1)*NPOIN2+J)=U2D%R(J)
            V%R((I-1)*NPOIN2+J)=V2D%R(J)
          ENDDO
        ENDDO
      ELSE
        CALL OS('X=0     ' , X=U)
        CALL OS('X=0     ' , X=V)
      ENDIF
!
      CALL OS('X=0     ' , X=W)
      ! USER FUNCTION
      CALL USER_CONDI3D_UVW
!
!-----------------------------------------------------------------------
!
!     INITIALISES TRACERS
!
      IF(NTRAC.GT.0) THEN
        IF(SUIT2) THEN
          DO I=1,NTRAC
            DO IPLAN=1,NPLAN
              DO J=1,NPOIN2
                TA%ADR(I)%P%R((IPLAN-1)*NPOIN2+J) =
     &          TRAV2%ADR(ADR_TRAV2+I)%P%R(J)
              ENDDO
            ENDDO
          ENDDO
        ELSE
          DO I=1,NTRAC
            CALL OS( 'X=C     ', X=TA%ADR(I)%P, C=TRAC0(I))
          ENDDO
        ENDIF
      ENDIF
      ! USER FUNCTION
      CALL USER_CONDI3D_TRAC
!
!-----------------------------------------------------------------------
!     INITIALISES THE K-EPSILON MODEL (OPTIONAL)
!     WHEN DONE: AKEP = .FALSE.
!
      AKEP=.TRUE.
      ! USER FUNCTION
      CALL USER_CONDI3D_KEP
!
!-----------------------------------------------------------------------
!
!     INITIALISES THE PRESSURE FIELDS TO 0. IF NON-HYDROSTATIC VERSION
!
      IF(NONHYD) THEN
        CALL OS('X=C     ',X=DP,C=0.D0)
        WRITE (LU,*) 'CONDIM: DYNAMIC PRESSURE INITIALISED TO ZERO'
        CALL OS('X=C     ',X=PH,C=0.D0)
        WRITE (LU,*) '        HYDROSTATIC PRESSURE INITIALISED TO ZERO.'
      ENDIF
      ! USER FUNCTION
      CALL USER_CONDI3D_P
!
!-----------------------------------------------------------------------
!
      RETURN
      END
