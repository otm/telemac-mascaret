!~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
!>@brief Getter/setter of tomawac variables
!~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
!
      MODULE API_HANDLE_VAR_WAC

        USE API_HANDLE_ERROR
        USE API_INSTANCE_WAC
        IMPLICIT NONE
        !> Size of the string containing the name of a variable
        INTEGER, PARAMETER :: WAC_VAR_LEN=40
        !> Size of the string containing the type of a variable
        INTEGER, PARAMETER :: WAC_TYPE_LEN=12
        !> Size of the string containing the information about a variable
        INTEGER, PARAMETER :: WAC_INFO_LEN=200
        !> The maximum number of variable
        INTEGER, PARAMETER :: NB_VAR_WAC=81
        !> List of variable names
        CHARACTER(LEN=WAC_VAR_LEN),ALLOCATABLE :: VNAME_WAC(:)
        !> List of variable info
        CHARACTER(LEN=WAC_INFO_LEN),ALLOCATABLE :: VINFO_WAC(:)
!
      CONTAINS
!
      !~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
      !>@brief Get a double array
      !~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
      SUBROUTINE GET_DOUBLE_ARRAY_WAC_D
     &     (INST, VARNAME, VALEUR, DIM1, IERR, BLOCK_INDEX)
!
        TYPE(INSTANCE_WAC),         INTENT(IN) :: INST
        CHARACTER(LEN=WAC_VAR_LEN), INTENT(IN) :: VARNAME
        INTEGER,                    INTENT(IN) :: DIM1
        DOUBLE PRECISION,           INTENT(OUT):: VALEUR(DIM1)
        INTEGER,                    INTENT(OUT):: IERR
        INTEGER, OPTIONAL,          INTENT(IN) :: BLOCK_INDEX
!
        IERR = 0
!
        IF(TRIM(VARNAME).EQ.'MODEL.X') THEN
          VALEUR(1:INST%MESH%X%DIM1) = INST%MESH%X%R(1:INST%MESH%X%DIM1)
        ELSE IF(TRIM(VARNAME).EQ.'MODEL.Y') THEN
          VALEUR(1:INST%MESH%Y%DIM1) = INST%MESH%Y%R(1:INST%MESH%Y%DIM1)
        ELSE IF(TRIM(VARNAME).EQ.'MODEL.BOTTOM') THEN
          VALEUR(1:SIZE(INST%ZF)) =
     &     INST%ZF(1:SIZE(INST%ZF))
        ELSE IF(TRIM(VARNAME).EQ.'MODEL.WINDX') THEN
          VALEUR(1:SIZE(INST%WINDX)) =
     &     INST%WINDX(1:SIZE(INST%WINDX))
        ELSE IF(TRIM(VARNAME).EQ.'MODEL.WINDY') THEN
          VALEUR(1:SIZE(INST%WINDY)) =
     &     INST%WINDY(1:SIZE(INST%WINDY))
        ELSE IF(TRIM(VARNAME).EQ.'MODEL.HM0') THEN
          VALEUR(1:SIZE(INST%HM0)) =
     &     INST%HM0(1:SIZE(INST%HM0))
        ELSE IF(TRIM(VARNAME).EQ.'MODEL.USTAR') THEN
          VALEUR(1:SIZE(INST%USNEW)) =
     &     INST%USNEW(1:SIZE(INST%USNEW))
        ELSE IF(TRIM(VARNAME).EQ.'MODEL.FREQ') THEN
          VALEUR(1:SIZE(INST%FREQ)) =
     &     INST%FREQ(1:SIZE(INST%FREQ))
        ELSE IF(TRIM(VARNAME).EQ.'MODEL.TETA') THEN
          VALEUR(1:SIZE(INST%TETA)) =
     &     INST%TETA(1:SIZE(INST%TETA))
        ELSE IF(TRIM(VARNAME).EQ.'MODEL.FMOY') THEN
          VALEUR(1:SIZE(INST%FMOY)) =
     &     INST%FMOY(1:SIZE(INST%FMOY))
        ELSE IF(TRIM(VARNAME).EQ.'MODEL.VARIAN') THEN
          VALEUR(1:SIZE(INST%VARIAN)) =
     &     INST%VARIAN(1:SIZE(INST%VARIAN))
        ELSE IF(TRIM(VARNAME).EQ.'MODEL.PFREAD5') THEN
          VALEUR(1:SIZE(INST%PFREAD5)) =
     &     INST%PFREAD5(1:SIZE(INST%PFREAD5))
        ELSE IF(TRIM(VARNAME).EQ.'MODEL.PFREAD8') THEN
          VALEUR(1:SIZE(INST%PFREAD8)) =
     &     INST%PFREAD8(1:SIZE(INST%PFREAD8))
        ELSE IF(TRIM(VARNAME).EQ.'MODEL.TM01') THEN
          VALEUR(1:SIZE(INST%TM01)) =
     &     INST%TM01(1:SIZE(INST%TM01))
        ELSE IF(TRIM(VARNAME).EQ.'MODEL.TM02') THEN
          VALEUR(1:SIZE(INST%TM02)) =
     &     INST%TM02(1:SIZE(INST%TM02))
        ELSE IF(TRIM(VARNAME).EQ.'MODEL.ENERGYDENSITY') THEN
          VALEUR(1:INST%SF%DIM1) = INST%SF%R(1:INST%SF%DIM1)
        ! <get_double_array>
        ELSE
          IERR = UNKNOWN_VAR_ERROR
          ERR_MESS = 'UNKNOWN VARIABLE NAME : '//TRIM(VARNAME)
        ENDIF
!
      END SUBROUTINE GET_DOUBLE_ARRAY_WAC_D
!
      !~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
      !>@brief Defines the value of a double variable of tomawac
      !~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
      SUBROUTINE SET_DOUBLE_ARRAY_WAC_D
     &     (INST, VARNAME, VALEUR, DIM1, IERR, BLOCK_INDEX)
!
        TYPE(INSTANCE_WAC),    INTENT(INOUT) :: INST
        CHARACTER(LEN=WAC_VAR_LEN), INTENT(IN)  :: VARNAME
        INTEGER,               INTENT(IN) :: DIM1
        DOUBLE PRECISION,      INTENT(IN) :: VALEUR(DIM1)
        INTEGER,               INTENT(OUT) :: IERR
        INTEGER, OPTIONAL,     INTENT(IN) :: BLOCK_INDEX
!
        IERR = 0
        IF(TRIM(VARNAME).EQ.'MODEL.X') THEN
          INST%MESH%X%R(1:INST%MESH%X%DIM1) = VALEUR(1:INST%MESH%X%DIM1)
        ELSE IF(TRIM(VARNAME).EQ.'MODEL.Y') THEN
          INST%MESH%Y%R(1:INST%MESH%Y%DIM1) = VALEUR(1:INST%MESH%Y%DIM1)
        ELSE IF(TRIM(VARNAME).EQ.'MODEL.BOTTOM') THEN
          INST%ZF(1:SIZE(INST%ZF)) = VALEUR(1:SIZE(INST%ZF))
        ELSE IF(TRIM(VARNAME).EQ.'MODEL.WINDX') THEN
          INST%WINDX(1:SIZE(INST%WINDX)) = VALEUR(1:SIZE(INST%WINDX))
        ELSE IF(TRIM(VARNAME).EQ.'MODEL.WINDY') THEN
          INST%WINDY(1:SIZE(INST%WINDY)) = VALEUR(1:SIZE(INST%WINDY))
        ELSE IF(TRIM(VARNAME).EQ.'MODEL.HM0') THEN
          INST%HM0(1:SIZE(INST%HM0)) = VALEUR(1:SIZE(INST%HM0))
        ELSE IF(TRIM(VARNAME).EQ.'MODEL.USTAR') THEN
          INST%USNEW(1:SIZE(INST%USNEW)) = VALEUR(1:SIZE(INST%USNEW))
        ELSE IF(TRIM(VARNAME).EQ.'MODEL.FREQ') THEN
          INST%FREQ(1:SIZE(INST%FREQ)) = VALEUR(1:SIZE(INST%FREQ))
        ELSE IF(TRIM(VARNAME).EQ.'MODEL.TETA') THEN
          INST%TETA(1:SIZE(INST%TETA)) = VALEUR(1:SIZE(INST%TETA))
        ELSE IF(TRIM(VARNAME).EQ.'MODEL.FMOY') THEN
          INST%FMOY(1:SIZE(INST%FMOY)) = VALEUR(1:SIZE(INST%FMOY))   
        ELSE IF(TRIM(VARNAME).EQ.'MODEL.VARIAN') THEN
          INST%VARIAN(1:SIZE(INST%VARIAN)) = VALEUR(1:SIZE(INST%VARIAN))
        ELSE IF(TRIM(VARNAME).EQ.'MODEL.TM01') THEN
          INST%TM01(1:SIZE(INST%TM01)) = VALEUR(1:SIZE(INST%TM01))
        ELSE IF(TRIM(VARNAME).EQ.'MODEL.TM02') THEN
          INST%TM02(1:SIZE(INST%TM02)) = VALEUR(1:SIZE(INST%TM02))
        ELSE IF(TRIM(VARNAME).EQ.'MODEL.PFREAD5') THEN
          INST%PFREAD5(1:SIZE(INST%PFREAD5)) =
     &       VALEUR(1:SIZE(INST%PFREAD5))
        ELSE IF(TRIM(VARNAME).EQ.'MODEL.PFREAD8') THEN
          INST%PFREAD8(1:SIZE(INST%PFREAD8)) =
     &       VALEUR(1:SIZE(INST%PFREAD8))
        ELSE IF(TRIM(VARNAME).EQ.'MODEL.ENERGYDENSITY') THEN
          INST%SF%R(1:INST%SF%DIM1) = VALEUR(1:INST%SF%DIM1)
        ! <set_double_array>
        ELSE
          IERR = UNKNOWN_VAR_ERROR
          ERR_MESS = 'UNKNOWN VARIABLE NAME : '//TRIM(VARNAME)
        ENDIF
!
      END SUBROUTINE SET_DOUBLE_ARRAY_WAC_D
!
      !~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
      !>@brief Get an integer variable from tomawac
      !~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
      SUBROUTINE GET_INTEGER_ARRAY_WAC_D
     &     (INST, VARNAME, VALEUR, DIM1, IERR)
!
        TYPE(INSTANCE_WAC),    INTENT(IN) :: INST
        CHARACTER(LEN=WAC_VAR_LEN), INTENT(IN)  :: VARNAME
        INTEGER,               INTENT(IN) :: DIM1
        INTEGER,               INTENT(OUT) :: VALEUR(DIM1)
        INTEGER,               INTENT(OUT) :: IERR
!
        IERR = 0
        IF(TRIM(VARNAME).EQ.'MODEL.IKLE') THEN
          VALEUR(1:SIZE(INST%MESH%IKLE%I)) =
     &    INST%MESH%IKLE%I(1:SIZE(INST%MESH%IKLE%I))
        ELSE IF(TRIM(VARNAME).EQ.'MODEL.NACHB') THEN
          VALEUR(1:SIZE(INST%MESH%NACHB%I)) =
     &         INST%MESH%NACHB%I(1:SIZE(INST%MESH%NACHB%I))
        ELSE IF(TRIM(VARNAME).EQ.'MODEL.KNOLG') THEN
          VALEUR(1:INST%MESH%KNOLG%DIM1) =
     &         INST%MESH%KNOLG%I(1:INST%MESH%KNOLG%DIM1)
        ! <get_integer_array>
        ELSE
          IERR = UNKNOWN_VAR_ERROR
          ERR_MESS = 'UNKNOWN VARIABLE NAME : '//TRIM(VARNAME)
        ENDIF
!
      END SUBROUTINE GET_INTEGER_ARRAY_WAC_D
!
      !~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
      !>@brief Defines the value of an integer variable of tomawac
      !~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
      SUBROUTINE SET_INTEGER_ARRAY_WAC_D
     &     (INST, VARNAME, VALEUR, DIM1, IERR)
!
        TYPE(INSTANCE_WAC),    INTENT(INOUT) :: INST
        CHARACTER(LEN=WAC_VAR_LEN), INTENT(IN)  :: VARNAME
        INTEGER,               INTENT(IN) :: DIM1
        INTEGER,               INTENT(IN) :: VALEUR(DIM1)
        INTEGER,               INTENT(OUT) :: IERR
!
        IERR = 0
        IF(TRIM(VARNAME).EQ.'MODEL.IKLE') THEN
          INST%MESH%IKLE%I(1:SIZE(INST%MESH%IKLE%I))
     &    = VALEUR(1:SIZE(INST%MESH%IKLE%I))
        ELSE IF(TRIM(VARNAME).EQ.'MODEL.NACHB') THEN
          INST%MESH%NACHB%I(1:SIZE(INST%MESH%NACHB%I)) =
     &    VALEUR(1:SIZE(INST%MESH%NACHB%I))
        ELSE IF(TRIM(VARNAME).EQ.'MODEL.KNOLG') THEN
          INST%MESH%KNOLG%I(1:INST%MESH%KNOLG%DIM1) =
     &    VALEUR(1:INST%MESH%KNOLG%DIM1)
        ! <set_integer_array>
        ELSE
          IERR = UNKNOWN_VAR_ERROR
          ERR_MESS = 'UNKNOWN VARIABLE NAME : '//TRIM(VARNAME)
        ENDIF
!
      END SUBROUTINE SET_INTEGER_ARRAY_WAC_D
!
      !~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
      !>@brief Get a double variable from tomawac
      !~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
      !>@param[in,out] INST The instance
      !>@param[in] VARNAME Name of the variable to read
      !>@param[out] VALEUR Contains the read value
      !>@param[in] INDEX1 Index on the first dimension
      !>@param[in] INDEX2 Index on the second dimension
      !>@param[in] INDEX3 Index on the third dimension
      !>@param[out] IERR 0 if subroutine successfull,
      !!                        error id otherwise
      !~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
      SUBROUTINE GET_DOUBLE_WAC_D
     &     (INST, VARNAME, VALEUR, INDEX1, INDEX2, INDEX3, IERR)
!
        TYPE(INSTANCE_WAC),         INTENT(IN) :: INST
        CHARACTER(LEN=WAC_VAR_LEN), INTENT(IN) :: VARNAME
        DOUBLE PRECISION,           INTENT(OUT):: VALEUR
        INTEGER,                    INTENT(IN) :: INDEX1
        INTEGER,                    INTENT(IN) :: INDEX2
        INTEGER,                    INTENT(IN) :: INDEX3
        INTEGER,                    INTENT(OUT):: IERR
!
        IERR = 0
        VALEUR = 0.0
!
        IF(TRIM(VARNAME).EQ.'MODEL.X') THEN
          VALEUR = INST%MESH%X%R(INDEX1)
        ELSE IF(TRIM(VARNAME).EQ.'MODEL.Y') THEN
          VALEUR = INST%MESH%Y%R(INDEX1)
        ELSE IF(TRIM(VARNAME).EQ.'MODEL.AT') THEN
          VALEUR = INST%AT
        ELSE IF(TRIM(VARNAME).EQ.'MODEL.DT') THEN
          VALEUR = INST%DT
        ELSE IF(TRIM(VARNAME).EQ.'MODEL.RAISF') THEN
          VALEUR = INST%RAISF
        ELSE IF(TRIM(VARNAME).EQ.'MODEL.VX_CTE') THEN
          VALEUR = INST%VX_CTE
        ELSE IF(TRIM(VARNAME).EQ.'MODEL.VY_CTE') THEN
          VALEUR = INST%VY_CTE      
        ELSE IF(TRIM(VARNAME).EQ.'MODEL.BOTTOM') THEN
          VALEUR = INST%ZF(INDEX1)
        ELSE IF(TRIM(VARNAME).EQ.'MODEL.WINDX') THEN
          VALEUR = INST%WINDX(INDEX1)
        ELSE IF(TRIM(VARNAME).EQ.'MODEL.WINDY') THEN
          VALEUR = INST%WINDY(INDEX1)
        ELSE IF(TRIM(VARNAME).EQ.'MODEL.HM0') THEN
          VALEUR = INST%HM0(INDEX1)
        ELSE IF(TRIM(VARNAME).EQ.'MODEL.USTAR') THEN
          VALEUR = INST%USNEW(INDEX1)
        ELSE IF(TRIM(VARNAME).EQ.'MODEL.FREQ') THEN
          VALEUR = INST%FREQ(INDEX1)
        ELSE IF(TRIM(VARNAME).EQ.'MODEL.TETA') THEN
          VALEUR = INST%TETA(INDEX1)
        ELSE IF(TRIM(VARNAME).EQ.'MODEL.FMOY') THEN
          VALEUR = INST%FMOY(INDEX1)   
        ELSE IF(TRIM(VARNAME).EQ.'MODEL.VARIAN') THEN
          VALEUR = INST%VARIAN(INDEX1)
        ELSE IF(TRIM(VARNAME).EQ.'MODEL.PFREAD5') THEN
          VALEUR = INST%PFREAD5(INDEX1)
        ELSE IF(TRIM(VARNAME).EQ.'MODEL.PFREAD8') THEN
          VALEUR = INST%PFREAD8(INDEX1)
        ELSE IF(TRIM(VARNAME).EQ.'MODEL.TM01') THEN
          VALEUR = INST%TM01(INDEX1)
        ELSE IF(TRIM(VARNAME).EQ.'MODEL.TM02') THEN
          VALEUR = INST%TM02(INDEX1)
        ELSE IF(TRIM(VARNAME).EQ.'MODEL.ENERGYDENSITY') THEN
          VALEUR = INST%SF%R((INDEX1-1)*INST%NDIRE*INST%MESH%NPOIN
     &                      +(INDEX2-1)*INST%MESH%NPOIN +INDEX3)
        ELSE IF(TRIM(VARNAME).EQ.'MODEL.BETAM') THEN
          VALEUR = INST%BETAM
        ELSE IF(TRIM(VARNAME).EQ.'MODEL.ALPHA') THEN
          VALEUR = INST%ALPHA
        ELSE IF(TRIM(VARNAME).EQ.'MODEL.ROAIR') THEN
          VALEUR = INST%ROAIR
        ELSE IF(TRIM(VARNAME).EQ.'MODEL.ROEAU') THEN
          VALEUR = INST%ROEAU
        ELSE IF(TRIM(VARNAME).EQ.'MODEL.DECAL') THEN
          VALEUR = INST%DECAL
        ELSE IF(TRIM(VARNAME).EQ.'MODEL.ZVENT') THEN
          VALEUR = INST%ZVENT
        ELSE IF(TRIM(VARNAME).EQ.'MODEL.XKAPPA') THEN
          VALEUR = INST%XKAPPA
        ELSE IF(TRIM(VARNAME).EQ.'MODEL.COEFWD') THEN
          VALEUR = INST%COEFWD
        ELSE IF(TRIM(VARNAME).EQ.'MODEL.COEFWE') THEN
          VALEUR = INST%COEFWE
        ELSE IF(TRIM(VARNAME).EQ.'MODEL.COEFWF') THEN
          VALEUR = INST%COEFWF
        ELSE IF(TRIM(VARNAME).EQ.'MODEL.COEFWH') THEN
          VALEUR = INST%COEFWH
        ELSE IF(TRIM(VARNAME).EQ.'MODEL.CMOUT1') THEN
          VALEUR = INST%CMOUT1
        ELSE IF(TRIM(VARNAME).EQ.'MODEL.CMOUT2') THEN
          VALEUR = INST%CMOUT2
        ELSE IF(TRIM(VARNAME).EQ.'MODEL.CMOUT3') THEN
          VALEUR = INST%CMOUT3
        ELSE IF(TRIM(VARNAME).EQ.'MODEL.CMOUT4') THEN
          VALEUR = INST%CMOUT4
        ELSE IF(TRIM(VARNAME).EQ.'MODEL.CMOUT5') THEN
          VALEUR = INST%CMOUT5
        ELSE IF(TRIM(VARNAME).EQ.'MODEL.CMOUT6') THEN
          VALEUR = INST%CMOUT6
        ELSE IF(TRIM(VARNAME).EQ.'MODEL.ALFABJ') THEN
          VALEUR = INST%ALFABJ
        ELSE IF(TRIM(VARNAME).EQ.'MODEL.GAMBJ1') THEN
          VALEUR = INST%GAMBJ1
        ELSE IF(TRIM(VARNAME).EQ.'MODEL.GAMBJ2') THEN
          VALEUR = INST%GAMBJ2
        ELSE IF(TRIM(VARNAME).EQ.'MODEL.BORETG') THEN
          VALEUR = INST%BORETG
        ELSE IF(TRIM(VARNAME).EQ.'MODEL.GAMATG') THEN
          VALEUR = INST%GAMATG
        ELSE IF(TRIM(VARNAME).EQ.'MODEL.ALFARO') THEN
          VALEUR = INST%ALFARO
        ELSE IF(TRIM(VARNAME).EQ.'MODEL.GAMARO') THEN
          VALEUR = INST%GAMARO
        ELSE IF(TRIM(VARNAME).EQ.'MODEL.GAM2RO') THEN
          VALEUR = INST%GAM2RO
        ELSE IF(TRIM(VARNAME).EQ.'MODEL.BETAIH') THEN
          VALEUR = INST%BETAIH
        ELSE IF(TRIM(VARNAME).EQ.'MODEL.EM2SIH') THEN
          VALEUR = INST%EM2SIH
        ELSE IF(TRIM(VARNAME).EQ.'MODEL.CFROT1') THEN
          VALEUR = INST%CFROT1
        ELSE IF(TRIM(VARNAME).EQ.'MODEL.CDSCUR') THEN
          VALEUR = INST%CDSCUR
        ! <get_double>
        ELSE
          IERR = UNKNOWN_VAR_ERROR
          ERR_MESS = 'UNKNOWN VARIABLE NAME : '//TRIM(VARNAME)
        ENDIF
!
      END SUBROUTINE GET_DOUBLE_WAC_D
!
      !~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
      !>@brief Defines the value of a double variable of tomawac
      !~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
      !>@param[in,out] INST The instance
      !>@param[in] VARNAME Name of the variable to write
      !>@param[in] VALEUR The value to write in the variable
      !>@param[in] INDEX1 Index on the first dimension
      !>@param[in] INDEX2 Index on the second dimension
      !>@param[in] INDEX3 Index on the third dimension
      !>@param[out] IERR 0 if subroutine successfull,
      !!                        error id otherwise
      !~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
      SUBROUTINE SET_DOUBLE_WAC_D
     &     (INST, VARNAME, VALEUR, INDEX1, INDEX2, INDEX3, IERR)
!
        TYPE(INSTANCE_WAC),    INTENT(INOUT) :: INST
        CHARACTER(LEN=WAC_VAR_LEN), INTENT(IN)  :: VARNAME
        DOUBLE PRECISION,      INTENT(IN) :: VALEUR
        INTEGER,               INTENT(IN) :: INDEX1
        INTEGER,               INTENT(IN) :: INDEX2
        INTEGER,               INTENT(IN) :: INDEX3
        INTEGER,               INTENT(OUT) :: IERR
!
        IERR = 0
        IF(TRIM(VARNAME).EQ.'MODEL.X') THEN
          INST%MESH%X%R(INDEX1) = VALEUR
        ELSE IF(TRIM(VARNAME).EQ.'MODEL.Y') THEN
          INST%MESH%Y%R(INDEX1) = VALEUR
        ELSE IF(TRIM(VARNAME).EQ.'MODEL.AT') THEN
          INST%AT = VALEUR
        ELSE IF(TRIM(VARNAME).EQ.'MODEL.DT') THEN
          INST%DT = VALEUR
        ELSE IF(TRIM(VARNAME).EQ.'MODEL.RAISF') THEN
          INST%RAISF = VALEUR
        ELSE IF(TRIM(VARNAME).EQ.'MODEL.VX_CTE') THEN
          INST%VX_CTE = VALEUR
        ELSE IF(TRIM(VARNAME).EQ.'MODEL.VY_CTE') THEN
          INST%VY_CTE = VALEUR           
        ELSE IF(TRIM(VARNAME).EQ.'MODEL.BOTTOM') THEN
          INST%ZF(INDEX1) = VALEUR
        ELSE IF(TRIM(VARNAME).EQ.'MODEL.WINDX') THEN
          INST%WINDX(INDEX1) = VALEUR
        ELSE IF(TRIM(VARNAME).EQ.'MODEL.WINDY') THEN
          INST%WINDY(INDEX1) = VALEUR
        ELSE IF(TRIM(VARNAME).EQ.'MODEL.HM0') THEN
          INST%HM0(INDEX1) = VALEUR
        ELSE IF(TRIM(VARNAME).EQ.'MODEL.USTAR') THEN
          INST%USNEW(INDEX1) = VALEUR
        ELSE IF(TRIM(VARNAME).EQ.'MODEL.FREQ') THEN
          INST%FREQ(INDEX1) = VALEUR
        ELSE IF(TRIM(VARNAME).EQ.'MODEL.TETA') THEN
          INST%TETA(INDEX1) = VALEUR
        ELSE IF(TRIM(VARNAME).EQ.'MODEL.FMOY') THEN
          INST%FMOY(INDEX1) = VALEUR
        ELSE IF(TRIM(VARNAME).EQ.'MODEL.VARIAN') THEN
          INST%VARIAN(INDEX1) = VALEUR
        ELSE IF(TRIM(VARNAME).EQ.'MODEL.PFREAD5') THEN
          INST%PFREAD5(INDEX1) = VALEUR
        ELSE IF(TRIM(VARNAME).EQ.'MODEL.PFREAD8') THEN
          INST%PFREAD8(INDEX1) = VALEUR
        ELSE IF(TRIM(VARNAME).EQ.'MODEL.TM01') THEN
          INST%TM01(INDEX1) = VALEUR
        ELSE IF(TRIM(VARNAME).EQ.'MODEL.TM02') THEN
          INST%TM02(INDEX1) = VALEUR
        ELSE IF(TRIM(VARNAME).EQ.'MODEL.ENERGYDENSITY') THEN
          INST%SF%R((INDEX1-1)*INST%NDIRE*INST%MESH%NPOIN
     &              +(INDEX2-1)*INST%MESH%NPOIN+INDEX3) = VALEUR
        ELSE IF(TRIM(VARNAME).EQ.'MODEL.BETAM') THEN
          INST%BETAM = VALEUR
        ELSE IF(TRIM(VARNAME).EQ.'MODEL.ALPHA') THEN
          INST%ALPHA = VALEUR
        ELSE IF(TRIM(VARNAME).EQ.'MODEL.ROAIR') THEN
          INST%ROAIR = VALEUR
        ELSE IF(TRIM(VARNAME).EQ.'MODEL.ROEAU') THEN
          INST%ROEAU = VALEUR
        ELSE IF(TRIM(VARNAME).EQ.'MODEL.DECAL') THEN
          INST%DECAL = VALEUR
        ELSE IF(TRIM(VARNAME).EQ.'MODEL.ZVENT') THEN
          INST%ZVENT = VALEUR
        ELSE IF(TRIM(VARNAME).EQ.'MODEL.XKAPPA') THEN
          INST%XKAPPA = VALEUR
        ELSE IF(TRIM(VARNAME).EQ.'MODEL.COEFWD') THEN
          INST%COEFWD = VALEUR
        ELSE IF(TRIM(VARNAME).EQ.'MODEL.COEFWE') THEN
          INST%COEFWE = VALEUR
        ELSE IF(TRIM(VARNAME).EQ.'MODEL.COEFWF') THEN
          INST%COEFWF = VALEUR
        ELSE IF(TRIM(VARNAME).EQ.'MODEL.COEFWH') THEN
          INST%COEFWH = VALEUR
        ELSE IF(TRIM(VARNAME).EQ.'MODEL.CMOUT1') THEN
          INST%CMOUT1 = VALEUR
        ELSE IF(TRIM(VARNAME).EQ.'MODEL.CMOUT2') THEN
          INST%CMOUT2 = VALEUR
        ELSE IF(TRIM(VARNAME).EQ.'MODEL.CMOUT3') THEN
          INST%CMOUT3 = VALEUR
        ELSE IF(TRIM(VARNAME).EQ.'MODEL.CMOUT4') THEN
          INST%CMOUT4 = VALEUR
        ELSE IF(TRIM(VARNAME).EQ.'MODEL.CMOUT5') THEN
          INST%CMOUT5 = VALEUR
        ELSE IF(TRIM(VARNAME).EQ.'MODEL.CMOUT6') THEN
          INST%CMOUT6 = VALEUR
        ELSE IF(TRIM(VARNAME).EQ.'MODEL.ALFABJ') THEN
          INST%ALFABJ = VALEUR
        ELSE IF(TRIM(VARNAME).EQ.'MODEL.GAMBJ1') THEN
          INST%GAMBJ1 = VALEUR
        ELSE IF(TRIM(VARNAME).EQ.'MODEL.GAMBJ2') THEN
          INST%GAMBJ2 = VALEUR
        ELSE IF(TRIM(VARNAME).EQ.'MODEL.BORETG') THEN
          INST%BORETG = VALEUR
        ELSE IF(TRIM(VARNAME).EQ.'MODEL.GAMATG') THEN
          INST%GAMATG = VALEUR
        ELSE IF(TRIM(VARNAME).EQ.'MODEL.ALFARO') THEN
          INST%ALFARO = VALEUR
        ELSE IF(TRIM(VARNAME).EQ.'MODEL.GAMARO') THEN
          INST%GAMARO = VALEUR
        ELSE IF(TRIM(VARNAME).EQ.'MODEL.GAM2RO') THEN
          INST%GAM2RO = VALEUR
        ELSE IF(TRIM(VARNAME).EQ.'MODEL.BETAIH') THEN
          INST%BETAIH = VALEUR
        ELSE IF(TRIM(VARNAME).EQ.'MODEL.EM2SIH') THEN
          INST%EM2SIH = VALEUR
        ELSE IF(TRIM(VARNAME).EQ.'MODEL.CFROT1') THEN
          INST%CFROT1 = VALEUR
        ELSE IF(TRIM(VARNAME).EQ.'MODEL.CDSCUR') THEN
          INST%CDSCUR = VALEUR

        ! <set_double>
        ELSE
          IERR = UNKNOWN_VAR_ERROR
          ERR_MESS = 'UNKNOWN VARIABLE NAME : '//TRIM(VARNAME)
        ENDIF
!
      END SUBROUTINE SET_DOUBLE_WAC_D
!
      !~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
      !>@brief Get an integer variable from tomawac
      !~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
      !>@param[in,out] INST The instance
      !>@param[in] VARNAME Name of the variable to read
      !>@param[out] VALEUR Containis the read value
      !>@param[in] INDEX1 Index on the first dimension
      !>@param[in] INDEX2 Index on the second dimension
      !>@param[in] INDEX3 Index on the third dimension
      !>@param[out] IERR 0 if subroutine successfull,
      !!                        error id otherwise
      !~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
      SUBROUTINE GET_INTEGER_WAC_D
     &     (INST, VARNAME, VALEUR, INDEX1, INDEX2, INDEX3, IERR)
!
        TYPE(INSTANCE_WAC),    INTENT(IN) :: INST
        CHARACTER(LEN=WAC_VAR_LEN), INTENT(IN)  :: VARNAME
        INTEGER,               INTENT(OUT) :: VALEUR
        INTEGER,               INTENT(IN) :: INDEX1
        INTEGER,               INTENT(IN) :: INDEX2
        INTEGER,               INTENT(IN) :: INDEX3
        INTEGER,               INTENT(OUT) :: IERR
!
        IERR = 0
        VALEUR = -1
        IF(TRIM(VARNAME).EQ.'MODEL.NPOIN') THEN
          VALEUR = INST%MESH%NPOIN
        ELSE IF(TRIM(VARNAME).EQ.'MODEL.NELEM') THEN
          VALEUR = INST%MESH%NELEM
        ELSE IF(TRIM(VARNAME).EQ.'MODEL.NPTFR') THEN
          VALEUR = INST%MESH%NPTFR
        ELSE IF(TRIM(VARNAME).EQ.'MODEL.NTIMESTEPS') THEN
          VALEUR = INST%NIT
        ELSE IF(TRIM(VARNAME).EQ.'MODEL.NFREQ') THEN
          VALEUR = INST%NFREQ
        ELSE IF(TRIM(VARNAME).EQ.'MODEL.NDIRE') THEN
          VALEUR = INST%NDIRE
        ELSE IF(TRIM(VARNAME).EQ.'MODEL.LT') THEN
          VALEUR = INST%LT
        ELSE IF(TRIM(VARNAME).EQ.'MODEL.DEBUG') THEN
          VALEUR = INST%DEBUG
        ELSE IF(TRIM(VARNAME).EQ.'MODEL.IKLE') THEN
          VALEUR = INST%MESH%IKLE%I((INDEX2-1)*INST%MESH%IKLE%DIM1
     &                               + INDEX1)
        ELSE IF(TRIM(VARNAME).EQ.'MODEL.NACHB') THEN
          VALEUR = INST%MESH%NACHB%I((INDEX2-1)*INST%NBMAXNSHARE
     &          + INDEX1)
        ELSE IF(TRIM(VARNAME).EQ.'MODEL.KNOLG') THEN
          VALEUR = INST%MESH%KNOLG%I(INDEX1)
        ELSE IF(TRIM(VARNAME).EQ.'MODEL.SBREK') THEN
          VALEUR = INST%SBREK
        ELSE IF(TRIM(VARNAME).EQ.'MODEL.IQBBJ') THEN
          VALEUR = INST%IQBBJ
        ELSE IF(TRIM(VARNAME).EQ.'MODEL.IHMBJ') THEN
          VALEUR = INST%IHMBJ
        ELSE IF(TRIM(VARNAME).EQ.'MODEL.IFRBJ') THEN
          VALEUR = INST%IFRBJ
        ELSE IF(TRIM(VARNAME).EQ.'MODEL.IWHTG') THEN
          VALEUR = INST%IWHTG
        ELSE IF(TRIM(VARNAME).EQ.'MODEL.IFRTG') THEN
          VALEUR = INST%IFRTG
        ELSE IF(TRIM(VARNAME).EQ.'MODEL.IDISRO') THEN
          VALEUR = INST%IDISRO
        ELSE IF(TRIM(VARNAME).EQ.'MODEL.IEXPRO') THEN
          VALEUR = INST%IEXPRO
        ELSE IF(TRIM(VARNAME).EQ.'MODEL.IFRRO') THEN
          VALEUR = INST%IFRRO
        ELSE IF(TRIM(VARNAME).EQ.'MODEL.IFRIH') THEN
          VALEUR = INST%IFRIH
        ELSE IF(TRIM(VARNAME).EQ.'MODEL.SMOUT') THEN
          VALEUR = INST%SMOUT
        ELSE IF(TRIM(VARNAME).EQ.'MODEL.SFROT') THEN
          VALEUR = INST%SFROT
        ELSE IF(TRIM(VARNAME).EQ.'MODEL.SDSCU') THEN
          VALEUR = INST%SDSCU
        ELSE IF(TRIM(VARNAME).EQ.'MODEL.SVENT') THEN
          VALEUR = INST%SVENT
        ELSE IF(TRIM(VARNAME).EQ.'MODEL.LVENT') THEN
          VALEUR = INST%LVENT
        ! <get_integer>
        ELSE
          IERR = UNKNOWN_VAR_ERROR
          ERR_MESS = 'UNKNOWN VARIABLE NAME : '//TRIM(VARNAME)
        ENDIF
!
      END SUBROUTINE GET_INTEGER_WAC_D
!
      !~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
      !>@brief Defines the value of an integer variable of tomawac
      !~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
      !>@param[in,out] INST The instance
      !>@param[in] VARNAME Name of the variable to write
      !>@param[in] VALEUR The value to write in the variable
      !>@param[in] INDEX1 Index on the first dimension
      !>@param[in] INDEX2 Index on the second dimension
      !>@param[in] INDEX3 Index on the third dimension
      !>@param[out] IERR 0 if subroutine successfull,
      !!                        error id otherwise
      !~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
      SUBROUTINE SET_INTEGER_WAC_D
     &     (INST, VARNAME, VALEUR, INDEX1, INDEX2, INDEX3, IERR)
!
        TYPE(INSTANCE_WAC),    INTENT(INOUT) :: INST
        CHARACTER(LEN=WAC_VAR_LEN), INTENT(IN)  :: VARNAME
        INTEGER,               INTENT(IN) :: VALEUR
        INTEGER,               INTENT(IN) :: INDEX1
        INTEGER,               INTENT(IN) :: INDEX2
        INTEGER,               INTENT(IN) :: INDEX3
        INTEGER,               INTENT(OUT) :: IERR
!
        IERR = 0
        IF(TRIM(VARNAME).EQ.'MODEL.NTIMESTEPS') THEN
          INST%NIT = VALEUR
        ELSE IF(TRIM(VARNAME).EQ.'MODEL.LT') THEN
          INST%LT = VALEUR
        ELSE IF(TRIM(VARNAME).EQ.'MODEL.NFREQ') THEN
          INST%NFREQ = VALEUR
        ELSE IF(TRIM(VARNAME).EQ.'MODEL.NDIRE') THEN
          INST%NDIRE = VALEUR
        ELSE IF(TRIM(VARNAME).EQ.'MODEL.DEBUG') THEN
          INST%DEBUG = VALEUR
        ELSE IF(TRIM(VARNAME).EQ.'MODEL.SBREK') THEN
          INST%SBREK = VALEUR
        ELSE IF(TRIM(VARNAME).EQ.'MODEL.IQBBJ') THEN
          INST%IQBBJ = VALEUR
        ELSE IF(TRIM(VARNAME).EQ.'MODEL.IHMBJ') THEN
          INST%IHMBJ = VALEUR
        ELSE IF(TRIM(VARNAME).EQ.'MODEL.IFRBJ') THEN
          INST%IFRBJ = VALEUR
        ELSE IF(TRIM(VARNAME).EQ.'MODEL.IWHTG') THEN
          INST%IWHTG = VALEUR
        ELSE IF(TRIM(VARNAME).EQ.'MODEL.IFRTG') THEN
          INST%IFRTG = VALEUR
        ELSE IF(TRIM(VARNAME).EQ.'MODEL.IDISRO') THEN
          INST%IDISRO = VALEUR
        ELSE IF(TRIM(VARNAME).EQ.'MODEL.IEXPRO') THEN
          INST%IEXPRO = VALEUR
        ELSE IF(TRIM(VARNAME).EQ.'MODEL.IFRRO') THEN
          INST%IFRRO = VALEUR
        ELSE IF(TRIM(VARNAME).EQ.'MODEL.IFRIH') THEN
          INST%IFRIH = VALEUR
        ELSE IF(TRIM(VARNAME).EQ.'MODEL.SMOUT') THEN
          INST%SMOUT = VALEUR
        ELSE IF(TRIM(VARNAME).EQ.'MODEL.SFROT') THEN
          INST%SFROT = VALEUR
        ELSE IF(TRIM(VARNAME).EQ.'MODEL.SDSCU') THEN
          INST%SDSCU = VALEUR
        ELSE IF(TRIM(VARNAME).EQ.'MODEL.SVENT') THEN
          INST%SVENT = VALEUR
        ELSE IF(TRIM(VARNAME).EQ.'MODEL.LVENT') THEN
          INST%LVENT = VALEUR
        ! <set_integer>
        ELSE
          IERR = UNKNOWN_VAR_ERROR
          ERR_MESS = 'UNKNOWN VARIABLE NAME : '//TRIM(VARNAME)
        ENDIF
!
      END SUBROUTINE SET_INTEGER_WAC_D
!
      !~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
      !>@brief Get a string variable from tomawac
      !~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
      !>@param[in,out] INST The instance
      !>@param[in] VARNAME Name of the variable to read
      !>@param[out] VALEUR Containis the read value
      !>@param[in] VALUELEN Length of the string
      !>@param[in] INDEX1 Index on the first dimension
      !>@param[in] INDEX2 Index on the second dimension
      !>@param[out] IERR 0 if subroutine successfull,
      !!                        error id otherwise
      !~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
      SUBROUTINE GET_STRING_WAC_D
     &     (INST, VARNAME, VALEUR, VALUELEN, INDEX1, INDEX2, IERR)
!
        TYPE(INSTANCE_WAC),    INTENT(IN) :: INST
        CHARACTER(LEN=WAC_VAR_LEN), INTENT(IN)  :: VARNAME
        INTEGER,               INTENT(IN) :: VALUELEN
        INTEGER,               INTENT(IN) :: INDEX1
        INTEGER,               INTENT(IN) :: INDEX2
        CHARACTER,             INTENT(OUT) :: VALEUR(VALUELEN)
        INTEGER,               INTENT(OUT) :: IERR
!
        INTEGER I,J
!
        IERR = 0
        VALEUR = ""
        IF(TRIM(VARNAME).EQ.'MODEL.RESULTFILE') THEN
          I = INST%WACRES
          DO J = 1,VALUELEN
            VALEUR(J:J) = INST%WAC_FILES(I)%NAME(J:J)
          ENDDO
        ELSE IF(TRIM(VARNAME).EQ.'MODEL.BCFILE') THEN
          I = INST%WACCLI
          DO J = 1,VALUELEN
            VALEUR(J:J) = INST%WAC_FILES(I)%NAME(J:J)
          ENDDO
        ELSE IF(TRIM(VARNAME).EQ.'MODEL.GEOMETRYFILE') THEN
          I = INST%WACGEO
          DO J = 1,VALUELEN
            VALEUR(J:J) = INST%WAC_FILES(I)%NAME(J:J)
          ENDDO
        ! <get_string>
        ELSE
          IERR = UNKNOWN_VAR_ERROR
          ERR_MESS = 'UNKNOWN VARIABLE NAME : '//TRIM(VARNAME)
        ENDIF
!
      END SUBROUTINE GET_STRING_WAC_D
!
      !~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
      !>@brief Defines the value of a string variable of tomawac
      !~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
      !>@param[in,out] INST The instance
      !>@param[in] VARNAME Name of the variable to write
      !>@param[in] VALEUR The value to write in the variable
      !>@param[in] VALUELEN Length of the string
      !>@param[in] INDEX1 Index on the first dimension
      !>@param[in] INDEX2 Index on the second dimension
      !>@param[out] IERR 0 if subroutine successfull,
      !!                        error id otherwise
      !~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
      SUBROUTINE SET_STRING_WAC_D
     &     (INST, VARNAME, VALEUR, VALUELEN, INDEX1, INDEX2, IERR)
!
        TYPE(INSTANCE_WAC),    INTENT(INOUT) :: INST
        CHARACTER(LEN=WAC_VAR_LEN), INTENT(IN)  :: VARNAME
        INTEGER,               INTENT(IN) :: VALUELEN
        INTEGER,               INTENT(IN) :: INDEX1
        INTEGER,               INTENT(IN) :: INDEX2
        CHARACTER,             INTENT(IN) :: VALEUR(VALUELEN)
        INTEGER,               INTENT(OUT) :: IERR
!
        INTEGER I,J
!
        IERR = 0
        IF(TRIM(VARNAME).EQ.'MODEL.RESULTFILE') THEN
          I = INST%WACRES
          DO J=1,VALUELEN
            INST%WAC_FILES(I)%NAME(J:J) = VALEUR(J)
          ENDDO
        ! <set_string>
        ELSE
          IERR = UNKNOWN_VAR_ERROR
          ERR_MESS = 'UNKNOWN VARIABLE NAME : '//TRIM(VARNAME)
        ENDIF
!
      END SUBROUTINE SET_STRING_WAC_D
!
      !~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
      !>@brief Get a boolean variable from tomawac
      !~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
      !>@param[in,out] INST The instance
      !>@param[in] VARNAME Name of the variable to read
      !>@param[out] VALEUR Containis the read value
      !>@param[in] INDEX1 Index on the first dimension
      !>@param[in] INDEX2 Index on the second dimension
      !>@param[in] INDEX3 Index on the third dimension
      !>@param[out] IERR 0 if subroutine successfull,
      !!                        error id otherwise
      !~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
      SUBROUTINE GET_BOOLEAN_WAC_D
     &     (INST, VARNAME, VALEUR, INDEX1, INDEX2, INDEX3, IERR)
!
        TYPE(INSTANCE_WAC),    INTENT(IN) :: INST
        CHARACTER(LEN=WAC_VAR_LEN), INTENT(IN)  :: VARNAME
        INTEGER,               INTENT(OUT) :: VALEUR
        INTEGER,               INTENT(IN) :: INDEX1
        INTEGER,               INTENT(IN) :: INDEX2
        INTEGER,               INTENT(IN) :: INDEX3
        INTEGER,               INTENT(OUT) :: IERR
!
        IERR = 0
        VALEUR = 0
        IF(TRIM(VARNAME).EQ.'MODEL.APISET') THEN
          IF(INST%APISET) THEN
            VALEUR = 1
          ELSE
            VALEUR = 0
          ENDIF
        ! <get_boolean>
        ELSE
          IERR = UNKNOWN_VAR_ERROR
          ERR_MESS = 'UNKNOWN VARIABLE NAME : '//TRIM(VARNAME)
        ENDIF
!
      END SUBROUTINE GET_BOOLEAN_WAC_D
!
      !~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
      !>@brief Defines the value of a boolean variable of tomawac
      !~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
      !>@param[in,out] INST The instance
      !>@param[in] VARNAME Name of the variable to write
      !>@param[in] VALEUR The value to write in the variable
      !>@param[in] INDEX1 Index on the first dimension
      !>@param[in] INDEX2 Index on the second dimension
      !>@param[in] INDEX3 Index on the third dimension
      !>@param[out] IERR 0 if subroutine successfull,
      !!                        error id otherwise
      !~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
      SUBROUTINE SET_BOOLEAN_WAC_D
     &     (INST, VARNAME, VALEUR, INDEX1, INDEX2, INDEX3, IERR)
!
        TYPE(INSTANCE_WAC),    INTENT(INOUT) :: INST
        CHARACTER(LEN=WAC_VAR_LEN), INTENT(IN)  :: VARNAME
        INTEGER,               INTENT(IN) :: VALEUR
        INTEGER,               INTENT(IN) :: INDEX1
        INTEGER,               INTENT(IN) :: INDEX2
        INTEGER,               INTENT(IN) :: INDEX3
        INTEGER,               INTENT(OUT) :: IERR
!
        IERR = 0
        IF(TRIM(VARNAME).EQ.'MODEL.APISET') THEN
          INST%APISET = VALEUR.EQ.1
        ! <set_boolean>
        ELSE
          IERR = UNKNOWN_VAR_ERROR
          ERR_MESS = 'UNKNOWN VARIABLE NAME : '//TRIM(VARNAME)
        ENDIF
!
      END SUBROUTINE SET_BOOLEAN_WAC_D
!
      !~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
      !>@brief Get size informations on a variable of tomawac
      !~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
      !>@param[in] INST Instance
      !>@param[in] VARNAME Name of the variable
      !>@param[out] DIM1 Size of the first dimension
      !>@param[out] DIM2 Size of the second dimension
      !>@param[out] DIM3 Size of the third dimension
      !>@param[out] IERR 0 if subroutine successfull,
      !!                        error id otherwise
      !~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
      SUBROUTINE GET_VAR_SIZE_WAC_D
     &         (INST, VARNAME, DIM1, DIM2, DIM3, IERR)
!
        TYPE(INSTANCE_WAC),    INTENT(IN) :: INST
        CHARACTER(LEN=WAC_VAR_LEN), INTENT(IN)  :: VARNAME
        INTEGER,               INTENT(OUT) :: DIM1
        INTEGER,               INTENT(OUT) :: DIM2
        INTEGER,               INTENT(OUT) :: DIM3
        INTEGER,               INTENT(OUT) :: IERR
!
        IERR = 0
        DIM1 = 0
        DIM2 = 0
        DIM3 = 0
!
        IF(TRIM(VARNAME).EQ.'MODEL.X') THEN
          DIM1 = INST%MESH%X%DIM1
        ELSE IF(TRIM(VARNAME).EQ.'MODEL.Y') THEN
          DIM1 = INST%MESH%Y%DIM1
        ELSE IF(TRIM(VARNAME).EQ.'MODEL.RESULTFILE') THEN
          DIM1 = 250
        ELSE IF(TRIM(VARNAME).EQ.'MODEL.GEOMETRYFILE') THEN
          DIM1 = 250
        ELSE IF(TRIM(VARNAME).EQ.'MODEL.IKLE')THEN
          DIM1 = INST%MESH%IKLE%DIM2
          DIM2 = INST%MESH%IKLE%DIM1
        ELSE IF(TRIM(VARNAME).EQ.'MODEL.NACHB')THEN
          DIM1 = INST%NPTIR
          DIM2 = INST%NBMAXNSHARE
        ELSE IF(TRIM(VARNAME).EQ.'MODEL.KNOLG') THEN
          DIM1 = INST%MESH%KNOLG%DIM1
        ELSE IF(TRIM(VARNAME).EQ.'MODEL.BOTTOM') THEN
          DIM1 = SIZE(INST%ZF)
        ELSE IF(TRIM(VARNAME).EQ.'MODEL.WINDX') THEN
          DIM1 = SIZE(INST%WINDX)
        ELSE IF(TRIM(VARNAME).EQ.'MODEL.WINDY') THEN
          DIM1 = SIZE(INST%WINDY)
        ELSE IF(TRIM(VARNAME).EQ.'MODEL.HM0') THEN
          DIM1 = SIZE(INST%HM0)
        ELSE IF(TRIM(VARNAME).EQ.'MODEL.USTAR') THEN
          DIM1 = SIZE(INST%USNEW)
        ELSE IF(TRIM(VARNAME).EQ.'MODEL.FREQ') THEN
          DIM1 = SIZE(INST%FREQ)
        ELSE IF(TRIM(VARNAME).EQ.'MODEL.TETA') THEN
          DIM1 = SIZE(INST%TETA)
        ELSE IF(TRIM(VARNAME).EQ.'MODEL.FMOY') THEN
          DIM1 = SIZE(INST%FMOY)
        ELSE IF(TRIM(VARNAME).EQ.'MODEL.VARIAN') THEN
          DIM1 = SIZE(INST%VARIAN)
        ELSE IF(TRIM(VARNAME).EQ.'MODEL.TM01') THEN
          DIM1 = SIZE(INST%TM01)
        ELSE IF(TRIM(VARNAME).EQ.'MODEL.TM02') THEN
          DIM1 = SIZE(INST%TM02)
        ELSE IF(TRIM(VARNAME).EQ.'MODEL.PFREAD5') THEN
          DIM1 = SIZE(INST%PFREAD5)
        ELSE IF(TRIM(VARNAME).EQ.'MODEL.PFREAD8') THEN
          DIM1 = SIZE(INST%PFREAD8)
        ELSE IF(TRIM(VARNAME).EQ.'MODEL.ENERGYDENSITY') THEN
          DIM1 = INST%NFREQ
          DIM2 = INST%NDIRE
          DIM3 = INST%MESH%NPOIN
        ! <get_var_size>
        ENDIF
!
      END SUBROUTINE GET_VAR_SIZE_WAC_D
!
      !~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
      !>@brief Get the information on the type of variable
      !~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
      !>@param[in] VARNAME Name of the varaible
      !>@param[out] VARTYPE Type of the varaible (INTEGER, DOUBLE,
      !                     BOOLEAN, STRING)
      !>@param[out] READONLY True if the varaible cannot be modified
      !>@param[out] NDIM Name of the varaible
      !>@param[out] IENT 1 if the numbering is on point
      !>@param[out] JENT 1 if the numbering is on point
      !>@param[out] KENT 1 if the numbering is on point
      !>@param[out] GETPOS Postion after which the get is posible
      !!                        on the variable
      !>@param[out] SETPOS Postion after which the set is posible
      !!                        on the variable
      !>@param[out] IERR 0 if subroutine successfull,
      !!                        error id otherwise
      !~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
      SUBROUTINE GET_VAR_TYPE_WAC_D
     &        (VARNAME, VARTYPE, READONLY, NDIM,IENT,JENT,KENT,
     &         GETPOS,SETPOS,IERR)
!
        CHARACTER(LEN=WAC_VAR_LEN),  INTENT(IN)  :: VARNAME
        CHARACTER(LEN=WAC_TYPE_LEN), INTENT(OUT) :: VARTYPE
        LOGICAL,                     INTENT(OUT) :: READONLY
        INTEGER,                     INTENT(OUT) :: NDIM
        INTEGER,                     INTENT(OUT) :: IERR
        INTEGER,                     INTENT(OUT) :: IENT
        INTEGER,                     INTENT(OUT) :: JENT
        INTEGER,                     INTENT(OUT) :: KENT
        INTEGER,                     INTENT(OUT) :: GETPOS
        INTEGER,                     INTENT(OUT) :: SETPOS
!
        IERR = 0
        VARTYPE = ''
        READONLY = .TRUE.
        NDIM = 0
        IENT = 0
        JENT = 0
        KENT = 0
        GETPOS = NO_POSITION
        SETPOS = NO_POSITION
!
        IF(TRIM(VARNAME).EQ.'MODEL.BCFILE') THEN
          VARTYPE = 'STRING'
          READONLY = .FALSE.
          NDIM = 0
          GETPOS = RUN_READ_CASE_POS
          SETPOS = RUN_READ_CASE_POS
        ELSE IF(TRIM(VARNAME).EQ.'MODEL.DEBUG') THEN
          VARTYPE = 'INTEGER'
          READONLY = .FALSE.
          NDIM = 0
          GETPOS = RUN_SET_CONFIG_POS
          SETPOS = RUN_SET_CONFIG_POS
        ELSE IF(TRIM(VARNAME).EQ.'MODEL.AT') THEN
          VARTYPE = 'DOUBLE'
          READONLY = .FALSE.
          NDIM = 0
          GETPOS = RUN_ALLOCATION_POS
          SETPOS = RUN_ALLOCATION_POS
        ELSE IF(TRIM(VARNAME).EQ.'MODEL.DT') THEN
          VARTYPE = 'DOUBLE'
          READONLY = .FALSE.
          NDIM = 0
          GETPOS = RUN_ALLOCATION_POS
          SETPOS = RUN_ALLOCATION_POS
        ELSE IF(TRIM(VARNAME).EQ.'MODEL.RAISF') THEN
          VARTYPE = 'DOUBLE'
          READONLY = .FALSE.
          NDIM = 0
          GETPOS = RUN_ALLOCATION_POS
          SETPOS = RUN_ALLOCATION_POS
        ELSE IF(TRIM(VARNAME).EQ.'MODEL.APISET') THEN
          VARTYPE = 'BOOLEAN'
          READONLY = .FALSE.
          NDIM = 0
          GETPOS = RUN_ALLOCATION_POS
          SETPOS = RUN_ALLOCATION_POS
        ELSE IF(TRIM(VARNAME).EQ.'MODEL.LT') THEN
          VARTYPE = 'INTEGER'
          READONLY = .FALSE.
          NDIM = 0
          GETPOS = RUN_ALLOCATION_POS
          SETPOS = RUN_ALLOCATION_POS
        ELSE IF(TRIM(VARNAME).EQ.'MODEL.IKLE') THEN
          VARTYPE = 'INTEGER'
          READONLY = .TRUE.
          NDIM = 2
          GETPOS = RUN_ALLOCATION_POS
          SETPOS = RUN_ALLOCATION_POS
        ELSE IF(TRIM(VARNAME).EQ.'MODEL.NACHB') THEN
          VARTYPE = 'INTEGER'
          READONLY = .TRUE.
          NDIM = 2
          GETPOS = RUN_ALLOCATION_POS
          SETPOS = RUN_ALLOCATION_POS
        ELSE IF(TRIM(VARNAME).EQ.'MODEL.KNOLG') THEN
          VARTYPE = 'INTEGER'
          READONLY = .TRUE.
          NDIM = 1
          GETPOS = RUN_ALLOCATION_POS
          SETPOS = RUN_ALLOCATION_POS
        ELSE IF(TRIM(VARNAME).EQ.'MODEL.X') THEN
          VARTYPE = 'DOUBLE'
          READONLY = .FALSE.
          NDIM = 1
          IENT = 1
          GETPOS = RUN_ALLOCATION_POS
          SETPOS = RUN_ALLOCATION_POS
        ELSE IF(TRIM(VARNAME).EQ.'MODEL.Y') THEN
          VARTYPE = 'DOUBLE'
          READONLY = .FALSE.
          NDIM = 1
          IENT = 1
          GETPOS = RUN_ALLOCATION_POS
          SETPOS = RUN_ALLOCATION_POS
        ELSE IF(TRIM(VARNAME).EQ.'MODEL.RESULTFILE') THEN
          VARTYPE = 'STRING'
          READONLY = .FALSE.
          NDIM = 1
          GETPOS = RUN_ALLOCATION_POS
          SETPOS = RUN_ALLOCATION_POS
        ELSE IF(TRIM(VARNAME).EQ.'MODEL.GEOMETRYFILE') THEN
          VARTYPE = 'STRING'
          READONLY = .FALSE.
          NDIM = 1
          GETPOS = RUN_ALLOCATION_POS
          SETPOS = RUN_ALLOCATION_POS
        ELSE IF(TRIM(VARNAME).EQ.'MODEL.NPOIN') THEN
          VARTYPE = 'INTEGER'
          READONLY = .FALSE.
          GETPOS = RUN_ALLOCATION_POS
          SETPOS = RUN_ALLOCATION_POS
        ELSE IF(TRIM(VARNAME).EQ.'MODEL.NELEM') THEN
          VARTYPE = 'INTEGER'
          READONLY = .FALSE.
          GETPOS = RUN_ALLOCATION_POS
          SETPOS = RUN_ALLOCATION_POS
        ELSE IF(TRIM(VARNAME).EQ.'MODEL.NELMAX') THEN
          VARTYPE = 'INTEGER'
          READONLY = .FALSE.
          GETPOS = RUN_ALLOCATION_POS
          SETPOS = RUN_ALLOCATION_POS
        ELSE IF(TRIM(VARNAME).EQ.'MODEL.NPTFR') THEN
          VARTYPE = 'INTEGER'
          READONLY = .FALSE.
          GETPOS = RUN_ALLOCATION_POS
          SETPOS = RUN_ALLOCATION_POS
        ELSE IF(TRIM(VARNAME).EQ.'MODEL.NTIMESTEPS') THEN
          VARTYPE = 'INTEGER'
          READONLY = .FALSE.
          NDIM = 0
          GETPOS = RUN_READ_CASE_POS
          SETPOS = RUN_READ_CASE_POS
        ELSE IF(TRIM(VARNAME).EQ.'MODEL.NFREQ') THEN
          VARTYPE = 'INTEGER'
          READONLY = .FALSE.
          NDIM = 0
          GETPOS = RUN_READ_CASE_POS
          SETPOS = RUN_READ_CASE_POS
        ELSE IF(TRIM(VARNAME).EQ.'MODEL.NDIRE') THEN
          VARTYPE = 'INTEGER'
          READONLY = .FALSE.
          NDIM = 0
          GETPOS = RUN_READ_CASE_POS
          SETPOS = RUN_READ_CASE_POS
        ELSE IF(TRIM(VARNAME).EQ.'MODEL.BOTTOM') THEN
          VARTYPE = 'DOUBLE'
          READONLY = .FALSE.
          NDIM = 1
          GETPOS = NO_POSITION
          SETPOS = NO_POSITION
        ELSE IF(TRIM(VARNAME).EQ.'MODEL.WINDX') THEN
          VARTYPE = 'DOUBLE'
          READONLY = .FALSE.
          NDIM = 1
          GETPOS = NO_POSITION
          SETPOS = NO_POSITION
        ELSE IF(TRIM(VARNAME).EQ.'MODEL.WINDY') THEN
          VARTYPE = 'DOUBLE'
          READONLY = .FALSE.
          NDIM = 1
          GETPOS = NO_POSITION
          SETPOS = NO_POSITION
        ELSE IF(TRIM(VARNAME).EQ.'MODEL.HM0') THEN
          VARTYPE = 'DOUBLE'
          READONLY = .FALSE.
          NDIM = 1
          GETPOS = NO_POSITION
          SETPOS = NO_POSITION
        ELSE IF(TRIM(VARNAME).EQ.'MODEL.USTAR') THEN
          VARTYPE = 'DOUBLE'
          READONLY = .FALSE.
          NDIM = 1
          GETPOS = NO_POSITION
          SETPOS = NO_POSITION
        ELSE IF(TRIM(VARNAME).EQ.'MODEL.FREQ') THEN
          VARTYPE = 'DOUBLE'
          READONLY = .FALSE.
          NDIM = 1
          GETPOS = NO_POSITION
          SETPOS = NO_POSITION  
        ELSE IF(TRIM(VARNAME).EQ.'MODEL.TETA') THEN
          VARTYPE = 'DOUBLE'
          READONLY = .FALSE.
          NDIM = 1
          GETPOS = NO_POSITION
          SETPOS = NO_POSITION
        ELSE IF(TRIM(VARNAME).EQ.'MODEL.FMOY') THEN
          VARTYPE = 'DOUBLE'
          READONLY = .FALSE.
          NDIM = 1
          GETPOS = NO_POSITION
          SETPOS = NO_POSITION
        ELSE IF(TRIM(VARNAME).EQ.'MODEL.VARIAN') THEN
          VARTYPE = 'DOUBLE'
          READONLY = .FALSE.
          NDIM = 1
          GETPOS = NO_POSITION
          SETPOS = NO_POSITION
        ELSE IF(TRIM(VARNAME).EQ.'MODEL.TM01') THEN
          VARTYPE = 'DOUBLE'
          READONLY = .FALSE.
          NDIM = 1
          GETPOS = NO_POSITION
          SETPOS = NO_POSITION
        ELSE IF(TRIM(VARNAME).EQ.'MODEL.TM02') THEN
          VARTYPE = 'DOUBLE'
          READONLY = .FALSE.
          NDIM = 1
          GETPOS = NO_POSITION
          SETPOS = NO_POSITION
        ELSE IF(TRIM(VARNAME).EQ.'MODEL.PFREAD5') THEN
          VARTYPE = 'DOUBLE'
          READONLY = .FALSE.
          NDIM = 1
          GETPOS = NO_POSITION
          SETPOS = NO_POSITION
        ELSE IF(TRIM(VARNAME).EQ.'MODEL.PFREAD8') THEN
          VARTYPE = 'DOUBLE'
          READONLY = .FALSE.
          NDIM = 1
          GETPOS = NO_POSITION
          SETPOS = NO_POSITION
        ELSE IF(TRIM(VARNAME).EQ.'MODEL.VX_CTE') THEN
          VARTYPE = 'DOUBLE'
          READONLY = .FALSE.
          NDIM = 0
          GETPOS = NO_POSITION
          SETPOS = NO_POSITION
        ELSE IF(TRIM(VARNAME).EQ.'MODEL.VY_CTE') THEN
          VARTYPE = 'DOUBLE'
          READONLY = .FALSE.
          NDIM = 0
          GETPOS = NO_POSITION
          SETPOS = NO_POSITION         
        ELSE IF(TRIM(VARNAME).EQ.'MODEL.ENERGYDENSITY') THEN
          VARTYPE = 'DOUBLE'
          READONLY = .FALSE.
          NDIM = 3
          IENT = 1
          GETPOS = RUN_TIMESTEP_POS
          SETPOS = RUN_ALLOCATION_POS
        ELSE IF(TRIM(VARNAME).EQ.'MODEL.SVENT') THEN
          VARTYPE = 'INTEGER'
          READONLY = .FALSE.
          NDIM = 0
          GETPOS = RUN_READ_CASE_POS
          SETPOS = RUN_READ_CASE_POS
        ELSE IF(TRIM(VARNAME).EQ.'MODEL.LVENT') THEN
          VARTYPE = 'INTEGER'
          READONLY = .FALSE.
          NDIM = 0
          GETPOS = RUN_READ_CASE_POS
          SETPOS = RUN_READ_CASE_POS
        ELSE IF(TRIM(VARNAME).EQ.'MODEL.BETAM') THEN
          VARTYPE = 'DOUBLE'
          READONLY = .FALSE.
          NDIM = 0
          GETPOS = NO_POSITION
          SETPOS = NO_POSITION
        ELSE IF(TRIM(VARNAME).EQ.'MODEL.ALPHA') THEN
          VARTYPE = 'DOUBLE'
          READONLY = .FALSE.
          NDIM = 0
          GETPOS = NO_POSITION
          SETPOS = NO_POSITION
        ELSE IF(TRIM(VARNAME).EQ.'MODEL.DECAL') THEN
          VARTYPE = 'DOUBLE'
          READONLY = .FALSE.
          NDIM = 0
          GETPOS = NO_POSITION
          SETPOS = NO_POSITION
        ELSE IF(TRIM(VARNAME).EQ.'MODEL.ZVENT') THEN
          VARTYPE = 'DOUBLE'
          READONLY = .FALSE.
          NDIM = 0
          GETPOS = NO_POSITION
          SETPOS = NO_POSITION
        ELSE IF(TRIM(VARNAME).EQ.'MODEL.XKAPPA') THEN
          VARTYPE = 'DOUBLE'
          READONLY = .FALSE.
          NDIM = 0
          GETPOS = NO_POSITION
          SETPOS = NO_POSITION
        ELSE IF(TRIM(VARNAME).EQ.'MODEL.COEFWD') THEN
          VARTYPE = 'DOUBLE'
          READONLY = .FALSE.
          NDIM = 0
          GETPOS = NO_POSITION
          SETPOS = NO_POSITION
        ELSE IF(TRIM(VARNAME).EQ.'MODEL.COEFWE') THEN
          VARTYPE = 'DOUBLE'
          READONLY = .FALSE.
          NDIM = 0
          GETPOS = NO_POSITION
          SETPOS = NO_POSITION
        ELSE IF(TRIM(VARNAME).EQ.'MODEL.COEFWF') THEN
          VARTYPE = 'DOUBLE'
          READONLY = .FALSE.
          NDIM = 0
          GETPOS = NO_POSITION
          SETPOS = NO_POSITION
        ELSE IF(TRIM(VARNAME).EQ.'MODEL.COEFWH') THEN
          VARTYPE = 'DOUBLE'
          READONLY = .FALSE.
          NDIM = 0
          GETPOS = NO_POSITION
          SETPOS = NO_POSITION
        ELSE IF(TRIM(VARNAME).EQ.'MODEL.ROAIR') THEN
          VARTYPE = 'DOUBLE'
          READONLY = .FALSE.
          NDIM = 0
          GETPOS = NO_POSITION
          SETPOS = NO_POSITION
        ELSE IF(TRIM(VARNAME).EQ.'MODEL.ROEAU') THEN
          VARTYPE = 'DOUBLE'
          READONLY = .FALSE.
          NDIM = 0
          GETPOS = NO_POSITION
          SETPOS = NO_POSITION
        ELSE IF(TRIM(VARNAME).EQ.'MODEL.SBREK') THEN
          VARTYPE = 'INTEGER'
          READONLY = .FALSE.
          NDIM = 0
          GETPOS = RUN_READ_CASE_POS
          SETPOS = RUN_READ_CASE_POS
        ELSE IF(TRIM(VARNAME).EQ.'MODEL.IQBBJ') THEN
          VARTYPE = 'INTEGER'
          READONLY = .FALSE.
          NDIM = 0
          GETPOS = RUN_READ_CASE_POS
          SETPOS = RUN_READ_CASE_POS
        ELSE IF(TRIM(VARNAME).EQ.'MODEL.IHMBJ') THEN
          VARTYPE = 'INTEGER'
          READONLY = .FALSE.
          NDIM = 0
          GETPOS = RUN_READ_CASE_POS
          SETPOS = RUN_READ_CASE_POS
        ELSE IF(TRIM(VARNAME).EQ.'MODEL.IFRBJ') THEN
          VARTYPE = 'INTEGER'
          READONLY = .FALSE.
          NDIM = 0
          GETPOS = RUN_READ_CASE_POS
          SETPOS = RUN_READ_CASE_POS
        ELSE IF(TRIM(VARNAME).EQ.'MODEL.ALFABJ') THEN
          VARTYPE = 'DOUBLE'
          READONLY = .FALSE.
          NDIM = 0
          GETPOS = NO_POSITION
          SETPOS = NO_POSITION
        ELSE IF(TRIM(VARNAME).EQ.'MODEL.GAMBJ1') THEN
          VARTYPE = 'DOUBLE'
          READONLY = .FALSE.
          NDIM = 0
          GETPOS = NO_POSITION
          SETPOS = NO_POSITION
        ELSE IF(TRIM(VARNAME).EQ.'MODEL.GAMBJ2') THEN
          VARTYPE = 'DOUBLE'
          READONLY = .FALSE.
          NDIM = 0
          GETPOS = NO_POSITION
          SETPOS = NO_POSITION
        ELSE IF(TRIM(VARNAME).EQ.'MODEL.IWHTG') THEN
          VARTYPE = 'INTEGER'
          READONLY = .FALSE.
          NDIM = 0
          GETPOS = RUN_READ_CASE_POS
          SETPOS = RUN_READ_CASE_POS
        ELSE IF(TRIM(VARNAME).EQ.'MODEL.IFRTG') THEN
          VARTYPE = 'INTEGER'
          READONLY = .FALSE.
          NDIM = 0
          GETPOS = RUN_READ_CASE_POS
          SETPOS = RUN_READ_CASE_POS
        ELSE IF(TRIM(VARNAME).EQ.'MODEL.BORETG') THEN
          VARTYPE = 'DOUBLE'
          READONLY = .FALSE.
          NDIM = 0
          GETPOS = NO_POSITION
          SETPOS = NO_POSITION
        ELSE IF(TRIM(VARNAME).EQ.'MODEL.GAMATG') THEN
          VARTYPE = 'DOUBLE'
          READONLY = .FALSE.
          NDIM = 0
          GETPOS = NO_POSITION
          SETPOS = NO_POSITION
        ELSE IF(TRIM(VARNAME).EQ.'MODEL.IDISRO') THEN
          VARTYPE = 'INTEGER'
          READONLY = .FALSE.
          NDIM = 0
          GETPOS = RUN_READ_CASE_POS
          SETPOS = RUN_READ_CASE_POS
        ELSE IF(TRIM(VARNAME).EQ.'MODEL.IEXPRO') THEN
          VARTYPE = 'INTEGER'
          READONLY = .FALSE.
          NDIM = 0
          GETPOS = RUN_READ_CASE_POS
          SETPOS = RUN_READ_CASE_POS
        ELSE IF(TRIM(VARNAME).EQ.'MODEL.IFRRO') THEN
          VARTYPE = 'INTEGER'
          READONLY = .FALSE.
          NDIM = 0
          GETPOS = RUN_READ_CASE_POS
          SETPOS = RUN_READ_CASE_POS
        ELSE IF(TRIM(VARNAME).EQ.'MODEL.ALFARO') THEN
          VARTYPE = 'DOUBLE'
          READONLY = .FALSE.
          NDIM = 0
          GETPOS = NO_POSITION
          SETPOS = NO_POSITION
        ELSE IF(TRIM(VARNAME).EQ.'MODEL.GAMARO') THEN
          VARTYPE = 'DOUBLE'
          READONLY = .FALSE.
          NDIM = 0
          GETPOS = NO_POSITION
          SETPOS = NO_POSITION
        ELSE IF(TRIM(VARNAME).EQ.'MODEL.GAM2RO') THEN
          VARTYPE = 'DOUBLE'
          READONLY = .FALSE.
          NDIM = 0
          GETPOS = NO_POSITION
          SETPOS = NO_POSITION
        ELSE IF(TRIM(VARNAME).EQ.'MODEL.IFRIH') THEN
          VARTYPE = 'INTEGER'
          READONLY = .FALSE.
          NDIM = 0
          GETPOS = RUN_READ_CASE_POS
          SETPOS = RUN_READ_CASE_POS
        ELSE IF(TRIM(VARNAME).EQ.'MODEL.BETAIH') THEN
          VARTYPE = 'DOUBLE'
          READONLY = .FALSE.
          NDIM = 0
          GETPOS = NO_POSITION
          SETPOS = NO_POSITION
        ELSE IF(TRIM(VARNAME).EQ.'MODEL.EM2SIH') THEN
          VARTYPE = 'DOUBLE'
          READONLY = .FALSE.
          NDIM = 0
          GETPOS = NO_POSITION
          SETPOS = NO_POSITION
        ELSE IF(TRIM(VARNAME).EQ.'MODEL.SMOUT') THEN
          VARTYPE = 'INTEGER'
          READONLY = .FALSE.
          NDIM = 0
          GETPOS = RUN_READ_CASE_POS
          SETPOS = RUN_READ_CASE_POS
        ELSE IF(TRIM(VARNAME).EQ.'MODEL.CMOUT1') THEN
          VARTYPE = 'DOUBLE'
          READONLY = .FALSE.
          NDIM = 0
          GETPOS = NO_POSITION
          SETPOS = NO_POSITION
        ELSE IF(TRIM(VARNAME).EQ.'MODEL.CMOUT2') THEN
          VARTYPE = 'DOUBLE'
          READONLY = .FALSE.
          NDIM = 0
          GETPOS = NO_POSITION
          SETPOS = NO_POSITION
        ELSE IF(TRIM(VARNAME).EQ.'MODEL.CMOUT3') THEN
          VARTYPE = 'DOUBLE'
          READONLY = .FALSE.
          NDIM = 0
          GETPOS = NO_POSITION
          SETPOS = NO_POSITION
        ELSE IF(TRIM(VARNAME).EQ.'MODEL.CMOUT4') THEN
          VARTYPE = 'DOUBLE'
          READONLY = .FALSE.
          NDIM = 0
          GETPOS = NO_POSITION
          SETPOS = NO_POSITION
        ELSE IF(TRIM(VARNAME).EQ.'MODEL.CMOUT5') THEN
          VARTYPE = 'DOUBLE'
          READONLY = .FALSE.
          NDIM = 0
          GETPOS = NO_POSITION
          SETPOS = NO_POSITION
        ELSE IF(TRIM(VARNAME).EQ.'MODEL.CMOUT6') THEN
          VARTYPE = 'DOUBLE'
          READONLY = .FALSE.
          NDIM = 0
          GETPOS = NO_POSITION
          SETPOS = NO_POSITION
        ELSE IF(TRIM(VARNAME).EQ.'MODEL.SFROT') THEN
          VARTYPE = 'INTEGER'
          READONLY = .FALSE.
          NDIM = 0
          GETPOS = RUN_READ_CASE_POS
          SETPOS = RUN_READ_CASE_POS
        ELSE IF(TRIM(VARNAME).EQ.'MODEL.CFROT1') THEN
          VARTYPE = 'DOUBLE'
          READONLY = .FALSE.
          NDIM = 0
          GETPOS = NO_POSITION
          SETPOS = NO_POSITION
        ELSE IF(TRIM(VARNAME).EQ.'MODEL.SDSCU') THEN
          VARTYPE = 'INTEGER'
          READONLY = .FALSE.
          NDIM = 0
          GETPOS = RUN_READ_CASE_POS
          SETPOS = RUN_READ_CASE_POS
        ELSE IF(TRIM(VARNAME).EQ.'MODEL.CDSCUR') THEN
          VARTYPE = 'DOUBLE'
          READONLY = .FALSE.
          NDIM = 0
          GETPOS = NO_POSITION
          SETPOS = NO_POSITION
        ELSE
          IERR = UNKNOWN_VAR_ERROR
          ERR_MESS = 'UNKNOWN VARIABLE NAME : '//TRIM(VARNAME)
        ENDIF
        ! <get_var_type>
!
      END SUBROUTINE GET_VAR_TYPE_WAC_D
!
      !~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
      !>@brief Get the description of the ith variable
      !~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
      !>@param[in] I Number of the variable
      !>@param[in] VAR_LEN Size of varname
      !>@param[in] INFO_LEN Size of varinfo
      !>@param[out] VARNAME Name of the variable
      !>@param[out] VARINFO Description of the variable
      !>@param[out] IERR 0 if subroutine successfull,
      !!                        error id otherwise
      !~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
      SUBROUTINE GET_VAR_INFO_WAC_D(I, VAR_LEN, INFO_LEN,
     &                              VARNAME, VARINFO, IERR)
!
        INTEGER, INTENT(IN) :: I
        INTEGER, INTENT(IN) :: VAR_LEN
        INTEGER, INTENT(IN) :: INFO_LEN
        CHARACTER, INTENT(OUT) :: VARNAME(VAR_LEN)
        CHARACTER, INTENT(OUT) :: VARINFO(INFO_LEN)
        INTEGER, INTENT(OUT) :: IERR
!
        INTEGER :: J
!
        IERR = 0

        DO J=1,WAC_VAR_LEN
          VARNAME(J:J) = VNAME_WAC(I)(J:J)
        ENDDO
        DO J=1,WAC_INFO_LEN
          VARINFO(J:J) = VINFO_WAC(I)(J:J)
        ENDDO

        RETURN
      END SUBROUTINE GET_VAR_INFO_WAC_D
!
      !~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
      !>@brief Get a description of each variable
      !~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
      !>@param[out] IERR 0 if subroutine successfull,
      !!                        error id otherwise
      !~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
      SUBROUTINE SET_VAR_LIST_WAC_D(IERR)
!
        INTEGER, INTENT(OUT) :: IERR
!
        INTEGER :: I
!
        I=0
        IERR = 0
        IF(.NOT.ALLOCATED(VNAME_WAC)) THEN
          ALLOCATE(VNAME_WAC(NB_VAR_WAC),STAT=IERR)
          IF(IERR.NE.0) RETURN
          ALLOCATE(VINFO_WAC(NB_VAR_WAC),STAT=IERR)
          IF(IERR.NE.0) RETURN
!
          I = I + 1
          VNAME_WAC(I) = 'MODEL.AT'
          VINFO_WAC(I) = 'CURRENT TIME'
          I = I + 1
          VNAME_WAC(I) = 'MODEL.DT'
          VINFO_WAC(I) = 'TIME STEP'         
          I = I + 1
          VNAME_WAC(I) = 'MODEL.RAISF'
          VINFO_WAC(I) = 'FREQUENTIAL RATIO'         
          I = I + 1
          VNAME_WAC(I) = 'MODEL.APISET'
          VINFO_WAC(I) = 'API SET FRICTION VELOCITY'         
          I = I + 1
          VNAME_WAC(I) = 'MODEL.LT'
          VINFO_WAC(I) = 'CURRENT TIME STEP'
          I = I + 1
          VNAME_WAC(I) = 'MODEL.BCFILE'
          VINFO_WAC(I) = 'BOUNDARY CONDITION FILE NAME'
          I = I + 1
          VNAME_WAC(I) = 'MODEL.DEBUG'
          VINFO_WAC(I) = 'ACTIVATING DEBUG MODE'
          I = I + 1
          VNAME_WAC(I) = 'MODEL.GEOMETRYFILE'
          VINFO_WAC(I) = 'NAME OF THE GEOMERY FILE'
          I = I + 1
          VNAME_WAC(I) = 'MODEL.IKLE'
          VINFO_WAC(I) = 'CONNECTIVITY TABLE BETWEEN ELEMENT AND NODES'
          I = I + 1
          VNAME_WAC(I) = 'MODEL.NACHB'
          VINFO_WAC(I) = 'NUMBERS OF PROC CONTAINING A GIVEN POINT'
          I = I + 1
          VNAME_WAC(I) = 'MODEL.KNOLG'
          VINFO_WAC(I) =
     &         'GIVES THE INITIAL GLOBAL NUMBER OF A LOCAL POINT'
          I = I + 1
          VNAME_WAC(I) = 'MODEL.NELEM'
          VINFO_WAC(I) = 'NUMBER OF ELEMENT IN THE MESH'
          I = I + 1
          VNAME_WAC(I) = 'MODEL.NELMAX'
          VINFO_WAC(I) = 'MAXIMUM NUMBER OF ELEMENTS ENVISAGED'
          I = I + 1
          VNAME_WAC(I) = 'MODEL.NPOIN'
          VINFO_WAC(I) = 'NUMBER OF POINT IN THE MESH'
          I = I + 1
          VNAME_WAC(I) = 'MODEL.NPTFR'
          VINFO_WAC(I) = 'NUMBER OF BOUNDARY POINTS'
          I = I + 1
          VNAME_WAC(I) = 'MODEL.NTIMESTEPS'
          VINFO_WAC(I) = 'NUMBER OF TIME STEPS'
          I = I + 1
          VNAME_WAC(I) = 'MODEL.NFREQ'
          VINFO_WAC(I) = 'NUMBER OF DISCRETISED FREQUENCIES'
          I = I + 1
          VNAME_WAC(I) = 'MODEL.NDIRE'
          VINFO_WAC(I) = 'NUMBER OF DISCRETISED DIRECTIONS'
          I = I + 1
          VNAME_WAC(I) = 'MODEL.RESULTFILE'
          VINFO_WAC(I) = 'NAME OF THE RESULT FILE'
          I = I + 1
          VNAME_WAC(I) = 'MODEL.X'
          VINFO_WAC(I) = 'X COORDINATES FOR EACH POINT OF THE MESH'
          I = I + 1
          VNAME_WAC(I) = 'MODEL.Y'
          VINFO_WAC(I) = 'Y COORDINATES FOR EACH POINT OF THE MESH'
          I = I + 1
          VNAME_WAC(I) = 'MODEL.BOTTOM'
          VINFO_WAC(I) = 'BOTTOM'
          I = I + 1
          VNAME_WAC(I) = 'MODEL.WINDX'
          VINFO_WAC(I) = 'WINDX'
          I = I + 1
          VNAME_WAC(I) = 'MODEL.WINDY'
          VINFO_WAC(I) = 'WINDY'
          I = I + 1
          VNAME_WAC(I) = 'MODEL.HM0'
          VINFO_WAC(I) = 'HM0'
          I = I + 1
          VNAME_WAC(I) = 'MODEL.USTAR'
          VINFO_WAC(I) = 'USNEW'
          I = I + 1
          VNAME_WAC(I) = 'MODEL.FREQ'
          VINFO_WAC(I) = 'FREQ'
          I = I + 1
          VNAME_WAC(I) = 'MODEL.TETA'
          VINFO_WAC(I) = 'TETA'
          I = I + 1
          VNAME_WAC(I) = 'MODEL.FMOY'
          VINFO_WAC(I) = 'FMOY'
          I = I + 1
          VNAME_WAC(I) = 'MODEL.VARIAN'
          VINFO_WAC(I) = 'VARIAN'
          I = I + 1
          VNAME_WAC(I) = 'MODEL.TM01'
          VINFO_WAC(I) = 'TM01'
          I = I + 1
          VNAME_WAC(I) = 'MODEL.TM02'
          VINFO_WAC(I) = 'TM02'
          I = I + 1
          VNAME_WAC(I) = 'MODEL.PFREAD5'
          VINFO_WAC(I) = 'PFREAD5'
          I = I + 1
          VNAME_WAC(I) = 'MODEL.PFREAD8'
          VINFO_WAC(I) = 'PFREAD8'
          I = I + 1
          VNAME_WAC(I) = 'MODEL.VX_CTE'
          VINFO_WAC(I) = 'VX_CTE'
          I = I + 1
          VNAME_WAC(I) = 'MODEL.VY_CTE'
          VINFO_WAC(I) = 'VY_CTE'
          I = I + 1
          VNAME_WAC(I) = 'MODEL.ENERGYDENSITY'
          VINFO_WAC(I) = 'SF'
          I = I + 1
          VNAME_WAC(I) = 'MODEL.SVENT'
          VINFO_WAC(I) = 'CHOICE OF THE WIND INPUT MODEL'
          I = I + 1
          VNAME_WAC(I) = 'MODEL.LVENT'
          VINFO_WAC(I) = 'CAVALERI LINEAR WAVE GROWTH'
          I = I + 1
          VNAME_WAC(I) = 'MODEL.BETAM'
          VINFO_WAC(I) = 'BETAM'
          I = I + 1
          VNAME_WAC(I) = 'MODEL.ROAIR'
          VINFO_WAC(I) = 'AIR DENSITY'
          I = I + 1
          VNAME_WAC(I) = 'MODEL.ROEAU'
          VINFO_WAC(I) = 'WATER DENSITY'
          I = I + 1
          VNAME_WAC(I) = 'MODEL.ALPHA'
          VINFO_WAC(I) = 'CHARNOCK CONSTANT'
          I = I + 1
          VNAME_WAC(I) = 'MODEL.DECAL'
          VINFO_WAC(I) = 'SHIFT GROWING CURVE DUE TO WIND'
          I = I + 1
          VNAME_WAC(I) = 'MODEL.ZVENT'
          VINFO_WAC(I) = 'WIND MEASUREMENTS LEVEL'
          I = I + 1
          VNAME_WAC(I) = 'MODEL.XKAPPA'
          VINFO_WAC(I) = 'VON KARMAN CONSTANT'
          I = I + 1
          VNAME_WAC(I) = 'MODEL.COEFWD'
          VINFO_WAC(I) = 'YAN GENERATION COEFFICIENT D'
          I = I + 1
          VNAME_WAC(I) = 'MODEL.COEFWE'
          VINFO_WAC(I) = 'YAN GENERATION COEFFICIENT E'
          I = I + 1
          VNAME_WAC(I) = 'MODEL.COEFWF'
          VINFO_WAC(I) = 'YAN GENERATION COEFFICIENT F'
          I = I + 1
          VNAME_WAC(I) = 'MODEL.COEFWH'
          VINFO_WAC(I) = 'YAN GENERATION COEFFICIENT H'
          I = I + 1
          VNAME_WAC(I) = 'MODEL.SBREK'
          VINFO_WAC(I) = 'CHOICE OF THE BREAKING MODEL'
          I = I + 1
          VNAME_WAC(I) = 'MODEL.IQBBJ'
          VINFO_WAC(I) = 'CHOICE OF THE QB COMPUTATION METHOD (BJ)'
          I = I + 1
          VNAME_WAC(I) = 'MODEL.IHMBJ'
          VINFO_WAC(I) = 'CHOICE OF THE HM COMPUTATION METHOD (BJ)'
          I = I + 1
          VNAME_WAC(I) = 'MODEL.IFRBJ'
          VINFO_WAC(I) = 'CHOICE OF THE CHARACTERISTIC FREQUENCY (BJ)'
          I = I + 1
          VNAME_WAC(I) = 'MODEL.ALFABJ'
          VINFO_WAC(I) = 'COEFFICIENT ALPHA (BJ)'
          I = I + 1
          VNAME_WAC(I) = 'MODEL.GAMBJ1'
          VINFO_WAC(I) = 'COEFFICIENT GAMMA 1 (BJ)'
          I = I + 1
          VNAME_WAC(I) = 'MODEL.GAMBJ2'
          VINFO_WAC(I) = 'COEFFICIENT GAMMA 2 (BJ)'
          I = I + 1
          VNAME_WAC(I) = 'MODEL.IWHTG'
          VINFO_WAC(I) = 'CHOICE OF THE WEIGHTING FUNCTION (TG)'
          I = I + 1
          VNAME_WAC(I) = 'MODEL.IFRTG'
          VINFO_WAC(I) = 'CHOICE OF THE CHARACTERISTIC FREQUENCY (TG)'
          I = I + 1
          VNAME_WAC(I) = 'MODEL.BORETG'
          VINFO_WAC(I) = 'B COEFFICIENT (TG)'
          I = I + 1
          VNAME_WAC(I) = 'MODEL.GAMATG'
          VINFO_WAC(I) = 'GAMMA COEFFICIENT (TG)'
          I = I + 1
          VNAME_WAC(I) = 'MODEL.IDISRO'
          VINFO_WAC(I) = 'CHOICE OF THE WAVE HEIGHT DISTRIBUTION (RO)'
          I = I + 1
          VNAME_WAC(I) = 'MODEL.IEXPRO'
          VINFO_WAC(I) = 'EXPONENT WEIGHTING FUNCTION (RO)'
          I = I + 1
          VNAME_WAC(I) = 'MODEL.IFRRO'
          VINFO_WAC(I) = 'CHARACTERISTIC FREQUENCY (RO)'
          I = I + 1
          VNAME_WAC(I) = 'MODEL.ALFARO'
          VINFO_WAC(I) = 'COEFFICIENT ALPHA (RO)'
          I = I + 1
          VNAME_WAC(I) = 'MODEL.GAMARO'
          VINFO_WAC(I) = 'COEFFICIENT GAMMA (RO)'
          I = I + 1
          VNAME_WAC(I) = 'MODEL.GAM2RO'
          VINFO_WAC(I) = 'COEFFICIENT GAMMA 2 (RO)'
          I = I + 1
          VNAME_WAC(I) = 'MODEL.IFRIH'
          VINFO_WAC(I) = 'CHARACTERISTIC FREQUENCY (IH)'
          I = I + 1
          VNAME_WAC(I) = 'MODEL.BETAIH'
          VINFO_WAC(I) = 'COEFFICIENT BETA0 (IH)'
          I = I + 1
          VNAME_WAC(I) = 'MODEL.EM2SIH'
          VINFO_WAC(I) = 'COEFFICIENT M2STAR (IH)'
          I = I + 1
          VNAME_WAC(I) = 'MODEL.SMOUT'
          VINFO_WAC(I) = 'CHOICE OF THE WHITE CAPPING MODEL'
          I = I + 1
          VNAME_WAC(I) = 'MODEL.CMOUT1'
          VINFO_WAC(I) = 'WHITE CAPPING DISSIPATION COEFFICIENT'
          I = I + 1
          VNAME_WAC(I) = 'MODEL.CMOUT2'
          VINFO_WAC(I) = 'WHITE CAPPING WEIGHTING COEFFICIENT'
          I = I + 1
          VNAME_WAC(I) = 'MODEL.CMOUT3'
          VINFO_WAC(I) = 'WESTHUYSEN DISSIPATION COEFFICIENT'
          I = I + 1
          VNAME_WAC(I) = 'MODEL.CMOUT4'
          VINFO_WAC(I) = 'SATURATION THRESHOLD FOR THE DISSIPATION'
          I = I + 1
          VNAME_WAC(I) = 'MODEL.CMOUT5'
          VINFO_WAC(I) = 'WESTHUYSEN WHITE CAPPING DISSIPATION'
          I = I + 1
          VNAME_WAC(I) = 'MODEL.CMOUT6'
          VINFO_WAC(I) = 'WESTHUYSEN WEIGHTING COEFFICIENT'
          I = I + 1
          VNAME_WAC(I) = 'MODEL.SFROT'
          VINFO_WAC(I) = 'CHOICE OF BOTTOM FRICTION DISSIPATION'
          I = I + 1
          VNAME_WAC(I) = 'MODEL.CFROT1'
          VINFO_WAC(I) = 'BOTTOM FRICTION COEFFICIENT'
          I = I + 1
          VNAME_WAC(I) = 'MODEL.SDSCU'
          VINFO_WAC(I) = 'CHOICE OF STRONG CURRENT LIMITATION'
          I = I + 1
          VNAME_WAC(I) = 'MODEL.CDSCUR'
          VINFO_WAC(I) = 'DISSIPATION COEFFICIENT FOR STRONG CURRENT'
          ! <set_var_list>
          IF(I.NE.NB_VAR_WAC) THEN
            IERR = INCREASE_NB_VAR_WAC_ERROR
            RETURN
          ENDIF
        ENDIF
!
      END SUBROUTINE SET_VAR_LIST_WAC_D
!
      END MODULE API_HANDLE_VAR_WAC
