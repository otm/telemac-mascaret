!~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
!>@brief Module handling all the instance function
!~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
!
      MODULE API_INSTANCE_WAC
!
      USE API_HANDLE_ERROR
      USE BIEF
      USE DECLARATIONS_SPECIAL, ONLY : MAXKEYWORD
      USE DECLARATIONS_TOMAWAC
      IMPLICIT NONE

      PRIVATE

      PUBLIC :: CREATE_INSTANCE_WAC
      PUBLIC :: DELETE_INSTANCE_WAC
      PUBLIC :: CHECK_INSTANCE_WAC
      PUBLIC :: GET_INSTANCE_ERROR_WAC
      PUBLIC :: INSTANCE_WAC
      PUBLIC :: INSTANCE_LIST_WAC
!
      TYPE INSTANCE_WAC
        ! RUN POSITION
        INTEGER MYPOSITION
        ! ERROR MESSAGE
        CHARACTER(LEN=200) :: ERROR_MESSAGE
        INTEGER :: MAXKEYWORD
        ! LIST OF ALL THE VARIABLE FOR MODEL
!
        TYPE(BIEF_MESH), POINTER :: MESH
!
        INTEGER, POINTER :: NIT
        INTEGER, POINTER :: LT
        DOUBLE PRECISION, POINTER :: AT
        DOUBLE PRECISION, POINTER :: DT        
!       FREQUENTIAL RATIO
        DOUBLE PRECISION, POINTER :: RAISF
        LOGICAL, POINTER :: APISET
!
        TYPE(BIEF_FILE), POINTER :: WAC_FILES(:)
        INTEGER :: MAXLU_WAC
        INTEGER, POINTER :: WACRES
        INTEGER, POINTER :: WACGEO
        INTEGER, POINTER :: WACCLI
!
        INTEGER, POINTER :: DEBUG
!
        INTEGER, POINTER :: NFREQ, NDIRE
!       
        !ENERGY DENSITY
        TYPE(BIEF_OBJ), POINTER :: SF
        !BOTTOM
        DOUBLE PRECISION, POINTER :: ZF(:)
        !WAVE HEIGHT
        DOUBLE PRECISION, POINTER :: HM0(:)
        !FRICTION VELOCITY AT TN+1
        DOUBLE PRECISION, POINTER :: USNEW(:)
        !DISCRETIZED FREQUENCIES
        DOUBLE PRECISION, POINTER :: FREQ(:)
        !DISCRETIZED ANGLES
        DOUBLE PRECISION, POINTER :: TETA(:)
        !MEAN WAVE FREQUENCY
        DOUBLE PRECISION, POINTER :: FMOY(:)
        !VARIANCE
        DOUBLE PRECISION, POINTER :: VARIAN(:)
        !PEAK FREQUECY (READ 5)
        DOUBLE PRECISION, POINTER :: PFREAD5(:)
        !PEAK FREQUECY (READ 8)
        DOUBLE PRECISION, POINTER :: PFREAD8(:)
        !ZERO CROSSING WAVE PERIOD
        DOUBLE PRECISION, POINTER :: TM02(:)
        !MEAN WAVE PERIOD
        DOUBLE PRECISION, POINTER :: TM01(:)
        ! WIND IN X DIRECTION
        DOUBLE PRECISION, POINTER :: WINDX(:)
        ! WIND IN Y DIRECTION
        DOUBLE PRECISION, POINTER :: WINDY(:)

        INTEGER         :: NBMAXNSHARE
        INTEGER,        POINTER :: NPTIR

        DOUBLE PRECISION, POINTER :: VX_CTE
        DOUBLE PRECISION, POINTER :: VY_CTE
!------------------------------------------
!     WIND INPUT
!       CHOICE OF THE MODEL [0-3]
        INTEGER, POINTER :: SVENT 
!       LINEAR WAVE GROWTH
        INTEGER, POINTER :: LVENT
!       WIND GENERATION COEFFICIENT [0.8-1.6]
        DOUBLE PRECISION, POINTER :: BETAM
!       CHARNOCK CONSTANT [0.005-0.02]
        DOUBLE PRECISION, POINTER :: ALPHA
!       AIR DENSITY
        DOUBLE PRECISION, POINTER :: ROAIR
!       WATER DENSITY
        DOUBLE PRECISION, POINTER :: ROEAU
!       SHIFT GROWING CURVE DUE TO WIND
        DOUBLE PRECISION, POINTER :: DECAL
!       WIND MEASUREMENTS LEVEL
        DOUBLE PRECISION, POINTER :: ZVENT
!       VON KARMAN CONSTANT
        DOUBLE PRECISION, POINTER :: XKAPPA
!       YAN GENERATION COEFFICIENT D
        DOUBLE PRECISION, POINTER :: COEFWD
!       YAN GENERATION COEFFICIENT E
        DOUBLE PRECISION, POINTER :: COEFWE
!       YAN GENERATION COEFFICIENT F
        DOUBLE PRECISION, POINTER :: COEFWF
!       YAN GENERATION COEFFICIENT H
        DOUBLE PRECISION, POINTER :: COEFWH       
!------------------------------------------
!     BREAKING
!       CHOICE OF THE MODEL [0-4]
        INTEGER, POINTER :: SBREK
!       QB COMPUTATION METHOD [1-3] (BJ)
        INTEGER, POINTER :: IQBBJ
!       DEPTH-INDUCED BREAKING 1 (BJ) HM COMPUTATION METHOD [1-2]
        INTEGER, POINTER :: IHMBJ
!       DEPTH-INDUCED BREAKING 1 (BJ) CHARACTERISTIC FREQUENCY [1-6]
        INTEGER, POINTER :: IFRBJ
!       DEPTH-INDUCED BREAKING 1 (BJ) COEFFICIENT ALPHA
        DOUBLE PRECISION, POINTER :: ALFABJ
!       DEPTH-INDUCED BREAKING 1 (BJ) COEFFICIENT GAMMA1
        DOUBLE PRECISION, POINTER :: GAMBJ1
!       DEPTH-INDUCED BREAKING 1 (BJ) COEFFICIENT GAMMA2
        DOUBLE PRECISION, POINTER :: GAMBJ2
!       DEPTH-INDUCED BREAKING 2 (TG) WEIGHTING FUNCTION [1-2]
        INTEGER, POINTER :: IWHTG
!       DEPTH-INDUCED BREAKING 2 (TG) CHARACTERISTIC FREQUENCY [1-6]
        INTEGER, POINTER :: IFRTG
!       DEPTH-INDUCED BREAKING 2 (TG) COEFFICIENT B
        DOUBLE PRECISION, POINTER :: BORETG
!       DEPTH-INDUCED BREAKING 2 (TG) COEFFICIENT GAMMA
        DOUBLE PRECISION, POINTER :: GAMATG
!       DEPTH-INDUCED BREAKING 3 (RO) WAVE HEIGHT DISTRIBUTION
        INTEGER, POINTER :: IDISRO
!       DEPTH-INDUCED BREAKING 3 (RO) EXPONENT WEIGHTING FUNCTION
        INTEGER, POINTER :: IEXPRO
!       DEPTH-INDUCED BREAKING 3 (RO) CHARACTERISTIC FREQUENCY
        INTEGER, POINTER :: IFRRO
!       DEPTH-INDUCED BREAKING 3 (RO) COEFFICIENT ALPHA
        DOUBLE PRECISION, POINTER :: ALFARO
!       DEPTH-INDUCED BREAKING 3 (RO) COEFFICIENT GAMMA
        DOUBLE PRECISION, POINTER :: GAMARO
!       DEPTH-INDUCED BREAKING 3 (RO) COEFFICIENT GAMMA2
        DOUBLE PRECISION, POINTER :: GAM2RO
!       DEPTH-INDUCED BREAKING 4 (IH) CHARACTERISTIC FREQUENCY
        INTEGER, POINTER :: IFRIH
!       DEPTH-INDUCED BREAKING 4 (IH) COEFFICIENT BETA0
        DOUBLE PRECISION, POINTER :: BETAIH
!       DEPTH-INDUCED BREAKING 4 (IH) COEFFICIENT M2STAR
        DOUBLE PRECISION, POINTER :: EM2SIH
!------------------------------------------
!     WHITE CAPPING
!       CHOICE OF THE MODEL [0-2]
        INTEGER, POINTER :: SMOUT
!       WHITE CAPPING DISSIPATION COEFFICIENT [1.5-7]
        DOUBLE PRECISION, POINTER :: CMOUT1
!       WHITE CAPPING WEIGHTING COEFFICIENT [0.1-1]
        DOUBLE PRECISION, POINTER :: CMOUT2 
!       WESTHUYSEN DISSIPATION COEFFICIENT [1e-5-1e-4]
        DOUBLE PRECISION, POINTER :: CMOUT3
!       SATURATION THRESHOLD FOR THE DISSIPATION [0.5e-3-2.5e-3] 
        DOUBLE PRECISION, POINTER :: CMOUT4
!       WESTHUYSEN WHITE CAPPING DISSIPATION [1-6]
        DOUBLE PRECISION, POINTER :: CMOUT5
!       WESTHUYSEN WEIGHTING COEFFICIENT [0-1]
        DOUBLE PRECISION, POINTER :: CMOUT6
!------------------------------------------
!       BOTTOM FRICTION [0-1]
        INTEGER, POINTER :: SFROT
!       BOTTOM FRICTION COEFFICIENT
        DOUBLE PRECISION, POINTER :: CFROT1
!------------------------------------------
!       STRONG CURRENT [0-2]
        INTEGER, POINTER :: SDSCU
!       DISSIPATION COEFFICIENT FOR STRONG CURRENT
        DOUBLE PRECISION, POINTER :: CDSCUR
! <new_var>
!
      END TYPE ! MODEL_WAC
!
      INTEGER, PARAMETER :: MAX_INSTANCES=10
      TYPE(INSTANCE_WAC), POINTER :: INSTANCE_LIST_WAC(:)
      LOGICAL, ALLOCATABLE :: USED_INSTANCE(:)
!
      CONTAINS
!
      !~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
      !>@brief Creates a tomawac instance
      !~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
      !>@param[out] ID Id of the new instance
      !>@param[out] IERR 0 if subroutine successfull,
      !!                   error id otherwise
      !~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
      SUBROUTINE CREATE_INSTANCE_WAC(ID,IERR)
      ! initialise instance for tomawac
        INTEGER, INTENT(OUT) :: ID
        INTEGER, INTENT(OUT) :: IERR
!
        INTEGER :: I
        ID = 0
        IERR = 0
        ! If first time createing an instance allocating the instance array
        IF(.NOT. ALLOCATED(USED_INSTANCE)) THEN
          ALLOCATE(USED_INSTANCE(MAX_INSTANCES),STAT=IERR)
          IF(IERR.NE.0) THEN
            ERR_MESS = 'ERROR WHILE ALLOCATING USED INSTANCE ARRAY'
            RETURN
          ENDIF
          USED_INSTANCE = .FALSE.
          ALLOCATE(INSTANCE_LIST_WAC(MAX_INSTANCES),STAT=IERR)
          IF(IERR.NE.0) THEN
            ERR_MESS = 'ERROR WHILE ALLOCATING INSTANCE ARRAY'
            RETURN
          ENDIF
        ENDIF
!
        ! look for the first instance available
        I = 1
        DO WHILE(USED_INSTANCE(I).AND.I.LE.MAX_INSTANCES)
          I = I + 1
        ENDDO
        ID = I
        USED_INSTANCE(ID) = .TRUE.
!
        ! if still equals 0 no available instance was found then we crash
        IF(ID.EQ.(MAX_INSTANCES+1))THEN
          IERR = MAX_INSTANCE_ERROR
          ERR_MESS = "MAX INSTANCE REACHED "
          RETURN
        ENDIF
        !
        INSTANCE_LIST_WAC(ID)%MYPOSITION = NO_POSITION
!       Link with tomawac variables
        CALL UPDATE_INSTANCE_WAC(ID,IERR)

      END SUBROUTINE CREATE_INSTANCE_WAC
!
      !~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
      !>@brief Updates a tomawac instance
      !~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
      !>@param[out] ID Id of the new instance
      !>@param[out] IERR 0 if subroutine successfull,
      !!                   error id otherwise
      !~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
      SUBROUTINE UPDATE_INSTANCE_WAC(ID,IERR)
      ! initialise instance for tomawac
        INTEGER, INTENT(IN) :: ID
        INTEGER, INTENT(OUT) :: IERR
!
        IERR = 0
!       Link with tomawac variables
        INSTANCE_LIST_WAC(ID)%MESH   => MESH
        INSTANCE_LIST_WAC(ID)%MAXLU_WAC = MAXLU_WAC
!
        INSTANCE_LIST_WAC(ID)%DEBUG  => DEBUG
        INSTANCE_LIST_WAC(ID)%NIT    => NIT
        INSTANCE_LIST_WAC(ID)%LT     => LT
        INSTANCE_LIST_WAC(ID)%AT     => AT
        INSTANCE_LIST_WAC(ID)%DT     => DT
        INSTANCE_LIST_WAC(ID)%NFREQ  => NF
        INSTANCE_LIST_WAC(ID)%NDIRE  => NDIRE
        INSTANCE_LIST_WAC(ID)%RAISF  => RAISF
        INSTANCE_LIST_WAC(ID)%APISET => APISET
        INSTANCE_LIST_WAC(ID)%VX_CTE  => VX_CTE
        INSTANCE_LIST_WAC(ID)%VY_CTE  => VY_CTE
!
        INSTANCE_LIST_WAC(ID)%WAC_FILES => WAC_FILES
        INSTANCE_LIST_WAC(ID)%WACRES => WACRES
        INSTANCE_LIST_WAC(ID)%WACGEO => WACGEO
        INSTANCE_LIST_WAC(ID)%WACCLI => WACCLI
        INSTANCE_LIST_WAC(ID)%MAXKEYWORD = MAXKEYWORD
        INSTANCE_LIST_WAC(ID)%ZF => ZF

        INSTANCE_LIST_WAC(ID)%NPTIR => NPTIR
        INSTANCE_LIST_WAC(ID)%NBMAXNSHARE = NBMAXNSHARE
        INSTANCE_LIST_WAC(ID)%SF => SF
        INSTANCE_LIST_WAC(ID)%FREQ => FREQ
        INSTANCE_LIST_WAC(ID)%TETA => TETA
        INSTANCE_LIST_WAC(ID)%HM0 => HM0
        INSTANCE_LIST_WAC(ID)%USNEW => USNEW
        INSTANCE_LIST_WAC(ID)%FMOY => FMOY
        INSTANCE_LIST_WAC(ID)%VARIAN => VARIAN
        INSTANCE_LIST_WAC(ID)%TM01 => PTM01
        INSTANCE_LIST_WAC(ID)%TM02 => PTM02
        INSTANCE_LIST_WAC(ID)%PFREAD5 => FREA5
        INSTANCE_LIST_WAC(ID)%PFREAD8 => FREA8
        INSTANCE_LIST_WAC(ID)%WINDX => SUV%R
        INSTANCE_LIST_WAC(ID)%WINDY => SVV%R
!     WIND INPUT
        INSTANCE_LIST_WAC(ID)%SVENT => SVENT
        INSTANCE_LIST_WAC(ID)%SVENT => LVENT
        INSTANCE_LIST_WAC(ID)%BETAM => BETAM
        INSTANCE_LIST_WAC(ID)%ROAIR => ROAIR
        INSTANCE_LIST_WAC(ID)%ROEAU => ROEAU
        INSTANCE_LIST_WAC(ID)%ALPHA => ALPHA
        INSTANCE_LIST_WAC(ID)%DECAL => DECAL
        INSTANCE_LIST_WAC(ID)%ZVENT => ZVENT
        INSTANCE_LIST_WAC(ID)%XKAPPA => XKAPPA
        INSTANCE_LIST_WAC(ID)%COEFWD => COEFWD
        INSTANCE_LIST_WAC(ID)%COEFWE => COEFWE
        INSTANCE_LIST_WAC(ID)%COEFWF => COEFWF
        INSTANCE_LIST_WAC(ID)%COEFWH => COEFWH
!     BREAKING
        INSTANCE_LIST_WAC(ID)%SBREK => SBREK
        INSTANCE_LIST_WAC(ID)%IQBBJ => IQBBJ
        INSTANCE_LIST_WAC(ID)%IHMBJ => IHMBJ
        INSTANCE_LIST_WAC(ID)%IFRBJ => IFRBJ
        INSTANCE_LIST_WAC(ID)%ALFABJ => ALFABJ
        INSTANCE_LIST_WAC(ID)%GAMBJ1 => GAMBJ1
        INSTANCE_LIST_WAC(ID)%GAMBJ2 => GAMBJ2
        INSTANCE_LIST_WAC(ID)%IWHTG => IWHTG
        INSTANCE_LIST_WAC(ID)%IFRTG => IFRTG
        INSTANCE_LIST_WAC(ID)%BORETG => BORETG
        INSTANCE_LIST_WAC(ID)%GAMATG => GAMATG
        INSTANCE_LIST_WAC(ID)%IDISRO => IDISRO
        INSTANCE_LIST_WAC(ID)%IEXPRO => IEXPRO
        INSTANCE_LIST_WAC(ID)%IFRRO => IFRRO
        INSTANCE_LIST_WAC(ID)%ALFARO => ALFARO
        INSTANCE_LIST_WAC(ID)%GAMARO => GAMARO
        INSTANCE_LIST_WAC(ID)%GAM2RO => GAM2RO
        INSTANCE_LIST_WAC(ID)%IFRIH => IFRIH
        INSTANCE_LIST_WAC(ID)%BETAIH => BETAIH
        INSTANCE_LIST_WAC(ID)%EM2SIH => EM2SIH
!     WHITE CAPPING
        INSTANCE_LIST_WAC(ID)%SMOUT => SMOUT
        INSTANCE_LIST_WAC(ID)%CMOUT1 => CMOUT1
        INSTANCE_LIST_WAC(ID)%CMOUT2 => CMOUT2
        INSTANCE_LIST_WAC(ID)%CMOUT3 => CMOUT3
        INSTANCE_LIST_WAC(ID)%CMOUT4 => CMOUT4
        INSTANCE_LIST_WAC(ID)%CMOUT5 => CMOUT5
        INSTANCE_LIST_WAC(ID)%CMOUT6 => CMOUT6
!     BOTTOM FRICTION
        INSTANCE_LIST_WAC(ID)%SFROT => SFROT
        INSTANCE_LIST_WAC(ID)%CFROT1 => CFROT1
!     STRONG CURRENT
        INSTANCE_LIST_WAC(ID)%SDSCU => SDSCU
        INSTANCE_LIST_WAC(ID)%CDSCUR => CDSCUR
!     <new_link>
!
      END SUBROUTINE UPDATE_INSTANCE_WAC
!
      !~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
      !>@brief Deletes a tomawac instance
      !~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
      !>@param[in] ID Id of the instance
      !>@param[out] IERR 0 if subroutine successfull,
      !!                   error id otherwise
      !~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
      SUBROUTINE DELETE_INSTANCE_WAC(ID,IERR)
        INTEGER, INTENT(IN) :: ID
        INTEGER, INTENT(OUT) :: IERR
!
        IERR = 0
        !
        CALL CHECK_INSTANCE_WAC(ID,IERR)
        IF(IERR.NE.0) RETURN
        USED_INSTANCE(ID) = .FALSE.
      END SUBROUTINE DELETE_INSTANCE_WAC
!
      !~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
      !>@brief Check if the id is following convention
      !~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
      !>@param[in] ID Id of the instance
      !>@param[out] IERR 0 if subroutine successfull,
      !!                   error id otherwise
      !~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
      SUBROUTINE CHECK_INSTANCE_WAC(ID,IERR)
        INTEGER, INTENT(IN) :: ID
        INTEGER, INTENT(OUT) :: IERR
!
        IERR = 0
        IF(ID.LE.0 .OR. ID.GT.MAX_INSTANCES) THEN
          IERR = INVALID_INSTANCE_NUM_ERROR
          ERR_MESS = 'INVALID INSTANCE NUMBER'
          RETURN
        ENDIF
        IF(.NOT.USED_INSTANCE(ID)) THEN
          IERR = UNUSED_INSTANCE_ERROR
          ERR_MESS = 'INSTANCE NUMBER WAS NOT CREATED'
          RETURN
        ENDIF
        CALL UPDATE_INSTANCE_WAC(ID,IERR)
      END SUBROUTINE CHECK_INSTANCE_WAC
!
      !~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
      !>@brief Returns the error message of the instance
      !~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
      !>@param[in] ID Id of the instance
      !>@param[out] MESS The error message
      !~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
      SUBROUTINE GET_INSTANCE_ERROR_WAC(ID,MESS)
        INTEGER, INTENT(IN) :: ID
        CHARACTER(LEN=200), INTENT(OUT) :: MESS
!
        MESS = INSTANCE_LIST_WAC(ID)%ERROR_MESSAGE
!
      END SUBROUTINE GET_INSTANCE_ERROR_WAC
      END MODULE API_INSTANCE_WAC
