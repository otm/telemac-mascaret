!                   *******************
                    SUBROUTINE LECVENTI
!                   *******************
!
     &(F1,NAME1,MODE1, F2,NAME2,MODE2,
     & NPOIN2,NDON,FFORMAT,AT,TV1,TV2,
     & F11,F12,F21,F22,INDIC,NVAR,TEXTE,
     & TROUVE,UNITIME,PHASTIME)
!
!***********************************************************************
! TOMAWAC   V9P0
!***********************************************************************
!
!brief    THIS SUBROUTINE READS THE WINDS IN THE WIND FILE AT THE FIRST
!         TIME STEP.
!+
!~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
!| AT             |-->| COMPUTATION TIME
!| FFORMAT        |-->| DATA FILE FORMAT
!| F1             |<--| FIRST VARIABLE TO READ
!| F2             |<--| SECOND VARIABLE TO READ
!| F11            |<->| DATA VALUES AT TIME TV1 IN THE DATA FILE FOR F1
!| F12            |<->| DATA VALUES AT TIME TV2 IN THE DATA FILE FOR F1
!| F21            |<->| DATA VALUES AT TIME TV1 IN THE DATA FILE FOR F2
!| F22            |<->| DATA VALUES AT TIME TV2 IN THE DATA FILE FOR F2
!| INDIC          |-->| FILE FORMAT
!| MODE1          |-->| MODE: 0= DO NOT READ
!|                |   |       1= READ IF PRESENT
!|                |   |       2= READ AND STOP IF NOT PRESENT
!| MODE2          |-->| LIKE MODE1 FOR SECOND VARIABLE
!| NAME1          |-->| NAME OF FIRST VARIABLE
!| NAME2          |-->| NAME OF SECOND VARIABLE
!| NDON           |-->| LOGICAL UNIT NUMBER OF THA DATA FILE
!| NPOIN2         |-->| NUMBER OF POINTS IN 2D MESH
!| NVAR           |<--| NUMBER OF VARIABLES READ
!| PHASTIME       |-->| TIME SHIFT IN FILE
!| TEXTE          |<->| NAMES OF VARIABLES IN SERAFIN FILE
!| TROUVE         |<->| 2 LOGICAL, WILL SAY IF VARIABLES HAVE BEEN FOUND
!| TV1            |<->| DATA TIME T1
!| TV2            |<->| DATA TIME T2
!| UNITIME        |-->| UNIT OF TIME IN FILE
!~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
!
      USE BIEF
      USE INTERFACE_TOMAWAC, EX_LECVENTI => LECVENTI
      USE INTERFACE_HERMES
!
      USE DECLARATIONS_SPECIAL
      USE DECLARATIONS_TOMAWAC,ONLY: RECORD_WIND1, RECORD_WIND2
      IMPLICIT NONE
!
!
!+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
!
      INTEGER, INTENT(IN)             :: NDON,NPOIN2,INDIC
      INTEGER, INTENT(IN)             :: MODE1,MODE2
      INTEGER, INTENT(INOUT)          :: NVAR
      DOUBLE PRECISION, INTENT(INOUT) :: F1(NPOIN2),F2(NPOIN2)
      DOUBLE PRECISION, INTENT(INOUT) :: F11(NPOIN2),F12(NPOIN2)
      DOUBLE PRECISION, INTENT(INOUT) :: F21(NPOIN2),F22(NPOIN2)
      DOUBLE PRECISION, INTENT(IN)    :: AT,UNITIME,PHASTIME
      DOUBLE PRECISION, INTENT(INOUT) :: TV1,TV2
      CHARACTER(LEN=8), INTENT(IN)    :: FFORMAT
      CHARACTER(LEN=32),INTENT(IN)    :: NAME1,NAME2
      CHARACTER(LEN=32),INTENT(INOUT) :: TEXTE(30)
      LOGICAL, INTENT(INOUT)          :: TROUVE(2)
!
!+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
!
      INTEGER NP,I,J,MODE(2)
      DOUBLE PRECISION COEF
      CHARACTER(LEN=80) TITCAS
      CHARACTER(LEN=32) NAME(2),FULL_NAME(2)
      CHARACTER(LEN=16), ALLOCATABLE :: VAR_NAME(:), VAR_UNIT(:)
      DOUBLE PRECISION :: TIME1,TIME2
      INTEGER :: IERR
!
!-----------------------------------------------------------------------
!
      MODE(1)=MODE1
      MODE(2)=MODE2
      NAME(1)=NAME1
      NAME(2)=NAME2
!
!-----------------------------------------------------------------------
!     READS THE POINTS FROM LOGICAL UNIT NDON
!-----------------------------------------------------------------------
!
      IF(INDIC.EQ.3) THEN
!
!       -----------------------------------------------------------------
!       TELEMAC FORMAT,
!       VARIABLES 1 AND 2 ARE THE X AND Y COMPONENTS OF THE WIND
!       -----------------------------------------------------------------
!
        ! Getting title
        CALL GET_MESH_TITLE(FFORMAT,NDON,TITCAS,IERR)
        CALL CHECK_CALL(IERR,'LECVENTI:GET_MESH_TITLE')
        !
        CALL GET_DATA_NVAR(FFORMAT,NDON,NVAR,IERR)
        CALL CHECK_CALL(IERR,'LECVENTI:GET_DATA_NVAR')
        !
        ALLOCATE(VAR_NAME(NVAR),STAT=IERR)
        CALL CHECK_ALLOCATE(IERR,'LECVENTI:VAR_NAME')
        ALLOCATE(VAR_UNIT(NVAR),STAT=IERR)
        CALL CHECK_ALLOCATE(IERR,'LECVENTI:VAR_UNIT')
        CALL GET_DATA_VAR_LIST(FFORMAT,NDON,NVAR,VAR_NAME,VAR_UNIT,IERR)
        CALL CHECK_CALL(IERR,'LECVENTI:GET_DATA_VAR_LIST')
        DO I=1,NVAR
          TEXTE(I)(1:16) = VAR_NAME(I)
          TEXTE(I)(17:32) = VAR_UNIT(I)
          ! CHECK IF THE VARIABLES ARE IN THE FILE
          DO J=1,2
            IF((TEXTE(I).EQ.NAME(J)).AND.
     &        MODE(J).GT.0) THEN
              TROUVE(J) = .TRUE.
              FULL_NAME(J) = NAME(J)
            ENDIF
          ENDDO
        ENDDO
        DEALLOCATE(VAR_NAME)
        DEALLOCATE(VAR_UNIT)
        ! get the number of points
        CALL GET_MESH_NPOIN(FFORMAT,NDON,POINT_BND_ELT_TYPE,NP,IERR)
        CALL CHECK_CALL(IERR,'LECVENTI:GET_MESH_NPOIN')
        WRITE(LU,*) '--------------------------------------------'
        WRITE(LU,*) 'LECVENTI : READING OF TELEMAC DATA FILE '
        WRITE(LU,*) '         FILE TITLE : ',TITCAS
        WRITE(LU,*) '         NUMBER OF POINTS   : ',NP
        WRITE(LU,*) '--------------------------------------------'
        IF(NP.NE.NPOIN2) THEN
          WRITE(LU,*) ' '
          WRITE(LU,*) 'THE MESH OF THE WIND FILE'
          WRITE(LU,*) 'IS DIFFERENT FROM THE GEOMETRY FILE'
          WRITE(LU,*) ' '
          CALL PLANTE(1)
          STOP
        ENDIF
        ! Timesteps
        RECORD_WIND1 = 0
        RECORD_WIND2 = 1
        CALL GET_DATA_TIME(FFORMAT,NDON,RECORD_WIND1,TIME1,IERR)
        CALL CHECK_CALL(IERR,'LECVENTI:GET_DATA_TIME')
        TV1=(TIME1-PHASTIME)*UNITIME
        !
        IF(TV1.GT.AT) THEN
          WRITE(LU,*) '************************************************'
          WRITE(LU,*) 'THE FIRST RECORDING OF THE WIND FILE '
          WRITE(LU,*) '  ',TV1,' IS OLDER THAN THE BEGINNING '
          WRITE(LU,*) '  OF THE COMPUTATION',AT
          WRITE(LU,*) '************************************************'
          CALL PLANTE(1)
          STOP
        ENDIF
        !
        ! Get the two records before and after at for the interpolation
        DO
          CALL GET_DATA_TIME(FFORMAT,NDON,RECORD_WIND2,TIME2,IERR)
          CALL CHECK_CALL(IERR,'LECVENTI:GET_DATA_TIME')
          TV2=(TIME2-PHASTIME)*UNITIME
          IF(TV2.LT.AT) THEN
            RECORD_WIND1 = RECORD_WIND2
            RECORD_WIND2 = RECORD_WIND2 + 1
            TV1 = TV2
          ELSE
            EXIT
          ENDIF
        ENDDO
        ! Check if all the variables are found for record1
        DO J=1,2
          IF(MODE(J).EQ.2.AND..NOT.TROUVE(J)) THEN
            WRITE(LU,*) 'LECVENTI: VARIABLE ',NAME1,' NOT FOUND'
            WRITE(LU,*) TRIM(NAME(J)(1:16))
            CALL PLANTE(1)
            STOP
          ELSEIF(MODE(J).GT.0.AND.TROUVE(J)) THEN
            WRITE(LU,*) 'VARIABLE ',J,' READ (',
     &      TRIM(NAME(J)(1:16)),') AT TIME ',TV1
            ! Reads the data for variable j on record1
            IF(J.EQ.1) THEN
              CALL GET_DATA_VALUE(FFORMAT,NDON,RECORD_WIND1,
     &                            FULL_NAME(J),F11,NP,IERR)
            ELSEIF(J.EQ.2) THEN
              CALL GET_DATA_VALUE(FFORMAT,NDON,RECORD_WIND1,
     &                            FULL_NAME(J),F21,NP,IERR)
            ENDIF
          ENDIF
        ENDDO
        ! Read the variables
        !
        ! Check if all the variables are found for record2
        DO J=1,2
          IF(MODE(J).EQ.2.AND..NOT.TROUVE(J)) THEN
            WRITE(LU,*) 'LECVENTI: VARIABLE ',NAME1,' NOT FOUND'
            WRITE(LU,*) NAME(J)
            CALL PLANTE(1)
            STOP
          ELSEIF(MODE(J).GT.0.AND.TROUVE(J)) THEN
            WRITE(LU,*) 'VARIABLE ',J,' READ (',
     &      TRIM(NAME(J)(1:16)),') AT TIME ',TV2
            ! Reads the data for variable j on record1
            IF(J.EQ.1) THEN
              CALL GET_DATA_VALUE(FFORMAT,NDON,RECORD_WIND2,
     &                            FULL_NAME(J),F12,NP,IERR)
            ELSEIF(J.EQ.2) THEN
              CALL GET_DATA_VALUE(FFORMAT,NDON,RECORD_WIND2,
     &                            FULL_NAME(J),F22,NP,IERR)
            ENDIF
          ENDIF
        ENDDO
!
      ELSEIF (INDIC.EQ.4) THEN
!
!       READS A USER-DEFINED FORMAT
!
!       READS A WIND FIELD
        TROUVE(1)=.TRUE.
        TROUVE(2)=.TRUE.
        NVAR=2
        CALL VENUTI(NDON,FFORMAT)
        !
      ELSE
        WRITE(LU,*)'************************************************'
        WRITE(LU,*)'LECVENTI : UNKNOWN INDICATOR OF FORMAT : ',INDIC
        WRITE(LU,*)'         FOR THE WIND DATA FILE '
        WRITE(LU,*)'************************************************'
        CALL PLANTE(1)
        STOP
      ENDIF
!
!-----------------------------------------------------------------------
!   INTERPOLATES IN TIME
!-----------------------------------------------------------------------
!
      COEF=(AT-TV1)/(TV2-TV1)
!
      IF(TROUVE(1)) THEN
        DO I=1,NPOIN2
          F1(I)=(F12(I)-F11(I))*COEF+F11(I)
        ENDDO
      ENDIF
      IF(TROUVE(2)) THEN
        DO I=1,NPOIN2
          F2(I)=(F22(I)-F21(I))*COEF+F21(I)
        ENDDO
      ENDIF
!
!-----------------------------------------------------------------------
!
      RETURN
      END
