!                   *************************
                    SUBROUTINE SMALLSCALEBWC
!                   *************************
!
     & (F   ,  NF   , NDIRE, NPOIN2, TAUX3, DT, LIMIT_DEP)
!
!***********************************************************************
! TOMAWAC   V7P3
!***********************************************************************
!
!brief    calculation of small scale processes
!         backward compatible version
!         should give (almost) same results as original version if called from semimp
!
!history  alexander breugem
!+        11/10/20
!
!~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
!| TAUX3          |<->| WORK TABLE
!| F              |<->| DIRECTIONAL SPECTRUM
!| NF             |-->| NUMBER OF FREQUENCIES
!| NDIRE          |-->| NUMBER OF DIRECTIONS
!| NPOIN2         |-->| NUMBER OF POINTS IN 2D MESH
!| DT             |-->| TIME STEP
!~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
!
      USE DECLARATIONS_TOMAWAC, ONLY : VARIAN, DEBUG,
     &   SBREK, FMOY, 
     &   STRIA, XDTBRK,NDTBRK, XK, STSTOT, XKMOY,USOLD,USNEW,
     &   VEGETATION, POROUS, SDSCU, PROINF, LT,
     &   AMORP, T3_01,T3_02,CF,CG, DISSIP_BREAK
!
      USE INTERFACE_TOMAWAC, EX_SMALLSCALEBWC => SMALLSCALEBWC
!
      USE DECLARATIONS_TELEMAC
      USE DECLARATIONS_SPECIAL
      IMPLICIT NONE
!
!+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
!
      INTEGER, INTENT(IN) :: NPOIN2,NDIRE,NF
      DOUBLE PRECISION, INTENT (IN) :: DT
      LOGICAL, INTENT(IN) :: LIMIT_DEP
      DOUBLE PRECISION, INTENT(INOUT) :: TAUX3(NPOIN2)
      DOUBLE PRECISION, INTENT(INOUT) :: F(NPOIN2,NDIRE,NF)
!
!+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
!
      INTEGER  IP, IFF, IDT,JP
      DOUBLE PRECISION :: SOMME, DTN
      DOUBLE PRECISION, DIMENSION(:,:,:), POINTER :: TSTOT
      LOGICAL :: FIRST_LOOP, SECOND_LOOP
!
!-----------------------------------------------------------------------

      TSTOT(1:NPOIN2,1:NDIRE,1:NF)=>STSTOT%R

!
!     0.0 LIMIT BY DEP
!     --------------------------
!
      IF (LIMIT_DEP) THEN
        IF(DEBUG.EQ.2) WRITE(LU,*) '     APPEL DE LIMIT_BY_DEP'
        CALL LIMIT_BY_DEP(F, NF, NDIRE, NPOIN2, TAUX3)
        IF(DEBUG.EQ.2) WRITE(LU,*) '     RETOUR DE LIMIT_BY_DEP'
      ENDIF

!======================================================================
!
!     1.0 SMALL SCALE PROCESSES IN ONE LOOP
!     --------------------------
!
      FIRST_LOOP  = (((SBREK.GT.0 .AND. SBREK.LT.10).OR.
     & (STRIA.GT.0.AND.STRIA.LT.10).OR.
     &  VEGETATION.OR.POROUS).AND..NOT.PROINF).OR.(SDSCU.EQ.2)
      SECOND_LOOP = .NOT.PROINF.AND.
     &  (SBREK.EQ.10.OR.STRIA.EQ.10.OR.SDSCU.EQ.20)

      IF(FIRST_LOOP) THEN
!
!       1.1 COMPUTES A REPRESENTATIVE FREQUENCY
!       ------------------------------------------
!
        IF (SBREK.GT.0. .AND. SBREK .LT.10) THEN
          CALL BREAKFREQ (F   ,  NF   , NDIRE, NPOIN2, TAUX3)
          DO IP=1,NPOIN2
            DISSIP_BREAK%R(IP) = 0.D0
          ENDDO
        ENDIF
!
!.........LOOP ON SUB-TIME STEPS FOR BREAKING
!         = = = = = = = = = = = = = = = = = = = = = = = = = = =

        SOMME=(XDTBRK**NDTBRK-1.D0)/(XDTBRK-1.D0)
        DTN=DT/SOMME
!
        DO IDT=1,NDTBRK
!         1.2 INITIALISES THE ARRAYS FOR THE SOURCE-TERMS
!         ----------------------------------------------------
          DO IFF=1,NF
            DO JP=1,NDIRE
              DO IP=1,NPOIN2
                TSTOT(IP,JP,IFF)=0.D0
              ENDDO
            ENDDO
          ENDDO
!
!         1.3 COMPUTES THE TOTAL VARIANCE OF THE SPECTRUM
!         --------------------------------------------
!
          IF(DEBUG.EQ.2) WRITE(LU,*) '     APPEL DE TOTNRJ'
          CALL TOTNRJ(VARIAN, F, NF, NDIRE, NPOIN2)
          IF(DEBUG.EQ.2) WRITE(LU,*) '     RETOUR DE TOTNRJ'
!
!
!         1.4 COMPUTES THE WAVE BREAKING CONTRIBUTION
!         --------------------------------------
!
!         1.4.1 BREAKING ACCORDING TO BATTJES AND JANSSEN (1978)
!         - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
!
          IF(SBREK.EQ.1) THEN
!
            IF(DEBUG.EQ.2) WRITE(LU,*) '     APPEL DE QBREK1'
            CALL QBREK1
     & ( TSTOT , F     , TAUX3 , VARIAN, NF    , NDIRE , NPOIN2, DTN)
            IF(DEBUG.EQ.2) WRITE(LU,*) '     RETOUR DE QBREK1'
!
!
!         1.4.2 BREAKING ACCORDING TO THORNTON AND GUZA (1983)
!         - - - - - - - - - - - - - - - - - - - - - - - - - - - -
!
          ELSEIF(SBREK.EQ.2) THEN
!
            IF(DEBUG.EQ.2) WRITE(LU,*) '     APPEL DE QBREK2'
            CALL QBREK2
     & ( TSTOT , F     , TAUX3 , VARIAN, NF    , NDIRE ,
     &   NPOIN2, DTN)
            IF(DEBUG.EQ.2) WRITE(LU,*) '     RETOUR DE QBREK2'
!
!
!         1.4.3 BREAKING ACCORDING TO ROELVINK (1993)
!         - - - - - - - - - - - - - - - - - - - - - -
!
          ELSEIF(SBREK.EQ.3) THEN
!
            IF(DEBUG.EQ.2) WRITE(LU,*) '     APPEL DE QBREK3'
            CALL QBREK3
     &( TSTOT , F     , TAUX3 , VARIAN, NF    , NDIRE ,
     &  NPOIN2, DTN)
            IF(DEBUG.EQ.2) WRITE(LU,*) '     RETOUR DE QBREK3'
!
!
!         1.4.4 BREAKING ACCORDING TO IZUMIYA AND HORIKAWA (1984)
!         - - - - - - - - - - - - - - - - - - - - - - - - - - - -
!
          ELSEIF(SBREK.EQ.4) THEN
!
            IF(DEBUG.EQ.2) WRITE(LU,*) '     APPEL DE QBREK4'
            CALL QBREK4
     &( TSTOT , F     ,TAUX3,VARIAN, NF    , NDIRE , NPOIN2, DTN)
            IF(DEBUG.EQ.2) WRITE(LU,*) '     RETOUR DE QBREK4'
!
          ELSEIF((SBREK.NE.0).AND.(SBREK.NE.10)) THEN
            WRITE(LU,*) 'BREAKING FORMULATION NOT PROGRAMMED: ',
     &                     SBREK
            CALL PLANTE(1)
            STOP
          ENDIF
!
!       1.5 NON-LINEAR INTERACTIONS BETWEEN FREQUENCY TRIPLETS
!       -----------------------------------------------------------
          IF(STRIA.EQ.1) THEN
            IF(DEBUG.EQ.2) WRITE(LU,*) '     APPEL DE FREMOY'
            CALL FREMOY( FMOY, F, NF, NDIRE, NPOIN2)
            IF(DEBUG.EQ.2) WRITE(LU,*) '     RETOUR DE FREMOY'
            IF(DEBUG.EQ.2) WRITE(LU,*) '     APPEL DE QTRIA1'
            CALL QTRIA1
     &( F     , XK    , NF    , NDIRE , NPOIN2, TSTOT , VARIAN, FMOY  )
            IF(DEBUG.EQ.2) WRITE(LU,*) '     RETOUR DE QTRIA1'
!
          ELSEIF(STRIA.EQ.2) THEN
            IF(DEBUG.EQ.2) WRITE(LU,*) '     APPEL DE QTRIA2'
            CALL QTRIA2
     &( F     , XK    , NF    , NDIRE , NPOIN2, TSTOT )
            IF(DEBUG.EQ.2) WRITE(LU,*) '     RETOUR DE QTRIA2'
          ENDIF
!
!
!         1.6 WAVE BLOCKING DISSIPATION
!         -----------------------------
          IF(SDSCU.EQ.2) THEN
            IF(DEBUG.EQ.2) WRITE(LU,*) '     APPEL DE QDSCUR'
            CALL QDSCUR
     &( TSTOT ,  F     , CF    , XK    , USOLD , USNEW ,
     &  NF    , NDIRE , NPOIN2, TAUX3 ,T3_01%R,T3_02%R)
            IF(DEBUG.EQ.2) WRITE(LU,*) '     RETOUR DE QDSCUR'
          ENDIF
!
!======================================================================
!         1.7 VEGETATION
!VBA PRISE EN COMPTE VEGETATION
!======================================================================
!
          IF(VEGETATION) THEN
            IF(DEBUG.EQ.2) WRITE(LU,*) '     APPEL DE QVEG'
            CALL QVEG( TSTOT, F, VARIAN, FMOY, XKMOY, NF,
     &                   NDIRE  ,NPOIN2)
            IF(DEBUG.EQ.2) WRITE(LU,*) '     RETOUR DE QVEG'
          ENDIF
!======================================================================
!         1.8 POROUS MEDIA
!TF PRISE EN COMPTE POROSITE
!======================================================================
!
          IF(POROUS) THEN
            IF(DEBUG.EQ.2) WRITE(LU,*) '     APPEL DE QPOROS'
            CALL QPOROS( TSTOT , F , CG, LT, XK,
     &                   NF    , NDIRE  , NPOIN2, AMORP)
            IF(DEBUG.EQ.2) WRITE(LU,*) '     RETOUR DE QPOROS'
          ENDIF
!
!     1.9 UPDATES THE SPECTRUM - TAKES THE BREAKING SOURCE TERM
!             INTO ACCOUNT (EXPLICIT EULER SCHEME)
!         ---------------------------------------------------------
!
          DO IFF=1,NF
            DO JP=1,NDIRE
              DO IP=1,NPOIN2
                F(IP,JP,IFF)=MAX(F(IP,JP,IFF)+DTN*TSTOT(IP,JP,IFF),0.D0)
              ENDDO
            ENDDO
          ENDDO
!
          DTN=DTN*XDTBRK
!
        ENDDO
        !update breaking dissipation for all substeps
        IF (SBREK.GT.0. .AND. SBREK .LT.10) THEN
          DO IP=1,NPOIN2
            DISSIP_BREAK%R(IP) = DISSIP_BREAK%R(IP)/DT
          ENDDO
        ENDIF
      ENDIF
        !TODO think whether averaging of the dissip_break
        ! over the various nonlinear steps is needed for the input in
        ! surface rollers

!======================================================================
!       Processes with their own internal time step
!======================================================================

!----------------------------------------------------------------------

      IF (SECOND_LOOP) THEN

!new triads (Eldeberky)
        IF(STRIA.EQ.10) THEN
          IF(DEBUG.EQ.2) WRITE(LU,*) '     APPEL DE FREMOY'
          CALL FREMOY( FMOY, F, NF, NDIRE, NPOIN2)
          IF(DEBUG.EQ.2) WRITE(LU,*) '     RETOUR DE FREMOY'

          IF(DEBUG.EQ.2) WRITE(LU,*) '     APPEL DE TOTNRJ'
          CALL TOTNRJ(VARIAN, F, NF, NDIRE, NPOIN2)
          IF(DEBUG.EQ.2) WRITE(LU,*) '     RETOUR DE TOTNRJ'

          IF(DEBUG.EQ.2) WRITE(LU,*) '     APPEL DE QTRIA10'
          CALL QTRIA10(F ,XK, NF, NDIRE, NPOIN2, VARIAN, FMOY, DT)
          IF(DEBUG.EQ.2) WRITE(LU,*) '     RETOUR DE QTRIA10'
        ENDIF

!new wave current interaction
        IF (SDSCU.EQ.20) THEN
          IF(DEBUG.EQ.2) WRITE(LU,*) '     APPEL DE QDSCUR20'
          CALL QDSCUR20
     &  (  F     ,TSTOT,  CF    , XK    , USOLD , USNEW,
     &    NF    , NDIRE , NPOIN2, TAUX3,  DT)
          IF(DEBUG.EQ.2) WRITE(LU,*) '     RETOUR DE QDSCUR20'
        ENDIF

!newer battjes-jansen

        IF(SBREK.EQ.10) THEN
          IF(DEBUG.EQ.2) WRITE(LU,*) '     APPEL DE BREAKFREQ'
          CALL BREAKFREQ (F   ,  NF   , NDIRE, NPOIN2, TAUX3)
          IF(DEBUG.EQ.2) WRITE(LU,*) '     RETOUR DE BREAKFREQ'

          IF(DEBUG.EQ.2) WRITE(LU,*) '     APPEL DE TOTNRJ'
          CALL TOTNRJ(VARIAN, F, NF, NDIRE, NPOIN2)
          IF(DEBUG.EQ.2) WRITE(LU,*) '     RETOUR DE TOTNRJ'

          IF(DEBUG.EQ.2) WRITE(LU,*) '     APPEL DE QBREK10'
          CALL QBREK10 (F, TAUX3 ,VARIAN,
     &             NF , NDIRE , NPOIN2, DT)
          IF(DEBUG.EQ.2) WRITE(LU,*) '     RETOUR DE QBREK10'
        ENDIF

!
!======================================================================
!
      ENDIF

      RETURN
      END SUBROUTINE
!       ---------------------------------------------------------------
!
