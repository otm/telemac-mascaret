!                   *****************
                    SUBROUTINE NOUVEN
!                   *****************
!
     &     (F1,NAME1,MODE1, F2,NAME2,MODE2,
     &     NPOIN,NDON,FFORMAT,AT,TV1,TV2,
     &     F11,F12,F21,F22,INDIC,NVAR,TEXTE,
     &     TROUVE,UNITIME,PHASTIME)
!
!***********************************************************************
!     TOMAWAC   V9P0
!***********************************************************************
!
!     brief    GET THE  WIND VELOCITY FOR THE CURRENT TIME STEP.
!     +
!~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
!     | AT         |-->| COMPUTATION TIME
!     | FFORMAT    |-->| DATA FILE FORMAT
!     | F1         |<--| FIRST VARIABLE TO READ
!     | F2         |<--| SECOND VARIABLE TO READ
!     | F11        |<->| DATA VALUES AT TIME TV1 IN THE DATA FILE FOR F1
!     | F12        |<->| DATA VALUES AT TIME TV2 IN THE DATA FILE FOR F1
!     | F21        |<->| DATA VALUES AT TIME TV1 IN THE DATA FILE FOR F2
!     | F22        |<->| DATA VALUES AT TIME TV2 IN THE DATA FILE FOR F2
!     | INDIC      |-->| FILE FORMAT
!     | MODE1      |-->| MODE: 0= DO NOT READ
!     |            |   |       1= READ IF PRESENT
!     | MODE2      |-->| LIKE MODE1 FOR SECOND VARIABLE
!     | NAME1      |-->| NAME OF FIRST VARIABLE
!     | NAME2      |-->| NAME OF SECOND VARIABLE
!     | NDON       |-->| LOGICAL UNIT NUMBER OF THA DATA FILE
!     | NPOIN      |-->| NUMBER OF POINTS IN 2D MESH
!     | NVAR       |-->| NUMBER OF VARIABLES TO BE READ
!     | PHASTIME   |-->| TIME SHIFT IN FILE
!     | TEXTE      |<->| NAMES OF VARIABLES IN SERAFIN FILE
!     | TROUVE     |<->| 2 LOGICAL, WILL SAY IF VARIABLES HAVE BEEN FOUND
!     | TV1        |<->| TIME T1 IN THE DATA FILE
!     | TV2        |<->| TIME T2 IN THE DATA FILE
!     | UNITIME    |-->| UNIT OF TIME IN FILE
!~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
!
      USE BIEF
      USE DECLARATIONS_TOMAWAC,ONLY: DEBUG, RECORD_WIND1, RECORD_WIND2
      USE INTERFACE_TOMAWAC, EX_NOUVEN => NOUVEN
      USE INTERFACE_HERMES
      USE DECLARATIONS_SPECIAL
      IMPLICIT NONE
!
!     +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
!
      INTEGER, INTENT(IN)             :: NDON,NPOIN,INDIC
      INTEGER, INTENT(INOUT)          :: NVAR
      INTEGER, INTENT(IN)             :: MODE1,MODE2
      DOUBLE PRECISION, INTENT(INOUT) :: F1(NPOIN),F2(NPOIN)
      DOUBLE PRECISION, INTENT(INOUT) :: F11(NPOIN),F21(NPOIN)
      DOUBLE PRECISION, INTENT(INOUT) :: F12(NPOIN),F22(NPOIN)
      DOUBLE PRECISION, INTENT(IN)    :: AT,UNITIME,PHASTIME
      DOUBLE PRECISION, INTENT(INOUT) :: TV1,TV2
      CHARACTER(LEN=8), INTENT(IN)    :: FFORMAT
      CHARACTER(LEN=32),INTENT(IN)    :: NAME1,NAME2
      CHARACTER(LEN=32),INTENT(IN)    :: TEXTE(30)
      LOGICAL, INTENT(INOUT)          :: TROUVE(2)
!
!     +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
!
      INTEGER I,J,MODE(2)
      DOUBLE PRECISION COEF
      CHARACTER(LEN=32) NAME(2),FULL_NAME(2)
      CHARACTER(LEN=16), ALLOCATABLE :: VAR_NAME(:), VAR_UNIT(:)
      CHARACTER(LEN=32) COMPONENT
      LOGICAL :: VENUT=.FALSE.
      DOUBLE PRECISION :: TIME1,TIME2
      INTEGER :: IERR
!
!-----------------------------------------------------------------------
!
      MODE(1)=MODE1
      MODE(2)=MODE2
      NAME(1)=NAME1
      NAME(2)=NAME2
      DO J=1,2
        TROUVE(J)=.FALSE.
      ENDDO
!
!-----------------------------------------------------------------------
!
      IF(AT.GT.TV2) THEN
!
        IF(DEBUG.GT.0) THEN
          WRITE(LU,*) '   NOUVEN : READING A NEW RECORD'
        ENDIF
!
        IF(INDIC.EQ.3) THEN
!
!     ------------------------------------------------------------------
!     READS A SELAFIN FILE OF TYPE: TELEMAC
!     ------------------------------------------------------------------
!
!     The test is useless as fields have already been checked for the
!     initial value
!
          CALL GET_DATA_NVAR(FFORMAT,NDON,NVAR,IERR)
          CALL CHECK_CALL(IERR,'NOUVEN:GET_DATA_NVAR')
!
          ALLOCATE(VAR_NAME(NVAR),STAT=IERR)
          CALL CHECK_ALLOCATE(IERR,'NOUVEN:VAR_NAME')
          ALLOCATE(VAR_UNIT(NVAR),STAT=IERR)
          CALL CHECK_ALLOCATE(IERR,'NOUVEN:VAR_UNIT')
          CALL GET_DATA_VAR_LIST(FFORMAT,NDON,NVAR,VAR_NAME,VAR_UNIT,
     &                           IERR)
          CALL CHECK_CALL(IERR,'NOUVEN:GET_DATA_VAR_LIST')
          DO I=1,NVAR
            COMPONENT (1:16) = VAR_NAME(I)
            COMPONENT (17:32) = VAR_UNIT(I)
!     CHECK IF THE VARIABLES ARE IN THE FILE
            DO J=1,2
              IF((COMPONENT.EQ.NAME(J)).AND.
     &             MODE(J).GT.0) THEN
                TROUVE(J) = .TRUE.
                FULL_NAME(J) = NAME(J)
              ENDIF
            ENDDO
          ENDDO
          DEALLOCATE(VAR_NAME)
          DEALLOCATE(VAR_UNIT)
!
!     Look for the two records before and after at for the interpolation
          CALL GET_DATA_TIME(FFORMAT,NDON,RECORD_WIND1,TIME1,IERR)
          CALL CHECK_CALL(IERR,'NOUVEN:GET_DATA_TIME')
          TV1=(TIME1-PHASTIME)*UNITIME
          DO
            CALL GET_DATA_TIME(FFORMAT,NDON,RECORD_WIND2,TIME2,IERR)
            CALL CHECK_CALL(IERR,'NOUVEN:GET_DATA_TIME')
            TV2=(TIME2-PHASTIME)*UNITIME
            IF(TV2.LT.AT) THEN
              IF(DEBUG.GT.0) THEN
                WRITE(LU,*) ' NOUVEN: JUMP OF 1 DATA RECORD'
              ENDIF
              RECORD_WIND1 = RECORD_WIND2
              RECORD_WIND2 = RECORD_WIND2 + 1
              TV1 = TV2
            ELSE
              EXIT
            ENDIF
          ENDDO
!     Check if all the variables are found for record1
          DO J=1,2
            IF(MODE(J).EQ.2.AND..NOT.TROUVE(J)) THEN
              WRITE(LU,*) 'NOUVEN: VARIABLE ',NAME1,' NOT FOUND'
              WRITE(LU,*) TRIM(NAME(J)(1:16))
              CALL PLANTE(1)
              STOP
            ELSEIF(MODE(J).GT.0.AND.TROUVE(J)) THEN
              IF(DEBUG.GT.0) THEN
                WRITE(LU,*) 'VARIABLE ',J,' READ (',
     &               TRIM(NAME(J)(1:16)),') AT TIME ',TV1
              ENDIF
!     Read the data for variable j on record1
              IF(J.EQ.1) THEN
                CALL GET_DATA_VALUE(FFORMAT,NDON,RECORD_WIND1,
     &               FULL_NAME(J),F11,NPOIN,IERR)
              ELSEIF(J.EQ.2) THEN
                CALL GET_DATA_VALUE(FFORMAT,NDON,RECORD_WIND1,
     &               FULL_NAME(J),F21,NPOIN,IERR)
              ENDIF
            ENDIF
          ENDDO
!     Read the variables
!
!     Check if all the variables are found for record2
          DO J=1,2
            IF(MODE(J).EQ.2.AND..NOT.TROUVE(J)) THEN
              WRITE(LU,*) 'NOUVEN: VARIABLE ',NAME1,' NOT FOUND'
              WRITE(LU,*) NAME(J)
              CALL PLANTE(1)
              STOP
            ELSEIF(MODE(J).GT.0.AND.TROUVE(J)) THEN
              IF(DEBUG.GT.0) THEN
                WRITE(LU,*) 'VARIABLE ',J,' READ (',
     &               TRIM(NAME(J)(1:16)),') AT TIME ',TV2
              ENDIF
!     Read the data for variable j on record2
              IF(J.EQ.1) THEN
                CALL GET_DATA_VALUE(FFORMAT,NDON,RECORD_WIND2,
     &               FULL_NAME(J),F12,NPOIN,IERR)
              ELSEIF(J.EQ.2) THEN
                CALL GET_DATA_VALUE(FFORMAT,NDON,RECORD_WIND2,
     &               FULL_NAME(J),F22,NPOIN,IERR)
              ENDIF
            ENDIF
          ENDDO
!
        ELSEIF(INDIC.EQ.4) THEN
!
!     ---------------------------------------------------------------------
!     READS A USER-DEFINED FILE FORMAT
!     ---------------------------------------------------------------------
!
          TROUVE(1)=.TRUE.
          TROUVE(2)=.TRUE.
          VENUT=.TRUE.
          CALL VENUTI(NDON,FFORMAT)
        ENDIF
!
      ELSE
        TROUVE(1)=.FALSE.
        TROUVE(2)=.FALSE.
        DO I=1,NVAR
          DO J=1,2
            IF((TEXTE(I).EQ.NAME(J)).AND.
     &           MODE(J).GT.0) THEN
              TROUVE(J)=.TRUE.
            ENDIF
          ENDDO
        ENDDO
        IF(VENUT) THEN
          TROUVE(1)=.TRUE.
          TROUVE(2)=.TRUE.
        ENDIF
!
      ENDIF
!
!     --------------------------------------------------------------
!     INTERPOLATES
!     --------------------------------------------------------------
!
      IF(ABS(TV1-TV2).GT.1.D-30)  THEN
        COEF=(AT-TV1)/(TV2-TV1)
      ELSE
        COEF=0
      ENDIF
!
      IF(TROUVE(1)) THEN
        DO I=1,NPOIN
          F1(I)=(F12(I)-F11(I))*COEF+F11(I)
        ENDDO
      ENDIF
!
      IF(TROUVE(2)) THEN
        DO I=1,NPOIN
          F2(I)=(F22(I)-F21(I))*COEF+F21(I)
        ENDDO
      ENDIF
!
!-----------------------------------------------------------------------
!
      RETURN
      END
