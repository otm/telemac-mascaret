!                   *************************
                    SUBROUTINE BREAKFREQ
!                   *************************
!
     & (F   ,  NF   , NDIRE, NPOIN2, TAUX3)
!
!***********************************************************************
! TOMAWAC   V7P3
!***********************************************************************
!
!brief    frequency for wave breaking processes
!
!history  alexander breugem
!+        11/10/20
!
!~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
!| TAUX3          |<->| WORK TABLE
!| F              |<->| DIRECTIONAL SPECTRUM
!| NF             |-->| NUMBER OF FREQUENCIES
!| NDIRE          |-->| NUMBER OF DIRECTIONS
!| NPOIN2         |-->| NUMBER OF POINTS IN 2D MESH
!| DT             |-->| TIME STEP
!~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

      USE INTERFACE_TOMAWAC, EX_BREAKFREQ => BREAKFREQ
!
      USE DECLARATIONS_TELEMAC
      USE DECLARATIONS_SPECIAL
      USE DECLARATIONS_TOMAWAC, ONLY : CBAJ, DEBUG, IFRBJ, IFRTG,
     &                             IFRRO, IFRIH, SBREK,   FMOY
      IMPLICIT NONE
!
!+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
!
      INTEGER, INTENT(IN) :: NPOIN2,NDIRE,NF
      DOUBLE PRECISION, INTENT(INOUT) :: TAUX3(NPOIN2)
      DOUBLE PRECISION, INTENT(INOUT) :: F(NPOIN2,NDIRE,NF)
!
!+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
!
!
      INTEGER IFCAR, IP
!
!-----------------------------------------------------------------------
      !type of frequency calculation
      IF (SBREK.EQ.1) IFCAR = IFRBJ
      IF (SBREK.EQ.2) IFCAR = IFRTG
      IF (SBREK.GE.3) IFCAR = IFRRO
      IF (SBREK.GE.4) IFCAR = IFRIH
      IF (SBREK.EQ.10) IFCAR = IFRBJ

      IF (IFCAR.EQ.1) THEN
!
!       MEAN FREQUENCY FMOY
!       - - - - - - - - - - - -
        IF (CBAJ.EQ.1) THEN
          IF(DEBUG.EQ.2) WRITE(LU,*) '     APPEL DE FREMOY'
          CALL FREMOY(TAUX3, F, NF, NDIRE, NPOIN2 )
          IF(DEBUG.EQ.2) WRITE(LU,*) '     RETOUR DE FREMOY'
        ELSE
          DO IP=1,NPOIN2
            TAUX3(IP)=FMOY(IP)
          ENDDO
        ENDIF

      ELSE IF (IFCAR.EQ.2) THEN
!
!       MEAN FREQUENCY F01
!       - - - - - - - - - - -
        IF(DEBUG.EQ.2) WRITE(LU,*) '     APPEL DE FREM01'
        CALL FREM01( TAUX3, F, NF, NDIRE, NPOIN2)
        IF(DEBUG.EQ.2) WRITE(LU,*) '     RETOUR DE FREM01'

      ELSE IF (IFCAR.EQ.3) THEN
!
!       MEAN FREQUENCY F02
!       - - - - - - - - - - -
        IF(DEBUG.EQ.2) WRITE(LU,*) '     APPEL DE FREM02'
        CALL FREM02( TAUX3, F, NF, NDIRE, NPOIN2)
        IF(DEBUG.EQ.2) WRITE(LU,*) '     RETOUR DE FREM02'

      ELSE IF (IFCAR.EQ.4) THEN
!
!       PEAK FREQUENCY (DISCRETE FREQUENCY WITH MAX VARIANCE)
!       - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
        IF(DEBUG.EQ.2) WRITE(LU,*) '     APPEL DE FREPIC'
        CALL FREPIC( TAUX3, F, NF, NDIRE, NPOIN2)
        IF(DEBUG.EQ.2) WRITE(LU,*) '     RETOUR DE FREPIC'

      ELSE IF (IFCAR.EQ.5) THEN
!
!       PEAK FREQUENCY (READ WITH EXPONENT 5)
!       - - - - - - - - - - - - - - - - - - - - - - - - - -
        IF(DEBUG.EQ.2) WRITE(LU,*) '     APPEL DE FPREAD'
        CALL FPREAD( TAUX3, F, NF, NDIRE, NPOIN2, 5.D0)
        IF(DEBUG.EQ.2) WRITE(LU,*) '     RETOUR DE FPREAD'

      ELSE IF (IFCAR.EQ.6) THEN
!
!       PEAK FREQUENCY (READ WITH EXPONENT 8)
!       - - - - - - - - - - - - - - - - - - - - - - - - - -
        IF(DEBUG.EQ.2) WRITE(LU,*) '     APPEL DE FPREAD'
        CALL FPREAD( TAUX3, F, NF, NDIRE, NPOIN2, 8.D0)
        IF(DEBUG.EQ.2) WRITE(LU,*) '     RETOUR DE FPREAD'

      ELSE

        WRITE(LU,*) 'WAVE FREQUENCY NOT EXPECTED......IFCAR=',
     &                 IFCAR
        CALL PLANTE(1)
        STOP
      ENDIF
      
!----------------------------------------------------------------------

      RETURN

      END SUBROUTINE
