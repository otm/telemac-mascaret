!                   ***************
                    FUNCTION FONCRO
!                   ***************
!
     &( X     , B     , N     , A     , XM    )
!
!***********************************************************************
! TOMAWAC   V6P1                                   14/06/2011
!***********************************************************************
!
!brief    COMPUTES THE VALUE OF THE FUNCTION TO BE INTEGRATED
!+                FOR WAVE BREAKING (ROELVINK, 1993).
!
!history  F. BECQ (EDF/DER/LNH)
!+        26/03/96
!+        V1P1
!+
!~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
!| A              |-->| PARAMETER A OF THE FUNCTION TO BE INTEGRATED
!| B              |-->| PARAMETER B OF THE FUNCTION TO BE INTEGRATED
!| N              |-->| EXPONENT N OF THE FUNCTION TO BE INTEGRATED
!| X              |-->| VALUE AT WHICH THE FUNCTION IS EVALUATED
!| XM             |-->| PARAMETER M OF THE FUNCTION TO BE INTEGRATED
!~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
!
      USE INTERFACE_TOMAWAC, EX_FONCRO => FONCRO
      IMPLICIT NONE
!
!     VARIABLES IN ARGUMENT
!     """""""""""""""""""""
      INTEGER,          INTENT(IN)    :: N
      DOUBLE PRECISION, INTENT(IN)    :: X      , B     , A     , XM
      DOUBLE PRECISION FONCRO
!
!     LOCAL VARIABLES
!     """"""""""""""""""
      DOUBLE PRECISION AUX
!
!
      AUX   = A*X**XM
      FONCRO= XM*AUX*EXP(-AUX)*(1.D0-EXP(-(B*X)**N))
!
      RETURN
      END
