!     ******************
      SUBROUTINE QBREK10
!     ******************
!
     & (F     , FCAR  , VARIAN, NF    , NDIRE , NPOIN2, DT)
!
!***********************************************************************
! TOMAWAC   V6P1                                   23/06/2011
!***********************************************************************
!
!brief    COMPUTES THE CONTRIBUTION OF THE DEPTH-INDUCED
!+                BREAKING SOURCE TERM BASED ON BATTJES AND JANSSEN (1978).
!
!note     THIS SOURCE TERM IS LINEAR IN F(FREQ,TETA), AND THE LINEAR
!+          COEFFICIENT DOES NOT VARY WITH TIME.
!
!reference  BATTJES AND JANSSEN (1978) :
!+                     "ENERGY LOSS AND SET-UP DUE TO BREAKING
!+                      OF RANDOM WAVES". ICCE'78.
!
!history  F. BECQ; M. BENOIT (EDF/DER/LNH)
!+        14/02/96
!+        V1P1
!+
!history  WA BREUGEM (IMDC)
!+        06/05/2020
!+        V8P1
!+   Updated fast version
!
!~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
!| ALFABJ         |-->| COEFFICIENT ALPHA OF BJ MODEL
!| F              |-->| DIRECTIONAL SPECTRUM
!| FCAR           |-->| CHARACTERISTIC FREQUENCY
!| GAMBJ1         |-->| GAMMA1 CONSTANT OF WAVE BREAKING BJ MODEL
!| GAMBJ2         |-->| GAMMA2 CONSTANT OF WAVE BREAKING BJ MODEL
!| IHMBJ          |-->| DEPTH-INDUCED BREAKING CRITERIUM GIVING THE
!|                |   | BREAKING WAVE HEIGHT
!| IQBBJ          |-->| SELECTED QB COMPUTATION METHOD FOR BJ MODEL
!| NF             |-->| NUMBER OF FREQUENCIES
!| NDIRE          |-->| NUMBER OF DIRECTIONS
!| NPOIN2         |-->| NUMBER OF POINTS IN 2D MESH
!| TSTOT          |<->| TOTAL PART OF THE SOURCE TERM CONTRIBUTION
!| VARIAN         |-->| SPECTRUM VARIANCE
!~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
!
      USE DECLARATIONS_SPECIAL
      USE INTERFACE_TOMAWAC, ONLY : QBBJ78
      USE INTERFACE_PARALLEL, ONLY: P_ISUM
      USE DECLARATIONS_TOMAWAC, ONLY : ALFABJ, GAMBJ1, GAMBJ2, IQBBJ,
     &                 IHMBJ, DEPTH, BETABR, DISSIP_BREAK,
     &                 NDTBRK, XDTBRK

      USE INTERFACE_TOMAWAC, EX_QBREK10 => QBREK10
      IMPLICIT NONE
!
!.....VARIABLES IN ARGUMENT
!     """"""""""""""""""""
      INTEGER, INTENT(IN)            :: NF, NDIRE, NPOIN2
      DOUBLE PRECISION, INTENT(INOUT)   :: F(NPOIN2,NDIRE,NF)
      DOUBLE PRECISION, INTENT(IN)   :: VARIAN(NPOIN2),FCAR(NPOIN2)
      DOUBLE PRECISION, INTENT(IN)   :: DT
!
!.....LOCAL VARIABLES
!     """""""""""""""""
      LOGICAL          UPDATE
      INTEGER          JP   , IFF , IP
      DOUBLE PRECISION COEF , HM  , XK8 , XKCAR , B , QB , SEUIL
      DOUBLE PRECISION VARLOC, D

      DOUBLE PRECISION :: SOMME,DTN
      INTEGER :: IDT

      !minimum percentage of breaking waves
      DOUBLE PRECISION, PARAMETER :: QB_MIN  = 1.0D-3 


!***********************************************************************

      SEUIL=1.D-6
      COEF =-.25D0*ALFABJ

!.....COMPUTES THE LINEAR COEFFICIENT BETABR: QBREK1 = BETABR * F
!     """""""""""""""""""""""""""""""""""""""""""""""""""""""
      SOMME=(XDTBRK**NDTBRK-1.D0)/(XDTBRK-1.D0)

      DO IP = 1,NPOIN2
        UPDATE = .FALSE.
        IF (VARIAN(IP).GT.SEUIL) THEN
!
!..........COMPUTES THE MAXIMUM WAVE HEIGHT
!          """""""""""""""""""""""""""""""""""""""
          IF(IHMBJ.EQ.1) THEN
            HM  = GAMBJ2*DEPTH(IP)
          ELSEIF(IHMBJ.EQ.2) THEN
            CALL WNSCOU(XKCAR,FCAR(IP),DEPTH(IP))
            XK8 = GAMBJ1/XKCAR
            HM  = XK8*TANH(GAMBJ2*DEPTH(IP)/XK8)
          ENDIF
!
!..........COMPUTES THE FRACTION OF BREAKING WAVES
!          """"""""""""""""""""""""""""""""""""""""""""
          B   = SQRT(8.D0*VARIAN(IP))/HM
          QB  = QBBJ78(B,IQBBJ)
          IF (QB.GT.QB_MIN) THEN
            UPDATE = .TRUE.
            VARLOC = VARIAN(IP)
            DTN=DT/SOMME
            DO IDT=1,NDTBRK
              B  = SQRT(8.D0*VARLOC)/HM
              QB = QBBJ78(B,IQBBJ)
              D  = MIN(COEF*QB*FCAR(IP)*HM**2,0.0D0)
              VARLOC = MAX(VARLOC+DTN*D,0.0D0)
              DTN=DTN*XDTBRK
            ENDDO
            BETABR(IP) = VARLOC/VARIAN(IP)
          ELSE
            BETABR(IP) = 1.D0
          ENDIF
        ENDIF

        !FOR NOW ONE ITERATION WORKS BEST WITH PICRD METHOD
!
!.....TAKES THE SOURCE TERM INTO ACCOUNT
!     """"""""""""""""""""""""""""""""
        IF (UPDATE) THEN
          DO IFF = 1,NF
            DO JP = 1,NDIRE
              F(IP,JP,IFF) = F(IP,JP,IFF)*BETABR(IP)
            ENDDO ! JP
          ENDDO ! IFF
          DISSIP_BREAK%R(IP) = (VARLOC-VARIAN(IP))/DT
          !update definition for output to be the same as previous runs
          BETABR(IP) = -(1.D0-BETABR(IP))/DT
        ELSE
          DISSIP_BREAK%R(IP) = 0.0D0
          !update definition for output to be the same as previous runs
          BETABR(IP) = 0.0D0
        ENDIF
      ENDDO!IP


      RETURN
      END SUBROUTINE

