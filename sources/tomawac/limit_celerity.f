!     *************************
      SUBROUTINE LIMIT_CELERITY
!     *************************
!
     &     (NPOIN2,NDIRE,CX,CY,CT,CF,IFF)
!
!***********************************************************************
! TOMAWAC   V9P0                                   23/08/2024
!***********************************************************************
!
!brief   Changes the propagation velocity in spectral space
!
!note
!
!reference: Dietrich, J. C., Zijlema, M., Allier, P. E., 
!           Holthuijsen, L. H., Booij, N., Meixner, J. D., 
!           ... & Westerink, J. J. (2013). 
!            Limiters for spectral propagation velocities in SWAN. 
!            Ocean Modelling, 70, 85-102.
!
!~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
!| NPOIN2              |-->| NR OF POINTS IN THE MESH
!| NDIRE               |-->| NR OF DIRECTIONS
!| CX                  |-->| VELOCITY IN X DIRECTION
!| CY                  |-->| VELOCITY IN Y DIRECTION
!| CT                  |-->| VELOCITY IN THETA DIRECTION
!| CF                  |-->| VELOCITY IN F DIRECTION
!| IFF                 |-->| FREQUENCY BIN
!~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
!
      USE DECLARATIONS_SPECIAL
      USE BIEF
      USE DECLARATIONS_TOMAWAC, ONLY : ST0, ST1, MESH,IELM2,
     &   PI,F1,RAISF,LIMIT_REF_VEL, ALPHA_F, ALPHA_THETA,TRA40

      IMPLICIT NONE
!
!.....VARIABLES IN ARGUMENT
!     """"""""""""""""""""
      INTEGER, INTENT(IN)   ::NPOIN2,NDIRE
      DOUBLE PRECISION, DIMENSION(NPOIN2,NDIRE), INTENT(IN)  :: CX,CY
      DOUBLE PRECISION, INTENT(INOUT) :: CT(NPOIN2,NDIRE)
      DOUBLE PRECISION, INTENT(INOUT),OPTIONAL   :: CF(NPOIN2,NDIRE)
      INTEGER, INTENT(IN), OPTIONAL   :: IFF
      
!----------------------------------------------------------------------      
!
!.....LOCAL VARIABLES
!     """""""""""""""""
      INTEGER :: IP, ITHETA
      DOUBLE PRECISION ::  DTHETA, DF, CMAX

      ! LIMIT MAXIMUM CELERITY DUE TO DEPTH INDUCED REFRACTION
      LOGICAL :: LIMIT_CT
      ! LIMIT MAXIMUM CELERITY DUE TO CURRENT INDUCED REFRACTION
      LOGICAL :: LIMIT_CF

      DOUBLE PRECISION, POINTER :: CTMP(:)

!-----------------------------------------------------------------------
!     INITIALISATION
!-----------------------------------------------------------------------

      LIMIT_CT = .FALSE.
      LIMIT_CF = .FALSE.
      
      !TODO, it would be better to calculate this variable once and store it
      !This variable is used also in radiat.f, diffrac.f, cormat.f and iniphy.f
      CALL VECTOR(ST0,'=','MASBAS          ',IELM2,1.D0,ST1,
     &            ST1,ST1,ST1,ST1,ST1,MESH,.FALSE.,ST1,ASSPAR=.TRUE.)
      CALL OV('X=1/Y   ', X=ST0%R, Y=ST0%R, DIM1=NPOIN2)

      CTMP=>TRA40

      IF (LIMIT_REF_VEL.EQ.3 .OR. LIMIT_REF_VEL.EQ.1) THEN
        LIMIT_CT = .TRUE.
      ENDIF

      IF (LIMIT_REF_VEL.GE.2) THEN
        LIMIT_CF = .TRUE.
      ENDIF

      IF (.NOT.PRESENT(CF).OR..NOT.PRESENT(IFF)) THEN
        LIMIT_CF = .FALSE.
      ENDIF


!-----------------------------------------------------------------------
!     LIMTER FOR DEPTH INDUCED REFRACTION
!-----------------------------------------------------------------------

      IF (LIMIT_CT) THEN
        !DETERMINE MAXIMUM CELERITY
        DTHETA = 2.0D0*PI/DBLE(NDIRE)
        DO IP=1,NPOIN2
          CTMP(IP) = ALPHA_THETA*DTHETA*SQRT(ST0%R(IP))
        ENDDO
        !ADAPT CELERITY
        DO ITHETA=1,NDIRE
          DO IP=1,NPOIN2
            CMAX = CTMP(IP)*SQRT(CX(IP,ITHETA)**2 +
     &                           CY(IP,ITHETA)**2)
            CT(IP,ITHETA) =
     &          SIGN(MIN(CMAX,ABS(CT(IP,ITHETA))),CT(IP,ITHETA))
          ENDDO
        ENDDO
      ENDIF

!-----------------------------------------------------------------------
!     LIMTER FOR CURRENT INDUCED REFRACTION
!-----------------------------------------------------------------------

      IF (LIMIT_CF) THEN
        !DETERMINE MAXIMUM CELERITY
        DO IP=1,NPOIN2
          CTMP(IP) = ALPHA_F*SQRT(ST0%R(IP))
        ENDDO
        !ADAPT CELERITY
        DF = F1*(RAISF**(IFF+1)-RAISF**(IFF))
        DO ITHETA=1,NDIRE
          DO IP=1,NPOIN2
            CMAX  = CTMP(IP)*DF *SQRT(CX(IP,ITHETA)**2 +
     &                                CY(IP,ITHETA)**2)
            CF(IP,ITHETA) =
     &          SIGN(MIN(CMAX,ABS(CF(IP,ITHETA))),CF(IP,ITHETA))
          ENDDO
        ENDDO
      ENDIF
!
      RETURN
      END


