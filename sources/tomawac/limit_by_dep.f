!                   *************************
                    SUBROUTINE LIMIT_BY_DEP
!                   *************************
!
     & (F   ,  NF   , NDIRE, NPOIN2, TAUX3)
!
!***********************************************************************
! TOMAWAC   V7P3
!***********************************************************************
!
!brief    limit spectrum by depth
!
!history  alexander breugem
!+        21/11/23
!
!~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
!| TAUX3          |<->| WORK TABLE
!| F              |<->| DIRECTIONAL SPECTRUM
!| NF             |-->| NUMBER OF FREQUENCIES
!| NDIRE          |-->| NUMBER OF DIRECTIONS
!| NPOIN2         |-->| NUMBER OF POINTS IN 2D MESH
!~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
!
      USE DECLARATIONS_TOMAWAC, ONLY : VARIAN, DEBUG,
     &   COEFHS, DEPTH, PROINF

      USE INTERFACE_TOMAWAC, EX_LIMIT_BY_DEP => LIMIT_BY_DEP
!
      USE DECLARATIONS_TELEMAC
      USE DECLARATIONS_SPECIAL
      IMPLICIT NONE
!
!+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
!
      INTEGER, INTENT(IN) :: NPOIN2,NDIRE,NF
      DOUBLE PRECISION, INTENT(INOUT) :: TAUX3(NPOIN2)
      DOUBLE PRECISION, INTENT(INOUT) :: F(NPOIN2,NDIRE,NF)
!
!+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
!
      INTEGER  IP, IFF, JP
      DOUBLE PRECISION :: HM0MAX,HM0
!
!-----------------------------------------------------------------------

!
!     0.0 ROUTINE ONLY FOR FINITE DEPTH
!
      IF (PROINF) RETURN

!     0.1 COMPUTES THE TOTAL VARIANCE OF THE SPECTRUM
!     -----------------------------------------------
!
      IF(DEBUG.EQ.2) WRITE(LU,*) '     APPEL DE TOTNRJ'
      CALL TOTNRJ(VARIAN, F, NF, NDIRE, NPOIN2)
      IF(DEBUG.EQ.2) WRITE(LU,*) '     RETOUR DE TOTNRJ'
!
!     0.2 COMPUTES THE CORRECTION COEFFICIENT ON THE SPECTRUM
!     -------------------------------------------------------
!
      DO IP=1,NPOIN2
        HM0MAX=COEFHS*DEPTH(IP)
        HM0 =MAX(4.D0*SQRT(VARIAN(IP)),1.D-20)
        TAUX3(IP)=MIN((HM0MAX/HM0)**2,1.D0)
      ENDDO
!
!     0.3 CORRECTS THE SPECTRUM
!     --------------------------
!
      DO IFF=1,NF
        DO JP=1,NDIRE
          DO IP=1,NPOIN2
            F(IP,JP,IFF)=F(IP,JP,IFF)*TAUX3(IP)
          ENDDO
        ENDDO
      ENDDO

      RETURN
      END SUBROUTINE
!       ---------------------------------------------------------------
!
