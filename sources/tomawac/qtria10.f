!                   ******************
                    SUBROUTINE QTRIA10
!                   ******************
!
     &( F  , XK    , NF    , NDIRE , NPOIN2, FTOT  , FMOY, DT  )
!
!***********************************************************************
! TOMAWAC   V6P1                                   27/06/2011
!***********************************************************************
!
!brief    COMPUTES THE CONTRIBUTION OF THE NON-LINEAR
!+                INTERACTIONS SOURCE TERM (FREQUENCY TRIADS).
!
!history  W.A.Breugem
!+        23/12/2022
!+        V6P1
!+   Add time loops and restructred from qrtria1.
!
!~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
!| ALFLTA         |-->| COEFFICIENT ALPHA OF LTA TRIAD INTERACTION MODEL
!| DEPTH          |-->| WATER DEPTH
!| F              |-->| DIRECTIONAL SPECTRUM
!| FMOY           |-->| MEAN FREQUENCIES F-10
!| FREQ           |-->| DISCRETIZED FREQUENCIES
!| FTOT           |-->| SPECTRUM VARIANCE
!| GRAVIT         |-->| GRAVITY ACCELERATION
!| NF             |-->| NUMBER OF FREQUENCIES
!| NDIRE          |-->| NUMBER OF DIRECTIONS
!| NPOIN2         |-->| NUMBER OF POINTS IN 2D MESH
!| RFMLTA         |-->| COEFFICIENT OF LTA TRIAD INTERACTION MODEL
!| TSTOT          |<->| TOTAL PART OF THE SOURCE TERM CONTRIBUTION
!| XK             |-->| DISCRETIZED WAVE NUMBER
!~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
!
      USE DECLARATIONS_TOMAWAC, ONLY : GRAVIT,DEUPI, ALFLTA, RFMLTA
     &                , RAISF, FREQ, DEPTH, XDTBRK, NSTRI, STSTOT
!
      USE INTERFACE_TOMAWAC, EX_QTRIA10 => QTRIA10
      IMPLICIT NONE
!
!.....VARIABLES IN ARGUMENT
!     """"""""""""""""""""
      INTEGER, INTENT(IN)    ::  NF, NDIRE, NPOIN2
      DOUBLE PRECISION, INTENT(INOUT)    :: F(NPOIN2,NDIRE,NF)
      DOUBLE PRECISION, INTENT(IN)    :: XK(NPOIN2,NF), DT
      DOUBLE PRECISION, INTENT(IN)    :: FTOT(NPOIN2) , FMOY(NPOIN2)
!
!.....VARIABLES FROM MODULE TOMAWAC
!     """""""""""""""""""""""""""""
!| ALFLTA         |-->| COEFFICIENT ALPHA OF LTA TRIAD INTERACTION MODEL
!| RFMLTA         |-->| COEFFICIENT OF LTA TRIAD INTERACTION MODEL
!| RAISF          |-->| FREQUENTIAL RATIO
!
!
!.....LOCAL VARIABLES
!     """""""""""""""""
      INTEGER           IFMA  , IFF   , IPL   , IPO, ITER
      DOUBLE PRECISION  CPH   , CGR   , BIF   ,
     &                  FPS2  , CPHS2 , XKPS2 , URS   , RIND  , FMAX  ,
     &                  F2P   , CPH2P , XK2P  , CGR2P , E2P   , EPS2  ,
     &                  OMP   , OM2P  , COEF  , D     , DEUKD ,
     &                  SPLUS , SMOIN ,FP    , XKP, SOMME, DTN

      INTEGER          :: IIND1(NF), IIND2(NF)
      DOUBLE PRECISION :: RIND1(NF), RIND2(NF), RPS2(NF), RP (NF)
      DOUBLE PRECISION, POINTER :: TMP(:,:)
!
!.....FUNCTION / FORMULATION
!     """"""""""""""""
!
      COEF = GRAVIT*SQRT(2.D0)
      SOMME=(XDTBRK**NSTRI-1.D0)/(XDTBRK-1.D0)
      TMP(1:NDIRE,1:NF)=> STSTOT%R(1:NDIRE*NF)
!
      DO IPO=1,NPOIN2
!
        D=DEPTH(IPO)

!The Ursell number is constant, because changes in fmoy during subtimesteps, are not considered,
! and the variance does not changes (as this term should conserve energy).
! This means however that influence of other terms (like breaking) is neglected.
!
!.......COMPUTES THE URSELL NUMBER AT THE CONSIDERED POINT IN SPACE
!       """""""""""""""""""""""""""""""""""""""""""""""""""""
        URS = COEF*SQRT(FTOT(IPO))/(DEUPI*D*FMOY(IPO))**2
!
!.......COMPUTES THE CONTRIBUTION OF TRIADS ONLY IF URSELL > 0.1
!       """""""""""""""""""""""""""""""""""""""""
        IF (URS.GT.0.1D0) THEN
!
!.........COMPUTES THE SINE OF THE BIPHASE
!         """""""""""""""""""""""""""""
          IF (URS.GT.10.D0) THEN
            BIF=1.D0
          ELSE
            BIF=SIN(ABS(DEUPI/4.D0*(-1.D0+TANH(0.2D0/URS))))
          ENDIF
!
!.........COMPUTES THE MAXIMUM FREQUENTIAL INDEX
!         """"""""""""""""""""""""""""""""""""""
! The  frequency remains the same during the full time step (all sub time steps),
! because fmoy is constant (calculated beforehand). Therefore

          FMAX = RFMLTA*MAX(FMOY(IPO),FREQ(1))
          RIND = 1.D0 + LOG(FMAX/FREQ(1))/LOG(RAISF)
          IFMA = MIN(INT(RIND),NF)

          !initialization, with values that prevent calling this in the if statements for S- and S+ in the time loop
          IIND1 = 0
          IIND2 = NF+1
          RPS2 = 0.0D0
          RP = 0.0D0

          DO IFF=1,IFMA
            FP  = FREQ(IFF)
            OMP = DEUPI*FP
            XKP = XK(IPO,IFF)
            CPH = OMP/XKP
!
!
!.............COMPUTES THE CONTIBUTION S+
!             """""""""""""""""""""""""""
            FPS2 = FP/2.D0
            RIND = 1.D0 + LOG(FPS2/FREQ(1))/LOG(RAISF)
            IIND1(IFF) = INT(RIND)
            RIND1(IFF) = RIND-DBLE(IIND1(IFF))
!
            IF (IIND1(IFF).GT.0) THEN
              DEUKD=2.D0*XKP*D
              IF(DEUKD.LE.7.D2) THEN
                CGR = CPH*(0.5D0+XKP*D/SINH(2.D0*XKP*D))
              ELSE
                CGR = 0.5D0*CPH
              ENDIF
              CALL WNSCOU(XKPS2,FPS2,D)
              CPHS2 = DEUPI*FPS2/XKPS2
              RPS2(IFF) = CPH*CGR*(
     &                 XKPS2**2*(GRAVIT*D+2.D0*CPHS2**2)/(XKP*D)/
     &     (GRAVIT*D+(2.D0/15.D0)*GRAVIT*D**3*XKP**2-0.4D0*(OMP*D)**2)
     &                 )**2

            ENDIF
!
!
!...........COMPUTES THE CONTIBUTION S-
!           """""""""""""""""""""""""""
            F2P = 2.D0*FP
            RIND = 1.D0 + LOG(F2P/FREQ(1))/LOG(RAISF)
            IIND2(IFF) = INT(RIND)
            RIND2(IFF) = RIND-DBLE(IIND2(IFF))
            IF (IIND2(IFF).LT.IFMA) THEN
              OM2P  = DEUPI*F2P
              CALL WNSCOU(XK2P,F2P,D)
              CPH2P = OM2P/XK2P
              DEUKD=2.D0*XK2P*D
              IF(DEUKD.LE.700D2) THEN
                CGR2P = CPH2P*(0.5D0+XK2P*D/SINH(2.D0*XK2P*D))
              ELSE
                CGR2P = CPH2P*0.5D0
              ENDIF
              RP(IFF) = CPH2P*CGR2P*(
     &                XKP**2*(GRAVIT*D+2.D0*CPH**2)/(XK2P*D)/
     &   (GRAVIT*D+(2.D0/15.D0)*GRAVIT*D**3*XK2P**2-0.4D0*(OM2P*D)**2)
     &                )**2

            ENDIF
          ENDDO! iff

          !Time loop starts here
          DTN=DT/SOMME
          DO ITER =1,NSTRI
            TMP = 0.0D0
            DO IFF = 1,NF
              IF (IIND1(IFF).GT.0) THEN
                !S+ contribution
                DO IPL=1,NDIRE
                  EPS2=(1.D0-RIND1(IFF))*F(IPO,IPL,IIND1(IFF)) +
     &                RIND1(IFF)*F(IPO,IPL,IIND1(IFF)+1)
                  SPLUS = ALFLTA*RPS2(IFF)*BIF*
     &                 (EPS2-2.D0*F(IPO,IPL,IFF))*EPS2
                  IF(SPLUS.LT.0.D0) SPLUS = 0.D0
                  TMP(IPL,IFF) = TMP(IPL,IFF) + SPLUS
                ENDDO
              ENDIF
              !S- contribution
              IF (IIND2(IFF).LT.IFMA) THEN
                DO IPL=1,NDIRE
                  E2P = (1.D0-RIND2(IFF))*F(IPO,IPL,IIND2(IFF)) +
     &                RIND2(IFF)*F(IPO,IPL,IIND2(IFF)+1)
                  SMOIN = 2.D0*ALFLTA*RP(IFF)*BIF*F(IPO,IPL,IFF)
     &                  *(F(IPO,IPL,IFF)-2.D0*E2P)
                  IF(SMOIN.LT.0.D0) SMOIN = 0.D0
                  TMP(IPL,IFF) = TMP(IPL,IFF) - SMOIN
                ENDDO
              ENDIF
            ENDDO!IFF

            !now update the spectrum
            DO IFF = 1,NF
              DO IPL=1,NDIRE
                F(IPO,IPL,IFF) = MAX(F(IPO,IPL,IFF)+DTN*TMP(IPL,IFF),
     &                            0.0D0)
              ENDDO
            ENDDO
            !update time step
            DTN = DTN*XDTBRK
          ENDDO !NSTRI
        ENDIF  !urs gt 0.1
      ENDDO
!
      RETURN
      END
