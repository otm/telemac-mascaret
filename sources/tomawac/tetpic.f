!                   *****************
                    SUBROUTINE TETPIC
!                   *****************
!
     &( TETAP , F     , NDIRE , NF    , NPOIN2)
!
!***********************************************************************
! TOMAWAC   V9P0                                   23/08/2014
!***********************************************************************
!
!brief    COMPUTES THE MEAN DIRECTION AT THE FREQUENCY OF THE SPECTRAL PEAK
!+               (BY COMPUTING THE ARC TANGENT OF THE MEAN SINE AND
!+                COSINE). THE RESULT IS IN RADIANS.
!
!~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
!| F              |-->| VARIANCE DENSITY DIRECTIONAL SPECTRUM
!| NF             |-->| NUMBER OF FREQUENCIES
!| NDIRE          |-->| NUMBER OF DIRECTIONS
!| NPOIN2         |-->| NUMBER OF POINTS IN 2D MESH
!| TETAP          |<--| DIRECTIONAL SPECTRUM peak DIRECTION
!~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
!
      USE DECLARATIONS_TOMAWAC, ONLY : DEUPI, FREQ, DFREQ, TAILF,
     &                                 COSTET, SINTET
!
      IMPLICIT NONE
!
!+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
!
      INTEGER, INTENT(IN)    ::  NF    , NDIRE , NPOIN2
      DOUBLE PRECISION, INTENT(IN)    :: F(NPOIN2,NDIRE,NF)
      DOUBLE PRECISION, INTENT(INOUT) :: TETAP(NPOIN2)
!
!+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
!
      INTEGER  IP    , JP    , JF, JPEAK
      DOUBLE PRECISION AUXC  , AUXS  ,SEUIL , COEFT, DFDTET, E,EMAX
      DOUBLE PRECISION DTETAR, COSMOY,SINMOY, TAUXC, TAUXS
!
!-----------------------------------------------------------------------
!
      DTETAR=DEUPI/DBLE(NDIRE)
      SEUIL =1.D-10

!
!.....FIND THE PEAK FREQUENCY
!     """""""""""""""""""""""""""""""""""""""""""""

      DO IP = 1,NPOIN2
        !negative value, to make sure there is always a peak frequency
        EMAX = -1.0D0
        COSMOY=0.D0
        SINMOY=0.D0

        DO JF = 1,NF
!
!.......INTEGRATES WRT DIRECTIONS TO GET E(F)
!       """""""""""""""""""""""""""""""""""""""""""""""""
          E = 0.D0
          DO JP = 1, NDIRE
            E = E + F(IP,JP,JF)
          ENDDO                ! JP
!
!.......KEEPS THE MAXIMUM VALUE FOR E(F) AND ASSOCIATED FREQUENCY
!       """""""""""""""""""""""""""""""""""""""""""""""""""""
          IF (E.GT.EMAX) THEN
            EMAX = E
            JPEAK = JF
          ENDIF
        ENDDO                   ! JF
!
!-----C-------------------------------------------------------C
!-----C  SUMS UP THE DISCRETISED PART OF THE SPECTRUM         C
!-----C-------------------------------------------------------C
!

        DO JP=1,NDIRE
          AUXC=COSTET(JP)*DTETAR
          AUXS=SINTET(JP)*DTETAR
          COSMOY=COSMOY+F(IP,JP,JPEAK)*AUXC
          SINMOY=SINMOY+F(IP,JP,JPEAK)*AUXS
        ENDDO
!Note that no high frequency peak is needed

!-----C-------------------------------------------------------------C
!-----C  COMPUTES THE MEAN DIRECTION                                C
!-----C  (IN RADIANS BETWEEN 0 AND 2.PI)                            C
!-----C-------------------------------------------------------------C
!
        IF(ABS(SINMOY).LT.SEUIL.AND.
     &       ABS(COSMOY).LT.SEUIL) THEN
          TETAP(IP) = 0.D0
        ELSE
          TETAP(IP)=ATAN2(SINMOY,COSMOY)
          IF(TETAP(IP).LT.0.D0) TETAP(IP)=TETAP(IP)+DEUPI
        ENDIF
      ENDDO                     ! IP
!
!-----------------------------------------------------------------------
!
      RETURN
      END
