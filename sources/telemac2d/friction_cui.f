!                   ***********************
                    SUBROUTINE FRICTION_CUI
!                   ***********************
!
     &(HA,CD,A,HDVEG,ZETA,Y,ALFA,BETA,CPI,KARMAN,CP)
!
!***********************************************************************
! TELEMAC2D
!***********************************************************************
!
!brief    COMPUTES FRICTION COEFFICIENT FOR SUBMERGED VEGETATION
!+        FROM PARAMETERS WITH CUI ET AL.(2023) APPROACH
!
!history  GABRIELE FARINA (UNIBS) and VITO BACCHI (LNHE)
!+        04/03/2024
!+        V9.1
!+
!+   The algorithm was developed by
!
!~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
!| CD             |-->| BULK DRAG COEFFICIENT FOR VEGETATION
!| CP             |<--| VEGETATION FRICTION COEFFICIENT
!| HA             |-->| WATER DEPTH
!| HDVEG          |-->| HEIGHT OF DEFLECTED VEGETATION
!| KARMAN         |-->| VON KARMAN CONSTANT
!| A              |-->| FRONTAL AREA PER UNIT VOLUME
!| ZETA           |-->| ADIMENSIONAL PARAMETER ui/uUD
!| Y              |-->| ADIMENSIONAL PARAMETER y0/HDVEG
!| ALFA           |-->| ADIMENSIONAL PARAMETER Le/HDVEG
!| BETA           |-->| ADIMENSIONAL PARAMETER yi/HDVEG
!| CPI            |-->| WAKE FUNCTION COEFFICIENT
!~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
!
      USE INTERFACE_TELEMAC2D, EX_FRICTION_CUI => FRICTION_CUI
      IMPLICIT NONE
!
!+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
!
      DOUBLE PRECISION, INTENT(IN)  :: HA, CD, A, HDVEG, ZETA, Y
      DOUBLE PRECISION, INTENT(IN)  :: ALFA,BETA,CPI, KARMAN
      DOUBLE PRECISION, INTENT(OUT) :: CP
!
!+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
!
      DOUBLE PRECISION TERM1,TERM2,TERM3,TERM4,TERM5,TERM6,TERM7,TERM8,C
!
!-----------------------------------------------------------------------
!
      IF(MIN(HA,HDVEG).LE.0.D0) THEN
        CP = 0.D0
      ELSE IF (MIN(HA,HDVEG).GT.0.D0.AND.HA.LE.HDVEG) THEN
        CP = CD*A*HA
      ELSE
        TERM1 = SQRT(2.D0/(CD*A))
        TERM2 = (ALFA*HDVEG)/HA
        TERM3 = LOG(COSH(BETA/ALFA-HA/(ALFA*HDVEG))/(COSH(BETA/ALFA)))
        TERM4 = SQRT(HDVEG*(HA/HDVEG-1.D0))
        TERM5 = (HA-BETA*HDVEG)/HA
        TERM6 = (HA-BETA*HDVEG)/HDVEG
        C = -1/KARMAN*LOG(Y)
        TERM7 = TERM1*(1.D0+(ZETA-1.D0)*(1.D0+TERM2*TERM3))
        TERM8=TERM4*(TERM5/KARMAN*(LOG(TERM6)+KARMAN*C-1.D0)+CPI/KARMAN)
        CP = 2.D0*HA/((TERM7+TERM8)*(TERM7+TERM8))
      ENDIF
!
!-----------------------------------------------------------------------
!
      RETURN
      END
