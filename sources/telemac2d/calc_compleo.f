!                   ***********************
                    SUBROUTINE CALC_COMPLEO
!                   ***********************
!
!***********************************************************************
! TELEMAC2D
!***********************************************************************
!
!brief    COMPUTES VARIABLE COMPLEO OUTSIDE PRERES_TELEMAC2D
!+        AS PREVIOUSLY DONE
!+        NEEDED NOT TO UPDATE COMPLEO IF ONLY WRITE RESTART FILE
!+        WITHOUT WRITING RESULTS FILE
!
!history  C.-T. PHAM (LNHE)
!+        17/06/2024
!+        V9P0
!+   Creation from PRERES_TELEMAC2D
!
!~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
!~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
!
!      USE BIEF
      USE DECLARATIONS_TELEMAC2D
!      USE INTERFACE_TELEMAC2D
!
      USE DECLARATIONS_SPECIAL
      IMPLICIT NONE
!
!+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
!
      INTEGER LTT
!
!-----------------------------------------------------------------------
!
!     ALWAYS WRITE THE INITIAL CONDITIONS
      IF(LT.EQ.0) THEN
        LEO=.TRUE.
        COMPLEO=0
      ELSE
        IF(EQUA(1:15).NE.'SAINT-VENANT VF') THEN
!         FEM
          LTT=(LT/LEOPRD)*LEOPRD
          IF(LT.EQ.LTT.AND.LT.GE.PTINIG) LEO=.TRUE.
          IF(ABS(AT-(DUREE+AT0)).LT.1.E-14)LEO=.TRUE.
        ENDIF
!       FOR GRAPHICAL OUTPUTS
        IF(LEO)COMPLEO=COMPLEO+1
      ENDIF
!
      RETURN
      END
