!                   ************************
                    SUBROUTINE FRICTION_CALC
!                   ************************
!
     &(N_START, N_END, KFROT, NDEF, VK, GRAV,
     & KARMAN, CHESTR, DW_MESH, HC, VRES, CF)
!
!***********************************************************************
! TELEMAC2D   V7P1
!***********************************************************************
!
!brief    SETS THE FRICTION COEFFICIENT.
!code
!+     FRICTION LAWS PROGRAMMED :
!+
!+     KFROT = 0 :  NO FRICTION
!+     KFROT = 1 :  LAW OF HAALAND
!+     KFROT = 2 :  LAW OF CHEZY
!+     KFROT = 3 :  LAW OF STRICKLER
!+     KFROT = 4 :  LAW OF MANNING
!+     KFROT = 5 :  LAW OF NIKURADSE
!+     KFROT = 6 :  LAW OF COLEBROOK-WHITE
!+     KFROT = 7 :  LOG LAW OF WALL
!
!note     LAWS CODED UP : NO FRICTION; HAALAND; CHEZY;
!+                STRICKLER; MANNING; NIKURADSE; COLEBROOK-WHITE; WALL
!
!history  F. HUVELIN
!+        20/04/2004
!+
!
!history  J-M HERVOUET (LNHE)
!+
!+        V5P5
!+
!
!history  N.DURAND (HRW), S.E.BOURBAN (HRW)
!+        13/07/2010
!+        V6P0
!+   Translation of French comments within the FORTRAN sources into
!+   English comments
!
!history  N.DURAND (HRW), S.E.BOURBAN (HRW)
!+        21/08/2010
!+        V6P0
!+   Creation of DOXYGEN tags for automated documentation and
!+   cross-referencing of the FORTRAN sources
!
!~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
!| CF             |<--| ADIMENSIONAL FRICTION COEFFICIENT
!| CHESTR         |-->| FRICTION PARAMETER
!| DW_MESH        |-->| DISTANCE TO THE BOUNDARY
!| GRAV           |-->| GRAVITY ACCELERATION
!| HC             |-->| WATER DEPTH : MAX(H,HMIN)
!| KARMAN         |-->| VON KARMAN'S CONSTANT
!| KFROT          |-->| LAW USED FOR THE CALCULATION
!| N_START,N_END  |-->| STARTING AND ENDING POINT
!| VK             |-->| KINEMATIC VISCOSITY
!| VRES           |-->| RESULTANT VELOCITY
!~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
!
      USE BIEF
!
      USE DECLARATIONS_TELEMAC2D, ONLY:ENTET
      USE DECLARATIONS_SPECIAL
      IMPLICIT NONE
!
!+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
!
      INTEGER,          INTENT(IN)    :: N_START, N_END, KFROT
      DOUBLE PRECISION, INTENT(IN)    :: NDEF, VK, GRAV, KARMAN
      TYPE(BIEF_OBJ),   INTENT(IN)    :: CHESTR,DW_MESH,HC,VRES
      TYPE(BIEF_OBJ),   INTENT(INOUT) :: CF
!
!+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
!
      INTEGER                       :: I, ITER
      DOUBLE PRECISION              :: TIERS
      DOUBLE PRECISION              :: UNORM, INLOG, AUX
      DOUBLE PRECISION              :: OLDUST, OLDCF
      DOUBLE PRECISION              :: RE, UST, DW, DWPLUS
!
!     PARAMETERS FOR COLEBROOK-WHITE LAW
      INTEGER, PARAMETER            :: FP_NITERMAX = 50
      DOUBLE PRECISION, PARAMETER   :: C1 = 2.D0
      DOUBLE PRECISION, PARAMETER   :: C2 = 2.51D0
      DOUBLE PRECISION, PARAMETER   :: C3 = 12.D0
      DOUBLE PRECISION, PARAMETER   :: FP_PREC = 1.D-8
      DOUBLE PRECISION              :: FP_RESIDUAL
!
!-----------------------------------------------------------------------
!
      TIERS = 1.D0/3.D0
      SELECT CASE (KFROT)
!
!     NO FRICTION
!     -----------
      CASE(0)
        DO I = N_START, N_END
          CF%R(I) = 0.D0
        ENDDO
!
!     LAW OF HAALAND
!     --------------
      CASE(1)
        DO I = N_START, N_END
          UNORM = MAX(VRES%R(I),1.D-6)
          RE = 4.D0*VRES%R(I)*HC%R(I)/VK
          INLOG = 6.9D0/RE + (CHESTR%R(I)/(12.D0*HC%R(I)))**1.11D0
          AUX   = -3.6D0*LOG10(INLOG)
          CF%R(I) = 1.D0/AUX**2
        ENDDO
!
!     LAW OF CHEZY
!     ------------
      CASE(2)
        DO I = N_START, N_END
          CF%R(I) = 2.D0*GRAV/(CHESTR%R(I)**2)
        ENDDO
!
!     LAW OF STRICKLER
!     ----------------
      CASE(3)
        DO I = N_START, N_END
          CF%R(I) = 2.D0*GRAV/CHESTR%R(I)**2/HC%R(I)**TIERS
        ENDDO
!
!     LAW OF MANNING
!     --------------
      CASE(4)
        DO I = N_START, N_END
          CF%R(I) = 2.D0*GRAV*(CHESTR%R(I)**2)/HC%R(I)**TIERS
        ENDDO
!
!     LAW OF NIKURADSE
!     ----------------
      CASE(5)
        DO I = N_START, N_END
!          AUX = 30.D0*HC%R(I)/(CHESTR%R(I)*EXP(1.D0))
          AUX = MAX(1.001D0, HC%R(I)*11.036D0/CHESTR%R(I))
          CF%R(I) = 2.D0/(LOG(AUX)/KARMAN)**2
        ENDDO
!
!     LAW OF COLEBROOK-WHITE
!     ----------------------
      CASE(6)
        DO I = N_START, N_END
          RE = 4.D0*VRES%R(I)*HC%R(I)/VK
!         INITIAL GUESS (HAALAND)
          CF%R(I) = 1.D0
          OLDCF = 6.9D0/RE + (CHESTR%R(I)/(12.D0*HC%R(I)))**1.11D0
          OLDCF = -3.6D0*LOG10(OLDCF)
!         ITERATIONS
          ITER = 1
          FP_RESIDUAL = 1.D0
!         FIXED POINT ITERATIONS
          DO WHILE (ITER.LE.FP_NITERMAX .AND. FP_RESIDUAL.GE.FP_PREC)
            CF%R(I) = -2.D0*C1*LOG10( (C2/RE)*OLDCF 
     &              + CHESTR%R(I)/(C3*HC%R(I)) )
            FP_RESIDUAL = ABS(CF%R(I)-OLDCF)
            OLDCF = CF%R(I)
            ITER = ITER + 1
          ENDDO
          CF%R(I) = 1.D0/(CF%R(I)**2)
        ENDDO
!
!     LOG LAW OF WALL FOR VISCOUS FRICTION
!     ------------------------------------
      CASE(7)
        DO I = N_START, N_END
          IF(VRES%R(I) < 1.0D-9) THEN
            CF%R(I) = 20.D0 ! RISMO2D = 10.D0 AND TELEMAC2D = 2*10.D0
          ELSE
            DW = 0.33D0*DW_MESH%R(I)
            IF (CHESTR%R(I) < 1.0D-9) THEN
!             ITERATIVE COMPUTATION OF FRICTION VELOCITY UST
              UST    = 100.0*VK/DW
              OLDUST = 0.D0
              DO ITER = 1, 50
                IF (ABS((UST-OLDUST)/UST)<=1.0D-6) EXIT
                DWPLUS = DW*UST/VK
                IF (DWPLUS < 11.D0) DWPLUS = 11.D0
                OLDUST = UST
                UST    = KARMAN*VRES%R(I) / LOG(9.D0*DWPLUS)
              ENDDO
            ELSE
              UST = KARMAN*VRES%R(I) / (LOG(DW/CHESTR%R(I))+8.5D0)
              RE  = CHESTR%R(I)*UST  / VK
!             ITERATIVE COMPUTATION OF FRICTION VELOCITY UST
              IF (RE < 70.D0) THEN
                OLDUST = 0.D0
                DO ITER = 1, 50
                  IF (ABS((UST-OLDUST)/UST)<=1.0D-6) EXIT
                  DWPLUS = DW*UST/VK
                  IF (DWPLUS < 11.D0) DWPLUS = 11.D0
                  RE     = CHESTR%R(I)*UST/VK
                  OLDUST = UST
                  IF (RE < 3.32D0) THEN
                    UST = KARMAN*VRES%R(I) / LOG(9.D0*DWPLUS)
                  ELSE
                    UST = KARMAN*VRES%R(I)
     &                  / (  LOG(DW/CHESTR%R(I))
     &                     + 3.32D0*LOG(RE)/RE
     &                     + KARMAN*(8.5D0-9.96D0/RE))
                  ENDIF
                ENDDO
              ENDIF
            ENDIF
            DWPLUS = DW*UST/VK
            IF (DWPLUS < 11.D0 ) THEN
              UST = 11.0*VK / DW
              UST = SQRT(VRES%R(I)*VK/DW)
            ENDIF
          CF%R(I) = 2.D0*UST**2 / VRES%R(I)**2
          ENDIF
        ENDDO
!
!     OTHER CASES
!     -----------
      CASE DEFAULT
!
        WRITE(LU,2) KFROT
2       FORMAT(I5,' : UNKNOWN BOTTOM FRICTION LAW')
        CALL PLANTE(1)
        STOP
!
      END SELECT

!     WARNING IF ABSURD VALUE OF CF
!     -----------------------------
!     RANGE OF CF BASED ON:
!     - STRICKLER COEFFICIENT IN [2, 100] (CHOW 1959)
!     - WATER DEPTH BETWEEN 1 CM AND 1 KM
      IF (KFROT.GT.0) THEN
        IF(ENTET) THEN
          DO I = N_START, N_END
            IF (CF%R(I) .GT. 2.D1) THEN
              WRITE(LU,*) "WARNING: EXTREMLY HIGH VALUE OF FRICTION"
              WRITE(LU,*) "CF = ", CF%R(I)
              WRITE(LU,*) "CHECK 'FRICTION COEFFICIENT' "
              WRITE(LU,*) "OR 'LAW OF BOTTOM FRICTION' "
            ENDIF
          ENDDO
        ENDIF
      ENDIF
!
!-----------------------------------------------------------------------
!
      RETURN
      END
