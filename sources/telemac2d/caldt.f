!                     ****************
                      SUBROUTINE CALDT
!                     ****************
!
     &(DT,DTN,LEO)
!
!***********************************************************************
! TELEMAC-2D
!***********************************************************************
!
!>@brief  Computes the time step under CFL condition
!
!>@history  INRIA FOR KINETIC SCHEMES
!!
!!        V5P8
!!
!
!>@history  R. ATA (EDF-LNHE) DT FOR REMAINING SCHEMES
!!        15/03/2010
!!        V6P1
!!   Translation of French comments within the FORTRAN sources into
!!   English comments
!
!>@history  R. ATA (EDF-LNHE)
!!        15/01/2013
!!        V6P3
!!   introduce fixed time step
!!   handle very specific cases
!!   parallelization
!
!>@history  R. ATA (EDF-LNHE) INTRODUCE FIXED TIME STEP
!!        30/06/2013
!!        V6P3
!!  clean and remove unused variables
!
!>@history  R. ATA (EDF-LNHE) INTRODUCE FIXED TIME STEP
!!        11/01/2016
!!        V7P2
!!  adjust time step to graphical outputs
!
!>@history  J,RIEHME (ADJOINTWARE)
!!        November 2016
!!        V7P2
!!   Replaced EXTERNAL statements to parallel functions / subroutines
!!   by the INTERFACE_PARALLEL
!
!>@history  R. ATA (EDF-LNHE)
!!        18/10/ 2018
!!        V78P0
!!   fix a bug with graphical output when coupling with sisyphe
!!   COMPLEO is now incremented only by preres_telemac (like FE)
!
!~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
!>@param  [in,out]  DT      TIME STEP
!>@param  [in,out]  DTN     TIME STEP AT PREVIOUS ITERATION
!>@param  [in,out]  LEO     LOGICAL FOR GRAPHICAL OUTPUT
!~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
!
      USE BIEF_DEF, ONLY:NCSIZE
      USE DECLARATIONS_TELEMAC2D,ONLY:LEOPRD,PTINIG,GRAV,TORDER,
     &                                LEO_TRAC,NPOIN,ICIN,SORDER,LT,HN,
     &                                U,V,CFLWTD,AT,TMAX,MESH,DTVARI,
     &                                ENTET,DTCAS,LIMPRO,IKLE,MVISUV,
     &                                VISC,NTRAC,DIFNU,MVIST,DTMIN_FV,
     &                                EPS_DIV0,ADAPT_DT_TO_LEOPRD
      USE INTERFACE_TELEMAC2D, EX_CALDT => CALDT
      USE DECLARATIONS_SPECIAL
      USE INTERFACE_PARALLEL, ONLY : P_MIN
      IMPLICIT NONE
!
!+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
!
      DOUBLE PRECISION, INTENT(INOUT) :: DT,DTN
      LOGICAL        ,  INTENT(INOUT) :: LEO
!
!+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
!
      INTEGER IS,ITRAC
      LOGICAL DEJA,THEEND
      DOUBLE PRECISION RA3,SIGMAX,UA2,UA3,UNORM,DTT,DTV
      DOUBLE PRECISION RESTE0,RESTE1,GPRDTIME,DT2
!
      INTRINSIC MIN,CEILING
!
!-----------------------------------------------------------------------
!
!     INIT OF GRAPHICAL PRINTOUTS PARAMETERS
      THEEND=.FALSE.
      IF(LEO.OR.LT.EQ.1) LEO_TRAC = .FALSE.
      LEO   =.FALSE.
!
!     GRAPHICAL PRINTOUTS TIME DELTA
      GPRDTIME = LEOPRD*DTCAS
!
!     ******************************************************************
!     COMPUTE TIME STEP
!     ******************************************************************
      DEJA=.FALSE.
      DT = DTCAS
!
!     COMPUTE DT FOR ADVECTION (CFL CONDITION)
      DO IS=1,NPOIN
        IF(HN%R(IS).LT.0.D0.AND.ENTET.AND..NOT.DEJA)THEN
          WRITE(LU,*) 'CALDT WARNING : NEGATIVE WATER DEPTH'
          WRITE(LU,*) ' SEE NODE:',IS,' FOR EXAMPLE'
          DEJA = .TRUE.
        ELSE
          UA2    = U%R(IS)
          UA3    = V%R(IS)
          UNORM=SQRT(UA2*UA2 + UA3*UA3)
!           KINETIC SCHEME
          IF(ICIN.EQ.1)THEN
            RA3 = SQRT(1.5D0*GRAV)
            SIGMAX= MAX(EPS_DIV0, RA3*SQRT(ABS(HN%R(IS))) + UNORM)
!           OTHER SCHEMES
          ELSE
            SIGMAX= MAX(EPS_DIV0, SQRT(GRAV*ABS(HN%R(IS))) + UNORM)
          ENDIF 
          DT = MIN(DT, CFLWTD*MESH%DTHAUT%R(IS)/SIGMAX)
!         DTHAUT=|Ci|/Sum(Lij)
        ENDIF
      ENDDO
!
!     UPDATE DT FROM BOUNDARY CONDITIONS
      CALL CDL_DT(DT,LIMPRO%I)
    
!     UPDATE DT FROM DIFFUSION (FOURIER CONDITION)
      CALL DIFF_DT(DT,IKLE%I,MESH%NUBO%I,MESH%ELTSEG%I,MESH%IFABOR%I,
     &             MESH%VNOIN%R,MESH%COORDR%R,MVISUV,VISC%R)
      IF(NTRAC.GT.0) THEN
        DO ITRAC=1,NTRAC
          CALL DIFF_DT(DT,IKLE%I,MESH%NUBO%I,MESH%ELTSEG%I,
     &                 MESH%IFABOR%I,
     &                 MESH%VNOIN%R,MESH%COORDR%R,MVIST(ITRAC),VISC%R,
     &                 DIFNU(ITRAC))
        ENDDO
      ENDIF
      
!     FOR NEWMARK SCHEME
      IF (TORDER.EQ.2) THEN
        IF (LT.GT.1) THEN
          DT2 = 0.5D0*MIN(DT, DTN)
          DT = DT2
        ENDIF
      ENDIF
!
!     FOR PARALLELISM
      IF(NCSIZE.GT.1) THEN
        DT = P_MIN(DT)
      ENDIF
!
!     ******************************************************************
!     CONSTANT SET TIME STEP
!     ******************************************************************
      IF(DTVARI.EQV..FALSE.) THEN
        DTV = DT  ! STORE DT FOR RECOMMENDATION IN WARNING
        DT = DTCAS
      ENDIF
!
!     ******************************************************************
!     END OF COMPUTATION AND OTHER SANITY CHECKS
!     ******************************************************************
      IF(DTVARI) THEN
        IF(TMAX.LT.DT) DT=TMAX !REALLY CRAZY CASES
        DTT = TMAX-AT
        IF(DTT.LE.DT.AND.DTT.GT.0.D0) THEN !LAST TIME STEP
          DT = DTT
!         END OF COMUTATION WILL BE DETECTED IN SECOND_ORDER FOR ORDER 2
          IF(SORDER.EQ.1)THEEND=.TRUE.
        ENDIF
        IF(AT.GT.TMAX) THEN
          WRITE(LU,*)'CALDT: BAD TIME PARAMETERS'
          WRITE(LU,*)'TIME AND TMAX',AT,TMAX
          CALL PLANTE(1)
          STOP
        ENDIF
      ENDIF
!     GPRDTIME TOO SMALL
      IF(GPRDTIME.LT.1.D-12)THEN
        WRITE(LU,*) 'CALDT: PROBLEM WITH PARAMETERS: DTCAS,LEOPRD',
     &               DTCAS,LEOPRD
        CALL PLANTE(1)
        STOP
      ENDIF
!     CASE WHERE TIME STEP IS BIGGER THEN GRAPHIC OUTPUT (GPRDTIME)
      IF(GPRDTIME.LT.DT)THEN
        DT = DTCAS
        IF(ENTET)THEN
          WRITE(LU,*) 'WARNING: GRAPHICAL OUTPUT NOT OPTIMIZED: '
          WRITE(LU,*) '   - INITIAL TIME STEP TOO SMALL '
          WRITE(LU,*) '   - AND/OR PERIOD OF GRAPHIC OUTPUT TOO SMALL'
          WRITE(LU,*) 'THIS COULD REDUCE COMPUTATION TIME-STEP'
          WRITE(LU,*) 'AND INCREASE CPU TIME'
        ENDIF
      ENDIF
!
!     ******************************************************************
!     DETECTION OF GRAPHICAL PRINTOUTS (LEO = TRUE)
!     ******************************************************************
!
!     ONLY FOR ORDER 1, ORDER 2 IS IN SECOND_ORDER
      IF (SORDER.EQ.1) THEN
!
!       VARIABLE DT
        IF(DTVARI) THEN
!
!         ADAPT DT TO TAKE INTO ACCOUNT GRAPHIC OUTPUT
          IF (ADAPT_DT_TO_LEOPRD) THEN
            IS = CEILING(AT/GPRDTIME)
            RESTE1 = IS*GPRDTIME-AT
            IF(THEEND.OR.(RESTE1.LE.DT
     &               .AND.RESTE1.GT.DTMIN_FV
     &               .AND.LT.GT.PTINIG)) THEN
!             HERE THERE IS GRAPHICAL OUTPUTY
              LEO = .TRUE.
              DT = MIN(RESTE1,DT)
            ENDIF
!
!         DETECT CLOSEST GRAPHIC PRINTOUT TO CURRENT TIME (AT+DT)
!         (DETECTION OF LEO AT TIME AT, BUT WRITES RESULTS AT TIME AT+DT)
          ELSE
            IS = CEILING((AT+DT)/GPRDTIME)
            RESTE0 = AT+DT - (IS-1)*GPRDTIME
            IF (LT.EQ.1) THEN
              IF(THEEND.OR.(RESTE0.LT.DT .AND.LT.GT.PTINIG)) THEN
                LEO = .TRUE.
              ENDIF
            ELSE
              IF(THEEND.OR.(RESTE0.LE.DT.AND.LT.GT.PTINIG)) THEN 
                LEO = .TRUE.
              ENDIF
            ENDIF  
          ENDIF
!
!       CONSTANT DT
        ELSE
          IF (THEEND.OR.(MOD(LT,LEOPRD)==0)) THEN 
            LEO = .TRUE.
          ENDIF
        ENDIF
!
      ENDIF ! SORDER1
!
!     ******************************************************************
!     ERRORS AND WARNINGS
!     ******************************************************************
!
!     CASE DT <= 0
      IF(DT.LE.0.D0) THEN
        WRITE(LU,*) 'NEGATIVE (OR NIL) TIME-STEP: ', DT
        CALL PLANTE(1)
        STOP
      ENDIF
!
!     CASE CFL >= 1
      IF(CFLWTD.GE.1.D0) THEN
        IF(ENTET) THEN
          WRITE(LU,*) 'WARNING: CFL NOT GIVEN OR >=1 !...! '
          WRITE(LU,*) 'RECOMMENDED TIME-STEP (WITH CFL = 0.9): ',
     &                 0.9D0*DT/CFLWTD
        ENDIF
      ENDIF
!
!     CASE CONSTANT TIME STEP
      IF((DTVARI.EQV..FALSE.).AND.(SORDER.EQ.1)) THEN
        IF(ENTET) THEN
          WRITE(LU,*) 'WARNING: FIXED TIME-STEP (NOT RECOMMENDED)'
          WRITE(LU,*) 'TIME-STEP MAY NOT SATISFY CFL CONDITION, '
          WRITE(LU,*) 'RECOMMENDED TIME-STEP (WITH CFL = 0.9): ',
     &                 0.9D0*DTV/CFLWTD
        ENDIF
      ENDIF
!
!-----------------------------------------------------------------------
!
      RETURN
      END
