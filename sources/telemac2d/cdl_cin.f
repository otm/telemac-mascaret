!                       ******************
                        SUBROUTINE CDL_CIN
!                       ******************
!
     &(LIMPRO,UA,CE,FLUENT,FLUSORT,FLBOR,UBOR,VBOR)
!
!***********************************************************************
! TELEMAC 2D
!***********************************************************************
!
!>@brief  COMPUTATION OF THE CONVECTIVE FLUXES AT BOUNDARIES
!!    UA(1,IS) = H,  UA(2,IS)=HU  ,UA(3,IS)=HV
!
!>@history  INRIA
!!
!!        V5P8
!!
!
!>@history  R. ATA (EDF-LNHE) BALANCE OF WATER
!!        15/03/2010
!!        V6P1
!!   Translation of French comments within the FORTRAN sources into
!!   English comments
!
!>@history  R. ATA (EDF-LNHE)
!!        30/01/2015
!!        V7p0
!!   parallelization
!!
!
!>@history  J,RIEHME (ADJOINTWARE)
!!        November 2016
!!        V7P2
!!   Replaced EXTERNAL statements to parallel functions / subroutines
!!   by the INTERFACE_PARALLEL
!
!~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
!>@param  [in,out]  CE         FLUX
!>@param  [in,out]  FLBOR      IN AND OUT WATER MASS FLUX
!>@param  [in,out]  FLUENT     ENTERING MASS FLUX
!>@param  [in,out]  FLUSORT    EXITING MASS FLUX
!>@param  [in]      LIMPRO     TYPES OF BOUNDARY CONDITION
!>@param  [in]      UA         UA(1,IS) = H,  UA(2,IS)=HU  ,UA(3,IS)=HV
!>@param  [in,out]  UBOR       PRESCRIBED VALUES ON BOUNDARIES FOR U
!>@param  [in,out]  VBOR       PRESCRIBED VALUES ON BOUNDARIES FOR V
!~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
!
      USE BIEF
      USE INTERFACE_TELEMAC2D, EX_CDL_CIN => CDL_CIN
      USE DECLARATIONS_SPECIAL
      USE DECLARATIONS_TELEMAC2D, ONLY: NPOIN,NPTFR,HBOR,GRAV,
     &                            MESH,EPS_FV,NUMLIQ,
     &                            NFRLIQ,NDEBIT,T2D_FILES,T2DIMP,
     &                            LIUBOR,VNX1,VNY1,YESNOFR
      USE DECLARATIONS_TELEMAC, ONLY: KDIR,KNEU,KENT
      USE INTERFACE_PARALLEL, ONLY : P_SUM
      IMPLICIT NONE
!
!+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
!
      INTEGER, INTENT(IN)             :: LIMPRO(NPTFR,6)
      DOUBLE PRECISION, INTENT(IN)    :: UA(3,NPOIN)
      DOUBLE PRECISION, INTENT(INOUT) :: CE(NPOIN,3),FLUENT,FLUSORT
      TYPE(BIEF_OBJ) , INTENT(INOUT)  :: FLBOR
      TYPE(BIEF_OBJ) , INTENT(INOUT)  :: UBOR,VBOR
!
!+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
!
      INTEGER IS,K,NIT,IFRLIQ,IDRY
!
      DOUBLE PRECISION RA3,RA32,RA33, ALP,ALP2,ALP3,SG,SQ2
      DOUBLE PRECISION VNX,VNY,XNN,YNN,VNL,H,U,V,RUN
      DOUBLE PRECISION FLX(NPTFR,3),FLXG(3),FLXG_DW(3)
      DOUBLE PRECISION AUX,FLUTMP,RH,HRH,UNN,VNN
      DOUBLE PRECISION FHPLUS,FUPLUS,FHMOINS,FUMOINS
      DOUBLE PRECISION A,A1,A2,A3,ALPHA0,ALPHA1,ALPHA2,C,VP1,VP2 ,VP3
      DOUBLE PRECISION HG ,RHG,HRHG,UG,VG,DEST,RVG,CA1,AM
      DOUBLE PRECISION UIN,VIN,HUIN,HVIN
      DOUBLE PRECISION OUTFLOW,Q2(NFRLIQ)
      DOUBLE PRECISION HG_DW,UG_DW,VG_DW,U1_DW,V1_DW,U10,VNL1,XNN1,YNN1
!
      SQ2   = SQRT(2.D0)
      SG    = SQRT(GRAV)
      RA3   = SQRT(1.5D0*GRAV)
      RA32  = RA3**2
      RA33  = RA3*RA32
      ALP   = 0.5D0/RA3
      ALP2  = 0.5D0 *ALP
      ALP3  = ALP/3.D0
!
!     CORRECTION OF U/VBOR IF NECESSARY
!
      DO IFRLIQ=1,NFRLIQ
        Q2(IFRLIQ) = 0.D0
      ENDDO
      DO K=1,NPTFR
        IS = MESH%NBOR%I(K)
        IFRLIQ = NUMLIQ%I(K)
        IF(IFRLIQ.GT.0) THEN
          VNX=MESH%XNEBOR%R(K+NPTFR)
          VNY=MESH%YNEBOR%R(K+NPTFR)
          Q2(IFRLIQ) = Q2(IFRLIQ)
     &                 -UA(1,IS)*(UBOR%R(K)*VNX+VBOR%R(K)*VNY)
        ENDIF
      ENDDO
      DO IFRLIQ = 1,NFRLIQ
        IF(NCSIZE.GT.1) Q2(IFRLIQ) = P_SUM(Q2(IFRLIQ))
      ENDDO
      DO K=1,NPTFR
        IFRLIQ = NUMLIQ%I(K)
        IF(IFRLIQ.GT.0) THEN
          IF(Q2(IFRLIQ).GT.0.D0.AND.LIUBOR%I(K).EQ.KENT.AND.
     &       (NDEBIT.GT.0.OR.T2D_FILES(T2DIMP)%NAME(1:1).NE.' ')) THEN
            UBOR%R(K) = UBOR%R(K)*Q(IFRLIQ)/Q2(IFRLIQ)
            VBOR%R(K) = VBOR%R(K)*Q(IFRLIQ)/Q2(IFRLIQ)
          ENDIF
        ENDIF
      ENDDO
!
!     ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
!     LOOP OVER BOUNDARY NODES
!     ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
      IF(NPTFR.GT.0)THEN ! USEFUL FOR PARALLEL
        DO K=1,NPTFR
          IS=MESH%NBOR%I(K)
!
!         INITIALIZATION
          FLUENT  = 0.D0
          FLUSORT = 0.D0
          FLX(K,1)= 0.D0
          FLX(K,2)= 0.D0
          FLX(K,3)= 0.D0
          FLXG(1) = 0.D0
          FLXG(2) = 0.D0
          FLXG(3) = 0.D0
          FLXG_DW(1) = 0.D0
          FLXG_DW(2) = 0.D0
          FLXG_DW(3) = 0.D0
!
!         INDICATOR FOR DRY CELLS
          IDRY=0
!
!         NORMALIZED NORMAL
          XNN=MESH%XNEBOR%R(K)
          YNN=MESH%YNEBOR%R(K)
!
!         NON NORMALIZED NORMAL
          VNX=MESH%XNEBOR%R(K+NPTFR)
          VNY=MESH%YNEBOR%R(K+NPTFR)
          VNL=SQRT(VNX**2+VNY**2)
!
!         NORMALS TO SOLID HALF EDGE
!         (LIQUID NEXT TO SOLID BND)
          IF(YESNOFR(K)) THEN
            VNL1=SQRT(VNX1(K)**2+VNY1(K)**2)
            XNN1=VNX1(K)/VNL1
            YNN1=VNY1(K)/VNL1
          ELSE
            VNL1=0.D0
            XNN1=0.D0
            YNN1=0.D0
          ENDIF
!
          H   = UA(1,IS)
          IF(H.GT.EPS_FV) THEN
            U   = UA(2,IS)/H
            V   = UA(3,IS)/H
            RH  = SQRT(H)
          ELSE
            U = 0.D0
            V = 0.D0
            RH = 0.D0
            IDRY=IDRY+1
          ENDIF
!
!         **************************************************
!         WALL BOUNDARY
!         SLIPPING CONDITION
!         **************************************************
          IF(LIMPRO(K,1).EQ.KNEU) THEN
            AUX=0.5D0*GRAV*H**2
            FLXG(1) = 0.D0
            FLXG(2) = AUX*VNX
            FLXG(3) = AUX*VNY
!
!         **************************************************
!         LIQUID BOUNDARIES
!         **************************************************
          ELSE
!
!           CALCULATION OF F+(H,U,V)
            HRH = RH * H
            IF(H.LT.EPS_FV) THEN
              U=0.D0
              V=0.D0
              UNN=0.D0
              VNN=0.D0
              FHPLUS = 0.D0
              FUPLUS = 0.D0
            ELSE
              UNN= +XNN*U+YNN*V
              VNN= -YNN*U+XNN*V
              A=MIN(RA3,MAX(-RA3,-UNN/RH))
              A2 =A * A
              A3 =A2 * A
              ALPHA0=ALP*(RA3-A)
              ALPHA1=ALP2*(RA32-A2)
              ALPHA2=ALP3*(RA33-A3)
              FHPLUS = H*UNN*ALPHA0 + HRH*ALPHA1
              FUPLUS = UNN*(FHPLUS+HRH*ALPHA1) + H*H*ALPHA2
            ENDIF
!
!           CALCULATION OF FICTIVE STATE (HG,UG,VG)
!
!           ===============================
!           IF H GIVEN
!           ===============================
            IF(LIMPRO(K,1).EQ.KDIR) THEN
!
              C   = SG*RH
              VP1 = UNN
              VP2 = VP1  + C
              VP3 = VP1  - C
!
              HG     =HBOR%R(K)
              RHG    =SQRT (HG)
              HRHG   =RHG*HG
!
!             SUBCRITICAL REGIME OR INFLOW
!             ----------------------------
              IF (VP2*VP3.LE.0.D0.OR. VP1.LE.0.D0) THEN
!
                IF(HG.EQ.0.D0) THEN
                  UG=0.D0
                  VG=0.D0
                  FHMOINS = 0.D0
                  FUMOINS = 0.D0
                  IDRY = IDRY + 1
                ELSE
                  IF (VP2*VP3.LE.0.D0) THEN
!
!                   SUBCRITICAL
!                   -----------
                    UG = UNN + 2.D0*SG*(RH-RHG)
                    VG = VNN
!
                  ELSE
!
!                   SUPERCRITICAL INFLOW
!                   --------------------
                    IF(LIMPRO(K,2).EQ.KDIR) THEN
!                     IMPOSED INFLOW
                      UIN = UBOR%R(K)
                      VIN = VBOR%R(K)
                      HUIN = H*UIN
                      HVIN = H*VIN
!
                      DEST = HUIN*XNN+HVIN*YNN
                      RVG  =-HUIN*YNN+HVIN*XNN
!
                      A1 = DEST-FHPLUS
                      CA1= SQ2*A1/(SG*HG*RHG)
                      CALL ZEROPHI(-1.D0,AM,NIT,CA1)
!
                      UG= AM*SG*RHG
                      VG=RVG/HG
!
                    ELSE
!                     ONE DATUM IS MISSING
!                     WE SUPPOSE "THE LAKE AT REST"
                      UG= 0.D0
                      VG= 0.D0
!
                    ENDIF
!
                  ENDIF
!
!                 CALCULATION OF F-(HG,UG,VG)
                  A=MIN(RA3,MAX(-RA3,-UG/RHG))
                  A2 =A * A
                  A3 =A2 * A
                  ALPHA0=ALP*(A+RA3)
                  ALPHA1=ALP2*(A2-RA32)
                  ALPHA2=ALP3*(A3+RA33)
!
                  FHMOINS = HG*UG*ALPHA0 + HRHG*ALPHA1
                  FUMOINS = UG*(FHMOINS + HRHG*ALPHA1)
     &                    + HG*HG*ALPHA2
!
                ENDIF
!
!               CALCUL DES FLUX ET ROTATION INVERSE
                FLXG(1) = (FHPLUS +FHMOINS)
                FLXG(2) = (FUPLUS +FUMOINS)
!
                IF (FLXG(1).GE.0.D0) THEN
                  FLXG(3) = VNN*FLXG(1)
                ELSE
                  FLXG(3) = VG*FLXG(1)
                ENDIF
!
                FLUTMP = FLXG(2)
                FLXG(2) = +XNN*FLUTMP-YNN*FLXG(3)
                FLXG(3) = +YNN*FLUTMP+XNN*FLXG(3)
!
!             SUPERCRITICAL OUTFLOW
!             ---------------------
!             THE OUTFLOW IS TORRENTIAL SO WE HAVE NO NEED FOR THE GIVEN H
              ELSE
                RUN     = H*UNN
                FLXG(1) =  RUN
                FLXG(2) =  (U *RUN + 0.5D0*GRAV*H**2* VNX)
                FLXG(3) =  (V *RUN + 0.5D0*GRAV*H**2* VNY)
              ENDIF
!
!           ==================================
!           IF GIVEN VELOCITY OR DISCHARGE
!           ==================================
            ELSE IF(LIMPRO(K,2).EQ.KDIR) THEN
!
              UIN = UBOR%R(K)
              VIN = VBOR%R(K)
              HUIN = H*UIN
              HVIN = H*VIN
!
              DEST=HUIN*XNN+HVIN*YNN
              RVG =-HUIN*YNN+HVIN*XNN
!             WARNING: SIGN CHANGE / INRIA REPORT
              A1 = -DEST+FHPLUS
              A2 = -UNN - 2.D0*SG*RH
!
              IF (A1.LE.0.D0) THEN
!
!               FH- =-A1 CANNOT BE SATISFIED
!
                FHMOINS = 0.D0
                FUMOINS = 0.D0
                VG=0.D0
              ELSE
                CA1= 1.D0/(GRAV*SQ2*A1)**(1.D0/3.D0)
                CALL ZEROPSI(-0.5D0,AM,NIT,CA1,A2)
!
                RHG =A2/(SG*(AM-2.D0))
                HG= RHG * RHG
                HRHG= RHG * HG
!
                IF (HG.EQ.0.D0) THEN
                  UG=0.D0
                  VG=0.D0
                  FHMOINS = 0.D0
                  FUMOINS = 0.D0
                  IDRY = IDRY + 1
                ELSE
                  UG=-AM*A2/(AM-2.D0)
                  VG=RVG/HG
!
!                 CALCULATION OF F-(HG,UG,VG)
                  A=MIN(RA3,MAX(-RA3,-UG/RHG))
                  A2 =A * A
                  A3 =A2 * A
                  ALPHA0=ALP*(A+RA3)
                  ALPHA1=ALP2*(A2-RA32)
                  ALPHA2=ALP3*(A3+RA33)
!
                  FHMOINS = HG*UG*ALPHA0 + HRHG*ALPHA1
                  FUMOINS = UG*(FHMOINS + HRHG*ALPHA1)
     &                    + HG*HG*ALPHA2
!
                ENDIF
              ENDIF
!
!             CALCUL DES FLUX ET ROTATION INVERSE
              FLXG(1) = (FHPLUS +FHMOINS)
              FLXG(2) = (FUPLUS +FUMOINS)
!
              IF (FLXG(1).GE.0.D0) THEN
                FLXG(3) = VNN*FLXG(1)
              ELSE
                FLXG(3) = VG*FLXG(1)
              ENDIF
!
              FLUTMP=FLXG(2)
              FLXG(2) = +XNN*FLUTMP-YNN*FLXG(3)
              FLXG(3) = +YNN*FLUTMP+XNN*FLXG(3)
!
!           ===============================
!           CRITICAL OUTFLOW
!           ===============================
            ELSE
              RUN     = H*UNN
              FLXG(1) =  RUN
              FLXG(2) =  (U *RUN + 0.5D0*GRAV*H**2* VNX)
              FLXG(3) =  (V *RUN + 0.5D0*GRAV*H**2* VNY)
!
            ENDIF
!
!           ============================================
!           WALL BOUNDARY CONDITION ON HALF EDGES
!           (LIQUID NODES CONNECTED TO SOLID BOUNDARIES)
!           ============================================
            IF(YESNOFR(K)) THEN
              HG_DW = H
              U1_DW = U
              V1_DW = V

!             ROTATION
              U10 = U1_DW
              U1_DW  = XNN1*U10+YNN1*V1_DW
              V1_DW  =-YNN1*U10+XNN1*V1_DW

!             PUT NORMAL COMPONENT = 0
              U1_DW = 0.D0
              UG_DW = U1_DW
              VG_DW = V1_DW

!             INVERSE ROTATION
              U10 = U1_DW
              U1_DW = -YNN1*V1_DW
              V1_DW =  XNN1*V1_DW
              UG_DW = -YNN1*VG_DW
              VG_DW =  XNN1*VG_DW

              CALL FLUX_KIN(H,HG_DW,U1_DW,UG_DW,
     &                      V1_DW,VG_DW,XNN1,YNN1,FLXG_DW)

            ENDIF
!
          ENDIF
!
!         **************************************************
!         COMPUTE THE FLUX
!         **************************************************   
          FLX(K,1) = VNL*FLXG(1) + VNL1*FLXG_DW(1)
          FLX(K,2) = VNL*FLXG(2) + VNL1*FLXG_DW(2)
          FLX(K,3) = VNL*FLXG(3) + VNL1*FLXG_DW(3)
!
        ENDDO
      ENDIF
!
!     ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
!     FINAL BALANCE
!     ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
!     FOR PARALLELISM
      IF(NCSIZE.GT.1)THEN
        CALL PARCOM_BORD(FLX(:,1),1,MESH)
        CALL PARCOM_BORD(FLX(:,2),1,MESH)
        CALL PARCOM_BORD(FLX(:,3),1,MESH)
      ENDIF
!
      IF(NPTFR.GT.0)THEN
        DO K=1,NPTFR
          IS=MESH%NBOR%I(K)
!
          IF(NCSIZE.GT.1)THEN
            OUTFLOW  = FLX(K,1)*MESH%IFAC%I(IS)
          ELSE
            OUTFLOW  = FLX(K,1)
          ENDIF
          IF(FLX(K,1).LE.0.D0)THEN ! INLET
            FLUENT = FLUENT + OUTFLOW
          ELSE                     ! OUTLET
            FLUSORT = FLUSORT + OUTFLOW
          ENDIF
          FLBOR%R(K) = OUTFLOW
!
          CE(IS,1)  = CE(IS,1) - FLX(K,1)
          CE(IS,2)  = CE(IS,2) - FLX(K,2)
          CE(IS,3)  = CE(IS,3) - FLX(K,3)
!
        ENDDO
      ENDIF
!
!-----------------------------------------------------------------------
!
      RETURN
      END
