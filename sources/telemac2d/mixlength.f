!                   ********************
                    SUBROUTINE MIXLENGTH
!                   ********************
!
     &(VISC,MESH,T1,T2,T3,T4)
!
!***********************************************************************
! TELEMAC2D
!***********************************************************************
!
!brief    COMPUTES THE EDDY VISCOSITY USING THE MIXING LENGTH
!         FOR THE HORIZONTAL + PARABOLIC MODEL FOR THE VERTICAL
!
!history  C. DORFMANN (TU GRAZ)
!+        15/03/2016
!+        V7P2
!+   First version, with negative depths secured and some optimisation.
!
!history  C. DORFMANN (FLOW ENGINEERING)
!+        22/10/2021
!+        V9P0
!+   New treatment for boundary nodes
!
!~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
!| MESH           |-->| MESH STRUCTURE
!| T1             |<->| WORK BIEF_OBJ STRUCTURE
!| T2             |<->| WORK BIEF_OBJ STRUCTURE
!| T3             |<->| WORK BIEF_OBJ STRUCTURE
!| T4             |<->| WORK BIEF_OBJ STRUCTURE
!| VISC           |-->| TURBULENT DIFFUSION
!~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
!
      USE BIEF
      USE DECLARATIONS_SPECIAL
      USE DECLARATIONS_TELEMAC, ONLY : KADH,KLOG
      USE DECLARATIONS_TELEMAC2D, ONLY : CF,U,V,H,MSK,MASKEL,PROPNU,
     &  UNSV2D,IELMU,NPTFR,KARMAN,NPOIN,CALMIXLENGTH,LIUBOR,KFROTL,WDIST
      USE INTERFACE_TELEMAC2D, EX_MIXLENGTH => MIXLENGTH
!
      IMPLICIT NONE
!
!+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
!
      TYPE(BIEF_MESH),  INTENT(INOUT) :: MESH
      TYPE(BIEF_OBJ),   INTENT(INOUT) :: VISC,T1,T2,T3,T4
!
!+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
!
      INTEGER I,K,N
      DOUBLE PRECISION LM,LMB,USTAR,VISCVERT,VISCHOR2,HC,BDIST,LIM1,LIM2
!
!-----------------------------------------------------------------------
!
      INTRINSIC SQRT
!
!-----------------------------------------------------------------------
!
!     COEFFICIENTS:
!
!     Horizontal mixing length model:
!     CL: theoretically from integration of lm along depth
!     can be used as calibration coefficient
!     Old value, correponding to CALMIXLENGTH(1)/KARMAN now
!
!     CALMIXLENGTH(1) = CL*KARMAN = 0.2666667D0*KARMAN
!
!     Vertical parabolic model:
!     ALPHA: theoretically from integration of parabolic model along
!     depth can be used as calibration coefficient
!
!     CALMIXLENGTH(2) = ALPHA = KARMAN/6.D0
!
!-----------------------------------------------------------------------
!
      LIM1 = 0.5D0-SQRT(3.D0)/6.D0
      LIM2 = 0.32444233594882926D0
!
      CALL VECTOR(T1,'=','GRADF          X',IELMU,
     &            1.D0,U,U,U,U,U,U,MESH,MSK,MASKEL)
      CALL VECTOR(T2,'=','GRADF          Y',IELMU,
     &            1.D0,U,U,U,U,U,U,MESH,MSK,MASKEL)
      CALL VECTOR(T3,'=','GRADF          X',IELMU,
     &            1.D0,V,V,V,V,V,V,MESH,MSK,MASKEL)
      CALL VECTOR(T4,'=','GRADF          Y',IELMU,
     &            1.D0,V,V,V,V,V,V,MESH,MSK,MASKEL)
!
      IF(NCSIZE.GT.1) THEN
        CALL PARCOM (T1, 2, MESH)
        CALL PARCOM (T2, 2, MESH)
        CALL PARCOM (T3, 2, MESH)
        CALL PARCOM (T4, 2, MESH)
      ENDIF
!
      DO I=1,NPOIN
        USTAR = SQRT(0.5D0*CF%R(I)*(U%R(I)**2+V%R(I)**2))
        HC = MAX(H%R(I),1.D-6)
!
        IF(KFROTL.NE.0.AND.WDIST%R(I)/HC.LT.LIM1) THEN
          VISCVERT = KARMAN*USTAR*WDIST%R(I)*(1.D0-WDIST%R(I)/HC)
        ELSE
          VISCVERT = CALMIXLENGTH(2)*HC*USTAR
        ENDIF
!
        IF(KFROTL.NE.0.AND.WDIST%R(I)/HC.LT.LIM2) THEN
          LM = KARMAN*WDIST%R(I)*SQRT(1.D0-WDIST%R(I)/HC)
        ELSE
          LM = CALMIXLENGTH(1)*HC
        ENDIF
!
        VISCHOR2 = LM**4 * (2.D0*T1%R(I)**2+2.D0*T4%R(I)**2
     &            +(T2%R(I)+T3%R(I))**2) * UNSV2D%R(I)**2
        VISC%R(I) = PROPNU + SQRT(VISCVERT**2+VISCHOR2)
      ENDDO
!
!     LOOP ON THE BOUNDARY NODES
!     REDUCING EVENTUALLY THE MIXING LENGTH LMB AND VISCVERT
!     AT THE NODES NEAR THE WALL
!
      IF(KFROTL.NE.0) THEN
        DO K=1,NPTFR
          N = MESH%NBOR%I(K)
          BDIST = MESH%DISBOR%R(K)*0.33D0
          USTAR = SQRT(0.5D0*CF%R(N)*(U%R(N)**2+V%R(N)**2))
          HC = MAX(H%R(N),1.D-6)
!
          IF(LIUBOR%I(K).EQ.KLOG.OR.LIUBOR%I(K).EQ.KADH) THEN
            IF(BDIST/HC.LT.LIM1) THEN
              VISCVERT = KARMAN*USTAR*BDIST*(1.D0-BDIST/HC)
            ELSE
              VISCVERT = CALMIXLENGTH(2)*HC*USTAR
            ENDIF
!
            IF(BDIST/HC.LT.LIM2) THEN
              LMB = KARMAN*BDIST*SQRT(1.D0-BDIST/HC)
            ELSE
              LMB = CALMIXLENGTH(1)*HC
            ENDIF
!
            VISCHOR2 = LMB**4 * (2.D0*T1%R(N)**2+2.D0*T4%R(N)**2
     &            +(T2%R(N)+T3%R(N))**2) * UNSV2D%R(N)**2
            VISC%R(N) = PROPNU + SQRT(VISCVERT**2+VISCHOR2)
          ENDIF
        ENDDO
      ENDIF
!
!      OLD AND SIMPLER BOUNDARY NODES TREATMENT
!
!      DO K=1,NPTFR
!        N = MESH%NBOR%I(K)
!        USTAR = SQRT(0.5D0*CF%R(N)*(U%R(N)**2+V%R(N)**2))
!        HC = MAX(0.D0,H%R(N))
!        VISCVERT = CALMIXLENGTH(2)*HC*USTAR
!        LMB = MIN(CALMIXLENGTH(1)*HC,KARMAN*MESH%DISBOR%R(K))
!        VISCHOR2 = LMB**4 * (2.D0*T1%R(N)**2+2.D0*T4%R(N)**2
!     &            +(T2%R(N)+T3%R(N))**2) * UNSV2D%R(N)**2
!        VISC%R(N) = PROPNU + SQRT(VISCVERT**2+VISCHOR2)
!      ENDDO
!
!-----------------------------------------------------------------------
!
      RETURN
      END
