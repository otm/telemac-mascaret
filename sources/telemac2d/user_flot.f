!                   ********************
                    SUBROUTINE USER_FLOT
!                   ********************
!
     &(XFLOT,YFLOT,NFLOT,NFLOT_MAX,X,Y,IKLE,NELEM,NELMAX,NPOIN,
     & TAGFLO,CLSFLO,SHPFLO,ELTFLO,MESH,LT,NIT,AT)
!
!***********************************************************************
! TELEMAC2D
!***********************************************************************
!
!brief    releasing and removing particles in the mesh.
!
!history  J-M JANIN (LNH)
!+        17/08/1994
!+        V5P2
!+
!
!~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
!| AT             |-->| TIME
!| CLSFLO         |<->| CLASS FOR EACH FLOAT
!| ELTFLO         |<->| NUMBERS OF ELEMENTS WHERE ARE THE FLOATS
!| IKLE           |-->| CONNECTIVITY TABLE
!| LT             |-->| CURRENT TIME STEP
!| MESH           |<->| MESH STRUCTURE
!| NELEM          |-->| NUMBER OF ELEMENT
!| NELMAX         |-->| MAXIMUM NUMBER OF ELEMENTS
!| NIT            |-->| NUMBER OF TIME STEPS
!| NFLOT          |-->| NUMBER OF FLOATS
!| NFLOT_MAX      |-->| MAXIMUM NUMBER OF FLOATS
!| NPOIN          |-->| NUMBER OF POINTS IN THE MESH
!| SHPFLO         |<->| BARYCENTRIC COORDINATES OF FLOATS IN THEIR
!|                |   | ELEMENTS.
!| TAGFLO         |<->| TAG FOR EACH FLOAT
!| X,Y            |-->| COORDINATES OF POINTS IN THE MESH
!| XFLOT,YFLOT    |<->| POSITIONS OF FLOATING BODIES
!~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
!
      USE BIEF
      USE STREAMLINE, ONLY : ADD_PARTICLE,DEL_PARTICLE
      USE ALGAE_TRANSP
      USE DECLARATIONS_TELEMAC2D, ONLY : PROTYP,PHI0,LAMBD0
!
      USE DECLARATIONS_SPECIAL
      IMPLICIT NONE
!
!+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
!
      INTEGER, INTENT(IN)             :: NPOIN,NIT,NFLOT_MAX,LT
      INTEGER, INTENT(IN)             :: NELEM,NELMAX
      INTEGER, INTENT(IN)             :: IKLE(NELMAX,3)
      INTEGER, INTENT(INOUT)          :: NFLOT
      INTEGER, INTENT(INOUT)          :: TAGFLO(NFLOT_MAX)
      INTEGER, INTENT(INOUT)          :: CLSFLO(NFLOT_MAX)
      INTEGER, INTENT(INOUT)          :: ELTFLO(NFLOT_MAX)
      DOUBLE PRECISION, INTENT(IN)    :: X(NPOIN),Y(NPOIN),AT
      DOUBLE PRECISION, INTENT(INOUT) :: XFLOT(NFLOT_MAX)
      DOUBLE PRECISION, INTENT(INOUT) :: YFLOT(NFLOT_MAX)
      DOUBLE PRECISION, INTENT(INOUT) :: SHPFLO(3,NFLOT_MAX)
      TYPE(BIEF_MESH) , INTENT(INOUT) :: MESH
!
!+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
!
!     DECLARATIONS FOR AN EXAMPLE TO GIVE COORDINATES OF DROGUES IN
!     LONGITUDE/LATITUDE, NOT MERCATOR PROJECTION
!
!     DOUBLE PRECISION, PARAMETER :: R=6.37D6
!     DOUBLE PRECISION PS4,TAN1,TAN2,LONGIRAD,LONG,LAT,XXX,YYY
!
!     INTRINSIC LOG,TAN,ATAN
!
!-----------------------------------------------------------------------
!
!     EXAMPLE : AT ITERATION 1 AND EVERY 10 ITERATIONS AFTER 600
!               A PARTICLE IS RELEASED WITH COORDINATES
!               X=-220.
!               Y=400.D0+LT/3.D0
!               AND TAG NUMBER LT (LT IS THE TIME STEP NUMBER)
!
!     IF(LT.LE.600.AND.(10*(LT/10).EQ.LT.OR.LT.EQ.1)) THEN
!       CALL ADD_PARTICLE(-220.D0,400.D0+LT/3.D0,0.D0,LT,1,NFLOT,
!    &                    NFLOT_MAX,XFLOT,YFLOT,YFLOT,TAGFLO,CLSFLO,
!    &                    SHPFLO,SHPFLO,ELTFLO,ELTFLO,MESH,1,
!    &                    0.D0,0.D0,0.D0,0.D0,0,0)
!     ENDIF
!
!     EXAMPLE : PARTICLE WITH TAG 20 REMOVED AT ITERATION 600
!
!     IF(LT.EQ.600) THEN
!        CALL DEL_PARTICLE(20,NFLOT,NFLOT_MAX,
!    &                     XFLOT,YFLOT,YFLOT,TAGFLO,CLSFLO,SHPFLO,SHPFLO,
!    &                     ELTFLO,ELTFLO,MESH%TYPELM)
!     ENDIF
!
!-----------------------------------------------------------------------
!
!     EXAMPLE : FOR ALGAE PARTICLE TRANSPORT
!       ( RELEASE START FOR ALGAE IS NOW DEFINED IN THE CAS FILE )
!
!       DO I=1,NFLOT_MAX
!         CALL ADD_PARTICLE(0.175D0,0.45D0,0.D0,I,1,NFLOT,
!      &                  NFLOT_MAX,XFLOT,YFLOT,YFLOT,TAGFLO,CLSFLO,
!      &                  SHPFLO,SHPFLO,ELTFLO,ELTFLO,MESH,1,
!      &                  0.D0,0.D0,0.D0,0.D0,0,0)
!       END DO
!
!     EXAMPLE : AT ITERATION 1 AND EVERY 10 ITERATIONS AFTER 600
!               A PARTICLE IS RELEASED WITH COORDINATES
!               X=-220.
!               Y=400.D0+LT/3.D0
!               AND TAG NUMBER LT (LT IS THE TIME STEP NUMBER)
!
!     IF(LT.LE.600.AND.(10*(LT/10).EQ.LT.OR.LT.EQ.1)) THEN
!       CALL ADD_PARTICLE(-220.D0,400.D0+LT/3.D0,0.D0,1,LT,NFLOT,
!    &                    NFLOT_MAX,XFLOT,YFLOT,YFLOT,TAGFLO,CLSFLO,
!    &                    SHPFLO,SHPFLO,ELTFLO,ELTFLO,MESH,1,
!    &                    0.D0,0.D0,0.D0,0.D0,0,0)
!     ENDIF
!
!     EXAMPLE : PARTICLE WITH TAG 20 REMOVED AT ITERATION 600
!
!     IF(LT.EQ.600) THEN
!        CALL DEL_PARTICLE(20,NFLOT,NFLOT_MAX,
!    &                     XFLOT,YFLOT,YFLOT,TAGFLO,CLSFLO,SHPFLO,SHPFLO,
!    &                     ELTFLO,ELTFLO,MESH%TYPELM)
!     ENDIF
!
!-----------------------------------------------------------------------
!
!     EXAMPLE : FOR ALGAE PARTICLE TRANSPORT
!       ( RELEASE START FOR ALGAE IS NOW DEFINED IN THE CAS FILE )
!
!       DO I=1,NFLOT_MAX
!         CALL ADD_PARTICLE(0.175D0,0.45D0,0.D0,I,1,NFLOT,
!      &                  NFLOT_MAX,XFLOT,YFLOT,YFLOT,TAGFLO,CLSFLO,
!      &                  SHPFLO,SHPFLO,ELTFLO,ELTFLO,MESH,1,
!      &                  0.D0,0.D0,0.D0,0.D0,0,0)
!       END DO
!
!-----------------------------------------------------------------------
!
!     EXAMPLE : AT ITERATION 1 AND EVERY 10 ITERATIONS BEFORE 600 S
!               A PARTICLE IS RELEASED WITH COORDINATES
!               X = -7.5D0
!               Y = 47.5D0+LT/3000.D0
!               COORDINATES GIVEN IN LONGITUDE/LATITUDE
!               AND TAG NUMBER LT (LT IS THE TIME STEP NUMBER)
!               CONVERSION IN MERCATOR PROJECTION IS TO BE KEPT
!               AS TELEMAC-2D WORKS IN THAT PROJECTION AT THAT TIME
!
!     PS4 = ATAN(1.D0)
!     LONGIRAD = PHI0*PS4/45.D0
!     TAN2 = TAN(LAMBD0*PS4/90.D0+PS4)
!
!     IF(LT.LE.600.AND.(10*(LT/10).EQ.LT.OR.LT.EQ.1)) THEN
!!      COORDINATES OF DROGUES TO BE RELEASED IN LONGITUDE/LATITUDE
!       LONG = -7.5D0
!       LAT  = 47.5D0+LT/3000.D0
!!      CONVERSION FROM LONGITUDE/LATITUDE TO MERCATOR PROJECTION
!!      DO NOT CHANGE THE 3 FOLLOWING LINES
!       XXX  = R*(LONG*PS4/45.D0-LONGIRAD)
!       TAN1 = TAN(LAT*PS4/90.D0+PS4)
!       YYY  = R*(LOG(TAN1)-LOG(TAN2))
!
!       CALL ADD_PARTICLE(XXX,YYY,0.D0,LT,1,NFLOT,
!    &                    NFLOT_MAX,XFLOT,YFLOT,YFLOT,TAGFLO,CLSFLO,
!    &                    SHPFLO,SHPFLO,ELTFLO,ELTFLO,MESH,1,
!    &                    0.D0,0.D0,0.D0,0.D0,0,0)
!     ENDIF
!
!-----------------------------------------------------------------------
!
      RETURN
      END
