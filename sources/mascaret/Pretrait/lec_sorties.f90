!== Copyright (C) 2000-2022 EDF-CEREMA ==
!
!   This file is part of MASCARET.
!
!   MASCARET is free software: you can redistribute it and/or modify
!   it under the terms of the GNU General Public License as published by
!   the Free Software Foundation, either version 3 of the License, or
!   (at your option) any later version.
!
!   MASCARET is distributed in the hope that it will be useful,
!   but WITHOUT ANY WARRANTY; without even the implied warranty of
!   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
!   GNU General Public License for more details.
!
!   You should have received a copy of the GNU General Public License
!   along with MASCARET.  If not, see <http://www.gnu.org/licenses/>
!

subroutine LEC_SORTIES( &
     VarSto              , &
     VarCalc             , &
     VarPre              , &
     unitNum             , & ! Unite logique .xcasL
     Erreur                & ! Erreur
                      )

! *********************************************************************
! PROGICIEL : MASCARET       S. MANDELKERN
!                            F. ZAOUI
!
! VERSION : V9P1R0              EDF-CEREMA
! *********************************************************************

   !========================= Declarations ===========================
   use M_PRECISION
   use M_ERREUR_T            ! Type ERREUR_T
   use M_MESSAGE_C           ! Messages d'erreur
   use M_CONSTANTES_CALCUL_C ! Constantes num, phys et info
   use M_INDEX_VARIABLE_C    ! Numeros des variables a sortir
   use M_TRAITER_ERREUR_I    ! Traitement de l'errreur
   use M_XCAS_S

   implicit none

   ! Arguments
   logical           , dimension(:)   , intent(  out) :: VarCalc
   logical           , dimension(:)   , intent(  out) :: VarSto
   integer           , dimension(:)   , intent(  out) :: VarPre
   integer, intent(in)                                :: unitNum
   type(ERREUR_T)                     , intent(inout) :: Erreur
   ! Variables locales
   integer :: retour       ! code de retour des fonctions intrinseques
   logical :: reponse_logique
   integer :: nb_deversoir
   logical, allocatable :: ltab1(:),ltab2(:)
   integer, allocatable :: ltab3(:)
   integer, dimension(42) :: VarPreDefault
   character(len=256)  :: pathNode
   character(len=8192) :: line
   !character(132) :: !arbredappel_old

   !========================= Instructions ===========================
   ! INITIALISATION
   ! --------------
   Erreur%Numero = 0
   !arbredappel_old = trim(!Erreur%arbredappel)
   !Erreur%arbredappel = trim(!Erreur%arbredappel)//'=>LEC_SORTIES'
   VarSto(:) = .false.
   VarCalc(:) = .false.
   VarPre(:) = 8
   VarPreDefault = (/ &
    4, 4, 4, 0, 0, 3, 3, 3, 2, 2, &
    5, 2, 2, 2, 2, 2, 2, 2, 2, 4, &
    4, 6, 3, 3, 2, 2, 2, 0, 0, 2, &
    2, 1, 4, 2, 1, 4, 4, 2, 1, 2, &
    1, 3 &
/)
   
    allocate( ltab1(42) , STAT = retour )
    if( retour /= 0 ) then
        Erreur%Numero = 5
        Erreur%ft     = err_5
        Erreur%ft_c   = err_5c
        call TRAITER_ERREUR( Erreur , 'ltab1' )
        return
    end if
    ltab1(:) = .false.
    allocate( ltab2(15) , STAT = retour )
    if( retour /= 0 ) then
        Erreur%Numero = 5
        Erreur%ft     = err_5
        Erreur%ft_c   = err_5c
        call TRAITER_ERREUR( Erreur , 'ltab2' )
        return
    end if
    ltab2(:) = .false.
    allocate( ltab3(42) , STAT = retour )
    if( retour /= 0 ) then
        Erreur%Numero = 5
        Erreur%ft     = err_5
        Erreur%ft_c   = err_5c
        call TRAITER_ERREUR( Erreur , 'ltab3' )
        return
    end if
    ltab3(:) = VarPreDefault(:)

   pathNode = 'parametresVariablesStockees/variablesStockees'
   line = xcasReader(unitNum, pathNode)
   if(len(trim(line)).ne.0) read(unit=line, fmt=*) ltab1

   pathNode = 'parametresVariablesStockees/variablesCalculees'
   line = xcasReader(unitNum, pathNode)
   if(len(trim(line)).ne.0) read(unit=line, fmt=*) ltab2
   
    pathNode = 'parametresVariablesStockees/variablesPrecision'
   line = xcasReader(unitNum, pathNode)
   if(len(trim(line)).ne.0) read(unit=line, fmt=*) ltab3

   ! Variables stockees
   VarSto(VAR_X)    = .true.

   VarSto(VAR_ZREF) = ltab1(1)
   VarSto(VAR_RGC)  = ltab1(2)
   VarSto(VAR_RDC)  = ltab1(3)
   VarSto(VAR_CF1)  = ltab1(4)
   VarSto(VAR_CF2)  = ltab1(5)
   VarSto(VAR_Z)    = ltab1(6)
   VarSto(VAR_Q1)   = ltab1(7)
   VarSto(VAR_Q2)   = ltab1(8)
   VarSto(VAR_S1)   = ltab1(9)
   VarSto(VAR_S2)   = ltab1(10)
   VarSto(VAR_FR)   = ltab1(11)
   VarSto(VAR_BETA) = ltab1(12)
   VarSto(VAR_B1)   = ltab1(13)
   VarSto(VAR_B2)   = ltab1(14)
   VarSto(VAR_BS)   = ltab1(15)
   VarSto(VAR_P1)   = ltab1(16)
   VarSto(VAR_P2)   = ltab1(17)
   VarSto(VAR_RH1)  = ltab1(18)
   VarSto(VAR_RH2)  = ltab1(19)
   VarSto(VAR_V1)   = ltab1(20)
   VarSto(VAR_V2)   = ltab1(21)
   VarSto(VAR_TAUF) = ltab1(22)
   VarSto(VAR_Y)    = ltab1(23)
   VarSto(VAR_HMOY) = ltab1(24)
   VarSto(VAR_Q2G)  = ltab1(25)
   VarSto(VAR_Q2D)  = ltab1(26)
   VarSto(VAR_SS)   = ltab1(27)
   VarSto(VAR_VOL)  = ltab1(28)
   VarSto(VAR_VOLS) = ltab1(29)
   VarSto(VAR_CHARG)= ltab1(30)
   VarSto(VAR_ZMAX) = ltab1(31)
   VarSto(VAR_TZMAX)= ltab1(32)
   VarSto(VAR_VZMAX)= ltab1(33)
   VarSto(VAR_ZMIN) = ltab1(34)
   VarSto(VAR_TZMIN)= ltab1(35)
   VarSto(VAR_V1MIN)= ltab1(36)
   VarSto(VAR_V1MAX)= ltab1(37)
   VarSto(VAR_BMAX) = ltab1(38)
   VarSto(VAR_TOND) = ltab1(39)
   VarSto(VAR_QMAX) = ltab1(40)
   VarSto(VAR_TQMAX)= ltab1(41)
   VarSto(VAR_EMAX) = ltab1(42)


   VarSto(VAR_QDEV) = .true.
   VarSto(VAR_Q)    = .true.
   VarSto(VAR_Debi) = .false.
   VarSto(VAR_YVRAI) = .false.
   VarSto(VAR_QVRAI) = .false.

   pathNode = 'parametresGeneraux/validationCode'
   line = xcasReader(unitNum, pathNode)
   read(unit=line, fmt=*) reponse_logique

   if(reponse_logique) then
       VarSto(VAR_YVRAI) = .true.
       VarSto(VAR_QVRAI) = .true.
   endif

   pathNode = 'parametresGeneraux/calcOndeSubmersion'
   line = xcasReader(unitNum, pathNode)
   read(unit=line, fmt=*) reponse_logique

   if(reponse_logique) then
      VarSto(VAR_ZMAX)  = .true.
      VarSto(VAR_TZMAX) = .true.
      VarSto(VAR_VZMAX) = .true.
      VarSto(VAR_TOND)  = .true.
      VarSto(VAR_QMAX)  = .true.
      VarSto(VAR_V1MAX) = .true.
   Endif
   If( VarSto(VAR_VZMAX) ) VarSto(Var_V1) = .true.

   pathNode = 'parametresApportDeversoirs/deversLate'
   line = xcasReader(unitNum, pathNode)

   if(len(trim(line)).eq.0) then
       nb_deversoir = 0
   else
       pathNode = 'parametresApportDeversoirs/deversLate/nbDeversoirs'
       line = xcasReader(unitNum, pathNode)
       read(unit=line, fmt=*) nb_deversoir
   endif
   If( Nb_deversoir == 0 ) then
      VarSto(VAR_QDEV) = .false.
   endif

   ! Precision Variables stockees
   VarPre(VAR_X)    = 4

   VarPre(VAR_ZREF)  = merge(ltab3(1),  VarPreDefault(1), ltab3(1)  >= 0 .and. ltab3(1)  <= 9)
   VarPre(VAR_RGC)   = merge(ltab3(2),  VarPreDefault(2), ltab3(2)  >= 0 .and. ltab3(2)  <= 9)
   VarPre(VAR_RDC)   = merge(ltab3(3),  VarPreDefault(3), ltab3(3)  >= 0 .and. ltab3(3)  <= 9)
   VarPre(VAR_CF1)   = merge(ltab3(4),  VarPreDefault(4), ltab3(4)  >= 0 .and. ltab3(4)  <= 9)
   VarPre(VAR_CF2)   = merge(ltab3(5),  VarPreDefault(5), ltab3(5)  >= 0 .and. ltab3(5)  <= 9)
   VarPre(VAR_Z)     = merge(ltab3(6),  VarPreDefault(6), ltab3(6)  >= 0 .and. ltab3(6)  <= 9)
   VarPre(VAR_Q1)    = merge(ltab3(7),  VarPreDefault(7), ltab3(7)  >= 0 .and. ltab3(7)  <= 9)
   VarPre(VAR_Q2)    = merge(ltab3(8),  VarPreDefault(8), ltab3(8)  >= 0 .and. ltab3(8)  <= 9)
   VarPre(VAR_S1)    = merge(ltab3(9),  VarPreDefault(9), ltab3(9)  >= 0 .and. ltab3(9)  <= 9)
   VarPre(VAR_S2)    = merge(ltab3(10), VarPreDefault(10), ltab3(10) >= 0 .and. ltab3(10) <= 9)
   VarPre(VAR_FR)    = merge(ltab3(11), VarPreDefault(11), ltab3(11) >= 0 .and. ltab3(11) <= 9)
   VarPre(VAR_BETA)  = merge(ltab3(12), VarPreDefault(12), ltab3(12) >= 0 .and. ltab3(12) <= 9)
   VarPre(VAR_B1)    = merge(ltab3(13), VarPreDefault(13), ltab3(13) >= 0 .and. ltab3(13) <= 9)
   VarPre(VAR_B2)    = merge(ltab3(14), VarPreDefault(14), ltab3(14) >= 0 .and. ltab3(14) <= 9)
   VarPre(VAR_BS)    = merge(ltab3(15), VarPreDefault(15), ltab3(15) >= 0 .and. ltab3(15) <= 9)
   VarPre(VAR_P1)    = merge(ltab3(16), VarPreDefault(16), ltab3(16) >= 0 .and. ltab3(16) <= 9)
   VarPre(VAR_P2)    = merge(ltab3(17), VarPreDefault(17), ltab3(17) >= 0 .and. ltab3(17) <= 9)
   VarPre(VAR_RH1)   = merge(ltab3(18), VarPreDefault(18), ltab3(18) >= 0 .and. ltab3(18) <= 9)
   VarPre(VAR_RH2)   = merge(ltab3(19), VarPreDefault(19), ltab3(19) >= 0 .and. ltab3(19) <= 9)
   VarPre(VAR_V1)    = merge(ltab3(20), VarPreDefault(20), ltab3(20) >= 0 .and. ltab3(20) <= 9)
   VarPre(VAR_V2)    = merge(ltab3(21), VarPreDefault(21), ltab3(21) >= 0 .and. ltab3(21) <= 9)
   VarPre(VAR_TAUF)  = merge(ltab3(22), VarPreDefault(22), ltab3(22) >= 0 .and. ltab3(22) <= 9)
   VarPre(VAR_Y)     = merge(ltab3(23), VarPreDefault(23), ltab3(23) >= 0 .and. ltab3(23) <= 9)
   VarPre(VAR_HMOY)  = merge(ltab3(24), VarPreDefault(24), ltab3(24) >= 0 .and. ltab3(24) <= 9)
   VarPre(VAR_Q2G)   = merge(ltab3(25), VarPreDefault(25), ltab3(25) >= 0 .and. ltab3(25) <= 9)
   VarPre(VAR_Q2D)   = merge(ltab3(26), VarPreDefault(26), ltab3(26) >= 0 .and. ltab3(26) <= 9)
   VarPre(VAR_SS)    = merge(ltab3(27), VarPreDefault(27), ltab3(27) >= 0 .and. ltab3(27) <= 9)
   VarPre(VAR_VOL)   = merge(ltab3(28), VarPreDefault(28), ltab3(28) >= 0 .and. ltab3(28) <= 9)
   VarPre(VAR_VOLS)  = merge(ltab3(29), VarPreDefault(29), ltab3(29) >= 0 .and. ltab3(29) <= 9)
   VarPre(VAR_CHARG) = merge(ltab3(30), VarPreDefault(30), ltab3(30) >= 0 .and. ltab3(30) <= 9)
   VarPre(VAR_ZMAX)  = merge(ltab3(31), VarPreDefault(31), ltab3(31) >= 0 .and. ltab3(31) <= 9)
   VarPre(VAR_TZMAX) = merge(ltab3(32), VarPreDefault(32), ltab3(32) >= 0 .and. ltab3(32) <= 9)
   VarPre(VAR_VZMAX) = merge(ltab3(33), VarPreDefault(33), ltab3(33) >= 0 .and. ltab3(33) <= 9)
   VarPre(VAR_ZMIN)  = merge(ltab3(34), VarPreDefault(34), ltab3(34) >= 0 .and. ltab3(34) <= 9)
   VarPre(VAR_TZMIN) = merge(ltab3(35), VarPreDefault(35), ltab3(35) >= 0 .and. ltab3(35) <= 9)
   VarPre(VAR_V1MIN) = merge(ltab3(36), VarPreDefault(36), ltab3(36) >= 0 .and. ltab3(36) <= 9)
   VarPre(VAR_V1MAX) = merge(ltab3(37), VarPreDefault(37), ltab3(37) >= 0 .and. ltab3(37) <= 9)
   VarPre(VAR_BMAX)  = merge(ltab3(38), VarPreDefault(38), ltab3(38) >= 0 .and. ltab3(38) <= 9)
   VarPre(VAR_TOND)  = merge(ltab3(39), VarPreDefault(39), ltab3(39) >= 0 .and. ltab3(39) <= 9)
   VarPre(VAR_QMAX)  = merge(ltab3(40), VarPreDefault(40), ltab3(40) >= 0 .and. ltab3(40) <= 9)
   VarPre(VAR_TQMAX) = merge(ltab3(41), VarPreDefault(41), ltab3(41) >= 0 .and. ltab3(41) <= 9)
   VarPre(VAR_EMAX)  = merge(ltab3(42), VarPreDefault(42), ltab3(42) >= 0 .and. ltab3(42) <= 9)


   VarPre(VAR_QDEV) = VarPre(VAR_Q1)
   VarPre(VAR_Q)    = VarPre(VAR_Q1)
   VarPre(VAR_Debi) = VarPre(VAR_Q1)
   VarPre(VAR_YVRAI) = VarPre(VAR_Y)
   VarPre(VAR_QVRAI) = VarPre(VAR_Q1)
   
   
   ! Variables calculees
   VarCalc(VAR_X)    = .true.
   VarCalc(VAR_ZREF) = .true.
   VarCalc(VAR_RGC)  = .true.
   VarCalc(VAR_RDC)  = .true.
   VarCalc(VAR_CF1)  = .true.
   VarCalc(VAR_CF2)  = .true.
   VarCalc(VAR_Z)    = .true.
   VarCalc(VAR_Q1)   = .true.
   VarCalc(VAR_Q2)   = .true.
   VarCalc(VAR_S1)   = .true.
   VarCalc(VAR_S2)   = .true.
   VarCalc(VAR_FR)   = .true.
   VarCalc(VAR_BETA) = .false.

   reponse_logique = ltab2(1)
   if( reponse_logique .or. VarSto(VAR_B1) ) VarCalc(VAR_B1) = .true.

   reponse_logique = ltab2(2)
   if( reponse_logique .or. VarSto(VAR_B2) ) VarCalc(VAR_B2) = .true.

   reponse_logique = ltab2(3)
   if( reponse_logique .or. VarSto(VAR_BS) ) VarCalc(VAR_BS) = .true.

   reponse_logique = ltab2(4)
   if( reponse_logique .or. VarSto(VAR_P1) ) VarCalc(VAR_P1) = .true.

   reponse_logique = ltab2(5)
   if( reponse_logique .or. VarSto(VAR_P2) ) VarCalc(VAR_P2) = .true.

   reponse_logique = ltab2(6)
   if( reponse_logique .or. VarSto(VAR_RH1) ) then
      VarCalc(VAR_RH1) = .true.
      VarCalc(VAR_P1)  = .true.
   endif

   reponse_logique = ltab2(7)
   if( reponse_logique .or. VarSto(VAR_RH2) ) then
      VarCalc(VAR_RH2) = .true.
      VarCalc(VAR_P2)  = .true.
   endif

   reponse_logique = ltab2(8)
   if( reponse_logique .or. VarSto(VAR_V1) ) VarCalc(VAR_V1) = .true.

   reponse_logique = ltab2(9)
   if( reponse_logique .or. VarSto(VAR_V2) ) VarCalc(VAR_V2) = .true.

   reponse_logique = ltab2(10)
   if( reponse_logique .or. VarSto(VAR_TAUF) ) then
      VarCalc(VAR_TAUF) = .true.
      VarCalc(VAR_B1)   = .true.
      VarCalc(VAR_B2)   = .true.
      VarCalc(VAR_P1)   = .true.
      VarCalc(VAR_P2)   = .true.
   endif

   reponse_logique = ltab2(11)
   if( reponse_logique .or. VarSto(VAR_Y) ) VarCalc(VAR_Y) = .true.

   reponse_logique = ltab2(12)
   if( reponse_logique .or. VarSto(VAR_HMOY) ) then
      VarCalc(VAR_HMOY) = .true.
      VarCalc(VAR_B1)   = .true.
      VarCalc(VAR_B2)   = .true.
   endif

   reponse_logique = ltab2(13)
   if( reponse_logique .or. VarSto(VAR_Q2G) ) VarCalc(VAR_Q2G) = .true.

   reponse_logique = ltab2(14)
   if( reponse_logique .or. VarSto(VAR_Q2D) ) then
      VarCalc(VAR_Q2D) = .true.
      VarCalc(VAR_Q2G) = .true.
   endif

   !----------------------------------------
   ! VOL et VOLS (donc SS) toujours calcules
   ! afin de controler le volume
   !----------------------------------------
   VarCalc(VAR_VOL)  = .true.
   VarCalc(VAR_SS)   = .true.
   VarCalc(VAR_VOLS) = .true.

   reponse_logique = ltab2(15)
   if( reponse_logique .or. VarSto(VAR_CHARG) ) VarCalc(VAR_CHARG) = .true.

   if( VarSto(VAR_ZMAX)  ) VarCalc(VAR_ZMAX)  = .true.
   if( VarSto(VAR_TZMAX) ) VarCalc(VAR_TZMAX) = .true.
   if( VarSto(VAR_VZMAX) ) VarCalc(VAR_VZMAX) = .true.
   if( VarSto(VAR_ZMIN)  ) VarCalc(VAR_ZMIN)  = .true.
   if( VarSto(VAR_TZMIN) ) VarCalc(VAR_TZMIN) = .true.
   if( VarSto(VAR_V1MIN) ) VarCalc(VAR_V1MIN) = .true.
   if( VarSto(VAR_V1MAX) ) VarCalc(VAR_V1MAX) = .true.
   if( VarSto(VAR_BMAX)  ) VarCalc(VAR_BMAX)  = .true.
   if( VarSto(VAR_TOND)  ) VarCalc(VAR_TOND)  = .true.
   if( VarSto(VAR_QMAX)  ) VarCalc(VAR_QMAX)  = .true.
   if( VarSto(VAR_TQMAX) ) VarCalc(VAR_TQMAX) = .true.
   if( VarSto(VAR_EMAX)  ) VarCalc(VAR_EMAX)  = .true.

   if( VarCalc(VAR_TZMAX).or.VarCalc(VAR_VZMAX) ) then
       VarCalc(VAR_ZMAX)  = .true.
   endif
   if (VarCalc(VAR_Q2G)) then
        VarCalc(VAR_V2) = .TRUE.
   endif
   if (VarCalc(VAR_TQMAX)) then
      VarCalc(VAR_QMAX) = .TRUE.
   endif
   if (VarCalc(VAR_QMAX)) then
      VarCalc(VAR_TQMAX) = .TRUE.
   endif

   ! Fin des traitements

   deallocate(ltab1)
   deallocate(ltab2)
   deallocate(ltab3)

   !Erreur%arbredappel = !arbredappel_old

   return

end subroutine LEC_SORTIES
